﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using csimplifyit.Model;
using Stripe.Checkout;

/// <summary>
/// Summary description for LoginRetunValue
/// </summary>
public class LoginRetunValue
{
	public LoginRetunValue(int uid,string displayName,string token)
	{
		 this.uid = uid;
        this.displayName = displayName;
        this.token = token;
	}
    public int uid { get; set; }
    public string displayName { get; set; }
    public string token { get; set; }


}

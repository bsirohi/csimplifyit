﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FacebookMessengerLib;
using System.IO;
using System.Data;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FreshersVilla" in code, svc and config file together.
public class FreshersVilla : IFreshersVilla
{
    FresherVillaDataModel model = new FresherVillaDataModel();
    DataModel dmTakeMyJob = new DataModel();

    ResponseData response;
    Rewards rewards = new Rewards();
    DataTable table = new DataTable();
    oAuthLinkedIn linkedin = new oAuthLinkedIn();
    QueueForFreshervilla queue = new QueueForFreshervilla();
    RegexUtilities validationOn = new RegexUtilities();
    public Stream getToken(string inputData)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {

                }
                if (objelement.Key.Equals("data"))
                {

                    list = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            string username = null;
            string password = null;
            string authSite = null;
            foreach (var keyvalue in list)
            {
                if (keyvalue.Key.Equals("userName"))
                {
                    username = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("userPassword"))
                {
                    password = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("authSite"))
                {
                    authSite = keyvalue.Value;
                }
            }
            string result = getPassword(username, password, authSite);
            string[] splitValuesFromGetgetPass = result.Split('&');
            if (splitValuesFromGetgetPass.Length == 1)
            {
                model.insertIntoTable("insert into Users ( userName,displayName,userType,userStatus,userPassword,modifiedBy,"
                 + "currentCompanyName,currentJobTitle,modifiedDateTime ) values" +
                 " ('" + username + "','" + null + "','" + "4'," + "'0','"
                 + dateTimeClass.datetime() + "','" + null + "','"
                 + null + "','" + null + "', now() )");
                dt = model.selectIntoTable("select uid userPassword from Users where userName='" + username + "'");
                if (dt.Rows.Count != 0)
                {
                    int uid = Convert.ToInt32(dt.Rows[0][0]);
                    byte[] array = Encoding.ASCII.GetBytes(uid.ToString() + dateTimeClass.datetime());
                    string encoded = System.Convert.ToBase64String(array);
                    model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,authSite) value (" + uid + ",'" + encoded + "','system')");
                    //Queue queue = new Queue();
                    //queue.queueRegistrationMail(uid, encoded);
                }
                response = new ResponseData(1, Constant.notvallid, "[{\"token\":\"\" ,\"uid\":\"\" }]");
            }
            else if (splitValuesFromGetgetPass[0].Equals("wrongpwd"))
            {
                response = new ResponseData(1, Constant.notvallid, "[{\"token\": \"\",\"uid\":\"" + splitValuesFromGetgetPass[1] + "\" }]");
            }
            else
            {
                dt = model.selectIntoTable("select uid from UserSkills where uid= " + splitValuesFromGetgetPass[2]);
                if (dt.Rows.Count == 0)
                {
                    response = new ResponseData(1, Constant.vallid, "[{\"token\":\"" + splitValuesFromGetgetPass[1] + "\",\"uid\":\"" + splitValuesFromGetgetPass[2] + "\",\"displayName\":\"" + splitValuesFromGetgetPass[3] + "\",\"userType\":\"" + splitValuesFromGetgetPass[4] + "\",\"isCustomerToBeCharged\":\"0\",\"havingSkills\":\"0\"  }]");
                }

                else {

                    response = new ResponseData(1, Constant.vallid, "[{\"token\":\"" + splitValuesFromGetgetPass[1] + "\",\"uid\":\"" + splitValuesFromGetgetPass[2] + "\",\"displayName\":\"" + splitValuesFromGetgetPass[3] + "\",\"userType\":\"" + splitValuesFromGetgetPass[4] + "\",\"isCustomerToBeCharged\":\"0\",\"havingSkills\":\"1\" }]");
                }
             }
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            returnString = response.returnStatment();
            return returnStmnt(returnString);
        }
    }
    private string getPassword(string username, string password, string authSite)
    {
        string uid = null;
        string displayName = null;
        string userType = null;
        try
        {
            DataTable dt;
            dt = model.selectIntoTable("select userPassword,uid,displayName,userType from Users where userName='" + username + "' and userType in (1,5,6) ");

            if (dt.Rows.Count > 0)
            {
                uid = dt.Rows[0][1].ToString();
                displayName = dt.Rows[0][2].ToString();
                userType = dt.Rows[0][3].ToString();
                if (dt.Rows[0][0].ToString().Equals(EncryptBase64.EncodeTo64(password)))
                {
                    string result = dt.Columns[0].ToString() + GenerateNonce();
                    byte[] array = Encoding.ASCII.GetBytes(result + GenerateNonce());
                    string encoded = System.Convert.ToBase64String(array);
                    dt = model.selectIntoTable("select * from AuthenticationTokens where regUserid=" + uid + " and authSite='takemyjob'");
                    if (dt.Rows.Count != 0)
                    {
                        model.updateIntoTable("update AuthenticationTokens set lastModifiedDate=now(), token='" + encoded + "' where regUserid=" + uid + " and authSite='takemyjob'");
                        return "vallid&" + encoded + "&" + uid + "&" + displayName + "&" + userType;
                    }
                    else
                    {
                        model.insertIntoTable("insert into AuthenticationTokens (lastModifiedDate,token,regUserid,authSite) values (now(),'" + encoded + "'," + uid + ",'takemyjob')");
                        return "vallid&" + encoded + "&" + uid + "&" + displayName + "&" + userType; ;
                    }
                }
                return "wrongpwd&" + uid;
            }
            else
            {
                dt = model.selectIntoTable("select userPassword,uid from Users where userName='" + username + "'");
                if (dt.Rows.Count == 0)
                {
                    return "saveuser";
                }
                else
                {
                    return "wrongpwd&" + uid;
                }
            }
        }
        catch (Exception exc)
        {

            throw exc;
        }
    }
    private string calculateMD5Hash(string input)
    {
        // step 1, calculate MD5 hash from input
        MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        byte[] hash = md5.ComputeHash(inputBytes);

        // step 2, convert byte array to hex string
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }

        return sb.ToString();
    }
    public Stream getRefrenceFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int regid = 0;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getRefrenceFriend", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("registeruser"))
                        {
                            regid = Convert.ToInt32(list.Value);
                            dt = model.selectIntoTable("SELECT distinct(uid),count(toID) as pendingRequests,displayName,userName,userContactNo," +
                                "refUserPrivacyConcern as isBlocked ,userType,userStatus,currentCompanyName,currentJobTitle,currentIndustry FROM " +
                              " Users A,RefereredUsers B  Left JOIN   FriendRequests As F ON F.RequestStatus=0" +
                                " and F.introducedBy=B.refUserId and F.toID=" + regid
                                + " WHERE A.userType!=2 and A.uid = B.refUserId AND B.regUserId =" + regid +
                                "  group by uid order by pendingRequests desc, displayName");
                            if (dt.Rows.Count != 0)
                            {
                                response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                            else
                            {
                                response = new ResponseData(1, Constant.foundData, "[{\"uid\":\"1\",\"pendingRequests\":\"0\",\"displayName\":\"TakeMyJob\"," +
                                    "\"userName\":\"takeMyJob@csimplifyit.com\",\"userContactNo\":\"0\",\"refUserPrivacyConcern\":\"0\",\"userType\":\"0\"" +
                                   ",\"userStatus\":\"1\",\"currentCompanyName\":\"\",\"currentJobTitle\":\"You no friend found.Either use extend or login with empty username & password\",\"currentIndustry\":\"\" }]");
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }

            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream blockUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string requestingUserid = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "blockUser", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string blockUserId = null;
                    string blockRequestType = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("requestingUserid"))
                        {
                            requestingUserid = list.Value;
                        }
                        if (list.Key.Equals("blockUserId"))
                        {
                            blockUserId = list.Value;
                        }
                        if (list.Key.Equals("blockRequestType"))
                        {
                            blockRequestType = list.Value;
                        }
                    }
                    if (Convert.ToInt32(blockRequestType) <= 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " regUserPrivacyConcern='" + blockRequestType + "' , regUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                        " refUserPrivacyConcern='" + blockRequestType + "' , refUserPrivacyConcernDateTime = now() where refUserId ='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            //dt=model.selectIntoTable("select "
                            response = new ResponseData(1, Constant.blockUser, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else if (Convert.ToInt32(blockRequestType) > 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " refUserPrivacyConcern='" + blockRequestType + "', refUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                       " regUserPrivacyConcern='" + blockRequestType + "', regUserPrivacyConcernDateTime = now() where refUserId='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            response = new ResponseData(1, Constant.blockUser, "[{\"blockUserId\":\"" + blockUserId + "\"}]");
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        response = new ResponseData(0, Constant.checkDataColumns, null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream unBlockUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string requestingUserid = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "unblockUser", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string blockUserId = null;
                    string blockRequestType = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("requestingUserid"))
                        {
                            requestingUserid = list.Value;
                        }
                        if (list.Key.Equals("blockUserId"))
                        {
                            blockUserId = list.Value;
                        }
                        if (list.Key.Equals("blockRequestType"))
                        {
                            blockRequestType = list.Value;
                        }
                    }
                    if (Convert.ToInt32(blockRequestType) <= 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                           " regUserPrivacyConcern='" + 0 + "' , regUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " refUserPrivacyConcern='" + 0 + "' , refUserPrivacyConcernDateTime = now() where refUserId ='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            //  auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "specify user is blocked", null, null);
                            response = new ResponseData(1, Constant.unblockUser, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else if (Convert.ToInt32(blockRequestType) > 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                         " refUserPrivacyConcern='" + 0 + "', refUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                       " regUserPrivacyConcern='" + 0 + "', regUserPrivacyConcernDateTime = now() where refUserId='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            // auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "specify user is blocked", null, null);
                            response = new ResponseData(1, Constant.unblockUser, "[{\"unblockUserId\":\"" + blockUserId + "\"}]");
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        //  auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "user data columns is not correct", null, null);
                        response = new ResponseData(0, Constant.checkDataColumns, null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            // auditInfo("takemyjob", "unblockuser", "want to un block friends", Convert.ToInt32(requestingUserid), "user not exists", null, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            //   auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private List<KeyValuePair<string, string>> jsonToKeyValuePair(string json)
    {
        var list = new List<KeyValuePair<string, string>>();
        // KeyValuePair<string, string> jsonkeyvalue=new KeyValuePair<string,string>(null,null);
        try
        {
            JObject root = JObject.Parse(json);
            JArray items = (JArray)root["data"];
            foreach (var data in items)
            {
                JObject localdata = JObject.Parse(data.ToString());
                foreach (var jsonobject in localdata)
                {
                    {
                        list.Add(new KeyValuePair<string, string>(jsonobject.Key.ToString(), jsonobject.Value.ToString()));
                    }
                }

            }
            return list;
        }
        catch (Exception exc)
        {
            throw exc;
        }
        throw new NotImplementedException();
    }
    public Stream postNewJob(string inputData)
    {
        string returnString = "";
        int userID = 0;
        DataTable dt = new DataTable();
        try
        {
            string jobIDInJson = null;
            Boolean flag = false;
            JObject jsonrow = JObject.Parse(inputData);
            List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
            foreach (var rowobj in jsonrow)
            {
                if (rowobj.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(rowobj.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(returnString);
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "post new Job", userID);
                            }
                        }
                    }
                }
                if (rowobj.Key.Equals("data") && flag == true)
                {
                    jobIDInJson = insertFromKeyValuePostJob("Job", "{\"data\":" + rowobj.Value.ToString() + "}");
                    //  string response = model.insertIntoTable(query);

                    if (jobIDInJson != null)
                    {
                        response = new ResponseData(1, "Successfully posted", jobIDInJson);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(0, "input parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }

            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            response = new ResponseData(0, exc.Message, null);
            //returnString = "{\"status\":0,\"Description\":\""+exc.Message+"\",\"results\":[]}";
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream applyForJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        string appliedByUser = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            Boolean flag = false;
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "apply Job", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data"))
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            if (flag == true)
            {
                string jobRefid = null;
                foreach (var list in jsonkeyvalue)
                {
                    if (list.Key.Equals("appliedByUser"))
                    {
                        appliedByUser = list.Value.ToString();
                    }
                    if (list.Key.Equals("jobRefid"))
                    {
                        jobRefid = list.Value.ToString();
                    }
                }
                if (appliedByUser != null && jobRefid != null)
                {
                    dt = model.selectIntoTable("SELECT Jaid FROM JobApplications where jobRefid=" + jobRefid + " and appliedByUser=" + appliedByUser);
                    if (dt.Rows.Count == 0)
                    {
                        foreach (var objelement in jsonrow)
                        {
                            if (objelement.Key.Equals("data"))
                            {
                                dt = model.insertIntoTableWithLastId("insert into JobApplications (" +
                                    "appliedOnDateTime,applicationStatus,jobRefid,appliedByUser) values (now(),1," + jobRefid + "," + appliedByUser + ")", "jaid");
                                if (dt.Rows.Count != 0)
                                {
                                    response = new ResponseData(1, Constant.applicationPostSuccessfully, "[{\"jobID\":\"" + jobRefid + "\"}]");
                                    return returnStmnt(response.returnStatment());
                                }
                            }
                        }
                    }
                    else
                    {
                        response = new ResponseData(1, Constant.alreadyapplyjob, "[{\"jobID\":\"" + jobRefid + "\"}]");
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(1, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            response = new ResponseData(0, exc.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public string convertDataTableToJson(object value)
    {
        Type type = value.GetType();

        Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

        json.NullValueHandling = NullValueHandling.Ignore;

        json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
        json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
        json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        if (type == typeof(DataTable))
            json.Converters.Add(new DataTableConverter());
        else if (type == typeof(DataSet))
            json.Converters.Add(new DataSetConverter());

        StringWriter sw = new StringWriter();
        Newtonsoft.Json.JsonTextWriter writer = new JsonTextWriter(sw);
        writer.Formatting = Formatting.None;
        writer.QuoteChar = '"';
        json.Serialize(writer, value);
        string output = sw.ToString();
        writer.Close();
        sw.Close();

        return output;
    }
    //private string convertDataTableToJson(DataTable dt)
    //{
    //    try
    //    {
    //        StringBuilder Sb = new StringBuilder();

    //        string[] StrDc = new string[dt.Columns.Count];
    //        string HeadStr = string.Empty;
    //        for (int i = 0; i < dt.Columns.Count; i++)
    //        {
    //            StrDc[i] = dt.Columns[i].Caption;
    //            HeadStr += "\"" + StrDc[i] + "\" : \"" + StrDc[i] + i.ToString() + "¾" + "\",";
    //        }
    //        HeadStr = HeadStr.Substring(0, HeadStr.Length - 1);
    //        Sb.Append("[");
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            string TempStr = HeadStr;
    //            Sb.Append("{");
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {
    //             string data = (dt.Rows[i][j].ToString().Replace("\n", " ")).Replace('"',' ');
    //             if (vallidateJson(data).Equals(true))
    //             {
    //                 TempStr = TempStr.Replace(dt.Columns[j] + j.ToString() + "¾", data);
    //             }
    //             else {
    //                 TempStr = TempStr.Replace(dt.Columns[j] + j.ToString() + "¾", "");
    //             }
    //            }
    //            Sb.Append(TempStr + "},");
    //        }
    //        Sb = new StringBuilder(Sb.ToString().Substring(0, Sb.ToString().Length - 1));
    //        if (dt.Rows[0][0] != null)
    //            Sb.Append("]");

    //        return Sb.ToString();
    //    }
    //    catch (Exception exc)
    //    {
    //        throw exc;
    //    }
    //}
    //private string convertDataTableToJson(DataSet ds) { 

    //}
    private string insertFromKeyValuePostJob(string tableName, string jsonarr)
    {
        DataTable dt = new DataTable();
        StringBuilder insertStmt = new StringBuilder();
        string jobTitle = null;
        string postedBy = null;
        string jobSkills = null;
        string jobLocationName = null;
        string jobLatitude = null;
        string jobLongitude = null;
        string postToSocialSite = null;
        string jobReferenceLink = null;
        string jobComments = null;
        string jobExperienceMinYears = null;
        string jobExperienceMaxYears = null;
        string jobVertical = null;
        string jobRole = null;
        string jobMinSalary = null;
        string jobMaxSalary = null;
        string jobSalaryCurrency = null;
        string canDisplayPostedByName = null;
        string conductInterview = null;
        string conductTest = null;
        string sponsoredJob = null;
        string candidateToPay = null;
        string findSkillProfiles = null;
        string associatedModule = null;
		
		string projectId = null;
        string examId = null;

        insertStmt.Append("insert into " + tableName + "(jobStatus,jobType,postedOnDateTime,jobExpiresOn,joblastModifiedOn," +
            "jobTitle,postedBy,jobSkills,jobLocationName,jobLatitude,jobLongitude,canDisplayPostedByName,jobReferenceLink,jobComments" +
            ",jobExperienceMinYears,jobExperienceMaxYears,jobVertical,jobRole,jobMinSalary,jobMaxSalary,postToSocialSite,jobSalaryCurrency," +
            "conductInterview,conductTest,sponsoredJob,candidateToPay,findSkillProfiles,associatedModule,projectId,examId) ");

        StringBuilder values = new StringBuilder();
        values.Append("values (1,2,now(),DATE_ADD(now(),INTERVAL 30 DAY),now(),");
        try
        {
            JObject root = JObject.Parse(jsonarr);
            JArray items = (JArray)root["data"];
            foreach (var data in items)
            {
                JObject localdata = JObject.Parse(data.ToString());
                foreach (var jsonobject in localdata)
                {
                    if (jsonobject.Key.Equals("jobTitle"))
                    {
                        jobTitle = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("postedBy"))
                    {
                        postedBy = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobSkills"))
                    {
                        jobSkills = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobLocationName"))
                    {
                        jobLocationName = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobLatitude"))
                    {
                        jobLatitude = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("postToSocialSite"))
                    {
                        postToSocialSite = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("jobMaxSalary"))
                    {
                        jobMaxSalary = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobSalaryCurrency"))
                    {
                        jobSalaryCurrency = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobLongitude"))
                    {
                        jobLongitude = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobReferenceLink"))
                    {
                        jobReferenceLink = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobComments"))
                    {
                        jobComments = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobExperienceMinYears"))
                    {
                        jobExperienceMinYears = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobExperienceMaxYears"))
                    {
                        jobExperienceMaxYears = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobVertical"))
                    {
                        jobVertical = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobRole"))
                    {
                        jobRole = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobMinSalary"))
                    {
                        jobMinSalary = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("conductInterview"))
                    {
                        conductInterview = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("conductTest"))
                    {
                        conductTest = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("sponsoredJob"))
                    {
                        sponsoredJob = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("candidateToPay"))
                    {
                        candidateToPay = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("findSkillProfiles"))
                    {
                        findSkillProfiles = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("associatedModule"))
                    {
                        associatedModule = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("canDisplayPostedByName"))
                    {
                        canDisplayPostedByName = jsonobject.Value.ToString();

                    }
					if (jsonobject.Key.Equals("projectId"))
                    {
                        projectId = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("examId"))
                    {
                        examId = jsonobject.Value.ToString();

                    }
                }
            }
            if (jobTitle != null && jobSkills != null && jobLocationName != null && postedBy != null && jobLatitude != null && jobLongitude != null)
            {
                values.Append("'" + jobTitle + "'," + "'" + postedBy + "'," + "'" + jobSkills + "'," + "'" + jobLocationName + "'," + "'" + jobLatitude + "'," + "'" + jobLongitude + "',"
                + "'" + canDisplayPostedByName + "'," + "'" + jobReferenceLink + "'," +
                "'" + jobComments + "'," + "'" + jobExperienceMinYears + "'," + "'" + jobExperienceMaxYears + "'," + "'" + jobVertical + "'," + "'" + jobRole + "'," +
                "'" + jobMinSalary + "'," + "'" + jobMaxSalary + "'," + "'" + postToSocialSite + "'," + "'" + jobSalaryCurrency + "'," + "'" + conductInterview + "'" +
                "," + "'" + conductTest + "'," + "'" + sponsoredJob + "'," + "'" + candidateToPay + "'," + "'" + findSkillProfiles + "'," + "'"+associatedModule+ "',"+"'"+ projectId+"',"+"'"+ examId+ "')");
                dt = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "jid");
                string spName = "jobLinkToRefrenceMaterials";
                string url = "@url";
                int jbId = Convert.ToInt32(dt.Rows[0][0]);
                string refType = "1";
                callStoredProcedure sp = new callStoredProcedure();
                sp.callStoreProcedureToFetchVideo(spName, refType, jbId, url);
                oAuthLinkedIn li = new oAuthLinkedIn();
               // if (Convert.ToInt32(postToSocialSite) == 1)
                    //li.updateJobOnTakeMyJob("Looking for  :" + jobTitle, jobSkills, "Min Exp=" + jobExperienceMinYears + ",Max Exp=" + jobExperienceMaxYears + " " + "Job Location=" + jobLocationName, "Min Salary=" + jobMinSalary + ",Max MaxSalary=" + jobMaxSalary);
                    if (dt.Rows.Count != 0)
                    {

                        model.updateIntoTable("Update Users set lattitude=" + jobLatitude + ",longitude=" + jobLongitude +
                            ",lastCheckInLocation='" + jobLocationName + "' where uid=" + postedBy);
                        return convertDataTableToJson(dt);
                    }

            }

            return null;
        }
        catch (Exception exc)
        {
            throw exc;
        }
    }
    public virtual string GenerateNonce()
    {
        Random random = new Random();
        // Just a simple implementation of a random number between 123400 and 9999999
        return random.Next(123400, 9999999).ToString();
    }
    public Stream getLov(string inputData)
    {
        /*string jsonWhereClause = null;*/
        string returnString = "";
        try
        {
            DataTable dt = new DataTable();

            List<KeyValuePair<string, string>> jsonkeyvalue;
            jsonkeyvalue = jsonToKeyValuePair(inputData);
            if (jsonkeyvalue[0].Value.Equals(""))
            {
                if (jsonkeyvalue[1].Key.Equals("jsonwhereclause"))
                {
                    dt = model.selectIntoTable("SELECT * FROM  ListofValues " + jsonkeyvalue[1].Value.ToString());
                }
                else
                {
                    returnString = "{\"status\":0,\"Description\":\"input string is not in correct format\",\"results\":[null]}";
                }
                string result = convertDataTableToJson(dt);

                returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[" + result + "]}";
            }
            else
            {
                returnString = "{\"status\":0,\"Description\":\"not a vallid token\",\"results\":[null]}";
            }
        }
        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":\"null\"}";
        }
        return new MemoryStream(Encoding.UTF8.GetBytes(returnString));
    }/* no result found is on description if no data found and status success */
    public Stream getJobs(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;// vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getJobs", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    string result = null;
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("userID"))
                    {
                        userID = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select distinct(jid),sponsoredJob,(case When canDisplayPostedByName = 1 then  U.displayName else 'Guess IT' end ) as postedByName,jobMinSalary,jobMaxSalary,jobSalaryCurrency,canDisplayPostedByName,DateDiff(J.jobExpiresOn,now()) as expireinDays,count(JA.jaid) as jobApplications,DATE_FORMAT(JA2.appliedOnDateTime,'%d-%b-%Y') as jobAppliedOnDate,(JA2.appliedByUser IS NOT NULL) as isJobApplied" +
                        ",postedBy,DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime,J.conductInterview,J.conductTest,J.sponsoredJob,J.candidateToPay,J.findSkillProfiles, " +
                        "jobTitle,(case When jobReferenceLink = 'null' then 'No url' else jobReferenceLink end ) as jobReferenceLink,joblatitude,jobLongitude,(case When jobLocationName = 'null' then 'No location'  else jobLocationName end ) as jobLocationName ,jobType,jobComments," +
                        "jobSkills,DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') " +
                        "as jobExpiresOn ,DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,SUM(JA.applicationNotes IS NOT NULL) as commentsCount," +
                         "jobExperienceMinYears,jobExperienceMaxYears,jobVertical,jobRole from RefereredUsers As R,Users as U,Job As J Left Join JobApplications As JA on " +
                        "JA.jobRefid=J.jid Left join JobApplications As JA2 on JA2.appliedByUser=" + userID + " and JA2.jobRefid=J.jid where  J.sponsoredJob != 1 AND J.postedBy = R.regUserID and J.postedBy=U.uid AND  R.refUserID =" + userID +
                          " and jobStatus = 1 and jobExpiresOn > now() and refUserPrivacyConcern = 0 group by jid " +
                          "UNION  " +
                          "select distinct(jid),sponsoredJob,(case When canDisplayPostedByName = 1 then  U.displayName else 'Guess IT' end ) as postedByName,jobMinSalary,jobMaxSalary,jobSalaryCurrency,canDisplayPostedByName,DateDiff(J.jobExpiresOn,now()) as expireinDays,count(JA.jaid) as jobApplications,DATE_FORMAT(JA2.appliedOnDateTime,'%d-%b-%Y') as jobAppliedOnDate,(JA2.appliedByUser IS NOT NULL) as isJobApplied" +
                        ",postedBy,DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime,J.conductInterview,J.conductTest,J.sponsoredJob,J.candidateToPay,J.findSkillProfiles, " +
                        "jobTitle,(case When jobReferenceLink = 'null' then 'No url' else jobReferenceLink end ) as jobReferenceLink,joblatitude,jobLongitude,(case When jobLocationName = 'null' then 'No location'  else jobLocationName end ) as jobLocationName ,jobType,jobComments," +
                        "jobSkills,DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') " +
                        "as jobExpiresOn ,DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,SUM(JA.applicationNotes IS NOT NULL) as commentsCount," +
                         "jobExperienceMinYears,jobExperienceMaxYears,jobVertical,jobRole from " +
                         "Users as U,Job As J Left Join JobApplications As JA on JA.jobRefid=J.jid Left join JobApplications As JA2 on JA2.appliedByUser=" + userID + " and " +
                         " JA2.jobRefid=J.jid where  J.sponsoredJob=1 and J.postedBy=U.uid and jobStatus = 1 and jobExpiresOn > now()" +
                          "group by jid order by isJobApplied , expireinDays");
                        if (dt.Rows.Count != 0)
                        {
                            result = convertDataTableToJson(dt);
                        }
                        if (result != null)
                        {
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean vallidateToken(string token)
    {
        table = model.selectIntoTable("select * from AuthenticationTokens where token='" + token + "' and authSite='takemyjob'");
        if (table.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private List<KeyValuePair<string, string>> metaToKeyValuePair(string meta)
    {
        var list = new List<KeyValuePair<string, string>>();
        // KeyValuePair<string, string> jsonkeyvalue=new KeyValuePair<string,string>(null,null);
        try
        {
            JObject jobj = JObject.Parse(meta);

            foreach (var data in jobj)
            {
                list.Add(new KeyValuePair<string, string>(data.Key.ToString(), data.Value.ToString()));

            }
            return list;
        }
        catch (Exception exc)
        {
            throw exc;
        }


    }
    public Stream knowMyFriends(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        //  string returnString = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "knowMyFriends", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != 0)
                {
                    Boolean resposnevalue = insertJsonintoUsers("Users", "{\"data\":" + objelement.Value.ToString() + "}", Convert.ToInt32(userID));
                    if (resposnevalue == true)
                    {
                        response = new ResponseData(1, "Contact has been saved", null);
                        return returnStmnt(response.returnStatment());
                    }
                }

            }
            response = new ResponseData(0, Constant.invallidtoken, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            //  returnString = "{\"status\":0,\"Description\":\"" + exc.Message + "\",\"results\":[null]}";
            response = new ResponseData(0, exc.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertJsonintoUsers(string tableName, string jsonarr, int uid)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = model.selectIntoTable("select displayName from Users where uid=" + uid.ToString());
            if (dt.Rows.Count != 0)
            {
                string modifiedBy = dt.Rows[0][0].ToString();
                string userContactNo = null;
                string userName = null;
                string source = null;
                string friendName = null;
                StringBuilder insertStmt = new StringBuilder();
                StringBuilder values = new StringBuilder();

                JObject root = JObject.Parse(jsonarr);
                JArray items = (JArray)root["data"];
                foreach (var data in items)
                {
                    insertStmt.Append("insert into " + tableName + "( userType,userStatus,userPassword,modifiedBy,modifiedDateTime,createdBy,createdDatetime,userName,userContactNo,displayName)");
                    values.Append("values ('0','0','" + dateTimeClass.datetime() + "','" + modifiedBy + "',now(),'" + modifiedBy + "',now(),");
                    JObject localdata = JObject.Parse(data.ToString());
                    foreach (var jsonobject in localdata)
                    {
                        if (jsonobject.Key.Equals("displayName"))
                        {
                            friendName = jsonobject.Value.ToString();
                        }
                        else if (jsonobject.Key.Equals("source"))
                        {
                            source = jsonobject.Value.ToString();
                        }
                        else if (jsonobject.Key.Equals("userContactNo"))
                        {
                            userContactNo = jsonobject.Value.ToString();

                        }
                        else if (jsonobject.Key.Equals("userName"))
                        {
                            userName = jsonobject.Value.ToString();
                        }

                    }
                    if (friendName != null && userContactNo != null && friendName != "" && userContactNo != "")
                    {
                        values.Append("'" + userName + "'," + "'" + userContactNo + "'," + "'" + friendName + "')");
                        string query = insertStmt.Append(values.ToString()).ToString();
                        insertStmt.Clear();
                        values.Clear();
                        try
                        {
                            dt = model.selectIntoTable("select distinct(uid) from Users where userContactNo='" + userContactNo + "' OR userName='" + userName + "'");
                            if (dt.Rows.Count == 0)
                            {
                                dt = model.insertIntoTableWithLastId(query, "uid");

                                if (dt.Rows.Count != 0)
                                {
                                    string refuid = dt.Rows[0][0].ToString();
                                    model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + uid + "','" + refuid + "',now())");
                                    model.insertIntoTable("insert into AuthenticationTokens (regUserId,lastModifiedDate,createdDateTime,authSite)values('" + refuid + "',now(),now(),'" + source + "')");
                                }
                            }
                            else
                            {
                                model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + uid + "','" + dt.Rows[0][0].ToString() + "',now())");
                            }
                        }

                        catch (Exception exc) { Console.WriteLine(exc.Message); }
                    }
                }

            }
            else { return false; }
            return true;
        }
        catch (Exception exc)
        {
            throw exc;
        }

    }
    public string testfunc()
    {
        try
        {

            return "";
        }
        catch (Exception exc) { return exc.Message; }



    }
    private Stream returnStmnt(string returnstmnt)
    {
        return new MemoryStream(Encoding.UTF8.GetBytes(returnstmnt));
    }
    private string convertTableIntoJob(DataTable dt, int registerUid)
    {
        string jobRole = null;
        string jobTitle = null;
        StringBuilder sb = new StringBuilder();
        StringBuilder sbforkey = new StringBuilder();
        try
        {
            int count = dt.Rows.Count;
            sb.Append("insert into Job (postedBy,postedOnDateTime,jobReferenceLink,jobLatitude,jobLongitude" +
                ",jobLocationName,jobType,jobComments,jobExpiresOn," +
                "joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,jobVertical,"
                + "JobTitle,jobRole,jobSkills ) values ('" + registerUid.ToString() +
                "',now(),'null','null','null','null',1,'null',DATE_ADD(now(),INTERVAL 30 DAY),now(),'1','1','2','1','");
            if (count != 0)
            {
                jobRole = dt.Rows[0][1].ToString();
                jobTitle = dt.Rows[0][1].ToString();
                for (int i = 0; i < count; i++)
                {
                    sbforkey.Append(dt.Rows[i][3].ToString() + ", ");
                }
            }
            sb.Append(jobTitle + "','" + jobRole + "','" + sbforkey.ToString() + "')");
            dt = model.selectIntoTable("select jid from Job where postedBy='" + registerUid.ToString()
                + "' and jobType=1 and now() < jobExpiresOn;");
            if (dt.Rows.Count == 0)
            {
                dt = model.insertIntoTableWithLastId(sb.ToString(), "jid");
                if (dt.Rows.Count != 0)
                {
                    //  dt = model.selectIntoTable("select "
                    return "1&" + dt.Rows[0][0].ToString();
                }
            }
            else
            {
                //  dt = model.selectIntoTable("select jid, max(jobExpiresOn) from Job where postedBy='" + registerUid.ToString()
                //+ "' and jobType=1 ");
                return "-1&" + dt.Rows[0][0].ToString();

            }

        }

        catch (Exception exc)
        {
            throw exc;
        }

        return sb.ToString();
    }
    public Stream takemyJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "takeMyJob", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("registerUid"))
                        {
                            dt = model.selectIntoTable("Select  u.currentJobTitle,u.currentIndustry,u.currentCompanyName,luvValueDisplayName " +
                             "from  Users u, UserSkills s, ListofValues  l " +
                               "where u.uid = " + list.Value.ToString() +
                                " and u.uid = s.uid" +
                                " and s.skillRef = l.luvid" +
                                 " Order by l.luvValueDisplayName;");
                            if (dt.Rows.Count == 0)
                            {
                                dt = model.selectIntoTable("Select  u.currentJobTitle,u.currentIndustry,u.currentCompanyName,u.currentJobTitle as luvValueDisplayName from Users as u where uid= " + list.Value.ToString());
                            }
                            string[] resultstring = convertTableIntoJob(dt, Convert.ToInt32(list.Value)).Split('&');
                            if (resultstring[0].Equals("1"))
                            {
                                response = new ResponseData(1, "Job successfully Posted", "[{\"jobID\" :\"" + resultstring[1] + "\"}]");
                                return returnStmnt(response.returnStatment());
                            }
                            else
                            {
                                response = new ResponseData(1, Constant.jobalreadyposted, "[{\"jobID\" :\"" + resultstring[1] + "\"}]");
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }
    }
    public Stream optOut(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "optOut", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (userID != 0)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update Users set" +
                          " userType='2' , modifiedDateTime = now() where uid='" + userID + "'");
                        int rowaffectingjobtable = model.updateIntoTablewithRowCount("update Job set" +
                          " jobStatus='3' where postedBy='" + userID + "'");
                        model.deleteFromTable("delete from RefereredUsers where regUserId=" + userID);
                        model.deleteFromTable("delete from RefereredUsers where refUserId=" + userID);
                        if (noOfRowAffecting >= 1 && rowaffectingjobtable >= 0)
                        {
                            //  returnString = "{\"status\":0,\"Description\":\"user  blocked\",\"results\":[null]}";
                            response = new ResponseData(0, "User Blocked", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        //returnString = "{\"status\":0,\"Description\":\"Check Column names of DataField\",\"results\":[null]}";
                        response = new ResponseData(0, "Check columns of inputData", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            // returnString = "{\"status\":0,\"Description\":\"this blockuser is not exist  \",\"results\":[null]}";
            response = new ResponseData(0, "this blockuser is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream payments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        //int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string paymentSource = null;
                    int paymentFromUserId = 0;
                    int paymentStatus = 0;
                    string paymentRecieptID = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("paymentSource"))
                        {
                            paymentSource = list.Value;
                        }
                        if (list.Key.Equals("paymentFromUserId"))
                        {
                            paymentFromUserId = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentStatus"))
                        {
                            paymentStatus = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentRecieptID"))
                        {
                            paymentRecieptID = list.Value;
                        }
                    }
                    if (Convert.ToInt32(paymentFromUserId) != 0 && paymentSource != null && paymentRecieptID != null && paymentStatus != 0)
                    {

                    }


                }
            }
            response = new ResponseData(0, "input parameter are not correct", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getcurrentRewards(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        //int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string paymentSource = null;
                    int paymentFromUserId = 0;
                    int paymentStatus = 0;
                    string paymentRecieptID = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("paymentSource"))
                        {
                            paymentSource = list.Value;
                        }
                        if (list.Key.Equals("paymentFromUserId"))
                        {
                            paymentFromUserId = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentStatus"))
                        {
                            paymentStatus = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentRecieptID"))
                        {
                            paymentRecieptID = list.Value;
                        }
                    }
                    if (Convert.ToInt32(paymentFromUserId) != 0 && paymentSource != null && paymentRecieptID != null && paymentStatus != 0)
                    {

                    }


                }
            }
            response = new ResponseData(0, "input parameter are not correct", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private void auditInfo(string inputData, int auditEvent, string eventDoneByUser, int associatedRefId)
    {
        try
        {
            DataTable dt = new DataTable();
            inputData = inputData.Replace("'", "\"");
            // dt = model.selectIntoTable("select luvid from ListofValues where luvValueDisplayName='" + luvValueDisplayName + "' and luvCategory='" + luvCategory + "'");
            if (inputData.Length > 500)
            {
                string[] arrOfString = splitByLength(inputData, 499).ToArray();
                if (arrOfString.Length == 2)
                    model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
                        "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
                        + eventDoneByUser + "','" + arrOfString[0] + "','" + arrOfString[1] + "','null'," + associatedRefId + ",now())");
                else
                {
                    model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
         "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
         + eventDoneByUser + "','" + arrOfString[0] + "','" + arrOfString[1] + "','" + arrOfString[2] + "'," + associatedRefId + ",now())");
                }
            }
            else
            {
                model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
        "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
        + eventDoneByUser + "','" + inputData + "','" + null + "','null'," + associatedRefId + ",now() )");
            }
        }
        catch (Exception exc) { Console.WriteLine(exc.Message); }
    }
    public List<string> splitByLength(string s, int length)
    {
        List<string> a = new List<string>();
        for (int i = 0; i < s.Length; i += 499)
        {
            if ((i + 499) < s.Length)
                a.Add(s.Substring(i, 499));
            else
                a.Add(s.Substring(i));
        }
        return a;
    }
    public Stream contentUpload(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo("Base64 string", 1, "contentUpload", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string associatedID = null;
                    string fileContent = null;
                    int fileMimeType = 0;
                    string fileName = null;
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("associatedID"))
                        {
                            associatedID = list.Value;
                        }
                        if (list.Key.Equals("fileContent"))
                        {
                            fileContent = list.Value;
                        }
                        if (list.Key.Equals("fileMimeType"))
                        {
                            fileMimeType = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("fileName"))
                        {
                            fileName = list.Value;
                        }
                    }
                    if (fileContent != "" && fileContent != null && associatedID != null && fileMimeType != 0)
                    {
                        if (fileMimeType == 1)
                        {

                            writeFileInDirectory(associatedID, fileContent, ref fileName, "contents");
                            string relativePath = "contents/" + associatedID + fileName;

                            dt = model.selectIntoTable("select rid from Resume where refUserID=" + associatedID);
                            if (dt.Rows.Count == 0)
                            {
                                dt = model.insertIntoTableWithLastId("insert into Resume (refDocumentPath,refUserID,uploadDateTime,resumeTitle,resumeMime)" +
                                  "values ('" + relativePath + "','" + associatedID + "',now(),'" + fileName + "','" + fileMimeType + "')", "rid");
                                if (dt.Rows.Count != 0)
                                {
                                    response = new ResponseData(1, "saved", convertDataTableToJson(dt));
                                    return returnStmnt(response.returnStatment());
                                }
                            }
                            else
                            {
                                int rowcount = model.updateIntoTablewithRowCount("update Resume set refDocumentPath='" + relativePath +
                                    "',uploadDateTime=now(),resumeTitle='" + fileName + "',resumeMime='" + fileMimeType + "' where refUserID=" + associatedID);
                                if (rowcount != 0)
                                {
                                    response = new ResponseData(1, "Successfully updated", convertDataTableToJson(dt));
                                    return returnStmnt(response.returnStatment());
                                }
                            }
                        }
                        else if (fileMimeType == 2)
                        {
                            string relativePath = writeFileInDirectory(associatedID, fileContent, ref fileName, "uploadFriends");
                            //   dt = GetDataTableFromCsv(relativePath);
                            string responseInArray = readFriendsFromCSV(Convert.ToInt32(associatedID), relativePath);
                            response = new ResponseData(1, "updated with result", responseInArray);
                            return returnStmnt(response.returnStatment());
                        }
                        else if (fileMimeType == 3)
                        {
                            string relativePath = writeFileInDirectory(associatedID, fileContent, ref fileName, "uploadJobs");
                            dt = GetDataTableFromCsv(relativePath);
                            string responseInArray = readJobsFromCSV(Convert.ToInt32(associatedID), relativePath, dt);
                            response = new ResponseData(1, "updated with result", responseInArray);
                            return returnStmnt(response.returnStatment());
                        }
                        else if (fileMimeType == 4)
                        {
                            string relativePath = writeFileInDirectory(associatedID, fileContent, ref fileName, "uploadRefrenceMaterial");
                            dt = GetDataTableFromCsv(relativePath);
                            string responseInArray = readJobsFromCSV(Convert.ToInt32(associatedID), relativePath, dt);
                            response = new ResponseData(1, "updated with result", responseInArray);
                            return returnStmnt(response.returnStatment());
                        }
                        else if (fileMimeType == 5)
                        {
                            string relativePath = writeFileInDirectory(associatedID, fileContent, ref fileName, "uploadRefrenceMaterial");
                            dt = GetDataTableFromCsv(relativePath);
                            string responseInArray = readJobsFromCSV(Convert.ToInt32(associatedID), relativePath, dt);
                            response = new ResponseData(1, "updated with result", responseInArray);
                            return returnStmnt(response.returnStatment());
                        }
                        else if (fileMimeType == 6)
                        {
                            string relativePath = writeFileInDirectory(associatedID, fileContent, ref fileName, "questionCSV");
                            dt = GetDataTableFromCsv(relativePath);
                            string responseInArray = readQuestionFromCSV(Convert.ToInt32(associatedID), relativePath, dt);
                            response = new ResponseData(1, "Question Uploaded Successfully:", responseInArray);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "input parameter is not vallid", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private static string writeFileInDirectory(string associatedID, string fileContent, ref string fileName, string fileSaveFolder)
    {
        fileName = fileName.Replace(" ", string.Empty);
        fileName = fileName.Replace(associatedID.ToString(), string.Empty);
        string relativePath = fileSaveFolder + "/" + associatedID + fileName;
        string str_uploadpath = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, relativePath);
        FileStream fileToupload = new FileStream(str_uploadpath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        byte[] bytearray = new byte[10000];
        bytearray = Convert.FromBase64String(fileContent);
        fileToupload.Write(bytearray, 0, bytearray.Length);
        fileToupload.Close();
        fileToupload.Dispose();
        return str_uploadpath;
    }
    public DataTable GetDataTableFromCsv(string path)
    {
        DataTable dataTable = new DataTable();
        string[] csvRows = System.IO.File.ReadAllLines(path);
        string[] headers = csvRows[0].Split(',');

        // Adding columns name
        foreach (var item in headers)
        {
            dataTable.Columns.Add(new DataColumn(item));
        }
        dataTable.Columns.Add(new DataColumn("status"));
        string[] fields = null;
        int count = 0;
        try
        {

            foreach (string csvRow in csvRows)
            {
                if (count != 0)
                {
                    fields = csvRow.Split(',');
                    if (fields.Length > headers.Length)
                    {
                        continue;
                    }
                    DataRow row = dataTable.NewRow();
                    row.ItemArray = fields;
                    row[fields.Length] = "";
                    dataTable.Rows.Add(row);
                }
                else
                {
                    count++;
                }
            }
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);


        }
        return dataTable;
    }
    private string readFriendsFromCSV(int uid, string filePath)
    {
        string friendName = null;
        string emailID = null;
        string contactNO = null;
        string currentJobTitle = null;
        string friendSkills = null;
        string createdBy = null;
        string currentCompany = null;
        DataTable dt = new DataTable();
        if (uid != 0)
        {
            dt = model.selectIntoTable("select displayName from Users where uid=" + uid);
            if (dt.Rows.Count != 0)
            {
                createdBy = dt.Rows[0][0].ToString();
            }
        }
        StringBuilder jsonarray = new StringBuilder();
        jsonarray.Append("[{");

        var reader = new StreamReader(File.OpenRead(@Path.Combine(HttpRuntime.AppDomainAppPath, filePath)));
        List<string> listColumnName = new List<string>();
        List<string> listValues = new List<string>();
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnName in appendValues)
            {
                listColumnName.Add(columnName);
            }
            break;
        }
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnValues in appendValues)
            {
                listValues.Add(columnValues);
            }
            if (listColumnName.Count == 6 && listValues.Count == 6)
            {
                friendName = listValues[0];
                emailID = listValues[1];
                contactNO = listValues[2];
                currentJobTitle = listValues[3];
                friendSkills = listValues[4];
                currentCompany = listValues[5];
                string status = makeFriends(uid, createdBy, "From Excel", friendName, emailID, contactNO, currentJobTitle, friendSkills, currentCompany);
                jsonarray.Append("\"" + emailID + "\":\"" + status + "\"$");
                listValues.Clear();
                emailID = ""; friendName = ""; contactNO = ""; currentJobTitle = ""; friendSkills = ""; currentCompany = "";
            }
            else
            {
                jsonarray.Append("\"" + emailID
                    + "\":\",please ensure multiple values sepratedBy and no of values should be equal to no of  columnname ';' CSV \"$");
                listValues.Clear();
            }
        }

        return vallidJsonArray(jsonarray);
    }
    private static string vallidJsonArray(StringBuilder jsonarray)
    {
        int count = 0;

        string[] arrofStatus = jsonarray.ToString().Split('$');
        jsonarray.Clear();
        foreach (var singleStatus in arrofStatus)
        {
            if (count != arrofStatus.Length - 2)
            {
                jsonarray.Append(singleStatus + ",");
                count++;
            }
            else
            {
                jsonarray.Append(singleStatus);
            }
        }
        return jsonarray.Append("}]").ToString();
    }
    private string readJobsFromCSV(int uid, string filePath, DataTable tableForCsv)
    {
        int count = 0;
        string jobTitle = "";
        string jobMinSalary = null;
        string jobMaxSalary = null;
        string jobSalaryCurrency = null;
        string jobSkills = "";
        string jobReferenceLink = null;
        string jobExperienceMinYears = null;
        string jobExperienceMaxYears = null;
        string jobLocationName = "";
        string jobComments = null;
        string JobExpiryDays = null;
        string canDisplayPostedByName = null;
        string postToSocialSite = null;
        StringBuilder jsonarray = new StringBuilder();
        StringBuilder insertStmt = new StringBuilder();
        StringBuilder values = new StringBuilder();
        jsonarray.Append("[{");

        var reader = new StreamReader(File.OpenRead(@filePath));
        List<string> listColumnName = new List<string>();
        List<string> listValues = new List<string>();
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnName in appendValues)
            {
                listColumnName.Add(columnName);
            }
            break;
        }
        string dynamicExpiryDays = null;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnValues in appendValues)
            {
                listValues.Add(columnValues);
            }
            if (listValues.Count == 13 && listColumnName.Count == 13)
            {
                jobTitle = listValues[0];
                jobMinSalary = listValues[1];
                jobMaxSalary = listValues[2];
                jobSalaryCurrency = listValues[3];
                jobSkills = listValues[4];
                jobReferenceLink = listValues[5];
                jobExperienceMinYears = listValues[6];
                jobExperienceMaxYears = listValues[7];
                jobLocationName = listValues[8];
                jobComments = listValues[9];
                JobExpiryDays = listValues[10];
                canDisplayPostedByName = listValues[11];
                postToSocialSite = listValues[12];
                dynamicExpiryDays = JobExpiryDays == "" ? "30" : JobExpiryDays;
                if (jobTitle != "" && jobSkills != "" && jobLocationName != "" && uid != 0)
                {
                    try
                    {
                        insertStmt.Append("insert into Job (jobStatus,jobType,postedOnDateTime,jobExpiresOn,joblastModifiedOn," +
               "jobTitle,postedBy,jobSkills,jobLocationName,canDisplayPostedByName,jobReferenceLink,jobComments" +
               ",jobExperienceMinYears,jobExperienceMaxYears,jobMinSalary,jobMaxSalary,jobSalaryCurrency,postToSocialSite) ");
                        values.Append("values (1,2,now(),DATE_ADD(now(),INTERVAL " + dynamicExpiryDays + " DAY),now(),");
                        values.Append("'" + jobTitle + "'," + "'" + uid + "'," + "'" + jobSkills + "'," + "'" + jobLocationName + "'," + "'" + canDisplayPostedByName + "'," + "'" + jobReferenceLink + "'," +
        "'" + jobComments + "'," + "'" + jobExperienceMinYears + "'," + "'" + jobExperienceMaxYears + "'," +
        "'" + jobMinSalary + "'," + "'" + jobSalaryCurrency + "','" + jobMaxSalary + "'," + "'" + postToSocialSite + "')");
                        DataTable dt = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "jid");
                        listValues.Clear();
                        insertStmt.Clear();
                        values.Clear();
                        DataRow dr = tableForCsv.Rows[count];
                        dr[tableForCsv.Columns.Count - 1] = "Successfull ! jid = " + dt.Rows[0][0].ToString();
                        count++;
                        jsonarray.Append("\"" + jobTitle + "\":\"Successfull,jobID = " + dt.Rows[0][0].ToString() + "\"$");
                        jobSalaryCurrency = ""; jobTitle = ""; jobMinSalary = ""; jobMaxSalary = ""; jobSkills = ""; jobReferenceLink = ""; jobExperienceMinYears = ""; jobExperienceMaxYears = ""; jobLocationName = ""; jobComments = ""; JobExpiryDays = ""; canDisplayPostedByName = ""; postToSocialSite = "";
                    }
                    catch (Exception exc)
                    {
                        listValues.Clear();
                        insertStmt.Clear();
                        values.Clear();
                        DataRow dr = tableForCsv.Rows[count];
                        dr[tableForCsv.Columns.Count - 1] = "Fail !" + exc.Message;
                        count++;
                        jsonarray.Append("\"" + jobTitle + "\":\"Fail ! " + exc.Message + "\"$");
                    }
                }
                else
                {
                    listValues.Clear();
                    insertStmt.Clear();
                    values.Clear();
                    DataRow dr = tableForCsv.Rows[count];
                    dr[tableForCsv.Columns.Count - 1] = "Fail ! JobTitle,skills,location necessary columns";
                    count++;
                    jsonarray.Append("\"" + jobTitle + "\":\"JobTitle,skills,location necessary columns\"$");
                    jobSalaryCurrency = ""; jobTitle = ""; jobMinSalary = ""; jobMaxSalary = ""; jobSkills = ""; jobReferenceLink = ""; jobExperienceMinYears = ""; jobExperienceMaxYears = ""; jobLocationName = ""; jobComments = ""; JobExpiryDays = ""; canDisplayPostedByName = ""; postToSocialSite = "";
                }
            }
            else
            {
                listValues.Clear();
                insertStmt.Clear();
                values.Clear();
                DataRow dr = tableForCsv.Rows[count];
                dr[tableForCsv.Columns.Count - 1] = "Fail ! Exceed columns value,please ensure multiple values sepratedBy CSV";
                count++;
                jsonarray.Append("\"" + jobTitle + "\":\"Exceed columns value,please ensure multiple values sepratedBy CSV \"$");
            }
        }

        GenrateExcel excel = new GenrateExcel();
        excel.WriteToCsvFile(tableForCsv, "jobs" + uid + "processed", "csvFilesForJobProcessed");
        return vallidJsonArray(jsonarray);
    }
    private string makeFriends(int uid, string createdBy, string source, string friendName, string emailID, string contactNO, string currentJobTitle, string friendSkills, string currentCompany)
    {
        DataTable dt = new DataTable();
        try
        {
            if (friendName != null && emailID != null && friendName != "" && emailID != "")
            {
                string dynamicWhereclause = null;
                if (!contactNO.Equals(""))
                {
                    dynamicWhereclause = "userContactNo =" + contactNO + " or ";
                }
                StringBuilder insertStmt = new StringBuilder();
                StringBuilder values = new StringBuilder();
                insertStmt.Append("insert into Users" + "( userType,userStatus,userPassword,modifiedBy,modifiedDateTime,createdBy,createdDatetime,userName,userContactNo,displayName,currentJobTitle)");
                values.Append("values ('0','0','" + dateTimeClass.datetime() + "','" + createdBy + "',now(),'" + createdBy + "',now(),");
                values.Append("'" + emailID + "'," + "'" + contactNO + "'," + "'" + friendName + "','" + currentJobTitle + "')");
                string query = insertStmt.Append(values.ToString()).ToString();
                insertStmt.Clear();
                values.Clear();
                dt = model.selectIntoTable("select distinct(uid) from Users where " + dynamicWhereclause + "userName='" + emailID + "'");
                if (dt.Rows.Count == 0)
                {
                    dt = model.insertIntoTableWithLastId(query, "uid");
                    if (dt.Rows.Count != 0)
                    {
                        string refuid = dt.Rows[0][0].ToString();
                        model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + uid + "','" + refuid + "',now())");
                        model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + refuid + "','" + uid + "',now())");
                        model.insertIntoTable("insert into AuthenticationTokens (regUserId,lastModifiedDate,createdDateTime,authSite)values('" + refuid + "',now(),now(),'" + source + "')");
                        dt.Clear();
                        return "success";
                    }
                }
                else
                {
                    if (dt.Rows.Count != 0)
                    {
                        int refUserId = Convert.ToInt32(dt.Rows[0][0]);
                        dt = model.selectIntoTable("select * from RefereredUsers where regUserId='" + uid + "' and refUserId='" + refUserId + "'");
                        if (dt.Rows.Count == 0)
                        {
                            model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + uid + "','" + refUserId + "',now())");
                            dt = model.selectIntoTable("select * from RefereredUsers where regUserId='" + refUserId + "' and refUserId='" + uid + "'");
                            if (dt.Rows.Count == 0)
                            {
                                model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + refUserId + "','" + uid + "',now())");

                            }
                            //table = model.selectIntoTable("select * from FriendRequests where forFriendID=" + uid + " and toID =" + refUserId);
                            //if (table.Rows.Count == 0)
                            //{
                            //    insertStmt.Append("insert into FriendRequests (requestStatus,introducedOn,introducedBy,requestType,forFriendID,toID) values (0,now(),"
                            //        + refUserId + "," + 1 + ","
                            //        + uid + "," + refUserId + ")");
                            //    model.insertIntoTable(insertStmt.ToString());

                            //}
                            dt.Clear();
                            return "duplicate friend";
                        }
                        else
                        {
                            dt.Clear();
                            return "friend exist";
                        }
                    }

                }
            }
            return "fail";
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
            return "fail !" + exc.Message;
        }


    }
    private byte[] GetBytes(Stream filecontents)
    {
        int result = 0;
        int bytesRead;
        byte[] buffer = new byte[100000];
        do
        {
            bytesRead = filecontents.Read(buffer, 0, buffer.Length);
            result += bytesRead;
        } while (bytesRead > 0);
        return buffer.Take(result).ToArray();
    }
    public Stream getCurrentRewards(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    int associatedRegId = 0;
                    int sumOfRewardes = 0;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("associatedRegId"))
                        {
                            associatedRegId = Convert.ToInt32(list.Value);
                        }
                    }
                    if (associatedRegId != 0)
                    {
                        sumOfRewardes = rewards.sumOfRewardes(associatedRegId);
                        returnString = "{\"status\":0,\"Description\":\"Your Rewards is calculted  \",\"results\":[{\"Rewards\":" + sumOfRewardes + "}]}";
                    }
                }
            }
            returnString = "{\"status\":0,\"Description\":\"  \",\"results\":[null]}";
            return returnStmnt(returnString);
        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[null]}";
            return returnStmnt(returnString);
        }
    }
    public Stream getCDNUrl(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                //data have 4 parameters=applicationName,location,lattitude,longitude
                {
                    response = new ResponseData(1, Constant.foundData, "[{\"CDNUrl\":\"https://csimplifyit.com/FreshersVilla.svc/\"}]");
                    return returnStmnt(response.returnStatment());
                }

            }
            // auditInfo("takemyjob", "getfriends", "want to get refrence friends",regid,Constant.jsonNotCorrect , null, null);
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getInterestedFriendsInmyjobs(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int postedBy = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("postedBy"))
                    {
                        postedBy = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select jid, count(appliedbyUser) as jobApplications,(jobExpiresOn>now()) as isJobExpired,  DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle,jobReferenceLink,joblatitude,jobLongitude,jobLocationName,jobType,jobComments," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn , DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,"
                            + "jobVertical,jobRole from Job As J LEFT JOIN   JobApplications As A ON J.jid = A.jobRefId Where J.postedBy = '" + postedBy + "' group by jid order by jobApplications desc,ABS( DATEDIFF( J.jobExpiresOn, NOW() ) ), postedOnDateTime desc");
                        string result = convertDataTableToJson(dt);
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getResumesForJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            string jobID = null;
            int userID = 0;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getResumesForJob", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }
                    if (jobID != null & userID != 0)
                    {
                        dt = model.selectIntoTable("select *  from RefereredUsers where regUserId=1 and refUserId=" + userID);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(0, "User is unsubscribe", "[{\"uid\":\"" + userID + "\"");
                            return returnStmnt(response.returnStatment());

                        }
                        int qid = queue.insertdetailForPostResumes(userID, Convert.ToInt32(jobID));
                        queue.processTo(qid);
                        response = new ResponseData(1, "process is in queue", "[{\"qid\":\"" + qid + "\"}]");
                        return returnStmnt(response.returnStatment());
                    }

                }
            }
            response = new ResponseData(0, "input parameters are not completed ", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            string exception = ex.Message.Replace('\'', ' ');
            response = new ResponseData(0, exception, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getMessagesFromFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        // string returnString = null; 
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    // recievedBy,recievedFrom,recievedSince,Message,messageType
                    string recievedBy = null;
                    string recievedFrom = null;
                    string recievedSince = "";
                    string dynamicWhereClause = null;
                    JObject root = JObject.Parse("{\"data\":" + objelement.Value.ToString() + "}");
                    JArray items = (JArray)root["data"];
                    foreach (var data in items)
                    {
                        JObject localdata = JObject.Parse(data.ToString());
                        foreach (var jsonobject in localdata)
                        {
                            {
                                if (jsonobject.Key.Equals("myID"))
                                {
                                    recievedBy = jsonobject.Value.ToString();
                                }
                                if (jsonobject.Key.Equals("friendID"))
                                {
                                    recievedFrom = jsonobject.Value.ToString();
                                }
                                if (jsonobject.Key.Equals("recievedSince"))
                                {
                                    recievedSince = jsonobject.Value.ToString();
                                }
                            }
                        }
                    }
                    if (recievedSince != "")
                    {
                        DateTime dateValue = DateTime.Parse(recievedSince);
                        recievedSince = dateValue.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    dynamicWhereClause = recievedSince == "" ? "" : " and messageDateTime > '" + recievedSince + "'";
                    if (Convert.ToInt32(recievedFrom) != 0)
                    {
                        model.updateIntoTable("update FriendlyMessages set messageStatus=2 where forID = " + recievedFrom + " and messageStatus=1 and byID = " + recievedBy);
                        dt = model.selectIntoTable("select fid as ChatID, messageMime,DATE_FORMAT( messageDateTime,'%a %b %D %r') as messageDateTime,messageDateTime as lastModifiedDate, messageStatus, messageRefUrl, msgType,forID,byID, message " +
                             "from FriendlyMessages where messageStatus!=-1 and forID = " + recievedBy + " and byID = " + recievedFrom + dynamicWhereClause + " UNION " +
                             "select fid as ChatID, messageMime, DATE_FORMAT( messageDateTime,'%a %b %D %r') as messageDateTime,messageDateTime as lastModifiedDate, messageStatus, messageRefUrl, msgType,forID,byID, message from FriendlyMessages where messageStatus!=-1 and byID=" + recievedBy +
                          " and forID =" + recievedFrom + dynamicWhereClause + " order by lastModifiedDate desc Limit 50");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Messages", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream sendMessagetoFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string sendBy = null;
                    string message = null;
                    string messageType = null;
                    string sendTo = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("sendBy"))
                        {
                            sendBy = list.Value;
                        }
                        if (list.Key.Equals("message"))
                        {
                            message = list.Value;
                        }
                        if (list.Key.Equals("messageType"))
                        {
                            messageType = list.Value;
                        }
                        if (list.Key.Equals("sendTo"))
                        {
                            sendTo = list.Value;
                        }
                    }
                    if (Convert.ToInt32(sendBy) != 0)
                    {
                        string responseOfInsert = model.insert("insert into FriendlyMessages(messageMime, messageDateTime, messageStatus, messageRefUrl, msgType,forID,byID, message) " +
                             "values('application/text', now(), 1, ''," + messageType + ", " + sendBy + ", " + sendTo + ", '" + message + "' )");
                        if (responseOfInsert.Equals("true"))
                        {
                            response = new ResponseData(1, Constant.messageSaved, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }

                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream queueSingleNotification(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Queue queue = new Queue();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    int queueType = 0;
                    int queueRegarding = 0;
                    int queueRegardingRefId = -1;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("queueType"))
                        {
                            queueType = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("queueRegarding"))
                        {
                            queueRegarding = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("queueRegardingRefId"))
                        {
                            queueRegardingRefId = Convert.ToInt32(list.Value);
                        }

                    }
                    if (queueRegarding != 0 && queueRegardingRefId != -1)
                    {
                        if (queueRegarding == 1)
                        {
                            queue.queueJobNotification(queueRegardingRefId);
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream postCommentOnYourSocialWall(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        // string returnString = null;
        string userID = null;

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        if (keypair.Key == "userID")
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string postOnSocialNetwork = null;
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("postOnSocialNetwork"))
                        {
                            postOnSocialNetwork = list.Value;
                        }
                        if (list.Key.Equals("postOnSocialNetwork"))
                        {
                            postOnSocialNetwork = list.Value;
                        }
                        if (list.Key.Equals("postOnSocialNetwork"))
                        {
                            postOnSocialNetwork = list.Value;
                        }

                    }
                    string responselinkedin = linkedin.updateStatusOnOauthServer(null, userID);
                    if (responselinkedin.Equals("true"))
                    {
                        response = new ResponseData(1, "Successfully posted", null);
                    }
                    else if (responselinkedin.Equals("false"))
                    {
                        response = new ResponseData(1, "Please register with your linkedin account with us.", null);
                    }
                }
                // response = new ResponseData(0, "Not a Vallid User", null);

            }
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private string callTwilioFormessage(string message)
    {
        try
        {
            string param = "Body={'inputData':{'meta': {'token': '098084038403240434324324kjhkhfjhdskf','action':'actionName','validation': 'runthisvalidationonserver', 'previousaction': 'actionName' },'data':[{'contactNo':'8950706505','message':'hi parveen'},{'contactNo':'965834474871','message':'thanks'}]}}";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://use-this.appspot.com/");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] buffer = System.Text.Encoding.Unicode.GetBytes(param);
            Stream POSTstream = request.GetRequestStream();
            POSTstream.Write(buffer, 0, buffer.Length);
            HttpWebResponse POSTResponse = (HttpWebResponse)request.GetResponse();
            Stream responseStream = POSTResponse.GetResponseStream();
            byte[] responsebytes = GetBytes(responseStream);
            string response = System.Text.Encoding.UTF8.GetString(responsebytes);
        }
        catch (Exception ecx)
        {
            Console.WriteLine(ecx.Message);
        }
        return "";
    }
    private Boolean vallidateJson(string message)
    {
        try
        {
            JObject vallidateobject = JObject.Parse("{\"data\":\"" + message + "\"}");
            return true;
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
        }
        return false;
    }
    private string readQuestionFromCSV(int uid, string filePath, DataTable tableForCsv)
    {
        int count = 0;
        string TestCategory = "";
        string TestName = null;
        string TestLevel = null;
        string Question = null;
        string QuestionType = "";
        string QuestionAnswerType = null;
        string QuestionDifficulity = null;
        string SolutionExplanation = null;
        string QuestionOption1 = "";
        string QuestionOption2 = null;
        string QuestionOption3 = null;
        string QuestionOption4 = null;
        string QuestionMarks = null;
        string ParentQuestionId = null;
        string Company = null;
        string TestSubject = null;
        int userId = uid;
        //string tcid = null;
        StringBuilder jsonarray = new StringBuilder();
        StringBuilder insertStmt = new StringBuilder();
        StringBuilder values = new StringBuilder();
        jsonarray.Append("[{");

        var reader = new StreamReader(File.OpenRead(@filePath));
        List<string> listColumnName = new List<string>();
        List<string> listValues = new List<string>();
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnName in appendValues)
            {
                listColumnName.Add(columnName);
            }
            break;
        }
        //string dynamicExpiryDays = null;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnValues in appendValues)
            {
                listValues.Add(columnValues);
            }
            if (listValues.Count == 16 && listColumnName.Count == 16)   //constant value  is equal to the number of column in CSV.
            //if (listColumnName.Count == 14)
            {
                TestCategory = listValues[0];
                TestName = listValues[1];
                TestLevel = listValues[2];
                Question = listValues[3];
                QuestionType = listValues[4];
                QuestionAnswerType = listValues[5];
                QuestionDifficulity = listValues[6];
                SolutionExplanation = listValues[7];
                QuestionOption1 = listValues[8];
                QuestionOption2 = listValues[9];
                QuestionOption3 = listValues[10];
                QuestionOption4 = listValues[11];
                QuestionMarks = listValues[12];
                ParentQuestionId = listValues[13];
                Company = listValues[14];
                TestSubject = listValues[15];

                //hard coded values.
                string testInstruction = "testInstruction.html";
                int maxQnAllowed = 50;
                int maxTimeInSecond = 45;
                string questionmimeType = "AED-SFMT-IPAD-4B4-NX-NX-NX-00";
                string answerOptionMimeType = "H-CICICICIR-S1";
                int version = 1;
                int testStatus = 1;
                int tcIndex;
                int tsIndex;
                int tqIndex;
                int skillId;
                int cID;

                try
                {
                    TestCategory = TestCategory.Replace("\'", " ").Trim();
                    DataTable dt = model.selectIntoTable("select count(*)  from TMTestCategory where testCategoryName= '" + TestCategory + "'");
                    int datarow = int.Parse(dt.Rows[0][0].ToString());
                    if (datarow != 0)
                    {
                        dt = model.selectIntoTable("select tcid  from TMTestCategory where testCategoryName= '" + TestCategory + "'");
                        tcIndex = int.Parse(dt.Rows[0][0].ToString());
                    }
                    else
                    {
                        insertStmt.Append("insert into TMTestCategory (testCategoryName,categorylastModifiedON)");
                        values.Append("values('" + TestCategory + "',now())");
                        dt = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "tcid");
                        tcIndex = int.Parse(dt.Rows[0][0].ToString());
                        listValues.Clear();
                        insertStmt.Clear();
                        values.Clear();

                    }
                    TestName = TestName.Replace("\'", " ").Trim();
                    Company = Company.Replace("\'", " ").Trim();
                    TestSubject = TestSubject.Replace("\'", " ").Trim();
                    if (Company == "")
                    {
                        DataTable dt1 = model.selectIntoTable("select luvid from ListofValues where luvValueDisplayName = '" + TestCategory + "'");
                        skillId = int.Parse(dt1.Rows[0][0].ToString());
                        dt1 = model.selectIntoTable("select count(*)  from TMTests where testName= '" + TestName + "' AND testLevel = '" + TestLevel + "' AND associatedTestCategory = '" + tcIndex + "'AND skillId = '" + skillId + "'AND companyId is null");
                        int datarow1 = int.Parse(dt1.Rows[0][0].ToString());
                        if (datarow1 != 0)
                        {
                            dt1 = model.selectIntoTable("select tsid  from TMTests where testName= '" + TestName + "' AND testLevel = '" + TestLevel + "' AND associatedTestCategory = '" + tcIndex + "'AND skillId = '" + skillId + "'AND companyId is null");
                            tsIndex = int.Parse(dt1.Rows[0][0].ToString());
                        }
                        else
                        {
                            insertStmt.Append("insert into TMTests (testName,testSubject,maxQnAAllowed,version,testStatus,testLevel,associatedTestCategory,skillId,testDependencyIfAny,purchaseReceiptid,testInstructions,createdOn,lastModifiedon,testDurationInSeconds)");
                            values.Append("values('" + TestName + "','" + TestSubject + "','" + maxQnAllowed + "','" + version + "','" + testStatus + "','" + TestLevel + "','" + tcIndex + "','" + skillId + "',null,'" + userId + "','" + testInstruction + "',now(),now(),1800)");
                            dt1 = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "tsid");
                            tsIndex = int.Parse(dt1.Rows[0][0].ToString());
                            listValues.Clear();
                            insertStmt.Clear();
                            values.Clear();
                        }
                    }
                    else
                    {
                        DataTable dt1 = model.selectIntoTable("select cid from Company where companyName = '" + Company + "'");
                        string cidtest = dt1.Rows[0][0].ToString();
                        cID = int.Parse(dt1.Rows[0][0].ToString());
                        if (cidtest != null)
                        {
                            dt1 = model.selectIntoTable("select luvid from ListofValues where luvValueDisplayName = '" + TestCategory + "'");
                            skillId = int.Parse(dt1.Rows[0][0].ToString());
                            dt1 = model.selectIntoTable("select count(*)  from TMTests where testName= '" + TestName + "' AND testLevel = '" + TestLevel + "' AND associatedTestCategory = '" + tcIndex + "' AND companyId = '" + cID + "' AND skillId = '" + skillId + "'");
                            int datarow1 = int.Parse(dt1.Rows[0][0].ToString());
                            if (datarow1 != 0)
                            {
                                dt1 = model.selectIntoTable("select tsid  from TMTests where testName= '" + TestName + "' AND testLevel = '" + TestLevel + "' AND associatedTestCategory = '" + tcIndex + "'AND companyId = '" + cID + "' AND skillId = '" + skillId + "'");
                                tsIndex = int.Parse(dt1.Rows[0][0].ToString());
                            }
                            else
                            {
                                insertStmt.Append("insert into TMTests (testName,testSubject,maxQnAAllowed,version,testStatus,testLevel,associatedTestCategory,companyId,skillId,testDependencyIfAny,purchaseReceiptId,testInstructions,createdOn,lastModifiedOn,testDurationInSeconds)");
                                values.Append("values('" + TestName + "','" + TestSubject + "','" + maxQnAllowed + "','" + version + "','" + testStatus + "','" + TestLevel + "','" + tcIndex + "','" + cID + "','" + skillId + "',null,'" + userId + "','" + testInstruction + "',now(),now(),1800)");
                                dt1 = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "tsid");
                                tsIndex = int.Parse(dt1.Rows[0][0].ToString());
                                listValues.Clear();
                                insertStmt.Clear();
                                values.Clear();

                            }
                        }
                        else
                        {
                            DataRow dr = tableForCsv.Rows[count];
                            dr[tableForCsv.Columns.Count - 1] = "SKIP ! Company not exist.";
                            count++;
                            break;
                        }

                    }
                    Question = Question.Replace("\'", string.Empty).Trim();
                    DataTable dt2 = model.selectIntoTable("select count(*)  from TMQuestions where question= '" + Question + "' ");
                    int datarow2 = int.Parse(dt2.Rows[0][0].ToString());
                    if (datarow2 != 0)
                    {

                        dt2 = model.selectIntoTable("select qid from TMQuestions where question =  '" + Question + "' ");
                        tqIndex = int.Parse(dt2.Rows[0][0].ToString());

                    }
                    else
                    {

                        insertStmt.Append("insert into TMQuestions (question,questionDifficulty,questionMimeType,createdOn,lastModifiedOn,maxTimeInSeconds)");
                        values.Append("values('" + Question + "','" + QuestionDifficulity + "','" + questionmimeType + "',now(),now(),'" + maxTimeInSecond + "')");

                        dt2 = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "qid");
                        tqIndex = int.Parse(dt2.Rows[0][0].ToString());
                        listValues.Clear();
                        insertStmt.Clear();
                        values.Clear();

                        QuestionOption1 = QuestionOption1.Replace("\'", " ").Trim();
                        QuestionOption2 = QuestionOption2.Replace("\'", " ").Trim();
                        QuestionOption3 = QuestionOption3.Replace("\'", " ").Trim();
                        QuestionOption4 = QuestionOption4.Replace("\'", " ").Trim();
                        string questionOption = "";
                        for (int i = 1; i < 5; i++)
                        {

                            if (i == 1)
                            {
                                questionOption = QuestionOption1;
                            }
                            else if (i == 2)
                            {
                                questionOption = QuestionOption2;
                            }
                            else if (i == 3)
                            {
                                questionOption = QuestionOption3;
                            }
                            else
                            {
                                questionOption = QuestionOption4;
                            }
                            int solutionFlag = 0;
                            if (i == Convert.ToInt32(SolutionExplanation))
                            {
                                solutionFlag = 1;
                            }
                            insertStmt.Append("insert into TMAnswersOptions (answerOptionText,associatedQuestionID,isCorrectAnswer,answerOptionType,answerOptionmimeType )");
                            values.Append("values('" + questionOption + "', '" + tqIndex + "','" + solutionFlag + "','" + QuestionAnswerType + "','" + answerOptionMimeType + "')");
                            dt2 = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "aoid");
                            // tqIndex = int.Parse(dt4.Rows[0][0].ToString());
                            listValues.Clear();
                            insertStmt.Clear();
                            values.Clear();

                        }
                    }
                    // check for if qid is already inserted in table and fetch the max question sequence from table.
                    DataTable dt3 = model.selectIntoTable("select count(*) from TMTestQuestions where questionID= '" + tqIndex + "'AND testId = '" + tsIndex + "' ");
                    int datarow3 = int.Parse(dt3.Rows[0][0].ToString());
                    dt3.Clear();
                    dt3 = model.selectIntoTable("select max(questionSequenceNo) from TMTestQuestions where testid= '" + tsIndex + "' ");
                    string seqcount = dt3.Rows[0][0].ToString();
                    //int scount = Convert.ToInt32(dt3.Rows[0][0]);
                    int scount;
                    if (seqcount == "")
                    {

                        scount = 1;
                    }
                    else
                    {
                        scount = Convert.ToInt32(dt3.Rows[0][0]);
                        scount = scount + 10;
                    }

                    if (datarow3 != 0)
                    {
                        DataRow dr = tableForCsv.Rows[count];
                        dr[tableForCsv.Columns.Count - 1] = "SKIP ! Question  already Inserted.";
                        count++;
                        //dt3 = model.selectIntoTable("select questionID  from TMTestQuestions where testID= '" + tsIndex + "' ");
                        //tsIndex = int.Parse(dt3.Rows[0][0].ToString());
                    }
                    else
                    {
                        insertStmt.Append("insert into TMTestQuestions (testID,questionID,questionSequenceNo)");
                        values.Append("values('" + tsIndex + "', '" + tqIndex + "','" + scount + "')");
                        dt3 = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "qid");
                        //tqIndex = int.Parse(dt3.Rows[0][0].ToString());
                        listValues.Clear();
                        insertStmt.Clear();
                        values.Clear();
                        DataRow dr = tableForCsv.Rows[count];
                        dr[tableForCsv.Columns.Count - 1] = "SUCCESS ! Question Inserted Successfully.";
                        count++;

                    }
                    listValues.Clear();
                    insertStmt.Clear();
                    values.Clear();
                    jsonarray.Append("\"" + TestCategory + "\":\"Successfull,tcid = " + dt.Rows[0][0].ToString() + "\"$");



                }
                catch (Exception exc)
                {
                    listValues.Clear();
                    insertStmt.Clear();
                    values.Clear();
                    DataRow dr = tableForCsv.Rows[count];
                    dr[tableForCsv.Columns.Count - 1] = "Fail !" + exc.Message;
                    count++;
                    jsonarray.Append("\"" + TestCategory + "\":\"Fail ! " + exc.Message + "\"$");
                }


            }
            else
            {
                listValues.Clear();
                insertStmt.Clear();
                values.Clear();
                DataRow dr = tableForCsv.Rows[count];
                dr[tableForCsv.Columns.Count - 1] = "Fail ! Exceed columns value,please ensure multiple values sepratedBy CSV";
                count++;
                jsonarray.Append("\"" + TestCategory + "\":\"Exceed columns value,please ensure multiple values sepratedBy CSV \"$");
            }

        }
        GenrateExcel excel = new GenrateExcel();
        excel.WriteToCsvFile(tableForCsv, "questionUpload", "csvFilesForJobProcessed");
        tableForCsv.Clear();
        return vallidJsonArray(jsonarray);
    }
    public Stream likeCount(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;
                    string jobComments = null;
                    string expiryDays = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }
                        if (list.Key.Equals("jobComments"))
                        {
                            jobComments = list.Value;
                        }
                        if (list.Key.Equals("expiryDays"))
                        {
                            expiryDays = list.Value;
                        }
                    }
                    if (userID != null && jobID != null)
                    {
                        int rows = model.updateIntoTablewithRowCount("update Job set jobComments='" + jobComments + "',jobExpiresOn="
                            + "DATE_ADD(now(),INTERVAL " + Convert.ToInt32(expiryDays) + " DAY) where jid=" + jobID + " and postedBy=" + userID);
                        if (rows != 0)
                        {
                            response = new ResponseData(1, "Your comments for job has been updated", null);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "You have no job for comments", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }

    }
    public Stream forgotPassword(string inputData)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        string returnString = null;
        int uid = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {

                }
                if (objelement.Key.Equals("data"))
                {

                    list = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            string username = null;
            foreach (var keyvalue in list)
            {
                if (keyvalue.Key.Equals("userName"))
                {
                    username = keyvalue.Value;
                }
            }
            if (username != null)
            {
                dt = model.selectIntoTable("select uid userPassword from Users where userName='" + username + "' and userType=1");
                if (dt.Rows.Count != 0)
                {
                    uid = Convert.ToInt32(dt.Rows[0][0]);
                    dt = model.selectIntoTable("select *  from RefereredUsers where regUserId=1 and refUserId=" + uid);
                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(0, "unSubscribe User", "[{\"uid\":\"" + uid + "\"");
                        return returnStmnt(response.returnStatment());

                    }
                    int qid = queue.queueForgotMail(uid);
                    response = new ResponseData(1, "Process is in queue ", "[{\"uid\":\"" + uid + "\",\"qid\":\"" + qid + "\"}]");
                    return returnStmnt(response.returnStatment());

                }
                else
                {
                    response = new ResponseData(0, "invallid userName", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input data is not correct", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            returnString = response.returnStatment();
            return returnStmnt(returnString);
        }
    }
    public Stream updateCommentOnJob(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "postResumeToHelpingFriends", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string jobComments = null;
                    string expiryDays = null;
                    string canDisplayJobByName = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("jobComments"))
                        {
                            jobComments = list.Value;
                        }
                        if (list.Key.Equals("expiryDays"))
                        {
                            expiryDays = list.Value;
                        }
                        if (list.Key.Equals("canDisplayJobByName"))
                        {
                            canDisplayJobByName = list.Value;
                        }
                    }
                    if (userID != 0 && jobID != null && canDisplayJobByName != null)
                    {
                        int rows = model.updateIntoTablewithRowCount("update Job set canDisplayPostedByName='" + canDisplayJobByName + "',jobComments='" + jobComments + "',jobExpiresOn="
                            + "DATE_ADD(now(),INTERVAL " + Convert.ToInt32(expiryDays) + " DAY) where jid=" + jobID + " and postedBy=" + userID);
                        if (rows != 0)
                        {
                            dt = model.selectIntoTable("select jid from Job where jid= " + jobID + " and postedBy = " + userID);
                            if (dt.Rows.Count != 0)
                            {
                                if (canDisplayJobByName != null)
                                {
                                }
                                response = new ResponseData(1, "Your comments for job has been updated", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else
                        {
                            response = new ResponseData(1, "You have no job for comments", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }

    }
    public Stream getJobApplications(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int jobID = 0;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "postResumeToHelpingFriends", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("jobID"))
                    {
                        jobID = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select distinct(jid) , appliedByUser,userName, userContactNo, displayName, DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle, DATE_FORMAT(appliedOnDateTime,'%d-%b-%Y') as appliedOnDateTime," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn, "
                            + "jobVertical,currentJobTitle as jobRole from Users U,  JobApplications As A LEFT JOIN Job As J  ON J.jid = A.jobRefId Where A.appliedByUser = U.uid and A.jobRefId = " + jobID + " order by appliedOnDateTime desc");
                        string result = convertDataTableToJson(dt
                            );
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream revokeJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "postResumeToHelpingFriends", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(list.Value);
                        }

                    }
                    if (userID != 0 && jobID != null)
                    {
                        int countJobRowUpdate = model.updateIntoTablewithRowCount("update Job set jobStatus=-2 where jid=" + jobID + " and postedBy=" + userID);
                        if (countJobRowUpdate > 0)
                        {
                            dt = model.selectIntoTable("select jid from Job where jid=" + jobID + " and postedBy=" + userID);
                            response = new ResponseData(1, "Job Revoked", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Could not revoke job", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream revokeJobApplication(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }

                    }
                    if (userID != null && jobID != null)
                    {
                        int countJobRowUpdate = model.updateIntoTablewithRowCount("update JobApplications set applicationStatus=4 where jobRefid=" + jobID + " and appliedByUser=" + userID);
                        if (countJobRowUpdate > 0)
                        {
                            dt = model.selectIntoTable("select jaid from JobApplications where jobRefid=" + jobID + " and appliedByUser=" + userID);
                            response = new ResponseData(1, "Job application Revoked", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Could not revoke job application", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream registerForServerNotification(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "post new Job", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string serverNotificationToken = null;

                    string deviceID = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("serverNotificationToken"))
                        {
                            serverNotificationToken = list.Value;
                        }
                        if (list.Key.Equals("deviceID"))
                        {
                            deviceID = list.Value;
                        }

                    }
                    if (userID != 0 && deviceID != null && serverNotificationToken != null)
                    {
                        int RowUpdate = model.updateIntoTablewithRowCount("update AuthenticationTokens where socialID ='" + deviceID + "',token='" + serverNotificationToken + "' where regUserId=" + userID);
                        if (RowUpdate != 0)
                        {
                            dt = model.insertIntoTableWithLastId("insert into AuthenticationTokens(regUserId,token,lastModifiedDate,createdDateTime) values"
                                + userID + ",'" + serverNotificationToken + "',now(),now()", "auid");
                            if (dt.Rows.Count != 0)
                            {
                                response = new ResponseData(1, "Server notification token successfully saves", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }

                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream introduceFriends(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        if (keypair.Key.Equals("userID"))
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != null)
                {
                    insertedIntoFriendRequests("{\"data\":" + objelement.Value.ToString() + "}", userID);
                    response = new ResponseData(1, "success", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertedIntoFriendRequests(string jsonarr, string uid)
    {
        DataTable dt = new DataTable();
        string requestType = null;
        string forFriendID = null;
        string toID = null;
        StringBuilder insertStmt = new StringBuilder();
        JObject root = JObject.Parse(jsonarr);
        JArray items = (JArray)root["data"];
        foreach (var data in items)
        {
            JObject localdata = JObject.Parse(data.ToString());
            foreach (var keyvalues in localdata)
            {
                if (keyvalues.Key.Equals("requestType"))
                {
                    requestType = keyvalues.Value.ToString();
                } if (keyvalues.Key.Equals("forFriendID"))
                {
                    forFriendID = keyvalues.Value.ToString();
                } if (keyvalues.Key.Equals("toID"))
                {
                    toID = keyvalues.Value.ToString();
                }
            }
            if (forFriendID != toID)
            {
                table = model.selectIntoTable("select * from RefereredUsers where regUserId=" + toID + " and refUserId=" + forFriendID);
                if (table.Rows.Count == 0)
                {
                    table = model.selectIntoTable("select * from FriendRequests where forFriendID=" + forFriendID + " and toID =" + toID);
                    if (table.Rows.Count == 0)
                    {
                        insertStmt.Append("insert into FriendRequests (requestStatus,introducedOn,introducedBy,requestType,forFriendID,toID) values (0,now(),"
                            + uid + "," + requestType + ","
                            + forFriendID + "," + toID + ")");
                        model.insertIntoTable(insertStmt.ToString());

                    }
                    insertStmt.Clear();
                }
            }
        }
        return true;
    }
    public Stream getPendingRequests(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key == "userID")
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string introducedBy = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("introducedBy"))
                        {
                            introducedBy = list.Value;
                        }
                    }
                    if (userID != null && introducedBy != null)
                    {
                        dt = model.selectIntoTable("select distinct(F.frid),U.uid,U.displayName,U.currentJobTitle," +
                            " I.displayName as introduceBy from Users as U, Users as I ,FriendRequests as F where U.uid=F.forFriendID and " +
                       "RequestStatus=0 and  F.introducedBy=I.uid and toID=" + userID + " and F.introducedBy=" + introducedBy);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream introductionUpdate(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key.Equals("userID"))
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != null)
                {
                    insertedIntroductionUpdate("{\"data\":" + objelement.Value.ToString() + "}", userID);
                    response = new ResponseData(1, "success", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertedIntroductionUpdate(string jsonarr, string toID)
    {
        DataTable dt = new DataTable();
        // DataRow row;
        string forFriendID = null;
        string updateStatus = null;
        StringBuilder insertStmt = new StringBuilder();
        JObject root = JObject.Parse(jsonarr);
        JArray items = (JArray)root["data"];
        foreach (var jsonkeyvalue in items)
        {
            JObject localdata = JObject.Parse(jsonkeyvalue.ToString());
            foreach (var list in localdata)
            {                          // sendBy,sendTo,Message,messageType
                if (list.Key.Equals("forFriendID"))
                {
                    forFriendID = list.Value.ToString();
                }
                if (list.Key.Equals("updateStatus"))
                {
                    updateStatus = list.Value.ToString();
                }
                if (toID != null && forFriendID != null && updateStatus != null)
                {
                    int status = Convert.ToInt32(updateStatus);
                    dt = model.selectIntoTable("select * from RefereredUsers where regUserId=" + toID + " and refUserId=" + forFriendID);
                    if (dt.Rows.Count == 0)
                    {
                        if (status == 1)
                        {
                            dt = model.insertIntoTableWithLastId("insert into RefereredUsers (regUserId,refUserId,createdDateTime) values(" + toID + "," + forFriendID + ",now())", "rid");
                            if (dt.Rows.Count != 0)
                            {
                                model.updateIntoTable("update FriendRequests set requestStatus=1 where forFriendID=" + forFriendID + " and toID=" + toID);

                                //table.Rows.Add(row);
                            }
                        }
                        else if (status == -1)
                        {
                            model.updateIntoTable("update FriendRequests set requestStatus=-1 where forFriendID=" + forFriendID + " and toID=" + toID);
                        }
                    }
                }
            }


        }
        return true;
    }
    public string keyValuePairToJson(List<KeyValuePair<string, string>> list)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var KeyValuePair in list)
        {
            //sb.Append("[{"+list.+"}");
        }
        return sb.ToString();
    }
    public Stream updateJobApplicationComments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            //flag=true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;

                    string applicationNotes = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("applicationNotes"))
                        {
                            applicationNotes = list.Value;
                        }
                    }
                    if (userID != null && jobID != null && applicationNotes != null)
                    {
                        int rowUpdates = model.updateIntoTablewithRowCount("update JobApplications set applicationNotes='" + applicationNotes + "' where jobRefid=" + jobID
                            + " and appliedByUser=" + userID);
                        dt = model.selectIntoTable("select jaid from JobApplications where  jobRefid=" + jobID
                            + " and appliedByUser=" + userID);

                        if (rowUpdates != 0 && dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Application notes successfully updated", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Application notes not updated", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }

    }
    public Stream getJobApplicationComments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;

                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }

                    }
                    if (jobID != null)
                    {
                        dt = model.selectIntoTable("select U.currentJobTitle , applicationNotes, appliedByUser,DATE_FORMAT(appliedOnDateTime,'%d-%b-%Y') as appliedOnDateTime from JobApplications,Users as U where appliedByUser=U.uid and jobRefid= " + jobID);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "application comments ", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "you have no any application comments for this job", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not completed", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }
    }
    public Stream getJobDetail(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key == "userID")
                        {
                            userID = Convert.ToInt32(keypair.Value);
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }
                    if (jobID != null)
                    {
                        dt = model.selectIntoTable("select distinct(jid),(case When canDisplayPostedByName = 1 then  U.displayName else 'Guess IT' end ) as postedByName,jobMinSalary,jobMaxSalary,jobSalaryCurrency,canDisplayPostedByName,DateDiff(J.jobExpiresOn,now()) as expireinDays,count(JA.jaid) as jobApplications,DATE_FORMAT(JA2.appliedOnDateTime,'%d-%b-%Y') as jobAppliedOnDate,(JA2.appliedByUser IS NOT NULL) as isJobApplied" +
                        ",postedBy,DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                        "jobTitle,(case When jobReferenceLink = 'null' then 'No url' else jobReferenceLink end ) as jobReferenceLink,joblatitude,jobLongitude,(case When jobLocationName = 'null' then 'No location'  else jobLocationName end ) as jobLocationName ,jobType,jobComments," +
                        "jobSkills,DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') " +
                        "as jobExpiresOn ,DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,SUM(JA.applicationNotes IS NOT NULL) as commentsCount," +
                         "jobExperienceMinYears,jobExperienceMaxYears,jobVertical,jobRole from " +
                     " Users as U, Job As J Left Join JobApplications As JA on " +
                        "JA.jobRefid=J.jid Left join JobApplications As JA2 on JA2.appliedByUser=" + userID + " and " +
                         " JA2.jobRefid=J.jid " +
                            " where U.uid=J.postedBy and J.jid =" + jobID +
                          " and jobStatus = 1 and jobExpiresOn > now()  " +
                          " group by jid " +
                           " order by postedOnDateTime desc ");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getFriendDetail(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key == "userID")
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string friendID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("friendID"))
                        {
                            friendID = list.Value;
                        }
                    }
                    if (friendID != null)
                    {
                        dt = model.selectIntoTable("SELECT distinct(uid),count(toID) as pendingRequests,displayName,userName,userContactNo," +
                                "refUserPrivacyConcern as isBlocked ,userType,userStatus,currentCompanyName,currentJobTitle,currentIndustry FROM " +
                              " Users A,RefereredUsers B  Left JOIN   FriendRequests As F ON F.RequestStatus=0" +
                                " and F.introducedBy=B.refUserId and F.toID=B.regUserId WHERE A.uid = B.refUserId AND B.regUserId =" + userID + " and B.RefUserId=" +
                                friendID + "  group by uid order by displayName");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }


    }
    public Stream getTokenByUserID(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "userID")
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data"))
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string password = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("userPassword"))
                        {
                            password = list.Value;
                        }
                    }
                    if (userID != null && password != null)
                    {
                        password = EncryptBase64.EncodeTo64(password);
                        dt = model.selectIntoTable("select A.token, U.uid,U.displayName as uid from AuthenticationTokens as A,Users as U where A.regUserId=U.uid  and A.regUserId=" + userID +
                     " and U.userPassword='" + password + "' and A.authSite='takemyjob'");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "token generated", "[{\"token\":\"" + dt.Rows[0][0].ToString() + "\",\"uid\":\"" + dt.Rows[0][1].ToString() + "\",\"displayName\":\"" + dt.Rows[0][2].ToString() + "\" }]");
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(0, "password incorrect", "[{\"uid\":\"" + userID + "\" }]");
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "userID/password missing", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    //public Stream getFriendsResume(string inputData)
    //{
    //    List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
    //    string tempFilePath = null;
    //    try
    //    {
    //        DataTable dt = new DataTable();
    //        JObject jsonrow = JObject.Parse(inputData);
    //        Boolean flag = false;
    //        string friendID = null;
    //        string userID = null;
    //        string jobID = null;
    //        foreach (var objelement in jsonrow)
    //        {
    //            if (objelement.Key.Equals("meta"))
    //            {
    //                jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
    //                foreach (var keypair in jsonkeyvalue)
    //                {
    //                    if (keypair.Key == "token")
    //                    {
    //                        flag = true;
    //                        if (flag != true)
    //                        {
    //                            response = new ResponseData(0, Constant.invallidtoken, null);
    //                            return returnStmnt(response.returnStatment());
    //                        }
    //                    }
    //                    else if (keypair.Key == "userID")
    //                    {
    //                        userID = keypair.Value;
    //                    }
    //                }
    //            }
    //            if (objelement.Key.Equals("data") && flag == true)
    //            {
    //                jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
    //                foreach (var list in jsonkeyvalue)
    //                {
    //                    if (list.Key.Equals("friendID"))
    //                    {
    //                        friendID = list.Value;
    //                    }
    //                    if (list.Key.Equals("jobID"))
    //                    {
    //                        jobID = list.Value;
    //                    }
    //                }
    //                if (friendID != null && userID != null && jobID!=null  )
    //                {
    //                    dt = model.selectIntoTable("select jaid from JobAppLication where jobRefid =" + jobID + " and appliedByUser=" + friendID);
    //                    if (dt.Rows.Count != 0)
    //                    {
    //                        dt = model.selectIntoTable("select distinct(U.uid), U.displayName ,R.refDocumentPath from Users as U , Resume as R " +
    //                            "where refUserId=uid and uid=" + friendID);
    //                        List<string> listOfFilePath = new List<string>();
    //                        if (dt.Rows.Count != 0)
    //                        {
    //                            string refdocumentPath = dt.Rows[0][1].ToString();
    //                            if (refdocumentPath != "")
    //                            {
    //                                listOfFilePath.Add(refdocumentPath);
    //                                tempFilePath = Path.Combine(HttpRuntime.AppDomainAppPath, "content\\" + dt.Rows[0][0].ToString() + dt.Rows[0][0].ToString() + ".Zip");
    //                                ZipHelper.ZipFiles(tempFilePath, listOfFilePath);
    //                                string zipPath= zipFile.zipResumeFromArrOfPath(listOfFilePath,Convert.ToInt32(userID));
    //                                int qid = queue.insertdetailForPostResumes(Convert.ToInt32(userID), "Resumes of friends those ap);
    //                                queue.processTo(qid, tempFilePath);
    //                                response = new ResponseData(1, "Resumes sent", "[{\"qid\":\"" + qid + "\"}]");
    //                                return returnStmnt(response.returnStatment());
    //                            }
    //                            else
    //                            {
    //                                response = new ResponseData(1, "Friend have no any resume", null);
    //                                return returnStmnt(response.returnStatment());
    //                            }
    //                        }
    //                        else
    //                        {
    //                            response = new ResponseData(1, "userID not vallid", null);
    //                            return returnStmnt(response.returnStatment());
    //                        }
    //                    }
    //                    else {
    //                        response = new ResponseData(1, "You can't see resume of this ID", null);
    //                        return returnStmnt(response.returnStatment());
    //                    }
    //                }

    //            }
    //        }
    //        response = new ResponseData(0, "input parameters are not completed ", null);
    //        return returnStmnt(response.returnStatment());
    //    }
    //    catch (Exception ex)
    //    {
    //        string exception = ex.Message.Replace('\'', ' ');
    //        response = new ResponseData(0, exception, null);
    //        return returnStmnt(response.returnStatment());
    //    }
    //}
    public Stream getJobTrackingDetail(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int jobID = 0;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getJobTrackingDetail", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("jobID") && userID != 0)
                    {
                        jobID = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select jid, count(appliedbyUser) as jobApplications,(jobExpiresOn>now()) as isJobExpired,  DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle,jobReferenceLink,joblatitude,jobLongitude,jobLocationName,jobType,jobComments," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn , DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,"
                            + "jobVertical,jobRole from Job As J LEFT JOIN   JobApplications As A ON J.jid = A.jobRefId Where J.jid= " + jobID + " and J.postedBy = '" + userID + "' group by jid ");
                        string result = convertDataTableToJson(dt);
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream deleteFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "delete Friend", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string friendID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("friendID"))
                        {
                            friendID = list.Value;
                        }
                    }
                    if (userID != 0 && friendID != null)
                    {
                        model.deleteFromTable("delete from RefereredUsers where regUserId=" + userID + " and refUserId=" + friendID);
                        response = new ResponseData(1, "friend Removed", "[{\"regUserId\":\"" + userID + "\",\"refUserId\":\"" + friendID + "\" }]");
                        return returnStmnt(response.returnStatment());

                    }
                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this friend is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream clearChat(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "clear Chat", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string forFriendID = null;
                    string chatID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("forFriendID"))
                        {
                            forFriendID = list.Value;
                        }
                        if (list.Key.Equals("chatID"))
                        {
                            chatID = list.Value;
                        }
                    }

                    if (userID != 0 && forFriendID != "")
                    {
                        model.updateIntoTable("update FriendlyMessages set messageStatus=-1 where byID=" + forFriendID + " and forID=" + userID);
                        response = new ResponseData(1, "friend Removed", "[{\"byID\":\"" + userID + "\",\"forID\":\"" + forFriendID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }
                    else if (chatID != "")
                    {
                        model.deleteFromTable("update FriendlyMessages set messageStatus=-1 where fid=" + chatID);
                        response = new ResponseData(1, "chat cleaned", "[{\"fid\":\"" + chatID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this messages is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream changePassword(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "change Password", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string password = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("password"))
                        {
                            password = list.Value;
                        }
                    }

                    if (userID != 0 && password != null && password != "")
                    {
                        model.insertIntoTable("update Users set userPassword='" + EncryptBase64.EncodeTo64(password) + "' where uid=" + userID);

                        response = new ResponseData(1, "Password changed", "[{\"uid\":\"" + userID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }

                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream subscribe(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "change Password", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string password = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("password"))
                        {
                            password = list.Value;
                        }
                    }

                    if (userID != 0 && password != null && password != "")
                    {
                        model.insertIntoTable("update Users set userPassword='" + EncryptBase64.EncodeTo64(password) + "' where uid=" + userID);
                        response = new ResponseData(1, "Password changed", "[{\"uid\":\"" + userID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }

                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this messages is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream unsubscribe(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int userID = 0;
        string token = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            token = keypair.Value;
                            dt = model.selectIntoTable("select regUserId from AuthenticationTokens where token='" + token + "'");
                            if (dt.Rows.Count != 0)
                            {
                                userID = Convert.ToInt32(dt.Rows[0][0]);
                                auditInfo(inputData, 1, "unSubscribe", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data"))
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    if (userID != 0)
                    {
                        model.insertIntoTable("insert into RefereredUsers  (regUserId,refUserId,createdDateTime) values (1," + userID + ",now())");
                        response = new ResponseData(1, "User has been unSubscribed", "[{\"uid\":\"" + userID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }

                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "token not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getUsageStats(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string role = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getUsageStats", userID);
                            }
                        }
                        else if (keypair.Key.Equals("role"))
                        {
                            role = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    if (role.Equals("admin"))
                    {
                        dt = model.selectIntoTable(" select (select  count(*)  from Users) as totalUsers,(select count(*) from Users where userType=1 )as regUserCount, " +
"(select  count(*)  from AuthenticationTokens where lastModifiedDate>DATE_ADD(now(),INTERVAL -1 DAY) and authSite='takemyjob') as userLoggedInToday , " +
"(select  count(*)  from Job) as totaljobPosted,(select  count(*)  from Job where jobType=1) as totaljobsFromResignations, " +
"(select  count(*)  from Job " +
" where jobStatus=1 and jobExpiresOn>now() ) as countOfActiveJobsToday, " +
"(select  count(*)  from Job where postedOnDateTime>DATE_ADD(now(),INTERVAL -1 DAY)) as jobPostedToday, " +
"(select  count(*)  from JobApplications where appliedOnDateTime>DATE_ADD(now(),INTERVAL -1 DAY)) as applicationsToday " +
",(select  count(*)  from JobApplications) as totalApplication ");
                        response = new ResponseData(1, "Results", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream processQueue(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string role = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "processQueue", userID);
                            }
                        }
                        else if (keypair.Key.Equals("role"))
                        {
                            role = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    if (role.Equals("admin"))
                    {
                        dt = model.selectIntoTable(" select qid from Queue where deliveryStatus=0 ");
                        if (dt.Rows.Count > 0)
                        {
                            for (int index = 0; index < dt.Rows.Count; index++)
                            {
                                queue.processTo(Convert.ToInt32(dt.Rows[index][0]));
                            }
                        }
                        response = new ResponseData(1, "Results", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getSuggestedVideos(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string emailaddress = "takemyjob@csimplifyit.com";
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);

            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                            emailaddress = dt.Rows[0][0].ToString();
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string itemType = null;
                    string refrenceRefId = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("forValue"))
                        {
                            refrenceRefId = list.Value;
                        }
                        if (list.Key.Equals("itemType"))
                        {
                            itemType = list.Value;
                        }
                    }
                    dt = dmTakeMyJob.selectIntoTable("Select luvValueDisplayName from ListofValues where luvid=" + refrenceRefId);
                    string skillName = dt.Rows[0][0].ToString();
                    string dynamicWhereClause = (refrenceRefId == "") ? "" : " and RefrenceRefId = " + refrenceRefId + "";
                    string dynamicQuery = (itemType == "c" || itemType == "C") ? " select rmID,refrenceType,RefrenceRefID,rmUrl ,searchTag,rmStatus,rmSubject ,viewCount,author ,urlFilterRefID from RefrenceSuggetionMaterials where rmStatus=2  " + dynamicWhereClause + " and refrenceType='c' union "
+ "select rmID,refrenceType,RefrenceRefID,rmUrl,searchTag,rmStatus,rmSubject,viewCount,author,urlFilterRefID from RefrenceSuggetionMaterials where rmStatus=2 " + dynamicWhereClause + " and refrenceType='CIQ' union " +
"select rmID,refrenceType,RefrenceRefID,rmUrl,searchTag,rmStatus,rmSubject,viewCount,author,urlFilterRefID from RefrenceSuggetionMaterials where rmStatus=2  " + dynamicWhereClause + " and refrenceType='r'  order By viewCount desc  limit 50 "
 : " select rmID,refrenceType,RefrenceRefID,rmUrl,searchTag,rmStatus,rmSubject,viewCount,author,urlFilterRefID from RefrenceSuggetionMaterials where rmStatus=2 and ( refrenceType='s' or refrenceType='IQ' ) " + dynamicWhereClause + " order By refrenceType asc   limit 50 ";
                    if (userID != 0 && itemType != null && itemType != "")
                    {
                        dt = dmTakeMyJob.selectIntoTable(dynamicQuery);
                        if (dt.Rows.Count < 20 && itemType == "S")
                        {
                            Task.Factory.StartNew(() =>
                            {
                                Queue qw = new Queue();
                                qw.queueForNewSkill(Convert.ToInt32(refrenceRefId));
                            });
                        }
                        else if (dt.Rows.Count < 20 && (itemType == "c" || itemType == "C"))
                        {
                            Task.Factory.StartNew(() =>
                            {
                                SearchEngine se = new SearchEngine();
                                se.searchIQForCompany(Convert.ToInt32(refrenceRefId));
                            });
                        }
                        Task.Factory.StartNew(() =>
                        {
                            Queue qw = new Queue();
                            qw.queueForNewSkill(Convert.ToInt32(refrenceRefId));
                        });

                        ConvertIntoHTML CH = new ConvertIntoHTML();
                        List<string> listForColumn = new List<string>();
                       
                        listForColumn.Add("rmUrl");
                        listForColumn.Add("rmSubject");
                        listForColumn.Add("author");
                        string Html = CH.convertDataTableToHTMLTable(dt, listForColumn, false);
                        Task.Factory.StartNew(() =>
                        {
                            EmailSend se = new EmailSend();
                            se.sendMailWithTemplate(emailaddress, Html, skillName);
                        });
                        response = new ResponseData(1, "Refrence material ", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }

                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream attachVideos(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string rmID = null;
                    string rmStatus = null;
                    string complexityLevel = "0";
                    string relvance = "1";
                    string supportedDivice = "50";
                    //int refrenceI
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("rmID"))
                        {
                            rmID = list.Value;
                        }
                        if (list.Key.Equals("rmStatus"))
                        {
                            rmStatus = list.Value;
                        }
                        if (list.Key.Equals("complexityLevel"))
                        {
                            complexityLevel = list.Value;
                        }
                        if (list.Key.Equals("supportedDevice"))
                        {
                            supportedDivice = list.Value;
                        }

                        if (list.Key.Equals("relvance"))
                        {
                            relvance = list.Value;
                        }

                    }
                    // ADD PARAMETERS IN TABLES AND SERVICE  COMPLEXIBLITY Level(0),SUPPORTED DIVICE(1),RELVANCE (50)
                    if (userID != 0 && rmID != null && rmID != "")
                    {
                        dmTakeMyJob.updateIntoTablewithRowCount("update RefrenceSuggetionMaterials set rmStatus=" + rmStatus + " where rmID=" + rmID);
                        if (Convert.ToInt32(rmStatus) == 1)
                        {
                            dt = model.insertIntoTableWithLastId("insert into RefrenceMaterials (refrenceType,refrenceRefID,rmUrl,searchTag,rmAddedOn,rmStatus,rmViewCount" +
                              ",rmSubject,rmMimeType,removeOn,removalFilter,viewCount,author,complexityLevel,supportedDivice,relvance)  select refrenceType,refrenceRefID,rmUrl,searchTag,rmAddedOn,rmStatus,rmViewCount" +
                              ",rmSubject,rmMimeType,removeOn,removalFilter,viewCount,author," + complexityLevel + "," + supportedDivice + "," + relvance + " from RefrenceSuggetionMaterials where rmID=" + rmID, "rmId");
                        }
                        dt = dmTakeMyJob.selectIntoTable("select refrenceType,refrenceRefID as RefrenceRefID from RefrenceSuggetionMaterials where rmID=" + rmID);
                        Queue qw = new Queue();
                        if (dt.Rows.Count != 0)
                        {
                            if (dt.Rows[0][0].ToString().Equals("s"))
                            {
                                qw.queueForNewSkill(Convert.ToInt32(dt.Rows[0][1]));
                            }
                            if (dt.Rows[0][0].ToString().Equals("IQ"))
                            {
                                qw.queueForNewSkill(Convert.ToInt32(dt.Rows[0][1]));
                            }
                            //if (dt.Rows[0][0].ToString().Equals("c"))
                            //{
                            //    qw.queueForNewSkill(Convert.ToInt32(dt.Rows[0][1]));
                            //}
                            //if (dt.Rows[0][0].ToString().Equals("CSIQ"))
                            //{
                            //    qw.queueForNewSkill(Convert.ToInt32(dt.Rows[0][1]));
                            //}
                            //if (dt.Rows[0][0].ToString().Equals("r"))
                            //{
                            //    qw.queueForNewSkill(Convert.ToInt32(dt.Rows[0][1]));
                            //}
                        }
                        response = new ResponseData(1, "Refrence material ", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }

                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream duplicateJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string requestedBy = null;
                    string jobID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("requestedBy"))
                        {
                            requestedBy = list.Value;
                        }
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }

                    if (userID != 0 && jobID != null && requestedBy != null)
                    {
                        List<KeyValuePair<string, string>> parametersOfSp = new List<KeyValuePair<string, string>>();
                        parametersOfSp.Add(new KeyValuePair<string, string>("jobID", jobID));
                        parametersOfSp.Add(new KeyValuePair<string, string>("requestedBy", requestedBy));
                        parametersOfSp.Add(new KeyValuePair<string, string>("lastJobID", "lastJobID"));
                        dt = model.selectIntoTableUsingSP("dubplicateJob", parametersOfSp);
                        response = new ResponseData(1, "Refrence material ", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getReferenceMaterialForJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true; // vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string jobID = null;
                    int approveCount;
                    int pendingApproveCount;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }

                    if (userID != 0 && jobID != null)
                    {
                        string skills = "";
                        dt = dmTakeMyJob.selectIntoTable("select distinct(l.luvValueDisplayName) from ListofValues as l, "+
                        " RefrenceMaterials as rm, RefrenceLink as rl where l.luvid = rm.RefrenceRefID AND rm.rmID = rl.refrenceID AND rl.jobID = '" + jobID + "' ");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                skills = dt.Rows[i][0].ToString();
                            }
                            else
                            {
                                skills = skills + "," + dt.Rows[i][0].ToString();
                            }
                        }
                        dt.Clear();

                        var usersSkill = skills.Split(',');
                        DataTable dt1 = new DataTable();
                        foreach (var skill in usersSkill)
                        {
                            dt = dmTakeMyJob.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r, ListofValues as l where r.rmStatus=1 AND r.refrenceType= 's' AND r.RefrenceRefID = l.luvid AND l.luvValueDisplayName = '" + skill + "'");
                            approveCount = Convert.ToInt32(dt.Rows[0][0]);
                            dt.Clear();
                            dt = dmTakeMyJob.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r, ListofValues as l where r.rmStatus=2 AND r.refrenceType= 's' AND r.RefrenceRefID = l.luvid AND l.luvValueDisplayName = '" + skill + "'");
                            pendingApproveCount = Convert.ToInt32(dt.Rows[0][0]);
                            dt.Clear();
                            dt = dmTakeMyJob.selectIntoTable("select " + approveCount + " as ApprovedCount," + pendingApproveCount + " as pendingApproveCount,refrenceType,refrenceRefID,rmUrl,searchTag,rmAddedOn,rmStatus,rmViewCount,rmSubject,rmMimeType" +
                                " from RefrenceMaterials as rm,RefrenceLink as r where rm.rmId=r.refrenceID and r.jobID=" + jobID);



                            dt1.Merge(dt);
                        }
                        if (dt1.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "found videos", convertDataTableToJson(dt1));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "not find any video ", null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getTokenForUploadVideo(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                /*forID ,videoType*/
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string forID = null;//forid for future refrence associate it with any refrenceid
                    string videoType = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("forID"))
                        {
                            forID = list.Value;
                        }
                        if (list.Key.Equals("forVideoType"))
                        {
                            videoType = list.Value;
                        }
                    }
                    dt = model.selectIntoTable("select displayName,currentJobTitle,currentCompanyName,userSkills,currentIndustry from Users where uid=" + userID);
                    if (dt.Rows.Count != 0 && videoType != null)
                    {
                        string[] xmlString = geUrlTokenFromYouTube(dt.Rows[0][0].ToString() + "," + dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString()
                            + "-" + dt.Rows[0][3].ToString(), dt.Rows[0][3].ToString());
                        if (xmlString != null)
                        {
                            dt = model.insertIntoTableWithLastId("insert into Resume (resumeStatus,refDocumentPath,refUserID,uploadDateTime,resumeTitle,resumeMime)" +
                                 "values (2,'','" + userID + "',now(),'" + "" + "','" + "video" + "')", "rid");
                            response = new ResponseData(1, "Token for upload Video", "[{\"token\":\"" + xmlString[0] + "\",\"url\":\"" + xmlString[1] + "\",\"rid\":\"" + dt.Rows[0][0].ToString() + "\"}]");
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    private string getAuthTokenUsingGoogleCLient()
    {
        try
        {
            StringBuilder sb = new StringBuilder();
            string param = "Email=takemyjob@csimplifyit.com&Passwd=Blah182728&service=youtube&source=takemyjob";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://www.google.com/accounts/ClientLogin");
            request.Method = "POST";
            request.Accept = "*/*";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] buffer = Encoding.UTF8.GetBytes(param);
            Stream POSTstream = request.GetRequestStream();
            POSTstream.Write(buffer, 0, buffer.Length);
            HttpWebResponse POSTResponse = (HttpWebResponse)request.GetResponse();
            Stream responseStream = POSTResponse.GetResponseStream();
            byte[] responsebytes = GetBytes(responseStream);
            string responseData = System.Text.Encoding.UTF8.GetString(responsebytes);
            string[] arr = responseData.Split(new string[] { "Auth=" }, StringSplitOptions.None);
            return arr[arr.Length - 1];
        }
        catch (WebException we)
        {
            var errrorResponse = we.Response as HttpWebResponse;
            try
            {
                var sr = new StreamReader(errrorResponse.GetResponseStream());
                string responseData = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception exc) { Console.WriteLine(exc.Message); }
        }
        return null;
    }
    private string[] geUrlTokenFromYouTube(string Title, string Description, string mediakeywork)
    {
        try
        {
            string token = getAuthTokenUsingGoogleCLient();
            string param = "<?xml version=\"1.0\"?>" +
                   "<entry xmlns=\"http://www.w3.org/2005/Atom\"" +
                   "  xmlns:media=\"http://search.yahoo.com/mrss/\"" +
                   " xmlns:yt=\"http://gdata.youtube.com/schemas/2007\">" +
                        " <media:group>" +
                          "  <media:title type=\"plain\">" + Title + "</media:title>" +
                   " <media:description type=\"plain\">" +
                     Description +
                       "  </media:description>" +
                        " <media:category" +
                         "   scheme=\"http://gdata.youtube.com/schemas/2007/categories.cat\">People" +
                            " </media:category>" +
                        "  <media:keywords>" + mediakeywork + "</media:keywords>" +
                          " </media:group>" +
                          "</entry>";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://gdata.youtube.com/action/GetUploadToken");
            request.Method = "POST";
            request.Headers.Add("Authorization", "GoogleLogin auth=" + token);
            request.Headers.Add("GData-Version", "2");
            request.Headers.Add("X-GData-Key", "key=AI39si4q2r7EHVqY3tgx8O4mF3JBxS_lj-bs4dv5VYdeYKWMdzaHBKfcAmRYhM3dPOVS_El2Ig0vxgbQKl-tr1454bcvJZck_Q");
            request.Headers.Add("charset", "UTF-8");

            request.ContentType = "application/atom+xml";
            byte[] buffer = Encoding.UTF8.GetBytes(param);
            Stream POSTstream = request.GetRequestStream();
            POSTstream.Write(buffer, 0, buffer.Length);
            HttpWebResponse POSTResponse = (HttpWebResponse)request.GetResponse();
            Stream responseStream = POSTResponse.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string xml = reader.ReadToEnd();
            string[] arr = xml.Split(new string[] { "<url>" }, StringSplitOptions.None);
            arr = arr[1].Split(new string[] { "</url>" }, StringSplitOptions.None);
            string youtubeUrl = arr[0];
            arr = arr[1].Split(new string[] { "<token>" }, StringSplitOptions.None);
            arr = arr[1].Split(new string[] { "</token>" }, StringSplitOptions.None);
            arr[1] = youtubeUrl;
            return arr;
        }
        catch (WebException we)
        {
            var errrorResponse = we.Response as HttpWebResponse;
            try
            {
                var sr = new StreamReader(errrorResponse.GetResponseStream());
                string responseData = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception exc) { Console.WriteLine(exc.Message); }
        }
        return null;
    }
    public Stream getSkills(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string skill = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skill"))
                        {
                            skill = list.Value;
                        }

                    }

                    if (skill != null && userID != 0)
                    {
                        //List<KeyValuePair<string, string>> parametersOfSp = new List<KeyValuePair<string, string>>();
                        //parametersOfSp.Add(new KeyValuePair<string, string>("jobID", jobID));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("refrenceType", requestedBy));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("url", "@url"));
                               dt = dmTakeMyJob.selectIntoTable("select a.luvid,CONCAT(a.luvValueDisplayName,' - (',CAST(count(RefrenceRefID) as CHAR(50)),')' ) luvValueDisplayName from ListofValues a LEFT JOIN RefrenceSuggetionMaterials b on a.luvid=b.RefrenceRefID  where luvValueDisplayName like '%" + 
                                   skill + "%' Group by b.RefrenceRefID,a.luvid,a.luvValueDisplayName ");

                   
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "found Skills", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "not found any Skill ", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getCompaniesByName(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string company = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("company"))
                        {
                            company = list.Value;
                        }
                    }
                    if (company != null)
                    {
                        //List<KeyValuePair<string, string>> parametersOfSp = new List<KeyValuePair<string, string>>();
                        //parametersOfSp.Add(new KeyValuePair<string, string>("jobID", jobID));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("refrenceType", requestedBy));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("url", "@url"));
                        dt = dmTakeMyJob.selectIntoTable("select cid,companyName from Company where companyName like '%" + company + "%'");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "found company", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "not found any company ", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getSkillTest(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int skillId = 0;
        int userID = -1;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {

                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != -1)
                            {
                                auditInfo(inputData, 1, "Fetch Question For Test", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skillid"))
                        {
                            if (list.Value == "")
                            {
                                string message = "Skill ID not Provided.";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            skillId = Convert.ToInt32(list.Value);
                        }
                    }
                    if (skillId != 0)
                    {
                        dt = dmTakeMyJob.selectIntoTable("select cid from Company where companyName in(select currentCompanyName from Users where uid = '" + userID + "')");
                        if (dt == null)
                        {
                            response = new ResponseData(0, "User is not associated with any company.", null);
                            return returnStmnt(response.returnStatment());
                            break;
                        }
                        int companyId = Convert.ToInt32(dt.Rows[0][0]);
                        dt.Clear();

                        dt = dmTakeMyJob.selectIntoTable("select cid as schID, companyName as schoolName from Company where cid = '" + companyId + "'");
                        string TMSchool = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select uid as canid,currentCompanyName as associatedSchool,userName as candidateEmail from Users where uid = " + userID);
                        if (dt.Rows.Count < 1)
                        {
                            response = new ResponseData(0, "User is not available.", null);
                            return returnStmnt(response.returnStatment());
                            break;
                        }
                        string users = convertDataTableToJson(dt);
                        dt.Clear();

                        dt = dmTakeMyJob.selectIntoTable("select * from TMTestCategory where testCategoryName in(select luvValueDisplayName from ListofValues where luvid = '" + skillId + "')");
                        if (dt.Rows.Count < 1)
                        {
                            response = new ResponseData(0, "There is no test for this Skill.", null);
                            return returnStmnt(response.returnStatment());
                            break;
                        }
                        string tmTestCategory = convertDataTableToJson(dt);
                        dt.Clear();

                        dt = dmTakeMyJob.selectIntoTable("select * from TMTests where testStatus = 1 AND skillId = '" + skillId + "' AND companyId is null");
                        if (dt.Rows.Count < 1)
                        {
                            response = new ResponseData(0, "There is no test for this Skill.", null);
                            return returnStmnt(response.returnStatment());
                            break;
                        }
                        string tmTest = convertDataTableToJson(dt);
                        dt.Clear();

                        dt = dmTakeMyJob.selectIntoTable("select * from TMTestQuestions where testID in(select tsid from TMTests where testStatus = 1 AND skillId = '" + skillId + "' AND companyId is null)");
                        if (dt.Rows.Count < 1)
                        {
                            response = new ResponseData(0, "There is no questions for the Specified test.", null);
                            return returnStmnt(response.returnStatment());
                            break;
                        }
                        string tmTestQuestion = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select * from TMQuestions where qid in(select questionID from TMTestQuestions where testID in (select tsid from TMTests where testStatus = 1 AND skillId = '" + skillId + "'AND companyId is null))");
                        string tmQuestions = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select * from TMAnswersOptions where associatedQuestionId in(select qid from TMQuestions where qid in(select questionId from TMTestQuestions where testID in (select tsid from TMTests where testStatus = 1 AND skillId = '" + skillId + "'AND companyId is null)))");
                        string tmasnwers = convertDataTableToJson(dt);
                        dt.Clear();
                        string result = "[{\"ttcandidate\":" + users + ",\"tmschool\":" + TMSchool + ",\"tmtestcategory\":" + tmTestCategory + ",\"tmtests\":" + tmTest + ",\"tmtestquestions\":" + tmTestQuestion + ",\"tmquestions\":" + tmQuestions + ",\"tmanswersoptions\":" + tmasnwers + "}]";
                        response = new ResponseData(1, "Test Questions :", result);
                        return returnStmnt(response.returnStatment());

                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getCompanySkillTest(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int skillId = 0;
        int userID = 0;
        int companyId = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Fetch Question For Test", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skillid"))
                        {
                            skillId = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("companyid"))
                        {
                            companyId = Convert.ToInt32(list.Value);
                        }
                    }
                    if (skillId != 0)
                    {
                        dt = dmTakeMyJob.selectIntoTable("select cid as schID, companyName as schoolName from Company where cid = '" + companyId + "'");
                        string TMSchool = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select uid as canid,currentCompanyName as associatedSchool,username as candidateEmail from Users where uid = " + userID);
                        string users = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select * from TMTestCategory where testCategoryName in(select luvValueDisplayName from ListofValues where luvid = '" + skillId + "')");
                        string tmTestCategory = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select * from TMTests where testStatus = 1 AND skillId = '" + skillId + "' AND companyId = '" + companyId + "'");
                        string tmTest = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select * from TMTestQuestions where testID in(select tsid from TMTests where testStatus = 1 AND skillId = '" + skillId + "' AND companyId = '" + companyId + "')");
                        string tmTestQuestion = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select * from TMQuestions where qid in(select questionID from TMTestQuestions where testID in (select tsid from TMTests where testStatus = 1 AND skillId = '" + skillId + "'AND companyId = '" + companyId + "'))");
                        string tmQuestions = convertDataTableToJson(dt);
                        dt.Clear();
                        dt = dmTakeMyJob.selectIntoTable("select * from TMAnswersOptions where associatedQuestionId in(select qid from TMQuestions where qid in(select questionID from TMTestQuestions where testID in (select tsid from TMTests where testStatus = 1 AND skillId = '" + skillId + "'AND companyId = '" + companyId + "')))");
                        string tmasnwers = convertDataTableToJson(dt);
                        dt.Clear();
                        string result = "[{\"ttcandidate\":" + users + ",\"tmSchool\":" + TMSchool + ",\"tmtestcategory\":" + tmTestCategory + ",\"tmtests\":" + tmTest + ",\"tmtestquestions\":" + tmTestQuestion + ",\"tmquestions\":" + tmQuestions + ",\"tmanswersoptions\":" + tmasnwers + "}]";
                        response = new ResponseData(0, "Test Questions :", result);
                        return returnStmnt(response.returnStatment());

                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
  //not to be use
    public Stream getCandidateResult(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int testId = 0;
        int userID = 0;
        string candidateScore = "";
        string outOfScore = "";
        string endTime = "";
        string startTime = "";
        int leaderBoardStatus = 0;
        int candidateId = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Fetch Question For Test", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("testid"))
                        {
                            testId = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("candidatescore"))
                        {
                            candidateScore = list.Value.ToString();
                        }
                        if (list.Key.Equals("outofscore"))
                        {
                            outOfScore = list.Value.ToString();
                        }
                        if (list.Key.Equals("endtime"))
                        {
                            endTime = list.Value.ToString();
                        }
                        if (list.Key.Equals("starttime"))
                        {
                            startTime = list.Value.ToString();
                        }
                        if (list.Key.Equals("leaderboardstatus"))
                        {
                            leaderBoardStatus = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("candidateid"))
                        {
                            candidateId = Convert.ToInt32(list.Value);
                        }
                    }
                    string result = dmTakeMyJob.insertIntoTable("insert into TTCandidateResult(testID,candidateScore,outOfScore,resultPublishedOn,endTime,leaderBoardStatus,startTime,candidateId) values('" + testId + "','" + candidateScore + "','" + outOfScore + "',now(),'" + endTime + "','" + leaderBoardStatus + "','" + startTime + "','" + candidateId + "')");
                    if (result == "true")
                    {
                        response = new ResponseData(0, "Test Data Stores successfully :", result);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(0, "Error during save data :", result);
                        return returnStmnt(response.returnStatment());
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getTokenForReferenceMaterial(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string ForType = "";
                    string ForContentType = "";
                    string Tittle = "";
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("forType"))
                        {
                            ForType = list.Value;
                        }
                        if (list.Key.Equals("forContentType"))
                        {
                            ForContentType = list.Value;
                        }
                        if (list.Key.Equals("suggestedTitle"))
                        {
                            Tittle = list.Value;
                        }
                    }

                    dt = model.selectIntoTable("select displayName,currentJobTitle,currentCompanyName,userSkills,currentIndustry from Users where uid=" + userID);
                    if (ForContentType == "11")
                    {


                        if (dt.Rows.Count != 0)
                        {
                            if (Tittle == "")
                            {
                                Tittle = dt.Rows[0][0].ToString() + "-" + dt.Rows[0][1].ToString();
                            }
                            else
                            {
                                Tittle = Tittle + "-" + dt.Rows[0][0].ToString() + "-" + dt.Rows[0][1].ToString();
                            }

                            string[] xmlString = geUrlTokenFromYouTube(dt.Rows[0][0].ToString() + "," + dt.Rows[0][1].ToString(),
                            Tittle, dt.Rows[0][3].ToString());
                            if (xmlString != null)
                            {
                                dt = model.insertIntoTableWithLastId("insert into Resume (resumeStatus,refDocumentPath,refUserID,uploadDateTime,resumeTitle,resumeMime)" +
                                "values (2,'','" + userID + "',now(),'" + "" + "','" + "video" + "')", "rid");
                                response = new ResponseData(1, "Token for upload Audio", "[{\"token\":\"" + xmlString[0] + "\",\"url\":\"" + xmlString[1] + "\",\"title\":\"" + Tittle + "\",\"rid\":\"" + dt.Rows[0][0].ToString() + "\"}]");
                                return returnStmnt(response.returnStatment());
                            }
                        }

                    }
                    else if (ForContentType == "9")
                    {
                        string soundCloudUrl = "https://api.soundcloud.com/tracks";         //url to upload audio on soundcloud.
                        if (Tittle == "")
                        {
                            Tittle = dt.Rows[0][0].ToString() + "-" + dt.Rows[0][1].ToString();
                        }
                        else
                        {
                            Tittle = Tittle + "-" + dt.Rows[0][0].ToString() + "-" + dt.Rows[0][1].ToString();
                        }
                        dt = model.selectIntoTable("select luvValueDisplayName from ListofValues where luvCategory= 'TokoenForSoundCloud'");
                        string soundCloudToken = dt.Rows[0][0].ToString();
                        dt = model.insertIntoTableWithLastId("insert into Resume (resumeStatus,refDocumentPath,refUserID,uploadDateTime,resumeTitle,resumeMime)" +
                                "values (2,'','" + userID + "',now(),'" + "" + "','" + ForType + "')", "rid");
                        response = new ResponseData(1, "Token for upload audio", "[{\"token\":\"" + soundCloudToken + "\",\"url\":\"" + soundCloudUrl + "\",\"rid\":\"" + dt.Rows[0][0].ToString() + "\",\"title\":\"" + Tittle + "\",\"forType\":\"" + ForType + "\",\"forContentType\":\"" + ForContentType + "\"}]");
                        return returnStmnt(response.returnStatment());


                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getReferenceMaterialForUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string skills = "";
        int uid = -1;
        int maxMaterialCount;
        string url = "";
        int approveCount;
        int pendingApproveCount;
        int fromRefMaterial = 1;    //indicates data is from ReferenceMaterials table
        int fromSuggestion = 0;     //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != -1)
                            {
                                auditInfo(inputData, 1, "Fetch Question For Test", userID);
                            }
                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("forUser"))
                        {
                            if (list.Value.ToString() == "")
                            {
                                uid = -1;
                            }
                            else
                            {
                                uid = Convert.ToInt32(list.Value);
                            }
                        }
                        if (list.Key.Equals("maxMaterialCount"))
                        {
                            maxMaterialCount = Convert.ToInt32(list.Value);
                        }
                    }
                    if (uid == -1)
                    {
                        response = new ResponseData(0, "No userID provided. ", null);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {

                        dt = model.selectIntoTable("select luvValueDisplayName from ListofValues where luvid in (select skillRef from UserSkills where uid ='" + uid + "')");

                        if (dt.Rows.Count != 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (skills == "")
                                {
                                    skills = dt.Rows[i][0].ToString();
                                }
                                else
                                {
                                    skills = skills + "," + dt.Rows[i][0].ToString();
                                }
                            }
                        }
                        dt.Clear();

                        var Skill = skills.Split(',');
                        DataTable dt1 = new DataTable();
                        foreach (var skill in Skill)
                        {
                            dt = model.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r, ListofValues as l where r.rmStatus=1 AND r.refrenceType= 's' AND r.RefrenceRefID = l.luvid AND l.luvValueDisplayName = '" + skill + "'");
                            approveCount = Convert.ToInt32(dt.Rows[0][0]);
                            dt.Clear();
                            dt = model.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r, ListofValues as l where r.rmStatus=2 AND r.refrenceType= 's' AND r.RefrenceRefID = l.luvid AND l.luvValueDisplayName = '" + skill + "'");
                            pendingApproveCount = Convert.ToInt32(dt.Rows[0][0]);
                            dt.Clear();
                            dt = model.selectIntoTable("select " + fromRefMaterial + " as fromTable, " + approveCount + " as ApprovedCount," + pendingApproveCount + " as pendingApproveCount, r.rmUrl,r.rmID,r.refrenceType,r.RefrenceRefID,r.searchTag,r.rmStatus," +
                            " r.rmSubject,r.viewCount,r.author,r.urlFilterRefID from RefrenceMaterials as r, ListofValues as l where "
                            + " r.refrenceType= 's' AND r.rmStatus != 0 AND r.RefrenceRefID = l.luvid AND l.luvValueDisplayName = '" + skill + "'");
                            if (dt.Rows.Count != 0)
                            {
                                dt1.Merge(dt);
                            }
                            else
                            {
                                continue;
                            }

                        }

                        if (dt1.Rows.Count != 0)
                        {
                            dt = model.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r,Company as c,Users as u where r.rmStatus=1 AND r.refrenceType= 'c' AND r.RefrenceRefID = c.cid AND c.companyName=u.currentCompanyName AND u.uid = '" + uid + "'");
                            approveCount = Convert.ToInt32(dt.Rows[0][0]);
                            dt.Clear();
                            dt = model.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r,Company as c,Users as u where r.rmStatus=2 AND r.refrenceType= 'c' AND r.RefrenceRefID = c.cid AND c.companyName=u.currentCompanyName AND u.uid = '" + uid + "'");
                            pendingApproveCount = Convert.ToInt32(dt.Rows[0][0]);
                            dt.Clear();
                            dt = model.selectIntoTable("select " + fromRefMaterial + " as fromTable, " + approveCount + " as ApprovedCount," + pendingApproveCount + " as pendingApproveCount, r.rmUrl,r.rmID,r.refrenceType,r.RefrenceRefID,r.searchTag,r.rmStatus," +
                               " r.rmSubject,r.viewCount,r.author,r.urlFilterRefID from RefrenceMaterials as r,Company as c,Users as u where "
                           + " r.refrenceType= 'c' AND r.rmStatus != 0 AND r.RefrenceRefID = c.cid AND c.companyName=u.currentCompanyName AND u.uid= '" + uid + "'");

                            dt1.Merge(dt);
                            url = convertDataTableToJson(dt1);
                            response = new ResponseData(1, "found Skills", url);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            foreach (var skill in Skill)
                            {
                                dt = model.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r, ListofValues as l where r.rmStatus=1 AND r.refrenceType= 's' AND r.RefrenceRefID = l.luvid AND l.luvValueDisplayName = '" + skill + "'");
                                approveCount = Convert.ToInt32(dt.Rows[0][0]);
                                dt.Clear();
                                dt = model.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r, ListofValues as l where r.rmStatus=2 AND r.refrenceType= 's' AND r.RefrenceRefID = l.luvid AND l.luvValueDisplayName = '" + skill + "'");
                                pendingApproveCount = Convert.ToInt32(dt.Rows[0][0]);
                                dt.Clear();

                                dt = model.selectIntoTable("select " + fromSuggestion + " as fromTable, " + approveCount + " as ApprovedCount," + pendingApproveCount + " as pendingApproveCount,r.rmUrl,r.rmID,r.refrenceType,r.RefrenceRefID,r.searchTag,r.rmStatus,r.rmSubject," +
                                    "r.viewCount,r.author,r.urlFilterRefID from RefrenceSuggetionMaterials as r, ListofValues as l where r.refrenceType= 's' AND r.rmStatus != 0 AND r.RefrenceRefID = l.luvid AND l.luvValueDisplayName = '" + skill + "'");
                                if (dt.Rows.Count != 0)
                                {
                                    dt1.Merge(dt);
                                }
                                else
                                {
                                    continue;
                                }

                            }
                            dt = model.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r,Company as c,Users as u where r.rmStatus=1 AND r.refrenceType= 'c' AND r.RefrenceRefID = c.cid AND c.companyName=u.currentCompanyName AND u.uid = '" + uid + "'");
                            approveCount = Convert.ToInt32(dt.Rows[0][0]);
                            dt.Clear();
                            dt = model.selectIntoTable("select count(rmID) as Approved from RefrenceSuggetionMaterials as r,Company as c,Users as u where r.rmStatus=2 AND r.refrenceType= 'c' AND r.RefrenceRefID = c.cid AND c.companyName=u.currentCompanyName AND u.uid = '" + uid + "'");
                            pendingApproveCount = Convert.ToInt32(dt.Rows[0][0]);
                            dt.Clear();
                            dt = model.selectIntoTable("select " + fromSuggestion + " as fromTable, " + approveCount + " as ApprovedCount," + pendingApproveCount + " as pendingApproveCount, r.rmUrl,r.rmID,r.refrenceType,r.RefrenceRefID,r.searchTag,r.rmStatus," +
                               " r.rmSubject,r.viewCount,r.author,r.urlFilterRefID from RefrenceSuggetionMaterials as r,Company as c,Users as u where "
                           + " r.refrenceType= 'c' AND r.rmStatus != 0 AND r.RefrenceRefID = c.cid AND c.companyName=u.currentCompanyName AND u.uid= '" + uid + "'");

                            dt1.Merge(dt);
                            if (dt1.Rows.Count != 0)
                            {
                                url = convertDataTableToJson(dt1);
                                response = new ResponseData(1, "found Suggested Skills", url);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }


            }
            response = new ResponseData(1, "No Data found for given user ", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream updateUserContactNo(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int uid = 0;
        int userID = 0;
        string mobileNo = "";
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Udate User Contact No", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("userID"))
                        {
                            uid = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("mobileNo"))
                        {
                            mobileNo = Convert.ToString(list.Value);
                        }
                    }
                    if (mobileNo == "")
                    {
                        response = new ResponseData(0, "Mobileno Not Provided", null);
                        return returnStmnt(response.returnStatment());
                    }
                    model.updateIntoTable("update Users set userContactNo ='" + mobileNo + "' where uid = '" + uid + "'");
                    string result = "[{\"userID\":" + uid + ",\"mobileNo\":" + mobileNo + "}]";
                    response = new ResponseData(1, "mobileno updated successfully", convertDataTableToJson(result));
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream savePaymentDetails(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int userID = 0;
        string paymentSource = "";
        string paymentFromUserId = "";
        string paymentStatus = "";
        string paymentInitiatedOn = "";
        string paymentRecieptID = "";
        string paymentRecieptIDDateTime = "";
        string isBadPayment = "";
        string BadPaymentSignedBy = "";
        string paymentMadeFromLogitude = "";
        string paymentMadeFromLattitude = "";
        string invoiceStatus = "";
        string invoiceSentOn = "";
        string invoiceID = "";
        string amount = "";
        string amountCurrency = "";
        string purchaseToken = "";
        string paymentForItem = "";


        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Insert Payment details", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("paymentSource"))
                        {
                            paymentSource = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("paymentFromUserId"))
                        {
                            paymentFromUserId = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("paymentStatus"))
                        {
                            paymentStatus = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("paymentInitiatedOn"))
                        {
                            paymentInitiatedOn = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("paymentRecieptID"))
                        {
                            paymentRecieptID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("paymentRecieptIDDateTime"))
                        {
                            paymentRecieptIDDateTime = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("isBadPayment"))
                        {
                            isBadPayment = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("BadPaymentSignedBy"))
                        {
                            BadPaymentSignedBy = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("paymentMadeFromLogitude"))
                        {
                            paymentMadeFromLogitude = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("paymentMadeFromLattitude"))
                        {
                            paymentMadeFromLattitude = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("invoiceStatus"))
                        {
                            invoiceStatus = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("invoiceSentOn"))
                        {
                            invoiceSentOn = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("invoiceID"))
                        {
                            invoiceID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("amount"))
                        {
                            amount = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("amountCurrency"))
                        {
                            amountCurrency = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("purchaseToken"))
                        {
                            purchaseToken = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("paymentForItem"))
                        {
                            paymentForItem = Convert.ToString(list.Value);
                        }
                    }
                    if ((paymentRecieptID == "") || (paymentFromUserId == "") || (paymentForItem == "") || (paymentRecieptIDDateTime == "") || (paymentStatus == "") || (amount == "") || (amountCurrency == ""))
                    {
                        response = new ResponseData(0, "You have not provide all details", null);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        dt = model.selectIntoTable("select payId from Payments where paymentRecieptID=" + paymentRecieptID);
                        if (dt.Rows[0][0].ToString() == "")
                        {

                            dt = model.insertIntoTableWithLastId("insert into Payments(paymentSource,paymentFromUserId,paymentStatus,paymentInitiatedOn,paymentRecieptID,paymentRecieptIDDateTime,isBadPayment," +
                            "BadPaymentSignedBy,paymentMadeFromLogitude,paymentMadeFromLattitude,invoiceStatus,invoiceSentOn,invoiceID,amount,amountCurrency,purchaseToken,paymentForItem)" +
                            "values('" + paymentSource + "','" + paymentFromUserId + "','" + paymentStatus + "','" + paymentInitiatedOn + "','" + paymentRecieptID + "','" + paymentRecieptIDDateTime + "','" + isBadPayment + "'" +
                            ",'" + BadPaymentSignedBy + "','" + paymentMadeFromLogitude + "','" + paymentMadeFromLattitude + "','" + invoiceStatus + "','" + invoiceSentOn + "','" + invoiceID + "','" + amount + "','" + amountCurrency + "','" + purchaseToken + "','" + paymentForItem + "')", "payId");

                            response = new ResponseData(1, "Payment Details Saved Successfully", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Payment Details already Saved", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getUserCountForJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true; // vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get user count for jjob", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string jobID = "";
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }

                    if (userID != 0 && jobID != "")
                    {
                        dt = model.selectIntoTable("select count(*) from Job where jid=" + jobID);
                        if (Convert.ToInt32(dt.Rows[0][0]) < 1)
                        {
                            response = new ResponseData(1, "This job is not available", null);
                            return returnStmnt(response.returnStatment());
                        }
                        dt = model.selectIntoTable("select count(distinct(uid)) as userCount from UserSkills where skillRef in (select distinct(l.luvid) from ListofValues as l, RefrenceMaterials as rm, RefrenceLink as rl where l.luvid = rm.RefrenceRefID AND rm.rmID = rl.refrenceID AND rl.jobID = '" + jobID + "' )");

                        if (Convert.ToInt32(dt.Rows[0][0]) != 0)
                        {
                            response = new ResponseData(1, "Found Users For This Job", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "There is no user for this job ", null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getPaymentDetailsForUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string forUserID = null;
                    int approveCount;
                    int pendingApproveCount;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("forUserID"))
                        {
                            forUserID = list.Value;
                        }
                    }

                    if (userID != 0 && forUserID != null)
                    {
                        string paymentDetails = "";
                        dt = model.selectIntoTable("SELECT paymentSource,paymentFromUserId,paymentStatus,paymentInitiatedOn,paymentRecieptID,paymentRecieptIDDateTime,isBadPayment," +
                            "BadPaymentSignedBy,paymentMadeFromLogitude,paymentMadeFromLattitude,invoiceStatus,invoiceSentOn,invoiceID,amount,amountCurrency,purchaseToken,paymentForItem FROM Payments WHERE paymentFromUserId = '" + forUserID + "' ");

                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "found payments", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "No Payments available", null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream enrollCandidate(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int userID = 0;
        string candidateID = "";
        string candidateName = "";
        string candidateEmailID = "";
        string candidateContactNo = "";
        string candidateSkills = "";
        string candidateCollegeName = "";
        string candidateCollegeURL = "";
        string candidateHomeAddress = "";
        string candidatePassword = "";

        string willTakeTrainingFromHome="1", candidateYearOfPassingOut="";

        string enrolledForPeriod = "";
        string enrollmentForProjectID = "";
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            flag = true; // vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Enroll Candidate", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {

                        if (list.Key.Equals("candidateID"))
                        {
                            candidateID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("candidateName"))
                        {
                            candidateName = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("candidatePassword"))
                        {
                            candidatePassword = Convert.ToString(list.Value);
                        }
      
                        if (list.Key.Equals("candidateCollegeName"))
                        {
                            candidateCollegeName = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("candidateEmailID"))
                        {
                            candidateEmailID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("candidateHomeAddress"))
                        {
                            candidateHomeAddress = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("enrolledForPeriod"))
                        {
                            enrolledForPeriod = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("enrollmentForProjectID"))
                        {
                            enrollmentForProjectID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("candidateContactNo"))
                        {
                            candidateContactNo = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("yearofpassing"))
                        {
                            candidateYearOfPassingOut = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("willTakeTrainingFromHome"))
                        {
                            willTakeTrainingFromHome = Convert.ToString(list.Value);
                        }
                        
                    }

                    //if ((candidateName == "") || (candidateContactNo == "") || (candidateEmailID == "")  || (parentName == "") || (parentContactNo == "") || (parentEmailID == "")|| (mentorContactNo == "") || (mentorName == "") || (mentorEmailID == "") || (enrollmentForProjectID==""))
                    if ((candidateName == "") || (candidateContactNo == "") || (candidateEmailID == ""))
                    {
                        response = new ResponseData(0, "You have not provided all enrollment details", null);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        // Check if user is already enrolled 
                        dt = model.selectIntoTable("select uid as candidateID , eid as enrollmentID, enrollmentNo, enrolledOnDateTime from Users, Enrollment where userName='" + candidateEmailID + "' AND candidateID = uid");
                        if (dt.Rows.Count == 0)
                        {

                            dt = model.insertIntoTableWithLastId("insert into Users(userName,userContactNo,userType,userPassword,userStatus,modifiedBy,modifiedDateTime,createdBy,createdDateTime,registerationDate,currentJobTitle,currentCompanyName,currentIndustry,displayName,lattitude,longitude,lastCheckInLocation,lastLoginDateTime,userSkills)VALUES("
                                + "'" + candidateEmailID + "'," + "'" + candidateContactNo + "'," + "1," + "'" + EncryptBase64.EncodeTo64(candidatePassword) + "',1,'Fresher Villa',now()," + "'Fresher Villa',now(),now(),'" + candidateHomeAddress + "','" + candidateCollegeName + "','" + candidateCollegeURL + "','" + candidateName + "',null, null, null, now(), 'User Skills')", "candidateID");
                            var newUserID = dt.Rows[0][0];
                            response = new ResponseData(1, "Candidate Details Saved Successfully", convertDataTableToJson(dt));
                            dt = model.insertIntoTableWithLastId("INSERT INTO Enrollment ( candidateID, candidateName, enrollmentNo, parentID, parentName, mentorID, mentorName, enrolledOnDateTime, enrolledForPeriod, lastModificationDate, enrollmentStatus, enrollmentAgreementAcceptanceON, enrollmentExpiresOn, willTakeTrainingFromHome, candidateYearOfPassingOut) VALUES (" + newUserID + ", '" + candidateName + "', CONCAT('E/FV/',DATE_FORMAT(now(),'%y/%m%j%k%s')), " + newUserID + ", '" + candidateName + "', " + newUserID + ", '" + candidateName + "', now(), '180', now(), 0, now(), DATE_ADD(now(),INTERVAL 180 DAY) ,'" + willTakeTrainingFromHome + "', '" + candidateYearOfPassingOut + "')", "enrollmentID");
                            EmailSend sendEmail = new EmailSend();
                            sendEmail.sendMailWithTemplateForEnroll("Congratulation for choosing Freshersvilla",
                                candidateName, candidateEmailID, candidatePassword, "EnrollPassword.html");
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Candidate Details already Saved", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                    }


                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message + ex.StackTrace, null);
            return returnStmnt(response.returnStatment());
        }


    }



    public Stream updateCandidateParentDetails(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int userID = 0;
        string candidateID = "";
        string parentContactNo = "";
        string parentEmailID = "";
        string parentName = "";
        string parentPassword = "";
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Enroll Candidate", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {

                        if (list.Key.Equals("candidateID"))
                        {
                            candidateID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("parentName"))
                        {
                            parentName = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("parentContactNo"))
                        {
                            parentContactNo = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("parentEmailID"))
                        {
                            parentEmailID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("parentPassword"))
                        {
                            parentPassword = "localfield";
                        }

                    }
                    parentPassword = "localfield";
                    //if ((candidateName == "") || (candidateContactNo == "") || (candidateEmailID == "")  || (parentName == "") || (parentContactNo == "") || (parentEmailID == "")|| (mentorContactNo == "") || (mentorName == "") || (mentorEmailID == "") || (enrollmentForProjectID==""))
                    if ((candidateID == "") || (parentContactNo == "") || (parentEmailID == "") || (parentName == ""))
                    {
                        response = new ResponseData(0, "You have not provided all details of Parent", null);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        // Check if user is already enrolled 
                        dt = model.selectIntoTable("select uid from Users where uid='" + candidateID + "'");
                        if (dt.Rows.Count > 0)
                        {

                            // Check if Parent Exists 
                            dt = model.selectIntoTable("select uid as parentID , eid as enrollmentID, enrollmentNo, enrolledOnDateTime from Users, Enrollment where userName='" + parentEmailID + "' AND parentID = uid");
                            if (dt.Rows.Count == 0)
                            {
                                dt = model.insertIntoTableWithLastId("insert into Users(userName,userContactNo,userType,userPassword,userStatus,modifiedBy,modifiedDateTime,createdBy,createdDateTime,registerationDate,currentJobTitle,currentCompanyName,currentIndustry,displayName,lattitude,longitude,lastCheckInLocation,lastLoginDateTime,userSkills)VALUES("
                                    + "'" + parentEmailID + "'," + "'" + parentContactNo + "'," + "5," + "'" +EncryptBase64.EncodeTo64(parentPassword) + "',1," + "'Fresher Villa',now()," + "'Fresher Villa',now(),now(),'Parent of Fresher', '?', '?', '" + parentName + "',null, null, null, now(), 'User Skills')", "parentID");

                            }

                            response = new ResponseData(1, "Candidate Parent Details Saved Successfully", convertDataTableToJson(dt));
                            var newParentID = dt.Rows[0][0];


                            model.updateIntoTablewithRowCount("UPDATE Enrollment SET parentID = " + newParentID + ", parentName = '" + parentName + "' WHERE candidateID = " + candidateID);
                            EmailSend sendEmail = new EmailSend();
                            sendEmail.sendMailWithTemplateForEnroll("Congratulation for choosing Freshersvilla",
                                parentName, parentEmailID, parentPassword, "EnrollPassword.html");
                          
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(0, "Candidate Not Found!", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream updateCandidateMentorDetails(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int userID = 0;
        string candidateID = "";
        string mentorContactNo = "";
        string mentorEmailID = "";
        string mentorName = "";
        string mentorPassword = "";
        string candidateCollegeName = "";
        string candidateCollegeURL = "";
        
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Enroll Candidate", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {

                        if (list.Key.Equals("candidateID"))
                        {
                            candidateID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("mentorName"))
                        {
                            mentorName = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("mentorContactNo"))
                        {
                            mentorContactNo = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("mentorEmailID"))
                        {
                            mentorEmailID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("mentorPassword"))
                        {
                            mentorPassword = "localfield";
                        }
                        else if (list.Key.Equals("candidateCollegeName"))
                        {
                            candidateCollegeName = Convert.ToString(list.Value);
                        }
                        else if (list.Key.Equals("candidateCollegeURL"))
                        {
                            candidateCollegeURL = Convert.ToString(list.Value);
                        }

                    }
                    mentorPassword = "localfield";
                    //if ((candidateName == "") || (candidateContactNo == "") || (candidateEmailID == "")  || (parentName == "") || (parentContactNo == "") || (parentEmailID == "")|| (mentorContactNo == "") || (mentorName == "") || (mentorEmailID == "") || (enrollmentForProjectID==""))
                    if ((candidateID == "") || (mentorContactNo == "") || (mentorEmailID == "") || (mentorName == ""))
                    {
                        response = new ResponseData(0, "You have not provided all details of Mentor", null);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        // Check if user is already enrolled 
                        dt = model.selectIntoTable("select uid from Users where uid='" + candidateID + "'");
                        if (dt.Rows.Count > 0)
                        {

                            // Check if Parent Exists 
                            dt = model.selectIntoTable("select uid as mentorID , eid as enrollmentID, enrollmentNo, enrolledOnDateTime from Users, Enrollment where userName='" + mentorEmailID + "' AND mentorID = uid");
                            if (dt.Rows.Count == 0)
                            {
                                dt = model.insertIntoTableWithLastId("insert into Users(userName,userContactNo,userType,userPassword,userStatus,modifiedBy,modifiedDateTime,createdBy,createdDateTime,registerationDate,currentJobTitle,currentCompanyName,currentIndustry,displayName,lattitude,longitude,lastCheckInLocation,lastLoginDateTime,userSkills)VALUES("
                                    + "'" + mentorEmailID + "'," + "'" + mentorContactNo + "'," + "6," + "'" +EncryptBase64.EncodeTo64( mentorPassword ) + "',1," + "'Fresher Villa',now()," + "'Fresher Villa',now(),now(),'Mentor of Fresher', '?', '?', '" + mentorName + "',null, null, null, now(), 'User Skills')", "mentorID");

                            }
                            response = new ResponseData(1, "Candidate Mentor Details Saved Successfully", convertDataTableToJson(dt));

                            var newMentorID = dt.Rows[0][0];


                            model.updateIntoTablewithRowCount("UPDATE Enrollment SET mentorID = " + newMentorID + ", mentorName = '" + mentorName  + "', candidateCollegeName = '" + candidateCollegeName + "', candidateCollegeURL = '"+ candidateCollegeURL + "' WHERE candidateID = " + candidateID);

                            EmailSend sendEmail = new EmailSend();
                            sendEmail.sendMailWithTemplateForEnroll("Congratulation for choosing Freshersvilla",
                                mentorName, mentorEmailID, mentorPassword, "EnrollPassword.html");

                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(0, "Candidate Not Found!", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }


    public Stream updateCandidateProjectDetails(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int userID = 0;
        string candidateID = "";
        string candidateName = "";
        string candidateEmailID = "";
        string candidateContactNo = "";
        string candidateSkills = "";
        string candidateCollegeName = "";
        string candidatePassword = "";

        string parentContactNo = "";
        string mentorContactNo = "";
        string parentEmailID = "";
        string mentorEmailID = "";

        string enrollmentNo = "";
        string parentID = "";
        string parentName = "";
        string mentorID = "";
        string mentorName = "";

        string enrolledForPeriod = "";
        string enrollmentForProjectID = "";




        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Enroll Candidate", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {

                        if (list.Key.Equals("candidateID"))
                        {
                            candidateID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("candidateName"))
                        {
                            candidateName = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("parentID"))
                        {
                            parentID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("parentName"))
                        {
                            parentName = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("mentorID"))
                        {
                            mentorID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("mentorName"))
                        {
                            mentorName = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("enrolledForPeriod"))
                        {
                            enrolledForPeriod = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("enrollmentForProjectID"))
                        {
                            enrollmentForProjectID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("candidateContactNo"))
                        {
                            candidateContactNo = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("parentContactNo"))
                        {
                            parentContactNo = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("mentorContactNo"))
                        {
                            mentorContactNo = Convert.ToString(list.Value);
                        } if (list.Key.Equals("candidateEmailID"))
                        {
                            candidateEmailID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("parentEmailID"))
                        {
                            parentEmailID = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("mentorEmailID"))
                        {
                            mentorEmailID = Convert.ToString(list.Value);
                        }
                    }

                    //if ((candidateName == "") || (candidateContactNo == "") || (candidateEmailID == "")  || (parentName == "") || (parentContactNo == "") || (parentEmailID == "")|| (mentorContactNo == "") || (mentorName == "") || (mentorEmailID == "") || (enrollmentForProjectID==""))
                    if ((candidateName == "") || (candidateContactNo == "") || (candidateEmailID == ""))
                    {
                        response = new ResponseData(0, "You have not provided all enrollment details", null);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        // Check if user is already enrolled 
                        dt = model.selectIntoTable("select uid from Users where userName='" + candidateEmailID + "'");
                        if (dt.Rows.Count == 0)
                        {



                            //dt = model.insertIntoTableWithLastId("insert into Payments(paymentSource,paymentFromUserId,paymentStatus,paymentInitiatedOn,paymentRecieptID,paymentRecieptIDDateTime,isBadPayment," +
                            //"BadPaymentSignedBy,paymentMadeFromLogitude,paymentMadeFromLattitude,invoiceStatus,invoiceSentOn,invoiceID,amount,amountCurrency,purchaseToken,paymentForItem)" +
                            //"values('" + paymentSource + "','" + paymentFromUserId + "','" + paymentStatus + "','" + paymentInitiatedOn + "','" + paymentRecieptID + "','" + paymentRecieptIDDateTime + "','" + isBadPayment + "'" +
                            //",'" + BadPaymentSignedBy + "','" + paymentMadeFromLogitude + "','" + paymentMadeFromLattitude + "','" + invoiceStatus + "','" + invoiceSentOn + "','" + invoiceID + "','" + amount + "','" + amountCurrency + "','" + purchaseToken + "','" + paymentForItem + "')", "payId");

                            dt = model.insertIntoTableWithLastId("insert into Users(userName,userContactNo,userType,userPassword,userStatus,modifiedBy,modifiedDateTime,createdBy,createdDateTime,registerationDate,currentJobTitle,currentCompanyName,currentIndustry,displayName,lattitude,longitude,lastCheckInLocation,lastLoginDateTime,userSkills)VALUES("
                                + "'" + candidateEmailID + "'," + "'" + candidateContactNo + "'," + "1," + "'" + candidatePassword + "',1," + "'Fresher Villa',now()," + "'Fresher Villa',now(),now(),'Fresher', 'College Name', 'IT', '" + candidateName + "',null, null, null, now(), 'User Skills')", "candidateID");

                            response = new ResponseData(1, "Candidate Details Saved Successfully", convertDataTableToJson(dt));

                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Candidate Details already Saved", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream saveCandidateAnswersData(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        //  string returnString = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "knowMyFriends", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != 0)
                {
                    string resposnevalue = saveAnswersData("Answers", "{\"data\":" + objelement.Value.ToString() + "}", Convert.ToInt32(userID));
                    if (resposnevalue != "error")
                    {
                        response = new ResponseData(1, "Answers are saved", "[" + resposnevalue + "]");
                        return returnStmnt(response.returnStatment());
                    }
                    else {
                        response = new ResponseData(0, "You can not update previous attempt", null);
                        return returnStmnt(response.returnStatment());
                    }
                }

            }
            response = new ResponseData(0, Constant.invallidtoken, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            //  returnString = "{\"status\":0,\"Description\":\"" + exc.Message + "\",\"results\":[null]}";
            response = new ResponseData(0, exc.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }

    
    private string saveAnswersData(string tableName, string jsonarr, int uid)
    {
        DataTable dt = new DataTable();
        int MAxAttemptId = 0;
        string answerStatus = "", associatedQuestionId = "", assocuatedAnswerOptionId = "",
            answerText = "", attemptID = "", associatedTestID = "",
            candidateID = "", timeInSecondAfterTest = "", jobID = "";

        candidateID = Convert.ToString(uid);

        JObject root = JObject.Parse(jsonarr);
        JArray items = (JArray)root["data"];
        int totalScore = items.Count();
        foreach (var data in items)
        {
            JObject answerData = JObject.Parse(data.ToString());
            foreach (var oAnswer in answerData)
            {
                if (oAnswer.Key.Equals("associatedQuestionId"))
                {
                    associatedQuestionId = oAnswer.Value.ToString();
                }
                else if (oAnswer.Key.Equals("assocuatedAnswerOptionId"))
                {
                    assocuatedAnswerOptionId = oAnswer.Value.ToString();
                }
                else if (oAnswer.Key.Equals("answerText"))
                {
                    answerText = oAnswer.Value.ToString();

                }
                else if (oAnswer.Key.Equals("attemptID"))
                {
                    attemptID = oAnswer.Value.ToString();
                }
                else if (oAnswer.Key.Equals("assocuatedAnswerOptionID"))
                {
                    assocuatedAnswerOptionId = oAnswer.Value.ToString();
                }
                else if (oAnswer.Key.Equals("answerText"))
                {
                    answerText = oAnswer.Value.ToString();

                }
                else if (oAnswer.Key.Equals("associatedTestID"))
                {
                    associatedTestID = oAnswer.Value.ToString();
                }
                else if (oAnswer.Key.Equals("timeInSecondsAfterTest"))
                {
                    timeInSecondAfterTest = oAnswer.Value.ToString();

                }
                else if (oAnswer.Key.Equals("jobID"))
                {
                    jobID = oAnswer.Value.ToString();
                }

            }

            // canID, associatedQuestionId, assocuatedAnswerOptionId, 
            // lastModifiedOn, answerText, attemptID, associatedTestID, 
            // candidateID, timeInSecondAfterTest, jobID;

            if (String.IsNullOrEmpty(associatedQuestionId) || String.IsNullOrEmpty(assocuatedAnswerOptionId) ||
                String.IsNullOrEmpty(attemptID) || String.IsNullOrEmpty(associatedTestID) || String.IsNullOrEmpty(jobID)
                || String.IsNullOrEmpty(jobID))
            {
                answerStatus = answerStatus + answerData;
            }
            else
            {
                    // Check if Parent Exists 
                if (MAxAttemptId == 0) {
                    dt = model.selectIntoTable("select max(attemptID) from TTCandidateAnswers where  jobID='" +
                         jobID + "'" + " AND associatedTestID='" + associatedTestID + "'"  +
                         " AND candidateID='" + candidateID + "'");
                    MAxAttemptId = Convert.ToInt32( dt.Rows[0][0].ToString())+1;

                }
                //dt = model.selectIntoTable("select canID as canID from TTCandidateAnswers where  jobID='" +
                //        jobID + "'" + " AND associatedTestID='" + associatedTestID + "'" + " AND attemptID='" + attemptID + "'"+
                //        " AND candidateID='" + candidateID + "'");
              
                    dt = model.insertIntoTableWithLastId("INSERT INTO TTCandidateAnswers (associatedQuestionId, assocuatedAnswerOptionId," +
                        " lastModifiedOn, answerText, attemptID, associatedTestID, candidateID, timeInSecondAfterTest, jobID) VALUES ( " +
                        " '" + associatedQuestionId + "','" + assocuatedAnswerOptionId + "',now(),'" + answerText + "','" +
                        MAxAttemptId + "','" + associatedTestID + "','" + candidateID + "','" + timeInSecondAfterTest + "','" +
                        jobID + "' )", "answerID");
               
               
                    answerStatus = answerStatus + "{\"associatedQuestionId\":\"" + associatedQuestionId + "\",\"assocuatedAnswerOptionId\":\"" + assocuatedAnswerOptionId + "\"},";
                    
                }
        }

       answerStatus= answerStatus.TrimEnd(',');
        return answerStatus ;
    }

   private void makeResultForTest(int jobId,int attemptId,int testId,int candidateId){
      DataTable tableForCorrectAnswer = model.selectIntoTable("SELECT count(*)  FROM TTCandidateAnswers TTCA inner join  " +
 "TMAnswersOptions TMAO on TMAO.associatedQuestionId=TTCA.associatedQuestionId " +
 "and  TTCA.assocuatedAnswerOptionId=TMAO.aoid  " +
 "and (TMAO.isCorrectAnswer=1 or TMAO.answerOptionType!=0) " +
 "where TTCA.candidateID=12015 and TTCA.attemptID=2 and TTCA.associatedTestID=6 and TTCA.jobID=2; ");

        DataTable tableForTotalAttemp = model.selectIntoTable("SELECT count(*)  FROM TTCandidateAnswers TTCA inner join  " +
 "TMAnswersOptions TMAO on TMAO.associatedQuestionId=TTCA.associatedQuestionId " +
 "and  TTCA.assocuatedAnswerOptionId=TMAO.aoid  " +
 "and (TMAO.isCorrectAnswer=1 or TMAO.answerOptionType!=0) " +
 "where TTCA.candidateID=12015 and TTCA.attemptID=2 and TTCA.associatedTestID=6 and TTCA.jobID=2; ");
        int rowCount = model.updateIntoTablewithRowCount("update TTCandidateResult set candidateScore='" +
            tableForCorrectAnswer.Rows[0][0].ToString() + "',outOfScore=" + tableForTotalAttemp.Rows[0][0].ToString() + " where   testID='" + testId
            + "' and candidateID='" + candidateId + "' and " + "attemptID='" + attemptId + "' and jobID='" + jobId + "'");
        if (rowCount == 0) { 
        
        }
   }
    
    public Stream auditUrl(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int userID = 0;
        string Url = "";
       

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "For Audit", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {

                        if (list.Key.Equals("Url"))
                        {
                            Url = Convert.ToString(list.Value);
                        }
                        auditInfo(inputData, 1, Url, userID);
                        response = new ResponseData(1, "Successfully Audit","[]");
                        return returnStmnt(response.returnStatment());
                    }


                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }

    public Stream getCounsellingEvents(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int userID = 0;
        string Url = "";


        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "For Audit", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    dt = model.selectIntoTable("select * from CounsellingEvents CE INNER JOIN  Users U on U.uid=CE.cCounsellorId where CE.cCounsellingDate>now() order by CE.cCounsellingDate desc");
                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Events found", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else {
                        response = new ResponseData(0,"No events found", null);
                        return returnStmnt(response.returnStatment());
                    }
                }

            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }

    public Stream updateSkillWithURL(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string emailaddress ="" ;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                            emailaddress = dt.Rows[0][0].ToString();
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string skill = null;
                    string Url = null;
                    string longitude = null;
                    string latitude = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skillList"))
                        {
                            skill = list.Value;
                        }
                        if (list.Key.Equals("url"))
                        {
                            Url = list.Value;
                        }
                        if (list.Key.Equals("longitude"))
                        {
                            longitude = list.Value;
                        }
                        if (list.Key.Equals("latitude"))
                        {
                            latitude = list.Value;
                        }

                    }
                    var SkillList = skill.Split(',');


                    foreach (string skillId in SkillList)
                    {
                        if (userID != 0)
                        {
                            //List<KeyValuePair<string, string>> parametersOfSp = new List<KeyValuePair<string, string>>();
                            //parametersOfSp.Add(new KeyValuePair<string, string>("jobID", jobID));
                            //parametersOfSp.Add(new KeyValuePair<string, string>("refrenceType", requestedBy));
                            //parametersOfSp.Add(new KeyValuePair<string, string>("url", "@url"));
                            dt = model.insertIntoTableWithLastId("insert into RefrenceMaterials select '','u','" + skillId + "','" + Url + "',"
                       + "luvValueDisplayName,Now(),0,1,luvValueDisplayName,'video',null,null,0,'" + userID + "',null,null,null,null,'" + userID + "','" + longitude + "','" + latitude + "','','','' From  ListofValues "
                       + "where luvid= " + skillId
                       + "", "RefId");

                          

                        }
                    }

                    dt = model.selectIntoTable("select distinct(rmUrl) ReferenceMaterial,DATE_FORMAT(rmAddedOn, '%d-%b-%Y') as Date,'Gurgaon' Location from RefrenceMaterials WHERE refrenceType='U'  And rmUrl ='" + Url + "' order by rmAddedOn desc");
                    ConvertIntoHTML CH = new ConvertIntoHTML();
                    List<string> listForColumn = new List<string>();
                    listForColumn.Add("Location");
                    listForColumn.Add("Date");
                    listForColumn.Add("ReferenceMaterial");



                    string Html = CH.convertDataTableToHTMLTableNoLimit(dt, listForColumn, false);
//Task.Factory.StartNew(() =>
                   // {
                        EmailSend se = new EmailSend();
                        se.sendMailForUploadedVideo(emailaddress, Html, "",Url);
                   // });
                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Skill Saved SuccessFully", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Error ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream updateAssignment(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string skill = null;
                    string Url = null;
                    string longitude = null;
                    string latitude = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skillList"))
                        {
                            skill = list.Value;
                        }
                        if (list.Key.Equals("url"))
                        {
                            Url = list.Value;
                        }
                        if (list.Key.Equals("longitude"))
                        {
                            longitude = list.Value;
                        }
                        if (list.Key.Equals("latitude"))
                        {
                            latitude = list.Value;
                        }

                    }
                    var SkillList = skill.Split(',');


                    foreach (string skillId in SkillList)
                    {
                        if (userID != 0)
                        {
                            
                            dt = model.insertIntoTableWithLastId("insert into RefrenceMaterials select '','ua','" + skillId + "','" + Url + "',"
                       + "luvValueDisplayName,Now(),0,1,luvValueDisplayName,'video',null,null,0,'" + userID + "',null,null,null,null,'" + userID + "','" + longitude + "','" + latitude + "','','','' From  ListofValues "
                       + "where luvid= " + skillId
                       + "", "RefId");


                        }
                    }
                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Skill Saved SuccessFully", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Error ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream getUploadedVideoForUsers(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string FromDate = null;
                    string forUser = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("FromDate"))
                        {
                            FromDate = list.Value;
                        }
                        if (list.Key.Equals("forUser"))
                        {
                            forUser = list.Value;
                        }


                    }

                    if (userID != 0)
                    {
                        //List<KeyValuePair<string, string>> parametersOfSp = new List<KeyValuePair<string, string>>();
                        //parametersOfSp.Add(new KeyValuePair<string, string>("jobID", jobID));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("refrenceType", requestedBy));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("url", "@url"));
                        dt = model.selectIntoTable("select distinct 0 as fromTable, 0 as ApprovedCount, "
                        + " 0 as pendingApproveCount, r.rmUrl,max(rmid),DATE_FORMAT(max(rmAddedOn), '%d-%b-%Y'),"
                        + "max(CommunicationLevel) CommunicationLevel,max(AtitudeLevel) AtitudeLevel, "
                        +"max(TechnicalLevel) TechnicalLevel from RefrenceMaterials r WHERE refrenceType='U'  And addedBy ='" 
                        + forUser + "' group by rmid "
                       + " order by max(CommunicationLevel),max(AtitudeLevel) ,max(TechnicalLevel) " +
                       " ANd DATE_FORMAT(rmAddedOn, '%d-%b-%Y')>'" + FromDate + "'");


                    }

                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Found Skills", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Error ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream getUsersWithSkillVideos(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string skillList = null;
                    string FromDate = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skillList"))
                        {
                            skillList = list.Value;
                        }
                        if (list.Key.Equals("FromDate"))
                        {
                            FromDate = list.Value;
                        }

                    }

                    if (userID != 0 && skillList != null && skillList != "")
                    {
                      
                        dt = model.selectIntoTable("select distinct addedBy as uid,0 pendingRequests,U.userName displayName,userName,userContactNo,0 isBlocked,userType,userStatus,currentCompanyName,currentJobTitle,currentIndustry   from RefrenceMaterials R LEFT JOIN Users U on U.uid=R.addedBy WHERE refrenceType='U'  And  RefrenceRefID in (" + skillList + ")");// ANd DATE_FORMAT(rmAddedOn, '%d-%b-%Y')>'" + FromDate + "'");


                    }

                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Found Skills", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Not found ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getSkillsForUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string skill = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skill"))
                        {
                            skill = list.Value;
                        }

                    }

                    if (userID != 0)
                    {
                        //List<KeyValuePair<string, string>> parametersOfSp = new List<KeyValuePair<string, string>>();
                        //parametersOfSp.Add(new KeyValuePair<string, string>("jobID", jobID));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("refrenceType", requestedBy));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("url", "@url"));
                        dt = model.selectIntoTable("select luvid skillID,luvValueDisplayName as skillName   from ListofValues luv where luvCategory='skills'");

                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "found Skills", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "not found any Skill ", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream generateReport(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string emailaddress = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                            emailaddress = dt.Rows[0][0].ToString();
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string FromDate = null;
                    string forUser = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("FromDate"))
                        {
                            FromDate = list.Value;
                        }
                        if (list.Key.Equals("forUser"))
                        {
                            forUser = list.Value;
                        }


                    }

                    if (userID != 0)
                    {

                        dt = model.selectIntoTable("select Distinct(rmUrl) ReferenceMaterial,max(RMID) rmid,DATE_FORMAT(rmAddedOn, '%d-%b-%Y') as Date,'Gurgaon' Location,CommunicationLevel ,AtitudeLevel,TechnicalLevel from RefrenceMaterials WHERE refrenceType='U'  And addedBy ='" + forUser + "' group by rmUrl order by rmAddedOn desc");
                        ConvertIntoHTML CH = new ConvertIntoHTML();
                        List<string> listForColumn = new List<string>();
                        listForColumn.Add("Location");
                        listForColumn.Add("Date");
                        listForColumn.Add("ReferenceMaterial");

                        listForColumn.Add("CommunicationLevel");
                        listForColumn.Add("AtitudeLevel");
                        listForColumn.Add("TechnicalLevel");
                      
                        

                        string Html = CH.convertDataTableToHTMLTableNoLimit(dt, listForColumn, false);
                        Task.Factory.StartNew(() =>
                        {
                            EmailSend se = new EmailSend();
                            se.sendMailForGenerateReport(emailaddress, Html, "",FromDate,"Today");
                        });
                        response = new ResponseData(1, "Refrence material ", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());

                    }

                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Found Skills", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Error ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }


    public Stream refrenceAssesment(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string emailaddress = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                            emailaddress = dt.Rows[0][0].ToString();
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string CommunicationLevel = null;
                    string AtitudeLevel = null;

                    string TechnicalLevel = null;
                    string RefrenceId = null;

                   
                    string forUser = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("CommunicationLevel"))
                        {
                            CommunicationLevel = list.Value;
                        }
                        if (list.Key.Equals("AtitudeLevel"))
                        {
                            AtitudeLevel = list.Value;
                        }
                        if (list.Key.Equals("TechnicalLevel"))
                        {
                            TechnicalLevel = list.Value;
                        }
                        if (list.Key.Equals("RefrenceId"))
                        {
                            RefrenceId = list.Value;
                        }

                        if (list.Key.Equals("forUser"))
                        {
                            forUser = list.Value;
                        }


                    }

                    if (userID != 0 || RefrenceId !="")
                    {
                        //List<KeyValuePair<string, string>> parametersOfSp = new List<KeyValuePair<string, string>>();
                        //parametersOfSp.Add(new KeyValuePair<string, string>("jobID", jobID));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("refrenceType", requestedBy));
                        //parametersOfSp.Add(new KeyValuePair<string, string>("url", "@url"));

                        dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                        emailaddress = dt.Rows[0][0].ToString();

                        dt = model.insertIntoTableWithLastId("Insert into ReviewDetail (CommunicationLevel,AtitudeLevel,TechnicalLevel,RefrenceId,UserId,CreatedDate,ModifiedDate)Values ("+CommunicationLevel+","+AtitudeLevel+","+TechnicalLevel+","+RefrenceId+","+userID+",now(),now()) ", "RDId");

                        model.updateIntoTable("Update RefrenceMaterials RM Inner join (Select SUM(CommunicationLevel)/Count(*) as cl ,SUM(AtitudeLevel)/Count(*) as Al,SUM(TechnicalLevel)/Count(*) as tl,RefrenceId from ReviewDetail where RefrenceId=" + RefrenceId + " Group By RefrenceId) tod on tod.RefrenceId=RM.rmID Set CommunicationLevel =tod.cl ,AtitudeLevel=tod.Al,TechnicalLevel=tod.tl  where rmId=" + RefrenceId);
                        model.updateIntoTable("Update RefrenceMaterials RM  Inner join (select CommunicationLevel CL,AtitudeLevel AL,TechnicalLevel TL,rmUrl from RefrenceMaterials where rmID="+RefrenceId+") tod on tod.rmUrl=RM.rmUrl Set CommunicationLevel = tod.CL,AtitudeLevel=tod.AL,TechnicalLevel=tod.TL");

                        dt = model.selectIntoTable("select rmUrl as ReferenceMaterial,RMID as RDId,DATE_FORMAT(rmAddedOn, '%d-%b-%Y') as Date,'Gurgaon' Location,CommunicationLevel ,AtitudeLevel,TechnicalLevel from RefrenceMaterials WHERE refrenceType='U'  And rmID=" + RefrenceId);
                      
                        
                        ConvertIntoHTML CH = new ConvertIntoHTML();
                        List<string> listForColumn = new List<string>();
                        listForColumn.Add("Location");
                        listForColumn.Add("Date");
                        listForColumn.Add("ReferenceMaterial");

                        listForColumn.Add("CommunicationLevel");
                        listForColumn.Add("AtitudeLevel");
                        listForColumn.Add("TechnicalLevel");



                        string Html = CH.convertDataTableToHTMLTableNoLimit(dt, listForColumn, false);
                        Task.Factory.StartNew(() =>
                        {
                            EmailSend se = new EmailSend();
                            se.sendMailForGenerateReport(emailaddress, Html, "", " ", "Today");
                        });
                        
                        response = new ResponseData(1, "Refrence material ", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());

                    }

                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Found Skills", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Error ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "Invalid attempt", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream getAssesment(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string emailaddress = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                            emailaddress = dt.Rows[0][0].ToString();
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string CommunicationLevel = null;
                    string AtitudeLevel = null;

                    string TechnicalLevel = null;
                    string RefrenceId = null;


                    string forUser = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                       
                        if (list.Key.Equals("RefrenceId"))
                        {
                            RefrenceId = list.Value;
                        }

                       


                    }

                    if (userID != 0)
                    {
                        
                        dt=model.selectIntoTable("select  * from ReviewDetail RD inner join Users U on U.uid =RD.UserId where RD.RefrenceId ='"+RefrenceId+"'");
                        response = new ResponseData(1, "Refrence material ", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());

                    }

                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Found Skills", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Error ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream getRefrenceMaterialWIthRmId(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string emailaddress = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                            emailaddress = dt.Rows[0][0].ToString();
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                  
                    string RefrenceId = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType

                        if (list.Key.Equals("RefrenceId"))
                        {
                            RefrenceId = list.Value;
                        }




                    }

                    if (userID != 0)
                    {

                        dt = model.selectIntoTable("select  * from RefrenceMaterials where rmId ='" + RefrenceId + "'");
                        response = new ResponseData(1, "Refrence material ", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());

                    }

                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Found Skills", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Error ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }


    public Stream getMaterialToReview(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string emailaddress = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                            emailaddress = dt.Rows[0][0].ToString();
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string skillId = null;

                    string forId = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType

                        if (list.Key.Equals("skillId"))
                        {
                            skillId = list.Value;
                        }

                        if (list.Key.Equals("forId"))
                        {
                            forId = list.Value;
                        }

                    }

                    string DynamicWhereClauseForSkill = "";

                    string DynamicWhereClauseForForId = "";

                    if (skillId != "")
                    {
                        DynamicWhereClauseForSkill = " and RefrenceRefId in (" + skillId + ")";
                    }

                    if (  forId != ""){

                        DynamicWhereClauseForForId = " and addedBy in (" + forId + ")";
                    } 
                    
                    if (userID != 0)
                    {

                        dt = model.selectIntoTable("select distinct(rmUrl) ,max(RMID) rmid,concat(us.displayName,' ',DATE_FORMAT(rmAddedOn, '%d-%b-%Y')) as Date from RefrenceMaterials rm"
                            + " join Users us on rm.addedby=us.uid "
                            + " where rm.CommunicationLevel=0 and rm.TechnicalLevel=0 and rm.AtitudeLevel=0 " + DynamicWhereClauseForSkill + DynamicWhereClauseForForId + " Group BY rm.rmUrl  order by rm.rmAddedOn desc");
                        response = new ResponseData(1, "Refrence material ", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());

                    }

                    if (dt.Rows.Count != 0)
                    {
                           response = new ResponseData(1, "Found Skills", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(1, "Error ", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    private string getPasswordByRemoteLogin(string username, string password, string authSite)
    {
        string uid = null;
        string displayName = null;
        try
        {
            DataTable dt;
            dt = model.selectIntoTable("select userPassword,uid,displayName from Users where userName='" + username + "' and userType=1");

            if (dt.Rows.Count > 0)
            {
                uid = dt.Rows[0][1].ToString();
                displayName = dt.Rows[0][2].ToString();
                if (true)
                {
                    string result = dt.Columns[0].ToString() + GenerateNonce();
                    byte[] array = Encoding.ASCII.GetBytes(result + GenerateNonce());
                    string encoded = System.Convert.ToBase64String(array);
                    dt = model.selectIntoTable("select * from AuthenticationTokens where regUserid=" + uid + " and authSite='takemyjob'");
                    if (dt.Rows.Count != 0)
                    {
                        model.updateIntoTable("update AuthenticationTokens set lastModifiedDate=now(), token='" + encoded + "' where regUserid=" + uid + " and authSite='takemyjob'");
                        return "vallid&" + encoded + "&" + uid + "&" + displayName;
                    }
                    else
                    {
                        model.insertIntoTable("insert into AuthenticationTokens (lastModifiedDate,token,regUserid,authSite) values (now(),'" + encoded + "'," + uid + ",'takemyjob')");
                        return "vallid&" + encoded + "&" + uid + "&" + displayName; ;
                    }
                }
                return "wrongpwd&" + uid;
            }
            else
            {
                dt = model.selectIntoTable("select userPassword,uid from Users where userName='" + username + "'");
                if (dt.Rows.Count == 0)
                {
                    return "saveuser";
                }
                else
                {
                    return "wrongpwd&" + uid;
                }
            }
        }
        catch (Exception exc)
        {

            throw exc;
        }
    }


    public static string Base64Decode(string base64EncodedData)
    {
        var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }
    public Stream shareContent(string inputData)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string emailaddress = null;
        int userID = 0;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            dt = model.selectIntoTable("select userName from Users where uid = " + userID);
                            emailaddress = dt.Rows[0][0].ToString();
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Refrence Materials", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data"))
                {

                    list = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            string title = "";
            string verificationCode = "";
            string htmlcontent = "";
            string url = "";
            string mailto = "";
            foreach (var keyvalue in list)
            {
                if (keyvalue.Key.Equals("title"))
                {
                    title = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("url"))
                {
                    url = keyvalue.Value;
                }   
                if (keyvalue.Key.Equals("verificationCode"))
                {
                    verificationCode = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("htmlcontent"))
                {
                    htmlcontent = keyvalue.Value;
                    htmlcontent=Base64Decode(htmlcontent);

                    
                }
                if (keyvalue.Key.Equals("mailto"))
                {
                    mailto = keyvalue.Value;
                }
            }

            model.insertIntoTableWithLastId("insert into Job (postedBy,postedOnDateTime,jobReferenceLink,jobLatitude,jobLongitude" +
                ",jobLocationName,jobType,jobComments,jobExpiresOn," +
                "joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,jobVertical,"
                + "JobTitle,jobRole,jobSkills ) values ('" + userID +
                "',now(),'null','null','null','null',1,'null',DATE_ADD(now(),INTERVAL 30 DAY),now()," +
                "'1','1','2','1','" + title + "','" + title + " ','" + htmlcontent + "')", "jId");
            if (verificationCode != "") // validate verification code
            {
                dt = model.selectIntoTable(" select   userName from Users where verificationCode ='"
                    + verificationCode + "' and uid=" + userID + " and userstatus=1  ");
            }
            else { //send email to mailto address
                 Task.Factory.StartNew(() =>
                        {
                        EmailSend ES = new EmailSend();
                        ES.sendcontent(mailto, htmlcontent, url, title, verificationCode,"");
                        });
            
            }
            if (dt.Rows.Count != 0)
                {
                    dt = model.selectIntoTable(" select   userName,displayName from Users where  userstatus=1  ");
                    if (verificationCode != "") //send emails to all .
                    {
                        Task.Factory.StartNew(() =>
                        {
                            for (int rowindex = 0; rowindex < dt.Rows.Count; rowindex++)
                            {
                                string UserName = (dt.Rows[rowindex][0].ToString());
                                EmailSend ES = new EmailSend();
                                ES.sendcontent(UserName, htmlcontent, url, title, verificationCode,dt.Rows[rowindex][1].ToString());
                            }

                        });
                    }
                    else {
                        EmailSend ES = new EmailSend();
                        ES.sendcontent(mailto, htmlcontent, url, title, verificationCode,"");
                    }
                   

                   
                }
                response = new ResponseData(0, Constant.notFound, "[{\"token\":\"\" ,\"uid\":\"\" }]");
           
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            returnString = response.returnStatment();
            return returnStmnt(returnString);
        }
    }



    public Stream getTokenByRemoteLogin(string inputData)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {

                }
                if (objelement.Key.Equals("data"))
                {

                    list = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            string username = null;
            string password = null;
            string authSite = null;
            foreach (var keyvalue in list)
            {
                if (keyvalue.Key.Equals("userName"))
                {
                    username = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("userPassword"))
                {
                    password = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("authSite"))
                {
                    authSite = keyvalue.Value;
                }
            }
            string result = getPassword(username, password, authSite);
            string[] splitValuesFromGetgetPass = result.Split('&');
            if (splitValuesFromGetgetPass.Length == 1)
            {
                model.insertIntoTable("insert into Users ( userName,displayName,userType,userStatus,userPassword,modifiedBy,"
                 + "currentCompanyName,currentJobTitle,modifiedDateTime ) values" +
                 " ('" + username + "','" + null + "','" + "4'," + "'0','"
                 + dateTimeClass.datetime() + "','" + null + "','"
                 + null + "','" + null + "', now() )");
                dt = model.selectIntoTable("select uid userPassword from Users where userName='" + username + "'");
                if (dt.Rows.Count != 0)
                {
                    int uid = Convert.ToInt32(dt.Rows[0][0]);
                    byte[] array = Encoding.ASCII.GetBytes(uid.ToString() + dateTimeClass.datetime());
                    string encoded = System.Convert.ToBase64String(array);
                    model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,authSite) value (" + uid + ",'" + encoded + "','system')");
                    //Queue queue = new Queue();
                    //queue.queueRegistrationMail(uid, encoded);
                }
                response = new ResponseData(1, Constant.notvallid, "[{\"token\":\"\" ,\"uid\":\"\" }]");
            }
            else if (splitValuesFromGetgetPass[0].Equals("wrongpwd"))
            {
                response = new ResponseData(1, Constant.notvallid, "[{\"token\": \"\",\"uid\":\"" + splitValuesFromGetgetPass[1] + "\" }]");
            }
            else
            {
                response = new ResponseData(1, Constant.vallid, "[{\"token\":\"" + splitValuesFromGetgetPass[1] + "\",\"uid\":\"" + splitValuesFromGetgetPass[2] + "\",\"userType\":\"" + splitValuesFromGetgetPass[3] + "\",\"displayName\":\"" + splitValuesFromGetgetPass[3] + "\",\"isCustomerToBeCharged\":\"0\" }]");
            }
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            returnString = response.returnStatment();
            return returnStmnt(returnString);
        }
    }

     public Stream updateSkillsForUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string skills = "";
        string module = "";
       
         //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != -1)
                            {
                                auditInfo(inputData, 1, "Fetch Question For Test", userID);
                            }
                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skills"))
                        {
                            skills = list.Value;
                        }
                        if (list.Key.Equals("module"))
                        {
                            module = list.Value;
                        }
                       
                    }

                    var Skill = skills.Split(',');
                    
                    foreach (var skill in Skill)
                    {
                        dt = model.insertIntoTableWithLastId("insert into UserSkills  (uid,addedOn,skillUsedFrom,likeCount,skillRef) values (" + userID + ",now(),now(),0," + skill + ")", "LUSId");
                    }
                    var lastid = convertDataTableToJson(dt);
                    var dt1 = model.updateIntoTablewithRowCount("update Users set associatedModule=" + module + " where uid=" + userID + "");
                    
                    if (dt.Rows.Count > 0 && dt1 > 0)
                    {
                        response = new ResponseData(1, "Data uploaded successfully ", lastid);
                        return returnStmnt(response.returnStatment());
                    }
                   
                }


            }
            response = new ResponseData(1, "No Data found for given user ", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
     public Stream getmoduleReferences(string inputData)
     {
         List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
         Boolean flag = false;
         int userID = 0;
         string moduleId = "";

         //indicates data is from suggestionReferenceMaterials table
         try
         {
             DataTable dt = new DataTable();
             JObject jsonrow = JObject.Parse(inputData);
             foreach (var objelement in jsonrow)
             {
                 if (objelement.Key.Equals("meta"))
                 {
                     jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                     foreach (var keypair in jsonkeyvalue)
                     {
                         flag = true;
                         if (keypair.Key == "token")
                         {
                             //flag = vallidateToken(keypair.Value);

                             if (flag != true)
                             {
                                 response = new ResponseData(0, Constant.invallidtoken, null);
                                 return returnStmnt(response.returnStatment());
                             }
                         }
                         else if (keypair.Key.Equals("userID"))
                         {

                             if (keypair.Value == "")
                             {
                                 string message = "User does not exist";
                                 response = new ResponseData(0, message, null);
                                 return returnStmnt(response.returnStatment());

                             }
                             userID = Convert.ToInt32(keypair.Value);
                             if (userID != -1)
                             {
                                 auditInfo(inputData, 1, "Fetch Question For Test", userID);
                             }
                         }
                     }
                 }

                 if (objelement.Key.Equals("data") && flag == true)
                 {

                     //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                     jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                     foreach (var list in jsonkeyvalue)
                     {                          // sendBy,sendTo,Message,messageType
                         if (list.Key.Equals("moduleId"))
                         {
                             moduleId = list.Value;
                         }

                     }




                     dt = model.selectIntoTable("select * from moduleReferenceLink where moduleID="+ moduleId);
                     if (dt.Rows.Count > 0)
                     {
                         response = new ResponseData(1, "Found Data ", convertDataTableToJson(dt));
                         return returnStmnt(response.returnStatment());
                     }

                 }


             }
             response = new ResponseData(1, "No Data found for given user ", null);
             return returnStmnt(response.returnStatment());
         }
         catch (Exception ex)
         {
             response = new ResponseData(0, ex.Message, null);
             return returnStmnt(response.returnStatment());
         }
     }
     public Stream getModule(string inputData)
     {
         List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
         Boolean flag = false;
         int userID = 0;
        

         //indicates data is from suggestionReferenceMaterials table
         try
         {
             DataTable dt = new DataTable();
             JObject jsonrow = JObject.Parse(inputData);
             foreach (var objelement in jsonrow)
             {
                 if (objelement.Key.Equals("meta"))
                 {
                     jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                     foreach (var keypair in jsonkeyvalue)
                     {
                         flag = true;
                         if (keypair.Key == "token")
                         {
                             //flag = vallidateToken(keypair.Value);

                             if (flag != true)
                             {
                                 response = new ResponseData(0, Constant.invallidtoken, null);
                                 return returnStmnt(response.returnStatment());
                             }
                         }
                         else if (keypair.Key.Equals("userID"))
                         {

                             if (keypair.Value == "")
                             {
                                 string message = "User does not exist";
                                 response = new ResponseData(0, message, null);
                                 return returnStmnt(response.returnStatment());

                             }
                             userID = Convert.ToInt32(keypair.Value);
                             if (userID != -1)
                             {
                                 auditInfo(inputData, 1, "Fetch Question For Test", userID);
                             }
                         }
                     }
                 }

                 if (objelement.Key.Equals("data") && flag == true)
                 {

                     //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                     jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");








                     dt = model.selectIntoTable("select * from Modules where status='1' Order by moduleName");
                     

                     if (dt.Rows.Count > 0)
                     {
                         response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));
                         return returnStmnt(response.returnStatment());
                     }

                 }


             }
             response = new ResponseData(1, Constant.notFound, null);
             return returnStmnt(response.returnStatment());
         }
         catch (Exception ex)
         {
             response = new ResponseData(0, ex.Message, null);
             return returnStmnt(response.returnStatment());
         }
     }
    public Stream getDashBoard(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        String skills = "";

        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != -1)
                            {
                                auditInfo(inputData, 1, "Fetch Question For Test", userID);
                            }
                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skills"))
                        {
                            skills = list.Value;
                        }

                    }

                    var Skill = skills.Split(',');

                    

                    foreach (var skill in Skill)
                    {
                        callStoredProcedure sp = new callStoredProcedure();
                        dt= sp.callStoreProcedureForLeaderBoardScores(skill, userID);

                        
                    }
                     

                    if (dt.Rows.Count > 0)
                    {
                        response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));
                        

                        return returnStmnt(response.returnStatment());
                    }

                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getskillid(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        String skills = "";

        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != -1)
                            {
                                auditInfo(inputData, 1, "Fetch Question For Test", userID);
                            }
                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skills"))
                        {
                            skills = list.Value;
                        }

                    }

                    var Skill = skills.Split(',');
                   



                    dt = model.selectIntoTable("select luvid,luvValueDisplayName from ListofValues where luvValueDisplayName in ('"+ skills+"')");


                    if (dt.Rows.Count > 0)
                    {
                        response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));


                        return returnStmnt(response.returnStatment());
                    }

                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }



    //////////////////////////////////////


    public Stream assign(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        
        Boolean flag = false;
        int userID = 0;
        int projectId = 0;
        String startDate = "";
        int assignToProjectUID = 0;


        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "userID does not Provided !Unable To proceed..";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid  from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "userID does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                            }

                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";

                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var Keypair in jsonkeyvalue)
                    {                         // sendBy,sendTo,Message,messageType
                        if (Keypair.Key.Equals("assignToProjectUID"))
                        {

                            if (Keypair.Value == "")
                            {
                                string message = "Please provide 'assignToProjectUID' ";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                assignToProjectUID = Convert.ToInt32(Keypair.Value);
                                dt = model.selectIntoTable("select Uid, Pid  from ProjectUser where Status=1 and Uid='" + assignToProjectUID + "'");
                                if (dt.Rows.Count != 0)
                                {
                                    string message = "assignToProjectUID "+assignToProjectUID+" is assign to project id "+dt.Rows[0][1]+"  ";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }

                            }
                            

                        }





                      else  if (Keypair.Key.Equals("projectId"))
                        {
                            
                            if (Keypair.Value=="")
                            {
                                string message = "Please Provide projectId";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());


                            }
                            else
                            {
                                projectId = Convert.ToInt32(Keypair.Value);
                                dt = model.selectIntoTable("select  Pid  from Project where projectStatus=1 and pid='" + projectId + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "projectID "+ projectId + " does Not Exits!  ";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                   
                                   
                                }

                            }


                        }
                        else if (Keypair.Key.Equals("startDate"))
                        {

                            if (Keypair.Value == "")
                            {
                                string message = "Please Provide a start Date";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());
                                

                            }
                            else
                            {
                                 startDate = Keypair.Value.ToString();
                                

                                DateTime myDate = DateTime.ParseExact(startDate, "yyyy-MM-dd",
                                           System.Globalization.CultureInfo.InvariantCulture);
                                if (myDate >= DateTime.Today)
                                {
                                    dt = model.insertIntoTableWithLastId("insert into ProjectUser (" +
                                   "Uid,Pid,StartDate,Status,CreatedOn) values (" + assignToProjectUID + "," + projectId + ", str_to_date('" + startDate + "','%Y-%m-%d'),1,now())", "Puid");
                                    if (dt.Rows.Count != 0)
                                    {
                                        response = new ResponseData(1, "saved", convertDataTableToJson(dt));
                                        return returnStmnt(response.returnStatment());
                                    }

                                }
                                else
                                {
                                    response = new ResponseData(1, "Start Date Should be greater than current date", convertDataTableToJson(dt));
                                    return returnStmnt(response.returnStatment());
                                }

                                






                            }
                        }
                    }


                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }







         public Stream unAssign(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        int projectId = 0;
        String endDate = "";
        int assignToProjectUID = 0;

        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid userPassword from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "User does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                            }

                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    foreach (var keypair in jsonkeyvalue)
                    {                         // sendBy,sendTo,Message,messageType
                        if (keypair.Key.Equals("assignToProjectUID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "Please provide 'assignToProjectUID' ";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                assignToProjectUID = Convert.ToInt32(keypair.Value);


                            }
                        }






                        if (keypair.Key.Equals("projectId"))
                             {

                            if (keypair.Value == "")
                            {
                                string message = "project does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                projectId = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select pid  from Project where pid='" + projectId + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "project does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }

                            }

                        }
                        else if (keypair.Key.Equals("endDate"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "Please Provide a end Date";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                endDate = keypair.Value.ToString();


                                DateTime myDate = DateTime.ParseExact(endDate, "yyyy-MM-dd",
                                           System.Globalization.CultureInfo.InvariantCulture);
                                if (myDate >= DateTime.Today)
                                {
                                    int rows = model.updateIntoTablewithRowCount("update ProjectUser set EndDate=str_to_date('"+endDate+"','%Y-%m-%d') , Status=0 where Pid=" + projectId + " and Uid=" + assignToProjectUID);
                                    if (rows != 0)
                                    {
                                        response = new ResponseData(1, " updation has been done!", rows.ToString()
                                            );
                                        return returnStmnt(response.returnStatment());
                                    }
                                    else
                                    {
                                        response = new ResponseData(1, "" + projectId + " is not Assign for User " + userID + " ", null);
                                        return returnStmnt(response.returnStatment());
                                    }


                                }
                                else
                                {
                                    response = new ResponseData(1, "EndDate Should be today or greater Then Today ", null);
                                    return returnStmnt(response.returnStatment());

                                }


                            }
                        }
                    }


                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    
    }










    public Stream getTeamAssignProject(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        int projectId = 0;
        

        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid  from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "User does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                            }

                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    foreach (var keypair in jsonkeyvalue)
                    {                         // sendBy,sendTo,Message,messageType
                        

                        if (keypair.Key.Equals("projectId"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "project does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                projectId = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select pid  from Project where pid='" + projectId + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "project does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                                else
                                {
                                   dt= model.selectIntoTable("SELECT users.uid, users.username,users.displayName,users.currentJobTitle,users.userContactNo,date_format(prUId.StartDate,'%Y-%b-%d') as startDate,date_format(prUId.EndDate,'%Y-%b-%d') as endDate,prUId.Status FROM ProjectUser prUId"
                                       + " join Users users on prUId.Uid = users.uid where prUId.pid='" + projectId + "'");

                                    if (dt.Rows.Count != 0)
                                    {
                                        response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));
                                        
                                        return returnStmnt(response.returnStatment());
                                    }

                                }

                            }

                        }
                       
                    }


                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }







    public Stream getActiveProject(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        

        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid  from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "User does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                                else
                                {
                                    dt=model.selectIntoTable("SELECT pid,projectName,projectOwnerID,projectStatus,date_format(projectlastModifiedON,'%Y-%b-%d') as ModifiedOn,projectDuraionInDays,date_format(projectStartDate,'%Y-%b-%d') as StartDate,date_format(projectEndDate,'%Y-%b-%d') as endDate,round((projectEfforts/30),2) as 'projectEfforts',projectType FROM Project prUId where projectStatus = 1 order by createdOn desc");
                                    if (dt.Rows.Count != 0)
                                    {
                                        response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));

                                        return returnStmnt(response.returnStatment());
                                    }

                                }
                            }

                        }
                    }
                }

                


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }




    public Stream getAvailableResource(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        String skills;
        

        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid  from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "User does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                            }

                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    foreach (var keypair in jsonkeyvalue)
                    {                         // sendBy,sendTo,Message,messageType


                        if (keypair.Key.Equals("skills"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "Skill does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {

                                
                                skills = keypair.Value.Replace("\r\n","");
                                
                                skills = skills.Replace("[", "");
                                skills = skills.Replace("]", "").Replace(@"\", "");
                                skills = skills.Replace("\"", "'");

                                dt = model.selectIntoTable("select Users.uid,userName,Users.userContactNo,Users.displayName,Users.currentJobTitle,Users.currentCompanyName,skill.luvValueDisplayName from Users"
                                        + " join UserSkills us on Users.uid = us.sid"
                                        + " join ListofValues skill on us.skillRef = skill.luvid"
                                    + "  where skill.luvid in ("+skills+") and Users.uid not in (select Uid from ProjectUser where Status = 1) ;");

                                    if (dt.Rows.Count != 0)
                                    {
                                        response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));

                                        return returnStmnt(response.returnStatment());
                                    }

                               

                            }

                        }

                    }


                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }





    public Stream addProject(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        String projectName="";
        String startDate="";
        String endDate="";
        int projectType=0;
        int projectDuration=0;
        int projectEffort=0;
        int ownerId=0;
        int projectId = 0;
        String skills="";


        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid  from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "User does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                            }

                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    foreach (var keypair in jsonkeyvalue)
                    {                         // sendBy,sendTo,Message,messageType


                        if (keypair.Key.Equals("projectName"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "project Name does not provided";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                projectName = keypair.Value;

                            }
                            
                           

                        }



                        if (keypair.Key.Equals("startDate"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "Start Date does not provided";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                startDate = keypair.Value;

                            }



                        }



                        if (keypair.Key.Equals("endDate"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "endDate  does not provided";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                endDate = keypair.Value;
                            }



                        }





                        if (keypair.Key.Equals("projectType"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "projectType does not provided";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                projectType =int.Parse( keypair.Value);
                            }



                        }




                        if (keypair.Key.Equals("projectDuration"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "projectDuration does not provided";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                projectDuration = int.Parse(keypair.Value);

                            }



                        }


                        if (keypair.Key.Equals("ownerId"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "projectDuration does not provided";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                ownerId = int.Parse(keypair.Value);

                            }



                        }




                        if (keypair.Key.Equals("projectEffort"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "projectEffort does not provided";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                projectEffort =int.Parse( keypair.Value);
                                projectEffort = projectEffort * 30;

                                dt = model.insertIntoTableWithLastId("insert into Project (" +
                                  "projectName,projectStatus,projectOwnerID,projectlastModifiedON,projectDuraionInDays,projectStartDate,projectEndDate,projectType,projectEfforts,createdBy,createdOn"
                                 + " ) values ('" + projectName + "',1, " + ownerId + ",now(), "+ projectDuration + ", str_to_date('" + startDate + "','%Y-%m-%d'),str_to_date('" + endDate + "','%Y-%m-%d'),"
                                 +" "+projectType+ ","+projectEffort+",'"+userID+"',now())", "pid");
                                if (dt.Rows.Count != 0)
                                {
                                    projectId = int.Parse(dt.Rows[0][0].ToString());
                                      response = new ResponseData(1, "saved", convertDataTableToJson(dt));

                                    
                                }

                            }



                        }




                        if (keypair.Key.Equals("skills"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "skill does not provided";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                skills = keypair.Value.Replace("\r\n", "");

                                skills = skills.Replace("[", "");
                                skills = skills.Replace("]", "").Replace(@"\", "");
                                skills = skills.Replace("\"", "'");
                                string[] values = skills.Split(',');
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = values[i].Trim();
                                    dt = model.insertIntoTableWithLastId("insert into ProjectSkill (ProjectId,SkillRef) values ("+projectId+","+ values[i] + ")","id");
                                    response = new ResponseData(1, "saved", projectId.ToString());
                                    return returnStmnt(response.returnStatment());
                                }

                            }



                        }




                    }


                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }



    public Stream getUnassignedresourcesForProject(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        int projectId = 0;


        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid  from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "User does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                            }

                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    foreach (var keypair in jsonkeyvalue)
                    {                         // sendBy,sendTo,Message,messageType


                        if (keypair.Key.Equals("projectId"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "project does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                projectId = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select pid  from Project where pid='" + projectId + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "project does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                                else
                                {
                                    dt = model.selectIntoTable("select  us.uid, us.username,us.displayName,us.currentJobTitle,us.userContactNo,Project.pid,Project.ProjectName,prSkill.SkillRef,skill.luvValueDisplayName"
                                                              + "  "
                                                            +"    from ProjectSkill prSkill "
                                                             +"   join ListofValues skill on prSkill.SkillRef = skill.luvid "
                                                             +"   join Project on prSkill.ProjectId = Project.pid "
                                                             +"    "
                                                             + "   join (select distinct uid,skillRef from UserSkills) usSkill on prSkill.SkillRef = usSkill.skillRef "
                                                             + "    "
                                                             +"   join Users us on usSkill.uid = us.uid "
                                                              + "  where prSkill.ProjectId = " + projectId + " and us.uid not in (select Uid from ProjectUser where Status = 1);");

                                    if (dt.Rows.Count != 0)
                                    {
                                        response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));

                                        return returnStmnt(response.returnStatment());
                                    }

                                }

                            }

                        }

                    }


                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }



    public Stream getSkillMetrices(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;


        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid  from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "User does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                                else
                                {
                                    dt = model.selectIntoTable("SELECT skill.luvid ,skill.luvValueDisplayName,count( Users.uid) skillHolders FROM ListofValues skill "
                                                                +" join UserSkills uSkill on uSkill.skillRef = skill.luvid and skill.luvCategory = 'skills'"
                                                                 +" join Users on uSkill.uid = Users.uid"
                                                                +" group by luvid order by skillHolders desc limit 20");
                                    if (dt.Rows.Count != 0)
                                    {
                                        response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));

                                        return returnStmnt(response.returnStatment());
                                    }

                                }
                            }

                        }
                    }
                }




            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }



    public Stream getUserProjectDetails(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        
        int userIdForQuery = 0;


        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {
                                userID = Convert.ToInt32(keypair.Value);
                                dt = model.selectIntoTable("select uid  from Users where uid='" + userID + "'");
                                if (dt.Rows.Count == 0)
                                {
                                    string message = "User does not exist";
                                    response = new ResponseData(0, message, null);
                                    return returnStmnt(response.returnStatment());
                                }
                            }

                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    foreach (var keypair in jsonkeyvalue)
                    {                         // sendBy,sendTo,Message,messageType


                        if (keypair.Key.Equals("userIdForQuery"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "userIdForQuery does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            else
                            {


                                userIdForQuery = int.Parse(keypair.Value);

                               

                                dt = model.selectIntoTable("SELECT Uid,pro.Pid,pro.projectName,Date_Format(pro.projectStartDate,'%Y-%b-%d')"
                              +"  as ProjectStartDate, Date_Format(pro.projectEndDate, '%Y-%b-%d') as ProjectEndDate, Date_Format(PU.StartDate, '%Y-%b-%d') as AssignStartDate"
                               +" , ifnull(EndDate, Date_Format(now(), '%Y-%b-%d')) as AssignEndDate FROM ProjectUser PU"
                               +" join Project  pro on PU.Pid = pro.pid "
                                +"    where Uid = '"+ userIdForQuery + "' order by DATE(PU.EndDate) desc");

                                if (dt.Rows.Count != 0)
                                {
                                    response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));

                                    return returnStmnt(response.returnStatment());
                                }



                            }

                        }

                    }


                }


            }
            response = new ResponseData(1, Constant.notFound, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }



    public Stream getContentFromSP(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string skillId = "";
        string userid = "";
        string spName = "";
        string refrenceID = "";
        string projectId = "";

        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                string message = "User does not exist";
                                response = new ResponseData(0, message, null);
                                return returnStmnt(response.returnStatment());

                            }
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != -1)
                            {
                                auditInfo(inputData, 1, "Fetch Question For Test", userID);
                            }
                        }
                    }
                }

                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("skillId"))
                        {
                            skillId = list.Value;
                        }
                        if (list.Key.Equals("userId"))
                        {
                            userid = list.Value;
                        }
                        if (list.Key.Equals("refrenceId"))
                        {
                            refrenceID = list.Value;
                        }
                        if (list.Key.Equals("projectId"))
                        {
                            projectId = list.Value;

                        }
                        if (list.Key.Equals("spName"))
                        {
                            spName = list.Value;

                        }

                    }
                    List<KeyValuePair<string, string>> array = new List<KeyValuePair<string, string>>();
                    array.Add(new KeyValuePair<string, string>("userId", userid));
                    array.Add(new KeyValuePair<string, string>("skillId", skillId));
                    array.Add(new KeyValuePair<string, string>("refrenceId",refrenceID));
                    array.Add(new KeyValuePair<string, string>("projectId", projectId));
                  

                    dt = model.selectIntoTableUsingSP(spName, array);
                    if (dt.Rows.Count > 0)
                    {
                        response = new ResponseData(1, "Data uploaded successfully ",convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }

                }


            }
            response = new ResponseData(1, "No Data found for given user ", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }


    }


    public Stream getAttendenceDetails(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;


        //indicates data is from suggestionReferenceMaterials table
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            //flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {

                            if (keypair.Value == "")
                            {
                                dt = model.selectIntoTable("select count(s1_datetime) as s1Attendence, count(s2_datetime) as s2Attendence, count(s3_datetime) as s3Attendence, userId, count(case when onLeave = 1 then 1 else null end)"
                                + " as Onleave, count(case when s1_datetime is not null then 1 when s2_datetime is not null then 1 when s3_datetime is not null "
                                + "   then 1 else null end) as leastAvailable from attendance where crtdate between  "
                                + " date_format(concat(year(now()), '-', month(DATE_SUB(now(), INTERVAL 1 MONTH)), '-', 01, ' 00:00:00'), '%Y-%m-%d %H:%i:%s') and now() "
                                + " group by userId ");


                                if (dt.Rows.Count > 0)
                                {
                                    response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));


                                    return returnStmnt(response.returnStatment());
                                }

                            }
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != -1)
                            {
                                dt = model.selectIntoTable("select count(s1_datetime) as s1Attendence, count(s2_datetime) as s2Attendence, count(s3_datetime) as s3Attendence, userId, count(case when onLeave = 1 then 1 else null end)"
                                 + " as Onleave, count(case when s1_datetime is not null then 1 when s2_datetime is not null then 1 when s3_datetime is not null "
                                 + "   then 1 else null end) as leastAvailable from attendance where crtdate between  "
                                 + " date_format(concat(year(now()), '-', month(DATE_SUB(now(), INTERVAL 1 MONTH)), '-', 01, ' 00:00:00'), '%Y-%m-%d %H:%i:%s') and now() "
                                 + " and userId = " + userID + " group by userId ");


                                if (dt.Rows.Count > 0)
                                {
                                    response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));


                                    return returnStmnt(response.returnStatment());
                                }

                            }
                        }
                    }
                }



            }
            response = new ResponseData(1, "No Data found for given user ", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }

    public Stream enrollRefCandidate(string inputData)
    {
       
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        
        DataSet ds = new DataSet();
        string candidateEmailID = "";
        string candidateContactNo = "";
        string candidatePassword = "refpassword";
        string candidateCollegeName = "";
        string candidateCollegeURL = "";
        string candidateName = "";
        string refChannel = "";

        int userID = 0;
       
        string willTakeTrainingFromHome = "1", candidateYearOfPassingOut = "";
        string enrolledForPeriod = "";
        string enrollmentForProjectID = "";
        string candidateID = "";
        string candidateHomeAddress = "";
        string candidateSkills = "";


        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        flag = true;
                        if (keypair.Key == "token")
                        {
                            flag = true; // vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "Enroll Ref Candidate", userID);
                            }
                            else
                            {
                                response = new ResponseData(0, "Please login again to refer", null);
                                return returnStmnt(response.returnStatment());
                            }

                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {

                    //string finalResult = "[{\"testCategory\":\"{0}\",\"QuestionCategory\":\"{1}\",\"AnswerCategory\":\"{2}\"}]";
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    foreach (var list in jsonkeyvalue)
                    {

                        if (list.Key.Equals("refEmail"))
                        {
                            candidateEmailID = Convert.ToString(list.Value);
                            candidateName = candidateEmailID;
                        }
                        if (list.Key.Equals("refContactNo"))
                        {
                            candidateContactNo = Convert.ToString(list.Value);
                        }
                        if (list.Key.Equals("refChannel"))
                        {
                            refChannel = Convert.ToString(list.Value);
                        }


                    }

                    //if ((candidateName == "") || (candidateContactNo == "") || (candidateEmailID == "")  || (parentName == "") || (parentContactNo == "") || (parentEmailID == "")|| (mentorContactNo == "") || (mentorName == "") || (mentorEmailID == "") || (enrollmentForProjectID==""))
                    if ((candidateName == "") || (candidateContactNo == "") || (candidateEmailID == ""))
                    {
                        response = new ResponseData(0, "You have not provided all enrollment details", null);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        // Check if user is already enrolled 
                        dt = model.selectIntoTable("select uid as candidateID , eid as enrollmentID, enrollmentNo, enrolledOnDateTime from Users, Enrollment where userName='" + candidateEmailID + "' AND candidateID = uid");
                        if (dt.Rows.Count == 0)
                        {

                            dt = model.insertIntoTableWithLastId("insert into Users(userName,userContactNo,userType,userPassword,userStatus,modifiedBy,modifiedDateTime,createdBy,createdDateTime,registerationDate,currentJobTitle,currentCompanyName,currentIndustry,displayName,lattitude,longitude,lastCheckInLocation,lastLoginDateTime,userSkills)VALUES("
                                + "'" + candidateEmailID + "'," + "'" + candidateContactNo + "'," + "1," + "'" + EncryptBase64.EncodeTo64(candidatePassword) + "',1,'Fresher Villa',now()," + "'Fresher Villa',now(),now(),'" + candidateHomeAddress + "','" + candidateCollegeName + "','" + candidateCollegeURL + "','" + candidateName + "',null, null, null, now(), 'User Skills')", "candidateID");
                            var newUserID = dt.Rows[0][0];
                            response = new ResponseData(1, "Candidate Details Saved Successfully", convertDataTableToJson(dt));
                            dt = model.insertIntoTableWithLastId("INSERT INTO Enrollment ( candidateID, candidateName, enrollmentNo, parentID, parentName, mentorID, mentorName, enrolledOnDateTime, enrolledForPeriod, lastModificationDate, enrollmentStatus, enrollmentAgreementAcceptanceON, enrollmentExpiresOn, willTakeTrainingFromHome, candidateYearOfPassingOut) VALUES (" + newUserID + ", '" + candidateName + "', CONCAT('E/FV/',DATE_FORMAT(now(),'%y/%m%j%k%s')), " + newUserID + ", '" + candidateName + "', " + newUserID + ", '" + candidateName + "', now(), '180', now(), 0, now(), DATE_ADD(now(),INTERVAL 180 DAY) ,'" + willTakeTrainingFromHome + "', '" + candidateYearOfPassingOut + "')", "enrollmentID");
                            model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + userID + "','" + newUserID + "',now())");
                            EmailSend sendEmail = new EmailSend();
                            sendEmail.sendMailWithTemplateForEnroll("Congratulation for choosing Freshersvilla",
                                candidateName, candidateEmailID, candidatePassword, "EnrollPassword.html");
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Candidate Details already Saved", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                    }


                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message + ex.StackTrace, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream getProjectList(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string user = "false";
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Project List", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string project = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("project"))
                        {
                            project = list.Value;
                        }
                        if (list.Key.Equals("user"))
                        {
                            user = list.Value;
                        }

                    }

                    if (userID != 0)
                    {
                        if (user == "true")
                        {
                            if (project == null || project == String.Empty)
                            {
                                dt = model.selectIntoTable("select jb.jid as JobId,pr.pid, pr.projectName, pr.projectStatus, pr.projectOwnerID, pr.projectlastModifiedON,pr.projectDuraionInDays,"
                                                            + " pr.projectStartDate, pr.projectEndDate from Project pr join Job jb on jb.projectId=pr.pid join JobApplications ja on ja.jobRefid=jb.jid  "
                                                            + " where ja.appliedByUser=" + userID + " order by pr.createdOn desc ");

                            }
                            else
                            {
                                dt = model.selectIntoTable("select jb.jid as JobId,pr.pid, pr.projectName, pr.projectStatus, pr.projectOwnerID, pr.projectlastModifiedON,pr.projectDuraionInDays,"
                                                            + " pr.projectStartDate, pr.projectEndDate from Project pr join Job jb on jb.projectId=pr.pid join JobApplications ja on ja.jobRefid=jb.jid  "
                                                            + " where ja.appliedByUser=" + userID + "and pr.projectName like '" + project + "%'  order by pr.createdOn desc");

                            }

                        }
                        else
                        {
                            if (project == null || project == String.Empty)
                            {
                                dt = model.selectIntoTable("select pid, projectName, projectStatus, projectOwnerID, projectlastModifiedON,"
                                                            + " projectDuraionInDays, projectStartDate, projectEndDate from Project ");

                            }
                            else
                            {
                                dt = model.selectIntoTable("select pid, projectName, projectStatus, projectOwnerID, projectlastModifiedON,"
                                                            + " projectDuraionInDays, projectStartDate, projectEndDate from Project where projectName like  '" + project + "%'");

                            }
                        }
                      
                        



                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "found Project", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(0, "not found any Project ", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }

    public Stream getVideosAssProject(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string user = "false";
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Videos List Associated to Subject", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    int projectId = 0;
                    bool lbSuccess=false;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("projectId"))
                        {
                            lbSuccess = int.TryParse(list.Value, out projectId);
                            if (lbSuccess == true)
                            {
                                lbSuccess = false;
                            }
                            else
                            {
                                response = new ResponseData(0, "Can't parse Project Id", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        if (list.Key.Equals("user"))
                        {
                            user = list.Value;
                        }

                    }

                    if (projectId > 0 && userID != 0)
                    {
                        if(user=="true")
                        {
                            dt = model.selectIntoTable("select rm.rmID,rm.rmUrl,rm.addedby,date_format(rm.rmAddedOn,'%d-%b-%Y') from RefrenceMaterials rm "
                                                   + " join RefrenceLink rl on rm.rmID = rl.refrenceID join Job jb on rl.jobID = jb.jid where jb.projectId = " + projectId + " and rm.addedby=" + userID);



                            if (dt.Rows.Count != 0)
                            {
                                response = new ResponseData(1, "found Video", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                            else
                            {
                                response = new ResponseData(0, "not found any Video ", null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else
                        {
                            dt = model.selectIntoTable("select rm.rmID,rm.rmUrl,rm.addedby,date_format(rm.rmAddedOn,'%d-%b-%Y') from RefrenceMaterials rm "
                                                   + " join RefrenceLink rl on rm.rmID = rl.refrenceID join Job jb on rl.jobID = jb.jid where jb.projectId = " + projectId);



                            if (dt.Rows.Count != 0)
                            {
                                response = new ResponseData(1, "found Video", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                            else
                            {
                                response = new ResponseData(0, "not found any Video ", null);
                                return returnStmnt(response.returnStatment());
                            }
                        }

                       
                    }
                    else
                    {
                            response = new ResponseData(0, "Can't found userId ", null);
                            return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }


    public Stream getMentorCandidateAttendence(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string month = "";
        bool lbSuccess = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Videos List Associated to Subject", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    int year = 0;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("monthName"))
                        {
                            month = list.Value;
                        }
                        if (list.Key.Equals("year"))
                        {
                            lbSuccess = int.TryParse(list.Value, out year);
                            if (lbSuccess == true)
                            {
                                lbSuccess = false;
                            }
                            else
                            {
                                response = new ResponseData(0, "Can't parse Year", null);
                                return returnStmnt(response.returnStatment());
                            }
                        }

                    }

                    callStoredProcedure sp = new callStoredProcedure();
                    dt = sp.callStoreProcedureGetMentorCandidateAttendence(month, year);

                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(1, "Attendence List", convertDataTableToJson(dt));
                    }
                    else
                    {
                        response = new ResponseData(0, "No List Found", null);
                    }


                }
            }
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }



    public Stream getProjectDetails(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        bool lbSuccess = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "get Project Details", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    int projectId = 0;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("projectId"))
                        {
                            lbSuccess = int.TryParse(list.Value, out projectId);
                            if (lbSuccess == true)
                            {
                                lbSuccess = false;
                            }
                            else
                            {
                                response = new ResponseData(0, "Can't parse Project Id", null);
                                return returnStmnt(response.returnStatment());
                            }
                        }

                    }

                    if (projectId > 0 && userID > 0)
                    {

                        dt = model.selectIntoTable(" select jb.jid as JobId,pr.pid as ProjectId,pr.projectName as ProjectName,pr.projectStartDate as PrjctStrtDate,pr.projectEndDate as ProjectEndDate, "
                                                    + " rmUrl as VideoUrl from Project pr join Job jb on pr.pid=jb.projectId join RefrenceLink rl on rl.jobID=jb.jid join RefrenceMaterials rm "
                                                   + " on rm.rmID=rl.refrenceID where jb.projectId=" + projectId);


                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Project List", convertDataTableToJson(dt));
                        }
                        else
                        {
                            response = new ResponseData(0, "No List Found", null);
                        }
                    }
                    else
                    {
                        response = new ResponseData(0, "User Id is not Valid", null);
                    }



                }
            }
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }

    public Stream FacebookApi(string inputData)
    {
        try
        {
            
            MessengerAPI msg = new MessengerAPI("EAAGSvzQIHoIBAPVCFEuEomGyZCuTFUzS0dJVSgpq9jrLMSFKH8Tg2ZBkfYcguYxuGKIseFDwl3ETKwWzkLZC5upYNiITSLSnHjfBptkPHzsWcmZCN81VnReVI05wRmXdsyZAdPk5Xjn9wRwiz8fScLnP7fT9lIOzxabcfQDcxMAZDZD");
            msg.SendTextMessageAsync(660746514005471, "Hello");
            response = new ResponseData(0, "Hello", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }



    }


   
}

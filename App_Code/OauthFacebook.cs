﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Collections.Specialized;
using OAuth4Client.OAuth;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Data;
namespace csimplifyit

{
    public static class Facebook
    {
        public static string ConsumerKey = "199574140186767";
        public static string ConsumerSecret = "b0e77da57c4d5d8027b27d6a3862860f";
        public static string VerifierUrl = "https://graph.facebook.com/oauth/authorize?client_id={0}&redirect_uri={1}&scope={2}";
        public static string RequestAccessTokenUrl = "https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}";
        public static string RequestProfileUrl = "https://graph.facebook.com/me/friends";
        public static string Scope = "publish_stream,read_stream,email,offline_access";
    }
    public class OAuthFacebook
    {
        OAuthContext OContext = null;
        public string getAllOauthconnection()
        {
            connectToOauthServer();
            return OContext.GetProfileResponse("https://graph.facebook.com/me/friends");        
        }
        public void connectToOauthServer()
        {
            OContext = new OAuthContext("facebook")
            {
                ConsumerKey = Facebook.ConsumerKey,
                ConsumerSecret = Facebook.ConsumerSecret,
                VerifierUrl = Facebook.VerifierUrl,
                RequestAccessTokenUrl = Facebook.RequestAccessTokenUrl,
                RequestProfileUrl = Facebook.RequestProfileUrl,
                Scope = Facebook.Scope,
                OAuthVersion = OAuthVersion.V2,
                SocialSiteName = "Facebook"
            };
            HttpContext.Current.Response.Redirect(OContext.ObtainVerifier());
              
        }
        private string connectAfterReg(string email)
        {
            OContext = new OAuthContext("facebook")
            {
                ConsumerKey = Facebook.ConsumerKey,
                ConsumerSecret = Facebook.ConsumerSecret,
                VerifierUrl = Facebook.VerifierUrl,
                RequestAccessTokenUrl = Facebook.RequestAccessTokenUrl,
                RequestProfileUrl = Facebook.RequestProfileUrl,
                Scope = Facebook.Scope,
                OAuthVersion = OAuthVersion.V2,
                SocialSiteName = "Facebook"
            };
            string query = "select fbtoken,fbcode from OauthData where emailaddress='" + email + "';";
            string connection = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            MySqlConnection con = new MySqlConnection(connection);
            try
            {
                MySqlCommand command = new MySqlCommand(query, con);
                con.Open();
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();
                if (reader.HasRows)
                {
                    IDataRecord record = (IDataReader)reader;
                    OContext.Code = record[1].ToString();
                    OContext.AccessToken = record[0].ToString();
                }
                else {
                    return "false";
                }
            }
            catch (Exception exc) {
                return "false";
            }
            return "true";

        }
        public string updateOnOauthServer(string postdata,string email){
          string status=  connectAfterReg(email);
          if (status == "false") {
              return "false";
          }
            OContext.postStatus(postdata);
            return "true";
            
        }
        public void sendRequestToLinkedin() { }
        public void sendRequestToTwitter() { }
        public string getProfile() {
            return OContext.GetProfileResponse("https://graph.facebook.com/me");
        }
    }

}

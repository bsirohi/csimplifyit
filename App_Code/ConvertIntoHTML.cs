﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Threading.Tasks;


/// <summary>
/// Summary description for ConvertIntoHTML
/// </summary>
public class ConvertIntoHTML
{
	public ConvertIntoHTML()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string convertDataTableToHTMLTable(DataTable dt, List<string> list,bool isConvertAllItem)
    {
        {
            int mincount = 0;
            int maxcount = 0;
            int countForIndexOfTable=0;
            Random rad = new Random();
            maxcount=rad.Next(dt.Rows.Count);
            if (maxcount < 3) {
                maxcount = 4;
            }
            mincount = maxcount - 4;
            if (dt.Rows.Count == 0)
                return "";

            StringBuilder builder = new StringBuilder();
            builder.Append("<html>");
            builder.Append("<head>");
            builder.Append("<title>");
            builder.Append("Page-");
            builder.Append(Guid.NewGuid().ToString());
            builder.Append("</title>");
            builder.Append("</head>");
            builder.Append("<body>");
            builder.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            builder.Append("style='border: solid 1px Silver; font-size: x-small;'>");
            builder.Append("<tr align='left' valign='top'>");
            foreach (DataColumn c in dt.Columns)
            {
                if (list.Contains(c.ColumnName) || isConvertAllItem)
                {
                    builder.Append("<td align='left' valign='top'><b>");
                    builder.Append(c.ColumnName);
                    builder.Append("</b></td>");
                }
            }
            builder.Append("</tr>");
            foreach (DataRow r in dt.Rows)
            {
                builder.Append("<tr align='left' valign='top'>");
                foreach (DataColumn c in dt.Columns)
                {
                    if ((list.Contains(c.ColumnName) || isConvertAllItem ) && (countForIndexOfTable>=mincount && countForIndexOfTable<maxcount))
                    {
                        builder.Append("<td align='left' valign='top'>");
                        builder.Append(r[c.ColumnName]);
                        builder.Append("</td>");
                    }
                }
                countForIndexOfTable++;
                builder.Append("</tr>");

            }
            builder.Append("</table>");
            builder.Append("</body>");
            builder.Append("</html>");

            return builder.ToString();
        }
    }

    public string convertDataTableToHTMLTableNoLimit(DataTable dt, List<string> list, bool isConvertAllItem)
    {
        {
            int mincount = 0;
            int maxcount = 0;
            int countForIndexOfTable = 0;
            Random rad = new Random();
            maxcount = dt.Rows.Count;

            mincount = 0;
            if (dt.Rows.Count == 0)
                return "";

            StringBuilder builder = new StringBuilder();
            builder.Append("<html>");
            builder.Append("<head>");
            builder.Append("<title>");
            builder.Append("Page-");
            builder.Append(Guid.NewGuid().ToString());
            builder.Append("</title>");
            builder.Append("</head>");
            builder.Append("<body>");
            builder.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            builder.Append("style='border: solid 1px Silver; font-size: x-small;'>");
            builder.Append("<tr align='left' valign='top'>");
            foreach (DataColumn c in dt.Columns)
            {
                if (list.Contains(c.ColumnName) || isConvertAllItem)
                {
                    builder.Append("<td align='left' valign='top'><b>");
                    builder.Append(c.ColumnName);
                    builder.Append("</b></td>");
                }
            }
            builder.Append("</tr>");
            foreach (DataRow r in dt.Rows)
            {
                builder.Append("<tr align='left' valign='top'>");
                foreach (DataColumn c in dt.Columns)
                {
                    if ((list.Contains(c.ColumnName) || isConvertAllItem) && (countForIndexOfTable >= mincount && countForIndexOfTable < maxcount))
                    {
                        builder.Append("<td align='left' valign='top'>");
                        builder.Append(r[c.ColumnName]);
                        builder.Append("</td>");
                    }
                }
                countForIndexOfTable++;
                builder.Append("</tr>");

            }
            builder.Append("</table>");
            builder.Append("</body>");
            builder.Append("</html>");

            return builder.ToString();
        }
    }


}
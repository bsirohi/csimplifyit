﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Collections.Specialized;
using System.Web;
using OAuth4Client.OAuth;

namespace csimplifyit
{
    public static class Google
    {
        public static string ConsumerKey = "238111495485.apps.googleusercontent.com";
        public static string ConsumerSecret = "zH3ZnFAh586lGt75Tpl0NO_m";
        public static string RequestTokenUrl = "https://www.google.com/accounts/OAuthGetRequestToken?scope=http://www-opensocial.googleusercontent.com/api/people/";
        public static string VerifierUrl = "https://www.google.com/accounts/OAuthAuthorizeToken";
        public static string RequestAccessTokenUrl = "https://www.google.com/accounts/OAuthGetAccessToken";
        public static string RequestProfileUrl = "https://www.googleapis.com/oauth2/v1/userinfo";
        public static string Scope = "http://www-opensocial.googleusercontent.com/api/people/  https://www.google.com/m8/feeds";
    }
    public class OAuthGoogle
    {
        public void BeginAuthentication()
        {
           
            var OContext = new OAuthContext("google")
            {
                ConsumerKey = Google.ConsumerKey,
                ConsumerSecret =Google.ConsumerSecret,
                RequestTokenUrl =Google.RequestTokenUrl,
                VerifierUrl =Google.VerifierUrl,
                RequestAccessTokenUrl =Google.RequestAccessTokenUrl,
                RequestProfileUrl = Google.RequestProfileUrl,
                OAuthVersion = OAuthVersion.V1,
                SocialSiteName = "Google"
            };
            OContext.GetRequestToken();
            
            HttpContext.Current.Response.Redirect(OContext.ObtainVerifier() + "&oauth_callback=http://localhost:49399/WebSite4/Default.aspx");
            
            
            //OContext.Verifier = aw._verifier;
            //OContext.GetAccessToken();
            //OContext.GetProfileResponse();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections;
using csimplifyit;


  public  class oAuthYahoo:oAuthBase
    {
        public string ConsumerKey
        {
            get
            {
                return "dj0yJmk9NkdMTUUxRDdJdFo4JmQ9WVdrOWJuVmtaV2xvTXpZbWNHbzlORE0zTURFME5UWXkmcz1jb25zdW1lcnNlY3JldCZ4PTgx";
            }
        }
        public string ConsumerSecret
        {
            get
            {
                return "db245e94a07b2d2138fd22710047f82fda4642f5";
            }
        }
        public  string Token = "";
        public string TokenSecret = "";
        private string accesstoken = "";
        private string accesssecrettoken = "";
        public enum Method { GET, POST, PUT, DELETE };
        public string OauthSessionHandle = "ACgCClFVQy5nSFArZLe8dZMAKsbaqrlwb66N6ve34GFJVhxVY1o-";
        public string OauthYahooGuid = "2WOFPLAUL4E7LHLI22EVFJVZUA"; 
        public string GetRequestToken()
        {
            string authorizationUrl = string.Empty;
            oAuthBase oauth = new oAuthBase();

            Uri uri = new Uri("https://api.login.yahoo.com/oauth/v2/get_request_token");
            string nonce = oauth.GenerateNonce();
            string timeStamp = oauth.GenerateTimeStamp();
            string normalizedUrl;
            string normalizedRequestParameters;
            string sig = oauth.GenerateSignature(uri, ConsumerKey, ConsumerSecret, string.Empty, string.Empty, "GET", timeStamp, nonce, "https://www.google.co.in/", oAuthBase.SignatureTypes.PLAINTEXT, out normalizedUrl, out normalizedRequestParameters); //OAuthBase.SignatureTypes.HMACSHA1
            StringBuilder sbRequestToken = new StringBuilder(uri.ToString());
            sbRequestToken.AppendFormat("?oauth_nonce={0}&", nonce);
            sbRequestToken.AppendFormat("oauth_timestamp={0}&", timeStamp);
            sbRequestToken.AppendFormat("oauth_consumer_key={0}&", ConsumerKey);
            sbRequestToken.AppendFormat("oauth_signature_method={0}&", "PLAINTEXT"); //HMAC-SHA1
            sbRequestToken.AppendFormat("oauth_signature={0}&", sig);
            sbRequestToken.AppendFormat("oauth_version={0}&", "1.0");
            sbRequestToken.AppendFormat("oauth_callback={0}", HttpUtility.UrlEncode("http://developer.csimplifyit.com/developer/signup.aspx"));
            //Response.Write(sbRequestToken.ToString());
            //Response.End();
            try
            {
                string returnStr = string.Empty;
                string[] returnData;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sbRequestToken.ToString());
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader streamReader = new StreamReader(res.GetResponseStream());
                returnStr = streamReader.ReadToEnd();
                returnData = returnStr.Split(new Char[] { '&' });
                //Response.Write(returnStr);

                int index;
                if (returnData.Length > 0)
                {
                    index = returnData[0].IndexOf("=");
                    string oauth_token = returnData[0].Substring(index + 1);
                    Token = oauth_token;

                    index = returnData[1].IndexOf("=");
                    string oauth_token_secret = returnData[1].Substring(index + 1);
                    TokenSecret = oauth_token_secret;

                    //index = returnData[2].IndexOf("=");
                    //int oauth_expires_in;
                    //Int32.TryParse(returnData[2].Substring(index + 1), out oauth_expires_in);
                    //Session["Oauth_Expires_In"] = oauth_expires_in;

                    index = returnData[3].IndexOf("=");
                    string oauth_request_auth_url = returnData[3].Substring(index + 1);
                    authorizationUrl = HttpUtility.UrlDecode(oauth_request_auth_url);
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return authorizationUrl;
        }
        public void AccessTokenGet()
        {
             oAuthBase oauth = new oAuthBase();

            Uri uri = new Uri("https://api.login.yahoo.com/oauth/v2/get_token");
            string nonce = oauth.GenerateNonce();
            string timeStamp = oauth.GenerateTimeStamp();
            string sig = ConsumerSecret + "%26" + TokenSecret;

            StringBuilder sbAccessToken = new StringBuilder(uri.ToString());
            sbAccessToken.AppendFormat("?oauth_consumer_key={0}&", ConsumerKey);
            sbAccessToken.AppendFormat("oauth_signature_method={0}&", "PLAINTEXT"); //HMAC-SHA1
            sbAccessToken.AppendFormat("oauth_signature={0}&", sig);
            sbAccessToken.AppendFormat("oauth_timestamp={0}&", timeStamp);
            sbAccessToken.AppendFormat("oauth_version={0}&", "1.0");
            sbAccessToken.AppendFormat("oauth_token={0}&", Token);
            sbAccessToken.AppendFormat("oauth_nonce={0}&", nonce);
            sbAccessToken.AppendFormat("oauth_verifier={0}",Verifier);
            //Response.Write(sbAccessToken.ToString());
            //Response.End();

            try
            {
                string returnStr = string.Empty;
                string[] returnData;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sbAccessToken.ToString());
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader streamReader = new StreamReader(res.GetResponseStream());
                returnStr = streamReader.ReadToEnd();
                returnData = returnStr.Split(new Char[] { '&' });
                //Response.Write(returnStr);
                //Response.End();

                int index;
                if (returnData.Length > 0)
                {
                    index = returnData[0].IndexOf("=");
                    accesstoken = returnData[0].Substring(index + 1);

                    index = returnData[1].IndexOf("=");
                    string oauth_token_secret = returnData[1].Substring(index + 1);
                   accesssecrettoken = oauth_token_secret;

                    //index = returnData[2].IndexOf("=");
                    //int oauth_expires_in;
                    //Int32.TryParse(returnData[2].Substring(index + 1), out oauth_expires_in);

                    index = returnData[3].IndexOf("=");
                    string oauth_session_handle = returnData[3].Substring(index + 1);
                    OauthSessionHandle = oauth_session_handle;

                    //index = returnData[4].IndexOf("=");
                    //int oauth_authorization_expires_in;
                    //Int32.TryParse(returnData[4].Substring(index + 1), out oauth_authorization_expires_in);

                    index = returnData[5].IndexOf("=");
                    string xoauth_yahoo_guid = returnData[5].Substring(index + 1);
                    OauthYahooGuid = xoauth_yahoo_guid;
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void connectToOauthServer(string authlink)
        {
            try
            {

                HttpContext.Current.Response.Redirect(authlink);

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        public string getAllOauthconnection(string url)
        {
            oAuthBase oauth = new oAuthBase();
              //this.Verifier = "67kvdd";
           //  connectToOauthServer();
           
            Uri uri = new Uri(string.Format(url,OauthYahooGuid));
            string nonce = oauth.GenerateNonce();
            string timeStamp = oauth.GenerateTimeStamp();
            string normalizedUrl;
            string normalizedRequestParameters;
            string sig = oauth.GenerateSignature(uri, ConsumerKey, ConsumerSecret, accesstoken, accesssecrettoken, "GET", timeStamp, nonce,null, oAuthBase.SignatureTypes.HMACSHA1, out normalizedUrl, out normalizedRequestParameters);

            StringBuilder sbGetContacts = new StringBuilder(uri.ToString());
            try
            {
                string returnStr = string.Empty;
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(sbGetContacts.ToString());
                req.Method = "GET";
                string authHeader = "Authorization: OAuth " +
                "realm=\"yahooapis.com\"" +
                ",oauth_consumer_key=\"" + ConsumerKey + "\"" +
                ",oauth_nonce=\"" + nonce + "\"" +
                ",oauth_signature_method=\"HMAC-SHA1\"" +
                ",oauth_timestamp=\"" + timeStamp + "\"" +
                ",oauth_token=\"" + accesstoken + "\"" +
                ",oauth_version=\"1.0\"" +
                ",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\"";

                //Response.Write("</br>Headers: " + authHeader);

                req.Headers.Add(authHeader);
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                StreamReader streamReader = new StreamReader(res.GetResponseStream());
                returnStr = streamReader.ReadToEnd();
                return returnStr;
            }
            #region error
            catch (WebException ex)
            {
                //Response.Write(ex.Message);

                return ex.Message;
            }
            #endregion error    
        }
        public void disconnectToOauthServer()
        {
            Token = null;
            TokenSecret = null;
        }
        public void updateAllOnOauthServer(string status) {
            oAuthBase oauth = new oAuthBase();
            //this.Verifier = "67kvdd";
          //  connectToOauthServer();
            Uri uri = new Uri("http://social.yahooapis.com/v1/user/" + OauthYahooGuid + "/profile/status?format=xml");
            string nonce = oauth.GenerateNonce();
            string timeStamp = oauth.GenerateTimeStamp();
            string normalizedUrl;
            string normalizedRequestParameters;
            string sig = oauth.GenerateSignature(uri, ConsumerKey, ConsumerSecret, accesstoken, accesssecrettoken, "PUT", timeStamp, nonce, null, oAuthBase.SignatureTypes.HMACSHA1, out normalizedUrl, out normalizedRequestParameters);
            StringBuilder sbGetContacts = new StringBuilder(uri.ToString());
            try
            {
                string returnStr = string.Empty;
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(sbGetContacts.ToString());
                webRequest.Method = "PUT";
                webRequest.ContentType = "application/xml;charset=utf-8";
                string authHeader = "Authorization: OAuth " +
                "realm=\"yahooapis.com\"" +
                ",oauth_consumer_key=\"" + ConsumerKey + "\"" +
                ",oauth_nonce=\"" + nonce + "\"" +
                ",oauth_signature_method=\"HMAC-SHA1\"" +
                ",oauth_timestamp=\"" + timeStamp + "\"" +
                ",oauth_token=\"" + accesstoken + "\"" +
                ",oauth_version=\"1.0\"" +
                ",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\"";
              //  string poststatus = "{\"status\": {  \"message\": \""+ status+"\"   }  }";  
                string poststatus = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+" <status>  <message>new status</message>  </status>";  
                //Response.Write("</br>Headers: " + authHeader);
                webRequest.Headers.Add(authHeader);
                if (poststatus != null)
                {
                    byte[] fileToSend = Encoding.UTF8.GetBytes(status);
                    webRequest.ContentLength = fileToSend.Length;
                    Stream reqStream = webRequest.GetRequestStream();
                    reqStream.Write(fileToSend, 0, fileToSend.Length);
                    reqStream.Close();
                }
                try
                {
                    var response = webRequest.GetResponse();
                    using (var responseStream = response.GetResponseStream())
                    {
                        var reader = new StreamReader(responseStream);
                       string responseText = reader.ReadToEnd();
                        reader.Close();

                    }
                }
                catch (WebException we)
                {
                    var errrorResponse = we.Response as HttpWebResponse;
                    try
                    {
                        var sr = new StreamReader(errrorResponse.GetResponseStream());
                        string responseData = sr.ReadToEnd();
                        sr.Close();
                    }
                    catch (Exception exc) { Console.WriteLine(exc.Message); }

                }
           
            }
            #region error
            catch (WebException we)
            {
                //Response.Write(ex.Message);
                var errrorResponse = we.Response as HttpWebResponse;
                try
                {
                    var sr = new StreamReader(errrorResponse.GetResponseStream());
                    string responseData = sr.ReadToEnd();
                    sr.Close();
                }
                catch (Exception exc) { Console.WriteLine(exc.Message); }
            }
            #endregion error  
        }
}

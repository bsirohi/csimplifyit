﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ItakeMyJob" in both code and config file together.
[ServiceContract]
public interface ItakeMyJob
{
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getToken/", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json),]
    Stream getToken(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getRefrenceFriends/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getRefrenceFriend(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/postJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream postJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/applyJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream applyForJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getLov/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getLov(string inputData);
    /* no result found is on description if no data found and status success */
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getJobs/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getJobs(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/knowMyFriends/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream knowMyFriends(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/blockUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream blockUser(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/unblockUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream unBlockUser(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/takemyJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream takemyJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/optOut/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream optOut(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/payments/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream payments(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/rewards/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getcurrentRewards(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/contentUpload/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream contentUpload(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getCDNUrl/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getCDNUrl(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getInterestedFriendsInmyjobs/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getInterestedFriendsInmyjobs(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getResumesForJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getResumesForJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getMessagesFromFriend/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getMessagesFromFriend(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/sendMessagetoFriend/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream sendMessagetoFriend(string inputData);
    [OperationContract]
    [WebInvoke(Method = "GET", UriTemplate = "/test/", ResponseFormat = WebMessageFormat.Json)]
    string testfunc();
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/postCommentOnYourSocialWall/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream postCommentOnYourSocialWall(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/updateCommentOnJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream updateCommentOnJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/forgotPassword/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream forgotPassword(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getJobApplications/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getJobApplications(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/revokeJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream revokeJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/revokeJobApplication/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream revokeJobApplication(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/introduceFriends/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream introduceFriends(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getPendingRequests/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getPendingRequests(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/introductionUpdate/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream introductionUpdate(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getJobDetail/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getJobDetail(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getFriendDetail/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getFriendDetail(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/updateJobApplicationComments/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream updateJobApplicationComments(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getJobApplicationComments/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getJobApplicationComments(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getTokenByUserID/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getTokenByUserID(string inputData);
    [OperationContract]
    //[WebInvoke(Method = "POST", UriTemplate = "/getFriendsResume/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    //Stream getFriendsResume(string inputData);
    //[OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/registerForServerNotification/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream registerForServerNotification(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getJobTrackingDetail/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getJobTrackingDetail(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/deleteFriend/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream deleteFriend(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/clearChat/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream clearChat(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/changePassword/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream changePassword(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/subscribe/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream subscribe(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/unsubscribe/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream unsubscribe(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getUsageStats/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getUsageStats(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getReferenceMaterialForJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getReferenceMaterialForJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getTokenForUploadVideo/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getTokenForUploadVideo(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/duplicateJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream duplicateJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getSuggestedVideos/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getSuggestedVideos(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/attachVideos/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream attachVideos(string inputData);
    [WebInvoke(Method = "POST", UriTemplate = "/getSkills/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getSkills(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getCompaniesByName/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getCompaniesByName(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getSkillTest/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getSkillTest(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getCompanySkillTest/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getCompanySkillTest(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getCandidateResult/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getCandidateResult(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getTokenForReferenceMaterial/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getTokenForReferenceMaterial(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getReferenceMaterialForUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getReferenceMaterialForUser(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/updateUserContactNo/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream updateUserContactNo(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/savePaymentDetails/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream savePaymentDetails(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/findUsersWithSkillMatchForJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream findUsersWithSkillMatchForJob(string inputData);
    //[OperationContract]
    //[WebInvoke(Method = "POST", UriTemplate = "/saveCandidateAnswersData/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    //Stream saveCandidateAnswersData(string inputData);
    
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getPaymentDetailsForUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getPaymentDetailsForUser(string inputData);


    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getTestForJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getTestForJob(string inputData);

    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getTestForUserSkills/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getTestForUserSkills(string inputData);

    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getPastPerformanceForUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getPastPerformanceForUser(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/postJsonVia/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream postJsonVia(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/auditUrl/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream auditUrl(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/updateSkillWithURL/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream updateSkillWithURL(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getSkillsForUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getSkillsForUser(string inputData);

}



/*blockUser(user who intiate blocking, user to be block) in user table set the values  */
/*private function */

//postOnYourSocialWall

//getApi() Meta part.....data:appName,currentLatitude,currentLognitude
//postResumeToHelpingFriends
//write auditTable api

// initiate payments,confirm paymens,bad payment,sentpayment invoice,

//getJobApplication
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/// <summary>
/// Summary description for QueueForFreshervilla
/// </summary>
public class QueueForFreshervilla
{
	public QueueForFreshervilla()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    FresherVillaDataModel model = new FresherVillaDataModel();
    EmailSend sendEmail = new EmailSend();
    DataTable dt = new DataTable();
    public int queueType { get; set; }
    public int deliverTo { get; set; }
    public int deliveryRequestedBy { get; set; }
    public int deliverUsingTemplate { get; set; }
    public string dataToMerge { get; set; }
    public int deliveryStatus { get; set; }
    public int deliveryPriority { get; set; }
    public string templateName { get; set; }
    public int associtedEntityRefId { get; set; }
    public string queueComments
    { get; set; }
   
    public int queueJobNotification(int jobID)
    {
        int postedBy = 0;
        string jobTitle = null;

        dt = model.selectIntoTable("select postedBy,jobTitle from Job where jid=" + jobID);
        if (dt.Rows.Count != 0)
        {
            postedBy = Convert.ToInt32(dt.Rows[0][0]);
            jobTitle = dt.Rows[0][1].ToString();
            dt = model.selectIntoTable("SELECT uid " +
                                       "FROM Users A, RefereredUsers B WHERE A.uid = B.refUserId AND B.regUserId =" + postedBy + " AND userType=1");
            if (dt.Rows.Count != 0)
            {
                int[] arrUid = new int[dt.Rows.Count];
                for (int rowindex = 0; rowindex < dt.Rows.Count; rowindex++)
                {
                    arrUid[rowindex] = Convert.ToInt32(dt.Rows[rowindex][0]);
                }

                foreach (int deliverTo in arrUid)
                {
                    dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,deliveryRequestedBy," +
                                     "deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn," +
                                       "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                                      ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                                      "(2," + deliverTo + "," + postedBy + ",3,'" + jobTitle + "',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')", "qid");
                    if (dt.Rows.Count != 0)
                    {
                        processTo(Convert.ToInt32(dt.Rows[0][0]), null);
                    }
                }
            }
        }
        return 0;

    }
    private int queueFriendInvitation(int registerUid)
    {

        return 0;
    }
    private void queueJobExpirationNotifications() { }
    public void processTo(int qid, string filePath)
    {
        DataTable temptable = new DataTable();
        try
        {
            dt = model.selectIntoTable("select distinct(T.templateRefPath),Q.queueType,Q.deliverTo,(Q.deliveryRequestedBy is not null) as deliveryRequestedBy," +
                                      "Q.deliverUsingTemplate,Q.dataToMerge,Q.deliveryStatus,Q.deliveryStatusUpdatedOn," +
                                        "Q.requestQueuedOn,Q.deliveryPriority,Q.removeFromQueueAfterProcessing,Q.expireMessageAfter" +
                                       ",Q.deliverOnlyAfterDateTime,Q.queueComments,Q.queueFilterExpression from Queue as Q , Templates as T  where qid="
                                       + qid + " and T.tid=Q.deliverUsingTemplate");
            if (dt.Rows.Count != 0)
            {
                intializePropertiesFromTable(dt);
            }
            dt = model.selectIntoTable("select displayName, userName from Users where uid =" + deliverTo);
            if (dt.Rows.Count != 0)
            {
                if (queueType == 2)
                {
                    if (deliveryRequestedBy == 0)
                    {
                        temptable = model.selectIntoTable("select luvValueDisplayName from ListofValues where luvCategory='unsubscribe'");
                        sendEmail.sendMailWithTemplate("Confirm your identity to reset takemyjob password", dt.Rows[0][0].ToString(), dt.Rows[0][1].ToString(), "https://csimplifyit.com/resetPassword.aspx?token=" + dataToMerge, templateName, temptable.Rows[0][0].ToString() + dataToMerge);

                    }
                }

                model.updateIntoTable("update Queue set deliveryStatus=1,deliveryStatusUpdatedOn=now(),ackKnowledgementRecievedOn=now()" +
                    " where qid=" + qid);
            }
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
        }

    }
    void processForNewSkills(int associtedEntityRefId)
    {
        SearchEngine se = new SearchEngine();
        se.searchForSingleSkill(associtedEntityRefId);
        se.searchIOForSingleSkills(associtedEntityRefId);
    }
    public void insertImp()
    {
        try
        {
            model.insert("insert into Queue (queueType,deliverTo,deliveryRequestedBy," +
                          "deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn," +
                            "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                           ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                           "(2,1,2,1,'hello',0,now(),now(),1,'1', DATE_ADD(now(),INTERVAL 30 DAY),DATE_ADD(now()," +
                           "INTERVAL 30 DAY),'null','null')");
        }
        catch (Exception exc) { Console.Write(exc.Message); }
    }
    public void queueRegistrationMail(int userID, string encodedToken)
    {
        dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,deliveryRequestedBy," +
                     "deliverUsingTemplate,dataToMerge,ackknowledgementToken,deliveryStatus,deliveryStatusUpdatedOn," +
                       "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                      ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                      "(2," + userID + ",null,1,'" + encodedToken + "','" + encodedToken + "',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')", "qid");
        if (dt.Rows.Count != 0)
        {
            processTo(Convert.ToInt32(dt.Rows[0][0]), null);
        }
    }
    public void queuePasswordForEnrollCandidate(int userID, string encodedToken)
    {
        dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,deliveryRequestedBy," +
                     "deliverUsingTemplate,dataToMerge,ackknowledgementToken,deliveryStatus,deliveryStatusUpdatedOn," +
                       "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                      ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                      "(2," + userID + ",null,1,'" + encodedToken + "','" + encodedToken + "',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')", "qid");
        if (dt.Rows.Count != 0)
        {
            processTo(Convert.ToInt32(dt.Rows[0][0]), null);
        }
    }
    public void queueForNewCompany(int cid)
    {
        dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,associtedEntityRefId,deliveryRequestedBy," +
                     "deliverUsingTemplate,dataToMerge,ackknowledgementToken,deliveryStatus,deliveryStatusUpdatedOn," +
                       "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                      ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                      "(8,1," + cid + ",null,1,'','',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')", "qid");

    }
    public void queueForNewSkill(int luvid)
    {
        try
        {
            dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,associtedEntityRefId,deliveryRequestedBy," +
                         "deliverUsingTemplate,dataToMerge,ackknowledgementToken,deliveryStatus,deliveryStatusUpdatedOn," +
                           "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                          ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                          "(7,1," + luvid + ",null,1,'','',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')", "qid");
            if (dt.Rows.Count != 0)
            {
                processTo(Convert.ToInt32(dt.Rows[0][0]));
                model.deleteFromTable("delete from Queue where qid=" + Convert.ToInt32(dt.Rows[0][0]));
            }
        }
        catch (Exception Exc) { 
        }
    }
    public int queueForgotMail(int userID)
    {
        var isoDateTimeFormat = CultureInfo.InvariantCulture.DateTimeFormat;
        string datetime = DateTime.Now.ToString(isoDateTimeFormat.SortableDateTimePattern);
        byte[] array = Encoding.ASCII.GetBytes(deliverTo.ToString() + datetime);
        string encoded = System.Convert.ToBase64String(array);
        dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,deliveryRequestedBy," +
       "deliverUsingTemplate,dataToMerge,ackknowledgementToken,deliveryStatus,deliveryStatusUpdatedOn," +
         "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
        ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
        "(6," + userID + ",null,1,'" + encoded + "','" + encoded + "',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')", "qid");
        if (dt.Rows.Count != 0)
        {
            Task.Factory.StartNew(() =>
            {
                processTo(Convert.ToInt32(dt.Rows[0][0]));
            });
            return Convert.ToInt32(dt.Rows[0][0]);
        }
        return 0;
    }
    public int insertdetailForPostResumes(int userID, int AssocoatedId)
    {
        dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,associtedEntityRefId,deliveryRequestedBy," +
                     "deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn," +
                       "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                      ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                      "(5," + userID + "," + AssocoatedId + ",null,2,'null',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')", "qid");
        if (dt.Rows.Count != 0)
        {
            return Convert.ToInt32(dt.Rows[0][0]);
        }
        return 0;
    }
    public int insertdetailForJobNotification(int userID, int jid)
    {
        dt = model.selectIntoTable("select count(*) from Queue where deliverTo =" + userID + " AND associtedEntityRefId=" + jid + " AND deliverUsingTemplate=3");
        int count = Convert.ToInt32(dt.Rows[0][0]);
        if (count == 0)
        {
            dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,associtedEntityRefId,deliveryRequestedBy," +
                         "deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn," +
                           "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                          ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                          "(9," + userID + "," + jid + ",null,3,'null',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')", "qid");
            if (dt.Rows.Count != 0)
            {
                return Convert.ToInt32((dt.Rows[0][0]));
            }
        }
        return 0;
    }
    private void intializePropertiesFromTable(DataTable dt)
    {
        try
        {
            for (int columnindex = 0; columnindex < dt.Columns.Count; columnindex++)
            {
                if (dt.Columns[columnindex].Caption.Equals("queueType"))
                {
                    queueType = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliverTo"))
                {
                    deliverTo = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliveryRequestedBy"))
                {
                    deliveryRequestedBy = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliverUsingTemplate"))
                {
                    deliverUsingTemplate = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliverTo"))
                {
                    deliverTo = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("dataToMerge"))
                {
                    dataToMerge = (dt.Rows[0][columnindex]).ToString();
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliveryStatus"))
                {
                    deliveryStatus = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliveryPriority"))
                {
                    deliveryPriority = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("queueComments"))
                {
                    queueComments = dt.Rows[0][columnindex].ToString();

                }
                else if (dt.Columns[columnindex].Caption.Equals("templateRefPath"))
                {
                    templateName = dt.Rows[0][columnindex].ToString();

                }
                else if (dt.Columns[columnindex].Caption.Equals("associtedEntityRefId"))
                {
                    try
                    {
                        associtedEntityRefId = Convert.ToInt32(dt.Rows[0][columnindex]);
                    }
                    catch (Exception exc) { Console.WriteLine(exc.Message); }


                }

            }

        }
        catch (Exception exc)
        {
            throw exc;
        }

    }
    public void processTo(int qid)
    {
        DataTable temptable = new DataTable();
        try
        {
            dt = model.selectIntoTable("select distinct(T.templateRefPath),Q.queueType,Q.deliverTo,(Q.deliveryRequestedBy is not null) as deliveryRequestedBy," +
                                      "Q.deliverUsingTemplate,Q.dataToMerge" +
                                       ",Q.associtedEntityRefId from Queue as Q , Templates as T  where qid="
                                       + qid + " and T.tid=Q.deliverUsingTemplate");
            if (dt.Rows.Count != 0)
            {
                intializePropertiesFromTable(dt);
            }
            dt = model.selectIntoTable("select displayName, userName,userPassword from Users where uid =" + deliverTo);
            if (dt.Rows.Count != 0)
            {
                if (queueType == 5)
                {
                    if (deliveryRequestedBy == 0)
                        queueSendResume(associtedEntityRefId, deliverTo);
                    model.updateIntoTable("update Queue set deliveryStatus=1,deliveryStatusUpdatedOn=now(),ackKnowledgementRecievedOn=now()" +
                  " where qid=" + qid);
                }
                else if (queueType == 2)
                {
                    if (deliveryRequestedBy == 0)
                    {
                        temptable = model.selectIntoTable("select luvValueDisplayName from ListofValues where luvCategory='unsubscribe'");
                        sendEmail.sendMailWithTemplate("Confirm your identity to reset FresherVilla password", dt.Rows[0][0].ToString(), dt.Rows[0][1].ToString(), "https://csimplifyit.com/resetPassword.aspx?token=" + dataToMerge, templateName, temptable.Rows[0][0].ToString() + dataToMerge);
                        model.updateIntoTable("update Queue set deliveryStatus=1,deliveryStatusUpdatedOn=now(),ackKnowledgementRecievedOn=now()" +
                  " where qid=" + qid);
                    }
                }
                else if (queueType == 7)
                {
                    processForNewSkills(associtedEntityRefId);
                }
                else if (queueType == 6)
                {

                    temptable = model.selectIntoTable("select luvValueDisplayName from ListofValues where luvCategory='unsubscribe'");
                    byte[] data = Convert.FromBase64String(dt.Rows[0][2].ToString());
                    string decodedString = Encoding.UTF8.GetString(data);
                    decodedString = decodedString.Trim('\0');
                    sendEmail.sendMailForChangePassword("Change FresherVilla password", dt.Rows[0][0].ToString(), dt.Rows[0][1].ToString(),
                        decodedString, templateName, temptable.Rows[0][0].ToString() + dataToMerge);
                    model.updateIntoTable("update Queue set deliveryStatus=1,deliveryStatusUpdatedOn=now(),ackKnowledgementRecievedOn=now()" +
                  " where qid=" + qid);
                }

            }
        }
        catch (Exception exc)
        {
            model.updateIntoTable("update Queue set deliveryStatus=1,deliveryStatusUpdatedOn=now(),ackKnowledgementRecievedOn=now()" +
                   " where qid=" + qid);
            Console.WriteLine(exc.Message);
        }

    }
    public void processToMail(int qid, string table)
    {
        DataTable temptable = new DataTable();
        try
        {
            dt = model.selectIntoTable("select distinct(T.templateRefPath),Q.queueType,Q.deliverTo,(Q.deliveryRequestedBy is not null) as deliveryRequestedBy," +
                                      "Q.deliverUsingTemplate,Q.dataToMerge" +
                                       ",Q.associtedEntityRefId from Queue as Q , Templates as T  where qid="
                                       + qid + " and T.tid=Q.deliverUsingTemplate");
            if (dt.Rows.Count != 0)
            {
                intializePropertiesFromTable(dt);
            }
            dt = model.selectIntoTable("select displayName, userName from Users where uid =" + deliverTo);
            if (dt.Rows.Count != 0)
            {
                if (queueType == 9)
                {

                    queueSendJobnotification(associtedEntityRefId, deliverTo, table);
                    model.updateIntoTable("update Queue set deliveryStatus=1,deliveryStatusUpdatedOn=now(),ackKnowledgementRecievedOn=now()" +
                  " where qid=" + qid);

                }

            }
        }
        catch (Exception exc)
        {
            model.updateIntoTable("update Queue set deliveryStatus=1,deliveryStatusUpdatedOn=now(),ackKnowledgementRecievedOn=now()" +
                   " where qid=" + qid);
            Console.WriteLine(exc.Message);
        }

    }
    private void queueSendResume(int jobID, int postedBy)
    {
        string userName = null;
        string postedOnDate = null;
        string expiresOnDate = null;
        string jobTitle = null;
        string displayName = null;
        string token = null;
        string jobComments = null;
        DataTable tempTable = new DataTable();
        dt = model.selectIntoTable("select  distinct(R.refDocumentPath) "
            + "from Job As J LEFT JOIN  JobApplications As A ON J.jid = A.jobRefId "
            + " LEFT JOIN Resume R ON  R.refUserId = A.appliedByUser where J.jid = " + jobID + "");
        List<string> listOfFilePath = new List<string>();


        for (int rowindex = 0; rowindex < dt.Rows.Count; rowindex++)
        {
            if (dt.Rows[rowindex][0].ToString() != "")
            {
                listOfFilePath.Add(dt.Rows[rowindex][0].ToString());
            }
        }

        dt = model.selectIntoTable("select jid ,jobTitle,DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime,DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') " +
                "as jobExpiresOn,postedBy,U.userName,U.displayName,A.token,jobComments  from Users as U, Job as J , AuthenticationTokens as A " +
            " where A.authSite='system' and A.regUserId=U.uid and J.postedBy=U.uid and jid=" + jobID);
        if (Convert.ToInt32(dt.Rows[0][4]) == postedBy)
        {
            jobTitle = dt.Rows[0][1].ToString();
            postedOnDate = dt.Rows[0][2].ToString();
            expiresOnDate = dt.Rows[0][3].ToString();
            userName = dt.Rows[0][5].ToString();
            displayName = dt.Rows[0][6].ToString();
            token = dt.Rows[0][7].ToString();
            jobComments = dt.Rows[0][8].ToString();
            GenrateExcel excel = new GenrateExcel();
            tempTable = model.selectIntoTable("select  distinct(R.resumeTitle) as ResumeName, " +
               " U.userName as EmailID,U.userContactNo as ContactNo,U.displayName as AppliedBy, " +
              "  U.currentJobTitle as ApplicantTitle,U.currentIndustry as ApplicantIndustry, " +
                "DATE_FORMAT(R.uploadDateTime,'%d-%b-%Y') as ResumeUpdatedOn,A.applicationNotes as  " +
             "   ApplicantComment,DATE_FORMAT( A.appliedOnDateTime,'%d-%b-%Y') as JobAppliedOn  " +
"  from Job As J LEFT JOIN  JobApplications As A ON J.jid = A.jobRefId  " +
" LEFT JOIN Resume R ON  R.refUserId = A.appliedByUser left join  " +
"  Users U on  " +
 " U.uid = A.appliedByUser  " +
"  where J.jid = " + jobID);
            string pathOFExcel = excel.WriteToCsvFile(tempTable, dt.Rows[0][0].ToString() + "Job", "excelFiles");
            listOfFilePath.Add(pathOFExcel);
            string tempFilePath = Path.Combine(HttpRuntime.AppDomainAppPath, "compressedResumes\\" + dt.Rows[0][0].ToString() + "Job" + ".Zip");
            ZipHelper.ZipFiles(tempFilePath, listOfFilePath);
            tempTable = model.selectIntoTable("select luvValueDisplayName from ListofValues where luvCategory='unsubscribe'");
            sendEmail.sendMailWithTemplate(userName, tempFilePath, templateName, displayName, jobTitle, postedOnDate, expiresOnDate, jobComments, tempTable.Rows[0][0].ToString() + token);

        }







    }
    private void queueSendJobnotification(int jobID, int deliverTo, string table)
    {
        string userName = null;
        string jobTitle = null;
        string displayName = null;
        string token = null;

        DataTable tempTable = new DataTable();

        dt = model.selectIntoTable("select J.jobTitle,U.userName,U.displayName,A.token from Users as U, Job as J , AuthenticationTokens as A " +
            " where A.authSite='system' and A.regUserId=U.uid and U.uid = " + deliverTo + " and J.jid=" + jobID);


        jobTitle = dt.Rows[0][0].ToString();
        userName = dt.Rows[0][1].ToString();
        displayName = dt.Rows[0][2].ToString();
        token = dt.Rows[0][3].ToString();


        tempTable = model.selectIntoTable("select luvValueDisplayName from ListofValues where luvCategory='unsubscribe'");

        sendEmail.sendMailWithTemplate1(userName, templateName, displayName, jobTitle, table, tempTable.Rows[0][0].ToString() + token);

        //string iqMaterial = Convert.ToString(material.Rows[0][0]);
        //string iqTittle = Convert.ToString(material.Rows[0][1]);
        //string lMaterial = Convert.ToString(material.Rows[1][0]);
        //string lTittle = Convert.ToString(material.Rows[1][1]);
        //sendEmail.sendMailWithTemplate1(userName,templateName, displayName, jobTitle,,tempTable.Rows[0][0].ToString() + token);


    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Test.Api;

namespace Test.Api
{
    public struct SearchType
    {
        public string url;
        public string title;
        public string content;
        public FindingEngine engine;
        public enum FindingEngine { Google, Bing, GoogleAndBing };
    }

    public interface ISearchResult
    {
        SearchType.FindingEngine Engine { get; set; }
        string SearchExpression { get; set; }
        List<SearchType> Search();
    }

    public class BingSearch : ISearchResult
    {
        public BingSearch(string searchExpression)
        {
            this.Engine = SearchType.FindingEngine.Bing;
            this.SearchExpression = searchExpression;
        }
        public SearchType.FindingEngine Engine { get; set; }
        public string SearchExpression { get; set; }

        public List<SearchType> Search()
        {
            // our appid from bing - 3F4313687C37F1....23A79E181D0A25
            const string urlTemplate = @"http://api.search.live.net/json.aspx?AppId=3F4313687C37F1...23A79E181D0A25& \
                                            Market=en-US&Sources=Web&Adult=Strict&Query={0}&Web.Count=50";
            const string offsetTemplate = "&Web.Offset={1}";
            var resultsList = new List<SearchType>();
            int[] offsets = { 0, 50, 100, 150 };
            Uri searchUrl;
            foreach (var offset in offsets)
            {
                if (offset == 0)
                    searchUrl = new Uri(string.Format(urlTemplate, SearchExpression));
                else
                    searchUrl = new Uri(string.Format(urlTemplate + offsetTemplate,
                                        SearchExpression, offset));

                var page = new WebClient().DownloadString(searchUrl);
                var o = (JObject)JsonConvert.DeserializeObject(page);

                var resultsQuery =
                  from result in o["SearchResponse"]["Web"]["Results"].Children()
                  select new SearchType
                  {
                      url = result.Value<string>("Url").ToString(),
                      title = result.Value<string>("Title").ToString(),
                      content = result.Value<string>("Description").ToString(),
                      engine = this.Engine
                  };

                resultsList.AddRange(resultsQuery);
            }
            return resultsList;
        }
    }

    public class GoogleSearch : ISearchResult
    {
        public GoogleSearch(string searchExpression)
        {
            this.Engine = SearchType.FindingEngine.Google;
            this.SearchExpression = searchExpression;
        }
        public SearchType.FindingEngine Engine { get; set; }
        public string SearchExpression { get; set; }

        public List<SearchType> Search()
        {
            var resultsList = new List<SearchType>();
            try
            {
                const string urlTemplate = @"http://ajax.googleapis.com/ajax/services/search/web?v=1.0& \
                                        rsz=large&safe=active&q={0}&start={1}";
              
                int[] offsets = {0,4};
                foreach (var offset in offsets)
                {
                    try
                    {
                        var searchUrl = new Uri(string.Format(urlTemplate,SearchExpression, offset));
                        var page = new WebClient().DownloadString(searchUrl);
                        var o = (JObject)JsonConvert.DeserializeObject(page);

                        var resultsQuery =
                          from result in o["responseData"]["results"].Children()
                          select new SearchType
                          {
                              url = result.Value<string>("url").ToString(),
                              title = result.Value<string>("title").ToString(),
                              content = result.Value<string>("content").ToString(),
                              engine = this.Engine
                          };
                        resultsList.AddRange(resultsQuery);
                    }
                    catch (Exception exc) { Console.WriteLine(exc.Message); }
                   
                }       
            }
            catch { }
            return resultsList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Data;
using System.Text;
using csimplifyit;

/// <summary>
/// Summary description for CreateInsertStmntFromKeyValue
/// </summary>
public class InsertFromOauth
{
    DataModel model = new DataModel();
    //CSimplifyitException exception;
    EmailSend sendEmail = new EmailSend();
    oAuthLinkedIn linkedin = new oAuthLinkedIn();
    JsonParserSaveData parser = new JsonParserSaveData();
    Queue queue = new Queue();
    public InsertFromOauth()
    {
    }
    public string[] insertLinkeinProfile(string json,string[]credentials)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        string[] primaryValues = new string[2];
        list = parser.profilejsonparser(json, "linkedin");
        string skills=null;
        string email=null;
        string displayname = null;
        string company = null;
        string industry = null;
        int uid;
        string socialID = null;
        string currentJobTitle=null;
        foreach (var listitem in list)
        { 
           if(listitem.Key.Equals("skills")){
           skills=listitem.Value;
            }
           if(listitem.Key.Equals("firstName")){
           displayname=displayname+listitem.Value.ToString();
           //displayname.Replace("'", " ");
           }
            if(listitem.Key.Equals("lastName")){
            displayname=displayname+" "+listitem.Value.ToString();
            displayname.Replace("'", " ");
            }
            if (listitem.Key.Equals("headline")){
                    company =listitem.Value;
                    company.Replace("'", " ");
                    currentJobTitle=company;
                   string[] arr = company.Split(new string[] { " at " }, StringSplitOptions.None);
                company=arr[arr.Length-1];
            }
            if (listitem.Key.Equals("industry")) {
                industry = listitem.Value;
                industry.Replace("'", " ");
            }
            if (listitem.Key.Equals("emailAddress")) {
                email = listitem.Value;
                email.Replace("'", " ");
            }
            if (listitem.Key.Equals("id"))
            {
                socialID = listitem.Value;
                //credentials[3] = socialID;
            }
        }
        var isoDateTimeFormat = CultureInfo.InvariantCulture.DateTimeFormat;
                string datetime = DateTime.Now.ToString(isoDateTimeFormat.SortableDateTimePattern);
                DataTable dt = new DataTable();
                primaryValues[1] = displayname;
               // dt=model.selectIntoTable("select uid from Users Where userName='" + email+"'");
                dt = model.selectIntoTable("select regUserId from AuthenticationTokens where socialID='" + socialID + "'");
        if(dt.Rows.Count==0)
        {
          dt=  model.insertIntoTableWithLastId("insert into Users (registerationDate,userName,displayName,userType,userStatus,userPassword,modifiedBy," +
                "modifiedDateTime,createdBy,createdDatetime"
                + ",currentCompanyName,currentJobTitle,currentIndustry) values"+
                " (now(),'" + email + "','" + displayname + "','" + "1'," + "'1','"
                + datetime + "','" + displayname + "','" + datetime + "','" + displayname + "','" + datetime + "','"
                + company + "','" + currentJobTitle + "','" + industry+"')","uid");
              if (dt.Rows.Count != 0)
              {
                  uid = Convert.ToInt32(dt.Rows[0][0]);
                  primaryValues[0] = uid.ToString();
                  byte[] array = Encoding.ASCII.GetBytes(uid.ToString() + datetime);
                  string encoded = System.Convert.ToBase64String(array);
                   model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,authSite) values (" + uid + ",'" + encoded + "','system')");
                   queue.queueRegistrationMail(uid, encoded);
                  updateusercredential(uid, "linkedin", credentials, socialID);
                  saveSkills(uid, skills);
                
                 // sendEmail.sendMail(email, "Your Link For Reset Password Is :http:/developer.csimplifyit.com/Default.aspx?linkshow$" + encoded);
                  
              }
              
         }
        else
        {
            uid = Convert.ToInt32(dt.Rows[0][0]);
            dt = model.selectIntoTable("select userType from Users where uid= " + uid);
            byte[] array = Encoding.ASCII.GetBytes(uid.ToString() + datetime);
            string encoded = System.Convert.ToBase64String(array);
            primaryValues[0] = uid.ToString();
            model.updateIntoTable("update Users set userName='"+email+"',displayName='"+displayname+
                "',userType=1,userStatus=1,modifiedBy='"+displayname+
                "',modifiedDateTime=now(),createdBy='" + displayname + "',createdDateTime=now(),currentCompanyName='"+
                company + "',currentJobTitle='" + currentJobTitle + "',currentIndustry='" + industry +"' where uid="+uid+"");
            if (dt.Rows.Count != 0)
            {
                if (Convert.ToInt32(dt.Rows[0][0]) != 1)
                {
                    dt=model.selectIntoTable("select auid from AuthenticationTokens where regUserid="+uid+ " and authSite='system'");
                    if(dt.Rows.Count==0){
                   model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,authSite) values (" + uid + ",'" + encoded + "','system')");
                    }
                    else{
                    model.updateIntoTable("update AuthenticationTokens set token='" + encoded + "' where regUserId=" + uid + " and authSite='system'");
                    }
                    //sendEmail.sendMail(email, "Your Link For Reset Password Is :http:/developer.csimplifyit.com/Default.aspx?linkshow$" + encoded);
                    queue.queueRegistrationMail(uid, encoded);
                }
            }
            queue.queueRegistrationMail(uid, encoded);
            updateusercredential(uid, "linkedin",credentials,socialID);
            saveSkills(uid, skills);
            insertNewCompany(company);
        }
            
        return primaryValues;
    }
    private void updateusercredential( int uid, string client,string[]credentials,string socialID)
    {
        DataTable dt=new DataTable();
        try
        {
            dt = model.selectIntoTable("select auid from AuthenticationTokens where regUserId='" + uid + "' and authSite='" + client + "'");
            if (dt.Rows.Count == 0)
            {
                model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,tockensecret,verifiercode,socialID,authSite) values ('" + uid + "','" + credentials[0] + "','" + credentials[1] + "','" + credentials[2] + "','" + socialID + "','linkedin')");
            }
            else
            {
                int auid = Convert.ToInt32(dt.Rows[0][0]);
                model.updateIntoTable("update AuthenticationTokens set token='" + credentials[0] + "',tockensecret='" + credentials[1] + "',socialID='" + socialID + "',verifiercode='" + credentials[2] + "' where auid=" + auid);
            }

        }
        catch (Exception exc) {
            throw new CSimplifyitException(1, 1, exc, null);
        }
    }
    private void saveSkills(int uid, string skills) {
        try
        {
            DataTable dt = new DataTable();
            model.updateIntoTable("update Users set userSkills='" + skills + "' where uid=" + uid);
            if (skills != null)
            {
                string[] skillarray = skills.Split(',');
                foreach (var skill in skillarray)
                {
                    if (skill != "")
                    {
                        dt = model.selectIntoTable("select luvid from ListofValues where luvCategory ='skills' and luvValueDisplayName ='" + skill + "'");
                        if (dt.Rows.Count != 0)
                        {
                            string luvid = dt.Rows[0][0].ToString();
                            dt = model.selectIntoTable("select * from UserSkills where uid=" + uid + " and skillRef =" + dt.Rows[0][0].ToString());
                            if (dt.Rows.Count != 0)
                            {
                                model.insertIntoTable("insert into UserSkills (uid,addedOn,skillRef) values ('" + uid + "',now(),'" + luvid + "')");
                            }
                            dt.Clear();
                        }
                        else
                        {
                            model.insertIntoTableWithLastId("insert into ListofValues (luvCategory,luvValueDisplayName,luvValueCode,luvSystemField,modifiedBy,createdBy,createdDateTime,modifiedDateTime) values ('skills','" + skill + "','" + skill + "','1','" + dateTimeClass.datetime() + "','" + dateTimeClass.datetime() + "','" + dateTimeClass.datetime() + "','" + dateTimeClass.datetime() + "')", "sid");
                            queue.queueForNewSkill(Convert.ToInt32(dt.Rows[0][0]));

                            if (dt.Rows.Count != 0)
                            {
                                model.insertIntoTable("insert into UserSkills (uid,addedOn,skillRef) values ('" + uid + "',now(),'" + dt.Rows[0][0].ToString() + "')");

                            }
                            dt.Clear();
                        }
                    }


                }
            }
        }
        catch (Exception exc) { Console.WriteLine(exc.Message); }
    }
    public void split()
    {
     //   DataTable dt = new DataTable();
     //   DataModel md = new DataModel();
     ////   dt = md.selectIntoTable("select currentJobTitle,uid from Users ");
     //   List<string> li = new List<string>();
     //   List<string> li2 = new List<string>();
     //   for (int i = 0; i < dt.Rows.Count; i++)
     //   {
     //       string data = dt.Rows[i][0].ToString();
     //       string uid = dt.Rows[i][1].ToString();
     //       string[] arr = data.Split(new string[] { " at " }, StringSplitOptions.None);
//int row = md.updateIntoTablewithRowCount("update Users set currentCompanyName='" + arr[arr.Length - 1] + "' where uid='" + uid + "'");
       }
    public void insertNewCompany(string companyName) {
        try
        {
            DataTable dt = model.selectIntoTable("select * from Company where companyName='" + companyName + "'");
            if (dt.Rows.Count == 0)
            {
                dt = model.insertIntoTableWithLastId("insert into Company (companyName,addedOn)values('" + companyName + "',now())", "cid");
                queue.queueForNewCompany(Convert.ToInt32(dt.Rows[0][0]));
            }
        }
        catch(Exception exc){
            Console.WriteLine(exc.Message);
        }
    }
}
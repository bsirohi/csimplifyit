﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

public class RegexUtilities
{

    static public bool IsValidEmail(string strIn)
    {
        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match match = regex.Match(strIn);
        if (match.Success)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    static public bool Is_Valid_Url(string WebUrl)
    {

        try
        {
            if (Regex.IsMatch(WebUrl, @"(http|https)://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"))
            {
                return true;
            }
            else if (Regex.IsMatch(WebUrl, @"([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch
        {
            return false;
        }
    }
    static public bool isVallidNumber(string number)
    {
        try
        {
            int num;
            bool res = int.TryParse(number, out num);
            if (res == false)
            {
                return false;
            }
            return true;
        }
        catch
        {
            return false;
        }
    }
}
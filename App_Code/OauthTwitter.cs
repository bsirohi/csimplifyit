﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Xml;
using System.Collections;
using System.Collections.Specialized;
using System.Security.Cryptography;

namespace csimplifyit
{
   public class OauthTwitter : oAuthBase
    {
        public enum Method { GET, POST, PUT, DELETE };
        public const string REQUEST_TOKEN = "https://api.twitter.com/oauth/request_token";
        public const string AUTHORIZE = "https://api.twitter.com/oauth/authorize";
        public const string ACCESS_TOKEN = "https://api.twitter.com/oauth/access_token";
        public const string CALLBACK = "http://developer.csimplifyit.com/signup.aspx";
        private string _consumerKey = "";
        private string _consumerSecret = "";
        private string _token = "";
        private string _tokenSecret = "";
        private string accesstoken = "1130953634-vRU8gcRcWc3aO3EgIWwjXImrQnFKWI9D0c1M17c";
        private string accesssecrettoken = "qJV7xkj4zeaL7Ra6hLoUKyC0P40di7jvo3K2ssAkQ";
        private string ScreenName = "";
        #region Properties
        public string ConsumerKey
        {
            get
            {
                if (_consumerKey.Length == 0)
                {
                    _consumerKey = "dPrmomnRtjrCpqViGQvwA";
                }
                return _consumerKey;
            }
            set { _consumerKey = value; }
        }
        public string ConsumerSecret
        {
            get
            {
                if (_consumerSecret.Length == 0)
                {
                    _consumerSecret = "xBenNRicelser6FUi3ip67nCKVBRkrMWgQYJkwtiA";
                }
                return _consumerSecret;
            }
            set { _consumerSecret = value; }
        }
        public string Token { get { return _token; } set { _token = value; } }
        public string TokenSecret { get { return _tokenSecret; } set { _tokenSecret = value; } }
        #endregion
        public string AuthorizationLinkGet()
        {
            string ret = null;
            string response = oAuthWebRequest(Method.POST, REQUEST_TOKEN, String.Empty);
            if (response.Length > 0)
            {
                //response contains token and token secret.  We only need the token.
                //oauth_token=36d1871d-5315-499f-a256-7231fdb6a1e0&oauth_token_secret=34a6cb8e-4279-4a0b-b840-085234678ab4&oauth_callback_confirmed=true
                NameValueCollection qs = HttpUtility.ParseQueryString(response);
                if (qs["oauth_token"] != null)
                {
                    this.Token = qs["oauth_token"];
                    this.TokenSecret = qs["oauth_token_secret"];
                    ret = AUTHORIZE + "?oauth_token=" + this.Token;
                }
            }
            return ret;
        }
        public void AccessTokenGet()
        {

            string response = oAuthWebRequest(Method.POST, ACCESS_TOKEN, string.Empty);

            if (response.Length > 0)
            {
                //Store the Token and Token Secret
                NameValueCollection qs = HttpUtility.ParseQueryString(response);
                if (qs["oauth_token"] != null)
                {
                    this.accesstoken = qs["oauth_token"];
                }
                if (qs["oauth_token_secret"] != null)
                {
                    this.accesssecrettoken = qs["oauth_token_secret"];
                }
                if (qs["screen_name"] != null)
                {
                    this.ScreenName = qs["screen_name"];
                }
            }
        }
        /// <summary>
        /// Submit a web request using oAuth.
        /// </summary>
        /// <param name="method">GET or POST</param>
        /// <param name="url">The full url, including the querystring.</param>
        /// <param name="postData">Data to post (querystring format)</param>
        /// <returns>The web server response.</returns>
        public string oAuthWebRequest(Method method, string url, string postData)
        {
            string outUrl = "";
            string querystring = "";
            string ret = "";
            //Setup postData for signing.
            //Add the postData to the querystring.
            if (method == Method.POST)
            {
                if (postData.Length > 0)
                {
                    //Decode the parameters and re-encode using the oAuth UrlEncode method.
                    NameValueCollection qs = HttpUtility.ParseQueryString(postData);
                    postData = "";
                    foreach (string key in qs.AllKeys)
                    {
                        if (postData.Length > 0)
                        {
                            postData += "&";
                        }
                        qs[key] = HttpUtility.UrlDecode(qs[key]);
                        qs[key] = this.UrlEncode(qs[key]);
                        postData += key + "=" + qs[key];

                    }
       
                    
                    if (url.IndexOf("?") > 0)
                    {
                        url += "&";
                    }
                    else
                    {
                        url += "?";
                    }
                    url += postData;
                }
            }

            Uri uri = new Uri(url);

            string nonce = this.GenerateNonce();
            string timeStamp = this.GenerateTimeStamp();
            string callback = "";
            if (url.ToString().Contains(REQUEST_TOKEN))
                callback = CALLBACK;
            //Generate Signature
            string sig = this.GenerateSignature(uri,
                this.ConsumerKey,
                this.ConsumerSecret,
                this.Token,
                this.TokenSecret,
                method.ToString(),
                timeStamp,
                nonce,
                callback,
                out outUrl,
                out querystring);


            querystring += "&oauth_signature=" + HttpUtility.UrlEncode(sig);

            //Convert the querystring to postData
            if (method == Method.POST)
            {
                postData = querystring;
                querystring = "";
            }

            if (querystring.Length > 0)
            {
                outUrl += "?";
            }

            if (method == Method.POST || method == Method.GET)
                ret = WebRequest(method, outUrl + querystring, postData);
            //else if (method == Method.PUT)
            //ret = WebRequestWithPut(outUrl + querystring, postData);
            return ret;
        }
        public string APIWebRequest(string method, string url, string postData)
        {
            this.Verifier = "d6WRvlKVx6xvCF8lI9W9msqA5b1F5YyQKiWFDst9Kg";
            Uri uri = new Uri(url);
            string nonce = this.GenerateNonce();
            string timeStamp = this.GenerateTimeStamp();
            string outUrl, querystring;
            //Generate Signature
            string sig;
            if (accesstoken == null)
            {
                sig = this.GenerateSignature(uri,
                   this.ConsumerKey,
                   this.ConsumerSecret,
                   this.Token,
                   this.TokenSecret,
                   method,
                   timeStamp,
                   nonce,
                   null,
                   out outUrl,
                   out querystring);
            }
            else
            {
                sig = this.GenerateSignature(uri,
                  this.ConsumerKey,
                  this.ConsumerSecret,
                  this.accesstoken,
                  this.accesssecrettoken,
                  method,
                  timeStamp,
                  nonce,
                  null,
                  out outUrl,
                  out querystring);
            }

            //querystring += "&oauth_signature=" + HttpUtility.UrlEncode(sig);
            //NameValueCollection qs = HttpUtility.ParseQueryString(querystring);

            //string finalGetUrl = outUrl + "?" + querystring;

            HttpWebRequest webRequest = null;

            //webRequest = System.Net.WebRequest.Create(finalGetUrl) as HttpWebRequest;
            webRequest = System.Net.WebRequest.Create(url) as HttpWebRequest;
            //webRequest.ContentType = "text/xml";
            webRequest.Method = method;
            webRequest.Credentials = CredentialCache.DefaultCredentials;
            webRequest.AllowWriteStreamBuffering = true;

            webRequest.PreAuthenticate = true;
            webRequest.ServicePoint.Expect100Continue = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            if (accesstoken == null)
            {
                webRequest.Headers.Add("Authorization", "OAuth realm=\"http://api.twitter.com/\",oauth_consumer_key=\"" + this.ConsumerKey + "\",oauth_token=\"" + this.Token + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\",oauth_timestamp=\"" + timeStamp + "\",oauth_nonce=\"" + nonce + "\",oauth_verifier=\"" + this.Verifier + "\", oauth_version=\"1.0\"");
            }
            else
            {

                webRequest.Headers.Add("Authorization", "OAuth realm=\"http://api.twitter.com/\",oauth_consumer_key=\"" + this.ConsumerKey + "\",oauth_token=\"" + this.accesstoken + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\",oauth_timestamp=\"" + timeStamp + "\",oauth_nonce=\"" + nonce + "\",oauth_verifier=\"" + this.Verifier + "\", oauth_version=\"1.0\"");
            }
            //webRequest.Headers.Add("Authorization", "OAuth oauth_nonce=\"" + nonce + "\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"" + timeStamp + "\", oauth_consumer_key=\"" + this.ConsumerKey + "\", oauth_token=\"" + this.Token + "\", oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\", oauth_version=\"1.0\"");
            if (postData != null)
            {

                byte[] fileToSend = Encoding.UTF8.GetBytes(postData);
                webRequest.ContentLength = fileToSend.Length;
                webRequest.ContentType = "application/json";
                Stream reqStream = webRequest.GetRequestStream();

                reqStream.Write(fileToSend, 0, fileToSend.Length);
                reqStream.Close();
            }

            string returned = "";
            try
            {
                returned = WebResponseGet(webRequest);
            }
            catch (WebException we)
            {
                var errrorResponse = we.Response as HttpWebResponse;
                try
                {
                    var sr = new StreamReader(errrorResponse.GetResponseStream());
                    string responseData = sr.ReadToEnd();
                    sr.Close();
                }
                catch (Exception exc) { Console.WriteLine(exc.Message); }

            }

            return returned;
        }
        public string WebRequest(Method method, string url, string postData)
        {
            HttpWebRequest webRequest = null;
            StreamWriter requestWriter = null;
            string responseData = "";

            webRequest = System.Net.WebRequest.Create(url) as HttpWebRequest;
            webRequest.Method = method.ToString();
            webRequest.ServicePoint.Expect100Continue = false;
            //webRequest.UserAgent  = "Identify your application please.";
            //webRequest.Timeout = 20000;

            if (method == Method.POST)
            {
                webRequest.ContentType = "application/x-www-form-urlencoded";

                //POST the data.
                requestWriter = new StreamWriter(webRequest.GetRequestStream());
                try
                {
                    requestWriter.Write(postData);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    requestWriter.Close();
                    requestWriter = null;
                }
            }

            responseData = WebResponseGet(webRequest);

            webRequest = null;

            return responseData;

        }
        public string WebResponseGet(HttpWebRequest webRequest)
        {
            StreamReader responseReader = null;
            string responseData = "";

            try
            {
                responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                responseData = responseReader.ReadToEnd();
            }
            catch
            {
                throw;
            }
            finally
            {
                webRequest.GetResponse().GetResponseStream().Close();
                responseReader.Close();
                responseReader = null;
            }

            return responseData;
        }
        public void connectToOauthServer(string authlink)
        {
           
          
            try
            {
                HttpContext.Current.Response.Redirect(authlink + "&oauth_callback=" + CALLBACK);
               // AccessTokenGet();

            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        public string getAllOauthconnection()
        {
            try
            {
              //  connectToOauthServer();
                string response = APIWebRequest("GET", "https://api.twitter.com/1.1/friends/list.json?cursor=-1&screen_name="+this.ScreenName+"&skip_status=true&include_user_entities=false", null);
                return response;
            }
            catch (Exception exc)
            {

                Console.WriteLine(exc.Message);
                return exc.Message;
            }
        }
        public string getProfile()
        {
            return APIWebRequest("GET", "https://api.twitter.com/1.1/users/show.json?screen_name"+ScreenName,null);
        }
        public void disconnectToOauthServer()
        {
            Token = null;
            TokenSecret = null;
        }
        public void updateStatusOnOauthServer(string status)
        {
            try
            {
                 //connectToOauthServer();
                 var oauth_token = "1130953634-vRU8gcRcWc3aO3EgIWwjXImrQnFKWI9D0c1M17c";
                 var oauth_token_secret = "qJV7xkj4zeaL7Ra6hLoUKyC0P40di7jvo3K2ssAkQ";
                var oauth_consumer_key = ConsumerKey;
                var oauth_consumer_secret = ConsumerSecret;

                // oauth implementation details
                var oauth_version = "1.0";
                var oauth_signature_method = "HMAC-SHA1";

                // unique request details
                var oauth_nonce = Convert.ToBase64String(
                    new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
                var timeSpan = DateTime.UtcNow
                    - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

                // message api details
  
                var resource_url = "http://api.twitter.com/1/statuses/update.json";

                // create oauth signature
                var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                                "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&status={6}";

                var baseString = string.Format(baseFormat,
                                            oauth_consumer_key,
                                            oauth_nonce,
                                            oauth_signature_method,
                                            oauth_timestamp,
                                            oauth_token,
                                            oauth_version,
                                            Uri.EscapeDataString(status)
                                            );

                baseString = string.Concat("POST&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

                var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                                        "&", Uri.EscapeDataString(oauth_token_secret));

                string oauth_signature;
                using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
                {
                    oauth_signature = Convert.ToBase64String(
                        hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
                }

                // create the request header
                var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                                   "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                                   "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                                   "oauth_version=\"{6}\"";

                var authHeader = string.Format(headerFormat,
                                        Uri.EscapeDataString(oauth_nonce),
                                        Uri.EscapeDataString(oauth_signature_method),
                                        Uri.EscapeDataString(oauth_timestamp),
                                        Uri.EscapeDataString(oauth_consumer_key),
                                        Uri.EscapeDataString(oauth_token),
                                        Uri.EscapeDataString(oauth_signature),
                                        Uri.EscapeDataString(oauth_version)
                                );


                // make the request
                var postBody = "status=" + Uri.EscapeDataString(status);

                ServicePointManager.Expect100Continue = false;
                HttpWebRequest request = null;
              request = System.Net.WebRequest.Create(resource_url) as HttpWebRequest;

                       request.Headers.Add("Authorization", authHeader);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream stream = request.GetRequestStream())
                {
                    byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                    stream.Write(content, 0, content.Length);
                }
                try { WebResponse response = request.GetResponse(); }
                catch (Exception exc) { Console.WriteLine(exc.Message); }
            }
            catch (Exception exc) {
                Console.WriteLine(exc.Message);
            }
        }
        
    }
}

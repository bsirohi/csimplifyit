﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization;
using System.Text;

/// <summary>
/// Summary description for returnStatement
/// </summary>
[DataContract]
public class ResponseData
{
    public int status { get; set; }
    public string description { get; set; }
    public string results { get; set; }
    public ResponseData(int status, string Description, string results) {
        this.status = status;
        this.description = Description;
        this.results = results;
    }
    public string returnStatment() {
        StringBuilder sb=new StringBuilder();
        if (results != null)
        {
            sb.Append("{\"status\":" + status + ",\"Description\":\"" + description + "\",\"results\":"+results+"}");
        }
        else {
            sb.Append("{\"status\":" + status + ",\"Description\":\"" + description + "\",\"results\":[]}");
        }
        return sb.ToString();
    }
}
// "{\"status\":0,\"Description\":\"Your json string is not in correct format\",\"results\":[null]}"
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for callStoredProcedure
/// </summary>
public class callStoredProcedure
{
    string connectionstring = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
    string conFreshervilla= "server=50.62.209.3;Port=3306;database=prodfreshervilla;User Id=ProdFreshervilla;password=Khatarnaak@123;pooling=false;";
   
    public callStoredProcedure() { }
    public string insertIntoTable(string query)
    {
        MySqlConnection con = new MySqlConnection(connectionstring);
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
            return "true";
        }

        catch (Exception exc)
        {
            return exc.Message;
        }
        finally { con.Close(); }
    }
    public DataTable callStoreProcedureToFetchVideo(string procedureName,string refType,int jobId,string url)
    {
        DataTable dt = new DataTable();
        MySqlConnection con = new MySqlConnection(connectionstring);
        MySqlDataReader reader;
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(procedureName, con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("refrenceType", MySqlDbType.VarChar, 10).Value = refType;
            command.Parameters.Add("jbID", MySqlDbType.Int32).Value = jobId;
            command.Parameters.Add("url", MySqlDbType.VarChar, 500).Direction = ParameterDirection.Output;
            ;
            reader = command.ExecuteReader();

            dt.Load(reader);

        }
        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
        return dt;
    }

    public DataTable prepareTestForUser(int forUserID)
    {
        
        DataTable dt = new DataTable();
        MySqlConnection con = new MySqlConnection(connectionstring);
        MySqlDataReader reader;
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand("prepareTestForUser", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("aForUserID", MySqlDbType.Int32).Value = forUserID;
            reader = command.ExecuteReader();
            dt.Load(reader);
        }
        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
        return dt;
    }
    public DataTable callStoreProcedureForLeaderBoardScores(string skill, int userid)
    {
        DataTable dt = new DataTable();
        MySqlConnection con = new MySqlConnection(conFreshervilla);
        MySqlDataReader reader;
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand("leaderboardScores", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("skills", MySqlDbType.VarChar).Value = skill;
            command.Parameters.Add("userID", MySqlDbType.Int32).Value = userid;
            
            reader = command.ExecuteReader();

            dt.Load(reader);

        }
        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
        return dt;
    }

public DataTable callStoreProcedureGetMentorCandidateAttendence(string monthName, int? year)
    {
        DataTable dt = new DataTable();
        MySqlConnection con = new MySqlConnection(conFreshervilla);
        MySqlDataReader reader;
        try
        {
            if(monthName=="" || monthName==string.Empty)
            {
                monthName = null;
            }
            if(year==0)
            {
                year = null;
            }
            con.Open();
            MySqlCommand command = new MySqlCommand("getMentorCandidateAttendence", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("month_name", MySqlDbType.VarChar).Value = monthName;
            command.Parameters.Add("year_", MySqlDbType.VarChar).Value = year;

            reader = command.ExecuteReader();

            dt.Load(reader);

        }
        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
        return dt;
    }

}
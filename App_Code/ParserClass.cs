﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System.Configuration;
using System.Data;

namespace csimplifyit
{
   public  class JsonParserSaveData
    {
        string connection = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        DataModel model = new DataModel();
       //string connection = ConfigurationManager.ConnectionStrings["lclcn"].ConnectionString;
        public string token = null;
        public string accesstoken = null;
        public string emailaddress = null;
        public string verifier = null;
        DataTable table = new DataTable();
        public void saveConnectionData(string Jsonreturnyserver, string website,string[]arr)
        {
            if (website == "linkedin")
            {
                linkedinSaveData(Jsonreturnyserver,arr[0],arr[1]);
            }
            if (website == "yahoo")
            {
                yahooSaveData(Jsonreturnyserver);
            }
            if (website == "twitter")
            {
                twitterSaveData(Jsonreturnyserver);
            }
            if (website == "facebook")
            {
                facebookSaveData(Jsonreturnyserver);
            }
        }
        private void linkedinSaveData(string json ,string uid,string modifiedBy)
        {
            string displayName="";
            string company="";
            string industry="";
            string jobTitle="";
            string id = "";
             string datetime = dateTimeClass.datetime();
           try
            {
                StringBuilder sb = new StringBuilder();
              
                JObject root = JObject.Parse(json);
                JArray items = (JArray)root["values"];
               
                  foreach (var data in items)
                 {
                     try
                     {
                     
                         JObject dataobject = JObject.Parse(data.ToString());
                         foreach (var dataelement in dataobject)
                         {
                             if (dataelement.Key.Equals("firstName"))
                             {
                                 displayName = dataelement.Value.ToString();
                                 displayName.Replace("'", " ");
                             }
                             if (dataelement.Key.Equals("lastName"))
                             {
                                 displayName = displayName + " " + dataelement.Value.ToString();
                                
                             }
                             if (dataelement.Key.Equals("headline"))
                             {
                                 jobTitle = dataelement.Value.ToString();
                                 jobTitle.Replace("'", " ");
                                 company = jobTitle;
                                 company.Replace("'", " ");
                                 string[] arr = company.Split(new string[] { " at " }, StringSplitOptions.None);
                                 company = arr[arr.Length - 1];

                             }
                             if (dataelement.Key.Equals("industry"))
                             {
                                 industry = dataelement.Value.ToString();
                                 industry.Replace("'", " ");
                             }
                              if (dataelement.Key.Equals("id"))
                             {
                                 id = dataelement.Value.ToString();
                                 
                             }

                         }
                     }
                     catch (Exception exc) {
                         Console.WriteLine(exc.Message);
                     }
                     DataTable dt = new DataTable();
                     InsertFromOauth ifo = new InsertFromOauth();
                     ifo.insertNewCompany(company);
                     dt=model.selectIntoTable("select regUserId from AuthenticationTokens where socialId='" + id + "'");
                     if (dt.Rows.Count == 0)
                     {
                         try
                         {
                             sb.Append("insert into Users  (userType,displayName,userName,userPassword,userStatus,modifiedBy,modifiedDateTime," +
                 "createdBy,createdDateTime,currentCompanyName,currentIndustry,currentJobTitle) values (");
                             sb.Append("0,'" + displayName + "','" + displayName + "','" + datetime + "','" + "0" +
                                 "',now(),now()" + ",'" + modifiedBy + "',now()" + ",'" + company + "','" + industry + "','" + jobTitle + "')");
                             model.insertIntoTable(sb.ToString());
                             sb.Clear();
                             dt = model.selectIntoTable("select uid from Users where userPassword='" + datetime + "' and displayName='" + displayName + "'");
                             if (dt.Rows.Count != 0)
                             {
                                 model.insertIntoTable("insert into RefereredUsers( regUserId,refUserId,createdDateTime) values ('" + uid + "','" + dt.Rows[0][0].ToString() + "','" + datetime + "')");
                                 model.insertIntoTable("insert into AuthenticationTokens (regUserId,socialId,authSite) values('" + dt.Rows[0][0].ToString() + "','" + id + "','linkedin')");
                             }
                         }

                         catch (Exception exc)
                         {
                             Console.WriteLine(exc.Message);
                         }
                     }
                     else {
                         model.insertIntoTable("insert into RefereredUsers( regUserId,refUserId,createdDateTime) values ('" + uid + "','" + dt.Rows[0][0].ToString() + "','" + datetime + "')");
                       //  model.insertIntoTable("insert into AuthenticationTokens (regUserId,socialId,authSite) values('" + dt.Rows[0][0].ToString() + "','" + id + "','linkedin')");
                         model.updateIntoTable("update AuthenticationTokens set socialId='" + id + "' where authSite='linkedin' and regUserId=" + dt.Rows[0][0].ToString()); 
                     }
               }
                 
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
        }
        private void yahooSaveData(string json)
        {
            MySqlConnection con = new MySqlConnection(connection);
            try
            {
                con.Open();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
            JObject items = null;
            StringBuilder sb = new StringBuilder();
            try
            {
                
                JObject root = JObject.Parse(json);
                 items = (JObject)root["contacts"];
           
            foreach (var data in items)
            {
                if (data.Key == "contact")
                {
                    JArray localitem = (JArray)items["contact"];
                    foreach (var localdata in localitem)
                    {
                        JObject localroot = JObject.Parse(localdata.ToString());
                        foreach (var local2data in localroot)
                        {
                            if (local2data.Key == "fields")
                            {
                                JArray localarray = (JArray)localroot["fields"];
                                int flag = 0;
                                if (localarray.Count == 2)
                                {
                                    {
                                        foreach (var localarraydata in localarray)
                                        {
                                            if (flag == 0)
                                             sb.Append("insert into OauthData  (oauthclient,id,name,lastname) values ('yahoo',");
                                            JObject local2root = JObject.Parse(localarraydata.ToString());
                                            if (flag == 0)
                                                foreach (var local2rootdata in local2root)
                                                {

                                                    if (local2rootdata.Key == "value")
                                                    {
                                                        sb.Append("'" + local2rootdata.Value.ToString() + "@yahoo.co.in',");

                                                    }
                                                }
                                            if (flag == 1)
                                            {
                                                local2root = JObject.Parse(localarraydata.ToString());
                                                foreach (var local2rootdata in local2root)
                                                {
                                                    if (local2rootdata.Key == "value")
                                                    {

                                                        try
                                                        {
                                                            JObject local2rootobject = JObject.Parse(local2rootdata.Value.ToString());

                                                            foreach (var local2rootobjectdata in local2rootobject)
                                                            {
                                                                if (local2rootobjectdata.Key == "givenName")
                                                                {
                                                                    sb.Append("'" + local2rootobjectdata.Value.ToString() + "',");
                                                                }
                                                                if (local2rootobjectdata.Key == "familyName")
                                                                {
                                                                    sb.Append("'" + local2rootobjectdata.Value.ToString() + "')");
                                                                    try
                                                                    {
                                                                        MySqlCommand command = new MySqlCommand(sb.ToString(), con);
                                                                        string data5 = sb.ToString();
                                                                        command.ExecuteNonQuery();
                                                                    }
                                                                    catch (Exception exc)
                                                                    {
                                                                        Console.WriteLine(exc.Message);
                                                                    }
                                                                    sb.Clear();
                                                                    flag = 0;
                                                                }
                                                            }
                                                        }
                                                        catch (Exception exc) { Console.Write(exc.Message); }
                                                    }
                                                }
                                            }
                                            flag++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            }
            catch (Exception exc) { Console.WriteLine(exc.Message); }

        }
        private void twitterSaveData(string json)
        {
            MySqlConnection con = new MySqlConnection(connection);
            try
            {
                con.Open();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
            try
            {
                StringBuilder sb = new StringBuilder();
                JObject root = JObject.Parse(json);
                JArray items = (JArray)root["users"];
                foreach (var data in items)
                {
                    sb.Append("insert into OauthData (oauthclient,id,id_str,name,screen_name,location,url,description,protected,followers_count,freinds_count,listed_count,created_at ,favourites_count,utc_offset,time_zone,geo_enable,verified,statuses_count,lang,contributorS_enabled,is_translator,profile_background_color,profile_background_image_url ,profile_background_image_url_https,profile_background_tile,profile_image_url,profile_image_url_https,profile_link_color,profile_sidebar_border_color,profile_sidebar_fill_color,profile_text_color,profile_use_background_image,default_profile,deault_profile_image,following,follow_request_sent,notification) values ('twitter',");
                    JObject localdata = JObject.Parse(data.ToString());
                    foreach (var localobject in localdata)
                    {
                        if (localobject.Key.ToString() != "notifications")
                        {
                            sb.Append("'" + localobject.Value.ToString() + "',");
                        }
                        else
                        {
                            sb.Append("'" + localobject.Value.ToString() + "')");
                            try
                            {
                                MySqlCommand command = new MySqlCommand(sb.ToString(), con);
                                command.ExecuteNonQuery();

                            }
                            catch (Exception exc)
                            {
                                Console.WriteLine(exc.Message);
                            }
                            sb.Clear();
                        }
                    }

                }
            }
            catch (Exception exc) { Console.WriteLine(exc.Message); }
        }
        private void facebookSaveData(string json)
        {
            MySqlConnection con = new MySqlConnection(connection);
            string oauthclientfriendid = null;
            MySqlCommand command = new MySqlCommand("select oauthid from OauthData where emailaddress='" + emailaddress + "'", con);
            try
            {
                con.Open();
                MySqlDataReader reader = command.ExecuteReader();
                reader.Read();
                oauthclientfriendid = reader[0].ToString();
                reader.Close();
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
            }
          
            try
            {
                StringBuilder sb = new StringBuilder();
                JObject root = JObject.Parse(json);
                JArray items = (JArray)root["data"];
                foreach (var data in items)
                {
                    sb.Append("insert into OauthData (oauthclient,oauthclientfriendid,name,id) values ('facebook','"+oauthclientfriendid+"',");
                    JObject localdata = JObject.Parse(data.ToString());

                    foreach (var localobject in localdata)
                    {
                        if (localobject.Key.ToString() == "name")
                        {
                            sb.Append("'" + localobject.Value.ToString() + "',");
                        }
                        else
                        {
                            sb.Append("'" + localobject.Value.ToString() + "')");
                            try
                            {
                                 command = new MySqlCommand(sb.ToString(), con);
                                string data5 = sb.ToString();
                                command.ExecuteNonQuery();

                            }
                            catch (Exception exc)
                            {
                                Console.WriteLine(exc.Message);
                            }
                            sb.Clear();
                        }
                    }
                }
            }
            catch (Exception exc) {
                Console.WriteLine(exc.Message);
            }
        }
        public  List<KeyValuePair<string, string>> profilejsonparser(string Jsonreturnbyserver, string website)
        {
            List<KeyValuePair<string, string>> keyvalue = new List<KeyValuePair<string, string>>();
           if (website == "linkedin")
           {
            keyvalue=  linkedinPrifileinKeyValuePair(Jsonreturnbyserver);
           }
           //if (website == "yahoo")
           //{
           //  //  yahooSaveData(Jsonreturnbyserver);
           //}
           //if (website == "twitter")
           //{
           //    returntopage = twitterdisplayData(Jsonreturnbyserver);
           //}
           //if (website == "facebook")
           //{
           //   returntopage =  fbdisplayData(Jsonreturnbyserver);
           //}
           return keyvalue;
   }
        private List<KeyValuePair<string, string>> linkedinPrifileinKeyValuePair(string json)
        {
            List<KeyValuePair<string, string>> keyvalue = new List<KeyValuePair<string, string>>();
            StringBuilder sb = new StringBuilder();
            JObject root = JObject.Parse(json);
            foreach (var data in root)
            {
                if (!data.Key.Equals("skills"))
                {
                    keyvalue.Add(new KeyValuePair<string, string>(data.Key.ToString(), data.Value.ToString()));
                }
                else
                {
                    JObject skillroot = JObject.Parse(data.Value.ToString());

                    foreach (var skillrootdata in skillroot)
                    {
                        if (skillrootdata.Key.Equals("values"))
                        {

                            JArray skillRootDataArr = (JArray)skillroot["values"];
                            foreach (var arrObj in skillRootDataArr)
                            {
                                JObject arrobjkeyvalue = JObject.Parse(arrObj.ToString());
                                foreach (var skillname in arrobjkeyvalue)
                                {
                                    if (skillname.Key.Equals("skill"))
                                    {
                                        JObject nameofskill = JObject.Parse(skillname.Value.ToString());
                                        foreach (var finalnameskill in nameofskill)
                                        {
                                            
                                                sb.Append(finalnameskill.Value.ToString() + ",");
                                            
                                        }


                                    }
                                }
                            }
                        }
                    }
                    keyvalue.Add(new KeyValuePair<string,string>(data.Key.ToString(),sb.ToString()));
                }

            }

            return keyvalue;
        }
        private string[] yahoodisplayData()
       {
           string[] displaydata = null;


           return displaydata;
       }
        private string[] fbdisplayData(string json)
       {
           string[] displaydata = new string[3];
             JObject root = JObject.Parse(json);
              //  JArray items = (JArray)root["data"];
                foreach (var data in root)
                {
                    if( data.Key=="first_name"){
                        displaydata[0] = data.Value.ToString();
                        
                    }
                    if(data.Key=="last_name"){
                    displaydata[1]=data.Value.ToString();
                    }
                    if(data.Key=="email"){
                    displaydata[2]=data.Value.ToString();
                    }
                
                }

           return displaydata;
       }
        private string[] twitterdisplayData(string json)
       {
           string[] displaydata = new string[2];
           JObject root = JObject.Parse(json);
           //  JArray items = (JArray)root["data"];
           foreach (var data in root)
           {
               if (data.Key == "name")
               {
                   displaydata[0] = data.Value.ToString();

               }
               if (data.Key == "email")
               {
                   displaydata[1] = data.Value.ToString();
               }
           }
           return displaydata;
       }
        public List<string> SplitTextByWord(string text, string splitTerm)
        {
            List<string> splitItems = new List<string>();
            if (string.IsNullOrEmpty(text)) return splitItems;
            if (string.IsNullOrEmpty(splitTerm))
            {
                splitItems.Add(text);
                return splitItems;
            }
            int nextPos = 0;
            int curPos = 0;
            while (nextPos > -1)
            {
                nextPos = text.IndexOf(splitTerm, curPos);
                if (nextPos != -1)
                {
                    splitItems.Add(text.Substring(curPos, nextPos - curPos));
                    curPos = nextPos + splitTerm.Length;
                }
            }
            splitItems.Add(text.Substring(curPos, text.Length - curPos));

            return splitItems;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Web;
using Test.Api;
using System.Data;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Text;

/// <summary>
/// Summary description for SearchEngine
/// </summary>
public class SearchEngine
{
    DataModel model = new DataModel();
    DataTable dt = new DataTable();
    public SearchEngine()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void uploadDataIntoRSM(string expression, string mimeType, string rmSubject, string refrenceType)
    {
        try
        {
            ISearchResult searchClass = new GoogleSearch(expression);
            var list = searchClass.Search();
            for (int i = 0; i < list.Count; i++)
            {
                string URl = list[i].url;
                model.insertIntoTable("insert into RefrenceMaterials (refrenceType,refrencRefID," +
                    "rmUrl,searchTag,rmStatus,rmAddedOn,rmViewCount,rmSubject,rmMimeType) values ('" + refrenceType + "',null,'" + URl
                    + "'," + "'" + expression + "',2,now(),null,'" + rmSubject + "','" + mimeType + "')");
                FileStream fs = new FileStream(@"d:\ScheduledService.txt", FileMode.OpenOrCreate, FileAccess.Write);

                //set up a streamwriter for adding text
                StreamWriter sw = new StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine("URl=" + URl + ", SearchOn= " + expression + "   ");
                //add the text to the underlying filestream

                sw.Flush();
                //close the writer
                sw.Close();
            }

        }
        catch (Exception exc) { Console.Write(exc.Message); }
    }
    public void searchCompanyOnLinkedin()
    {
        dt = model.selectIntoTable("select currentCompanyName from Users");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i][0].ToString() != "")
                uploadDataIntoRSM(dt.Rows[i][0].ToString() + " on linkedin", "web url", "company on linkedin", "c");

        }
    }
    public void searchVideoForCompanies()
    {
        int googleCount = 0;
        try
        {
            dt = model.selectIntoTable("select companyName,cid from Company order by cid limit 4261,7000");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,priority From UrlFilter where searchType=5 and associatedRefID=0 union select searchTag," +
                    "searchOn,filterID,priority From UrlFilter where searchType=5 and associatedRefID=" + Convert.ToInt32(dt.Rows[i][1]) + " order by priority desc");
                for (int j = 0; j < dt2.Rows.Count; j++)
                {
                    if (dt.Rows[i][0].ToString() != "")
                    //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                    {
                        if (dt2.Rows[j][1].ToString().Equals("google"))
                        {

                            string temp = dt2.Rows[j][0].ToString();
                            string temp2 = dt.Rows[i][0].ToString();
                            string searchTag = string.Format(temp, temp2);
                            if (googleCount <= 90)
                            {
                                // searchOnGoggle(searchTag, Convert.ToInt32(dt.Rows[i][1]), "c", 5, Convert.ToInt32(dt2.Rows[j][2]));
                                // googleCount++;
                            }
                            else
                            {
                                return;
                            }
                        }
                        else if (dt2.Rows[j][1].ToString().Equals("youtube"))
                        {
                            string temp = dt2.Rows[j][0].ToString();
                            string temp2 = dt.Rows[i][0].ToString();
                            string searchTag = string.Format(temp, temp2);
                            searchOnYoutube(searchTag, Convert.ToInt32(dt.Rows[i][1]), "c", 5, 0, Convert.ToInt32(dt2.Rows[j][2]));
                        }
                    }

                }
            }
        }
        catch (Exception exc) {
          string upload=  Path.Combine(HttpRuntime.AppDomainAppPath, "uploadJobs//" );
            System.IO.File.WriteAllText(upload+"error.txt", exc.Message+" /n" );
        }
    }
    public void searchVideoForSkills()
    {
        int googleCount = 0;
       
        dt = model.selectIntoTable("select luvValueDisplayName,luvid from ListofValues where luvCategory='skills'");
        for ( int i = 0; i < dt.Rows.Count; i++)
        {
            DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,limitResult,priority From UrlFilter where searchType=1 and associatedRefID=0 union select searchTag," +
                "searchOn,filterID,limitResult,priority From UrlFilter where searchType=1 and associatedRefID=" + Convert.ToInt32(dt.Rows[i][1]) + " order by priority desc");
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                if (dt.Rows[j][0].ToString() != "")
                //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                {
                    if (dt2.Rows[j][1].ToString().Equals("google"))
                    {
                        string temp = dt2.Rows[j][0].ToString();
                        string temp2 = dt.Rows[i][0].ToString();
                        string searchTag = string.Format(temp, temp2);
                        if (googleCount < 90)
                        {
                            searchOnGoggle(searchTag, Convert.ToInt32(dt.Rows[i][1]), "s", Convert.ToInt32(dt2.Rows[j][3]), Convert.ToInt32(dt2.Rows[j][2]));
                            googleCount++;
                        }
                        else {
                            return;
                        }
                    }
                    else if (dt2.Rows[j][1].ToString().Equals("youtube"))
                    {
                        string temp = dt2.Rows[j][0].ToString();
                        string temp2 = dt.Rows[i][0].ToString();
                        string searchTag = string.Format(temp, temp2);
                        searchOnYoutube(searchTag, Convert.ToInt32(dt.Rows[j][1]), "s", Convert.ToInt32(dt2.Rows[j][3]), 0, Convert.ToInt32(dt2.Rows[j][2]));
                    }
                }

            }
        }

        
    }
    public void searchForSingleSkill(int luvid) {
        try
        {
            dt = model.selectIntoTable("select luvValueDisplayName,luvid from ListofValues where luvid=" + luvid);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,priority From UrlFilter where searchType=1 and associatedRefID=0 union select searchTag," +
                     "searchOn,filterID,priority From UrlFilter where searchType=1 and associatedRefID=" + Convert.ToInt32(dt.Rows[i][1]) + " order by priority desc");
                for (int j = 0; j < dt2.Rows.Count; j++)
                {
                    if (dt2.Rows[j][0].ToString() != "")
                    //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                    {
                        if (dt2.Rows[j][1].ToString().Equals("google"))
                        {
                            string temp = dt2.Rows[j][0].ToString();
                            string temp2 = dt.Rows[i][0].ToString();
                            string searchTag = string.Format(temp, temp2);
                            searchOnGoggle(searchTag, Convert.ToInt32(dt.Rows[i][1]), "s", 5, Convert.ToInt32(dt2.Rows[j][2]));
                        }
                        else if (dt2.Rows[j][1].ToString().Equals("youtube"))
                        {
                            string temp = dt2.Rows[j][0].ToString();
                            string temp2 = dt.Rows[i][0].ToString();
                            string searchTag = string.Format(temp, temp2);
                            searchOnYoutube(searchTag, Convert.ToInt32(dt.Rows[i][1]), "s", 5, 0, Convert.ToInt32(dt2.Rows[j][2]));
                        }
                    }

                }
            }
        }
        catch(Exception exc){
        Console.WriteLine(exc.Message);
        }
    }
    public void searchIOForSkills()
    {
        
        int googleCount = 0;
        dt = model.selectIntoTable("select luvValueDisplayName,luvid from ListofValues where luvCategory='skills' limit 92,300");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,priority From UrlFilter where searchType=1 and associatedRefID=0 union select searchTag," +
               "searchOn,filterID,priority From UrlFilter where searchType=1 and associatedRefID=" + Convert.ToInt32(dt.Rows[i][1]) + " order by priority desc");
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                if (dt.Rows[j][0].ToString() != "")
                //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                {
                    int todaycountOFGoogle = totalgoogleCountOfDay();
                    if (todaycountOFGoogle < 90)
                    {
                        if (dt2.Rows[j][1].ToString().Equals("google"))
                        {
                            string temp = dt2.Rows[j][0].ToString();
                            string temp2 = dt.Rows[i][0].ToString();
                            string searchTag = string.Format(temp, temp2);
                            if (googleCount <= 90)
                            {
                                searchOnGoggle(searchTag, Convert.ToInt32(dt.Rows[i][1]), "IQ", 5, Convert.ToInt32(dt2.Rows[j][2]));
                                googleCount++;
                                
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    else if (dt2.Rows[j][1].ToString().Equals("youtube"))
                    {
                        string temp = dt2.Rows[j][0].ToString();
                        string temp2 = dt.Rows[i][0].ToString();
                        string searchTag = string.Format(temp, temp2);
                        searchOnYoutube(searchTag, Convert.ToInt32(dt.Rows[i][1]), "IQ", 5, 0, Convert.ToInt32(dt.Rows[j][2]));
                    }
                }

            }
        }

        
    }
    public void searchOnYoutube(string searchtag, int refid, string reftype, int resultcount, int newlyInsertCount, int urlFilterID)
    {
        try
        {
            
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://gdata.youtube.com/feeds/api/videos?q=" + searchtag + "&start-index=1&max-results=" + resultcount + "&v=2&alt=json");
            request.Method = "GET";
            HttpWebResponse POSTResponse = (HttpWebResponse)request.GetResponse();
            Stream responseStream = POSTResponse.GetResponseStream();
            StreamReader responseReader = new System.IO.StreamReader(responseStream, Encoding.UTF8);
            string responseString = responseReader.ReadToEnd();
            JObject feedobj = JObject.Parse(responseString);
            var feeditems = (JObject)feedobj["feed"];
            JArray entryarray = (JArray)feeditems["entry"];
            string urlofVideo = "";
            string author = "";
            string viecount = "";
            string title = "";
            string lastmodified = "";
            foreach (var entryitem in entryarray)
            {
                JObject entryobj = JObject.Parse(entryitem.ToString());
                JArray linkarray = (JArray)entryobj["link"];
                foreach (var linkitem in linkarray)
                {
                    JObject linkitemobj = JObject.Parse(linkitem.ToString());
                    foreach (var dictionary in linkitemobj)
                    {
                        if (dictionary.Key.Equals("href"))
                        {
                            urlofVideo = dictionary.Value.ToString();
                            break;
                        }
                    }
                    break;
                }
                JArray authrarray = (JArray)entryobj["author"];
                foreach (var linkitem in authrarray)
                {
                    JObject linkitemobj = JObject.Parse(linkitem.ToString());
                    foreach (var dictionary in linkitemobj)
                    {
                        if (dictionary.Key.Equals("name"))
                        {
                            JObject nameobj = JObject.Parse(dictionary.Value.ToString());
                            foreach (var dictionary2 in nameobj)
                            {
                                if (dictionary2.Key.Equals("$t"))
                                {
                                    author = dictionary2.Value.ToString();
                                }
                            }
                            break;
                        }
                    }
                    break;
                }
                JObject ytstatistics = (JObject)entryobj["yt$statistics"];
                foreach (var dictionary in ytstatistics)
                {
                    if (dictionary.Key.Equals("viewCount"))
                    {
                        viecount = dictionary.Value.ToString();
                        break;
                    }
                }
                JObject titleobj = (JObject)entryobj["title"];
                foreach (var dictionary in titleobj)
                {
                    if (dictionary.Key.Equals("$t"))
                    {
                        title = dictionary.Value.ToString();
                        break;
                    }
                }
                JObject publishobj = (JObject)entryobj["published"];
                foreach (var dictionary in publishobj)
                {
                    if (dictionary.Key.Equals("$t"))
                    {
                        lastmodified = dictionary.Value.ToString();
                        break;
                    }
                }

                if (newlyInsertCount != 6)
                {
                    Boolean result = insertintoRefrenceMaterial(lastmodified,title, author, urlofVideo, viecount, refid, reftype, searchtag,"video",urlFilterID);
                    if (result == true)
                    {
                        newlyInsertCount++;
                    }
                }
           
            }
            if (newlyInsertCount >= 5) {
                return;
            }
            searchOnYoutube(searchtag, refid, reftype, resultcount + 5, newlyInsertCount,urlFilterID);
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
        }

    }
    private Boolean insertintoRefrenceMaterial(string addedON,string subject,string aothor, string url, string viewCount, int refid, string reftype, string searchTag,string mimetype,int urlFilterID)
    {
 
        //dt = model.selectIntoTable("select searchTag From UrlFiter where searchFor=1");
      DataTable  dt=model.selectIntoTable("select rmID from RefrenceSuggetionMaterials where rmUrl='"+url+"' and RefrenceRefID="+refid);
        if(dt.Rows.Count==0){
      dt= model.insertIntoTableWithLastId("insert into RefrenceSuggetionMaterials (refrenceType,RefrenceRefID," +
                    "rmUrl,searchTag,rmStatus,rmAddedOn,rmViewCount,rmSubject,rmMimeType,viewCount,author,urlFilterRefID) values ('" + reftype + "'," + refid + ",'" + url
                    + "'," + "'" + searchTag + "',2,'"+addedON+"',null,'" + subject + "','" + mimetype + "'," + viewCount + ",'" + aothor + "','" + urlFilterID + "')", "rmid");
      if (dt.Rows.Count != 0)
      {
         return true;
      }
        }

        return false;
    }
    public void searchOnGoggle(string searchtag, int refid, string reftype,int resultCount,int urlFilterID)
    { 
     
                   
        try
        {
          int todaycountOFGoogle = totalgoogleCountOfDay();
          if (todaycountOFGoogle < 90)
          {
              googleCountinDay(urlFilterID, refid, reftype);
              int start = 1;
              int newlyInsertCount = 0;
              /*AIzaSyCeYwwW_AEwNGKFZA1H7V-6ru2V2JXLxCk*/
              HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://www.googleapis.com/customsearch/v1?key=AIzaSyCeYwwW_AEwNGKFZA1H7V-6ru2V2JXLxCk&cx=013036536707430787589:_pqjad5hr1a&q="
                  + searchtag + "&alt=json&num=" + resultCount + "&start=" + start);
              request.Method = "GET";
              HttpWebResponse POSTResponse = (HttpWebResponse)request.GetResponse();
              Stream responseStream = POSTResponse.GetResponseStream();
              StreamReader responseReader = new System.IO.StreamReader(responseStream, Encoding.UTF8);
              string responseString = responseReader.ReadToEnd();
              JObject feedobj = JObject.Parse(responseString);

              JArray itemArrray = (JArray)feedobj["items"];
              string url = "";
              string title = "";
              foreach (var itemElement in itemArrray)
              {
                  JObject itemObj = JObject.Parse(itemElement.ToString());
                  foreach (var itemOFentry in itemObj)
                  {
                      if (itemOFentry.Key.Equals("title"))
                      {
                          title = itemOFentry.Value.ToString();
                      }
                      if (itemOFentry.Key.Equals("link"))
                      {
                          url = itemOFentry.Value.ToString();
                          break;
                      }

                  }
                  if (newlyInsertCount != 6)
                  {
                      Boolean result = insertintoRefrenceMaterial(null, title, "", url, "0", refid, reftype, searchtag, "page", urlFilterID);
                      if (result == true)
                      {
                          newlyInsertCount++;
                      }
                  }
                  if (newlyInsertCount >= resultCount)
                  {
                      return;
                  }

              }
          }
            //searchOnGoggle(searchtag, refid, reftype, resultCount + 5);
        }
        catch (Exception ecx)
        {
            Console.WriteLine(ecx.Message);
        }

    }

    public void searchIQForCompanyWithSkills()
    {
         DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,priority From UrlFilter where searchType=6 and associatedRefID=0 union select searchTag," +
               "searchOn,filterID,priority From UrlFilter where searchType=6 and associatedRefID=0 order by priority desc");
        DataTable dt3 = model.selectIntoTable("select luvValueDisplayName  from ListofValues where luvCategory='skills'");
         dt = model.selectIntoTable("select companyName,cid from Company order by cid");
         for (int k = 0; k < dt3.Rows.Count; k++)
       {
        for (int i = 0; i < dt.Rows.Count; i++)
        {
           
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                if (dt2.Rows[j][0].ToString() != "")
                //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                {

                    if (dt2.Rows[j][1].ToString().Equals("youtube"))
                    {
                        string temp3 = dt3.Rows[k][0].ToString();
                        string temp = dt2.Rows[j][0].ToString();
                        string temp2 = dt.Rows[i][0].ToString();
                        string searchTag = string.Format(temp, temp3, temp2);
                        searchOnYoutube(searchTag, Convert.ToInt32(dt.Rows[i][1]), "CSIQ", 5, 0, Convert.ToInt32(dt2.Rows[j][2]));
                    }
                }
             

            }
        }
    }


    }

    public void searchIQForAllCompany(int fromwheresearchStartIndex)
    {
        DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,priority From UrlFilter where searchType=6 and associatedRefID=0 union select searchTag," +
              "searchOn,filterID,priority From UrlFilter where searchType=7 and associatedRefID=0 order by priority desc");
       DataTable dt3 ;// model.selectIntoTable("select luvValueDisplayName  from ListofValues where luvCategory='skills'");
       dt3 = model.selectIntoTable("select companyName,cid from Company  where cid >= " + fromwheresearchStartIndex + " order by cid");
        int googleCount = 0;
        for (int companyInc = 0; companyInc < dt3.Rows.Count; companyInc++)
            {

                for (int filterInc = 0; filterInc < dt2.Rows.Count; filterInc++)
                {
                    if (dt2.Rows[filterInc][0].ToString() != "")
                    //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                    {

                        if (dt2.Rows[filterInc][1].ToString().Equals("google"))
                        {
                            string temp = dt2.Rows[filterInc][0].ToString();
                            string temp2 = dt3.Rows[companyInc][0].ToString();
                            string searchTag = string.Format(temp, temp2);
                            if (googleCount <= 90)
                            {
                                searchOnGoggle(searchTag, Convert.ToInt32(dt3.Rows[companyInc][1]), "CIQ", 10, Convert.ToInt32(dt2.Rows[filterInc][2]));
                                googleCount++;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }


                }
            }
        


    }

    public void searchIQForCompany(int cid)
    {
        DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,priority From UrlFilter where searchType=7 and associatedRefID=0  order by priority desc");
        //DataTable dt3 = model.selectIntoTable("select luvValueDisplayName  from ListofValues where luvCategory='skills'");
        dt = model.selectIntoTable("select companyName,cid from Company where cid="+cid+" order by cid");
        int googleCount = 0;
        for (int companyInc = 0; companyInc < dt.Rows.Count; companyInc++)
        {

            for (int filterInc = 0; filterInc < dt2.Rows.Count; filterInc++)
            {
                if (dt2.Rows[filterInc][0].ToString() != "")
                //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                {

                    if (dt2.Rows[filterInc][1].ToString().Equals("google"))
                    {
                        string temp = dt2.Rows[filterInc][0].ToString();
                        string temp2 = dt.Rows[companyInc][0].ToString();
                        string searchTag = string.Format(temp, temp2);
                        if (googleCount <= 90)
                        {
                            searchOnGoggle(searchTag, Convert.ToInt32(dt.Rows[companyInc][1]), "CIQ", 5, Convert.ToInt32(dt2.Rows[filterInc][2]));
                            googleCount++;
                        }
                        else
                        {
                            return;
                        }
                    }
                }


            }
        }



    }



    public void searchReviewForCompany()
    {
        int googleCount = 0;
        dt = model.selectIntoTable("select companyName,cid from Company order by cid limit 3323,8000");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,priority From UrlFilter where searchType=4 and associatedRefID=0 union select searchTag," +
               "searchOn,filterID,priority From UrlFilter where searchType=1 and associatedRefID=" + Convert.ToInt32(dt.Rows[i][1]) + " order by priority desc");
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                if (dt.Rows[j][0].ToString() != "")
                //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                {
                    //if (dt2.Rows[j][1].ToString().Equals("google"))
                    //{
                    //    string temp = dt2.Rows[j][0].ToString();
                    //    string temp2 = dt.Rows[i][0].ToString();
                    //    string searchTag = string.Format(temp, temp2);
                    //    if (googleCount <= 90)
                    //    {
                    //        searchOnGoggle(searchTag, Convert.ToInt32(dt.Rows[i][1]), "r",5, Convert.ToInt32(dt2.Rows[j][2]));
                    //        googleCount++;
                    //    }
                    //    else
                    //    {
                    //        return;
                    //    }
                    //}
                    //else 
                    if (dt2.Rows[j][1].ToString().Equals("youtube"))
                    {
                        string temp = dt2.Rows[j][0].ToString();
                        string temp2 = dt.Rows[i][0].ToString();
                        string searchTag = string.Format(temp, temp2);
                        searchOnYoutube(searchTag, Convert.ToInt32(dt.Rows[i][1]), "r", 5, 0, Convert.ToInt32(dt2.Rows[j][2]));
                    }
                }

            }
        }


    }

    public void searchIOForSingleSkills(int luvid)
    {
        try
        {
            int googleCount = 0;
            dt = model.selectIntoTable("select luvValueDisplayName,luvid from ListofValues where luvid=" + luvid);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataTable dt2 = model.selectIntoTable("select searchTag,searchOn,filterID,priority From UrlFilter where searchType=2 and associatedRefID=0 union select searchTag," +
                   "searchOn,filterID,priority From UrlFilter where searchType=2 and associatedRefID=" + Convert.ToInt32(dt.Rows[i][1]) + " order by priority desc");
                for (int j = 0; j < dt2.Rows.Count; j++)
                {
                    if (dt2.Rows[j][0].ToString() != "")
                    //uploadDataIntoRSM("video tutorials:" + dt.Rows[i][0].ToString(), "Video","skills video","s");
                    {
                        if (dt2.Rows[j][1].ToString().Equals("google"))
                        {
                            string temp = dt2.Rows[j][0].ToString();
                            string temp2 = dt.Rows[i][0].ToString();
                            string searchTag = string.Format(temp, temp2);
                            if (googleCount <= 90)
                            {
                                searchOnGoggle(searchTag, Convert.ToInt32(dt.Rows[i][1]), "IQ", 10, Convert.ToInt32(dt2.Rows[j][2]));
                                googleCount++;
                            }
                            else
                            {
                                return;
                            }
                        }
                        else if (dt2.Rows[j][1].ToString().Equals("youtube"))
                        {
                            string temp = dt2.Rows[j][0].ToString();
                            string temp2 = dt.Rows[i][0].ToString();
                            string searchTag = string.Format(temp, temp2);
                            searchOnYoutube(searchTag, Convert.ToInt32(dt.Rows[j][1]), "TQ", 5, 0, Convert.ToInt32(dt.Rows[j][2]));
                        }
                    }

                }
            }
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message
                );
        }


    }

    public void googleCountinDay(int filterId,int refrenceId,string typeForRefrenceId) {

        dt = model.selectIntoTable("select searchCount from UrlFilter where filterId="+filterId+" and Date(searchdate)=Date(now())");
        if(dt.Rows.Count>0)    //update count by 1 in filter
        {
            model.updateIntoTablewithRowCount("update UrlFilter set searchCount=searchCount+1  where filterId=" + filterId);
        }
        else   // update count by 1 in filter
        {
            model.updateIntoTablewithRowCount("update UrlFilter set searchCount=1,searchdate=now()  where filterId=" + filterId);
        }
        if (typeForRefrenceId == "c" || typeForRefrenceId == "CIQ" || typeForRefrenceId == "CSIQ")
        {
            model.updateIntoTablewithRowCount("update Company set searchdate=now()  where cid=" + refrenceId);
        }
        else if (typeForRefrenceId == "s" || typeForRefrenceId == "IQ")
        {
            model.updateIntoTablewithRowCount("update ListofValue set searchdate=now()  where luvId=" + refrenceId);
        }
    
    }
    public int totalgoogleCountOfDay() {
        int countOfGoggle = 0;
        dt = model.selectIntoTable("select sum(searchCount) from UrlFilter where  Date(searchdate)=Date(now()) and searchOn='google'");
        if (dt.Rows.Count > 0) {
            countOfGoggle = Convert.ToInt32(dt.Rows[0][0]);
        }

        return countOfGoggle;
    
    }
    public int searchForWhichId( string RefrenceType) {
        int refrenceId = 0;
        DataTable tableForRefrence=new DataTable();
        if (RefrenceType == "c") {
            tableForRefrence = model.selectIntoTable("select cid+1 cid from Company Order by searchDate desc limit 0,1");
        }
        else if (RefrenceType == "s") {
            tableForRefrence = model.selectIntoTable("select luvId+1 luvId from ListofValues Order by searchDate desc limit 0,1");     

        }
        if (tableForRefrence.Rows.Count > 0)
        {
           refrenceId= Convert.ToInt32(tableForRefrence.Rows[0][0]);
        }
        return refrenceId;
    }
}
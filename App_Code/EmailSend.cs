﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.IO;

/// <summary>
/// Summary description for EmailSend
/// </summary>
public class EmailSend
{
    public EmailSend()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void sendMailWithTemplate(string subject, string displayName, string address, string message, string templatePath, string unsubscribepath)
    {
        var fromAddress = new MailAddress(" TalentNest@CSimplifyIT.com", "Talent Nest");
        try
        {
            string relativePath = "templates\\" + templatePath;
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), displayName, message, address, unsubscribepath);
            var toAddress = new MailAddress(address);
            const string fromPassword = "qHdv_=+n/8rA^<b";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),

            };
            using (var message1 = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message1);
            }
        }
        catch (Exception exc)
        {
            throw exc;
        }

    }
    public void sendMailForChangePassword(string subject, string displayName, string address, string message, string templatePath, string unsubscribepath)
    {
        var fromAddress = new MailAddress(" TalentNest@CSimplifyIT.com", "Talent Nest");
        try
        {
            string relativePath = "templates\\ChangePassword.html";
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), displayName, message, address, unsubscribepath);
            var toAddress = new MailAddress(address);
            const string fromPassword = "qHdv_=+n/8rA^<b";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),

            };
            using (var message1 = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message1);
            }
        }
        catch (Exception exc)
        {
            throw exc;
        }

    }

    public void sendMailWithTemplate1(string address, string templateName, string displayName, string jobTitle, string table, string unsubscribedPath)
    {
        var fromAddress = new MailAddress(" TalentNest@CSimplifyIT.com", "Talent Nest");
        try
        {
            string relativePath = "templates\\" + templateName;
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), displayName, jobTitle, table, address, unsubscribedPath);
            var toAddress = new MailAddress(address);
            const string fromPassword = "qHdv_=+n/8rA^<b";
            string subject = "Get Job '" + jobTitle + "' Now ";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
            };
            MailMessage message1 = new MailMessage(fromAddress, toAddress);
            message1.Subject = subject;
            message1.Body = body;
            message1.IsBodyHtml = true;
            smtp.Send(message1);
        }

        catch (Exception exc)
        {
            throw exc;
        }

    }
    public void sendMailWithTemplate(string address, string filePath, string templateName, string displayName, string jobTitle, string postedOn, string expiresOn, string jobComments, string unsubscribedPath)
    {
        var fromAddress = new MailAddress(" TalentNest@CSimplifyIT.com", "Talent Nest");
        try
        {
            string relativePath = "templates\\" + templateName;
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), displayName, jobTitle, postedOn, expiresOn, jobComments, unsubscribedPath);
            var toAddress = new MailAddress(address);
            const string fromPassword = "qHdv_=+n/8rA^<b";
            Attachment attach = new Attachment(filePath);
            const string subject = "Resumes of friends from Talent Nest";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
            };
            MailMessage message1 = new MailMessage(fromAddress, toAddress);
            message1.Attachments.Add(attach);
            message1.Subject = subject;
            message1.Body = body;
            message1.IsBodyHtml = true;
            smtp.Send(message1);
        }

        catch (Exception exc)
        {
            throw exc;
        }

    }
    public void sendMailWithTemplate(string emailAddress, string htmlString, string skill)
    {
        var fromAddress = new MailAddress("freshervilla@CSimplifyIT.com", "Talent Nest");
        try
        {

            string relativePath = "templates\\sendMailForSkill.html";
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), emailAddress, htmlString);

            var toAddress = new MailAddress(emailAddress);

            var toCCAddress = new MailAddress("dmehta104@gmail.com");
            const string fromPassword = "qHdv_=+n/8rA^<b";

            string subject = "Learn " + skill + " using freshersvilla skill builder";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
            };
            MailMessage message1 = new MailMessage(fromAddress, toAddress);
            message1.CC.Add(toCCAddress);
            message1.Subject = subject;
            message1.Body = body;
            message1.IsBodyHtml = true;
            smtp.Send(message1);
        }

        catch (Exception exc)
        {
            throw exc;
        }

    }

    public void sendMailForGenerateReport(string emailAddress, string htmlString, string skill, string fromDate, string toDate)
    {
        var fromAddress = new MailAddress("freshervilla@CSimplifyIT.com", "Talent Nest");
        try
        {

            string relativePath = "templates\\generateReport.html";
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), emailAddress, fromDate, toDate, htmlString);

            var toAddress = new MailAddress(emailAddress);


            const string fromPassword = "qHdv_=+n/8rA^<b";

            string subject = string.Format("Here is your report from {0} to {1}", fromDate, toDate);
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
            };
            MailMessage message1 = new MailMessage(fromAddress, toAddress);
            message1.Bcc.Add("freshervilla-mentors@googlegroups.com");

            message1.Subject = subject;
            message1.Body = body;
            message1.IsBodyHtml = true;
            smtp.Send(message1);
        }

        catch (Exception exc)
        {
            //throw exc;
        }

    }

    public void sendMailAsesmentVideo(string emailAddress, string htmlString, string skill, string fromDate, string toDate)
    {
        var fromAddress = new MailAddress("freshervilla@CSimplifyIT.com", "Talent Nest");
        try
        {

            string relativePath = "templates\\generateReport.html";
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), emailAddress, fromDate, toDate, htmlString);

            var toAddress = new MailAddress(emailAddress);


            const string fromPassword = "qHdv_=+n/8rA^<b";

            string subject = string.Format("Here is your report from {0} to {1}", fromDate, toDate);
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
            };
            MailMessage message1 = new MailMessage(fromAddress, toAddress);
            message1.Bcc.Add("freshervilla-mentors@googlegroups.com");

            message1.Subject = subject;
            message1.Body = body;
            message1.IsBodyHtml = true;
            smtp.Send(message1);
        }

        catch (Exception exc)
        {
            throw exc;
        }

    }

    public void sendMailForUploadedVideo(string emailAddress, string htmlString, string skill, string url)
    {
        var fromAddress = new MailAddress("freshervilla@CSimplifyIT.com", "Talent Nest");
        try
        {

            string relativePath = "templates\\uploadedVideo.html";
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), emailAddress, htmlString);

            var toAddress = new MailAddress(emailAddress);


            const string fromPassword = "qHdv_=+n/8rA^<b";

            string subject = "Your video is uploaded successfully. Your url is " + url;
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
            };
            MailMessage message1 = new MailMessage(fromAddress, toAddress);
            message1.Bcc.Add("freshervilla-mentors@googlegroups.com");

            message1.Subject = subject;
            message1.Body = body;
            message1.IsBodyHtml = true;
            smtp.EnableSsl = true;
            smtp.Send(message1);
        }

        catch (Exception exc)
        {
            throw exc.InnerException;
        }

    }

    public void sendcontent(string emailAddress, string htmlString, string url, string title, string VarificationCode, string Name)
    {
        var fromAddress = new MailAddress("freshervilla@CSimplifyIT.com", "Talent Nest");
        try
        {

            string relativePath = "templates\\sendjob.html";
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = "";
            if (VarificationCode != "")
            {
                body = string.Format(File.ReadAllText(str_uploadpath), Name, htmlString, url);
            }
            else
            {
                body = string.Format(File.ReadAllText(str_uploadpath), "There", htmlString, url);
            }
            var toAddress = new MailAddress(emailAddress);


            const string fromPassword = "qHdv_=+n/8rA^<b";

            string subject = title;
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
            };
            MailMessage message1 = new MailMessage(fromAddress, toAddress);
            message1.Bcc.Add("freshervilla-mentors@googlegroups.com");

            message1.Subject = subject;
            message1.Body = body;
            message1.IsBodyHtml = true;
            smtp.Send(message1);
        }

        catch (Exception exc)
        {
            throw exc;
        }

    }


    public void sendMailWithTemplateForEnroll(string subject, string candidateName, string userName, string password, string templatePath)
    {
        var fromAddress = new MailAddress("freshervilla@CSimplifyIT.com", "Talent Nest");
        try
        {
            string relativePath = "templates\\" + templatePath;
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), candidateName, userName, password);
            var toAddress = new MailAddress(userName);

            const string fromPassword = "qHdv_=+n/8rA^<b";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),

            };
            using (var message1 = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })

            {
                message1.Bcc.Add("freshervilla-managers@googlegroups.com");
                smtp.Send(message1);
            }
        }
        catch (Exception exc)
        {

        }

    }

    public void sendMailWhenVideoIsUploaded(string subject, string candidateName, string userName, string password, string templatePath)
    {
        var fromAddress = new MailAddress("freshervilla@CSimplifyIT.com", "Talent Nest");
        try
        {
            string relativePath = "templates\\" + templatePath;
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
            string body = string.Format(File.ReadAllText(str_uploadpath), candidateName, userName, password);
            var toAddress = new MailAddress(userName);

            const string fromPassword = "qHdv_=+n/8rA^<b";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword),

            };
            using (var message1 = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                message1.Bcc.Add("freshervilla-mentors@googlegroups.com");
                smtp.Send(message1);
            }
        }
        catch (Exception exc)
        {

        }

    }
}
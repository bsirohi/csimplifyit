﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for GenrateExcel
/// </summary>
public class GenrateExcel
{
    public GenrateExcel()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string generateExcel(DataTable dt, string fileName)
    {
        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        object misValue = System.Reflection.Missing.Value;
        xlApp = new Excel.ApplicationClass();
        xlWorkBook = xlApp.Workbooks.Add(misValue);
        xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        int c = dt.Columns.Count;
        for (int j = 0; j <= dt.Columns.Count - 1; j++)
        {
            xlWorkSheet.Cells[1, j + 1] = dt.Columns[j].Caption;
        }

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            for (int j = 0; j <= dt.Columns.Count - 1; j++)
            {
                xlWorkSheet.Cells[i + 2, j + 1] = dt.Rows[i][j];
            }
        }
        string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, "excelFiles\\"+fileName+".xls");
        xlWorkBook.SaveAs(str_uploadpath, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
        xlWorkBook.Close(true, misValue, misValue);
        xlApp.Quit();
        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);
        return str_uploadpath;
    }
    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;

        }
        finally
        {
            GC.Collect();
        }
    }
 public string WriteToCsvFile(DataTable dataTable, string fileName,string folderName) {
            StringBuilder fileContent = new StringBuilder();

            foreach (var col in dataTable.Columns) {
                fileContent.Append(col.ToString() + ",");
            }
           fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);
            foreach (DataRow dr in dataTable.Rows) {

                foreach (var column in dr.ItemArray) {
                    fileContent.Append("\"" + column.ToString() + "\",");
                }

                fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);
            }
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, folderName + "\\" + fileName + ".csv");
      System.IO.File.WriteAllText(str_uploadpath, fileContent.ToString());
      return "excelFiles\\" + fileName + ".csv";

        }
}
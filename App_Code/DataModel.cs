﻿using System;
using System.Collections.Generic;

using System.Web;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for DataModel
/// </summary>
public class DataModel
{
    //CSimplifyitException exception;
    string connectionstring = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;

   // string connectionstring = "SERVER=50.62.209.9;PORT=3306;UID=ProdFreshervilla;PWD=Blah182728$;DATABASE=ProdFreshervilla;";

   // string connectionstring = "Server=50.62.209.9;Database=ProdFreshervilla;User=ProdFreshervilla;Password=Blah182728$;";

    //string connectionstring2 = ConfigurationManager.ConnectionStrings["loccn"].ConnectionString;
    public string insertIntoTable(string query) {
         MySqlConnection con = new MySqlConnection(connectionstring);
         try
         {
             con.Open();
             MySqlCommand command = new MySqlCommand(query, con);
             command.ExecuteNonQuery();
             con.Close();
             return "true";
         }
              
         catch(Exception exc) {
             return exc.Message;
         }
         finally { con.Close(); }
    }
    public Boolean updateIntoTable(string query) {
        MySqlConnection con = new MySqlConnection(connectionstring);
        try
        {
          
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            int noOfRecords= command.ExecuteNonQuery();
           
            return true;

        }
        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
       
    
    }
    public Boolean deleteFromTable(string query)
    {
        MySqlConnection con = new MySqlConnection(connectionstring);
        try
        {

            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();       
        }
             
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
        }
        finally { con.Close(); }
        return true;

    }
    public DataTable selectIntoTable(string query) {
        DataTable dt = new DataTable();
        MySqlConnection con = new MySqlConnection(connectionstring);
        MySqlDataReader reader;
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            reader = command.ExecuteReader();
         
            dt.Load(reader);
            
        }
        catch (Exception exc) {
           throw exc;
        }
        finally { con.Close(); }
        return dt ;
    }
    public int updateIntoTablewithRowCount(string query)
    {
        int noOfRecords = 0;
        MySqlConnection con = new MySqlConnection(connectionstring);
        try
        {
           
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            noOfRecords = command.ExecuteNonQuery();

            return noOfRecords;

        }
             
        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }

    }
    public string insert(string query)
    {
        MySqlConnection con = new MySqlConnection(connectionstring);

        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
            return "true";
        }

        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
    }
    public int insertwithRowCount(string query) {
        MySqlConnection con = new MySqlConnection(connectionstring);
        int rowCount = 0;
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
           rowCount= command.ExecuteNonQuery();
            con.Close();
            return rowCount;
        }

        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
    }
    public DataSet selectQueryInDataSet(string query) {
        DataSet ds = new DataSet();
        
        MySqlDataAdapter adp = new MySqlDataAdapter(query, connectionstring);
        try
        {
            adp.Fill(ds); 
        }
        catch (Exception exc)
        {
            throw exc;
        }
        return ds;
    }
    public DataTable insertIntoTableWithLastId(string query,string lastID) {
        DataTable dt=new DataTable();
        MySqlDataReader reader;
        MySqlConnection con = new MySqlConnection(connectionstring);
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            command = new MySqlCommand("SELECT LAST_INSERT_ID() as "+lastID, con);
            reader = command.ExecuteReader();
            dt.Load(reader);
            return dt;
        }

        catch (Exception exc)
        {
            throw exc; 
        }
        finally { con.Close(); }
    
    }
    //public string insertIntoLocalDB(string query)
    //{
    //    MySqlConnection con = new MySqlConnection(connectionstring2);
    //    try
    //    {
    //        con.Open();
    //        MySqlCommand command = new MySqlCommand(query, con);
    //        command.ExecuteNonQuery();
    //        con.Close();
    //        return "true";
    //    }

    //    catch (Exception exc)
    //    {
    //        return exc.Message;
    //    }
    //    finally { con.Close(); }
    //}
    public DataTable selectIntoTableUsingSP(string procedureName, List<KeyValuePair<string, string>> listOfParameters)
    {
        DataTable dt = new DataTable();
        MySqlConnection con = new MySqlConnection(connectionstring);
        MySqlDataReader reader;
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(procedureName, con);
            command.CommandType = CommandType.StoredProcedure;
            foreach (var keyvalue in listOfParameters)
            {
               
            } reader = command.ExecuteReader();

            dt.Load(reader);

        }
        catch
        {
           
        }
        finally { con.Close(); }
        return dt;
    }


}
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Runtime.Remoting.Messaging;
using System.Text;
using csimplifyit;
using MySql.Data.MySqlClient;
using System.Diagnostics;

public class oAuthLinkedIn : oAuthBase
{
    public enum Method { GET, POST, PUT, DELETE };
    public const string REQUEST_TOKEN = "https://api.linkedin.com/uas/oauth/requestToken?scope=r_emailaddress%20r_basicprofile";
    public const string AUTHORIZE = "https://api.linkedin.com/uas/oauth/authenticate";
    public const string ACCESS_TOKEN = "https://api.linkedin.com/uas/oauth/accessToken";
    public const string CALLBACK = "https://CSimplifyIT.com/signup.aspx";
    private string _consumerKey = "vw0yq6pmoc50";
    private string _consumerSecret = "QR77lYaK837yuNoB";
    private string _token = "";
    private string _tokenSecret = "";
    public string Verifier2 = "";
    public string accesstoken = "";
    public string accesssecrettoken = "";
    public const string USER_AGENT = "YourAgent";
    #region Properties
    public string ConsumerKey
    {
        get
        {
            if (_consumerKey.Length == 0)
            {
                _consumerKey = "vw0yq6pmoc50";
            }
            return _consumerKey;
        }
        set { _consumerKey = value; }
    }

    public string ConsumerSecret
    {
        get
        {
            if (_consumerSecret.Length == 0)
            {
                _consumerSecret = "QR77lYaK837yuNoB";
            }
            return _consumerSecret;
        }
        set { _consumerSecret = value; }
    }

    public string Token { get { return _token; } set { _token = value; } }
    public string TokenSecret { get { return _tokenSecret; } set { _tokenSecret = value; } }
    #endregion
    public string AuthorizationLinkGet()
    {
        string ret = null;
        string response = oAuthWebRequest(Method.POST, REQUEST_TOKEN, String.Empty);
        if (response.Length > 0)
        {
            //response contains token and token secret.  We only need the token.
            //oauth_token=36d1871d-5315-499f-a256-7231fdb6a1e0&oauth_token_secret=34a6cb8e-4279-4a0b-b840-085234678ab4&oauth_callback_confirmed=true
            NameValueCollection qs = HttpUtility.ParseQueryString(response);
            if (qs["oauth_token"] != null)
            {
                this.Token = qs["oauth_token"];
                this.TokenSecret = qs["oauth_token_secret"];
                ret = AUTHORIZE + "?oauth_token=" + this.Token;
            }
        }
        return ret;
    }
    public void AccessTokenGet()
    {
        string response = oAuthWebRequest(Method.POST, ACCESS_TOKEN, string.Empty);
        JsonParserSaveData savedata = new JsonParserSaveData();
        if (response.Length > 0)
        {
            //Store the Token and Token Secret
            NameValueCollection qs = HttpUtility.ParseQueryString(response);
            if (qs["oauth_token"] != null)
            {
                this.accesstoken = qs["oauth_token"];
            }
            if (qs["oauth_token_secret"] != null)
            {
                this.accesssecrettoken = qs["oauth_token_secret"];
            }
        }
    }
    public string oAuthWebRequest(Method method, string url, string postData)
    {
        string outUrl = "";
        string querystring = "";
        string ret = "";
        if (method == Method.POST)
        {
            if (postData.Length > 0)
            {
                //Decode the parameters and re-encode using the oAuth UrlEncode method.
                NameValueCollection qs = HttpUtility.ParseQueryString(postData);
                postData = "";
                foreach (string key in qs.AllKeys)
                {
                    if (postData.Length > 0)
                    {
                        postData += "&";
                    }
                    qs[key] = HttpUtility.UrlDecode(qs[key]);
                    qs[key] = this.UrlEncode(qs[key]);
                    postData += key + "=" + qs[key];

                }
                if (url.IndexOf("?") > 0)
                {
                    url += "&";
                }
                else
                {
                    url += "?";
                }
                url += postData;
            }
        }

        Uri uri = new Uri(url);

        string nonce = this.GenerateNonce();
        string timeStamp = this.GenerateTimeStamp();
        string callback = "";
        if (url.ToString().Contains(REQUEST_TOKEN))
            callback = CALLBACK;
        //Generate Signature
        string sig = this.GenerateSignature(uri,
            this.ConsumerKey,
            this.ConsumerSecret,
            this.Token,
            this.TokenSecret,
            method.ToString(),
            timeStamp,
            nonce,
            callback,
            out outUrl,
            out querystring);


        querystring += "&oauth_signature=" + HttpUtility.UrlEncode(sig);

        //Convert the querystring to postData
        if (method == Method.POST)
        {
            postData = querystring;
            querystring = "";
        }

        if (querystring.Length > 0)
        {
            outUrl += "?";
        }
        try
        {
            if (method == Method.POST || method == Method.GET)
                ret = WebRequest(method, outUrl + querystring, postData);
        }
        catch (WebException we)
        {
            var errrorResponse = we.Response as HttpWebResponse;
            try
            {
                var sr = new StreamReader(errrorResponse.GetResponseStream());
                string responseData = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception exc) { Console.WriteLine(exc.Message); }
        }
        //else if (method == Method.PUT)
        //ret = WebRequestWithPut(outUrl + querystring, postData);
        return ret;
    }
    public string APIWebRequest(string method, string url, string postData)
    {

        Uri uri = new Uri(url);
        string nonce = this.GenerateNonce();
        string timeStamp = this.GenerateTimeStamp();
        string outUrl, querystring;
        string sig;
        //Generate Signature
        if (accesstoken == null)
        {
            sig = this.GenerateSignature(uri,
               this.ConsumerKey,
               this.ConsumerSecret,
               this.Token,
               this.TokenSecret,
               method,
               timeStamp,
               nonce,
               null,
               out outUrl,
               out querystring);
        }
        else
        {
            sig = this.GenerateSignature(uri,
              this.ConsumerKey,
              this.ConsumerSecret,
              this.accesstoken,
              this.accesssecrettoken,
              method,
              timeStamp,
              nonce,
              null,
              out outUrl,
              out querystring);
        }

        //querystring += "&oauth_signature=" + HttpUtility.UrlEncode(sig);
        //NameValueCollection qs = HttpUtility.ParseQueryString(querystring);

        //string finalGetUrl = outUrl + "?" + querystring;

        HttpWebRequest webRequest = null;

        //webRequest = System.Net.WebRequest.Create(finalGetUrl) as HttpWebRequest;
        webRequest = System.Net.WebRequest.Create(url) as HttpWebRequest;
        //webRequest.ContentType = "text/xml";
        webRequest.Method = method;
        webRequest.Credentials = CredentialCache.DefaultCredentials;
        webRequest.AllowWriteStreamBuffering = true;

        webRequest.PreAuthenticate = true;

        webRequest.ServicePoint.Expect100Continue = false;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
        if (accesstoken == null)
        {
            webRequest.Headers.Add("Authorization", "OAuth realm=\"http://api.linkedin.com/\",oauth_consumer_key=\"" + this.ConsumerKey + "\",oauth_token=\"" + this.Token + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\",oauth_timestamp=\"" + timeStamp + "\",oauth_nonce=\"" + nonce + "\",oauth_verifier=\"" + this.Verifier + "\", oauth_version=\"1.0\"");
        }
        else
        {

            webRequest.Headers.Add("Authorization", "OAuth realm=\"http://api.linkedin.com/\",oauth_consumer_key=\"" + this.ConsumerKey + "\",oauth_token=\"" + this.accesstoken + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\",oauth_timestamp=\"" + timeStamp + "\",oauth_nonce=\"" + nonce + "\",oauth_verifier=\"" + this.Verifier + "\", oauth_version=\"1.0\"");
        }
        //webRequest.Headers.Add("Authorization", "OAuth oauth_nonce=\"" + nonce + "\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"" + timeStamp + "\", oauth_consumer_key=\"" + this.ConsumerKey + "\", oauth_token=\"" + this.Token + "\", oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\", oauth_version=\"1.0\"");
        if (postData != null)
        {
            byte[] fileToSend = Encoding.UTF8.GetBytes(postData);
            webRequest.ContentLength = fileToSend.Length;

            Stream reqStream = webRequest.GetRequestStream();

            reqStream.Write(fileToSend, 0, fileToSend.Length);
            reqStream.Close();
        }

        string returned = WebResponseGet(webRequest);

        return returned;
    }
    public string WebRequest(Method method, string url, string postData)
    {
        HttpWebRequest webRequest = null;
        StreamWriter requestWriter = null;
        string responseData = "";

        webRequest = System.Net.WebRequest.Create(url) as HttpWebRequest;
        webRequest.Method = method.ToString();
        webRequest.ServicePoint.Expect100Continue = false;
        //webRequest.UserAgent  = "Identify your application please.";
        //webRequest.Timeout = 20000;
        webRequest.UserAgent = USER_AGENT;
        if (method == Method.POST)
        {
            webRequest.ContentType = "application/x-www-form-urlencoded";

            //POST the data.
            requestWriter = new StreamWriter(webRequest.GetRequestStream());
            try
            {
                requestWriter.Write(postData);
            }
            catch
            {
                throw;
            }
            finally
            {
                requestWriter.Close();
                requestWriter = null;
            }
        }

        responseData = WebResponseGet(webRequest);

        webRequest = null;

        return responseData;

    }
    public string WebResponseGet(HttpWebRequest webRequest)
    {
        StreamReader responseReader = null;
        string responseData = "";

        try
        {
            responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
            responseData = responseReader.ReadToEnd();
        }
        catch (WebException we)
        {
            var errrorResponse = we.Response as HttpWebResponse;
            try
            {
                var sr = new StreamReader(errrorResponse.GetResponseStream());
                string a = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception exc) { Console.WriteLine(exc.Message); }

            //throw new OAuthNetworkException("ERROR_NETWORK_PROBLEM", request.RequestUri.ToString(),
            //                                errrorResponse.StatusCode, responseData,
            //                                request.Headers["Authorization"]);
        }
        finally
        {
            webRequest.GetResponse().GetResponseStream().Close();
            responseReader.Close();
            responseReader = null;
        }

        return responseData;
    }
    public void connectToOauthServer(string authlink)
    {
        try
        {

            //  authLink = AuthorizationLinkGet();
            HttpContext.Current.Response.Redirect(authlink + "&oauth_callback=" + CALLBACK);

        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
        }

    }
    public string getAllOauthconnection()
    {
        try
        {
            //  connectToOauthServer();
            string response = APIWebRequest("GET", "http://api.linkedin.com/v1/people/~/connections:(id,first-name,last-name,industry,headline)?format=json", null);
            return response;
        }
        catch (Exception exc)
        {

            Console.WriteLine(exc.Message);
            return exc.Message;
        }
    }
    public string getProfile()
    {
        string profile = APIWebRequest("GET", "http://api.linkedin.com/v1/people/~:(id,headline,first-name,last-name,industry,email-address,skills:(skill:(name)))?format=json", null);
        return profile;
    }
    public string updateStatusOnOauthServer(string status, string email)
    {
        try
        {
            // connectToOauthServer();
            string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            xml += "<current-status>" + status + "</current-status>";
            string response = webrRequestForclient("PUT", "http://api.linkedin.com/v1/people/~/current-status", xml, email);
            if (response == "false")
            {
                return "false";
            }
        }
        catch (Exception exc)
        {

        }
        return "true";
    }
    public void disconnectToOauthServer()
    {
        Token = null;
        TokenSecret = null;
    }
    public string webrRequestForclient(string method, string url, string postData, string email)
    {
        string query = "select token,tockensecret,verifiercode from AuthenticationTokens where regUserId='" + email + "' and authSite='linkedin';";
        string connection = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        string returned = null;
        MySqlConnection con = new MySqlConnection(connection);
        try
        {
            MySqlCommand command = new MySqlCommand(query, con);
            con.Open();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                IDataRecord record = (IDataReader)reader;
                accesstoken = record[0].ToString();
                accesssecrettoken = record[1].ToString();
                Verifier = record[2].ToString();
            }
            if (accesstoken == "")
            {
                return "false";
            }
            con.Close();
            Uri uri = new Uri(url);
            string nonce = this.GenerateNonce();
            string timeStamp = this.GenerateTimeStamp();
            string outUrl, querystring;
            string
                sig = this.GenerateSignature(uri,
                  this.ConsumerKey,
                  this.ConsumerSecret,
                  this.accesstoken,
                  this.accesssecrettoken,
                  method,
                  timeStamp,
                  nonce,
                  null,
                  out outUrl,
                  out querystring);

            //querystring += "&oauth_signature=" + HttpUtility.UrlEncode(sig);
            //NameValueCollection qs = HttpUtility.ParseQueryString(querystring);

            //string finalGetUrl = outUrl + "?" + querystring;

            HttpWebRequest webRequest = null;

            //webRequest = System.Net.WebRequest.Create(finalGetUrl) as HttpWebRequest;
            webRequest = System.Net.WebRequest.Create(url) as HttpWebRequest;
            //webRequest.ContentType = "text/xml";
            webRequest.Method = method;
            webRequest.Credentials = CredentialCache.DefaultCredentials;
            webRequest.AllowWriteStreamBuffering = true;

            webRequest.PreAuthenticate = true;

            webRequest.ServicePoint.Expect100Continue = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            if (accesstoken == null)
            {
                webRequest.Headers.Add("Authorization", "OAuth realm=\"http://api.linkedin.com/\",oauth_consumer_key=\"" + this.ConsumerKey + "\",oauth_token=\"" + this.Token + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\",oauth_timestamp=\"" + timeStamp + "\",oauth_nonce=\"" + nonce + "\",oauth_verifier=\"" + this.Verifier + "\", oauth_version=\"1.0\"");
            }
            else
            {

                webRequest.Headers.Add("Authorization", "OAuth realm=\"http://api.linkedin.com/\",oauth_consumer_key=\"" + this.ConsumerKey + "\",oauth_token=\"" + this.accesstoken + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\",oauth_timestamp=\"" + timeStamp + "\",oauth_nonce=\"" + nonce + "\",oauth_verifier=\"" + this.Verifier + "\", oauth_version=\"1.0\"");
            }
            //webRequest.Headers.Add("Authorization", "OAuth oauth_nonce=\"" + nonce + "\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"" + timeStamp + "\", oauth_consumer_key=\"" + this.ConsumerKey + "\", oauth_token=\"" + this.Token + "\", oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\", oauth_version=\"1.0\"");
            if (postData != null)
            {
                byte[] fileToSend = Encoding.UTF8.GetBytes(postData);
                webRequest.ContentLength = fileToSend.Length;

                Stream reqStream = webRequest.GetRequestStream();

                reqStream.Write(fileToSend, 0, fileToSend.Length);
                reqStream.Close();
            }

            returned = WebResponseGet(webRequest);
        }
        catch (Exception exc) { throw exc; }

        return returned;

    }
    public string updateJobOnTakeMyJob(string jobTitle, string jobSkills, string concatenateString, string concatenateString2)
    {
        try
        {
            // connectToOauthServer();
            string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<post> <title>" + jobTitle + "</title>" +
              "  <summary>" + jobSkills + "</summary>" +
              " <content>" +
            "  <submitted-url>http://csimplyfyit.appspot.com</submitted-url>" +
                  "<title>" + concatenateString2 + "</title>" +
             "<description>" + concatenateString + "</description>" +
             "</content>" +
             "</post>";
            webRequestForSystem("POST", "http://api.linkedin.com/v1/groups/5040873/posts", xml);
        }
        catch
        {

        }
        return "true";
    }
    public void webRequestForSystem(string method, string url, string postData)
    {
        try
        {
            accesstoken = "59ec1e90-b03d-48a8-b5f3-cac01cf2314b";
            accesssecrettoken = "63348a92-5849-4024-b0f1-cdd72bcc3c3f";
            Verifier = "05854";
            Uri uri = new Uri(url);
            string nonce = this.GenerateNonce();
            string timeStamp = this.GenerateTimeStamp();
            string outUrl, querystring;
            string
                sig = this.GenerateSignature(uri,
                  this.ConsumerKey,
                  this.ConsumerSecret,
                  this.accesstoken,
                  this.accesssecrettoken,
                  method,
                  timeStamp,
                  nonce,
                  null,
                  out outUrl,
                  out querystring);

            //querystring += "&oauth_signature=" + HttpUtility.UrlEncode(sig);
            //NameValueCollection qs = HttpUtility.ParseQueryString(querystring);

            //string finalGetUrl = outUrl + "?" + querystring;

            HttpWebRequest webRequest = null;

            //webRequest = System.Net.WebRequest.Create(finalGetUrl) as HttpWebRequest;
            webRequest = System.Net.WebRequest.Create(url) as HttpWebRequest;
            //webRequest.ContentType = "text/xml";
            webRequest.Method = method;
            webRequest.Credentials = CredentialCache.DefaultCredentials;
            webRequest.AllowWriteStreamBuffering = true;

            webRequest.PreAuthenticate = true;

            webRequest.ServicePoint.Expect100Continue = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            if (accesstoken == null)
            {
                webRequest.Headers.Add("Authorization", "OAuth realm=\"http://api.linkedin.com/\",oauth_consumer_key=\"" + this.ConsumerKey + "\",oauth_token=\"" + this.Token + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\",oauth_timestamp=\"" + timeStamp + "\",oauth_nonce=\"" + nonce + "\",oauth_verifier=\"" + this.Verifier + "\", oauth_version=\"1.0\"");
            }
            else
            {

                webRequest.Headers.Add("Authorization", "OAuth realm=\"http://api.linkedin.com/\",oauth_consumer_key=\"" + this.ConsumerKey + "\",oauth_token=\"" + this.accesstoken + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\",oauth_timestamp=\"" + timeStamp + "\",oauth_nonce=\"" + nonce + "\",oauth_verifier=\"" + this.Verifier + "\", oauth_version=\"1.0\"");
            }
            //webRequest.Headers.Add("Authorization", "OAuth oauth_nonce=\"" + nonce + "\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"" + timeStamp + "\", oauth_consumer_key=\"" + this.ConsumerKey + "\", oauth_token=\"" + this.Token + "\", oauth_signature=\"" + HttpUtility.UrlEncode(sig) + "\", oauth_version=\"1.0\"");
            if (postData != null)
            {
                byte[] fileToSend = Encoding.UTF8.GetBytes(postData);
                webRequest.ContentLength = fileToSend.Length;

                Stream reqStream = webRequest.GetRequestStream();

                reqStream.Write(fileToSend, 0, fileToSend.Length);
                reqStream.Close();
            }

            WebResponseGet(webRequest);
        }
        catch (Exception exc) { throw exc; }

        return;

    }


}
//"http://api.linkedin.com/v1/people/~/email-address"
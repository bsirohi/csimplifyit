var ContactPage = function () {

    return {
        
    	//Basic Map
        initMap: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				scrollwheel: false,				
				lat: 28.4538144,
				lng: 77.0511009
			  });
			  
			  var marker = map.addMarker({
				lat: 28.4538144,
				lng: 77.0511009,
	            title: 'C Simplify IT Services Private Limited.'
		       });
			});
        },

        //Panorama Map
        initPanorama: function () {
		    var panorama;
		    $(document).ready(function(){
		      panorama = GMaps.createPanorama({
		        el: '#panorama',
		        lat: 28.4538144,
				lng: 77.0511009
			  });
		    });
		}        

    };
}()
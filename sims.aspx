<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  %>
<asp:Content ID="simshead" ContentPlaceHolderID="HeadContent" runat="server">
    <title>SIMS | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="simsMaincontent" ContentPlaceHolderID="MainContent" runat="server">
    <%-- Body or main content of content page --%>      


	<div class="wrapper">
    
		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Smart Incidence Management</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.aspx">Home</a></li>
					<li><a href="">Products</a></li>
                    <li><a href="">Smart Incidence Management</a></li>
					
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		
        <!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				<!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
                        <div class="embed-responsive embed-responsive-4by3">
                              
                        <iframe width="530" height="300" src="https://www.youtube.com/embed/n_5tQrszX8s" frameborder="0" allowfullscreen></iframe>
                            </div>
						
					</div>
				</div>
				<!-- End Carousel -->

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>Smart Incidence Management - For Point of Sale and Inventory Management Application Support</h2>
					<p>C Simplify IT is focussed on cutting the supports cost and has build SIMS to stabilise the web systems faster and reduce the to & fro exchange mails/phone calls on support incidences.</p>
					<p></p>
					
					<a href="https://www.youtube.com/embed/n_5tQrszX8s" class="btn-u btn-u-large" data-toggle="modal" data-target="#myModal">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
			</div><!--/row-->

			<div class="tag-box tag-box-v2">
				<p>C Simplify IT is focussed on cutting the supports cost and has build SIMS to stabilise the web systems faster and reduce the to & fro exchange mails/phone calls on support incidences.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>
            
            
            <!------------------------------Modal--------------------------------------->
             <!-- Modal -->
              <div class="modal fade wow bounceInRight" id="myModal" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header" style="background-color:#EF6262;">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title text-center" style="color:#fff;">Smart Incidence Management</h4>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-4by3">
                          <iframe class="embed-responsive-item" width="530" height="300" src="https://www.youtube.com/embed/n_5tQrszX8s" frameborder="0" allowfullscreen></iframe>

                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>
             <!------------------------------ End Modal--------------------------------------->


			
		</div><!--/container-->
		<!--=== End Content Part ===-->
</div><!--/wrapper-->

</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using System.Web.Script.Serialization;
using System.ServiceModel.Web;
using System.Globalization;
using System.Web;
using System.Net;
using System.Runtime.Serialization.Json;
// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "takeMyJob" in code, svc and config file together.
public class takeMyJob : ItakeMyJob
{
    DataModel model = new DataModel();
    ResponseData response;
    Rewards rewards = new Rewards();
    DataTable table = new DataTable();
    oAuthLinkedIn linkedin = new oAuthLinkedIn();
    Queue queue = new Queue();
    public Stream getToken(string inputData)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {

                }
                if (objelement.Key.Equals("data"))
                {

                    list = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            string username = null;
            string password = null;
            string authSite = null;
            foreach (var keyvalue in list)
            {
                if (keyvalue.Key.Equals("userName"))
                {
                    username = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("userPassword"))
                {
                    password = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("authSite"))
                {
                    authSite = keyvalue.Value;
                }
            }
            string result = getPassword(username, password, authSite);
            string[] splitValuesFromGetgetPass = result.Split('&');
            if (splitValuesFromGetgetPass.Length == 1)
            {
                model.insertIntoTable("insert into Users ( userName,displayName,userType,userStatus,userPassword,modifiedBy,"
                 + "currentCompanyName,currentJobTitle,modifiedDateTime ) values" +
                 " ('" + username + "','" + null + "','" + "4'," + "'0','"
                 + dateTimeClass.datetime() + "','" + null + "','"
                 + null + "','" + null + "', now() )");
                dt = model.selectIntoTable("select uid userPassword from Users where userName='" + username + "'");
                if (dt.Rows.Count != 0)
                {
                    int uid = Convert.ToInt32(dt.Rows[0][0]);
                    byte[] array = Encoding.ASCII.GetBytes(uid.ToString() + dateTimeClass.datetime());
                    string encoded = System.Convert.ToBase64String(array);
                    model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,authSite) value (" + uid + ",'" + encoded + "','system')");
                    //Queue queue = new Queue();
                    //queue.queueRegistrationMail(uid, encoded);
                }
                response = new ResponseData(1, Constant.notvallid, "[{\"token\":\"\" ,\"uid\":\"\" }]");
            }
            else if (splitValuesFromGetgetPass[0].Equals("wrongpwd"))
            {
                response = new ResponseData(1, Constant.notvallid, "[{\"token\": \"\",\"uid\":\"" + splitValuesFromGetgetPass[1] + "\" }]");
            }
            else
            {
                response = new ResponseData(1, Constant.vallid, "[{\"token\":\"" + splitValuesFromGetgetPass[1] + "\",\"uid\":\"" + splitValuesFromGetgetPass[2] + "\",\"displayName\":\"" + splitValuesFromGetgetPass[3] + "\" }]");
            }
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            returnString = response.returnStatment();
            return returnStmnt(returnString);
        }
    }
    private string getPassword(string username, string password, string authSite)
    {
        string uid = null;
        string displayName = null;
        try
        {
            DataTable dt;
            dt = model.selectIntoTable("select userPassword,uid,displayName from Users where userName='" + username + "' and userType=1");

            if (dt.Rows.Count > 0)
            {
                uid = dt.Rows[0][1].ToString();
                displayName = dt.Rows[0][2].ToString();
                if (dt.Rows[0][0].ToString().Equals(EncryptBase64.EncodeTo64(password)))
                {
                    string result = dt.Columns[0].ToString() + GenerateNonce();
                    byte[] array = Encoding.ASCII.GetBytes(result + GenerateNonce());
                    string encoded = System.Convert.ToBase64String(array);
                    dt = model.selectIntoTable("select * from AuthenticationTokens where regUserid=" + uid + " and authSite='" + authSite + "'");
                    if (dt.Rows.Count != 0)
                    {
                        model.updateIntoTable("update AuthenticationTokens set token='" + encoded + "' where regUserid=" + uid + " and authSite='" + authSite + "'");
                        return "vallid&" + encoded + "&" + uid + "&" + displayName;
                    }
                    else
                    {
                        model.insertIntoTable("insert into AuthenticationTokens (token,regUserid,authSite) values ('" + encoded + "'," + uid + ",'" + authSite + "')");
                        return "vallid&" + encoded + "&" + uid + "&" + displayName; ;
                    }
                }
                return "wrongpwd&" + uid;
            }
            else
            {
                dt = model.selectIntoTable("select userPassword,uid from Users where userName='" + username + "'");
                if (dt.Rows.Count == 0)
                {
                    return "saveuser";
                }
                else
                {
                    return "wrongpwd&" + uid;
                }
            }
        }
        catch (Exception exc)
        {

            throw exc;
        }
    }
    private string calculateMD5Hash(string input)
    {
        // step 1, calculate MD5 hash from input
        MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        byte[] hash = md5.ComputeHash(inputBytes);

        // step 2, convert byte array to hex string
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }

        return sb.ToString();
    }
    public Stream getRefrenceFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int regid = 0;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getRefrenceFriend", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("registeruser"))
                        {
                            regid = Convert.ToInt32(list.Value);
                            dt = model.selectIntoTable("SELECT distinct(uid),count(toID) as pendingRequests,displayName,userName,userContactNo," +
                                "refUserPrivacyConcern as isBlocked ,userType,userStatus,currentCompanyName,currentJobTitle,currentIndustry FROM " +
                              " Users A,RefereredUsers B  Left JOIN   FriendRequests As F ON F.RequestStatus=0" +
                                " and F.introducedBy=B.refUserId and F.toID=" + regid
                                + " WHERE A.userType!=2 and A.uid = B.refUserId AND B.regUserId =" + regid +
                                "  group by uid order by pendingRequests desc, displayName");
                            if (dt.Rows.Count != 0)
                            {
                                response = new ResponseData(1, Constant.foundData, convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                            else
                            {
                                response = new ResponseData(1, Constant.foundData, "[{\"uid\":\"1\",\"pendingRequests\":\"0\",\"displayName\":\"TakeMyJob\"," +
                                    "\"userName\":\"takeMyJob@csimplifyit.com\",\"userContactNo\":\"0\",\"refUserPrivacyConcern\":\"0\",\"userType\":\"0\"" +
                                   ",\"userStatus\":\"1\",\"currentCompanyName\":\"\",\"currentJobTitle\":\"You no friend found.Either use extend or login with empty username & password\",\"currentIndustry\":\"\" }]");
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }

            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream blockUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string requestingUserid = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "blockUser", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string blockUserId = null;
                    string blockRequestType = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("requestingUserid"))
                        {
                            requestingUserid = list.Value;
                        }
                        if (list.Key.Equals("blockUserId"))
                        {
                            blockUserId = list.Value;
                        }
                        if (list.Key.Equals("blockRequestType"))
                        {
                            blockRequestType = list.Value;
                        }
                    }
                    if (Convert.ToInt32(blockRequestType) <= 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " regUserPrivacyConcern='" + blockRequestType + "' , regUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                        " refUserPrivacyConcern='" + blockRequestType + "' , refUserPrivacyConcernDateTime = now() where refUserId ='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            //dt=model.selectIntoTable("select "
                            response = new ResponseData(1, Constant.blockUser, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else if (Convert.ToInt32(blockRequestType) > 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " refUserPrivacyConcern='" + blockRequestType + "', refUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                       " regUserPrivacyConcern='" + blockRequestType + "', regUserPrivacyConcernDateTime = now() where refUserId='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            response = new ResponseData(1, Constant.blockUser, "[{\"blockUserId\":\"" + blockUserId + "\"}]");
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        response = new ResponseData(0, Constant.checkDataColumns, null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream unBlockUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string requestingUserid = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "unblockUser", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string blockUserId = null;
                    string blockRequestType = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("requestingUserid"))
                        {
                            requestingUserid = list.Value;
                        }
                        if (list.Key.Equals("blockUserId"))
                        {
                            blockUserId = list.Value;
                        }
                        if (list.Key.Equals("blockRequestType"))
                        {
                            blockRequestType = list.Value;
                        }
                    }
                    if (Convert.ToInt32(blockRequestType) <= 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                           " regUserPrivacyConcern='" + 0 + "' , regUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " refUserPrivacyConcern='" + 0 + "' , refUserPrivacyConcernDateTime = now() where refUserId ='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            //  auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "specify user is blocked", null, null);
                            response = new ResponseData(1, Constant.unblockUser, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else if (Convert.ToInt32(blockRequestType) > 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                         " refUserPrivacyConcern='" + 0 + "', refUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                       " regUserPrivacyConcern='" + 0 + "', regUserPrivacyConcernDateTime = now() where refUserId='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            // auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "specify user is blocked", null, null);
                            response = new ResponseData(1, Constant.unblockUser, "[{\"unblockUserId\":\"" + blockUserId + "\"}]");
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        //  auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "user data columns is not correct", null, null);
                        response = new ResponseData(0, Constant.checkDataColumns, null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            // auditInfo("takemyjob", "unblockuser", "want to un block friends", Convert.ToInt32(requestingUserid), "user not exists", null, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            //   auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private List<KeyValuePair<string, string>> jsonToKeyValuePair(string json)
    {
        var list = new List<KeyValuePair<string, string>>();
        // KeyValuePair<string, string> jsonkeyvalue=new KeyValuePair<string,string>(null,null);
        try
        {
            JObject root = JObject.Parse(json);
            JArray items = (JArray)root["data"];
            foreach (var data in items)
            {
                JObject localdata = JObject.Parse(data.ToString());
                foreach (var jsonobject in localdata)
                {
                    {
                        list.Add(new KeyValuePair<string, string>(jsonobject.Key.ToString(), jsonobject.Value.ToString()));
                    }
                }

            }
            return list;
        }
        catch (Exception exc)
        {
            throw exc;
        }
        throw new NotImplementedException();
    }
    public Stream postNewJob(string inputData)
    {
        string returnString = "";
        int userID = 0;
        DataTable dt = new DataTable();
        try
        {
            string jobIDInJson = null;
            Boolean flag = false;
            JObject jsonrow = JObject.Parse(inputData);
            List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
            foreach (var rowobj in jsonrow)
            {
                if (rowobj.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(rowobj.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(returnString);
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "post new Job", userID);
                            }
                        }
                    }
                }
                if (rowobj.Key.Equals("data") && flag == true)
                {
                    jobIDInJson = insertFromKeyValuePostJob("Job", "{\"data\":" + rowobj.Value.ToString() + "}");
                    //  string response = model.insertIntoTable(query);

                    if (jobIDInJson != null)
                    {
                        response = new ResponseData(1, "Successfully posted", jobIDInJson);
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(0, "input parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }

            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            response = new ResponseData(0, exc.Message, null);
            //returnString = "{\"status\":0,\"Description\":\""+exc.Message+"\",\"results\":[]}";
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream applyForJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        string appliedByUser = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            Boolean flag = false;
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != null)
                            {
                                auditInfo(inputData, 1, "apply Job", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data"))
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            if (flag == true)
            {
                string jobRefid = null;
                foreach (var list in jsonkeyvalue)
                {
                    if (list.Key.Equals("appliedByUser"))
                    {
                        appliedByUser = list.Value.ToString();
                    }
                    if (list.Key.Equals("jobRefid"))
                    {
                        jobRefid = list.Value.ToString();
                    }
                }
                if (appliedByUser != null && jobRefid != null)
                {
                    dt = model.selectIntoTable("SELECT Jaid FROM JobApplications where jobRefid=" + jobRefid + " and appliedByUser=" + appliedByUser);
                    if (dt.Rows.Count == 0)
                    {
                        foreach (var objelement in jsonrow)
                        {
                            if (objelement.Key.Equals("data"))
                            {
                                dt = model.insertIntoTableWithLastId("insert into JobApplications (" +
                                    "appliedOnDateTime,applicationStatus,jobRefid,appliedByUser) values (now(),1," + jobRefid + "," + appliedByUser + ")", "jaid");
                                if (dt.Rows.Count != 0)
                                {
                                    response = new ResponseData(1, Constant.applicationPostSuccessfully, "[{\"jobID\":\"" + jobRefid + "\"}]");
                                    return returnStmnt(response.returnStatment());
                                }
                            }
                        }
                    }
                    else
                    {
                        response = new ResponseData(1, Constant.alreadyapplyjob, "[{\"jobID\":\"" + jobRefid + "\"}]");
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(1, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            response = new ResponseData(0, exc.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public string convertDataTableToJson(object value)
    {
        Type type = value.GetType();

        Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

        json.NullValueHandling = NullValueHandling.Ignore;

        json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
        json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
        json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        if (type == typeof(DataTable))
            json.Converters.Add(new DataTableConverter());
        else if (type == typeof(DataSet))
            json.Converters.Add(new DataSetConverter());

        StringWriter sw = new StringWriter();
        Newtonsoft.Json.JsonTextWriter writer = new JsonTextWriter(sw);
        writer.Formatting = Formatting.None;
        writer.QuoteChar = '"';
        json.Serialize(writer, value);
        string output = sw.ToString();
        writer.Close();
        sw.Close();

        return output;
    }
    //private string convertDataTableToJson(DataTable dt)
    //{
    //    try
    //    {
    //        StringBuilder Sb = new StringBuilder();

    //        string[] StrDc = new string[dt.Columns.Count];
    //        string HeadStr = string.Empty;
    //        for (int i = 0; i < dt.Columns.Count; i++)
    //        {
    //            StrDc[i] = dt.Columns[i].Caption;
    //            HeadStr += "\"" + StrDc[i] + "\" : \"" + StrDc[i] + i.ToString() + "¾" + "\",";
    //        }
    //        HeadStr = HeadStr.Substring(0, HeadStr.Length - 1);
    //        Sb.Append("[");
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            string TempStr = HeadStr;
    //            Sb.Append("{");
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {
    //             string data = (dt.Rows[i][j].ToString().Replace("\n", " ")).Replace('"',' ');
    //             if (vallidateJson(data).Equals(true))
    //             {
    //                 TempStr = TempStr.Replace(dt.Columns[j] + j.ToString() + "¾", data);
    //             }
    //             else {
    //                 TempStr = TempStr.Replace(dt.Columns[j] + j.ToString() + "¾", "");
    //             }
    //            }
    //            Sb.Append(TempStr + "},");
    //        }
    //        Sb = new StringBuilder(Sb.ToString().Substring(0, Sb.ToString().Length - 1));
    //        if (dt.Rows[0][0] != null)
    //            Sb.Append("]");

    //        return Sb.ToString();
    //    }
    //    catch (Exception exc)
    //    {
    //        throw exc;
    //    }
    //}
    //private string convertDataTableToJson(DataSet ds) { 

    //}
    private string insertFromKeyValuePostJob(string tableName, string jsonarr)
    {
        DataTable dt = new DataTable();
        StringBuilder insertStmt = new StringBuilder();
        string jobTitle = null;
        string postedBy = null;
        string jobSkills = null;
        string jobLocationName = null;
        string jobLatitude = null;
        string jobLongitude = null;
        string postToSocialSite = null;
        string jobReferenceLink = null;
        string jobComments = null;
        string jobExperienceMinYears = null;
        string jobExperienceMaxYears = null;
        string jobVertical = null;
        string jobRole = null;
        string jobMinSalary = null;
        string jobMaxSalary = null;
        string jobSalaryCurrency = null;
        string canDisplayPostedByName = null;

        insertStmt.Append("insert into " + tableName + "(jobStatus,jobType,postedOnDateTime,jobExpiresOn,joblastModifiedOn," +
            "jobTitle,postedBy,jobSkills,jobLocationName,jobLatitude,jobLongitude,canDisplayPostedByName,jobReferenceLink,jobComments" +
            ",jobExperienceMinYears,jobExperienceMaxYears,jobVertical,jobRole,jobMinSalary,jobMaxSalary,postToSocialSite,jobSalaryCurrency) ");

        StringBuilder values = new StringBuilder();
        values.Append("values (1,2,now(),DATE_ADD(now(),INTERVAL 30 DAY),now(),");
        try
        {
            JObject root = JObject.Parse(jsonarr);
            JArray items = (JArray)root["data"];
            foreach (var data in items)
            {
                JObject localdata = JObject.Parse(data.ToString());
                foreach (var jsonobject in localdata)
                {
                    if (jsonobject.Key.Equals("jobTitle"))
                    {
                        jobTitle = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("postedBy"))
                    {
                        postedBy = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobSkills"))
                    {
                        jobSkills = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobLocationName"))
                    {
                        jobLocationName = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobLatitude"))
                    {
                        jobLatitude = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("postToSocialSite"))
                    {
                        postToSocialSite = jsonobject.Value.ToString();

                    }
                    if (jsonobject.Key.Equals("jobMaxSalary"))
                    {
                        jobMaxSalary = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobSalaryCurrency"))
                    {
                        jobSalaryCurrency = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobLongitude"))
                    {
                        jobLongitude = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobReferenceLink"))
                    {
                        jobReferenceLink = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobComments"))
                    {
                        jobComments = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobExperienceMinYears"))
                    {
                        jobExperienceMinYears = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobExperienceMaxYears"))
                    {
                        jobExperienceMaxYears = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobVertical"))
                    {
                        jobVertical = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobRole"))
                    {
                        jobRole = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("jobMinSalary"))
                    {
                        jobMinSalary = jsonobject.Value.ToString();

                    }

                }
            }
            if (jobTitle != null && jobSkills != null && jobLocationName != null && postedBy != null && jobLatitude != null && jobLongitude != null)
            {
                values.Append("'" + jobTitle + "'," + "'" + postedBy + "'," + "'" + jobSkills + "'," + "'" + jobLocationName + "'," + "'" + jobLatitude + "'," + "'" + jobLongitude + "',"
+ "'" + canDisplayPostedByName + "'," + "'" + jobReferenceLink + "'," +
"'" + jobComments + "'," + "'" + jobExperienceMinYears + "'," + "'" + jobExperienceMaxYears + "'," + "'" + jobVertical + "'," + "'" + jobRole + "'," +
"'" + jobMinSalary + "'," + "'" + jobMaxSalary + "'," + "'" + postToSocialSite + "'," + "'" + jobSalaryCurrency + "')");
                dt = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "jid");
                if (dt.Rows.Count != 0)
                {

                    model.updateIntoTable("Update Users set lattitude=" + jobLatitude + ",longitude=" + jobLongitude +
                        ",lastCheckInLocation='" + jobLocationName + "' where uid=" + postedBy);
                    return convertDataTableToJson(dt);
                }

            }

            return null;
        }
        catch (Exception exc)
        {
            throw exc;
        }
    }
    public virtual string GenerateNonce()
    {
        Random random = new Random();
        // Just a simple implementation of a random number between 123400 and 9999999
        return random.Next(123400, 9999999).ToString();
    }
    public Stream getLov(string inputData)
    {
        /*string jsonWhereClause = null;*/
        string returnString = "";
        try
        {
            DataTable dt = new DataTable();

            List<KeyValuePair<string, string>> jsonkeyvalue;
            jsonkeyvalue = jsonToKeyValuePair(inputData);
            if (jsonkeyvalue[0].Value.Equals(""))
            {
                if (jsonkeyvalue[1].Key.Equals("jsonwhereclause"))
                {
                    dt = model.selectIntoTable("SELECT * FROM  ListofValues " + jsonkeyvalue[1].Value.ToString());
                }
                else
                {
                    returnString = "{\"status\":0,\"Description\":\"input string is not in correct format\",\"results\":[null]}";
                }
                string result = convertDataTableToJson(dt);

                returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[" + result + "]}";
            }
            else
            {
                returnString = "{\"status\":0,\"Description\":\"not a vallid token\",\"results\":[null]}";
            }
        }
        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":\"null\"}";
        }
        return new MemoryStream(Encoding.UTF8.GetBytes(returnString));
    }/* no result found is on description if no data found and status success */
    public Stream getJobs(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getJobs", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    string result = null;
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("userID"))
                    {
                        userID = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select distinct(jid),(case When canDisplayPostedByName = 1 then  U.displayName else 'Guess IT' end ) as postedByName,jobMinSalary,jobMaxSalary,jobSalaryCurrency,canDisplayPostedByName,DateDiff(J.jobExpiresOn,now()) as expireinDays,count(JA.jaid) as jobApplications,DATE_FORMAT(JA2.appliedOnDateTime,'%d-%b-%Y') as jobAppliedOnDate,(JA2.appliedByUser IS NOT NULL) as isJobApplied" +
                        ",postedBy,DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                        "jobTitle,(case When jobReferenceLink = 'null' then 'No url' else jobReferenceLink end ) as jobReferenceLink,joblatitude,jobLongitude,(case When jobLocationName = 'null' then 'No location'  else jobLocationName end ) as jobLocationName ,jobType,jobComments," +
                        "jobSkills,DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') " +
                        "as jobExpiresOn ,DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,SUM(JA.applicationNotes IS NOT NULL) as commentsCount," +
                         "jobExperienceMinYears,jobExperienceMaxYears,jobVertical,jobRole from " +
                     " RefereredUsers As R,Users as U,Job As J Left Join JobApplications As JA on " +
                        "JA.jobRefid=J.jid Left join JobApplications As JA2 on JA2.appliedByUser=" + userID + " and " +
                         " JA2.jobRefid=J.jid " +
                            " where  J.postedBy = R.regUserID and J.postedBy=U.uid AND  R.refUserID =" + userID +
                          " and jobStatus = 1 and jobExpiresOn > now() and " +
                          "refUserPrivacyConcern = 0 group by jid " +
                           "order by isJobApplied , expireinDays  ");
                        if (dt.Rows.Count != 0)
                        {
                            result = convertDataTableToJson(dt);
                        }
                        if (result != null)
                        {
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean vallidateToken(string token)
    {
        table = model.selectIntoTable("select * from AuthenticationTokens where token='" + token + "'");
        if (table.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private List<KeyValuePair<string, string>> metaToKeyValuePair(string meta)
    {
        var list = new List<KeyValuePair<string, string>>();
        // KeyValuePair<string, string> jsonkeyvalue=new KeyValuePair<string,string>(null,null);
        try
        {
            JObject jobj = JObject.Parse(meta);

            foreach (var data in jobj)
            {
                list.Add(new KeyValuePair<string, string>(data.Key.ToString(), data.Value.ToString()));

            }
            return list;
        }
        catch (Exception exc)
        {
            throw exc;
        }


    }
    public Stream knowMyFriends(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        //  string returnString = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "knowMyFriends", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != 0)
                {
                    Boolean resposnevalue = insertJsonintoUsers("Users", "{\"data\":" + objelement.Value.ToString() + "}", Convert.ToInt32(userID));
                    if (resposnevalue == true)
                    {
                        response = new ResponseData(1, "Contact has been saved", null);
                        return returnStmnt(response.returnStatment());
                    }
                }

            }
            response = new ResponseData(0, Constant.invallidtoken, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            //  returnString = "{\"status\":0,\"Description\":\"" + exc.Message + "\",\"results\":[null]}";
            response = new ResponseData(0, exc.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertJsonintoUsers(string tableName, string jsonarr, int uid)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = model.selectIntoTable("select displayName from Users where uid=" + uid.ToString());
            if (dt.Rows.Count != 0)
            {
                string modifiedBy = dt.Rows[0][0].ToString();
                string userContactNo = null;
                string userName = null;
                string source = null;
                string friendName = null;
                StringBuilder insertStmt = new StringBuilder();
                StringBuilder values = new StringBuilder();

                JObject root = JObject.Parse(jsonarr);
                JArray items = (JArray)root["data"];
                foreach (var data in items)
                {
                    insertStmt.Append("insert into " + tableName + "( userType,userStatus,userPassword,modifiedBy,modifiedDateTime,createdBy,createdDatetime,userName,userContactNo,displayName)");
                    values.Append("values ('0','0','" + dateTimeClass.datetime() + "','" + modifiedBy + "',now(),'" + modifiedBy + "',now(),");
                    JObject localdata = JObject.Parse(data.ToString());
                    foreach (var jsonobject in localdata)
                    {
                        if (jsonobject.Key.Equals("displayName"))
                        {
                            friendName = jsonobject.Value.ToString();
                        }
                        else if (jsonobject.Key.Equals("source"))
                        {
                            source = jsonobject.Value.ToString();
                        }
                        else if (jsonobject.Key.Equals("userContactNo"))
                        {
                            userContactNo = jsonobject.Value.ToString();

                        }
                        else if (jsonobject.Key.Equals("userName"))
                        {
                            userName = jsonobject.Value.ToString();
                        }

                    }
                    if (friendName != null && userContactNo != null && friendName != "" && userContactNo != "")
                    {
                        values.Append("'" + userName + "'," + "'" + userContactNo + "'," + "'" + friendName + "')");
                        string query = insertStmt.Append(values.ToString()).ToString();
                        insertStmt.Clear();
                        values.Clear();
                        try
                        {
                            dt = model.selectIntoTable("select distinct(uid) from Users where userContactNo='" + userContactNo + "' OR userName='" + userName + "'");
                            if (dt.Rows.Count == 0)
                            {
                                dt = model.insertIntoTableWithLastId(query, "uid");

                                if (dt.Rows.Count != 0)
                                {
                                    string refuid = dt.Rows[0][0].ToString();
                                    model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + uid + "','" + refuid + "',now())");
                                    model.insertIntoTable("insert into AuthenticationTokens (regUserId,lastModifiedDate,createdDateTime,authSite)values('" + refuid + "',now(),now(),'" + source + "')");
                                }
                            }
                            else
                            {
                                model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + uid + "','" + dt.Rows[0][0].ToString() + "',now())");
                            }
                        }

                        catch (Exception exc) { Console.WriteLine(exc.Message); }
                    }
                }

            }
            else { return false; }
            return true;
        }
        catch (Exception exc)
        {
            throw exc;
        }

    }
    public string testfunc()
    {
        try
        {
            queue.processTo(50);
            return "success";
        }
        catch (Exception exc) { return exc.Message; }



    }
    private Stream returnStmnt(string returnstmnt)
    {
        return new MemoryStream(Encoding.UTF8.GetBytes(returnstmnt));
    }
    private string convertTableIntoJob(DataTable dt, int registerUid)
    {
        string jobRole = null;
        string jobTitle = null;
        StringBuilder sb = new StringBuilder();
        StringBuilder sbforkey = new StringBuilder();
        try
        {
            int count = dt.Rows.Count;
            sb.Append("insert into Job (postedBy,postedOnDateTime,jobReferenceLink,jobLatitude,jobLongitude" +
                ",jobLocationName,jobType,jobComments,jobExpiresOn," +
                "joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,jobVertical,"
                + "JobTitle,jobRole,jobSkills ) values ('" + registerUid.ToString() +
                "',now(),'null','null','null','null',1,'null',DATE_ADD(now(),INTERVAL 30 DAY),now(),'1','1','2','1','");
            if (count != 0)
            {
                jobRole = dt.Rows[0][0].ToString();
                jobTitle = dt.Rows[0][1].ToString();
                for (int i = 0; i < count; i++)
                {
                    sbforkey.Append(dt.Rows[i][3].ToString() + ", ");
                }
            }
            sb.Append(jobTitle + "','" + jobRole + "','" + sbforkey.ToString() + "')");
            dt = model.selectIntoTable("select jid from Job where postedBy='" + registerUid.ToString()
                + "' and jobType=1 and now() < jobExpiresOn;");
            if (dt.Rows.Count == 0)
            {
                dt = model.insertIntoTableWithLastId(sb.ToString(), "jid");
                if (dt.Rows.Count != 0)
                {
                    //  dt = model.selectIntoTable("select "
                    return "1&" + dt.Rows[0][0].ToString();
                }
            }
            else
            {
                //  dt = model.selectIntoTable("select jid, max(jobExpiresOn) from Job where postedBy='" + registerUid.ToString()
                //+ "' and jobType=1 ");
                return "-1&" + dt.Rows[0][0].ToString();

            }

        }

        catch (Exception exc)
        {
            throw exc;
        }

        return sb.ToString();
    }
    public Stream takemyJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "takeMyJob", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("registerUid"))
                        {
                            dt = model.selectIntoTable("Select  u.currentJobTitle,u.currentIndustry,u.currentCompanyName,luvValueDisplayName " +
                             "from  Users u, UserSkills s, ListofValues  l " +
                               "where u.uid = " + list.Value.ToString() +
                                " and u.uid = s.uid" +
                                " and s.skillRef = l.luvid" +
                                 " Order by l.luvValueDisplayName;");
                            if (dt.Rows.Count == 0)
                            {
                                dt = model.selectIntoTable("Select  u.currentJobTitle,u.currentIndustry,u.currentCompanyName,u.currentJobTitle as luvValueDisplayName from Users as u where uid= " + list.Value.ToString());
                            }
                            string[] resultstring = convertTableIntoJob(dt, Convert.ToInt32(list.Value)).Split('&');
                            if (resultstring[0].Equals("1"))
                            {
                                response = new ResponseData(1, "Job successfully Posted", "[{\"jobID\" :\"" + resultstring[1] + "\"}]");
                                return returnStmnt(response.returnStatment());
                            }
                            else
                            {
                                response = new ResponseData(1, Constant.jobalreadyposted, "[{\"jobID\" :\"" + resultstring[1] + "\"}]");
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }
    }
    public Stream optOut(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "optOut", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (userID != 0)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update Users set" +
                          " userType='2' , modifiedDateTime = now() where uid='" + userID + "'");
                        int rowaffectingjobtable = model.updateIntoTablewithRowCount("update Job set" +
                          " jobStatus='3' where postedBy='" + userID + "'");
                        model.deleteFromTable("delete from RefereredUsers where regUserId=" + userID);
                        model.deleteFromTable("delete from RefereredUsers where refUserId=" + userID);
                        if (noOfRowAffecting >= 1 && rowaffectingjobtable >= 0)
                        {
                            //  returnString = "{\"status\":0,\"Description\":\"user  blocked\",\"results\":[null]}";
                            response = new ResponseData(0, "User Blocked", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        //returnString = "{\"status\":0,\"Description\":\"Check Column names of DataField\",\"results\":[null]}";
                        response = new ResponseData(0, "Check columns of inputData", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            // returnString = "{\"status\":0,\"Description\":\"this blockuser is not exist  \",\"results\":[null]}";
            response = new ResponseData(0, "this blockuser is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream payments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string paymentSource = null;
                    int paymentFromUserId = 0;
                    int paymentStatus = 0;
                    string paymentRecieptID = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("paymentSource"))
                        {
                            paymentSource = list.Value;
                        }
                        if (list.Key.Equals("paymentFromUserId"))
                        {
                            paymentFromUserId = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentStatus"))
                        {
                            paymentStatus = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentRecieptID"))
                        {
                            paymentRecieptID = list.Value;
                        }
                    }
                    if (Convert.ToInt32(paymentFromUserId) != 0 && paymentSource != null && paymentRecieptID != null && paymentStatus != 0)
                    {

                    }


                }
            }
            response = new ResponseData(0, "input parameter are not correct", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getcurrentRewards(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string paymentSource = null;
                    int paymentFromUserId = 0;
                    int paymentStatus = 0;
                    string paymentRecieptID = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("paymentSource"))
                        {
                            paymentSource = list.Value;
                        }
                        if (list.Key.Equals("paymentFromUserId"))
                        {
                            paymentFromUserId = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentStatus"))
                        {
                            paymentStatus = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentRecieptID"))
                        {
                            paymentRecieptID = list.Value;
                        }
                    }
                    if (Convert.ToInt32(paymentFromUserId) != 0 && paymentSource != null && paymentRecieptID != null && paymentStatus != 0)
                    {

                    }


                }
            }
            response = new ResponseData(0, "input parameter are not correct", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private void auditInfo(string inputData, int auditEvent, string eventDoneByUser, int associatedRefId)
    {
        try
        {
            DataTable dt = new DataTable();
            inputData = inputData.Replace("'", "\"");
            // dt = model.selectIntoTable("select luvid from ListofValues where luvValueDisplayName='" + luvValueDisplayName + "' and luvCategory='" + luvCategory + "'");
            if (inputData.Length > 500)
            {
                string[] arrOfString = splitByLength(inputData, 499).ToArray();
                if (arrOfString.Length == 2)
                    model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
                        "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
                        + eventDoneByUser + "','" + arrOfString[0] + "','" + arrOfString[1] + "','null'," + associatedRefId + ",now())");
                else
                {
                    model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
         "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
         + eventDoneByUser + "','" + arrOfString[0] + "','" + arrOfString[1] + "','" + arrOfString[2] + "'," + associatedRefId + ",now())");
                }
            }
            else
            {
                model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
        "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
        + eventDoneByUser + "','" + inputData + "','" + null + "','null'," + associatedRefId + ",now() )");
            }
        }
        catch (Exception exc) { Console.WriteLine(exc.Message); }
    }
    public List<string> splitByLength(string s, int length)
    {
        List<string> a = new List<string>();
        for (int i = 0; i < s.Length; i += 499)
        {
            if ((i + 499) < s.Length)
                a.Add(s.Substring(i, 499));
            else
                a.Add(s.Substring(i));
        }
        return a;
    }
    public Stream contentUpload(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo("Base64 string", 1, "contentUpload", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string associatedID = null;
                    string fileContent = null;
                    int fileMimeType = 0;
                    string fileName = null;
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("associatedID"))
                        {
                            associatedID = list.Value;
                        }
                        if (list.Key.Equals("fileContent"))
                        {
                            fileContent = list.Value;
                        }
                        if (list.Key.Equals("fileMimeType"))
                        {
                            fileMimeType = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("fileName"))
                        {
                            fileName = list.Value;
                        }
                    }
                    if (fileContent != "" && fileContent != null && associatedID != null && fileMimeType != 0)
                    {
                        if (fileMimeType == 1)
                        {

                            string relativePath = writeFileInDirectory(associatedID, fileContent, ref fileName, "contents");
                            dt = model.selectIntoTable("select rid from Resume where refUserID=" + associatedID);
                            if (dt.Rows.Count == 0)
                            {
                                dt = model.insertIntoTableWithLastId("insert into Resume (refDocumentPath,refUserID,uploadDateTime,resumeTitle,resumeMime)" +
                                  "values ('" + relativePath + "','" + associatedID + "',now(),'" + fileName + "','" + fileMimeType + "')", "rid");
                                if (dt.Rows.Count != 0)
                                {
                                    response = new ResponseData(1, "saved", convertDataTableToJson(dt));
                                    return returnStmnt(response.returnStatment());
                                }
                            }
                            else
                            {
                                int rowcount = model.updateIntoTablewithRowCount("update Resume set refDocumentPath='" + relativePath +
                                    "',uploadDateTime=now(),resumeTitle='" + fileName + "',resumeMime='" + fileMimeType + "' where refUserID=" + associatedID);
                                if (rowcount != 0)
                                {
                                    response = new ResponseData(1, "Successfully updated", convertDataTableToJson(dt));
                                    return returnStmnt(response.returnStatment());
                                }
                            }
                        }
                        else if (fileMimeType == 2)
                        {
                            string relativePath = writeFileInDirectory(associatedID, fileContent, ref fileName, "uploadFriends");
                          //  dt = GetDataTableFromCsv(relativePath);
                            string responseInArray = readFriendsFromCSV(Convert.ToInt32(associatedID), relativePath, dt);
                            response = new ResponseData(1, "updated with result", responseInArray);
                            return returnStmnt(response.returnStatment());
                        }
                        else if (fileMimeType == 3)
                        {
                            string relativePath = writeFileInDirectory(associatedID, fileContent, ref fileName, "uploadJobs");
                          //  dt = GetDataTableFromCsv(relativePath);
                            string responseInArray = readJobsFromCSV(Convert.ToInt32(associatedID), relativePath, dt);
                            response = new ResponseData(1, "updated with result", responseInArray);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "input parameter is not vallid", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private static string writeFileInDirectory(string associatedID, string fileContent, ref string fileName, string fileSaveFolder)
    {
        fileName = fileName.Replace(" ", string.Empty);
        fileName = fileName.Replace(associatedID.ToString(), string.Empty);
        string relativePath = fileSaveFolder + "/" + associatedID + fileName;
        string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, relativePath);
        FileStream fileToupload = new FileStream(str_uploadpath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        byte[] bytearray = new byte[10000];
        bytearray = Convert.FromBase64String(fileContent);
        fileToupload.Write(bytearray, 0, bytearray.Length);
        fileToupload.Close();
        fileToupload.Dispose();
        return relativePath;
    }
    public DataTable GetDataTableFromCsv(string path)
    {
        DataTable dataTable = new DataTable();
        string[] csvRows = System.IO.File.ReadAllLines(path);
        string[] headers = csvRows[0].Split(',');

        // Adding columns name
        foreach (var item in headers)
        {
            dataTable.Columns.Add(new DataColumn(item));
        }
        dataTable.Columns.Add(new DataColumn("status"));
        string[] fields = null;
        int count = 0;
        foreach (string csvRow in csvRows)
        {
            if (count != 0)
            {
                fields = csvRow.Split(',');
                DataRow row = dataTable.NewRow();
                row.ItemArray = fields;
                row[fields.Length] = "";
                dataTable.Rows.Add(row);
            }
            else
            {
                count++;
            }
        }
        return dataTable;
    }
    private string readFriendsFromCSV(int uid, string filePath, DataTable tableForCsv)
    {
        string friendName = null;
        string emailID = null;
        string contactNO = null;
        string currentJobTitle = null;
        string friendSkills = null;
        string createdBy = null;
        string currentCompany = null;
        DataTable dt = new DataTable();
        if (uid != 0)
        {
            dt = model.selectIntoTable("select displayName from Users where uid=" + uid);
            if (dt.Rows.Count != 0)
            {
                createdBy = dt.Rows[0][0].ToString();
            }
        }
        StringBuilder jsonarray = new StringBuilder();
        jsonarray.Append("[{");

        var reader = new StreamReader(File.OpenRead(@Path.Combine(HttpRuntime.AppDomainAppPath, filePath)));
        List<string> listColumnName = new List<string>();
        List<string> listValues = new List<string>();
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnName in appendValues)
            {
                listColumnName.Add(columnName);
            }
            break;
        }
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnValues in appendValues)
            {
                listValues.Add(columnValues);
            }
            if (listColumnName.Count == 6 && listValues.Count == 6)
            {
                friendName = listValues[0];
                emailID = listValues[1];
                contactNO = listValues[2];
                currentJobTitle = listValues[3];
                friendSkills = listValues[4];
                currentCompany = listValues[5];
                string status = makeFriends(uid, createdBy, "From Excel", friendName, emailID, contactNO, currentJobTitle, friendSkills, currentCompany);
                jsonarray.Append("\"" + emailID + "\":\"" + status + "\"$");
                listValues.Clear();
                emailID = ""; friendName = ""; contactNO = ""; currentJobTitle = ""; friendSkills = ""; currentCompany = "";
            }
            else
            {
                jsonarray.Append("\"" + emailID
                    + "\":\",please ensure multiple values sepratedBy and no of values should be equal to no of  columnname ';' CSV \"$");
                listValues.Clear();
            }
        }

        return vallidJsonArray(jsonarray);
    }
    private static string vallidJsonArray(StringBuilder jsonarray)
    {
        int count = 0;

        string[] arrofStatus = jsonarray.ToString().Split('$');
        jsonarray.Clear();
        foreach (var singleStatus in arrofStatus)
        {
            if (count != arrofStatus.Length - 2)
            {
                jsonarray.Append(singleStatus + ",");
                count++;
            }
            else
            {
                jsonarray.Append(singleStatus);
            }
        }
        return jsonarray.Append("}]").ToString();
    }
    private string readJobsFromCSV(int uid, string filePath, DataTable tableForCsv)
    {
        int count = 0;
        string jobTitle = null;
        string jobMinSalary = null;
        string jobMaxSalary = null;
        string jobSalaryCurrency = null;
        string jobSkills = null;
        string jobReferenceLink = null;
        string jobExperienceMinYears = null;
        string jobExperienceMaxYears = null;
        string jobLocationName = null;
        string jobComments = null;
        string JobExpiryDays = null;
        string canDisplayPostedByName = null;
        string postToSocialSite = null;
        StringBuilder jsonarray = new StringBuilder();
        StringBuilder insertStmt = new StringBuilder();
        StringBuilder values = new StringBuilder();
        jsonarray.Append("[{");

        var reader = new StreamReader(File.OpenRead(@filePath));
        List<string> listColumnName = new List<string>();
        List<string> listValues = new List<string>();
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnName in appendValues)
            {
                listColumnName.Add(columnName);
            }
            break;
        }
        string dynamicExpiryDays = null;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            var appendValues = line.Split(',');
            foreach (var columnValues in appendValues)
            {
                listValues.Add(columnValues);
            }
            if (listValues.Count == 13 && listColumnName.Count == 13)
            {
                jobTitle = listValues[0];
                jobMinSalary = listValues[1];
                jobMaxSalary = listValues[2];
                jobSalaryCurrency = listValues[3];
                jobSkills = listValues[4];
                jobReferenceLink = listValues[5];
                jobExperienceMinYears = listValues[6];
                jobExperienceMaxYears = listValues[7];
                jobLocationName = listValues[8];
                jobComments = listValues[9];
                JobExpiryDays = listValues[10];
                canDisplayPostedByName = listValues[11];
                postToSocialSite = listValues[12];
                dynamicExpiryDays = JobExpiryDays == "" ? "30" : JobExpiryDays;
                if (jobTitle != "" && jobSkills != "" && jobLocationName != "" && uid != 0 && jobTitle != null && jobSkills != null && jobLocationName != null)
                {
                    try
                    {
                        insertStmt.Append("insert into Job (jobStatus,jobType,postedOnDateTime,jobExpiresOn,joblastModifiedOn," +
               "jobTitle,postedBy,jobSkills,jobLocationName,canDisplayPostedByName,jobReferenceLink,jobComments" +
               ",jobExperienceMinYears,jobExperienceMaxYears,jobMinSalary,jobMaxSalary,jobSalaryCurrency,postToSocialSite) ");
                        values.Append("values (1,2,now(),DATE_ADD(now(),INTERVAL " + dynamicExpiryDays + " DAY),now(),");
                        values.Append("'" + jobTitle + "'," + "'" + uid + "'," + "'" + jobSkills + "'," + "'" + jobLocationName + "'," + "'" + canDisplayPostedByName + "'," + "'" + jobReferenceLink + "'," +
        "'" + jobComments + "'," + "'" + jobExperienceMinYears + "'," + "'" + jobExperienceMaxYears + "'," +
        "'" + jobMinSalary + "'," + "'" + jobSalaryCurrency + "','" + jobMaxSalary + "'," + "'" + postToSocialSite + "')");
                        DataTable dt = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString(), "jid");
                        listValues.Clear();
                        insertStmt.Clear();
                        values.Clear();
                        //DataRow dr = tableForCsv.Rows[count];
                        //dr[tableForCsv.Columns.Count - 1] = "Successfull posted with jid = " + dt.Rows[0][0].ToString();
                        //count++;
                        jsonarray.Append("\"" + jobTitle + "\":\"Successfull,jobID = " + dt.Rows[0][0].ToString() + "\"$");
                        jobSalaryCurrency = ""; jobTitle = ""; jobMinSalary = ""; jobMaxSalary = ""; jobSkills = ""; jobReferenceLink = ""; jobExperienceMinYears = ""; jobExperienceMaxYears = ""; jobLocationName = ""; jobComments = ""; JobExpiryDays = ""; canDisplayPostedByName = ""; postToSocialSite = "";
                    }
                    catch (Exception exc)
                    {
                        //DataRow dr = tableForCsv.Rows[count];
                        //dr[tableForCsv.Columns.Count - 1] = "Fail !" + exc.Message;
                        //count++;
                        jsonarray.Append("\"" + jobTitle + "\":\"Fail ! " + exc.Message + "\"$");
                    }
                }
                else
                {
                    //DataRow dr = tableForCsv.Rows[count];
                    //dr[tableForCsv.Columns.Count - 1] = "Fail ! JobTitle,skills,location necessary columns";
                    //count++;
                    jsonarray.Append("\"" + jobTitle + "\":\"JobTitle,skills,location necessary columns\"$");
                    jobSalaryCurrency = ""; jobTitle = ""; jobMinSalary = ""; jobMaxSalary = ""; jobSkills = ""; jobReferenceLink = ""; jobExperienceMinYears = ""; jobExperienceMaxYears = ""; jobLocationName = ""; jobComments = ""; JobExpiryDays = ""; canDisplayPostedByName = ""; postToSocialSite = "";
                }

            }
            else
            {
                //DataRow dr = tableForCsv.Rows[count];
                //dr[tableForCsv.Columns.Count - 1] = "Fail ! Exceed columns value,please ensure multiple values sepratedBy CSV";
                //count++;
                jsonarray.Append("\"" + jobTitle + "\":\"Exceed columns value,please ensure multiple values sepratedBy CSV \"$");
            }
        }
        GenrateExcel excel = new GenrateExcel();
      //  excel.WriteToCsvFile(tableForCsv, "jobs" + uid + "processed", "csvFilesForJobProcessed");
        return vallidJsonArray(jsonarray);
    }
    private string makeFriends(int uid, string createdBy, string source, string friendName, string emailID, string contactNO, string currentJobTitle, string friendSkills, string currentCompany)
    {
        DataTable dt = new DataTable();
        try
        {
            if (friendName != null && emailID != null && friendName != "" && emailID != "")
            {
                StringBuilder insertStmt = new StringBuilder();
                StringBuilder values = new StringBuilder();
                insertStmt.Append("insert into Users" + "( userType,userStatus,userPassword,modifiedBy,modifiedDateTime,createdBy,createdDatetime,userName,userContactNo,displayName,currentJobTitle)");
                values.Append("values ('0','0','" + dateTimeClass.datetime() + "','" + createdBy + "',now(),'" + createdBy + "',now(),");
                values.Append("'" + emailID + "'," + "'" + contactNO + "'," + "'" + friendName + "','" + currentJobTitle + "')");
                string query = insertStmt.Append(values.ToString()).ToString();
                insertStmt.Clear();
                values.Clear();
                dt = model.selectIntoTable("select distinct(uid) from Users where userContactNo='" + contactNO + "' OR userName='" + emailID + "'");
                if (dt.Rows.Count == 0)
                {
                    dt = model.insertIntoTableWithLastId(query, "uid");
                    if (dt.Rows.Count != 0)
                    {
                        string refuid = dt.Rows[0][0].ToString();
                        model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + uid + "','" + refuid + "',now())");
                        model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + refuid + "','" + uid + "',now())");
                        model.insertIntoTable("insert into AuthenticationTokens (regUserId,lastModifiedDate,createdDateTime,authSite)values('" + refuid + "',now(),now(),'" + source + "')");
                        return "success";
                    }
                }
                else
                {
                    if (dt.Rows.Count != 0)
                    {
                        int refUserId = Convert.ToInt32(dt.Rows[0][0]);
                        dt = model.selectIntoTable("select * from RefereredUsers where regUserId='" + uid + "' and refUserId='" + refUserId + "'");
                        if (dt.Rows.Count == 0)
                        {
                            dt = model.selectIntoTable("select * from RefereredUsers where regUserId='" + refUserId + "' and refUserId='" + uid + "'");
                            if (dt.Rows.Count == 0)
                            {
                                model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + refUserId + "','" + uid + "',now())");

                            }
                            table = model.selectIntoTable("select * from FriendRequests where forFriendID=" + uid + " and toID =" + refUserId);
                            if (table.Rows.Count == 0)
                            {
                                insertStmt.Append("insert into FriendRequests (requestStatus,introducedOn,introducedBy,requestType,forFriendID,toID) values (0,now(),"
                                    + refUserId + "," + 1 + ","
                                    + uid + "," + refUserId + ")");
                                model.insertIntoTable(insertStmt.ToString());

                            }
                            return "duplicate friend";
                        }
                        else
                        {
                            return "friend exist";
                        }
                    }

                }
            }
            return "fail";
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
            return "fail !" + exc.Message;
        }


    }
    private byte[] GetBytes(Stream filecontents)
    {
        int result = 0;
        int bytesRead;
        byte[] buffer = new byte[100000];
        do
        {
            bytesRead = filecontents.Read(buffer, 0, buffer.Length);
            result += bytesRead;
        } while (bytesRead > 0);
        return buffer.Take(result).ToArray();
    }
    public Stream getCurrentRewards(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    int associatedRegId = 0;
                    int sumOfRewardes = 0;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("associatedRegId"))
                        {
                            associatedRegId = Convert.ToInt32(list.Value);
                        }
                    }
                    if (associatedRegId != 0)
                    {
                        sumOfRewardes = rewards.sumOfRewardes(associatedRegId);
                        returnString = "{\"status\":0,\"Description\":\"Your Rewards is calculted  \",\"results\":[{\"Rewards\":" + sumOfRewardes + "}]}";
                    }
                }
            }
            returnString = "{\"status\":0,\"Description\":\"  \",\"results\":[null]}";
            return returnStmnt(returnString);
        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[null]}";
            return returnStmnt(returnString);
        }
    }
    public Stream getCDNUrl(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                //data have 4 parameters=applicationName,location,lattitude,longitude
                {
                    response = new ResponseData(1, Constant.foundData, "[{\"CDNUrl\":\"https://csimplifyit.com/takemyjob.svc/\"}]");
                    return returnStmnt(response.returnStatment());
                }

            }
            // auditInfo("takemyjob", "getfriends", "want to get refrence friends",regid,Constant.jsonNotCorrect , null, null);
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getInterestedFriendsInmyjobs(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int postedBy = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("postedBy"))
                    {
                        postedBy = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select jid, count(appliedbyUser) as jobApplications,(jobExpiresOn>now()) as isJobExpired,  DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle,jobReferenceLink,joblatitude,jobLongitude,jobLocationName,jobType,jobComments," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn , DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,"
                            + "jobVertical,jobRole from Job As J LEFT JOIN   JobApplications As A ON J.jid = A.jobRefId Where J.postedBy = '" + postedBy + "' group by jid order by jobApplications desc,ABS( DATEDIFF( J.jobExpiresOn, NOW() ) ), postedOnDateTime desc");
                        string result = convertDataTableToJson(dt);
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getResumesForJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            string jobID = null;
            int userID = 0;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getResumesForJob", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }
                    if (jobID != null & userID != 0)
                    {
                        dt = model.selectIntoTable("select *  from RefereredUsers where regUserId=1 and refUserId=" + userID);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(0, "User is unsubscribe", "[{\"uid\":\"" + userID + "\"");
                            return returnStmnt(response.returnStatment());

                        }
                        int qid = queue.insertdetailForPostResumes(userID, Convert.ToInt32(jobID));
                        queue.processTo(qid);
                        response = new ResponseData(1, "process is in queue", "[{\"qid\":\"" + qid + "\"}]");
                        return returnStmnt(response.returnStatment());
                    }

                }
            }
            response = new ResponseData(0, "input parameters are not completed ", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            string exception = ex.Message.Replace('\'', ' ');
            response = new ResponseData(0, exception, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getMessagesFromFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        // string returnString = null; 
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    // recievedBy,recievedFrom,recievedSince,Message,messageType
                    string recievedBy = null;
                    string recievedFrom = null;
                    string recievedSince = "";
                    string dynamicWhereClause = null;
                    JObject root = JObject.Parse("{\"data\":" + objelement.Value.ToString() + "}");
                    JArray items = (JArray)root["data"];
                    foreach (var data in items)
                    {
                        JObject localdata = JObject.Parse(data.ToString());
                        foreach (var jsonobject in localdata)
                        {
                            {
                                if (jsonobject.Key.Equals("myID"))
                                {
                                    recievedBy = jsonobject.Value.ToString();
                                }
                                if (jsonobject.Key.Equals("friendID"))
                                {
                                    recievedFrom = jsonobject.Value.ToString();
                                }
                                if (jsonobject.Key.Equals("recievedSince"))
                                {
                                    recievedSince = jsonobject.Value.ToString();
                                }
                            }
                        }
                    }
                    if (recievedSince != "")
                    {
                        DateTime dateValue = DateTime.Parse(recievedSince);
                        recievedSince = dateValue.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    dynamicWhereClause = recievedSince == "" ? "" : " and messageDateTime > '" + recievedSince + "'";
                    if (Convert.ToInt32(recievedFrom) != 0)
                    {
                        model.updateIntoTable("update FriendlyMessages set messageStatus=2 where forID = " + recievedFrom + " and messageStatus=1 and byID = " + recievedBy);
                        dt = model.selectIntoTable("select fid as ChatID, messageMime,DATE_FORMAT( messageDateTime,'%a %b %D %r') as messageDateTime,messageDateTime as lastModifiedDate, messageStatus, messageRefUrl, msgType,forID,byID, message " +
                             "from FriendlyMessages where messageStatus!=-1 and forID = " + recievedBy + " and byID = " + recievedFrom + dynamicWhereClause + " UNION " +
                             "select fid as ChatID, messageMime, DATE_FORMAT( messageDateTime,'%a %b %D %r') as messageDateTime,messageDateTime as lastModifiedDate, messageStatus, messageRefUrl, msgType,forID,byID, message from FriendlyMessages where messageStatus!=-1 and byID=" + recievedBy +
                          " and forID =" + recievedFrom + dynamicWhereClause + " order by lastModifiedDate desc Limit 50");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Messages", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream sendMessagetoFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string sendBy = null;
                    string message = null;
                    string messageType = null;
                    string sendTo = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("sendBy"))
                        {
                            sendBy = list.Value;
                        }
                        if (list.Key.Equals("message"))
                        {
                            message = list.Value;
                        }
                        if (list.Key.Equals("messageType"))
                        {
                            messageType = list.Value;
                        }
                        if (list.Key.Equals("sendTo"))
                        {
                            sendTo = list.Value;
                        }
                    }
                    if (Convert.ToInt32(sendBy) != 0)
                    {
                        string responseOfInsert = model.insert("insert into FriendlyMessages(messageMime, messageDateTime, messageStatus, messageRefUrl, msgType,forID,byID, message) " +
                             "values('application/text', now(), 1, ''," + messageType + ", " + sendBy + ", " + sendTo + ", '" + message + "' )");
                        if (responseOfInsert.Equals("true"))
                        {
                            response = new ResponseData(0, Constant.messageSaved, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }

                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream queueSingleNotification(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Queue queue = new Queue();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    int queueType = 0;
                    int queueRegarding = 0;
                    int queueRegardingRefId = -1;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("queueType"))
                        {
                            queueType = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("queueRegarding"))
                        {
                            queueRegarding = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("queueRegardingRefId"))
                        {
                            queueRegardingRefId = Convert.ToInt32(list.Value);
                        }

                    }
                    if (queueRegarding != 0 && queueRegardingRefId != -1)
                    {
                        if (queueRegarding == 1)
                        {
                            queue.queueJobNotification(queueRegardingRefId);
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream postCommentOnYourSocialWall(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        // string returnString = null;
        string userID = null;

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        if (keypair.Key == "userID")
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string postOnSocialNetwork = null;
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("postOnSocialNetwork"))
                        {
                            postOnSocialNetwork = list.Value;
                        }
                        if (list.Key.Equals("postOnSocialNetwork"))
                        {
                            postOnSocialNetwork = list.Value;
                        }
                        if (list.Key.Equals("postOnSocialNetwork"))
                        {
                            postOnSocialNetwork = list.Value;
                        }

                    }
                    string responselinkedin = linkedin.updateStatusOnOauthServer(null, userID);
                    if (responselinkedin.Equals("true"))
                    {
                        response = new ResponseData(1, "Successfully posted", null);
                    }
                    else if (responselinkedin.Equals("false"))
                    {
                        response = new ResponseData(1, "Please register with your linkedin account with us.", null);
                    }
                }
                // response = new ResponseData(0, "Not a Vallid User", null);

            }
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private string callTwilioFormessage(string message)
    {
        try
        {
            string param = "Body={'inputData':{'meta': {'token': '098084038403240434324324kjhkhfjhdskf','action':'actionName','validation': 'runthisvalidationonserver', 'previousaction': 'actionName' },'data':[{'contactNo':'8950706505','message':'hi parveen'},{'contactNo':'965834474871','message':'thanks'}]}}";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://use-this.appspot.com/");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] buffer = System.Text.Encoding.Unicode.GetBytes(param);
            Stream POSTstream = request.GetRequestStream();
            POSTstream.Write(buffer, 0, buffer.Length);
            HttpWebResponse POSTResponse = (HttpWebResponse)request.GetResponse();
            Stream responseStream = POSTResponse.GetResponseStream();
            byte[] responsebytes = GetBytes(responseStream);
            string response = System.Text.Encoding.UTF8.GetString(responsebytes);
        }
        catch (Exception ecx)
        {
            Console.WriteLine(ecx.Message);
        }
        return "";
    }
    private Boolean vallidateJson(string message)
    {
        try
        {
            JObject vallidateobject = JObject.Parse("{\"data\":\"" + message + "\"}");
            return true;
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
        }
        return false;
    }
    public Stream likeCount(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;
                    string jobComments = null;
                    string expiryDays = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }
                        if (list.Key.Equals("jobComments"))
                        {
                            jobComments = list.Value;
                        }
                        if (list.Key.Equals("expiryDays"))
                        {
                            expiryDays = list.Value;
                        }
                    }
                    if (userID != null && jobID != null)
                    {
                        int rows = model.updateIntoTablewithRowCount("update Job set jobComments='" + jobComments + "',jobExpiresOn="
                            + "DATE_ADD(now(),INTERVAL " + Convert.ToInt32(expiryDays) + " DAY) where jid=" + jobID + " and postedBy=" + userID);
                        if (rows != 0)
                        {
                            response = new ResponseData(1, "Your comments for job has been updated", null);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "You have no job for comments", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }

    }
    public Stream forgotPassword(string inputData)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        string returnString = null;
        int uid = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {

                }
                if (objelement.Key.Equals("data"))
                {

                    list = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            string username = null;
            foreach (var keyvalue in list)
            {
                if (keyvalue.Key.Equals("userName"))
                {
                    username = keyvalue.Value;
                }
            }
            if (username != null)
            {
                dt = model.selectIntoTable("select uid userPassword from Users where userName='" + username + "' and userType=1");
                if (dt.Rows.Count != 0)
                {
                    uid = Convert.ToInt32(dt.Rows[0][0]);
                    //byte[] array = Encoding.ASCII.GetBytes(uid.ToString() + dateTimeClass.datetime());
                    //string encoded = System.Convert.ToBase64String(array);
                    //dt = model.selectIntoTable("select * from AuthenticationTokens where regUserId=" + uid + " and authSite='system'");
                    //if (dt.Rows.Count == 0)
                    //{
                    //    model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,authSite) value (" + uid + ",'" + encoded + "','system')");

                    //}
                    //else
                    //{
                    //    model.updateIntoTable("update AuthenticationTokens set token='" + encoded + "' where regUserId=" + uid + " and authSite='system'");
                    //}
                    dt = model.selectIntoTable("select *  from RefereredUsers where regUserId=1 and refUserId=" + uid);
                    if (dt.Rows.Count != 0)
                    {
                        response = new ResponseData(0, "unSubscribe User", "[{\"uid\":\"" + uid + "\"");
                        return returnStmnt(response.returnStatment());

                    }
                    int qid = queue.queueForgotMail(uid);
                    response = new ResponseData(1, "Process is in queue ", "[{\"uid\":\"" + uid + "\",\"qid\":\"" + qid + "\"}]");
                    return returnStmnt(response.returnStatment());

                }
                else
                {
                    response = new ResponseData(0, "invallid userName", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input data is not correct", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            returnString = response.returnStatment();
            return returnStmnt(returnString);
        }
    }
    public Stream updateCommentOnJob(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "postResumeToHelpingFriends", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string jobComments = null;
                    string expiryDays = null;
                    string canDisplayJobByName = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("jobComments"))
                        {
                            jobComments = list.Value;
                        }
                        if (list.Key.Equals("expiryDays"))
                        {
                            expiryDays = list.Value;
                        }
                        if (list.Key.Equals("canDisplayJobByName"))
                        {
                            canDisplayJobByName = list.Value;
                        }
                    }
                    if (userID != 0 && jobID != null && canDisplayJobByName != null)
                    {
                        int rows = model.updateIntoTablewithRowCount("update Job set canDisplayPostedByName='" + canDisplayJobByName + "',jobComments='" + jobComments + "',jobExpiresOn="
                            + "DATE_ADD(now(),INTERVAL " + Convert.ToInt32(expiryDays) + " DAY) where jid=" + jobID + " and postedBy=" + userID);
                        if (rows != 0)
                        {
                            dt = model.selectIntoTable("select jid from Job where jid= " + jobID + " and postedBy = " + userID);
                            if (dt.Rows.Count != 0)
                            {
                                if (canDisplayJobByName != null)
                                {
                                }
                                response = new ResponseData(1, "Your comments for job has been updated", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else
                        {
                            response = new ResponseData(1, "You have no job for comments", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }

    }
    public Stream getJobApplications(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int jobID = 0;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "postResumeToHelpingFriends", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("jobID"))
                    {
                        jobID = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select distinct(jid) , appliedByUser,userName, userContactNo, displayName, DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle, DATE_FORMAT(appliedOnDateTime,'%d-%b-%Y') as appliedOnDateTime," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn, "
                            + "jobVertical,currentJobTitle as jobRole from Users U,  JobApplications As A LEFT JOIN Job As J  ON J.jid = A.jobRefId Where A.appliedByUser = U.uid and A.jobRefId = " + jobID + " order by appliedOnDateTime desc");
                        string result = convertDataTableToJson(dt
                            );
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream revokeJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "postResumeToHelpingFriends", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(list.Value);
                        }

                    }
                    if (userID != 0 && jobID != null)
                    {
                        int countJobRowUpdate = model.updateIntoTablewithRowCount("update Job set jobStatus=-2 where jid=" + jobID + " and postedBy=" + userID);
                        if (countJobRowUpdate > 0)
                        {
                            dt = model.selectIntoTable("select jid from Job where jid=" + jobID + " and postedBy=" + userID);
                            response = new ResponseData(1, "Job Revoked", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Could not revoke job", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream revokeJobApplication(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }

                    }
                    if (userID != null && jobID != null)
                    {
                        int countJobRowUpdate = model.updateIntoTablewithRowCount("update JobApplications set applicationStatus=4 where jobRefid=" + jobID + " and appliedByUser=" + userID);
                        if (countJobRowUpdate > 0)
                        {
                            dt = model.selectIntoTable("select jaid from JobApplications where jobRefid=" + jobID + " and appliedByUser=" + userID);
                            response = new ResponseData(1, "Job application Revoked", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Could not revoke job application", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream registerForServerNotification(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "post new Job", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string serverNotificationToken = null;

                    string deviceID = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("serverNotificationToken"))
                        {
                            serverNotificationToken = list.Value;
                        }
                        if (list.Key.Equals("deviceID"))
                        {
                            deviceID = list.Value;
                        }

                    }
                    if (userID != 0 && deviceID != null && serverNotificationToken != null)
                    {
                        int RowUpdate = model.updateIntoTablewithRowCount("update AuthenticationTokens where socialID ='" + deviceID + "',token='" + serverNotificationToken + "' where regUserId=" + userID);
                        if (RowUpdate != 0)
                        {
                            dt = model.insertIntoTableWithLastId("insert into AuthenticationTokens(regUserId,token,lastModifiedDate,createdDateTime) values"
                                + userID + ",'" + serverNotificationToken + "',now(),now()", "auid");
                            if (dt.Rows.Count != 0)
                            {
                                response = new ResponseData(1, "Server notification token successfully saves", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }

                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream introduceFriends(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        if (keypair.Key.Equals("userID"))
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != null)
                {
                    insertedIntoFriendRequests("{\"data\":" + objelement.Value.ToString() + "}", userID);
                    response = new ResponseData(1, "success", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertedIntoFriendRequests(string jsonarr, string uid)
    {
        DataTable dt = new DataTable();
        string requestType = null;
        string forFriendID = null;
        string toID = null;
        StringBuilder insertStmt = new StringBuilder();
        JObject root = JObject.Parse(jsonarr);
        JArray items = (JArray)root["data"];
        foreach (var data in items)
        {
            JObject localdata = JObject.Parse(data.ToString());
            foreach (var keyvalues in localdata)
            {
                if (keyvalues.Key.Equals("requestType"))
                {
                    requestType = keyvalues.Value.ToString();
                } if (keyvalues.Key.Equals("forFriendID"))
                {
                    forFriendID = keyvalues.Value.ToString();
                } if (keyvalues.Key.Equals("toID"))
                {
                    toID = keyvalues.Value.ToString();
                }
            }
            if (forFriendID != toID)
            {
                table = model.selectIntoTable("select * from RefereredUsers where regUserId=" + toID + " and refUserId=" + forFriendID);
                if (table.Rows.Count == 0)
                {
                    table = model.selectIntoTable("select * from FriendRequests where forFriendID=" + forFriendID + " and toID =" + toID);
                    if (table.Rows.Count == 0)
                    {
                        insertStmt.Append("insert into FriendRequests (requestStatus,introducedOn,introducedBy,requestType,forFriendID,toID) values (0,now(),"
                            + uid + "," + requestType + ","
                            + forFriendID + "," + toID + ")");
                        model.insertIntoTable(insertStmt.ToString());

                    }
                    insertStmt.Clear();
                }
            }
        }
        return true;
    }
    public Stream getPendingRequests(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key == "userID")
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string introducedBy = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("introducedBy"))
                        {
                            introducedBy = list.Value;
                        }
                    }
                    if (userID != null && introducedBy != null)
                    {
                        dt = model.selectIntoTable("select distinct(F.frid),U.uid,U.displayName,U.currentJobTitle," +
                            " I.displayName as introduceBy from Users as U, Users as I ,FriendRequests as F where U.uid=F.forFriendID and " +
                       "RequestStatus=0 and  F.introducedBy=I.uid and toID=" + userID + " and F.introducedBy=" + introducedBy);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream introductionUpdate(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key.Equals("userID"))
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != null)
                {
                    insertedIntroductionUpdate("{\"data\":" + objelement.Value.ToString() + "}", userID);
                    response = new ResponseData(1, "success", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertedIntroductionUpdate(string jsonarr, string toID)
    {
        DataTable dt = new DataTable();
        // DataRow row;
        string forFriendID = null;
        string updateStatus = null;
        StringBuilder insertStmt = new StringBuilder();
        JObject root = JObject.Parse(jsonarr);
        JArray items = (JArray)root["data"];
        foreach (var jsonkeyvalue in items)
        {
            JObject localdata = JObject.Parse(jsonkeyvalue.ToString());
            foreach (var list in localdata)
            {                          // sendBy,sendTo,Message,messageType
                if (list.Key.Equals("forFriendID"))
                {
                    forFriendID = list.Value.ToString();
                }
                if (list.Key.Equals("updateStatus"))
                {
                    updateStatus = list.Value.ToString();
                }
                if (toID != null && forFriendID != null && updateStatus != null)
                {
                    int status = Convert.ToInt32(updateStatus);
                    dt = model.selectIntoTable("select * from RefereredUsers where regUserId=" + toID + " and refUserId=" + forFriendID);
                    if (dt.Rows.Count == 0)
                    {
                        if (status == 1)
                        {
                            dt = model.insertIntoTableWithLastId("insert into RefereredUsers (regUserId,refUserId,createdDateTime) values(" + toID + "," + forFriendID + ",now())", "rid");
                            if (dt.Rows.Count != 0)
                            {
                                model.updateIntoTable("update FriendRequests set requestStatus=1 where forFriendID=" + forFriendID + " and toID=" + toID);

                                //table.Rows.Add(row);
                            }
                        }
                        else if (status == -1)
                        {
                            model.updateIntoTable("update FriendRequests set requestStatus=-1 where forFriendID=" + forFriendID + " and toID=" + toID);
                        }
                    }
                }
            }


        }
        return true;
    }
    public string keyValuePairToJson(List<KeyValuePair<string, string>> list)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var KeyValuePair in list)
        {
            //sb.Append("[{"+list.+"}");
        }
        return sb.ToString();
    }
    public Stream updateJobApplicationComments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            //flag=true;
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;

                    string applicationNotes = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("applicationNotes"))
                        {
                            applicationNotes = list.Value;
                        }
                    }
                    if (userID != null && jobID != null && applicationNotes != null)
                    {
                        int rowUpdates = model.updateIntoTablewithRowCount("update JobApplications set applicationNotes='" + applicationNotes + "' where jobRefid=" + jobID
                            + " and appliedByUser=" + userID);
                        dt = model.selectIntoTable("select jaid from JobApplications where  jobRefid=" + jobID
                            + " and appliedByUser=" + userID);

                        if (rowUpdates != 0 && dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Application notes successfully updated", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Application notes not updated", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }

    }
    public Stream getJobApplicationComments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;

                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }

                    }
                    if (jobID != null)
                    {
                        dt = model.selectIntoTable("select U.currentJobTitle , applicationNotes, appliedByUser,DATE_FORMAT(appliedOnDateTime,'%d-%b-%Y') as appliedOnDateTime from JobApplications,Users as U where appliedByUser=U.uid and jobRefid= " + jobID);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "application comments ", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "you have no any application comments for this job", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not completed", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }
    }
    public Stream getJobDetail(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key == "userID")
                        {
                            userID = Convert.ToInt32(keypair.Value);
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }
                    if (jobID != null)
                    {
                        dt = model.selectIntoTable("select distinct(jid),(case When canDisplayPostedByName = 1 then  U.displayName else 'Guess IT' end ) as postedByName,jobMinSalary,jobMaxSalary,jobSalaryCurrency,canDisplayPostedByName,DateDiff(J.jobExpiresOn,now()) as expireinDays,count(JA.jaid) as jobApplications,DATE_FORMAT(JA2.appliedOnDateTime,'%d-%b-%Y') as jobAppliedOnDate,(JA2.appliedByUser IS NOT NULL) as isJobApplied" +
                        ",postedBy,DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                        "jobTitle,(case When jobReferenceLink = 'null' then 'No url' else jobReferenceLink end ) as jobReferenceLink,joblatitude,jobLongitude,(case When jobLocationName = 'null' then 'No location'  else jobLocationName end ) as jobLocationName ,jobType,jobComments," +
                        "jobSkills,DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') " +
                        "as jobExpiresOn ,DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,SUM(JA.applicationNotes IS NOT NULL) as commentsCount," +
                         "jobExperienceMinYears,jobExperienceMaxYears,jobVertical,jobRole from " +
                     " Users as U, Job As J Left Join JobApplications As JA on " +
                        "JA.jobRefid=J.jid Left join JobApplications As JA2 on JA2.appliedByUser=" + userID + " and " +
                         " JA2.jobRefid=J.jid " +
                            " where U.uid=J.postedBy and J.jid =" + jobID +
                          " and jobStatus = 1 and jobExpiresOn > now()  " +
                          " group by jid " +
                           " order by postedOnDateTime desc ");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getFriendDetail(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key == "userID")
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string friendID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("friendID"))
                        {
                            friendID = list.Value;
                        }
                    }
                    if (friendID != null)
                    {
                        dt = model.selectIntoTable("SELECT distinct(uid),count(toID) as pendingRequests,displayName,userName,userContactNo," +
                                "refUserPrivacyConcern as isBlocked ,userType,userStatus,currentCompanyName,currentJobTitle,currentIndustry FROM " +
                              " Users A,RefereredUsers B  Left JOIN   FriendRequests As F ON F.RequestStatus=0" +
                                " and F.introducedBy=B.refUserId and F.toID=B.regUserId WHERE A.uid = B.refUserId AND B.regUserId =" + userID + " and B.RefUserId=" +
                                friendID + "  group by uid order by displayName");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }


    }
    public Stream getTokenByUserID(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "userID")
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data"))
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string password = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("userPassword"))
                        {
                            password = list.Value;
                        }
                    }
                    if (userID != null && password != null)
                    {
                        password = EncryptBase64.EncodeTo64(password);
                        dt = model.selectIntoTable("select A.token, U.uid,U.displayName as uid from AuthenticationTokens as A,Users as U where A.regUserId=U.uid  and A.regUserId=" + userID +
                     " and U.userPassword='" + password + "' and A.authSite=''");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "token generated", "[{\"token\":\"" + dt.Rows[0][0].ToString() + "\",\"uid\":\"" + dt.Rows[0][1].ToString() + "\",\"displayName\":\"" + dt.Rows[0][2].ToString() + "\" }]");
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(0, "password incorrect", "[{\"uid\":\"" + userID + "\" }]");
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "userID/password missing", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    //public Stream getFriendsResume(string inputData)
    //{
    //    List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
    //    string tempFilePath = null;
    //    try
    //    {
    //        DataTable dt = new DataTable();
    //        JObject jsonrow = JObject.Parse(inputData);
    //        Boolean flag = false;
    //        string friendID = null;
    //        string userID = null;
    //        string jobID = null;
    //        foreach (var objelement in jsonrow)
    //        {
    //            if (objelement.Key.Equals("meta"))
    //            {
    //                jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
    //                foreach (var keypair in jsonkeyvalue)
    //                {
    //                    if (keypair.Key == "token")
    //                    {
    //                        flag = true;
    //                        if (flag != true)
    //                        {
    //                            response = new ResponseData(0, Constant.invallidtoken, null);
    //                            return returnStmnt(response.returnStatment());
    //                        }
    //                    }
    //                    else if (keypair.Key == "userID")
    //                    {
    //                        userID = keypair.Value;
    //                    }
    //                }
    //            }
    //            if (objelement.Key.Equals("data") && flag == true)
    //            {
    //                jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
    //                foreach (var list in jsonkeyvalue)
    //                {
    //                    if (list.Key.Equals("friendID"))
    //                    {
    //                        friendID = list.Value;
    //                    }
    //                    if (list.Key.Equals("jobID"))
    //                    {
    //                        jobID = list.Value;
    //                    }
    //                }
    //                if (friendID != null && userID != null && jobID!=null  )
    //                {
    //                    dt = model.selectIntoTable("select jaid from JobAppLication where jobRefid =" + jobID + " and appliedByUser=" + friendID);
    //                    if (dt.Rows.Count != 0)
    //                    {
    //                        dt = model.selectIntoTable("select distinct(U.uid), U.displayName ,R.refDocumentPath from Users as U , Resume as R " +
    //                            "where refUserId=uid and uid=" + friendID);
    //                        List<string> listOfFilePath = new List<string>();
    //                        if (dt.Rows.Count != 0)
    //                        {
    //                            string refdocumentPath = dt.Rows[0][1].ToString();
    //                            if (refdocumentPath != "")
    //                            {
    //                                listOfFilePath.Add(refdocumentPath);
    //                                tempFilePath = Path.Combine(HttpRuntime.AppDomainAppPath, "content\\" + dt.Rows[0][0].ToString() + dt.Rows[0][0].ToString() + ".Zip");
    //                                ZipHelper.ZipFiles(tempFilePath, listOfFilePath);
    //                                string zipPath= zipFile.zipResumeFromArrOfPath(listOfFilePath,Convert.ToInt32(userID));
    //                                int qid = queue.insertdetailForPostResumes(Convert.ToInt32(userID), "Resumes of friends those ap);
    //                                queue.processTo(qid, tempFilePath);
    //                                response = new ResponseData(1, "Resumes sent", "[{\"qid\":\"" + qid + "\"}]");
    //                                return returnStmnt(response.returnStatment());
    //                            }
    //                            else
    //                            {
    //                                response = new ResponseData(1, "Friend have no any resume", null);
    //                                return returnStmnt(response.returnStatment());
    //                            }
    //                        }
    //                        else
    //                        {
    //                            response = new ResponseData(1, "userID not vallid", null);
    //                            return returnStmnt(response.returnStatment());
    //                        }
    //                    }
    //                    else {
    //                        response = new ResponseData(1, "You can't see resume of this ID", null);
    //                        return returnStmnt(response.returnStatment());
    //                    }
    //                }

    //            }
    //        }
    //        response = new ResponseData(0, "input parameters are not completed ", null);
    //        return returnStmnt(response.returnStatment());
    //    }
    //    catch (Exception ex)
    //    {
    //        string exception = ex.Message.Replace('\'', ' ');
    //        response = new ResponseData(0, exception, null);
    //        return returnStmnt(response.returnStatment());
    //    }
    //}
    public Stream getJobTrackingDetail(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int jobID = 0;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getJobTrackingDetail", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("jobID") && userID != 0)
                    {
                        jobID = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select jid, count(appliedbyUser) as jobApplications,(jobExpiresOn>now()) as isJobExpired,  DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle,jobReferenceLink,joblatitude,jobLongitude,jobLocationName,jobType,jobComments," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn , DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,"
                            + "jobVertical,jobRole from Job As J LEFT JOIN   JobApplications As A ON J.jid = A.jobRefId Where J.jid= " + jobID + " and J.postedBy = '" + userID + "' group by jid ");
                        string result = convertDataTableToJson(dt);
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream deleteFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "delete Friend", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string friendID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("friendID"))
                        {
                            friendID = list.Value;
                        }
                    }
                    if (userID != 0 && friendID != null)
                    {
                        model.deleteFromTable("delete from RefereredUsers where regUserId=" + userID + " and refUserId=" + friendID);
                        response = new ResponseData(1, "friend Removed", "[{\"regUserId\":\"" + userID + "\",\"refUserId\":\"" + friendID + "\" }]");
                        return returnStmnt(response.returnStatment());

                    }
                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this friend is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream clearChat(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "clear Chat", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string forFriendID = null;
                    string chatID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("forFriendID"))
                        {
                            forFriendID = list.Value;
                        }
                        if (list.Key.Equals("chatID"))
                        {
                            chatID = list.Value;
                        }
                    }

                    if (userID != 0 && forFriendID != "")
                    {
                        model.updateIntoTable("update FriendlyMessages set messageStatus=-1 where byID=" + forFriendID + " and forID=" + userID);
                        response = new ResponseData(1, "friend Removed", "[{\"byID\":\"" + userID + "\",\"forID\":\"" + forFriendID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }
                    else if (chatID != "")
                    {
                        model.deleteFromTable("update FriendlyMessages set messageStatus=-1 where fid=" + chatID);
                        response = new ResponseData(1, "chat cleaned", "[{\"fid\":\"" + chatID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this messages is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream changePassword(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "change Password", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string password = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("password"))
                        {
                            password = list.Value;
                        }
                    }

                    if (userID != 0 && password != null && password != "")
                    {
                        model.insertIntoTable("update Users set userPassword='" + EncryptBase64.EncodeTo64(password) + "' where uid=" + userID);

                        response = new ResponseData(1, "Password changed", "[{\"uid\":\"" + userID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }

                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream subscribe(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "change Password", userID);
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string password = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("password"))
                        {
                            password = list.Value;
                        }
                    }

                    if (userID != 0 && password != null && password != "")
                    {
                        model.insertIntoTable("update Users set userPassword='" + EncryptBase64.EncodeTo64(password) + "' where uid=" + userID);
                        response = new ResponseData(1, "Password changed", "[{\"uid\":\"" + userID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }

                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this messages is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream unsubscribe(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int userID = 0;
        string token = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            token = keypair.Value;
                            dt = model.selectIntoTable("select regUserId from AuthenticationTokens where token='" + token + "'");
                            if (dt.Rows.Count != 0)
                            {
                                userID = Convert.ToInt32(dt.Rows[0][0]);
                                auditInfo(inputData, 1, "unSubscribe", userID);
                            }
                        }

                    }
                }
                if (objelement.Key.Equals("data"))
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");


                    if (userID != 0)
                    {
                        model.insertIntoTable("insert into RefereredUsers  (regUserId,refUserId,createdDateTime) values (1," + userID + ",now())");
                        response = new ResponseData(1, "User has been unSubscribed", "[{\"uid\":\"" + userID + "\" }]");
                        return returnStmnt(response.returnStatment());
                    }

                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "token not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getUsageStats(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int userID = 0;
        string role = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else if (keypair.Key.Equals("userID"))
                        {
                            userID = Convert.ToInt32(keypair.Value);
                            if (userID != 0)
                            {
                                auditInfo(inputData, 1, "getUsageStats", userID);
                            }
                        }
                        else if (keypair.Key.Equals("role"))
                        {
                            role = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    if (role.Equals("admin"))
                    {
                        dt = model.selectIntoTable(" select (select  count(*)  from Users) as totalUsers,(select count(*) from Users where userType=1 )as regUserCount, " +
"(select  count(*)  from AuthenticationTokens where lastModifiedDate>DATE_ADD(now(),INTERVAL -1 DAY) ) as userLoggedInToday, " +
"(select  count(*)  from Job) as totaljobPosted,(select  count(*)  from Job where jobType=1) as totaljobsFromResignations, " +
"(select  count(*)  from Job " +
" where jobStatus=1 and jobExpiresOn>now() ) as countOfActiveJobsToday, " +
"(select  count(*)  from Job where postedOnDateTime>DATE_ADD(now(),INTERVAL -1 DAY)) as jobPostedToday, " +
"(select  count(*)  from JobApplications where appliedOnDateTime>DATE_ADD(now(),INTERVAL -1 DAY)) as applicationsToday " +
",(select  count(*)  from JobApplications) as totalApplication ");
                        response = new ResponseData(1, "Results", convertDataTableToJson(dt));
                        return returnStmnt(response.returnStatment());
                    }
                    else
                    {
                        response = new ResponseData(0, "inputData parameter not completed", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, "this user is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
}



// metaToKeyValuePair(" {\"token\": 1,\"action\": \"actionName\", \"validation\": \"runthisvalidationonserver\", \"previousaction\": \"actionName\" }");
//   insertJsonintoUsers("Users", "{ \"data\": [   {   \"name\": \"kuldeep\",   \"userContactNo\": \"89769954\",   \"userName\": \"abc@gmail.com;\",\"source\": \"iOS/Android/LinkedIn/FaceBook etc\" },   {\"name\":\"mohit\",\"userContactNo\": \"8901232002\",\"emailid\": \"cvf@gmail.com\",\"source\":\"iOS Or Android/LinkedIn/FaceBook etc\",  }]}", 1);
// .........require a candidate for the job having an experience of .........

// getCsimplifyitCDNBasedServerUrl(string inputData,application name,lattitude ,longitude,divicetype,){
//return "baseurl"
//}
//userid 
//sendMessageRoFriend
// sendBy,sendTo,Message,messageType

//getMessagesFromFriend
// recievedBy,recievedFrom,recievedSince,Message,messageType
//  updateComments and Expiry(userid,no of days)jid commnet expirydays


//getJobDetail(jobid)
//getFriend (friend)
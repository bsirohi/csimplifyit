﻿

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>C Simplify IT</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.jpg"/>
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.jpg"/>
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.jpg"/>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicons/favicon.ico"/>
    <link rel="manifest" href="assets/img/favicons/manifest.json"/>
    <meta name="msapplication-TileImage" content="assets/img/favicons/mstile-150x150.jpg"/>
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="assets/lib/loaders.css/loaders.min.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
    <link href="assets/lib/remodal/remodal.css" rel="stylesheet"/>
    <link href="assets/lib/remodal/remodal-default-theme.css" rel="stylesheet"/>
    <link href="assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet"/>
    <link href="assets/lib/lightbox2/css/lightbox.css" rel="stylesheet"/>
    <link href="assets/css/theme.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/css/simpleMassMailMerge.css"/>
</head>
<body>
    
    <!--===============================================-->
    <!--    Fancynav-->
    <!--===============================================-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#landing-page">
          <img src="assets/img/logo.png" alt="C Simplify IT" height="45" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown1">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#landing-page" >Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" id="sln"  href="Default.aspx#whatwedo">Solutions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#offerings">Offerings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#technology">Technologies</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbarDropdownMenuProduct" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product</a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuProduct">
                <a class="dropdown-item" href="./simpleMassMailMerge.html">Simple Mass Mail Merge</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#whyus">Why Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#clients">Our Clients</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact Us</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!--===============================================-->
    <!--    End of Fancynav-->
    <!--===============================================-->



    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main minh-100vh" id="top">


      <!-- ============================================-->
      <!-- Preloader ==================================-->
      <div class="preloader" id="preloader">
        <div class="loader">
          <div class="line-scale-pulse-out-rapid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
      <!-- ============================================-->
      <!-- End of Preloader ===========================-->

	      
      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="text-center" id="simplemassmail">

        <div class="container">
          <div class="col-12 mb-3 mb-md-5 text-center">
              <h2 class="fs-3 fs-sm-4"><span class="text-underline"><img src="assets/img/simpleMailLogo.jpg"/> simple mass mail merge:</span></h2>
          </div>
          <div class="output">
                      <div class="row">
                        <div class="col-md-3">
                            <div class="card installation">
                                <div class="card-body">
                                    <h4 class="card-title">Installation<span><i class="fa fa-info-circle" id="btnInstallation" onclick="installation()" aria-hidden="true" data-toggle="tooltip" title="More Info"></i></span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card configuration">
                                <div class="card-body">
                                    <h4 class="card-title">Configuration<span><i class="fa fa-info-circle" id="btnConfiguration" onclick="configuration()" aria-hidden="true" data-toggle="tooltip" title="More Info"></i></span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card execution">
                                <div class="card-body">
                                    <h4 class="card-title">Execution<span><i class="fa fa-info-circle" id="btnExecution" onclick="execution()" aria-hidden="true" data-toggle="tooltip" title="More Info"></i></span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card additionalLinks">
                                <div class="card-body">
                                    <h4 class="card-title">Additional Links<span><i class="fa fa-info-circle" id="btnAdditionalLinks" onclick="additionalLinks()" aria-hidden="true" data-toggle="tooltip" title="More Info"></i></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="installationData">
                  <h4>Installation<span onclick="cross()" class="close" data-toggle="tooltip" title="Close">X</span></h4>
                  <ul>
                    <li>Install SM3 add-on from the Google Marketplace. The add-on is compatible with all browsers and only requires a Gmail account.</li>
                    <li>Open a Google sheet and click on Add-ons from Menu bar.</li>
                    <li>Search for “Simple Mass Mail Merge” and install the Add-on.</li>
                  </ul>
                </div>
                <div class="configurationData">
                  <h4>Configuration<span onclick="cross()" class="close" data-toggle="tooltip" title="Close">X</span></h4>
                  <ul>
                    <li>Go to your Gmail or Google Inbox account and create a new draft message. You can include one or more variable fields in the email message using the {{FIELDNAME}} notation and these will be replaced with the actual values from the Google sheet when the emails are sent. </li>
                    <li>Say you want to send an email to a group where the content of the message body is mostly similar except a few fields like salutation, first name and city that will be unique for each message. What you need to do is add columns in the sheet (Created in Google Sheets) for each of these variable fields. And in your Gmail Draft, you can refer to these variable fields as {{First Name}}, {{Company Name}} and so on.</li>
                  </ul>
                </div>
                <div class="executionData">
                  <h4>Execution<span onclick="cross()" class="close" data-toggle="tooltip" title="Close">X</span></h4>
                  <ul>
                    <li>Add the source data in a Google Sheet and follow the wizard to run SM3. One email would be sent for each row in the sheet.</li>
                  </ul>
                </div>
                <div class="additionalLinksData">
                  <h4>Additional Links<span onclick="cross()" class="close" data-toggle="tooltip" title="Close">X</span></h4>
                  <ul>
                    <li><a href="#">Documentation <i class="fa fa-exclamation-circle" aria-hidden="true" data-toggle="tooltip" title="Coming Soon"></i></a></li>
                    <li><a href="#">Request a feature <i class="fa fa-exclamation-circle" aria-hidden="true" data-toggle="tooltip" title="Coming Soon"></i></a></li>
                    <li><a href="./TOS.aspx">Terms of Service</a></li>
                  </ul>
                </div>

        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

 </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!--===============================================-->
    <!--    Footer-->
    <!--===============================================-->
    <footer class="footer bg-black text-600 py-4 text-sans-serif text-center overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-4 order-lg-2">
            <a class="indicator indicator-up js-scroll-trigger" href="#top"><span class="indicator-arrow indicator-arrow-one" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":0.9}'></span><span class="indicator-arrow indicator-arrow-two" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":1.05}'></span></a>
          </div>
          <div class="col-lg-4 text-lg-left mt-4 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              Copyright &copy; 2018 C Simplify IT</p>
          </div>
          <div class="col-lg-4 text-lg-right order-lg-2 mt-2 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              <a class="text-600" href="TOS.aspx">Terms of Service</a>
            </p>
          </div> 
        </div>
      </div>
    </footer>


    <!--===============================================-->
    <!--    Modal for language selection-->
    <!--===============================================-->
    <div class="remodal bg-black remodal-select-language" data-remodal-id="language">
      <div class="remodal-close" data-remodal-action="close"></div>
      <ul class="list-unstyled pl-0 my-0 py-4 text-sans-serif">
        <li>
          <a class="pt-1 d-block text-white font-weight-semi-bold" href="#">English</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="#">Français</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="page-rtl.html">عربى</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="#">Deutsche</a>
        </li>
      </ul>
    </div>


    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    
     <script src="assets/js/scrolling-nav.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/lib/loaders.css/loaders.css.js"></script>
    <script src="assets/js/stickyfill.min.js"></script>
    <script src="assets/lib/remodal/remodal.js"></script>
    <script src="assets/lib/jtap/jquery.tap.js"></script>
    <script src="assets/js/rellax.min.js"></script>
    <script src="assets/lib/owl.carousel/owl.carousel.js"></script>
    <script src="assets/lib/lightbox2/js/lightbox.js"></script>
    <script src="assets/lib/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/lib/isotope-packery/packery-mode.pkgd.min.js"></script>
    <script src="assets/js/theme.js"></script>   
    <script type="text/javascript" src="assets/js/simpleMassMailMerge.js"></script>
</body>
</html>


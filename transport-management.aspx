<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>
<asp:Content ID="hearlthHead" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Transport-Management| C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="Transportmaincontent" ContentPlaceHolderID="MainContent" runat="server">


    <div class="wrapper">

        <!--=== Breadcrumbs ===-->
        <div class="breadcrumbs">
            <div class="container">
                <h1 class="pull-left">Transport Management</h1>
                <ul class="pull-right breadcrumb">
                    <li><a href="index..aspx">Home</a></li>
                    <li><a href="">Verticals</a></li>
                    <li class="active">Transport Management</li>
                </ul>
            </div>
        </div>
        <!--/breadcrumbs-->
        <!--=== End Breadcrumbs ===-->

        <!--=== Content Part ===-->
        <div class="container content">
            <div class="row">
                <div class="col-md-6">
                    <h2>YOCarry Transport Management Product</h2>
                    <p>YOCarry Transport Management System with illiterate design, A TMS system designed to be used by people who cannot read/write. Be 10X better.</p>
                    <p>
                        TMS is a mobile enabled, web-based tool that helps customers gain real time visibility across their booking and delivery offices.
                    </p>
                    <br>

                    <p>
                        It helps to manage all the activities from booking, trans-shipment, delivery, POD, tracking, billing to customer and vendor and financial control over branches and can be integrated with ERP to provide a seamless solution.
                    </p>
                    <br>
                    <p>We address the unique needs of the mobile enterprise such as workflow sharing across operations, rapid development to adapt quickly to changing needs, secure and reliable access to enterprise information and automatic over-the-air application updates. With C Simplify IT team as the foundation for your strategic mobile architecture, you will have the most cost-effective and flexible solution for now and tomorrow to streamline your mobile business operations and capture new revenue opportunities. </p>
                    <br>

                    <button type="button" class="btn btn-primary" onclick="window.location.href='/transport-management-system.aspx'">View Our YOCarry Transport Management Product</button>

                </div>
                <div class="col-md-6">
                    <img class="wow fadeInLeft img-responsive pull-right md-pt-40" src="assets/img/main/YOCarryTMS .png" alt="">
                </div>
            </div>
        </div>
        <!--/container-->
        <!--=== End Content Part ===-->

    </div>
    <!--/wrapper-->


</asp:Content>

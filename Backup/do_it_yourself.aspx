﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="do_it_yourself.aspx.cs" Inherits="csimplifyit.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
   
    
    <link href="Scripts/accordian/accordion.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/accordian/modernizr.custom.29473.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="body-wrapper" class="clearfix"><!--start body-wrapper-->

 <div class="clear"></div>
 <hr />
 <h1 class="text_blue">Do it yourself !</h1>
 <p>This page is dedicated to those people who want to do things themselves, as they have time and skills to take themselves at higher level. It is our sincere effort to share the best and intelligent practices on bringing up your businesss online. Here we have collacted excellent links to get strated, setup and manage your efforts. By no means this is complete in every sense, but this can be your first step towards your dreams fulfillment. </p>
     <div class=one_third"> 
         <section class="ac-container">
				<div>
					<input id="ac-1" name="accordion-1" type="radio" checked />
					<label for="ac-1">Godaddy </label>
					<article class="ac-small">
                    <ul>
                       <li>  Buy Domain </li>  
                       <li> Host your website (Use Google Sites !)  </li> 
                       <li> Get Hosting space (Amazon)  </li> 
                   
                    
                    </ul>
						</article>
				</div>
				<div>
					<input id="ac-2" name="accordion-1" type="radio" />
					<label for="ac-2">Google</label>
					<article class="ac-medium">
						
                        <ul>

                          <li>    Sites</li> 

                          <li>  Adsense</li> 

                           <li>  Google Apps (free email for 50 users)</li> 

                           <li>   Google Analytics</li> 

                            <li>  Google Webmaster tools</li> 

                           <li>   Places</li> 

                            <li>  Merchant Services</li> 

                            <li>  Maps </li> 
                            </ul>
                        
                    </article>
				</div>
				<div>
					<input id="ac-3" name="accordion-1" type="radio" />
					<label for="ac-3">LinkedIn</label>
					<article class="ac-small">
                     <ul>

                          <li>    Tell your friends</li> 

                          <li>   Join Groups</li> 

                           <li>  Create Groups </li> 

                           <li>  LinkedIn Ads</li> 

                    
                            </ul>
                            </article>
				</div>
				<div>
					<input id="ac-4" name="accordion-1" type="radio" />
					<label for="ac-4">Facebook</label>
					<article class="ac-large">
						<ul>

                          <li> Tell your friends</li> 

                          <li>   Join Groups</li> 

                           <li>   Create Games/Apps  </li> 

                           <li>  Create Fan pages </li> 

                    
                            </ul></article>
				</div>
			</section>
            </div>  

            <a href="templates.aspx">Templates</a>
 </div>
</asp:Content>

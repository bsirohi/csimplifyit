﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace csimplifyit
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        HttpClient client = new HttpClient();
        protected void Page_Load(object sender, EventArgs e)
        {

            client.BaseAddress = new Uri("http://service.csimplifyit.com");
            client.DefaultRequestHeaders.Accept.Add(
               new MediaTypeWithQualityHeaderValue("application/json"));

        }

        protected void submit_Click(object sender, EventArgs e)
        {
            var contact = new Contact() { Name =name.Text, Email = email.Text, Message = query.Text, Date = DateTime.Now };
           
            HttpResponseMessage response = client.PostAsJsonAsync("api/contact", contact).Result;
            if (response.IsSuccessStatusCode)
            {
                Label1.Text = "Query Submitted ! Thank you";
                Label1.Visible = true;
               
                name.Text = query.Text = email.Text = null;


            }
            else
            {
                Label1.Text = "Sorry, Service is unavailable Please try later";
                Label1.Visible = true;
            }
           
        }
    }
}

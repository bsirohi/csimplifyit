<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>C Simplify IT - Do eCommerce!10x better.</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="gaurav developer">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@dmehta104">
    <meta name="twitter:creator" content="@dmehta104">
    <meta name="twitter:title" content="C Simplify IT Do eCommerce!10x better">
    <meta name="twitter:description" content="C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...">
    <meta name="twitter:image" content="http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png">
    
	<meta name="description" content="">
    <meta name="keywords" content="simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us ">
	

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>
    <!-- JS Global Compulsory -->
	<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>


<!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/2816000.js"></script>
<!-- End of HubSpot Embed Code -->

<!--Start of Zendesk Chat Script--> 
<!-- <script type="text/javascript"> -->
<!-- window.$zopim||(function(d,s){var z=$zopim=function(c){ -->
<!-- z._.push(c)},$=z.s= -->
<!-- d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set. -->
<!-- _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8'); -->
<!-- $.src='https://v2.zopim.com/?4YO0ZQwHPMQ5DHV7rEgN1soWwLvkTVKG';z.t=+new Date;$. -->
<!-- type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script'); -->
<!-- </script> -->
<!--End of Zendesk Chat Script-->


<!-- Start of Async Drift Code ChatBox -->
<script>
!function() {
  var t;
  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
  t.factory = function(e) {
    return function() {
      var n;
      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
    };
  }, t.methods.forEach(function(e) {
    t[e] = t.factory(e);
  }), t.load = function(t) {
    var e, n, o, i;
    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
  });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('4czvhtuy6ubu');
</script>
<!-- End of Async Drift Code -->


<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/blocks.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/headers/header-v6.css">
    <link rel="stylesheet" href="assets/css/footers/footer-v2.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/parallax-slider/css/parallax-slider.css">
	<link rel="stylesheet" href="assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="assets/plugins/layer-slider/layerslider/css/layerslider.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/css/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
    <!-- Style Switcher -->
	<link rel="stylesheet" href="assets/css/plugins/style-switcher.css">
     <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-63272985-1', 'auto');
        ga('send', 'pageview');
    </script>
  <script>
(function(doc, script) {
var pushBIZIN, 
__pushBIZ_In = doc.getElementsByTagName(script)[0], 
addPushBIZVisibility = function(url, id) { 
if (doc.getElementById(id)) {return;} 
pushBIZIN = doc.createElement(script); 
pushBIZIN.src = url;
 id && (pushBIZIN.id = id);
 __pushBIZ_In.parentNode.insertBefore(pushBIZIN, __pushBIZ_In); }; 
addPushBIZVisibility('https://cdn.pushbiz.in/cdn/509cb8fce49944f8b6aafed7ba83f7ba_main.js','pushBIZ.IN');
 }(document, 'script')); 
      
 </script>
</head>

<body class="header-fixed header-fixed-space">

	<div class="wrapper">
    
		<!--=== Header ===-->
		<div class="header header-sticky">
			<div class="container">
				<!-- Logo -->
				<a class="logo" href="Default.aspx">
					<img src="assets/img/logo1-default.png" alt="Logo">
				</a>
				<!-- End Logo -->
                <!-- Topbar -->
				<div class="topbar">
					<ul class="loginbar pull-right">
						<li class="hoverSelector">
							<i class="icon-custom  rounded-x   icon-call-in "></i>
							<a href="tel://+9198999 76227" style="font-size:14px;">+91 98999 76227</a>
					</ul>
				</div>
				<!-- End Topbar -->


				
				<!-- Toggle get grouped for better mobile display -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
                   
				</button>
				<!-- End Toggle -->
			</div><!--/end container-->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse ">
				<div class="container">
					<ul class="nav navbar-nav">
                        <!-- Verticals -->
						<li><a href="technology.aspx" class="">Technology</a></li>
						<!-- End Verticals -->
                        
                        <!-- Verticals -->
						<li class="dropdown page-scroll"><a href="#verticals" class="">Verticals</a>
				            <ul class="dropdown-menu">
										<li><a href="retail.aspx">Retail</a></li>
										<li><a href="transport-management.aspx">Transport Management</a></li>
                                        <li><a href="mediapublish.aspx">Media and Publishing</a></li>
										<li><a href="financial.aspx">Financial Services</a></li>
                                        <li><a href="healthcare.aspx">Health Care</a></li>
                                        
				            </ul>
						</li>
						<!-- End Verticals -->
                        
                        <!---Products ------>
                        <li ></li>
                        <li class="page-scroll dropdown">
							<a href="#products" class="">Products</a>
				            <ul class="dropdown-menu">
										<li><a href="pushbiz.aspx">PushBiz - Be Visible</a></li>
										<li><a href="sims.aspx">SIMS - Smart Incedent Management System</a></li>
										<li><a href="freshervilla.aspx">FresherVilla - Skill Builder</a></li>
								        <li><a  href="transport-management-system.aspx">TMS - Transport Management System</a></li></li>										
										<li><a href="tallent-nest.aspx">Talent Nest - Knowledge Management</a></li>
                                        <li><a href="smartpos.aspx">Smart POS - Geo/IOT Support</a></li>
                                        <li><a href="insynch.aspx">IN SYNCH - Intelligent follow ups</a></li>
                                        
				            </ul>
						</li>
                        <!---End Products ----->

						<!-- Servces -->
						<li class="dropdown page-scroll">
							<a href="#services" class="">Services</a>
				            <ul class="dropdown-menu">
                                        <li><a href="bots-machinelearning.aspx">Bots and Machine Learning Services</a></li><li><a href="bpm-services.aspx">BPM Services</a></li>  
<li><a href="recruitcircle.aspx">Recruitment Services</a></li>                                      
										<li><a href="e-commerce.aspx">E-Commerce Services</a></li>
										<li><a href="geo-based-iot-internet-of-thing-services.aspx">Geo Based/IOT Internet Of Thing Services</a></li> <li><a href="artificial-intelligence-services.aspx">Artificial Intelligence Services</a></li>
										<li><a href="cloud_integrations.aspx">Cloud Integration Management Services</a></li>
                                        <li ><a href="mobility_apps.aspx">Mobility Apps Services</a></li> 
									                                 
                                        <li><a href="testing.aspx">Testing Services</a></li>
				            </ul>
						</li>
						<!-- End Services -->
                        <!-- Pages -->
						  <li class=""><a href="aboutus.aspx" class="">About Us</a></li>
						<!-- End Pages -->
                        
                        <!----Contact Us ---->
                        <li class=""><a href="contact.aspx" class="">Contact Us</a></li>
						<!-- End Contact Us -->

					</ul>
                    
				</div><!--/end container-->
			</div><!--/navbar-collapse-->           
                  
		</div>
		<!--=== End Header ===-->
        
        	<!--=== Slider ===-->
		<div id="layerslider" style="width: 100%; height: 500px;">
            
            <div class="ls-slide" style="slidedirection: right; transition2d: 92,93,105; ">
				<img src="assets/img/sliders/layer/1.jpg" class="ls-bg" alt="Slide background">

				<span class="ls-s-1" style=" color: #fff; line-height:45px; font-weight: 200; font-size: 35px; top:150px; left: -40px; slidedirection : top; slideoutdirection : bottom; durationin : 1000; durationout : 1000; ">
					<i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">Use our expertise in  </i><br><i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">Bots and Machine Learning </i><br><i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">to reconfigure the man machine </i><br> <i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">relationships in your enterprise</i> <br><br>
				</span>
                

				<a class="btn-u btn-u-red ls-s-1" href="http://csimplifyit.com/bots-machinelearning.aspx" style=" padding: 9px 20px; font-size:25px; top:350px; left: -40px; slidedirection : bottom; slideoutdirection : bottom; durationin : 2000; durationout : 2000; ">
					Read More
				</a>

				<img src="assets/plugins/parallax-slider/img/slider-botsandMachine.jpg" alt="Slider Image" class="ls-s-1" style=" top:30px; left: 500px; slidedirection : right; slideoutdirection : bottom; durationin : 3000; durationout : 3000; ">
			</div>
            
			<!-- First slide -->
			<div class="ls-slide" style="slidedirection: right; transition2d: 92,93,105; ">
				<img src="assets/img/sliders/layer/1.jpg" class="ls-bg" alt="Slide background">

				<span class="ls-s-1" style=" color: #fff; line-height:45px; font-weight: 200; font-size: 35px; top:200px; left: 10px; slidedirection : top; slideoutdirection : bottom; durationin : 1000; durationout : 1000; ">
					<i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">PushBiz</i><br><i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">Makes your Business &#39;Visible&#39;</i><br> <i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">to your clients</i> 
				</span>
                

				<a class="btn-u btn-u-red ls-s-1" href="https://www.pushbiz.in/" style=" padding: 9px 20px; font-size:25px; top:350px; left: 10px; slidedirection : bottom; slideoutdirection : bottom; durationin : 2000; durationout : 2000; ">
					Signup Now
				</a>

				<img src="assets/plugins/parallax-slider/img/pushbiz.png" alt="Slider Image" class="ls-s-1" style=" top:30px; left: 400px; slidedirection : right; slideoutdirection : bottom; durationin : 3000; durationout : 3000; ">
			</div>
			<!-- End First slide -->

			<!-- Second Slide -->
			<div class="ls-slide" data-ls="slidedelay:4500; transition2d:25;">
				<img src="assets/img/sliders/layer/2.jpg" class="ls-bg" alt="Slide background"/>

				<img class="ls-l" src="assets/plugins/parallax-slider/img/1.png" style="top: 55%; left: 30%;"
				data-ls="offsetxin:left; durationin:1500; delayin:900; fadein:false; offsetxout:left; durationout:1000; fadeout:false;" />

				<span class="ls-s-1" style=" line-height: 45px; font-size:35px; color:#fff; top:150px; left: 590px; slidedirection : top; slideoutdirection : bottom; durationin : 3500; durationout : 3500; delayin : 1000;">
					<i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">Premier IT Company </i><br><i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">For eCommerce, BPM, </i><br> <i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">Mobility and Analytics</i> 
				</span>

				<a class="btn-u btn-u-orange ls-s-1" href="aboutus.aspx" style=" padding: 9px 20px; font-size:25px; top:340px; left: 590px; slidedirection : bottom; slideoutdirection : top; durationin : 3500; durationout : 2500; delayin : 1000; ">
					Know More
				</a>
			</div>
			<!-- End Second Slide -->
            <!-- Third slide -->	
			<div class="ls-slide" style="slidedirection: right; transition2d:105; ">
				<img src="assets/img/sliders/layer/1.jpg" class="ls-bg" alt="Slide background">

				<span class="ls-s-1" style=" color: #fff; line-height:45px; font-weight: 200; font-size: 35px; top:200px; left: 10px; slidedirection : top; slideoutdirection : bottom; durationin : 1000; durationout : 1000; ">
					<i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">In SYNCH "Integrates"</i><br><i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">Your Ecommerce</i><br> <i style="background-color: #ef6262;opacity: 0.8;padding:0px 20px;">Business, Suppliers..</i> 
				</span>
                

				<a class="btn-u btn-u-red ls-s-1" href="insynch.aspx" style=" padding: 9px 20px; font-size:25px; top:350px; left: 10px; slidedirection : bottom; slideoutdirection : bottom; durationin : 2000; durationout : 2000; ">
					Visit Project
				</a>

				<img src="assets/img/sliders/insynch-video.png" alt="Slider Image" class="ls-s-1" style=" top:100px; left: 550px; slidedirection : right; slideoutdirection : bottom; durationin : 3000; durationout : 3000; ">
			</div>
			<!-- End Third slide -->

			<!-- Fourth Slide -->
			<div class="ls-slide" data-ls="transition2d:93;">
				<img src="assets/img/sliders/layer/3.jpg" class="ls-bg" alt="Slide background">

				<i class="fa fa-chevron-circle-right ls-s-1" style=" color: #fff; font-size: 24px; top:70px; left: 40px; slidedirection : left; slideoutdirection : top; durationin : 1500; durationout : 500; "></i>

				<span class="ls-s-2" style=" color: #fff; font-weight: 200; font-size: 22px; top:70px; left: 70px; slidedirection : top; slideoutdirection : bottom; durationin : 1500; durationout : 500; ">
                    <i style="background-color: #ef6262;opacity: 0.8;padding:5px 20px;">Business Intelligence</i>
				</span>

				<i class="fa fa-chevron-circle-right ls-s-1" style=" color: #fff; font-size: 24px; top:120px; left: 40px; slidedirection : left; slideoutdirection : top; durationin : 2500; durationout : 1500; "></i>

				<span class="ls-s-2" style=" color: #fff; font-weight: 200; font-size: 22px; top:120px; left: 70px; slidedirection : top; slideoutdirection : bottom; durationin : 2500; durationout : 1500; ">
                    <i style="background-color: #ef6262;opacity: 0.8;padding:5px 20px;">Content Management</i>
					
				</span>

				<i class="fa fa-chevron-circle-right ls-s-1" style=" color: #fff; font-size: 24px; top:170px; left: 40px; slidedirection : left; slideoutdirection : top; durationin : 3500; durationout : 3500; "></i>

				<span class="ls-s-2" style=" color: #fff; font-weight: 200; font-size: 22px; top:170px; left: 70px; slidedirection : top; slideoutdirection : bottom; durationin : 3500; durationout : 2500; ">
                    <i style="background-color: #ef6262;opacity: 0.8;padding:5px 20px;">Microsoft & Java technologies</i>
					
				</span>

				<i class="fa fa-chevron-circle-right ls-s-1" style=" color: #fff; font-size: 24px; top:220px; left: 40px; slidedirection : left; slideoutdirection : top; durationin : 4500; durationout : 3500; "></i>

				<span class="ls-s-2" style=" color: #fff; font-weight: 200; font-size: 22px; top:220px; left: 70px; slidedirection : top; slideoutdirection : bottom; durationin : 4500; durationout : 3500; ">
                     <i style="background-color: #ef6262;opacity: 0.8;padding:5px 20px;">Application Development</i>
					
				</span>

				<i class="fa fa-chevron-circle-right ls-s-1" style=" color: #fff; font-size: 24px; top:270px; left: 40px; slidedirection : left; slideoutdirection : top; durationin : 5500; durationout : 4500; "></i>

				<span class="ls-s-2" style=" color: #fff; font-weight: 200; font-size: 22px; top:270px; left: 70px; slidedirection : top; slideoutdirection : bottom; durationin : 5500; durationout : 4500; ">
                     <i style="background-color: #ef6262;opacity: 0.8;padding:5px 20px;">Support and Testing</i>
					
				</span>

				<a class="btn-u btn-u-blue ls-s1" href="contact.aspx" style=" padding: 9px 20px; font-size:25px; top:340px; left: 40px; slidedirection : bottom; slideoutdirection : bottom; durationin : 6500; durationout : 3500; ">
					Ask for demo
				</a>

				<img src="assets/img/sliders/sam.png" alt="Slider Image" class="ls-s-1" style=" top:50px; left: 550px; slidedirection : right; slideoutdirection : bottom; durationin : 1500; durationout : 1500; ">
			</div>
			<!-- End  Fourth Slide -->
		</div><!--/layer_slider-->
		<!--=== End Slider ===-->

		
		

		<!--=== Content Part ===-->
		<div class="container content-sm">
            <!-- Info Blokcs -->
			<div class="row margin-bottom-30">
				<!-- Welcome Block -->
				<div class="col-md-8 md-margin-bottom-40">
					<div class="headline"><h2>Welcome To C Simplify IT</h2></div>
					<div class="row">
						<div class="col-sm-4">
							<div class="embed-responsive embed-responsive-16by9">                              
                                <iframe class="embed-responsive-item" width="420" height="315" 
                                        src="https://www.youtube.com/embed/UTS8Y9VEpeY" frameborder="0" allowfullscreen></iframe>
                            </div>
						</div>
						<div class="col-sm-8 justify" >
							<p>C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care. We have developed six different products PushBiz - Be Visible, SIMS (Smart Incident management System), Freshers Villa - Skill Builder, Tallent Nest - Knowledge Management, Smart POS - Geo/IOT Support and IN SYNCH - Intelligent follow ups.Our in-house solution products are SIMS (Smart Incident management System), IN SYNC (Follow up framework).</p>
							
						</div>
					</div>

				</div><!--/col-md-8-->

				<!-- Latest Shots -->
				<div class="col-md-4">
					<div class="headline"><h2>Our Clients</h2></div>
					<div id="myCarousel" class="wow bounceInRight carousel slide carousel-v1">
						<div class="carousel-inner ">
                           
                                    <div class="item active block">
                                        <a href="https://www.pushbiz.in/" target="_blank">
                                            <img src="assets/img/main/img4.jpg" alt="">
                                            <div class="carousel-caption block-caption text-center">
                                                <p class="justify text-center">C Simplify IT Product</p>
                                                <p class="justify text-center">Be - visible - Earn 10x More.</p><br>
                                            </div>
                                        </a>
                                    </div>
							<div class="item block">
                                <a href="https://myaccount.vinculumgroup.com/" target="_blank">
                                    <img src="assets/img/main/express.png" alt="">
                                    <div class="carousel-caption block-caption">
                                       <p class="justify text-center">Eretail Express</p>
                                       <p class="justify text-center">Sell on ecommerce marketplaces globally.</p><br>
                                    </div>
                                </a>
							</div>
							<div class="item block">
                                <a href="#" target="_blank">
                                    <img src="assets/img/main/tms1.png" alt="">
                                    <div class="carousel-caption block-caption">
                                       <p class="justify text-center">Transport Management System</p>
                                       <p class="justify text-center">Transport Management System - Web based tool.</p><br>
                                    </div>
                                </a>
							</div>
							<div class="item block">
                                <a href="http://www.vskills.in/practice/vSkillsHome" target="_blank">
                                    <img src="assets/img/main/img2.jpg" alt="">
                                    <div class="carousel-caption block-caption">
                                       <p class="justify text-center">Vskills</p>
                                       <p class="justify text-center">Thousnads of pratice tests.</p><br>
                                    </div>
                                </a>
							</div>                             
							<div class="item block">
                                <a href="http://ec2-52-27-39-197.us-west-2.compute.amazonaws.com/" target="_blank">
								<img src="assets/img/main/gyg.jpg" alt="">
								<div class="carousel-caption block-caption">
									<p class="justify text-center">GYG</p>
                                    <p class="justify text-center">Responsive UI Point of sale</p>
                                    <p class="justify text-center">Singapore/Malaysia</p><br>
								</div>
                                 </a>
							</div>
                           
                            <div class="item block">
								<img class="project-img" src="assets/img/main/bi.jpg" alt="">
								<div class="carousel-caption block-caption">
									<p class="justify">Priority Vendor</p>
                                    <p class="justify">Analytics for Invoice Discounting</p>
								</div>
							</div>
                            <div class="item block">
                                <a href="smartpos.aspx">
								<img class="project-img" src="assets/img/main/pos.jpg" alt="">
								<div class="carousel-caption block-caption">
									<p class="justify">Vestige</p>
                                    <p class="justify">Point of Sale running at 3000+ locations</p>
								</div>
                                </a>
							</div>
                            <div class="item block">
								<img class="project-img" src="assets/img/main/lanepro.png" alt="">
								<div class="carousel-caption block-caption">
									<p class="justify">Motherson Sumi</p>
                                    <p class="justify">Mobility App For Servicing</p>
								</div>
							</div>
                            <div class="item block">
								<img class="project-img" src="assets/img/main/picknpacks.png" alt="">
								<div class="carousel-caption block-caption">
									<p class="justify">Ur Doorstep</p>
                                    <p class="justify">Mobility App For Shipping Orders</p>
								</div>
							</div>
                            <div class="item block">
								<img class="project-img" src="assets/img/main/shipping.jpg" alt="">
								<div class="carousel-caption block-caption">
									<p class="justify">Ur Doorstep</p>
                                    <p class="justify">Mobility App For Shipping Orders</p>
								</div>
							</div>
                            <div class="item block">
								<img class="project-img" src="assets/img/main/eretail.png" alt="">
								<div class="carousel-caption block-caption">
									<p class="justify">eRetail</p>
                                    <p class="justify">Mobility App For Inventory Management System</p>
								</div>
							</div>                           
						</div>

						<div class="carousel-arrow">
							<a class="left carousel-control" href="#myCarousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a class="right carousel-control" href="#myCarousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div><!--/col-md-4-->
				<div class="col-md-12">
					
					<blockquote class="hero-C Simplify IT">
						<p class="justify">We help its Customers increase their customer engagement, brand recognition, leads and sales. We have learned how to interpret our customers&#39; needs and wants. We are passionate about exploiting technology and deploying intelligent business practices to meet the challenges faced by today&#39;s services industry. We build IT systems which are Simple, Intelligent and which gel seamlessly with existing applications and IT landscape.</p>
						<p>C Simplify IT AI (Artificial Intelligence) and Machine Learning services can understand, learn, predict, adapt and potentially operate autonomously without human intervetions. C Simplify IT eCommerce and financial services bots help its clients answers the queries of its unique customer in unique ways using AI and machine learning. C Simplify IT AI and machine-learning techniques model current real-time transactions, as well as predictive models for next likely steps of each customer. C Simplify IT PushBIZ product provides AI based customer engagement platform.</p>
						<p>Using AI, technology C Simplify IT will focus on three areas &#45; advanced analytics, AI-powered and increasingly autonomous business processes and AI-powered immersive, conversational and continuous interfaces using virtual assistants. C Simplify IT SIMS (Smart Incident Management System) capture incidents with no click required mechanism and runs advanced analytics and AI powered business processes to deliver straight through processing in complex scenarios.</p>
						<p>C Simplify IT also creating services around &#34;Blockchain&#34; which is a type of distributed ledger in which value exchange transactions (in bitcoin or other token) are sequentially grouped into blocks. C Simplify IT solutions using blockchain and distributed-ledger concepts are gaining traction because they hold the promise of transforming industry operating models in industries such as music distribution, identify verification and title registry.  Using Blockchain model, adds trust to untrusted environments and reduce business friction by providing transparent access to the information in the chain. C Simplify IT systems captures data from multiple inputs, processes the data using R models and allow users to manage complex portfolio alert system for bonds and equities . </p>
						<p>C Simplify IT mesh services refers to the dynamic connection of people, processes, things and services supporting intelligent digital ecosystems. As these mesh services evolves, the user experience fundamentally changes and the supporting technology and security architectures and platforms must change as well. C Simplify IT  mesh app and service architecture (MASA) is a multichannel solution architecture that leverages cloud and serverless computing, containers and microservices as well as APIs and events to deliver modular, flexible and dynamic solutions. Solutions ultimately support multiple users in multiple roles using multiple devices and communicating over multiple networks. C Simplify IT POS(Point of sale), Inventory Management system and TMS (Transport Management System) create 10X better meshed services around eCommerce solutions. </p>
						<p>C Simplify IT conversational systems shift from a model where people adapt to computers to one where the computer &#34;hears&#34; and adapts to a person&#39;s desired outcome. C Simplify IT expertise on IBM watson API, SiriKit, Wit.AI and Google API provides its clients a unique solutioning advantages. </p>
						<p>C Simplify IT adaptive security services works with clients application, solution and enterprise architects to consider security early in the design of applications or IoT solutions. Multilayered security and use of user and entity behaviour analytics is a requirement for every enterprise. C Simplify IT solutions implement data in motion and data at rest security best practices.</p>
						<p>C Simplify IT provides 10X Premier IT services around mobility, bpm and business analytics using AI, mesh services , conversational systems and adaptive security implementations.</p>
						
						
						<small>Founder, Deepak Mehta.</small>
					</blockquote>
				
				
				</div>
			</div>
			<!-- End Info Blokcs -->
        </div><!--container End-->
        
         <!-- Icon Boxes 39 -->
        <section id="products" class="bg-grey">
            <div class="container content-md our-product">
               
                    <div class="title-v1">
                    <h2>Our Products</h2>
                    <p>C Simplify IT products are excellent and the roadmap for future improvements is genuinely exciting.</p>
                </div>
                    
                  <!-- Icon Boxes 41 -->
		<div class="container  our-solution">
			<div class="row margin-bottom-40">
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-sea service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light icon-layers rounded-x "></i>
						<h2 class="heading-md">PushBiz - Be Visible</h2>
						<p>Earn 10x More</p>
                        <a href="pushbiz.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-red service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light icon-credit-card icon-line "></i>
						<h2 class="heading-md">SIMS</h2>
						<p> cutting the supports cost </p>
                        <a href="sims.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-blue service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light rounded-x icon-line icon-bar-chart"></i>
						<h2 class="heading-md">Fresher Villa</h2>
						<p>Skill Builder</p>
                        <a href="freshervilla.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
			</div>

			

			<div class="row ">
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-grey">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light icon-diamond"></i>
						<h2 class="heading-md">Talent Nest</h2>
						<p>Knowledge Management</p>
                        <a href="tallent-nest.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-yellow">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-rocket"></i>
						<h2 class="heading-md">Smart POS(Point of Sale)</h2>
						<p>Geo/IOT Support</p>
                        <a href="smartpos.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-dark-blue">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-handbag"></i>
						<h2 class="heading-md">IN SYNCH</h2>
						<p>Intelligent follow ups</p>
                        <a href="insynch.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
			</div>

			
		</div>
		<!-- End Icon Boxes 41 -->

            </div>
        </section>
		<!-- End Icon Boxes 39 -->
			<!-- Verticals Blocks -->
        <section id="verticals">
			<div class=" wow bounceInUp row  " style="margin:0px;">
                <div class="title-v1 md-pt-20 md-mb-30">
				<h2>Focused Verticals</h2>
				<p style="text-align:center;">We are <strong>passionate</strong> about exploiting technology and deploying intelligent business practices<br> to meet the challenges faced by <strong>today&#39;s</strong> services industry.</p>
			</div>
                 <!-- Icon Boxes 41 -->
		<div class="container  our-verticals">
			<div class="row margin-bottom-40">
				<div class="col-md-6 col-sm-6">
					<div class="service-block service-block-sea service-or">
						
						<i class="icon-custom icon-color-light  icon-handbag  rounded-x "></i>
						<h2 class="heading-md">Retail</h2>
                        <p class="justify">Our Retail practice offers a comprehen-sive suite of Front End/Back End IT Services spanning consulting,enterprise services , implementations, and functionality additions. Our robust retail process knowledge, specialized tools, and consulting expertise helps clients to enhance retail processes efficiency and fulfillment precision, reduce costs and decrease cycle times from order to fulfillment.</p><a href="retail.aspx" class="pull-right"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                        
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="service-block service-block-red service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light  icon-plane  icon-line "></i>
						<h2 class="heading-md">Transport Management</h2>
						<p class="justify"> TMS is a mobile enabled, web-based tool that helps customers gain real time visibility across their booking and delivery offices.It helps to manage all the activities from booking, trans-shipment, delivery, POD, tracking, billing to customer and vendor and financial control over branches and can be integrated with ERP to provide a seamless solution. </p><a href="transport-management.aspx" class="pull-right"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
				
			</div>

			

			<div class="row ">
				<div class="col-md-6 col-sm-6">
					<div class="service-block service-block-blue">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-credit-card "></i>
						<h2 class="heading-md">Financial</h2>
						<p class="justify">C Simplify IT is leading the transformation of financial services. We have been helping clients meet growth objectives while coping with market realities. We deliver an array of innovative financial services technology solutions your clients want now and anticipate what you will need in the future.Our industry-leading solutions can help change the way you serve your customers providing new products and services...</p><a href="financial.aspx" class="pull-right"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="service-block service-block-dark-blue">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light   icon-briefcase"></i>
						<h2 class="heading-md">Health Care</h2>
						<p class="justify">C Simplify IT involving in the design, development, creation, use and maintenance of information systems for the healthcare industry. Automated and interoperable healthcare information systems are expected to lower costs, improve efficiency and reduce error.We are also providing better consumer care and services.</p><a href="healthcare.aspx" class="pull-right md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
				
			</div>
            <div class="row ">
                <div class="col-md-3 col-sm-2"></div>
				<div class="col-md-6 col-sm-8">
					<div class="service-block service-block-blue service-mediablock">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light   icon-volume-1  "></i>
						<h2 class="heading-md">Media and Publishing</h2>
						<p class="justify">Process Re-engineering using BPM Publishing processes from production and editorial, to content management, rights management and ERP integration. Assets management for dynamic content and digital rights management. Digital Media content lifecycle and distribution Social CRM manage information with the ever-growing social media channel by implementing new approaches to data analytics and CRM Use Tools to manage Media and Publishing Marklogic, Adobe, Bonitasoft BPM, Push BIZ IN...</p><a href="mediapublish.aspx" class="pull-right"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
                <div class="col-md-3 col-sm-2"></div>
				
				
			</div>

			
		</div>
            </div>
        </section>
        
        <!--=== Content Part ===-->
		<div class="container content-sm">
<!-- Services Section -->
	<section id="services">
		<div class="wow bounceInRight container">
        <div class="title-v1">
				<h2>What WE DO</h2>
				<p style="text-align:center;">We are <strong>passionate</strong> about exploiting technology and deploying intelligent business practices<br> to meet the challenges faced by <strong>today&#39;s</strong> services industry.</p>
			</div>
             <!-- Icon Boxes 41 -->
		<div class="container  our-services">
			<div class="row margin-bottom-40">
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-sea service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light icon-handbag rounded-x "></i>
						<h2 class="heading-md">E-Commerce Services</h2>
						<p class="justify">Our Magento practice offers a comprehensive suite of eCommerce related services spanning consulting, enterprise services and functionality additions. Our robust retail process knowledge, specialized tools, and consulting expertise helps us to enhance eCommerce efficiency and fulfillment precision, reduce costs and decrease cycle times from order to fulfillment..</p>
                        <a href="retail.aspx" class=""><button type="button" class="btn-u btn-u-aqua md-mt-20">Read More</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-red service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light icon-directions icon-line "></i>
						<h2 class="heading-md">Geo Based/IOT Internet of things</h2>
						<p class="justify">Simplify IT context-enriched services use information about a person or object to proactively anticipate the user&#39;s need and serve up the content, product or service most appropriate to the user. The IT industry is beginning to recognize a pattern where augmented reality offerings, mobile location-aware ads, mobile search and social mobile sites fall under the umbrella term...</p>
                        <a href="retail.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-blue service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light rounded-x icon-line icon-cloud-download"></i>
						<h2 class="heading-md">Cloud Integrations Management</h2>
						<p class="justify">At C SIMPLIFY IT, we have been helping enterprises drive business transformation by harnessing the power of technology. Leveraging technology expertise & decades of experience in managing multiple customer IT environments,C SIMPLIFY IT team has put together the right people, tools and processes to deliver end-to-end cloud IT services... </p>
                        <a href="cloud_integrations.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
					</div>
				</div>
			</div>

			

			<div class="row ">
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-grey">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light icon-screen-smartphone"></i>
						<h2 class="heading-md">Mobility Apps</h2>
						<p class="justify">Apple&#39;s iPhone/iPad makes rich content, information and entertainment usable in locations both at home and on the road much more simply than before. It also makes makes limited document editing and data entry on a handheld device feasible rather than challenging. C Simplify IT is neck-deep in iPhone apps development and offers end-to-end iPhone application solutions at... </p>
                        <a href="mobility.aspx" class=" "><button type="button" class="btn-u btn-u-aqua md-mt-20">Read More</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-purple">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-social-youtube"></i>
						<h2 class="heading-md">Bots and Machine learning</h2>
						<p class="justify">These days, in technology and digital marketing circles, &#34;bots&#34; are a frequent source of discussion. By definition, a bot (short for &#34; web robot &#34;) is a software program that operates as an agent for an individual, group of individuals, organization or even legitimate businesses.A Bot starts off with no knowledge of how to communicate. Each time a user enters a statement as input, response is computed using Machine learning... </p>
                        <a href="botsandmachinelearning.aspx" class=" "><button type="button" class="btn-u btn-u-aqua ">Read More</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-dark-blue">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light   icon-wrench "></i>
						<h2 class="heading-md">Testing</h2>
						<p class="justify">Mobile application testing is becoming more complex, critical and expensive. Organizations won&#39;t find a tool to meet all their mobile testing needs, but can assemble a useful portfolio of tools to address many mobile testing challenges.C Simplify IT provides Maintenance FREE Apps, to cut the pains of its customers and ensure that this channel should help customers improve..</p>
                        <a href="testing-verticals.aspx" class=""><button type="button" class="btn-u btn-u-aqua md-mt-20">Read More</button></a>
					</div>
				</div>
			</div>

			
		</div>
		<!-- End Icon Boxes 41 -->
		</div>
	</section>
            </div><!--/container-->
        
        <!-- Colorful Solution Blocks -->
      	<section class=" wow bounceInLeft container-fluid bg-grey" id="solution">
		<div class="row no-gutter equal-height-columns margin-bottom-50">
        <div class="container md-mt-40">
        <div class="title-v1">
				<h2>Our Solutions</h2>
				<p>C Simplify IT has executed numerous webbased and workflow onsite and offshore projects successfully and has created an extensive pool of resources that are trained to use the right processes, standards, and tools to deliver high levels of service.</p>
			</div>
			
            <!-- Icon Boxes 41 -->
		<div class="container  our-solution">
			<div class="row margin-bottom-40">
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-sea service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light icon-layers rounded-x "></i>
						<h2 class="heading-md">SIMS</h2>
						<p>Cuts your IT support by 50%. Reports and Records incidence of failure on any web based system with single click</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-red service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light icon-credit-card icon-line "></i>
						<h2 class="heading-md">Payment Gateway Integration</h2>
						<p>Improve your cash flow by enabling payments through Apple Pay, Google Playstore and Payment Gateways integration</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-blue service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light rounded-x icon-line icon-bar-chart"></i>
						<h2 class="heading-md">Skill Builder</h2>
						<p>Builds skills of your workforce to deliver quality.It trains,assess and compares their performance</p>
					</div>
				</div>
			</div>

			

			<div class="row ">
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-grey">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light icon-diamond"></i>
						<h2 class="heading-md">Constant Connect</h2>
						<p>Communicate with your clients on missed calls and on marketing events, based on</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-yellow">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-rocket"></i>
						<h2 class="heading-md">Smart Data Input Services</h2>
						<p>Use OCR/ICR based scanning solution process your paper based orders on fly.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-dark-blue">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-handbag"></i>
						<h2 class="heading-md">Magento/Shopify Integration</h2>
						<p>Integrate orders on "internet of things" channel with your</p>
					</div>
				</div>
			</div>

			
		</div>
		<!-- End Icon Boxes 41 -->

                </div>
		</div>        
	</section>
	<!-- End Colorful Service Blocks -->
        
        
        
      

            <!-- Parallax Counter -->
		<div class="parallax-counter-v4 parallaxBg1" id="facts">
			<div class="container content-sm">
				<div class="row">
					<div class="col-md-3 col-xs-6 md-margin-bottom-50">
						<i class="icon-cup"></i>
						<span class="counter">20</span>
						<h4>Coffee&#39;s Drunk</h4>
					</div>
					<div class="col-md-3 col-xs-6 md-margin-bottom-50">
						<i class="icon-clock"></i>
						<span class="counter">21</span>
						<h4>Projects</h4>
					</div>
					<div class="col-md-3 col-xs-6">
						<i class="icon-emoticon-smile"></i>
						<span class="counter">21</span>
						<h4>Happy Clients</h4>
					</div>
					<div class="col-md-3 col-xs-6">
						<i class=" icon-users"></i>
						<span class="counter">78</span>
						<h4>Team Members</h4>
					</div>
				</div><!--/end row-->
			</div><!--/end container-->
		</div>
		<!-- End Parallax Counter -->
         <!--Our Partners-->
    
    
			<div class="container content-sm" id="ourpartners">
            	<div class="title-v1">
					<h2>Our Partners</h2>
				</div>
				<div class="row">
					<div class=" wow rotateInDownLeft col-md-6 md-margin-bottom-40">
						<div class="funny-boxes funny-boxes-colored funny-boxes-red">
							<div class="row">
								<div class="col-md-4 funny-boxes-img">
									<img alt="" src="assets/img/patterns/vheader.png" class="img-responsive partner-logo-img">
									<ul class="list-unstyled">
										<li><i class="fa-fw fa fa-briefcase"></i> Noida </li>
										<li><i class="fa-fw fa fa-map-marker"></i> Uttar Pradesh, India</li>
									</ul>
								</div>
								<div class="col-md-8">
									<h2><a href="#">Vinculum</a></h2>
									<ul class="list-unstyled funny-boxes-rating">
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
									</ul>
									<p class="justify">Vinculum partnered with C Simplify IT for Implementation & Roll out of Retail Products, Standardization of Store and Merchandising Operations. C Simplify IT helped to implement and rollout the Retail products in record time.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="wow rotateInDownRight col-md-6">
						<div class="funny-boxes funny-boxes-colored funny-boxes-blue">
							<div class="row">
								<div class="col-md-4 funny-boxes-img">
									<img alt="" src="assets/img/patterns/webexpress.jpg" class="img-responsive partner-logo-img">
									<ul class="list-unstyled">
										<li><i class="fa-fw fa fa-briefcase"></i> Chamarajpet</li>
										<li><i class="fa-fw fa fa-map-marker"></i> Bangalore, India </li>
									</ul>
								</div>
								<div class="col-md-8">
									<h2><a href="#">Webxpress</a></h2>
									<ul class="list-unstyled funny-boxes-rating">
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
										<li><i class="fa fa-star"></i></li>
									</ul>
									<p class="justify">Webxpress partnered with C Simplify IT for mobile enabled and web-based tool that help the customers gain real time visibility across their booking and delivery offices through Transportation Products.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
    
    
    <!--End Our Partners-->
		
            
	<!-- End Services Section -->
			<!-- Owl Clients v1 -->
            
                <div class="col-md-12 clients-section parallaxBg">
                    <div class="container">
                        <div class="title-v1">
                            <h2>Our Clients</h2>
                        </div>
                        <ul class="owl-clients-v2">
                            <li class="item"><a href="http://www.myvestige.com/" target="_blank"><img src="assets/img/clients/vestige-logo.png" alt="" target="_blank"></a></li>
                            <li class="item"><a href="https://paytm.com/" target="_blank"><img src="assets/img/clients/paytm.png" alt=""></a></li>
                            <li class="item"><a href="http://www.apllvascor.com/" target="_blank"><img src="assets/img/clients/apl-logo.png" alt=""></a></li>
                             <li class="item"><a href="https://bondevalue.com/" target="_blank"><img src="assets/img/clients/bondevalue.png" alt=""></a></li>
							<li class="item"><a href="http://transorg.com/" target="_blank"><img src="assets/img/clients/transorg.png" alt=""></a></li>
                            <li class="item"><a href="http://www.askmebazaar.com/"><img src="assets/img/clients/askmebazaar.png" alt="" target="_blank"></a></li>
                            <li class="item"><a href="https://www.vinculumgroup.com/"><img src="assets/img/clients/vinculum.png" alt="" target="_blank"></a></li>
                            <li class="item"><a href="http://www.jardines.com/" target="_blank"><img src="assets/img/clients/jardine.jpeg" alt=""></a></li>
                            <li class="item"><a href="http://www.aswatson.com/" target="_blank"><img src="assets/img/clients/aswatson.png" alt=""></a></li>
                             <li class="item"><a href="https://www.priorityvendor.com/priorityvendor/handleGeneral.do"><img src="assets/img/clients/priority-vendor.png" alt="" target="_blank"></a></li>
                            <li class="item"><a href="http://www.urdoorstep.com/" target="_blank"><img src="assets/img/clients/ur-doorstep.png" alt="" target="_blank"></a></li>
                            <li class="item"><a href="http://www.vskills.in/practice/vSkillsHome"><img src="assets/img/clients/vskills.jpg" alt="" target="_blank"></a></li>                            
                             <li class="item"><a href="http://www.dhl.co.in/en.aspx" target="_blank"><img src="assets/img/clients/dhl.png" alt=""></a></li>
                            <li class="item"><a href="http://coldex.in/" target="_blank"><img src="assets/img/clients/coldex.png" alt="" target="_blank"></a></li>
                            <li class="item"><a href="http://www.motherson.com/motherson-sumi-systems-limited.aspx"><img src="assets/img/clients/motherson.jpg" alt="" target="_blank"></a></li>
                            
                        </ul>
                    </div>
                  </div>
            

			<!-- End Owl Clients v1 -->
            
        
		<!-- End Content Part -->
        <!--=== Footer v2 ===-->
		<div id="footer-v2" class="footer-v2">
			<div class="footer">
				<div class="container">
					<div class="row">
						<!-- About -->
						<div class="col-md-3 md-mt-40">
							<a href="index.aspx"><img id="logo-footer" class="footer-logo" src="assets/img/logo-original.png" alt=""></a>
							<p class="margin-bottom-20">C Simplify IT Services established in the year 2011 located in Gurgaon, India. we are handling projects in India, Singapore, Hong Kong, Thailand and USA.We have expertise in enterprise BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.</p>

							</div>
						<!-- End About -->

						<!-- Link List -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="headline"><h2 class="heading-sm">Useful Links</h2></div>
							<ul class="list-unstyled link-list">
								<li><a href="aboutus.aspx">About us</a><i class="fa fa-angle-right"></i></li>
								<li><a href="e-commerce.aspx">Services</a><i class="fa fa-angle-right"></i></li>
								<li><a href="retail.aspx">Verticals</a><i class="fa fa-angle-right"></i></li>
								<li><a href="contact.aspx">Contact us</a><i class="fa fa-angle-right"></i></li>
                                <li><a href="sitemap.xml">Sitemap</a><i class="fa fa-angle-right"></i></li>
							</ul>
						</div>
						<!-- End Link List -->

						<!-- Latest Tweets -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="latest-tweets">
								<div class="headline"><h2 class="heading-sm">Social Links</h2></div>
								<div class="latest-tweets-inner">
									<ul class="social-icons">
                                        <li>
                                            <a href="https://www.facebook.com/CSimplifyIT-210115279023481/"
                                               data-original-title="Facebook" class="rounded-x social_facebook" target="_blank"></a></li>
                                        <li><a href="https://twitter.com/dmehta104" data-original-title="Twitter" class="rounded-x social_twitter" target="_blank"></a></li>
                                        <li><a href="https://plus.google.com/u/0/+CSimplifyITServicesPrivateLimitedNewDelhi/about" data-original-title="Goole Plus" class="rounded-x social_googleplus" target="_blank"></a></li>
                                        <li><a href="https://www.linkedin.com/company/2702366" data-original-title="Linkedin" class="rounded-x social_linkedin" target="_blank"></a></li>
                                    </ul>
                                </div>
							</div>
						</div>
						<!-- End Latest Tweets -->

						<!-- Address -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="headline"><h2 class="heading-sm">Contact Us</h2></div>
							<address class="md-margin-bottom-40">
								<i class="fa fa-home"></i>C Simplify IT Services Private Limited <br />&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Unit-305, JMD Pacific Square, Sector 15<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbspGurgaon, Haryana ( India ).<br />
								<i class="fa fa-phone"></i>Phone: +91 98999 76227 <br />
								<i class="fa fa-globe"></i>Website: <a href="#">www.csimplifyit.com</a> <br />
								<i class="fa fa-envelope"></i>Email: <a href="sales@csimplifyit.com" class="">sales@csimplifyit.com</a></a>
							</address>

							<!-- Social Links -->
							
							<!-- End Social Links -->
						</div>
						<!-- End Address -->
					</div>
				</div>
			</div><!--/footer-->

			<div class="copyright">
				<div class="container">
					<p class="text-center"><script>					                           document.write(new Date().getFullYear());</script> &copy; All Rights Reserved. by <a target="_blank" href="index.aspx">C Simplify IT</a></p>
<p class="text-center lbl-address">Contact us @ Cues Simplify IT Services Private Limited (CIN U72900HR2011PTC043111)
Regd. Office: H No 314/21, Street No 5, Ward No 21, Madanpuri, Gurgaon, Haryana - 122001, INDIA<br/> Phone: +91 98 999 76227 Email ID: ez@CSimplifyIT.com</p>
				</div>
			</div><!--/copyright-->
		</div>
		<!--=== End Footer v2 ===-->

		
	</div><!--/wrapper-->


	

	
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
	<script type="text/javascript" src="assets/plugins/parallax-slider/js/modernizr.js"></script>
	<script type="text/javascript" src="assets/plugins/parallax-slider/js/jquery.cslider.js"></script>
	<script type="text/javascript" src="assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="assets/plugins/layer-slider/layerslider/js/greensock.js"></script>
	<script type="text/javascript" src="assets/plugins/layer-slider/layerslider/js/layerslider.transitions.js"></script>
	<script type="text/javascript" src="assets/plugins/layer-slider/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
    <script src="assets/plugins/jquery.parallax.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<!-- JS Page Level -->
  
	<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/layer-slider.js"></script>
	<script type="text/javascript" src="assets/js/plugins/owl-carousel.js"></script>
	<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
	<script type="text/javascript" src="assets/js/plugins/parallax-slider.js"></script>
    <script type="text/javascript" src="assets/plugins/wow-animations/js/wow.min.js"></script>
	<script type="text/javascript">
	    jQuery(document).ready(function () {
	        App.init();
	        App.initParallaxBg();
	        LayerSlider.initLayerSlider();
	        OwlCarousel.initOwlCarousel();
	        StyleSwitcher.initStyleSwitcher();
	        ParallaxSlider.initParallaxSlider();
	        new WOW().init();
	    });
	</script>
		<!--[if lt IE 9]>
			<script src="assets/plugins/respond.js"></script>
			<script src="assets/plugins/html5shiv.js"></script>
			<script src="assets/plugins/placeholder-IE-fixes.js"></script>
		<![endif]-->

	</body>
	</html>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="constant_connect.aspx.cs" Inherits="csimplifyit.WebForm14" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->
 
 <div class="clear"></div>
 <hr />
<h1 class="text_blue">Constant Connect</h1>
 <div class="clear"></div>

 <div class="three_fourth">
 
 
 <p>Connections brings credibility and business, maintaing the connections through Email, SMS, and social media, is a key requirement. Constant Connect helps remind, share, and enage your connections, in timely manner.</p>
 </div>
     
<div class="one_fourth column-last">
<img class="radius_10" src="images/Constant_Connect.PNG" />
</div>
     
 </div>
</asp:Content>

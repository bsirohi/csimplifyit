﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="p_sellers.aspx.cs" Inherits="csimplifyit.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->
 
<hr />
<h1 class="text_blue">Earn from your products online </h1>


<p> <q>"SpreadIn Market"</q> enables website advertisers of all sizes to display their products to people intrested in their products. 
You can easily display your products on multiple websites, site search results, and mobile sites. You fo not need to own any IT infrastructure, programming skills, 
payment gateways subscriptions, and customer support. Payments of products delivered will be deposited directly to your accounts, get paid on time. C Simplify 
IT will manage commisions to the affiliated web/feed/mobile sites. </p>
<div class="one_half border_2_blue padding_10">
<p class="text_blue"><strong>SpreadIn Market Online</strong></p>
<p><strong>Target the right user in the right context</strong><br />Using right keywords on your Ads, C Simplify IT contextual targeting 
technology can automatically match your ads to webpages that are most relevant to your business.  </p>
<p><strong>Measure and optimize your results</strong><br />Review your ad's performance on a site-by-site basis to see impression, click, cost, and conversion data, and 
use this data to identify well-performing ads to target more aggressively and low-value placements that require content optimization or exclusion.</p>

</div>
<div class="one_half column-last padding_10 bigtext_25">
<h2 class="text_blue"> How you go online with CSimplifyIT?</h2>
<p> <span class=" text_red">Step 1:</span> Join C Simplify IT (Sign In! here) </p>
<p><span class=" text_red">Step 2:</span> Create the products (SpreadIn Market) </p>
<p> <span class=" text_red">Step 3:</span> Setup the product (SpreadIn Market) </p>
<p><span class=" text_red">Step 4:</span> Activate the product (SpreadIn Market) </p>
<p><span class=" text_red">Step 5:</span> Reconcililation reports (Thermometer)  </p>
<p><span class=" text_red">Step 6:</span> Create more products, to earn more     </p>
</div>
<div class="clear"></div>
<p>* Sellers who qualify will be provided with a designated account manager, expedited technical support, and custom transactional feeds, while benefiting from C Simplify IT advanced technology and features.</p>

</div>


</asp:Content>

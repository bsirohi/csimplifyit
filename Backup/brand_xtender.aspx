﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="brand_xtender.aspx.cs" Inherits="csimplifyit.WebForm13" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->

 <div class="clear"></div>
 <hr />
<h1 class="text_blue">Brand Xtender</h1>
 <div class="clear"></div>

 <div class="one_half">
 
 
 <p>Delivering consistent message and maintaining agility is big challenge. Brand Xtender helps in managing multiple channels e.g. Google Ads, LinkedIn Ads, Facebook Ads, Wordpress, twitter, myspace, joining right groups, news, events participation, from a single interface.</p>
 </div>
     
<div class="one_half column-last">
<img class="radius_10" src="images/Brand_Xtender.png" />
</div>
     
 </div>
</asp:Content>

<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<title>Technology | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 

	<div class="wrapper">
    
		<!--=== Header ===-->
		<!--=== End Header ===-->

		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Our Technology Expertise</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li><a href="">Our Technology Expertise</a></li>					
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->
	    <!--=== Content Part ===-->
		<div class="container content">
			<div class="row">
				<div class="col-md-12">
					<h2 class="title-v2">Bots and Machine Learning Technology</h2>
					<div class="col-md-12">
                        <div class="col-md-2 col-xs-4"><img width="100%" src="assets/img/technology/cloud-mchine-learnings.png"></div>
                        <div class="col-md-2 col-xs-4"><img  width="100%" src="assets/img/technology/witi-ai-facebook.png"></div>
                        <div class="col-md-2 col-xs-4"><img width="100%" src="assets/img/technology/sirikit.png"></div>
						 <div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/ibm-watson.png"></div>
<div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/UiPath.jpg"></div>
<div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/blueprism.jpg"></div>
                    </div>                   
				</div>
				
			</div>
		</div><!--/container-->
		<!--=== Content Part ===-->
        <div class="bg-grey">
            <div class="container content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title-v2">Server Side Technology</h2>
                        <div class="col-md-12">
                            <div class="col-md-2 col-xs-4"><img width="100%" src="assets/img/technology/j2ee.png"></div>
                            <div class="col-md-2 col-xs-4"><img  width="100%" src="assets/img/technology/jsp-logo.png"></div>
                            <div class="col-md-2 col-xs-4"><img  width="100%" src="assets/img/technology/struts.png"></div>
                            <div class="col-md-2 col-xs-4"><img width="100%" src="assets/img/technology/Spring.png"></div>
                            <div class="col-md-2 col-xs-4"><img  width="100%"src="assets/img/technology/Hibernate.png"></div>
                            <div class="col-md-2 col-xs-4"><img  width="100%" src="assets/img/technology/net.jpg"></div>
                        </div>
                        <div class="col-md-12 ">
                            <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/php.png"></div>
                            <div class="col-md-2  col-xs-4 xs-mt-20"><img  width="100%" src="assets/img/technology/cakephp.png"></div>                       
                        </div>
                    </div>

                </div>
            </div><!--/container-->
        </div>
		<!--=== End Content Part ===-->
        <!--=== Content Part ===-->
        
            <div class="container content">
               <div class="col-md-12">
					<h2 class="title-v2">Business Intelligence, Analytics and BPM Technology</h2>
					<div class="col-md-12">
                        <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/spagobi.jpg"></div>
                        <div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/Pentaho.png"></div>
                        <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/qlikview.png"></div>
                        <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/pega.jpg"></div>
                        <div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/filenet.jpg"></div>
                        <div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/bonitasoft.png"></div>	
                    </div>
				   <div class="col-md-12">
				        <div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/ibm-watson.jpeg"></div>
				   </div>
				</div>
            </div><!--/container-->
       
		<!--=== End Content Part ===-->
         <!--=== Content Part ===-->
	     <div class="bg-grey">
			<div class="container content-sm">
			  <div class="col-md-12">
					<h2 class="title-v2">Mobility, Omnichannel Technology</h2>
					<div class="col-md-12">
                        <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/android-logo.png"></div>
                        <div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/ios-apple.png"></div>
                        <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/windows-app.png"></div>
                        <div class="col-md-2  col-xs-4"><img  width="100%"src="assets/img/technology/phone-gap-logo.png"></div>                        
                    </div>
				</div>
		    </div><!--/container-->
        </div>
        
		<!--=== End Content Part ===-->
       
          <!--=== Content Part ===-->
	
			<div class="container content-sm">
			<div class="row">
                	<div class="col-md-12">
				   <h2 class="title-v2">UI/UX Technology</h2>
					<div class="col-md-12">
                        <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/html5.png"></div>
                        <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/css-3.png"></div>
                        <div class="col-md-2  col-xs-4"><img  width="100%" src="assets/img/technology/bootstrap.png"></div>
                        <div class="col-md-2  col-xs-4"><img width="100%" src="assets/img/technology/angularjs.png"></div>
                        <div class="col-md-2  col-xs-4"><img  width="100%"src="assets/img/technology/JSON.png"></div>                        
                    </div>
				</div>
				
			</div>
		</div><!--/container-->
        
		<!--=== End Content Part ===-->
        
   
       
       
  <!--=== Footer v2 ===-->
		
		<!--=== End Footer v2 ===-->
</div><!--/wrapper-->

</asp:Content>





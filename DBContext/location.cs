//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace csimplifyit.DBContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class location
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public location()
        {
            this.location_settings = new HashSet<location_settings>();
            this.usermasters = new HashSet<usermaster>();
        }
    
        public long LocCode { get; set; }
        public string LocName { get; set; }
        public string Shortname { get; set; }
        public string LocationType { get; set; }
        public string HierarchyCode { get; set; }
        public Nullable<int> Country { get; set; }
        public Nullable<long> State { get; set; }
        public string PinCode { get; set; }
        public Nullable<bool> Status { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Fax { get; set; }
        public string Description { get; set; }
        public Nullable<long> City { get; set; }
        public string PanNumber { get; set; }
        public Nullable<decimal> lattitude { get; set; }
        public Nullable<decimal> longitude { get; set; }
        public Nullable<int> SourceLocation { get; set; }
        public Nullable<long> Owner { get; set; }
        public string UDF1 { get; set; }
        public string UDF2 { get; set; }
        public string UDF3 { get; set; }
        public string UDF4 { get; set; }
        public string UDF5 { get; set; }
        public long CRTBy { get; set; }
        public System.DateTime CRTDate { get; set; }
        public long UPDBy { get; set; }
        public System.DateTime UPDdate { get; set; }
    
        public virtual country country1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<location_settings> location_settings { get; set; }
        public virtual state state1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<usermaster> usermasters { get; set; }
    }
}

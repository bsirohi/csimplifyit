//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace csimplifyit.DBContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class ncounter
    {
        public string KeyName { get; set; }
        public Nullable<int> StartNo { get; set; }
        public Nullable<int> NextNo { get; set; }
        public string CRTBy { get; set; }
        public System.DateTime CRTDate { get; set; }
        public string UPDBy { get; set; }
        public System.DateTime UPDdate { get; set; }
        public string Year { get; set; }
    }
}

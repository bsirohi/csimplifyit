<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>Products | Do eCommerce!10x better.</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">
     <link rel="stylesheet" href="assets/css/blocks.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-default.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v2.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
	<link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="assets/css/pages/page_search.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/css/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-63272985-1’, 'auto');
        ga('send', 'pageview');
    </script>
</head>

<body>
	<div class="wrapper">
		<!--=== Header ===-->
		<div class="header">
			<div class="container">
				<!-- Logo -->
				<a class="logo" href="index.html">
					<img src="assets/img/logo1-default.png" alt="Logo">
				</a>
				<!-- End Logo -->

				

				<!-- Toggle get grouped for better mobile display -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
				</button>
				<!-- End Toggle -->
			</div><!--/end container-->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
				<div class="container">
					<ul class="nav navbar-nav">
                        <!-- Verticals -->
						<li class="dropdown"><a href="#" class="">Verticals</a>
				            <ul class="dropdown-menu">
										<li><a href="retail.aspx">Retail</a></li>
										<li><a href="transport-management.html">Transport Management</a></li>
										<li><a href="mobility.html">Mobility</a></li>                                       
										<li><a href="financial.html">Financial</a></li>
                                        <li><a href="healthcare.html">Health Care</a></li>
                                        <li><a href="testing-verticals.html">Testing</a></li>
				            </ul>
						</li>
						<!-- End Verticals -->
                        
                        <!---Products ------>
                        <li class="active"><a href="products.html" class="">Products</a></li>
                        <!---End Products ----->

						<!-- Servces -->
						<li class="dropdown">
							<a href="#" class="">Services</a>
				            <ul class="dropdown-menu">
										<li><a href="e-commerce.html">E-Commerce Services</a></li>
										<li><a href="context-centeric.html">Context Centric Services</a></li>
										<li><a href="cloud_integrations.html">Cloud Integrations</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="mobility_apps.html">Mobility Apps</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="budget_it_project.html">Budget IT Project</a></li>
                                            </ul>
                                        </li>
										<li><a href="botsandmachinelearning.html">Bots and Machine Learning</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="do_it_yourself.html">Do It Yourself</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="fly_high.html">Fly High</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="testing.html">Testing</a></li>
				            </ul>
						</li>
						<!-- End Services -->
                        <!-- Pages -->
						  <li class=""><a href="aboutus.html" class="">About Us</a></li>
						<!-- End Pages -->
                        
                        <!----Contact Us ---->
                        <li class=""><a href="contact.html" class="">Contact Us</a></li>
						<!-- End Contact Us -->

					</ul>
				</div><!--/end container-->
			</div><!--/navbar-collapse-->
		</div>
		<!--=== End Header ===-->


		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Products</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li><a href="">Products</a></li>
					
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				<!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
						<div class="carousel-inner">
							<div class="item active">
								<img alt="" src="assets/img/main/img4.jpg">
								<div class="carousel-caption">
									<p>PushBiz - Make your Business 'Visible' to your clients</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/pushbiz-2.png">
								<div class="carousel-caption">
									<p>PushBIZ helps you engage your customers in real time and with cutting edge technology</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/pushbiz-3.png">
								<div class="carousel-caption">
									<p>In 5 minutes, you can begin pushing your business messages to your clients. It is magical!</p>
								</div>
							</div>
                           
						</div>

						<div class="carousel-arrow">
							<a data-slide="prev" href="#myCarousel" class="left carousel-control">
								<i class="fa fa-angle-left"></i>
							</a>
							<a data-slide="next" href="#myCarousel" class="right carousel-control">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
				<!-- End Carousel -->

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>PushBiz - Make your Business 'Visible' to your clients</h2>
					<p><strong>Earn more - "Be Visible" - </strong> Make your clients fall in love with your magical offering without sending emails/sms/physical mails.Just be visible to your clients when you wish to. Increase traffic to your website upto 10X.</p>
                    <p><strong>Share Goodies - </strong> Share your special promotions, breaking news, product updates with your customers in real time.Excite them on special business occasions to visit you. Share enjoyable and actionable stories.</p>
					<p><strong>Be Fast and Furious</strong> - Watch the traffic roll in - fast and furious. It creates "loyal" customers in real time! and will help you beat your competitors hands down.</p>
					<ul class="list-unstyled">
						<li><i class="fa fa-users color-green"></i> Harshvardhan, Prashant Agarwal, Rajat Rana, Gaurav Kumar, Sanjog Mittal. </li>
						
						<li><i class="fa fa-tags color-green"></i> Websites, Google, HTML5/CSS3</li>
					</ul>
					<a href="https://www.pushbiz.in/pushbizhome" class="btn-u btn-u-large">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
			</div><!--/row-->

			<div class="tag-box tag-box-v2">
				<p>PushBIZ helps you engage your customers in real time and with cutting edge technology. All actions taken by customer are based on messages targeted by you to your customers. All clicks point to your own website. You can expect 10X increase in traffic and increased customer loyalty.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
        <!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>Freshers Villa - Let the world know about you. They need you.</h2>
					<p>Freshers bring new energies and hopes to any Organization. Their energies, ways of thinking and passion to do something important is unique. Welcome, we will help you gain respect which you deserve.</p>
					<p></p>
					<ul class="list-unstyled">
						<li><i class="fa fa-users color-green"></i> Parveen Verma, Vishal Gupta, Akshay Gupta, Minku Mittal, Rajat Rana. </li>					
						<li><i class="fa fa-tags color-green"></i> Websites, Google, HTML5/CSS3</li>
					</ul>
					<a href="http://freshervilla.appspot.com/" class="btn-u btn-u-large">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
                <!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
						<div class="carousel-inner">
							<div class="item active">
								<img alt="" src="assets/img/main/freshervilla.png">
								<div class="carousel-caption">
									<p>Freshers bring new energies and hopes to any Organization. </p>
								</div>
							</div>
							<!--<div class="item">
								<img alt="" src="assets/img/main/img12.jpg">
								<div class="carousel-caption">
									<p>Cras justo odio, dapibus ac facilisis into egestas.</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/img13.jpg">
								<div class="carousel-caption">
									<p>Justo cras odio apibus ac afilisis lingestas de.</p>
								</div>
							</div>-->
						</div>

						<div class="carousel-arrow">
							<a data-slide="prev" href="#myCarousel" class="left carousel-control">
								<i class="fa fa-angle-left"></i>
							</a>
							<a data-slide="next" href="#myCarousel" class="right carousel-control">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
				<!-- End Carousel -->
			</div><!--/row-->

			<div class="tag-box tag-box-v2">
				<p>Freshers bring new energies and hopes to any Organization. Their energies, ways of thinking and passion to do something important is unique. Welcome, we will help you gain respect which you deserve.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
        <!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				<!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
                        <div class="embed-responsive embed-responsive-4by3">
                              
                        <iframe width="530" height="300" src="https://www.youtube.com/embed/n_5tQrszX8s" frameborder="0" allowfullscreen></iframe>
                            </div>
						
					</div>
				</div>
				<!-- End Carousel -->

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>Smart Incidence Management - For Point of Sale and Inventory Management Application Support</h2>
					<p>C Simplify IT is focussed on cutting the supports cost and has build SIMS to stabilise the web systems faster and reduce the to & fro exchange mails/phone calls on support incidences.</p>
					<p></p>
					<ul class="list-unstyled">
						<li><i class="fa fa-users color-green"></i> Vishal Tayal, Ankit Kumar, Ashwani Gupta, Sanjog Mittal.</li>				
						<li><i class="fa fa-tags color-green"></i> Websites, Google, HTML5/CSS3</li>
					</ul>
					<a href="https://www.youtube.com/embed/n_5tQrszX8s" class="btn-u btn-u-large">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
			</div><!--/row-->

			<div class="tag-box tag-box-v2">
				<p>C Simplify IT is focussed on cutting the supports cost and has build SIMS to stabilise the web systems faster and reduce the to & fro exchange mails/phone calls on support incidences.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
        <!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>In SYNCH</h2>
					<p>In SYNCH "Integerates" Your Ecommerce Business, Suppliers, Vendors, and Customers in many ways.</p>
                    <p>Try "In SYNCH " For Follow UPS.</p>
                    <p><i class="fa fa-arrow-circle-right color-green"></i> It's Cool.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i> It's Good.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i>  It Saves Pain.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i> It Integerates.</p>
					
					<ul class="list-unstyled">
						<li><i class="fa fa-users color-green"></i> Arvind Pal, Bhuwan Chawla,Manoj Shukla, Bhoopesh Sisoudiya. </li>						
						<li><i class="fa fa-tags color-green"></i> Websites, Google, HTML5/CSS3</li>
					</ul>
					<a href="https://www.youtube.com/embed/f7CfNRBX-2k" class="btn-u btn-u-large">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
                <!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
						
							<div class="embed-responsive embed-responsive-4by3">
                              <iframe src="https://www.youtube.com/embed/f7CfNRBX-2k"  width="530" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                            </div>
                        

						
					</div>
				</div>
				<!-- End Carousel -->
			</div><!--/row-->

			<div class="tag-box tag-box-v2">
				<p>In SYNCH "Integerates" Your Ecommerce Business, Suppliers, Vendors, and Customers in many ways.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
       
       
  <!--=== Footer v2 ===-->
		<div id="footer-v2" class="footer-v2">
			<div class="footer">
				<div class="container">
					<div class="row">
						<!-- About -->
						<div class="col-md-3 md-mt-40">
							<a href="index.html"><img id="logo-footer" class="footer-logo" src="assets/img/logo-original.png" alt=""></a>
							<p class="margin-bottom-20">C Simplify IT Services established in the year 2011 located in gurgaon, India. we are handling projects in India, Singapore, Hong Kong, Thailand and USA.We have expertise in Expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.</p>

							</div>
						<!-- End About -->

						<!-- Link List -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="headline"><h2 class="heading-sm">Useful Links</h2></div>
							<ul class="list-unstyled link-list">
								<li><a href="aboutus.html">About us</a><i class="fa fa-angle-right"></i></li>
								<li><a href="services.html">Services</a><i class="fa fa-angle-right"></i></li>								
								<li><a href="retail.aspx">Verticals</a><i class="fa fa-angle-right"></i></li>
								<li><a href="contact.html">Contact us</a><i class="fa fa-angle-right"></i></li>
							</ul>
						</div>
						<!-- End Link List -->

						<!-- Latest Tweets -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="latest-tweets">
								<div class="headline"><h2 class="heading-sm">Social Links</h2></div>
								<div class="latest-tweets-inner">
									<ul class="social-icons">
                                        <li>
                                            <a href="https://www.facebook.com/CSimplifyIT-210115279023481/"
                                               data-original-title="Facebook" class="rounded-x social_facebook"></a></li>
                                        <li><a href="https://twitter.com/dmehta104" data-original-title="Twitter" class="rounded-x social_twitter"></a></li>
                                        <li><a href="https://plus.google.com/u/0/+CSimplifyITServicesPrivateLimitedNewDelhi/about" data-original-title="Goole Plus" class="rounded-x social_googleplus"></a></li>
                                        <li><a href="https://www.linkedin.com/company/2702366" data-original-title="Linkedin" class="rounded-x social_linkedin"></a></li>
                                    </ul>
                                </div>
							</div>
						</div>
						<!-- End Latest Tweets -->

						<!-- Address -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="headline"><h2 class="heading-sm">Contact Us</h2></div>
							<address class="md-margin-bottom-40">
								<i class="fa fa-home"></i>C Simplify IT Services Private Limited <br />&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;SCO-36, Huda Market, Sector 31,<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbspGurgaon, Haryana ( India ).<br />
								<i class="fa fa-phone"></i>Phone: +91 98999 76227 <br />
								<i class="fa fa-globe"></i>Website: <a href="https://csimplifyit.com/">www.csimplifyit.com</a> <br />
								<i class="fa fa-envelope"></i>Email: <a href="sales@csimplifyit.com" class="">sales@csimplifyit.com</a></a>
							</address>

							<!-- Social Links -->
							
							<!-- End Social Links -->
						</div>
						<!-- End Address -->
					</div>
				</div>
			</div><!--/footer-->

			<div class="copyright">
				<div class="container">
					<p class="text-center"><script>	document.write(new Date().getFullYear());</script> &copy; All Rights Reserved. by <a target="_blank" href="index.aspx">C Simplify IT</a></p>
<p class="text-center" style="font-size:12px;">Contact us @ Cues Simplify IT Services Private Limited (CIN U72900HR2011PTC043111)
Regd. Office: H No 314/21, Street No 5, Ward No 21, Madanpuri, Gurgaon, Haryana - 122001, INDIA<br/> Phone: +91 98 999 76227 Email ID: ez@CSimplifyIT.com</p>
				</div>
			</div><!--/copyright-->
		</div>
		<!--=== End Footer v2 ===-->
</div><!--/wrapper-->



<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="assets/plugins/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/plugins/counter/waypoints.min.js"></script>
<script type="text/javascript" src="assets/plugins/counter/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/cube-portfolio/cube-portfolio-lightbox.js"></script>
<script type="text/javascript" src="assets/plugins/wow-animations/js/wow.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		App.init();
		App.initCounter();
		App.initParallaxBg();
		StyleSwitcher.initStyleSwitcher();
        new WOW().init();
	});
</script>
<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->

</body>
</html>

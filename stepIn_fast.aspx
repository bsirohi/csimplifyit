﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="stepIn_fast.aspx.cs" Inherits="csimplifyit.WebForm16" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->

 <div class="clear"></div>
 <hr />
<h1 class="text_blue">StepIn Fast</h1>
 <div class="clear"></div>

 <div class="three_fourth">
 
 
 <p>Planning to setup a new company in India and establish it online, we can help you StepIn Fast, in your dream company. We can setup your domains, hosting, websites, products, workflows, search engine marketing and search engine optimizations, to make your business visible to world in cost-effective manner.</p>
 </div>
     
<div class="one_fourth column-last">
<img class="radius_10" src="images/Change_Management.PNG" />
</div>
<div class="clear"></div>
<h2 class="text_blue bold">Web packages</h2>
      <div class="pricing_table_wdg">
		<ul>
			<li>Basic</li>
			<li>INR 1600/-*</li>
			<li>1 Year Domain.</li>
			<li>5 Pages Hosted with Google</li>
			<li>No Google Checkout </li>
			<li>1 Flash Banner</li>
            <li>Enquire Form</li>
			<li>Personnel Email</li>
			<li>Basic SEO with Google</li>
			<li>Map for  1 Business Location</li>
            
            <li>--</li>
            <li>--</li>
			<li><a href="" class="buy_now">Buy Now</a></li>
		</ul>
		
		<ul>
			<li>Stable</li>
			<li>INR 4500/- *</li>
			<li>3 Year Domain.</li>
			<li>15 Pages Hosted with Google</li>
			<li> Payment checkout Google </li>
			<li>2 Flash Banner</li>
			<li> Enquiry + Announcement Form</li>
			<li>  Branded 50 Users Email</li>			
            <li> Basic SEO with Google</li>
            <li>Map for  3 Business Location</li>
            <li>--</li>
            <li>--</li>
			<li><a href="" class="buy_now">Buy Now</a></li>
		</ul>
		
		<ul>
			<li>Advanced</li>
			<li>INR 7000/- *</li>
			<li>5 Year Domain.</li>
			<li>30 Pages Hosted with Google</li>
			<li> Payment checkout Google </li>
			<li>3 Flash Banner</li>
			<li> 3 Enquiry + 2 Announcement Form</li>
			<li>  Branded 50 Users Email</li>			
            <li> Basic SEO with Google</li>
            <li>Map for 10 Business Location</li>
            <li> Site Builder Tool</li>
            <li>SMS Channel with Google</li>
			<li><a href="" class="buy_now">Buy Now</a></li>
		</ul>
		
	
</div>
<div class="clear"></div>
<p> * Taxes Included for India, elsewhere GST excluded<br />
# Content and images for websites to be provided<br />
# Project completion time Basic 3 Days, Stable 5 Days, Advanced 10 Days, with one month support for minor adjustments<br />
## Expect number of free goodies from vendors engaged! </p>
     

     <h2 class="text_blue">For Corporates:</h2>
     <p><span class="text_red bold">At C Simplify IT</span>, we have been helping enterprises drive business transformation by harnessing the power of technology. Leveraging technology expertise & decades of experience in managing multiple customer IT environments, C Simplify IT team has put together the right people, tools and processes to make you StepIN faster. </p>
     <p class="text_red">C Simplify IT Offerring includes</p>

     <ul>
	<li>An unbiased assessment of the application portfolio to obtain readiness to be moved into a given cloud environment </li>
	<li>Helps choose the right cloud environment, by providing an objective comparison amongst various cloud models - ROI Assessments</li>
	<li>Helps in laying out roadmap for cloud adoption by identifying high potential candidates that can quickly take advantage of cloud benefits </li>
	<li>Allows to decide the correct threshold by rating the threshold parameter values rather than choosing an arbitrary point </li>
	<li>Narrows down the scope of analysis for cost-benefit, Risk Mitigation, migration approach, target platform etc. </li>
	<li>Cloud migration planning</li>
	<li>General cloud education and best practices</li>
	
</ul>
<p class="text_blue bold">Why you need to evaluate the cloud offerings now !:</p>
<p class="text_red bold">Ride the wave</p>

<ul class="dashed_list">
<li>The Cloud is an emerging phenomenon,but will be mainstream in 3-5 years </li>
</ul>


<p class="text_red bold">You and your subsidiaries need to begin now</p>

<ul class="dashed_list">
<li>We are at the outer bands of an approaching hurricane</li>
<li>Business models need to be adapted</li>
<li>Investments and partnerships need to be evaluated</li>
<li>This takes time</li>
</ul>

<p class="text_red bold">Position yourself for success</p>

<ul class="dashed_list">
<li>Determine what your role(s) is going to be</li>
<li>Develop your competitive differentiation</li>
</ul>
 </div>
</asp:Content>

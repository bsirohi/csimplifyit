﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeBehind="About.aspx.cs" Inherits="csimplifyit.About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
   
</asp:Content>

<asp:Content ID="AboutBody" ContentPlaceHolderID="MainContent" runat="server">
    
<div id="body-wrapper" style="margin-top:100px;" class="clearfix"><!--start body-wrapper-->
 
 <div class="clear"></div>
 <hr />
<h1 class="text_blue">Let’s work together</h1>
 <div class="clear"></div>
 <p>
 Chances are you have a good idea of where you want to go in life. At C Simplify IT, we’ve designed a culture that helps you get there. From our flexible, project-based approach to corporate structure to our innovative perks and benefits, we do everything we can to make sure our employees not only have great jobs, but great lives. Into being challenged? Into having fun? Want to change the world? If the answer is yes, then you’ve come to the right place

Send your resume at <a href="mailto:Jobs@CSimpifyIT.com">Jobs@CSimpifyIT.com</a>
 </p>
 </div>
</asp:Content>

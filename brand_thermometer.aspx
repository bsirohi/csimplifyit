﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="brand_thermometer.aspx.cs" Inherits="csimplifyit.WebForm12" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->

 <div class="clear"></div>
 <hr />
<h1 class="text_blue">Brand Thermometer</h1>
 <div class="clear"></div>


 <p>Customer are changing their mind with-in few clicks and ensuring they have recognised and got associated with your brand is key. Measuring how much brand re-collection is shown by your choosen customer segments is key to success and will help in creating a focussed strategy to build. Deviations in consistent message across varied channels can be big damager of brand, especially when multiple teams are involved. Brand Xtender helps deliver same message across channels and Brand Thermometer measures its impact. Keeping brand red-hot and green is key to success</p>



 

 
 <div class="one_half">
 <p class="text_blue bold">Types of analysis Brand Thermometer provides:</p>
 <ol>
	<li>Optimal frequency </li>
	<li>Impact of technology - e.g. flash vs. rich media vs. gif </li>
	<li>Impact of ad format - e.g. text vs. skyscraper vs. banner vs. colors </li>
	<li> Valuable in-depth data filters, e.g. 
    <ul class="blank_list">
	<li>Age</li>
	<li>Frequency</li>
	<li>Brand Attributes </li>
	<li>Geo</li>
	<li>Creative Attributes </li>
	<li>Gender</li>
	<li>Creative Format </li>
	<li>Income</li>
    <li>Creative Technology </li>
    <li>Industry</li>
</ul>
    </li>
</ol></div>
<div class="one_half column-last">
<img class="radius_10" src="images/brand_tharmameter.png" />
</div>
 </div>
</asp:Content>

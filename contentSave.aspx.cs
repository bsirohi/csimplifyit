﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class contentSave : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string json;
        json = "[{";
        ResponseData response;
       //string token =request.QueryString[]

        foreach(var key in Request.QueryString.AllKeys){
             json = json+"\"" + key + "\":\"" + Request.QueryString[key] + "\",";
        }
        json=json.TrimEnd(',');
        json = json + "}]";
        response = new ResponseData(1, "Results", json);
        
        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";
        Response.Write(response.returnStatment());
        Response.End();
    }
}
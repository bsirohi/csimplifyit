<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>Services | Do eCommerce!10x better.</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-default.css">
	 <link rel="stylesheet" href="assets/css/footers/footer-v2.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css" type="text/css" media="screen">

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="assets/css/pages/page_search.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/css/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
	<div class="wrapper">
		<!--=== Header ===-->
		<div class="header">
			<div class="container">
				<!-- Logo -->
				<a class="logo" href="index.html">
					<img src="assets/img/logo1-default.png" alt="Logo">
				</a>
				<!-- End Logo -->

				

				<!-- Toggle get grouped for better mobile display -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
				</button>
				<!-- End Toggle -->
			</div><!--/end container-->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
				<div class="container">
					<ul class="nav navbar-nav">
						<!-- Home -->
						<li class="active">
							<a href="../index.html" >Home</a>
							
						</li>
						<!-- End Home -->

						<!-- Pages -->
						<li class="">
							<a href="../aboutus.html" class="">
								About Us
							</a>
							
						</li>
						<!-- End Pages -->

						<!-- Blog -->
						<li class="dropdown">
							<a href="#" class="">
								Services
							</a>
                            
				            <ul class="dropdown-menu">
										<li><a href="e-commerce.html">E-Commerce Services</a></li>
										<li><a href="page_jobs1.html">Context Centric Services</a></li>
										<li><a href="page_jobs_inner.html">Cloud Integrations</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="page_jobs_inner1.html">Mobility Apps</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="page_jobs.html">Budget IT Project</a></li>
                                            </ul>
                                        </li>
										<li><a href="page_jobs_inner2.html">Bots and Machine Learning</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="page_jobs_inner1.html">Do It Yourself</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="page_jobs.html">Fly High</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="page_jobs_inner2.html">Testing</a></li>
									</ul>
								
							
						</li>
						<!-- End Features -->

						
						<!-- Demo Pages -->
						<li class="">
							<a href="#" class="">
								Verticals
							</a>
							
						</li>
                        <li class="">
							<a href="contact.html" class="">
								Contact Us
							</a>
							
						</li>
						<!-- End Demo Pages -->

					</ul>
				</div><!--/end container-->
			</div><!--/navbar-collapse-->
		</div>
		<!--=== End Header ===-->

		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs breadcrumbs-light">
			<div class="container">
				<h1 class="pull-left">Our Services</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li><a href="">Pages</a></li>
					<li class="active">Services</li>
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		<!--=== News Block ===-->
		<div class="container content-sm">
			<div class="container">
				<div class="row">
					<div class="col-md-6 md-margin-bottom-30">
						<h2 class="title-v2">WATCH ABOUT US</h2>
						<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
						<p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p><br>
						<a href="#" class="btn-u btn-brd btn-brd-hover btn-u-dark">Read More</a>
						<a href="#" class="btn-u">Purchase Now</a>
					</div>
					<div class="col-md-6">
						<div class="responsive-video">
							<iframe src="//player.vimeo.com/video/78451097?title=0&amp;byline=0&amp;portrait=0&amp;badge=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--=== End News Block ===-->

		<!--=== Parallax Quote ===-->
		<div class="parallax-quote parallaxBg" style="background-position: 50% 20px;">
			<div class="container">
				<div class="parallax-quote-in">
					<p>If you can design one thing <span class="color-green">you can design</span> everything. <br> Just Believe It.</p>
					<small>- csimplifyit -</small>
				</div>
			</div>
		</div>
		<!--=== End Parallax Quote ===-->

		<!--=== Colorful Service Blocks ===-->
		<div class="container-fluid">
			<div class="row no-gutter equal-height-columns margin-bottom-10">
				<div class="col-sm-4">
					<div class="service-block service-block-purple no-margin-bottom content equal-height-column">
						<i class="icon-custom icon-md rounded icon-color-light icon-line icon-badge"></i>
						<h2 class="heading-md font-light">Best Solutions</h2>
						<p class="no-margin-bottom font-light">Provide; shifting landscape reduce carbon emissions human potential sustainability Jane Addams solve. Global network; care Rockefeller, vaccines equal opportunity human being.</p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-block service-block-red no-margin-bottom content equal-height-column">
						<i class="icon-custom icon-md rounded icon-color-light icon-line icon-fire"></i>
						<h2 class="heading-md font-light">Excellent Features</h2>
						<p class="no-margin-bottom font-light">Provide; shifting landscape reduce carbon emissions human potential sustainability Jane Addams solve. Global network; care Rockefeller, vaccines equal opportunity human being.</p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-block service-block-aqua no-margin-bottom content equal-height-column">
						<i class="icon-custom icon-md rounded icon-color-light icon-line icon-directions"></i>
						<h2 class="heading-md font-light">Creative Ideas</h2>
						<p class="no-margin-bottom font-light">Provide; shifting landscape reduce carbon emissions human potential sustainability Jane Addams solve. Global network; care Rockefeller, vaccines equal opportunity human being.</p>
					</div>
				</div>
			</div>
		</div>
		<!--=== End Colorful Service Blocks ===-->

		<!--=== Service Blcoks ===-->
		<div class="container content-sm">
			<div class="text-center margin-bottom-50">
				<h2 class="title-v2 title-center">OUR SERVICES</h2>
				<p class="space-lg-hor">If you are going to use a <span class="color-green">passage of Lorem Ipsum</span>, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making <span class="color-green">this the first</span> true generator on the Internet.</p>
			</div>

			<!-- Service Blcoks -->
			<div class="row service-box-v1 margin-bottom-40">
				<div class="col-md-4 col-sm-6 md-margin-bottom-40">
					<div class="service-block service-block-default no-margin-bottom">
						<i class="icon-lg rounded-x icon icon-badge"></i>
						<h2 class="heading-sm">Web &amp; Mobile  Technologies</h2>
						<p> C Simplify IT is an incredibly beautiful responsive Bootstrap Template for corporate and creative professionals. It works on all major web browsers, tablets and phone.</p>
						<ul class="list-unstyled">
							<li>Responsive Web Desgin</li>
							<li>E-commerce</li>
							<li>App &amp; Icon Design</li>
							<li>Email Marketing</li>
							<li>Mobile Development</li>
							<li>UI/UX Design</li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 md-margin-bottom-40">
					<div class="service-block service-block-default no-margin-bottom">
						<i class="icon-lg rounded-x icon-line icon-trophy"></i>
						<h2 class="heading-sm">Software &amp; Development</h2>
						<p>Donec id elit non mi porta gravida at eget metus id elit mi egetine usce dapibus elit nondapibus</p>
						<ul class="list-unstyled">
							<li>Analysis &amp; Consulting</li>
							<li>Email Marketing</li>
							<li>App &amp; Icon Design</li>
							<li>Responsive Web Desgin</li>
							<li>Social Networking</li>
							<li>Documentation</li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-default no-margin-bottom">
						<i class="icon-lg rounded-x icon-line icon-layers"></i>
						<h2 class="heading-sm">SEO &amp; Advertising</h2>
						<p>Donec id elit non mi porta gravida at eget metus id elit mi egetine. Fusce dapibus</p>
						<ul class="list-unstyled">
							<li>Display Advertising</li>
							<li>App &amp; Icon Design</li>
							<li>Analysis &amp; Consulting</li>
							<li>Google AdSense</li>
							<li>Social Media</li>
							<li>Google/Bing Analysis</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- End Service Blcoks -->
		</div>
		<!--=== End Service Blcoks ===-->

		<!--=== Parallax Counter ===-->
		<div class="parallax-counter-v2 parallaxBg1" style="background-position: 50% 90px;">
			<div class="container">
				<ul class="row list-row">
					<li class="col-md-3 col-sm-6 col-xs-12 md-margin-bottom-30">
						<div class="counters rounded">
							<span class="counter">18298</span>
							<h4 class="text-transform-normal">Web Developers</h4>
						</div>
					</li>
					<li class="col-md-3 col-sm-6 col-xs-12 md-margin-bottom-30">
						<div class="counters rounded">
							<span class="counter">24583</span>
							<h4 class="text-transform-normal">Designers</h4>
						</div>
					</li>
					<li class="col-md-3 col-sm-6 col-xs-12 sm-margin-bottom-30">
						<div class="counters rounded">
							<span class="counter">37904</span>
							<h4 class="text-transform-normal">Open Contests</h4>
						</div>
					</li>
					<li class="col-md-3 col-sm-6 col-xs-12">
						<div class="counters rounded">
							<span class="counter">50892</span>
							<h4 class="text-transform-normal">Happy Customors</h4>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<!--=== End Parallax Counter ===-->

		<!--=== News Block ===-->
		<div class="container content-sm">
			<div class="text-center margin-bottom-50">
				<h2 class="title-v2 title-center">RECENT NEWS</h2>
				<p class="space-lg-hor">If you are going to use a <span class="color-green">passage of Lorem Ipsum</span>, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making <span class="color-green">this the first</span> true generator on the Internet.</p>
			</div>

			<div class="row news-v2">
				<div class="col-md-4 md-margin-bottom-30">
					<div class="news-v2-badge">
						<img class="img-responsive" src="assets/img/main/img12.jpg" alt="">
						<p>
							<span>26</span>
							<small>Feb</small>
						</p>
					</div>
					<div class="news-v2-desc bg-color-light">
						<h3><a href="#">Corrupti Quos Dolores</a></h3>
						<small>By Admin | <a class="color-inherit" href="#">16 Comments</a> | In <a href="#">Web Trends</a></small>
						<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.</p>
					</div>
				</div>
				<div class="col-md-4 md-margin-bottom-30">
					<div class="news-v2-badge">
						<img class="img-responsive" src="assets/img/main/img3.jpg" alt="">
						<p>
							<span>24</span>
							<small>Feb</small>
						</p>
					</div>
					<div class="news-v2-desc bg-color-light">
						<h3><a href="#">Blanditi Praesium Voluptum</a></h3>
						<small>By Admin | <a class="color-inherit" href="#">51 Comments</a> | In <a href="#">Art &amp; Design</a></small>
						<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="news-v2-badge">
						<img class="img-responsive" src="assets/img/main/img16.jpg" alt="">
						<p>
							<span>21</span>
							<small>Feb</small>
						</p>
					</div>
					<div class="news-v2-desc bg-color-light">
						<h3><a href="#">Key Digital Services</a></h3>
						<small>By Admin | <a class="color-inherit" href="#">32 Comments</a> | In <a href="#">Google &amp; Android</a></small>
						<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.</p>
					</div>
				</div>
			</div>
		</div>
		<!--=== End News Block ===-->

		<!--=== Call To Action ===-->
		<div class="call-action-v1 bg-color-red">
			<div class="container">
				<div class="call-action-v1-box">
					<div class="call-action-v1-in">
						<p class="color-light">Csimplifyit creative technology company providing key digital services and focused on helping our clients to build a successful business on web and mobile.</p>
					</div>
					<div class="call-action-v1-in inner-btn page-scroll">
						<a href="#portfolio" class="btn-u btn-brd btn-brd-hover btn-u-light btn-u-block">View Our Portfolio</a>
					</div>
				</div>
			</div>
		</div>
		<!--=== End Call To Action ===-->

	 <!--=== Footer v2 ===-->
		<div id="footer-v2" class="footer-v2">
			<div class="footer">
				<div class="container">
					<div class="row">
						<!-- About -->
						<div class="col-md-3 md-mt-40">
							<a href="index.html"><img id="logo-footer" class="footer-logo" src="assets/img/logo-original.png" alt=""></a>
							<p class="margin-bottom-20">C Simplify IT Services established in the year 2011 located in gurgaon, India. we are handling projects in India, Singapore, Hong Kong, Thailand and USA.We have expertise in Expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.</p>

							</div>
						<!-- End About -->

						<!-- Link List -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="headline"><h2 class="heading-sm">Useful Links</h2></div>
							<ul class="list-unstyled link-list">
								<li><a href="aboutus.html">About us</a><i class="fa fa-angle-right"></i></li>
								<li><a href="services.html">Services</a><i class="fa fa-angle-right"></i></li>								
								<li><a href="retail.aspx">Verticals</a><i class="fa fa-angle-right"></i></li>
								<li><a href="contact.html">Contact us</a><i class="fa fa-angle-right"></i></li>
							</ul>
						</div>
						<!-- End Link List -->

						<!-- Latest Tweets -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="latest-tweets">
								<div class="headline"><h2 class="heading-sm">Social Links</h2></div>
								<div class="latest-tweets-inner">
									<ul class="social-icons">
                                        <li>
                                            <a href="https://www.facebook.com/CSimplifyIT-210115279023481/"
                                               data-original-title="Facebook" class="rounded-x social_facebook"></a></li>
                                        <li><a href="https://twitter.com/dmehta104" data-original-title="Twitter" class="rounded-x social_twitter"></a></li>
                                        <li><a href="https://plus.google.com/u/0/+CSimplifyITServicesPrivateLimitedNewDelhi/about" data-original-title="Goole Plus" class="rounded-x social_googleplus"></a></li>
                                        <li><a href="https://www.linkedin.com/company/2702366" data-original-title="Linkedin" class="rounded-x social_linkedin"></a></li>
                                    </ul>
                                </div>
							</div>
						</div>
						<!-- End Latest Tweets -->

						<!-- Address -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="headline"><h2 class="heading-sm">Contact Us</h2></div>
							<address class="md-margin-bottom-40">
								<i class="fa fa-home"></i>C Simplify IT Services Private Limited <br />&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;SCO-36, Huda Market, Sector 31,<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbspGurgaon, Haryana ( India ).<br />
								<i class="fa fa-phone"></i>Phone: +91 98999 76227 <br />
								<i class="fa fa-globe"></i>Website: <a href="https://csimplifyit.com/">www.csimplifyit.com</a> <br />
								<i class="fa fa-envelope"></i>Email: <a href="sales@csimplifyit.com" class="">sales@csimplifyit.com</a></a>
							</address>

							<!-- Social Links -->
							
							<!-- End Social Links -->
						</div>
						<!-- End Address -->
					</div>
				</div>
			</div><!--/footer-->

			<div class="copyright">
				<div class="container">
					<p class="text-center"><script>	document.write(new Date().getFullYear());</script> &copy; All Rights Reserved. by <a target="_blank" href="index.aspx">C Simplify IT</a></p>
<p class="text-center" style="font-size:12px;">Contact us @ Cues Simplify IT Services Private Limited (CIN U72900HR2011PTC043111)
Regd. Office: H No 314/21, Street No 5, Ward No 21, Madanpuri, Gurgaon, Haryana - 122001, INDIA<br/> Phone: +91 98 999 76227 Email ID: ez@CSimplifyIT.com</p>
				</div>
			</div><!--/copyright-->
		</div>
		<!--=== End Footer v2 ===-->
	</div><!--/wrapper-->

	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
	<script type="text/javascript" src="assets/plugins/jquery.parallax.js"></script>
	<script type="text/javascript" src="assets/plugins/counter/waypoints.min.js"></script>
	<script type="text/javascript" src="assets/plugins/counter/jquery.counterup.min.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="assets/js/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="assets/js/app.js"></script>
	<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			App.initCounter();
			App.initParallaxBg();
			StyleSwitcher.initStyleSwitcher();
		});
	</script>
<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->

</body>
</html>

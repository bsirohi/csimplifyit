﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="StripePayment.aspx.cs" Inherits="csimplifyit.Strip_Payment_way.StripePayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        
    <link href="../assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="../assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="../assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="../assets/css/theme.css" rel="stylesheet">


     <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.jpg">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.jpg">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicons/favicon.ico">

    <link rel="manifest" href="../assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="../assets/img/favicons/mstile-150x150.jpg">
    <meta name="theme-color" content="#ffffff">

    <meta name="google-sigin-client_id" content="509303823636-4nmni39qd4m0egmiv7cgkdqq57teuo5h.apps.googleusercontent.com" />
        <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->

    <link href="../assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="../assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="../assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="../assets/lib/semantic-ui-accordion/accordion.min.css" rel="stylesheet">
    <link rel="../stylesheet" href="assets/css/font-awesome.css">
    <link href="../assets/css/theme.css" rel="stylesheet">
    <link rel="../stylesheet" type="text/css" href="assets/css/document_management.css">
    <link href="../contents/UtilityCss/common.css" rel="stylesheet" />

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
   
    <script src="../assets/js/scrolling-nav.js"></script>
    <script src="../assets/js/plugins.js"></script>
    <script src="../assets/lib/loaders.css/loaders.css.js"></script>
    <script src="../assets/js/stickyfill.min.js"></script>
    <script src="../assets/lib/remodal/remodal.js"></script>
    <script src="../assets/lib/jtap/jquery.tap.js"></script>
    <script src="../assets/js/rellax.min.js"></script>
    <script src="../assets/lib/owl.carousel/owl.carousel.js"></script>
    <script src="../assets/lib/lightbox2/js/lightbox.js"></script>
    <script src="../assets/lib/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="../assets/lib/isotope-packery/packery-mode.pkgd.min.js"></script>
    <script src="../assets/js/Default.js"></script>
    <script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
    <script src="../assets/js/theme.js"></script>
    <script src="../Scripts/Utility/common.js"></script>

  <%-- <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>--%>

    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script src="../Scripts\Stripe\Stripe.js"></script>


    <script type="text/javascript">

       
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
              m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
          ga('create', 'UA-63272985-1', 'auto');
          ga('send', 'pageview');

      </script>



    
        <style type="text/css">
     .plans:hover {
        box-shadow: 1px 5px 8px #3333337d;
        background: white !important;
      }
      #pricing_details li {
          list-style: none;
      }
      .yes_feature:before {
          content: '\2713';
          color: green;
          font-weight: bold;
          padding-right: 5px;
      }
      .not_feature:before {
          content: '\2715';
          color: red;
          font-weight: bold;
          padding-right: 5px;
      }
      .promo-code { 
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
      }
      .promo-checkbox { margin: 0.5rem 0; }
      .upgradePlans {
        color: blue;
        font-weight: bold;
        font-size: 20px;
        display: block;
      }
      #studio-contact {
          border-bottom: 2px solid #929292;
      }
      .amount span{
        font-size: 40px; 
        vertical-align: top; 
        font-weight: bold; 
        color: #333;
      }
      .discountedPrice {
        font-size: 24px !important; 
        text-decoration: line-through;
      }
    </style>
</head>


<body>


    <!--===============================================-->
    <!--    Fancynav-->
    <!--===============================================-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#landing-page">
          <img src="../assets/img/logo.png" alt="C Simplify IT" height="45" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown1">
          <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#landing-page">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#whatwedo">Solutions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#offerings">Offerings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#technology">Technologies</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbarDropdownMenuProduct" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product</a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuProduct">
                <a class="dropdown-item" href="../SM3/SimpleMassMailMerge.aspx">Simple Mass Mail Merge</a>
                     <a class="dropdown-item" href="../SRM/SimpleRecruitmentManager.aspx">Simple Recruitment Manager</a>
                  <a class="dropdown-item" href="../googleworkflow/SimpleWorkflowManager.aspx">Simple Workflow Manager</a>
                  <a class="dropdown-item" href="SPOS/SimplePointofSale.aspx">Simple Point of Sale</a>
                  <a class="dropdown-item" href="STDGDoc/SimpleTabularDocumentGenerator.aspx">Simple Tabular Document Generator</a>
                  
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Strip_Payment_way/StripePayment.aspx">Pricing</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#whyus">Why Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#clients">Our Clients</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact Us</a>
            </li>
               <li class="nav-item" id="AddUserName">                             
                           
            </li>
               <li class="nav-item" id="AddLogOutButton">                             
                            
            </li>


          </ul>
        </div>
      </div>
    </nav>
    <!--===============================================-->
    <!--    End of Fancynav-->
    <!--===============================================-->



    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main minh-100vh" id="top">


      <!-- ============================================-->
      <!-- Preloader ==================================-->
      <div class="preloader" id="preloader">
        <div class="loader">
          <div class="line-scale-pulse-out-rapid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
      <!-- ============================================-->
      <!-- End of Preloader ===========================-->

	  
	  <!-- ============================================-->
      <!-- <section> begin ============================-->
      <div class="speciality">
        <a href="../Our_Specialities.aspx">Our Specialities</a>
      </div>
      <!-- <section> close ============================-->
      <!-- ============================================-->
	  
      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0" id="plansPricing">

        <div class="container-fluid">
          <div class="row" data-zanim-timeline="{}" data-zanim-trigger="scroll">
            <div class="col-lg-6">
              <div class="row align-items-center justify-content-center py-6 py-md-8">
                <div class="col-11 col-sm-10">
                  <div class="overflow-hidden">
                    <h2 class="font-weight-light text-uppercase fs-3 fs-lg-4" data-zanim-lg='{"delay":0.4}'><span class="font-weight-black"> Plans &<br/> Pricing</span></h2>
                  </div>
                  <div class="overflow-hidden">
                    <p class="lead text-sans-serif font-weight-normal pr-xl-9 mt-4 text-500" data-zanim-lg='{"delay":0.5}'>Whether your time-saving<br/> automation needs are large or small,<br/> we’re here to help you scale.</p>
                  </div>
                  <div id="SignInSection" class="overflow-hidden">
                    <div class="mt-3 mt-lg-5" data-zanim-xs='{"delay":0.6}'>
                      <a class="btn btn-danger" href="../signin/signin.aspx?id=1">Sign Up/Log In</a>
                     <%-- <h6 class="font-weight-normal mt-2 text-500">Have an account?
                        <a class="text-900 font-weight-bold ml-1" href="#">Sign in<span class="d-inline-block ml-1">&xrarr;</span></a>
                      </h6>--%>
                    </div>
                  </div>
                     <div id="USerDetailsSection" class="overflow-hidden">
                    <div class="mt-3 mt-lg-5" data-zanim-xs='{"delay":0.6}'>
                      <a class="btn btn-danger" href="../SubScriptionDetails/UserSubScriptionDetails.aspx">Show All SubScription Details</a>                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 px-0">
              <div class="h-100 py-9" data-zanim-lg='{"animation":"slide-right","from":{"opacity":1},"delay":0.4}' data-zanim-trigger="scroll">
                <div class="bg-holder" style="background-image:url(../assets/img/pricing.jpg);" data-zanim-trigger="scroll" data-zanim-lg='{"animation":"slide-right","delay":0}'>
                </div>
                <!--/.bg-holder-->

              </div>
            </div>
            
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->
         <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="text-sans-serif" id="pricing_details" style="padding-top: 0 !important;">
          <%--  <button class="button" id="gSignInid" onclick="OpenGoogleLoginPopup()" style="background-color:#e82121"><i class="fa fa-google-plus"></i> Sign in with Google</button>--%>
       
                  <div class="container-fluid">


                <%-- SM3 Section Start Here --%>

                      <div id="SimpleMassMailMearge">

                      
                <div class="container">
                    <h3 class="text-center" style="padding: 3%">Simple mass mail merge</h3>
                </div>


                <div class="row">
                    <div class="col-sm-6 col-lg-1 mb-1"></div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3" style="box-shadow: 1px 5px 8px #3333337d;">
                            <div class="amount">
                                <p id="basicprice">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span  class="discountedPrice">20</span><span> 0</span>--%>
                                </p>
                                <p id="basicvalidity"><%--Free till Mar,2020--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="basicDesc" class="product_desc">
                                <h3>Basic</h3>
                                <%--<p>Anyone can automate their work. Start with the basics.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="basic_period" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$20</span> - 750 tasks/month</option>
                      <option><span>$35</span> - 1,500 tasks/month</option>--%>
                  </select>
                     <%-- <p style="border-color: #e1e1e1 !important; border: 1px solid #e1e1e1; border-radius: 0.1875rem; padding: 0.8rem 0.8rem;">100 tasks/month</p>--%>
                </div>
                <div class="product_plan">
                  <a class="btn btn-dark btn-block "  onclick="bascicPlan()" href="#">Current</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-2 mb-1">
              <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                <div class="amount">
                  <p id="StarterPrice" ><img src="../assets/img/dollar.png" style="width: 25px;"> <%--<span id="" class="discountedPrice">50</span><span> 20</span>--%></p> 
                  <p id="StarterValidate"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                </div>
                <hr align="left" width="30%" style="border: 1px solid #333;">
                <div id="StarterDesc" class="product_desc">
                  <h3>Starter</h3>
                 <%-- <p>Unleash the power of automation.</p>--%>
                </div>
                <div class="product_period" style="margin-bottom: 20px;">
                  <select id="Starter_period" class="form-control border-300 d-inline-block text-sans-serif">
                      <%--<option><span>$20</span> - 750 tasks/month</option>
                      <option><span>$35</span> - 1,500 tasks/month</option>--%>
                  </select>
                </div>
                <div class="product_plan">
                  <a class="btn btn-dark btn-block"  onclick="StarterPlan()"  href='#'>Upgrade</a>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6 col-lg-2 mb-1">
              <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                <div class="amount">
                  <p id="ProfessionalPrice"><img src="../assets/img/dollar.png" style="width: 25px;"><%--<span class="discountedPrice"> 80</span> <span>50</span>--%></p> 
                  <p id="ProfessionalValiditity"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                </div>
                <hr align="left" width="30%" style="border: 1px solid #333;">
                <div id="ProfessionalDesc" class="product_desc">
                  <h3>Professional</h3>
               <%--   <p>Advanced tools to take your work to the next level.</p>--%>
                </div>
                <div class="product_period" style="margin-bottom: 20px;">
                  <select id="Professionalperiod" class="form-control border-300 d-inline-block text-sans-serif">
                     <%-- <option><span>$50</span> - 2,000 tasks/month</option>
                      <option><span>$90</span> - 5,000 tasks/month</option>
                      <option><span>$130</span> - 10,000 tasks/month</option>--%>
                  </select>
                </div>
                <div class="product_plan">
                  <a class="btn btn-dark btn-block" onclick="ProfessionalPlan()" href="#">Upgrade</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-2 mb-1">
              <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                <div class="amount">
                  <p id="PremiumPrice"><img src="../assets/img/dollar.png" style="width: 25px;"> <%--<span class="discountedPrice">350</span><span> 300</span>--%></p> 
                  <p id="PremiumValidity"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                </div>
                <hr align="left" width="30%" style="border: 1px solid #333;">
                <div id="PremiumDesc" class="product_desc">
                  <h3>Premium</h3>
                 <%-- <p>Bring your team together to collaborate on automation.</p>--%>
                </div>
                <div class="product_period" style="margin-bottom: 20px;">
                  <select id="Premiumperiod" class="form-control border-300 d-inline-block text-sans-serif">
                      <%--<option><span>$300</span> - 50,000 tasks/month</option>
                      <option><span>$500</span> - 1,00,000 tasks/month</option>
                      <option><span>$800</span> - 2,00,000 tasks/month</option>
                      <option><span>$1000</span> - Unlimited tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block"  onclick="PremiumPlan()" href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="SupportPrice">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span style="font-size: 40px; vertical-align: top; font-weight: bold; color: #333;">50</span>--%>
                                </p>
                                <p id="Supportvalidity"><%--hour/day<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="SupportDesc" class="product_desc">
                                <h3>Support</h3>
                                <%-- <p>Provide you support dedicatedly</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="Supportperiod" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$50</span> - 1 hour/day</option>
                      <option><span>$100</span> - 8 hours/day</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="SupportPlan()"  href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-1 mb-1"></div>
                </div>
                <div class="row mt-6">
                    <div class="col-sm-6 col-lg-1 mb-4"></div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">basic plan features</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="not_feature">Automated scheduling</li>
                            <li class="not_feature">Escalations</li>
                            <li class="not_feature">Email trigger</li>
                            <li class="not_feature">Support available</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">starter plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">12 hours automated scheduling</li>
                            <li class="not_feature">Escalations</li>
                            <li class="not_feature">Email Trigger</li>
                            <li class="not_feature">Support available</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">professional plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">6 hours automated scheduling</li>
                            <li class="yes_feature">6 hours escalation emails</li>
                            <li class="not_feature">Email trigger</li>
                            <li class="yes_feature">Free support for 1 month</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">premium plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">1 hour automated scheduling</li>
                            <li class="yes_feature">1 hour escalation emails</li>
                            <li class="yes_feature">1 hour email trigger</li>
                            <li class="yes_feature">Free support for 3 months</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">support plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">Dedicated support</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-1 mb-4"></div>
                </div>
                <%-- End section sm3 --%>
           </div>


                <%-- Start Simple Recruitement manager  --%>

                       <div id="SimpleRecruitmentManger">

                <div class="container">
                    <h3 class="text-center" style="padding: 3%">Simple Recruitment Manger</h3>


                </div>
                <div class="row">
                    <div class="col-sm-6 col-lg-1 mb-1"></div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3" style="box-shadow: 1px 5px 8px #3333337d;">
                            <div class="amount">
                                <p id="basicpriceSRM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span  class="discountedPrice">20</span><span> 0</span>--%>
                                </p>
                                <p id="basicvaliditySRM"><%--Free till Mar,2020--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="basicDescSRM" class="product_desc">
                                <h3>Basic</h3>
                                <%--<p>Anyone can automate their work. Start with the basics.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="basic_periodSRM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$20</span> - 750 tasks/month</option>
                      <option><span>$35</span> - 1,500 tasks/month</option>--%>
                                </select>
                                <%-- <p style="border-color: #e1e1e1 !important; border: 1px solid #e1e1e1; border-radius: 0.1875rem; padding: 0.8rem 0.8rem;">100 tasks/month</p>--%>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block " onclick="bascicPlan1()" href="#">Current</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="StarterPriceSRM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span id="" class="discountedPrice">50</span><span> 20</span>--%>
                                </p>
                                <p id="StarterValidateSRM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="StarterDescSRM" class="product_desc">
                                <h3>Starter</h3>
                                <%-- <p>Unleash the power of automation.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="Starter_periodSRM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%--<option><span>$20</span> - 750 tasks/month</option>
                      <option><span>$35</span> - 1,500 tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="StarterPlan1()" href='#'>Upgrade</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="ProfessionalPriceSRM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;"><%--<span class="discountedPrice"> 80</span> <span>50</span>--%>
                                </p>
                                <p id="ProfessionalValidititySRM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="ProfessionalDescSRM" class="product_desc">
                                <h3>Professional</h3>
                                <%--   <p>Advanced tools to take your work to the next level.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="ProfessionalperiodSRM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$50</span> - 2,000 tasks/month</option>
                      <option><span>$90</span> - 5,000 tasks/month</option>
                      <option><span>$130</span> - 10,000 tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="ProfessionalPlan1()"  href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="PremiumPriceSRM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span class="discountedPrice">350</span><span> 300</span>--%>
                                </p>
                                <p id="PremiumValiditySRM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="PremiumDescSRM" class="product_desc">
                                <h3>Premium</h3>
                                <%-- <p>Bring your team together to collaborate on automation.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="PremiumperiodSRM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%--<option><span>$300</span> - 50,000 tasks/month</option>
                      <option><span>$500</span> - 1,00,000 tasks/month</option>
                      <option><span>$800</span> - 2,00,000 tasks/month</option>
                      <option><span>$1000</span> - Unlimited tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="PremiumPlan1()"  href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="SupportPriceSRM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span style="font-size: 40px; vertical-align: top; font-weight: bold; color: #333;">50</span>--%>
                                </p>
                                <p id="SupportvaliditySRM"><%--hour/day<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="SupportDescSRM" class="product_desc">
                                <h3>Support</h3>
                                <%-- <p>Provide you support dedicatedly</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="SupportperiodSRM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$50</span> - 1 hour/day</option>
                      <option><span>$100</span> - 8 hours/day</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block"  onclick="SupportPlan1()" href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-1 mb-1"></div>
                </div>
                <div class="row mt-6">
                    <div class="col-sm-6 col-lg-1 mb-4"></div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">basic plan features</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="not_feature">Automated scheduling</li>
                            <li class="not_feature">Escalations</li>
                            <li class="not_feature">Email trigger</li>
                            <li class="not_feature">Support available</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">starter plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">12 hours automated scheduling</li>
                            <li class="not_feature">Escalations</li>
                            <li class="not_feature">Email Trigger</li>
                            <li class="not_feature">Support available</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">professional plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">6 hours automated scheduling</li>
                            <li class="yes_feature">6 hours escalation emails</li>
                            <li class="not_feature">Email trigger</li>
                            <li class="yes_feature">Free support for 1 month</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">premium plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">1 hour automated scheduling</li>
                            <li class="yes_feature">1 hour escalation emails</li>
                            <li class="yes_feature">1 hour email trigger</li>
                            <li class="yes_feature">Free support for 3 months</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">support plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">Dedicated support</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-1 mb-4"></div>
                </div>
                <%-- End simple Recruitement Manager  --%>


 </div>

                      <div id="SimpleWorkflowManager">
                <%-- Start simple workflow manager --%>
                <div class="container">
                    <h3 class="text-center" style="padding: 3%">Simple Workflow Manager</h3>


                </div>
                <div class="row">
                    <div class="col-sm-6 col-lg-1 mb-1"></div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3" style="box-shadow: 1px 5px 8px #3333337d;">
                            <div class="amount">
                                <p id="basicpriceSWM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span  class="discountedPrice">20</span><span> 0</span>--%>
                                </p>
                                <p id="basicvaliditySWM"><%--Free till Mar,2020--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="basicDescSWM" class="product_desc">
                                <h3>Basic</h3>
                                <%--<p>Anyone can automate their work. Start with the basics.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="basic_periodSWM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$20</span> - 750 tasks/month</option>
                      <option><span>$35</span> - 1,500 tasks/month</option>--%>
                                </select>
                                <%-- <p style="border-color: #e1e1e1 !important; border: 1px solid #e1e1e1; border-radius: 0.1875rem; padding: 0.8rem 0.8rem;">100 tasks/month</p>--%>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block " onclick="bascicPlan3()" href="#">Current</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="StarterPriceSWM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span id="" class="discountedPrice">50</span><span> 20</span>--%>
                                </p>
                                <p id="StarterValidateSWM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="StarterDescSWM" class="product_desc">
                                <h3>Starter</h3>
                                <%-- <p>Unleash the power of automation.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="Starter_periodSWM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%--<option><span>$20</span> - 750 tasks/month</option>
                      <option><span>$35</span> - 1,500 tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block"  onclick="StarterPlan3()" href='#'>Upgrade</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="ProfessionalPriceSWM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;"><%--<span class="discountedPrice"> 80</span> <span>50</span>--%>
                                </p>
                                <p id="ProfessionalValidititySWM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="ProfessionalDescSWM" class="product_desc">
                                <h3>Professional</h3>
                                <%--   <p>Advanced tools to take your work to the next level.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="ProfessionalperiodSWM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$50</span> - 2,000 tasks/month</option>
                      <option><span>$90</span> - 5,000 tasks/month</option>
                      <option><span>$130</span> - 10,000 tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="ProfessionalPlan3()" href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="PremiumPriceSWM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span class="discountedPrice">350</span><span> 300</span>--%>
                                </p>
                                <p id="PremiumValiditySWM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="PremiumDescSWM" class="product_desc">
                                <h3>Premium</h3>
                                <%-- <p>Bring your team together to collaborate on automation.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="PremiumperiodSWM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%--<option><span>$300</span> - 50,000 tasks/month</option>
                      <option><span>$500</span> - 1,00,000 tasks/month</option>
                      <option><span>$800</span> - 2,00,000 tasks/month</option>
                      <option><span>$1000</span> - Unlimited tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="PremiumPlan3()" href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="SupportPriceSWM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span style="font-size: 40px; vertical-align: top; font-weight: bold; color: #333;">50</span>--%>
                                </p>
                                <p id="SupportvaliditySWM"><%--hour/day<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="SupportDescSWM" class="product_desc">
                                <h3>Support</h3>
                                <%-- <p>Provide you support dedicatedly</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="SupportperiodSWM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$50</span> - 1 hour/day</option>
                      <option><span>$100</span> - 8 hours/day</option>--%>
                  </select>
                </div>
                <div class="product_plan">
                  <a class="btn btn-dark btn-block" onclick="SupportPlan3()" href="#">Upgrade</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-1 mb-1"></div>
          </div>

<div class="row mt-6">
                    <div class="col-sm-6 col-lg-1 mb-4"></div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">basic plan features</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="not_feature">Automated scheduling</li>
                            <li class="not_feature">Escalations</li>
                            <li class="not_feature">Email trigger</li>
                            <li class="not_feature">Support available</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">starter plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">12 hours automated scheduling</li>
                            <li class="not_feature">Escalations</li>
                            <li class="not_feature">Email Trigger</li>
                            <li class="not_feature">Support available</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">professional plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">6 hours automated scheduling</li>
                            <li class="yes_feature">6 hours escalation emails</li>
                            <li class="not_feature">Email trigger</li>
                            <li class="yes_feature">Free support for 1 month</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">premium plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">1 hour automated scheduling</li>
                            <li class="yes_feature">1 hour escalation emails</li>
                            <li class="yes_feature">1 hour email trigger</li>
                            <li class="yes_feature">Free support for 3 months</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">support plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">Dedicated support</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-1 mb-4"></div>
                </div>

 </div>

                <%-- Start 4th Products  --%>
                
                      <div id="SimplePaymentManger">
                <%-- Start Simple Recruitement manager  --%>
                <div class="container">
                    <h3 class="text-center" style="padding: 3%">Simple Payment Manger</h3>


                </div>
                <div class="row">
                    <div class="col-sm-6 col-lg-1 mb-1"></div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3" style="box-shadow: 1px 5px 8px #3333337d;">
                            <div class="amount">
                                <p id="basicpriceSPM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span  class="discountedPrice">20</span><span> 0</span>--%>
                                </p>
                                <p id="basicvaliditySPM"><%--Free till Mar,2020--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="basicDescSPM" class="product_desc">
                                <h3>Basic</h3>
                                <%--<p>Anyone can automate their work. Start with the basics.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="basic_periodSPM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$20</span> - 750 tasks/month</option>
                      <option><span>$35</span> - 1,500 tasks/month</option>--%>
                                </select>
                                <%-- <p style="border-color: #e1e1e1 !important; border: 1px solid #e1e1e1; border-radius: 0.1875rem; padding: 0.8rem 0.8rem;">100 tasks/month</p>--%>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block " onclick="bascicPlan4()" href="#">Current</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="StarterPriceSPM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span id="" class="discountedPrice">50</span><span> 20</span>--%>
                                </p>
                                <p id="StarterValidateSPM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="StarterDescSPM" class="product_desc">
                                <h3>Starter</h3>
                                <%-- <p>Unleash the power of automation.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="Starter_periodSPM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%--<option><span>$20</span> - 750 tasks/month</option>
                      <option><span>$35</span> - 1,500 tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="StarterPlan4()"  href='#'>Upgrade</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="ProfessionalPriceSPM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;"><%--<span class="discountedPrice"> 80</span> <span>50</span>--%>
                                </p>
                                <p id="ProfessionalValidititySPM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="ProfessionalDescSPM" class="product_desc">
                                <h3>Professional</h3>
                                <%--   <p>Advanced tools to take your work to the next level.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="ProfessionalperiodSPM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$50</span> - 2,000 tasks/month</option>
                      <option><span>$90</span> - 5,000 tasks/month</option>
                      <option><span>$130</span> - 10,000 tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block"  onclick="ProfessionalPlan4()" href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="PremiumPriceSPM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span class="discountedPrice">350</span><span> 300</span>--%>
                                </p>
                                <p id="PremiumValiditySPM"><%--Till Mar,2020<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="PremiumDescSPM" class="product_desc">
                                <h3>Premium</h3>
                                <%-- <p>Bring your team together to collaborate on automation.</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="PremiumperiodSPM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%--<option><span>$300</span> - 50,000 tasks/month</option>
                      <option><span>$500</span> - 1,00,000 tasks/month</option>
                      <option><span>$800</span> - 2,00,000 tasks/month</option>
                      <option><span>$1000</span> - Unlimited tasks/month</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="PremiumPlan4()"  href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-1">
                        <div class="h-100 border border-300 justify-content-between d-flex flex-column rounded px-3 py-3 plans" style="background: #fafafa;">
                            <div class="amount">
                                <p id="SupportPriceSPM">
                                    <img src="../assets/img/dollar.png" style="width: 25px;">
                                    <%--<span style="font-size: 40px; vertical-align: top; font-weight: bold; color: #333;">50</span>--%>
                                </p>
                                <p id="SupportvaliditySPM"><%--hour/day<br/>per month, billed annually--%></p>
                            </div>
                            <hr align="left" width="30%" style="border: 1px solid #333;">
                            <div id="SupportDescSPM" class="product_desc">
                                <h3>Support</h3>
                                <%-- <p>Provide you support dedicatedly</p>--%>
                            </div>
                            <div class="product_period" style="margin-bottom: 20px;">
                                <select id="SupportperiodSPM" class="form-control border-300 d-inline-block text-sans-serif">
                                    <%-- <option><span>$50</span> - 1 hour/day</option>
                      <option><span>$100</span> - 8 hours/day</option>--%>
                                </select>
                            </div>
                            <div class="product_plan">
                                <a class="btn btn-dark btn-block" onclick="SupportPlan4()"  href="#">Upgrade</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-1 mb-1"></div>
                </div>
                <div class="row mt-6">
                    <div class="col-sm-6 col-lg-1 mb-4"></div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">basic plan features</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="not_feature">Automated scheduling</li>
                            <li class="not_feature">Escalations</li>
                            <li class="not_feature">Email trigger</li>
                            <li class="not_feature">Support available</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">starter plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">12 hours automated scheduling</li>
                            <li class="not_feature">Escalations</li>
                            <li class="not_feature">Email Trigger</li>
                            <li class="not_feature">Support available</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">professional plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">6 hours automated scheduling</li>
                            <li class="yes_feature">6 hours escalation emails</li>
                            <li class="not_feature">Email trigger</li>
                            <li class="yes_feature">Free support for 1 month</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">premium plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">1 hour automated scheduling</li>
                            <li class="yes_feature">1 hour escalation emails</li>
                            <li class="yes_feature">1 hour email trigger</li>
                            <li class="yes_feature">Free support for 3 months</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-2 mb-4">
                        <h5 class="mb-3 mt-1">support plan features:</h5>
                        <ul class="text-sans-serif pl-0">
                            <li class="yes_feature">Dedicated support</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-1 mb-4"></div>
                </div>
                          </div>
                <%-- End simple Recruitement Manager  --%>
                <%--End 4th Products section   --%>
                <div class="modal" id="upgrade">
                    <div class="modal-dialog modal-dialog-centered" style="width: 350px;">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Your Order Summary</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <p class="paymentMethod">* Payment Method: <span class="upgradePlans">Paytm</span></p>
                                <p class="selectedPlan">* Selected Plan: <span class="upgradePlans">Basic</span></p>
                                <p class="totalAmountPayable">* Total Amount Payable: <span class="upgradePlans">$30</span></p>
                                <p class="promoCode" style="margin-bottom: 0 !important;">* Promo Code:</p>
                                <div class="promo-code">
                                    <input type="text" name="promo-checkbox" placeholder="Enter your promo code here" checked style="width: 100%;">
                                    <button class="apply">Apply</button>
                                </div>
                                <div class="result">
                                    <a class="btn btn-dark" href="#" style="float: right; margin-top: 8px;">Continue</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
            <!-- end of .container-->
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->
      
      <!-- ============================================-->
    

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0 bg-light" id="studio-contact">

        <div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12 py-8 pr-lg-7">
                <div class="col-12 mb-3 mb-md-5 text-center">
                  <h2 class="fs-3 fs-sm-4"><span class="text-underline">frequently asked questions</span></h2>
                </div>
                <div class="ui styled fluid accordion" data-options='{"exclusive":false}'>
                  <div class="title active">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">What are Tasks and how many do I need?</h5>
                  </div>
                  <div class="content active">
                    <div class="transition visible">
                      <p>A Task is counted every time a Zap successfully moves data or takes action for you. For example, if your Zap has an action to create new Google Contacts, each contact that is created will use one Task.</p>
                      <p>The number of Tasks you need depends on the number of active Zaps you have and how frequently those Zaps will run to automate your workflow. Don’t worry, if you use up all your Tasks in a given month we’ll hold your Tasks until you upgrade or your monthly usage resets. If you use more than 2 million Tasks, you will be billed at a per-task rate of $0.0010.</p>
                    </div>
                  </div>
                  <div class="title">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">What are Apps?</h5>
                  </div>
                  <div class="content">
                    <div class="transition hidden">
                      <p>An App is a web service or application, such as Google Docs, Evernote or Salesforce. Zapier offers integrations for more than 1,500 apps, letting you easily move data between them to automate repetitive tasks.</p>
                    </div>
                  </div>
                  <div class="title">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">Do you have a free trial of your premium features?</h5>
                  </div>
                  <div class="content">
                    <div class="transition hidden">
                      <p>Yes, when you create a new Zapier account, you’re instantly enrolled in a free 14-day trial of the Zapier Professional plan.</p>
                    </div>
                  </div>
                  <div class="title">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">Can I change my plan later?</h5>
                  </div>
                  <div class="content">
                    <div class="transition hidden">
                      <p>Yes, you can upgrade or downgrade at any time. If you choose to upgrade, you'll pay a pro-rated amount for the rest of the month. If you choose to downgrade, you'll be credited on next month's bill.</p>
                    </div>
                  </div>
                  <div class="title">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">What if I decide to cancel?</h5>
                  </div>
                  <div class="content">
                    <div class="transition hidden">
                      <p>If you no longer wish to use Zapier, you may cancel at any time. You will receive a pro-rated credit with Zapier for the remainder of that month's billing cycle. See our full refund policy here.</p>
                    </div>
                  </div>
                  <div class="title">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">Can you charge me in my local currency?</h5>
                  </div>
                  <div class="content">
                    <div class="transition hidden">
                      <p>No, at the moment we only accept payment in United States Dollars (USD). We do accept all major credit/debit cards as well as PayPal.</p>
                    </div>
                  </div>
                  <div class="title">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">Do you offer any discounts?</h5>
                  </div>
                  <div class="content">
                    <div class="transition hidden">
                      <p>Yes! When you choose to pre-pay for a year of service, we’ll give you a 20% discount. We also offer an additional 15% discount for non-profits. Learn more at https://zapier.com/non-profits/.</p>
                    </div>
                  </div>
                  <div class="title">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">Do I need to know how to code?</h5>
                  </div>
                  <div class="content">
                    <div class="transition hidden">
                      <p>Nope! Zapier makes it easy for you to move data between your apps without being a developer. We may need you to locate information for certain accounts, but we'll guide you through any additional steps required and our support champions are here to help you by email should you need assistance.</p>
                    </div>
                  </div>
                  <div class="title">
                    <i class="dropdown icon"></i>
                    <h5 class="d-inline-block fs-0">What if I have more questions?</h5>
                  </div>
                  <div class="content">
                    <div class="transition hidden">
                      <p>Try searching for what you need in our Help & Support site. If you don't find what you're looking for, you can email us at contact@zapier.com for more help.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="text-sans-serif" id="contact">

        <div class="container">
          <div class="row">
            <div class="col-lg-4 pr-lg-5">
              <h3 class="mb-3">C Simplify IT</h3>
              <p>We are a specialized Global IT services & solution organization established in 2011, with a team of 120+ consulting & engineering professionals spread across its delivery centers at India (Gurugram), USA (Fremont, CA) . We are a specialist IT Consulting firm, leveraging innovation & passion at its best, C Simplify IT is trusted by Fortune 500 companies and start-ups to help business to add value across our clients worldwide. Our clients are based in USA, Singapore, Malaysia, Switzerland, UK, Hong Kong, and India.</p>
            </div>
            <div class="col-md-6 col-lg-4 pr-lg-6 ml-lg-auto mt-6 mt-lg-0">             
              <h3 class="mb-3">Socials</h3>
              <p class="mb-0">Stay connected with C Simplify IT via our social media pages. If you haven't taken the opportunity to visit our social media pages, please do so. It's a great way to interact with us, get your questions answered and make suggestions.</p>
              <br/>
              <a class="btn btn-dark btn-sm mr-2" href="https://www.facebook.com/CSimplifyIT-210115279023481/" target="_blank"><span class="fab fa-facebook-f"></span></a>
              <a class="btn btn-dark btn-sm" href="https://in.linkedin.com/company/c-simplify-it-services-private-limited" target="_blank"><span class="fab fa-linkedin-in"></span></a>
            </div>
            <div class="col-md-6 col-lg-4 mt-6 mt-lg-0 office-address">
              <h3 class="mb-3">Get in touch</h3>
              <address><b><u>India Head Office:</u></b>
                <br/>Plot No. 52, II Floor,
                <br/> Sector 32, Gurgaon (Haryana) - 122003
                <br/><b>Call Us:</b> <a href="tel:+91 98999762274">+91 9899976227</a> (For Tech Queries),<br /><a href="tel:+91 8950230044">+91 8950230044</a> (For Business Queries)
                <br/><b>E-Mail:</b> <a href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
                <br/><br/><b><u>US Head Office:</u></b>
                <br/>46878 Fernald St, Fremont, CA 94539, USA
                <br/><b>Call Us:</b>  <a href="tel:+1 408 987 5597">+1 408 987 5597</a>
                <br/><b>E-Mail:</b> <a href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
              </address>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

 </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!--===============================================-->
    <!--    Footer-->
    <!--===============================================-->
    <footer class="footer bg-black text-600 py-4 text-sans-serif text-center overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-4 order-lg-2">
            <a class="indicator indicator-up js-scroll-trigger" href="#top"><span class="indicator-arrow indicator-arrow-one" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":0.9}'></span><span class="indicator-arrow indicator-arrow-two" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":1.05}'></span></a>
          </div>
          <div class="col-lg-4 text-lg-left mt-4 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              Copyright &copy; 2018 C Simplify IT</p>
          </div>
          <div class="col-lg-4 text-lg-right order-lg-2 mt-2 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              <a class="text-600" href="TOS.aspx">Terms of Service</a>
            </p>
          </div>
        </div>
      </div>
    </footer>


    <!--===============================================-->
    <!--    Modal for language selection-->
    <!--===============================================-->
    <div class="remodal bg-black remodal-select-language" data-remodal-id="language">
      <div class="remodal-close" data-remodal-action="close"></div>
      <ul class="list-unstyled pl-0 my-0 py-4 text-sans-serif">
        <li>
          <a class="pt-1 d-block text-white font-weight-semi-bold" href="#">English</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="#">Français</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="page-rtl.html">عربى</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="#">Deutsche</a>
        </li>
      </ul>
    </div>


    
    

</body>
</html>

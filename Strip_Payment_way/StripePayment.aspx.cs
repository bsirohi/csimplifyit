﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using csimplifyit.DBContext;
using csimplifyit.Model;
using csimplifyit.MsgFormatter;
//using Newtonsoft.JsonResult;
using System.Web.Services;
using Newtonsoft.Json.Linq;

namespace csimplifyit.Strip_Payment_way
{
    public partial class StripePayment : System.Web.UI.Page
    {
        private static csitEntities1 db = new csitEntities1();
        private static Dictionary<string, object> moResponse;
        public string stripePublishableKey = WebConfigurationManager.AppSettings["StripePublishableKey"];
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack ) {
             // var x= getSubSription();
            }
        }

        [WebMethod]
        public static object getALlSubSriptionDetails()
        {
            try
            {

                Dictionary<string, object> obResult = new Dictionary<string, object>();

               var getSubscriptionName = db.subscription_plan.Where(s => s.status == 1).Select(d=>new { d.spId, d.planDescription, d.planAutoRenewalDescription,d.subscriptionProduct } ).ToList();
           
                var getall = "select ssp.spid as spid, ssp.planDescription as planDescription, ssps.planFeaturesCode as planFeaturesCode,ssp.planAutoRenewalDescription as planName, "
                           + " ssps.planValue1 as DiscountValue, ssps.planValue2 as ActualValue, ssps.spsId as spsId,ssp.subscriptionProduct as Producttype from subscription_plan ssp "
                           + " join subscription_plan_settings ssps on ssp.spId = ssps.associatedPlanId "
                           +" where ssp.status = 1 and ssps.status = 1 and ssps.planFeaturesCode in(1001,1002,1003) order by ssp.spId asc ";
                var Result = db.Database.SqlQuery<getSubscription>(getall).ToList();
                obResult.Add("SubScriptionPlan", getSubscriptionName);
                obResult.Add("SubScriptionPlanDetails", Result);

                moResponse = RespMsgFormatter.formatResp(1, "Successfully Execute", obResult, "SaveDocumentClass");               
                return moResponse;
            }
            catch(Exception ex)
            {
                 moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:"+ex.InnerException +"Message:"+ex.Message+"Stack Trace:"+ex.StackTrace, null, "SaveDocumentClass");
                return moResponse;
            }        
        }



        [WebMethod]
        public static object canInitiateForPayment(string requestData)
        {
            try
            {

                JObject jobt = JObject.Parse(requestData);
                int PlanID = 0;
                bool lbSuccess = false;
                Login.Login log = new Login.Login();
                long UserID = log.getUserId();
                if (UserID == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, "Session Time Out. Please Login!", null, "canInitiateForPayment");
                    return moResponse;
                }

                if (jobt["PlanId"]!=null && jobt["PlanId"].ToString()!="" && jobt["PlanId"].ToString()!=string.Empty)
                {
                    lbSuccess = int.TryParse(jobt["PlanId"].ToString(),out PlanID);
                    if(lbSuccess==false)
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "Please first Select Plan", null, "canInitiateForPayment");
                        return moResponse;
                    }
                    else
                    {
                        lbSuccess = true;
                    }
                }

                var getPlan = db.subscription_plan.Where(s => s.spId == PlanID && s.status == 1).FirstOrDefault();

                if (getPlan != null)
                {
                    // var Result = db.user_subscriptions.Where(s => s.associatedPlanId == liSubScriptionId && s.userId == UserID && s.status==1 && s.isPlanAutoRenewal==true && s).ToList();
                    DateTime dt = DateTime.Now.Date;
                    var Result = db.user_subscriptions.Join(db.subscription_plan,
                              us => us.associatedPlanId,
                              sp => sp.spId,
                              (us, sp) => new { us, sp }).Where(s => s.us.userId == UserID && s.us.status == 1 && s.us.associatedPlanId == PlanID
                              && s.sp.subscriptionProduct == getPlan.subscriptionProduct && s.us.isPlanAutoRenewal == true && (s.us.subscriptionEnd) > dt).ToList();

                    if (Result.Count > 0)
                    {
                        var PlanName = Result.Select(s => s.sp.planName).LastOrDefault();
                        var FromDate= Result.Select(s => s.us.subscriptionOn).LastOrDefault();
                        var ToDate= Result.Select(s => s.us.subscriptionEnd).LastOrDefault();
                        moResponse = RespMsgFormatter.formatResp(0, "User already have "+ PlanName +"From Date "+ FromDate.Date +" To Date "+ ToDate.Date, null, "canInitiateForPayment");
                        return moResponse;
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "Plan is not exist", null, "canInitiateForPayment");
                    return moResponse;
                }


                var getSubscriptionName = db.subscription_plan.Where(s => s.status == 1 && s.spId== PlanID).Select(d => new { d.spId, d.planDescription, d.planAutoRenewalDescription }).ToList();

                moResponse = RespMsgFormatter.formatResp(1, "Successfully Execute", getSubscriptionName, "canInitiateForPayment");
                return moResponse;
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:" + ex.InnerException + "Message:" + ex.Message + "Stack Trace:" + ex.StackTrace, null, "canInitiateForPayment");
                return moResponse;
            }
        }

        [WebMethod]
        public static object getstripeKey()
        {
            try
            {
                var Result = db.codelkups.Where(s => s.LkUpCode == "PUBLISHABLE_KEY" && s.KeyCode == 0).FirstOrDefault();
                moResponse = RespMsgFormatter.formatResp(1, "Successfully Execute", Result, "getstripeKey");
                return moResponse;
            }
            catch(Exception ex)
            {               
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception"+ex.InnerException +"Stack Trace"+ex.StackTrace, null, "getstripeKey");
                return moResponse;
            } 
        }

        protected void setLoggedInUserDetails(UserInfo userInfo)
        {
            if (Session == null)
            {
                var sSession = System.Web.HttpContext.Current.Session;
                sSession["userInfo"] = userInfo;
            }
            else if (Session["userInfo"] == null)
            {
                Session["userInfo"] = userInfo;
            }
        }

       

    }
}
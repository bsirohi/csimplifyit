(function() {
  if (gapi && gapi.hangout) {

    var initHangout = function(apiInitEvent) {
      if (apiInitEvent.isApiReady) {
	 var avatar =  gapi.hangout.getParticipantId();
gapi.hangout.av.setAvatar(avatar,"https://csimplifyit.com/hangout/mask_freshervilla.png");
      }
    };

    gapi.hangout.onApiReady.add(initHangout);
  }
})();
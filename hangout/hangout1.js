var KEY_NAME = "WebServiceName";
var KEY_URL = "WebServiceParameterURL";
var KEY_DATA = "WebServicePostData";
var KEY_DESCRIPTION = "WebServiceDescription";
getParameters = function(){
var ret = {};
var queryString = window.location.search.substring(1);
var params = queryString.split('&');
for(var co=0; co<params.length; co++){
var keyValue = params[co].split("=");
ret[keyValue[0]] = unescape(keyValue[1]);
}
return ret;
};

var param = getParameters();
    param = param["gd"];
    param = param.split('-');
var token = param[0];
var userId = param[1];
    userId = parseInt(userId);

var GET_SKILLS = "getSkills";
var GET_REFERENCEMATERIAL = "getSuggestedVideos";
var GET_SMS = "sendSMSUsingTwilio";
var GET_TOKEN = "Token";
var GET_COMPANIES = "getCompaniesByName";
var GET_TESTFORSKILL = "getTestForSkill";

var text = $('#Search_content').val();

var web_services = [
	{
  		"WebServiceName":"getSkills",
  		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getSkills/",
  		"WebServicePostData":{"inputData":"{'meta': {'token': '%d','userID': '%e', 'role':'admin'},'data':[{'skill':'%s'}]}"},
  		"WebServiceDescription":"Search Skills"
        },
	{
                "WebServiceName":"getSuggestedVideos",
  		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getSuggestedVideos/",
  		"WebServicePostData":{"inputData":"{'meta': {'token': '%d','userID': '%e', 'role':'admin'},'data':[{'forValue':'%u','itemType':'%i'}]}"},
  		"WebServiceDescription":"Get Suggested Videos for Company or SKills"
        },
	 {
  		"WebServiceName":"sendSMSUsingTwilio",
  		"WebServiceParameterURL":"https://use-this.appspot.com/",
                "WebServicePostData":{"inputData":"{'meta': {'token': '%d' },'data':[{'contactNo':'%$','message':'%@'}]}"},
  		"WebServiceDescription":"Send message through twilio"
  	},
	{
		"WebServiceName":"Token",
		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getToken/",
		"WebServicePostData":{"inputData":"{'meta':{'token': 0,'action' : 'actionName','validation': 'runthisvalidationonserver','previousaction': 'actionName'},'data':[{'userName': '%p','userPassword': '%q'}]}"},
		"WebServiceDescription":"Web Service to get token"
	},
	{
  		"WebServiceName":"getCompaniesByName",
  		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getCompaniesByName/",
  		"WebServicePostData":{"inputData":"{'meta': {'token': '%d','userID': '%e', 'role':'admin'},'data':[{'company':'%s'}]}"},
  		"WebServiceDescription":"Search Skills"
        },
	{
  		"WebServiceName":"getTestForSkill",
  		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getSkillTest/",
  		"WebServicePostData":{"inputData":"{'meta': {'token': '%d','userID': '%e', 'role':'admin'},'data':[{'skillid':'6'}]}"},
  		"WebServiceDescription":"Get Skill Tests"


	}

];

function loadDataInGridForSkillAndComapnySearch(){
 	        				$("#Skill").html("<table id='SkillList'></table>");
						jqGridForSkillOrCompany();
 	        						
 	        			}

function Search(serviceName) {

	var web_service_json;
	
	for (var i = 0; i < web_services.length; i++) {

		var object = web_services[i];
		
		if(object[KEY_NAME] == serviceName){
			web_service_json = object;
			break;
		}
		
	}
	
	return web_service_json;
}


//For New Generated Token

var token_ = null;
var uid = null;

function logIN() {
	
	var serviceObj = Search(GET_TOKEN);
	var dataToBeModified = serviceObj[KEY_DATA];
	var userName = $('#username').val();
	var password = $('#password').val();
        var inputJsonString = JSON.stringify(dataToBeModified);
	var finalDataInput = inputJsonString.replace('%p',userName);
        finalDataInput = finalDataInput.replace('%q',password);
	var getSkillData = serviceObj[KEY_DATA];


$.ajax({
    url: serviceObj[KEY_URL],
    contentType: 'application/json',
    type: 'POST',
    data: finalDataInput,
    success: function(result){

	var response1 = JSON.parse(result);
	var skillList = response1['results'];
	token_ = skillList[0].token;
        uid = skillList[0].uid;
	uid = parseInt(uid);
       $(document).ready(function(){
       $("#context1").hide();
       $("#holder").show();
       });

	
	var dataToPost = {  "inputData": "{'meta': {'token': '%d','action':'' ,'actionName':'', 'validation': 'runthisvalidationonserver', 'previousaction':'', 'actionName':'' },'data':[{'registeruser':'%e','flag':'0' }]}"
          };

dataToPost = JSON.stringify(dataToPost).replace('%d',token_)
dataToPost = dataToPost.replace('%e',uid);

$.ajax({
    url: "https://csimplifyit.com/freshersvilla.svc/getRefrenceFriends/",
    contentType: 'application/json',
    type: 'POST',
    data: dataToPost,
    success: function(result){
	
		var response1 = JSON.parse(result);
		var friendList1 = response1['results'];
		jQuery("#FriendList").jqGrid({
		datatype:'jsonstring',
 	        datastr:friendList1,
		    colNames:['Friends','UserName'],
 	     	colModel:[
                           {name:'displayName', index:'displayName', hidden:false},
			   {name:'userName' , index:'userName',hidden:true}
                          ],
		jsonReader:{repeatitems:false},
		rowNum:20,
		width:$("#main").width(),
		height:'15%',
		rowList:[5,10],
		scrollOffset:true,
		pager: $('#Friends'),
		viewrecords: true,
		//caption: "Refrences"
		onSelectRow:function(){
 	        			var sel = $(this).jqGrid('getGridParam','selrow');
					/*Extract username from cell*/
						var username = $(this).jqGrid('getCell',sel,'userName');					
 	      					showPopupDialogForInvitingFriends(username);
						sendMessage();
					}
		});
		//jQuery("#FriendList").jqGrid('navGrid','#Friends',{edit:false,add:false,del:false});
			
}
});

}

});

}

(function() {
$(document).ready(function(){
    $("#holder_").hide();
});


})();


//For Companies
var inputJsonStringCompany = null;
var finalDataInputCompany = null;
var luvid = null;
function getCompanies() {
	
	var serviceObj = Search(GET_COMPANIES);
	var dataToBeModified = serviceObj[KEY_DATA];
	var text = $('#Search_content').val();
        if (param[0] == undefined || param[0] == "undefined")
        {
        inputJsonStringCompany = JSON.stringify(dataToBeModified);
	finalDataInputCompany = inputJsonStringCompany.replace('%s',text);
        finalDataInputCompany = finalDataInputCompany.replace('%d',token_);
        finalDataInputCompany = finalDataInputCompany.replace('%e',uid);
        }
	 else
        {	
        inputJsonStringCompany = JSON.stringify(dataToBeModified);
	finalDataInputCompany = inputJsonStringCompany.replace('%s',text);
        finalDataInputCompany = finalDataInputCompany.replace('%d',token);
        finalDataInputCompany = finalDataInputCompany.replace('%e',userId);
	}
	var getSkillData = serviceObj[KEY_DATA];


$.ajax({
    url: serviceObj[KEY_URL],
    contentType: 'application/json',
    type: 'POST',
    data: finalDataInputCompany,
    success: function(result){

	var response1 = JSON.parse(result);
	var skillList = response1['results'];
        jQuery("#SkillList").jqGrid({
		datatype: 'jsonstring',
		datastr: skillList,
		colNames:['Id','References'],
 	     	colModel:[ {name:'cid', index:'cid', classes:'id', hidden:true},
                           {name:'companyName', index:'companyName'}
                          ],
		jsonReader:{repeatitems:false},
		width:$("#main").width(),
		height:'15%',
		rowNum:5,
		rowList:[5,10],
		scrollOffset:true,
		pager: $('#Skills'),
		viewrecords: true,
		onSelectRow: function(){
			var sel = $(this).jqGrid('getGridParam','selrow');
 	        	/*Extract luvid from cell*/
 			cid = $(this).jqGrid('getCell', sel, 'cid');
 			/*Value to be passed for skill*/
 			var skill = 'C';
			$("#Skill").html('<table border="1" align="centre" id="URLList"></table>');
			getReferenceMaterial(cid,skill);
			}
		});
		//jQuery("#SkillList").jqGrid('navGrid','#Skills',{edit:false,add:false,del:false});
			}
	
});
}


//For Skills
var inputJsonString = null;
var finalDataInput = null;

function getSkills() {
	
	var serviceObj = Search(GET_SKILLS);
	var dataToBeModified = serviceObj[KEY_DATA];
	var text = $('#Search_content').val();
        if (param[0] == undefined || param[0] == "undefined")
        {
        inputJsonString = JSON.stringify(dataToBeModified);
	finalDataInput = inputJsonString.replace('%s',text);
        finalDataInput = finalDataInput.replace('%d',token_);
        finalDataInput = finalDataInput.replace('%e',uid);
        }
	 else
        {	
        inputJsonString = JSON.stringify(dataToBeModified);
	finalDataInput = inputJsonString.replace('%s',text);
        finalDataInput = finalDataInput.replace('%d',token);
        finalDataInput = finalDataInput.replace('%e',userId);
	}
	var getSkillData = serviceObj[KEY_DATA];

$.ajax({
    url: serviceObj[KEY_URL],
    contentType: 'application/json',
    type: 'POST',
    data: finalDataInput,
    success: function(result){

	var response1 = JSON.parse(result);
	var skillList = response1['results'];
        jQuery("#SkillList").jqGrid({
		datatype: 'jsonstring',
		datastr: skillList,
		colNames:['Id','References'],
 	     	colModel:[ {name:'luvid', index:'luvid', classes:'id', hidden:true},
                           {name:'luvValueDisplayName', index:'luvValueDisplayName'}
                         ],
		jsonReader:{repeatitems:false},
		width:$("#main").width(),
		height:'15%',
		rowNum:5,
		rowList:[5,10],
		scrollOffset:true,
		pager: $('#Skills'),
		viewrecords: true,
		onSelectRow: function(){
			var sel = $(this).jqGrid('getGridParam','selrow');
 	         	/*Extract luvid from cell*/
 			luvid = $(this).jqGrid('getCell', sel, 'luvid');
 			/*Value to be passed for skill*/
 			var skill = 'S';
			$("#Skill").html('<table border="1" align="centre" id="URLList"></table>');
			getReferenceMaterial(luvid,skill);
			
			}
		});
	//	jQuery("#SkillList").jqGrid('navGrid','#Skills',{edit:false,add:false,del:false});
			}
	
});
}


//For Test
var inputJsonStringTest = null;
var finalDataInputTest = null;

function getTest() {
	
	var serviceObj = Search(GET_TESTFORSKILL);
	var dataToBeModified = serviceObj[KEY_DATA];
	var uidd = uid;
	uid = parseInt(uidd);
        inputJsonStringTest = JSON.stringify(dataToBeModified);
        finalDataInputTest = inputJsonStringTest.replace('%e',uid);
		finalDataInputTest = finalDataInputTest.replace('%d',token_);
        
	var getSkillData = serviceObj[KEY_DATA];

$.ajax({
    url: serviceObj[KEY_URL],
    contentType: 'application/json',
    type: 'POST',
    data: finalDataInputTest,
    success: function(result){

	var response1 = JSON.parse(result);
	var testList = response1['results'];
        questionList = testList[0].tmquestions;
	console.log(questionList);
        jQuery("#TestList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#tableWidth").width(),
   	colNames:['Id','Questions'],
   	colModel:[
   		{name:'qid', index:'qid', classes:'qid', hidden:true},
                           {name:'question', index:'question'}		
   	]
});

for (var i = 0, iLen = questionList.length; i < iLen; ++i){
//for (var i = 0; i < b; ++i){

if (b === i-1)
  {
	jQuery("#TestList").jqGrid('addRowData',b,questionList[b]);
    
}
}

}
});
}

var questionList = null;
function callTest() {
//$("#ShowTest").html("<div style='width: 600;float: center;margin: 0px auto;height: inherit;'><div><div class='mainTest'>&nbsp;&nbsp;Freshervilla Test<div style='float: right;'><button class='commandButton' style='border-radius: 40px;color: white;background-color: #AFAFAF;font: inherit;height: 35;font-family: none;font-size: 16;' id='instruction_btn'>Instructions</button></div></div></div><div style='box-shadow:#eee 0.7px 0.7px 15px inset;float:left;'><div id='context'></div><div style='height:330px;'><div id='ShowTest'>&nbsp&nbsp&nbsp&nbsp<table id='TestList'></table></div><div id='ShowOption'>&nbsp&nbsp&nbsp&nbsp<table id='OptionList'></table></div></div><div style='float:left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></div><div id='flag' style='float:right;display:none;'><img src='https://csimplifyit.com/hangout/flag.png'>&nbsp;&nbsp;&nbsp;&nbsp;</div><div><div style='height:40;float:right;border-top:red 3px solid;width: 560px;padding: 20px;margin-top:-8px;'><div style='float: right;'><button class='commandButton' id='previous_btn'>Previous</button><button class='commandButton' id='next_btn'>Next</button><button id='flagButton' class='commandButton' onclick='showFlag()'>Flag</button><button class='commandButton'>Review</button></div><div id='progressbar' style='width:250;border-radius:10;'><div id='progress-label' class='progress-label'></div></div></div></div></div>");
$("#ShowTest1").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table border="1" align="centre" id="TestList"></table>');
for(var a = 0; a < 2; a++){
        jQuery("#TestList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#tableWidth").width(),
   	colNames:['Id','Questions'],
   	colModel:[
   		{name:'qid', index:'qid', classes:'qid', hidden:true},
                           {name:'question', index:'question'}		
   	]
});
}
for (var i = 0, iLen = questionList.length; i < iLen; ++i){

if (b === i-1)
  {

    jQuery("#TestList").jqGrid('addRowData',b,questionList[b]);
    
}
}
}


//For Option
var inputJsonStringOption = null;
var finalDataInputOption = null;
var optionList = null;
function getOption() {
	
	var serviceObj = Search(GET_TESTFORSKILL);
	var dataToBeModified = serviceObj[KEY_DATA];
	var uidd = uid;
	uid = parseInt(uidd);
        inputJsonStringOption = JSON.stringify(dataToBeModified);
        finalDataInputOption = inputJsonStringOption.replace('%e',uid);
        finalDataInputOption = finalDataInputOption.replace('%d',token_);
	var getSkillData = serviceObj[KEY_DATA];

$.ajax({
    url: serviceObj[KEY_URL],
    contentType: 'application/json',
    type: 'POST',
    data: finalDataInputOption,
    success: function(result){

	var response1 = JSON.parse(result);
	var testList = response1['results'];
        optionList = testList[0].tmanswersoptions;
	console.log(optionList);
        jQuery("#OptionList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#tableWidth").width(),
   	colNames:['Id','QuestionID','isCorrectAnswer','answerOptionType','answerOptionMimeType','Options'],
   	colModel:[
			{name:'aoid', index:'aoid', classes:'aoid', hidden:true},
                     {name:'associatedQuestionID', index:'associatedQuestionID', hidden:true},
 	        		   {name:'isCorrectAnswer', index:'isCorrectAnswer', hidden:true},
 	        	       {name:'answerOptionType',index:'answerOptionType',hidden:true},
 	                   {name:'answerOptionMimeType',index:'answerOptionMimeType',hidden:true},
			 {name:'answerOptionText', index:'answerOptionText'},
 	                   
   	]
});

for (var i = 0, iLen = optionList.length; i < 4; ++i){

	jQuery("#OptionList").jqGrid('addRowData',b,optionList[i]);

}

}
});
}

var questionList = null;
var o = 0;
var questionList = null;
function callOption() {
     $("#ShowOption").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<table border="1" align="centre" id="OptionList"></table>');
	 for(var a = 0; a < 2; a++){
        jQuery("#OptionList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#tableWidth").width(),
   	colNames:['Id','QuestionID','isCorrectAnswer','answerOptionType','answerOptionMimeType','Options'],
   	colModel:[
			{name:'aoid', index:'aoid', classes:'aoid', hidden:true},
                     {name:'associatedQuestionID', index:'associatedQuestionID', hidden:true},
 	        		   {name:'isCorrectAnswer', index:'isCorrectAnswer', hidden:true},
 	        	       {name:'answerOptionType',index:'answerOptionType',hidden:true},
 	                   {name:'answerOptionMimeType',index:'answerOptionMimeType',hidden:true},
			 {name:'answerOptionText', index:'answerOptionText'},
 	                   
   	]
});
}
var op = o+4;
//for (var i = 0, iLen = op; i < iLen; ++i){
for (var i = o;i < op; ++i){

//for(var c = o; c < op; c++)
 // {

    jQuery("#OptionList").jqGrid('addRowData',i,optionList[i]);
    
//}
}
}


//function nextQuestion() {
//var b;
//$("#next_btn").click(function(){
	//										b++;  
		//									});

//}

function jqGridForSkillOrCompany()
{
/*read the prference of user for search*/
    var pref =  $('.search_criteria').val();
        pref = parseInt(pref);
if(pref == 1)
{
getCompanies();
}
else
{
getSkills();
}
}

var inputJsonString1 = null;

function getReferenceMaterial(luvid,skill) {

 var serviceObj1 = Search(GET_REFERENCEMATERIAL);
 var dataToBeModified1 = serviceObj1[KEY_DATA];

if (param[0] == undefined || param[0] == "undefined")
   {
     
     inputJsonString1 = JSON.stringify(dataToBeModified1);
     inputJsonString1 = inputJsonString1.replace('%d',token_);
     inputJsonString1 = inputJsonString1.replace('%u',luvid);
     inputJsonString1 = inputJsonString1.replace('%i',skill);
     inputJsonString1 = inputJsonString1.replace('%e',uid);

 $.ajax({
	 url: serviceObj1[KEY_URL],
	 contentType: 'application/json',
	 type: 'POST',
	 data: inputJsonString1,
	 success: function(result) {
		 var response2 = JSON.parse(result);
		var urlList = response2['results'];
		jQuery("#URLList").jqGrid({
		datatype:'jsonstring',
 	        datastr:urlList,
		colNames:['Id','ReferenceType','Author','RmStatus','RmUrl','SearchTag','UrlFilterRefId','Views','References'],
 	     	colModel:[ {name:'rmID', index:'rmID', classes:'id', hidden:true},
 	        		   {name:'refrenceType', index:'refrenceType', hidden:true},
 	        	       {name:'author',index:'author',hidden:true},
 	    	           {name:'rmStatus', index:'rmStatus',hidden:true},
 	                   {name:'rmUrl',index:'rmUrl',hidden:true},
 	                   {name:'searchTag',index:'searchTag',hidden:true},
 	        	       {name:'urlFilterRefId',index:'urlFilterRefId',hidden:true},
 	        	       {name:'viewCount',index:'viewCount',hidden:true},
 	        	       {name:'rmSubject', index:'rmSubject', hidden:false}       
                         ],
		jsonReader:{repeatitems:false},
		rowNum:5,
		width:$("#main").width(),
		height:'15%',
		rowList:[5,10],
		scrollOffset:true,
		pager: $('#Skills'),
		viewrecords: true,
		//caption: "Refrences",
		onSelectRow: function(){
											var sel = $(this).jqGrid('getGridParam','selrow');
 	        								/*Extract rmID from cell*/
 							 				var rmid = $(this).jqGrid('getCell', sel, 'rmID');
 							 				var rmStatus = $(this).jqGrid('getCell', sel, 'rmStatus');
 							 				var rmUrl = $(this).jqGrid('getCell', sel, 'rmUrl');
 							 				var rmSubject = $(this).jqGrid('getCell', sel, 'rmSubject');

 							 				/*Video Key in url*/
 							 				var videoKey = 'v';
 							 				/*path to show in popup*/
 							 				var path = 'https://www.youtube.com/embed/';
 							 				
 							 				/*Extract video id from url and configure
 							 				 * new url
 							 				 */
 							 				var videoId = getVideoIdFromUrl(videoKey, rmUrl);
 							 				
											var newPath = path+videoId;

 							 				showPopupDialogForVideoCapture(rmid, rmStatus, newPath);
											
 							 				
		}
		});
		//jQuery("#URLList").jqGrid('navGrid','#Skills',{edit:false,add:false,del:false});
		 
	}
 });


   }
else
{
     inputJsonString1 = JSON.stringify(dataToBeModified1);
     inputJsonString1 = inputJsonString1.replace('%d',token);
     inputJsonString1 = inputJsonString1.replace('%u',luvid);
     inputJsonString1 = inputJsonString1.replace('%i',skill);
     inputJsonString1 = inputJsonString1.replace('%e',userId);

 $.ajax({
	 url: serviceObj1[KEY_URL],
	 contentType: 'application/json',
	 type: 'POST',
	 data: inputJsonString1,
	 success: function(result) {
		 var response2 = JSON.parse(result);
		var urlList = response2['results'];
		jQuery("#URLList").jqGrid({
		datatype:'jsonstring',
 	        datastr:urlList,
		colNames:['Id','ReferenceType','Author','RmStatus','RmUrl','SearchTag','UrlFilterRefId','Views','References'],
 	     	colModel:[ {name:'rmID', index:'rmID', classes:'id', hidden:true},
 	        		   {name:'refrenceType', index:'refrenceType', hidden:true},
 	        	       {name:'author',index:'author',hidden:true},
 	    	           {name:'rmStatus', index:'rmStatus',hidden:true},
 	                   {name:'rmUrl',index:'rmUrl',hidden:true},
 	                   {name:'searchTag',index:'searchTag',hidden:true},
 	        	       {name:'urlFilterRefId',index:'urlFilterRefId',hidden:true},
 	        	       {name:'viewCount',index:'viewCount',hidden:true},
 	        	       {name:'rmSubject', index:'rmSubject', hidden:false}       
                         ],
		jsonReader:{repeatitems:false},
		rowNum:5,
		width:$("#main").width(),
		height:'15%',
		rowList:[5,10],
		scrollOffset:true,
		pager: $('#Skills'),
		viewrecords: true,
		//caption: "Refrences",
		onSelectRow: function(){
											var sel = $(this).jqGrid('getGridParam','selrow');
 	        								/*Extract rmID from cell*/
 							 				var rmid = $(this).jqGrid('getCell', sel, 'rmID');
 							 				var rmStatus = $(this).jqGrid('getCell', sel, 'rmStatus');
 							 				var rmUrl = $(this).jqGrid('getCell', sel, 'rmUrl');
 							 				var rmSubject = $(this).jqGrid('getCell', sel, 'rmSubject');

											gapi.hangout.data.setValue("subject",rmSubject);

 							 				/*Video Key in url*/
 							 				var videoKey = 'v';
 							 				/*path to show in popup*/
 							 				var path = 'https://www.youtube.com/embed/';
 							 				
 							 				/*Extract video id from url and configure
 							 				 * new url
 							 				 */
 							 				var videoId = getVideoIdFromUrl(videoKey, rmUrl);
 							 				
											var newPath = path+videoId;

 							 				gapi.hangout.data.setValue("videoUrl",newPath);
											
 							 				
		}
		});
		//jQuery("#URLList").jqGrid('navGrid','#Skills',{edit:false,add:false,del:false});
		 
	}
 });
}
 }

/*Function to get parameters from url*/
 function getVideoIdFromUrl(key, url){
         						
 key = key.replace(/[\[/,"\\\[").replace(/[\]]/,"\\\]");
 	        						
 var regexP = "[\\?&]"+key+"=([^&#]*)";
        						
 var regex = new RegExp(regexP);
         						
 var results = regex.exec(url);
 	        						
 if(results == null)
 return "";
else
 return decodeURIComponent(results[1].replace(/\+/g," "));
 }

	var b = -1;
/*Function to show the dialog ui for video play*/
 	        					function showPopupDialogForVideoCapture(rmID, rmStatus, url){
 	        					
 	        						jQuery("#frame").append("<div id='dialog_popup' title='Take My Job Skill Video'></dialog>");
 	        					
 	        						               var RmID = rmID;
 	        							       var RmStatus = rmStatus;
 	        							       var RmUrl = url;
										console.log(RmID);console.log(RmStatus);console.log(RmUrl);
 	        						jQuery("#dialog_popup").html("<p>hello</p>");
 	        						
 	        						$('#dialog_popup').html("<div id='dialog_container' style='width:100%;height:100%;margin:0;position:absolute;'><div id='video_container'><input id=\"RmID\" type=\"hidden\" name=\"RmID\" value=\""+RmID+"\"></input><input id=\"RmStatus\" type=\"hidden\" name=\"RmStatus\" value=\""+RmStatus+"\"></input><div id='ShowTest'><iframe id=\"video_frame\" src=\""+RmUrl+"\" style='width:95%;height:80%;display:block;'></iframe></div></div><div id='bottom_bar' align = 'center'><button id='test_btn' width='25%'height='20%'>Test</button></div></div>");
 	        								
 	        								//$('#video_frame').target(url);
 	        								
 	        								$('#test_btn').click(function(){
											
 	        									$("#video_frame").hide();
											$("#ShowTest").html("<div style='width: 600;float: center;margin: 0px auto;height: inherit;'><div><div class='mainTest'>&nbsp;&nbsp;Freshervilla Test<div style='float: right;'><button class='commandButton' style='border-radius: 40px;color: white;background-color: #AFAFAF;font: inherit;height: 35;font-family: none;font-size: 16;' id='instruction_btn'>Instructions</button></div></div></div><div style='box-shadow:#eee 0.7px 0.7px 15px inset;float:left;'><div id='context'></div><div id='tableWidth' style='height:330px;width:400px;'><div id='ShowTest1'>&nbsp&nbsp&nbsp&nbsp<table id='TestList'></table></div><div id='ShowOption'>&nbsp&nbsp&nbsp&nbsp<table id='OptionList'></table></div></div><div style='float:left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></div><div id='flag' style='float:right;display:none;'><img src='https://csimplifyit.com/hangout/flag.png'>&nbsp;&nbsp;&nbsp;&nbsp;</div><div><div style='height:40;float:right;border-top:red 3px solid;width: 560px;padding: 20px;margin-top:-8px;'><div style='float: right;'><button class='commandButton' id='previous_btn'>Previous</button><button class='commandButton' id='next_btn'>Next</button><button id='flagButton' class='commandButton' onclick='showFlag()'>Flag</button><button class='commandButton'>Review</button></div><div id='progressbar' style='width:250;border-radius:10;'><div id='progress-label' class='progress-label'></div></div></div></div></div>");
											getTest();
											getOption();
											b = b+1;
							     			$("#test_btn").hide();
											$(document).ready(function(){
                                               $('#next_btn').click(function(){
													console.log("hello");
													
													callTest();
													b = b+1;
													callOption();
													o = o+4;
                                                });
												$("#previous_btn").click(function(){
													b = b-1;   
													callTest();
													o = o-4;
													callOption();
													// var question_length = questionList.length;
													// var option_length = optionList.length; 
													//	if ( b == 0 && o == 0){
													//  b = question_length - 1;
													//  o = option_length;
													//  }										
												});
												$('#instruction_btn').click(function(){
												showPopupDialogForTest();
												});
});

												});
												

 	        								$('#dialog_popup').dialog({
 	        									autoOpen:true,
 	        									minWidth:800,
 	        									minHeight:500,
 	        									show:{
 	        										effect:'Scale',
 	        										duration:1000
 	        									},
 	        									close:function(){
 	        										$(this).dialog("close");
 	        										$(this).remove();
 	        									}
 	        								});
 	        						}


//For Test
function showFlag()
{
elem = document.getElementById('flag');
if (elem.style.display == 'block')
{
elem.style.display = 'none';
}
else
{
elem.style.display = 'block';
}
}

function showPopupDialogForTest(){
 	        					
 	        						jQuery("#dialog_show").append("<div id='dialog_popup' title='Freshervilla Test'></dialog>");
 	        					
										
 	        						jQuery("#dialog_popup").html("<p>hello</p>");
 	        						
 	        						$('#dialog_popup').html("<div id='dialog_container' style='width:100%;height:100%;margin:0;position:absolute;'>Instruction</div>");
 	        								
 	        								//$('#video_frame').target(url);
 	        								
 	        							
 	        								
 	        								$('#dialog_popup').dialog({
 	        									autoOpen:true,
 	        									minWidth:800,
 	        									minHeight:500,
 	        									show:{
 	        										effect:'Scale',
 	        										duration:1000
 	        									},
 	        									close:function(){
 	        										$(this).dialog("close");
 	        										$(this).remove();
 	        									}
 	        								});
 	        						}


var out = null;
var amount = 2701;
function GetCount(iid){

	amount = amount-1;
	var time = amount;   

	// if time is already past
	if(amount < 0){
		document.getElementById(iid).innerHTML="FINISH!";
	}
	// else date is still good
	else{

		days=0;hours=0;mins=0;secs=0;out="";

		mins=Math.floor(amount/60);//minutes
		amount=amount%60;

		secs=Math.floor(amount);//seconds
		out += mins +" "+((mins==1)?"min":"mins")+": ";
		out += secs +" "+((secs==1)?"sec":"secs")+", ";
		out = out.substr(0,out.length-2);
		document.getElementById(iid).innerHTML=out;
               amount = time;
		setTimeout(function(){GetCount(iid)}, 1000);
	}
}

window.onload=function(){
	GetCount('progress-label');

};

  $(function() {
    var progressbar = $( "#progressbar" ),
      progressLabel = $( ".progress-label" );
 
    progressbar.progressbar({
 //     value: false,
      complete: function() {
        progressLabel.text( "Finish" );
      }
    });

    function progress() {
      var val = progressbar.progressbar( "value" ) ;
      progressbar.progressbar( "value", val+1);
      
      if ( val > -1 && val < 99 ) {
        setTimeout( progress, 27000 );
      }
    }
 
    setTimeout( progress, 27000 );
  });									
									

//For FriendList
function friendList() {
   // $("#myplayer").attr('src',"https://www.youtube.com/embed/R6sU9eGuv7U");
   
var dataToPost = {  "inputData": "{'meta': {'token': '%d','action':'' ,'actionName':'', 'validation': 'runthisvalidationonserver', 'previousaction':'', 'actionName':'' },'data':[{'registeruser':'%e','flag':'0' }]}"
          };
if (param[0] == undefined || param[0] == "undefined")
{
$(document).ready(function(){
    $("#holder").hide();
     $("#context1").show();
});

}
else
{

dataToPost = JSON.stringify(dataToPost).replace('%d',token)
dataToPost = dataToPost.replace('%e',userId);

$.ajax({
    url: "https://csimplifyit.com/freshersvilla.svc/getRefrenceFriends/",
    contentType: 'application/json',
    type: 'POST',
    data: dataToPost,
    success: function(result){
	
		var response1 = JSON.parse(result);
		var friendList1 = response1['results'];
		jQuery("#FriendList").jqGrid({
		datatype:'jsonstring',
 	        datastr:friendList1,
		    colNames:['Friends','UserName'],
 	     	colModel:[
                           {name:'displayName', index:'displayName', hidden:false},
			   {name:'userName' , index:'userName',hidden:true}
                          ],
		jsonReader:{repeatitems:false},
		rowNum:20,
		width:$("#main").width(),
		height:'15%',
		rowList:[5,10],
		scrollOffset:true,
		pager: $('#Friends'),
		viewrecords: true,
		//caption: "Refrences"
		onSelectRow:function(){
 	        			var sel = $(this).jqGrid('getGridParam','selrow');
					/*Extract username from cell*/
						var username = $(this).jqGrid('getCell',sel,'userName');					
 	      					showPopupDialogForInvitingFriends(username);
						sendMessage();
					}
		});
	//	jQuery("#FriendList").jqGrid('navGrid','#Friends',{edit:false,add:false,del:false});
			
}
});
}
}

		function sendMessage(){

		var phoneNo = "+918800337921";
		var hangoutUrl = gapi.hangout.getHangoutUrl();
		var message = "Vishal invites you to freshersVilla Hangout :"+ hangoutUrl;
				var serviceObj2 = Search(GET_SMS);
 				var dataToBeModified2 = serviceObj2[KEY_DATA];
 				var inputJsonString2 = JSON.stringify(dataToBeModified2);
     				inputJsonString2 = inputJsonString2.replace('%d',token);
     				inputJsonString2 = inputJsonString2.replace('%$',phoneNo);
     				inputJsonString2 = inputJsonString2.replace('%@',message);

 $.ajax({
	 url: serviceObj2[KEY_URL],
	 contentType: 'application/json',
	 type: 'POST',
	 data: inputJsonString2,
	 success: function(result) {
		 var response2 = JSON.parse(result);
		var urlList = response2['result'];
			},
	error: function(xhr, errorType, exception){
		var err = exception || xhr.statusText;
		console.log(err);
		alert("there is an error");
		}
 });



		}


							/*Function to show the dialog ui for inviting friend*/
 	        					function showPopupDialogForInvitingFriends(username){
								console.log(username);
								var r = confirm("Are You want to send invitation to: "+username);
								
								if(r==true)
								{
									window.location.href = "mailto:"+"vishalgupta921@gmail.com"+"?subject="+"Hangout"+"&body="+"hello";

								}
								jQuery("#main").append("<div id='dialog_popup' title='Take My Job Skill Video'></dialog>");
								}




(function(){
//event call when participant add
gapi.hangout.onParticipantsAdded.add(function(ParticipantsAddedEvent){
	
	var participant = ParticipantsAddedEvent.addedParticipants;
	console.log(participant);
	/*var myId = gapi.hangout.getParticipantId();
	var enabledParticipant = gapi.hangout.getEnabledParticipants();
	for(var x = 0, xLen = enabledParticipant.length; x < xLen; x++) {
		var a = enabledParticipant[x];
	 	console.log(a);
		console.log(a.id);
		
        for (var i = 0, iLen = participant.length; i < iLen; ++i) {

	var p = participant[i];
	if( p.id === myId)	{
			return;
			console.log("id same");
		}
	else {*/
		var state = gapi.hangout.data.getState();
	        var url = state.videoUrl;
		if(url == undefined)
		{
		}		
		else {
			var metadata = gapi.hangout.data.getStateMetadata(state);
		       //var time = metadata.videoUrl.timediff;
		       var video = url+"?rel=0&start="+"&autoplay=1";
		       $("#myplayer").attr('src',video);
		      }
		//}
           // }
	  //}
	});
})();


(function() {
  if (gapi && gapi.hangout) {

    var initHangout = function(apiInitEvent) {
      if (apiInitEvent.isApiReady) {
	friendList();
	
        gapi.hangout.data.onStateChanged.add(function(StateChangedEvent){
		
		console.log("hello onStateChangedEvent call");
		var state = StateChangedEvent.state;
	        var url = state.videoUrl;
		var videoSubject = state.subject;
		if(url == undefined)
		{
		}		
		else {
		var metadata = StateChangedEvent.metadata;
		var time = metadata.videoUrl.timediff;
		var video = url+"?rel=0&start="+time+"&autoplay=1";
		document.getElementById("showVideo").innerHTML= videoSubject;
		$("#myplayer").attr('src',video);
		      }
		
	});

	

	gapi.hangout.data.onMessageReceived.add(function(MessageReceivedEvent){
		var id = MessageReceivedEvent.senderId;
		console.log("senderId is: "+id);
		var msg = MessageReceivedEvent.message;
		console.log("message is: "+msg);
		});
 	gapi.hangout.onApiReady.add(function(ApiReadyEvent){
		var show = ApiReadyEvent.isApiReady;
		console.log(show);
	});
	
	var load = gapi.hangout.wasAutoLoaded();
	console.log(load);

        gapi.hangout.onApiReady.remove(initHangout);
      }
    };

    gapi.hangout.onApiReady.add(initHangout);
  }
})();
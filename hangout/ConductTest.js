var showarray = null;
var correctresult = null;
var showAnswerjqgrid = [{questionid : null,
						optionid: null,
						answertext: null,
						correct: null}];
var KEY_NAME = "WebServiceName";
var KEY_URL = "WebServiceParameterURL";
var KEY_DATA = "WebServicePostData";
var KEY_DESCRIPTION = "WebServiceDescription";

var userId = 12015;
    uid = parseInt(userId);

var GET_SKILLS = "getSkills";
var GET_REFERENCEMATERIAL = "getSuggestedVideos";
var GET_SMS = "sendSMSUsingTwilio";
var GET_TOKEN = "Token";
var GET_COMPANIES = "getCompaniesByName";
var GET_TESTFORSKILL = "getTestForSkill";
var SEND_ANSWER = "saveCandidateAnswersData";

var text = $('#Search_content').val();

var web_services = [
	{
  		"WebServiceName":"getSkills",
  		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getSkills/",
  		"WebServicePostData":{"inputData":"{'meta': {'token': '%d','userID': '%e', 'role':'admin'},'data':[{'skill':'%s'}]}"},
  		"WebServiceDescription":"Search Skills"
        },
	{
                "WebServiceName":"getSuggestedVideos",
  		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getSuggestedVideos/",
  		"WebServicePostData":{"inputData":"{'meta': {'token': '%d','userID': '%e', 'role':'admin'},'data':[{'forValue':'%u','itemType':'%i'}]}"},
  		"WebServiceDescription":"Get Suggested Videos for Company or SKills"
        },
	 {
  		"WebServiceName":"sendSMSUsingTwilio",
  		"WebServiceParameterURL":"https://use-this.appspot.com/",
                "WebServicePostData":{"inputData":"{'meta': {'token': '%d' },'data':[{'contactNo':'%$','message':'%@'}]}"},
  		"WebServiceDescription":"Send message through twilio"
  	},
	{
		"WebServiceName":"Token",
		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getToken/",
		"WebServicePostData":{"inputData":"{'meta':{'token': 0,'action' : 'actionName','validation': 'runthisvalidationonserver','previousaction': 'actionName'},'data':[{'userName': '%p','userPassword': '%q'}]}"},
		"WebServiceDescription":"Web Service to get token"
	},
	{
  		"WebServiceName":"getCompaniesByName",
  		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getCompaniesByName/",
  		"WebServicePostData":{"inputData":"{'meta': {'token': '%d','userID': '%e', 'role':'admin'},'data':[{'company':'%s'}]}"},
  		"WebServiceDescription":"Search Skills"
        },
	{
  		"WebServiceName":"getTestForSkill",
  		"WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/getSkillTest/",
  		"WebServicePostData":{"inputData":"{'meta': {'token': 'dXNlclBhc3N3b3JkODE3NzY0NzgxNzc2NDc=','userID': '%e', 'role':'admin'},'data':[{'skillid':'6'}]}"},
  		"WebServiceDescription":"Get Skill Tests"


	},
	{
        "WebServiceName":"saveCandidateAnswersData",
        "WebServiceParameterURL":"https://csimplifyit.com/freshersvilla.svc/saveCandidateAnswersData/",
        "WebServicePostData":{"inputData":"{'meta': {'token': 'dXNlclBhc3N3b3JkODE3NzY0NzgxNzc2NDc=','userID': '%e', 'role':'admin'},'data':%s}"},
        "WebServiceDescription":"Save candidate answer data."
  }

];


function Search(serviceName) {

	var web_service_json;
	
	for (var i = 0; i < web_services.length; i++) {

		var object = web_services[i];
		
		if(object[KEY_NAME] == serviceName){
			web_service_json = object;
			break;
		}
		
	}
	
	return web_service_json;
}



function showPopupDialogForTest(){
 	        					
 	        						jQuery("#context").append("<div id='dialog_popup' title='Freshervilla Test'></dialog>");
 	        					
										
 	        						jQuery("#dialog_popup").html("<p>hello</p>");
 	        						
 	        						$('#dialog_popup').html("<div id='dialog_container' style='width:100%;height:100%;margin:0;position:absolute;'>Instruction</div>");
 	        								
 	        								//$('#video_frame').target(url);
 	        								
 	        							
 	        								
 	        								$('#dialog_popup').dialog({
 	        									autoOpen:true,
 	        									minWidth:800,
 	        									minHeight:400,
 	        									show:{
 	        										effect:'Scale',
 	        										duration:1000
 	        									},
 	        									close:function(){
 	        										$(this).dialog("close");
 	        										$(this).remove();
 	        									}
 	        								});
 	        						}


//For Test
var inputJsonStringTest = null;
var finalDataInputTest = null;
var testSubject = null;
function getTest() {
	
	var serviceObj = Search(GET_TESTFORSKILL);
	var dataToBeModified = serviceObj[KEY_DATA];
	var uidd = uid;
	uid = parseInt(uidd);
        inputJsonStringTest = JSON.stringify(dataToBeModified);
        finalDataInputTest = inputJsonStringTest.replace('%e',uid);
        
	var getSkillData = serviceObj[KEY_DATA];

$.ajax({
    url: serviceObj[KEY_URL],
    contentType: 'application/json',
    type: 'POST',
    data: finalDataInputTest,
    success: function(result){
 
	var response1 = JSON.parse(result);
	var testList = response1['results'];
        questionList = testList[0].tmquestions;
		optionList = testList[0].tmanswersoptions;
		console.log(optionList);
		var subject = testList[0].tmtests;
		testSubject = subject[0].testSubject;
		
	//	document.getElementById('testName').innerHTML = testSubject;
		$('#testName').text("                                                  "+testSubject);
	console.log(questionList);
        jQuery("#TestList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#ShowTest").width(),
   	//colNames:['Id','Questions'],
   	colModel:[
			
   		{name:'qid', index:'qid', classes:'qid', hidden:true},
                           {name:'question', index:'question'}		
   	]
	
});
$('#TestList_question').hide();
var showques =  incdecQues+1;
 document.getElementById('questionNumber').innerHTML=showques+"/"+questionList.length;
for (var i = 0, iLen = questionList.length; i < iLen; ++i){
//for (var i = 0; i < incdecQues; ++i){

if (incdecQues === i-1)
  {
	jQuery("#TestList").jqGrid('addRowData',incdecQues,questionList[incdecQues]);
    
}
}
  getOption();
}
});
}

var questionList = null;
function callTest() {
$("#ShowTest").html('&nbsp&nbsp&nbsp&nbsp&nbsp <table border="1" align="centre" id="TestList"></table>');
//for(var a = 0; a < 2; a++){
        jQuery("#TestList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#ShowTest").width(),
   	colNames:['Id','Questions'],
   	colModel:[
	            
   		{name:'qid', index:'qid', classes:'qid', hidden:true},
                           {name:'question', index:'question'}		
   	]
});
//}
incdecQues=incdecQues+1;
$('#TestList_question').hide();
for (var i = 0, iLen = questionList.length; i <= iLen; ++i){

if (incdecQues === i)
  {

    jQuery("#TestList").jqGrid('addRowData',incdecQues,questionList[incdecQues]);
    
}
}
}

function previousTest() {
$("#ShowTest").html('&nbsp&nbsp&nbsp&nbsp&nbsp <table border="1" align="centre" id="TestList"></table>');
//for(var a = 0; a < 2; a++){
        jQuery("#TestList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#ShowTest").width(),
   	colNames:['Id','Questions'],
   	colModel:[
	            
   		{name:'qid', index:'qid', classes:'qid', hidden:true},
                           {name:'question', index:'question'}		
   	]
});
//}
incdecQues=incdecQues-1;
$('#TestList_question').hide();
for (var i = 0, iLen = questionList.length; i <= iLen; ++i){

if (incdecQues === i)
  {

    jQuery("#TestList").jqGrid('addRowData',incdecQues,questionList[incdecQues]);
    
}
}
}


 /*var showAnswer = [{questionid: null,
            answerid: null
		}];*/
		var showAnswer = [{associatedQuestionId: null,
                    assocuatedAnswerOptionID: null,
					answerText: null,
					attemptID: 2,
					associatedTestID: 6,
					timeInSecondsAfterTest: 30,
					jobID: 2
				}];
  var array = null;
  var arrayFlag = null;
  var arFlag = [{questionid: null,
                 flagvalue: null }];
var incdecQues = -1;
$(document).ready(function(){
$("#test").click(function(){
       $('#previous_btn').hide();
       $("#ShowResult").hide();
	   $("#showcalculatedResult").hide();
       $("#next_btn").show();
       $("#finish_btn").hide();
	   $("#review_btn").hide();
	   $("#result_btn").hide();
elem = document.getElementById('flag');
elem.style.display = 'none';
flagValue = 0;
   getTest();
   
   	GetCount('progress-label');
   incdecQues = incdecQues+1;
   oldRowid = incdecQues;
  newRowid = "question"+oldRowid;
  $("#" + oldRowid).attr("id", newRowid);
  $("#test").hide();		  
  $("#jqgh_OptionList_answerOptionText").hide();
  document.getElementById('previous_btn').disabled=true;
  //document.getElementById("gbox_TestList").disabled=true;
  });
  var b = [];
  $("#next_btn").click(function(){
  $('#previous_btn').show();
  document.getElementById('previous_btn').disabled=false;
  if(incdecQues == questionList.length-2) {
    // document.getElementById('next_btn').disabled=true;
	//  $("#button_btn").html("<button id='previous_btn' class='commandButton'>Previous</button><button id='finish_btn' class='commandButton'>Finish</button><button id='flagButton' class='commandButton' onclick='showFlag()'>Flag</button><button class='commandButton'>Review</button>");
	   $("#next_btn").hide();
       $("#finish_btn").show();
    }else{}
   $('#showAnswer').hide();
   var questionListLength = questionList.length;
   questionListLength = questionListLength - 1;
    elem = document.getElementById('flag');
elem.style.display = 'none';

   callTest();
 //  incdecQues = incdecQues+1;
   callOption();
   o = o+4;
   showPreviousAnswer();
   showPreviousFlag();
   flagValue = 0;
   //var question_length = questionList.length;
   //var option_length = optionList.length;
   //if ( incdecQues == question_length && o == option_length){
   //incdecQues = 0;
   //o = 0;
  // }
			oldRowid = incdecQues;
  newRowid = "question"+oldRowid;
  $("#" + oldRowid).attr("id", newRowid);	
 var showques =  incdecQues+1;
 document.getElementById('questionNumber').innerHTML=showques+"/"+questionList.length;
});
$("#previous_btn").click(function(){
$("#finish_btn").hide();
$("#review_btn").hide();
       $("#next_btn").show();
if(incdecQues == 1) {
     $('#previous_btn').hide();
    }
document.getElementById("next_btn").disabled=false;
$('#showAnswer').hide();
//var length = showAnswer.length;
/*for(var i = 0; i < length;i++){
console.log("question id is: "+showAnswer[i].questionid);
var id = showAnswer[i].questionid;
if(showAnswer[i].questionid == questionList[id-1].qid){
console.log("hello");
break;
}
}*/

//if(incdecQues < questionList.length)
  //document.getElementById("next_btn").disabled=false;
elem = document.getElementById('flag');
elem.style.display = 'none';

   //incdecQues = incdecQues-1;   
previousTest();
  // o = o-4;
   previousOption();
  showPreviousAnswer();
  showPreviousFlag();
flagValue = 0;
                                
   // var question_length = questionList.length;
  // var option_length = optionList.length; 
//	if ( incdecQues == 0 && o == 0){
 //  incdecQues = question_length - 1;
 //  o = option_length;
 //  }	
		oldRowid = incdecQues;
  newRowid = "question"+oldRowid;
  $("#" + oldRowid).attr("id", newRowid);	
 var showques =  incdecQues+1;
 document.getElementById('questionNumber').innerHTML=showques+"/"+questionList.length;  
  });
$('#instruction_btn').click(function(){
											showPopupDialogForTest();
											});
$('#close_btn').click(function(){
									
						var close = confirm("Are you sure?");
								
								if(close==true)
								{
								    open(location, '_self').close();
								
								}
});	

$('#result_btn').click(function(){
							$("#showcalculatedResult").show();
						for (var i = 0; i<showAnswer.length;i++){
                        for(var j = 0;j<optionList.length;j++){
						if (showAnswer[i].assocuatedAnswerOptionID == optionList[j].aoid){
						if (optionList[j].isCorrectAnswer == 1){
						showarray = {questionid : showAnswer[i].associatedQuestionId,
									optionid: showAnswer[i].assocuatedAnswerOptionID,
									answertext: showAnswer[i].answerText,
									correct: 'Correct'
									};
									correctresult = correctresult + 1;
						showAnswerjqgrid.unshift(showarray);}
						else
						{
						showarray = {questionid : showAnswer[i].associatedQuestionId,
									optionid: showAnswer[i].assocuatedAnswerOptionID,
									answertext: showAnswer[i].answerText,
									correct: 'Incorrect'
									};
						showAnswerjqgrid.unshift(showarray);
						}
						}}}
						var calculation = (correctresult/questionList.length)*100;
						calculation = parseInt(calculation);
						document.getElementById('showcalculatedResult').innerHTML="Your Result is: "+calculation+"%";
									//showAnswerjqgrid.pop();
						/*for(var j = 0;j<questionList.length;j++){
						for(var i = 0;i<showAnswerjqgrid.length;i++){
							if( showAnswerjqgrid[i].questionid == questionList[j].qid){
							break;
							}
							else{
							      showarray = {questionid : j,
									optionid: " ",
									answertext: " ",
									correct: 'Incorrect'
									};
							    showAnswerjqgrid.unshift(showarray); 
								if(showAnswerjqgrid.length == questionList.length){break;}
							}
						}}*/
						
						$("#ShowTest").hide();
						$("#ShowOption").hide();
                        $("#showAnswer").hide();
						$("#ShowResult").show();
						
						for(var wholeQuestion1 = 0;wholeQuestion1<questionList.length;wholeQuestion1++){
   if(showAnswer.length != 0){
	for(var wholeQuestion = 0;wholeQuestion<showAnswer.length;wholeQuestion++){
		
		if(showAnswer[wholeQuestion].associatedQuestionId == questionList[wholeQuestion1].qid){
				break;
		}
		else{
				var question = wholeQuestion1+1;
		        showarray = {questionid : question,
									optionid: " ",
									answertext: " ",
									correct: 'Did Not Answer'
									};
						         showAnswerjqgrid.unshift(showarray);
								 break;
		}
		}}else
		{
		var question = wholeQuestion1+1;
		        showarray = {questionid : question,
									optionid: " ",
									answertext: " ",
									correct: 'Did Not Answer'
									};
						         showAnswerjqgrid.unshift(showarray);
								 break;
		}
	}
						showAnswerjqgrid.pop();
						for(var c = 0;c<11;c++){
	for(var i = 0; i<showAnswerjqgrid.length;i++){
	for(var j=i+1;j<showAnswerjqgrid.length;j++){
	if(showAnswerjqgrid[i].questionid == showAnswerjqgrid[j].questionid){
		b=j;
		console.log(b);
		showAnswerjqgrid.splice(i,1);
		//break;
	}}}	}
						
						
						
						//for result grid
						jQuery("#ResultList").jqGrid({
	datatype: "local",
	height: 250,
	width: 600,
   	colNames:['Question No.','Your Answer','Result'],
   	colModel:[
                     {name:'questionid', index:'questionid',align:"center"},
					 {name:'answertext', index:'answertext',align:"center",hidden:true},
 	        		   {name:'correct', index:'correct',align:"center", edittype: 'image', formatter: imageFormatter },
 	                   
   	],
   	caption: "Result",
	onSelectRow: function() {
	grid.jqGrid('ResultList');
     return false;
     }
});


function imageFormatter(cellvalue, options, row) 
  {
   if (cellvalue == 'Incorrect') 
   {
    return "<img src='https://csimplifyit.com/hangout/redflag.png' height='30'>";

   }
   if (cellvalue == 'Correct') 
   {
    return "<img src='https://csimplifyit.com/hangout/greenflag.png' height='30'>";

   }
   if (cellvalue == 'Did Not Answer') 
   {
    return "<img src='https://csimplifyit.com/hangout/blueflag.png' height='30'>";

   }
  }


var v = 0
while (v<showAnswerjqgrid.length)
  {
  jQuery("#ResultList").jqGrid('addRowData',v+1,showAnswerjqgrid[v]);
  v++;
  }
				document.getElementById("result_btn").disabled=true;		
						
});	


$('#review_btn').click(function(){
							
						$("#ShowTest").show();
						$("#ShowOption").show();
                        $("#showAnswer").show();
						$("#ShowResult").hide();
						$("#previous_btn").show();
						
});	


										
											
		$("#finish_btn").click(function(){
		
		                    
var finish = confirm("Are you finished with your question?\nTime remaining:   "+out+"\nYou can review in this time");
								
								if(finish==true)
								{
								$grid = $("#OptionList");
								var $gbox = $grid.closest('.ui-jqgrid');
            /*$gbox.block({
                message: "<h1>Being processed...</h1>",
                theme: true,
                themedCSS: {
                    width: "33%",
                    left: "33%",
                    border: "3px solid #a00"
                }
            });
            setTimeout(function () {
                $gbox.unblock();
            }, 3000);*/
			jQuery("#OptionList").jqGrid({
			onSelectRow: function() {
    return false;
}
			});
								$("#finish_btn").hide();
								$("#review_btn").show();
								$("#result_btn").show();
								$("#flagButton").hide();
							    $("#previous_btn").hide();
								$("#progressbar").hide();
								$("#progress-label").hide();
								
  showAnswer.pop();
  for(var c = 0;c<11;c++){
	for(var i = 0; i<showAnswer.length;i++){
	for(var j=i+1;j<showAnswer.length;j++){
	if(showAnswer[i].associatedQuestionId == showAnswer[j].associatedQuestionId){
		b=j;
		console.log(b);
		showAnswer.splice(j,1);
		//break;
	}}}	}
	document.getElementById("finish_btn").disabled=true;
	document.getElementById("flagButton").disabled=true;
	document.getElementById("OptionList").disabled=true;
	/*for(var wholeQuestion1 = 0;wholeQuestion1<questionList.length;wholeQuestion1++){
   if(showAnswer.length != 0){
	for(var wholeQuestion = 0;wholeQuestion<showAnswer.length;wholeQuestion++){
		
		if(showAnswer[wholeQuestion].associatedQuestionId == questionList[wholeQuestion1].qid){
				break;
		}
		else{
				var question = wholeQuestion1+1;
		        array = {associatedQuestionId: question,
                                     assocuatedAnswerOptionID: " ",
					                 answerText: " ",
					                 attemptID: 2,
					                 associatedTestID: 6,
					                 timeInSecondsAfterTest: 30,
					                 jobID: 2
					                 };
						         showAnswer.unshift(array);
								 break;
		}
		}}else
		{
		var question = wholeQuestion1+1;
		        array = {associatedQuestionId: question,
                                     assocuatedAnswerOptionID: " ",
					                 answerText: " ",
					                 attemptID: 2,
					                 associatedTestID: 6,
					                 timeInSecondsAfterTest: 30,
					                 jobID: 2
					                 };
						         showAnswer.unshift(array);
								 break;
		}
	}*/
	sendAnswer();
	finishvar = 0;
	//$('#OptionList').disabled="disabled";
		}
});											
											
											
});

//For Option
var inputJsonStringTest1 = null;
var finalDataInputTest1 = null;
var optionList = null;
function getOption() {
	
	
//$.ajax({
         $("#ShowOption").html('&nbsp&nbsp&nbsp&nbsp&nbsp <table border="1" align="centre" id="OptionList"></table>');
	 for(var a = 0; a < 2; a++){
        jQuery("#OptionList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#ShowTest").width(),
   	colNames:['Actions','Id','QuestionID','IsCorrectAnswer','AnswerOptionType','AnswerOptionMimeType','Options'],
   	colModel:[
	            {name:'act',index:'act', width:75,sortable:false},
			{name:'aoid', index:'aoid', classes:'aoid', hidden:true},
                     {name:'associatedQuestionID', index:'associatedQuestionID', hidden:true},
 	        		   {name:'isCorrectAnswer', index:'isCorrectAnswer', hidden:true},
 	        	       {name:'answerOptionType',index:'answerOptionType',hidden:true},
 	                   {name:'answerOptionMimeType',index:'answerOptionMimeType',hidden:true},
			 {name:'answerOptionText', index:'answerOptionText'},
 	                   
   	],
	gridComplete: function(){
		var ids = jQuery("#OptionList").jqGrid('getDataIDs');
		for(var i=0;i < ids.length;i++){
			var cl = ids[i];
			be = "<button class='buttons' id='"+cl+"'onclick='changeColor()'></button>";
		//	be = "<input style='height:22px;width:20px;' type='button' class='buttons' value='E' onclick=\"jQuery('#OptionList').editRow('"+cl+"');\"  />"; 
			//se = "<input style='height:22px;width:20px;' type='button' class='buttons' value='S' onclick=\"jQuery('#OptionList').saveRow('"+cl+"');\"  />"; 
			//ce = "<input style='height:22px;width:20px;' type='button' class='buttons' value='C' onclick=\"jQuery('#OptionList').restoreRow('"+cl+"');\" />"; 
			jQuery("#OptionList").jqGrid('setRowData',i,{act:be});
		}	
	},
	onSelectRow:function(){
	                       var sel = $(this).jqGrid('getGridParam','selrow');
	                       var option = $(this).jqGrid('getCell',sel,'answerOptionText');
 	        		        answerId = $(this).jqGrid('getCell',sel,'aoid');
						    questionId = $(this).jqGrid('getCell',sel,'associatedQuestionID');
						   console.log(questionId+answerId);
						// $('#h').css("background-color", "blue");
						   arr = jQuery.makeArray(option);
						   
						  for (i = 0; i<showAnswer.length;i++){
						      if(showAnswer[i].associatedQuestionId == questionId){
							    showAnswer[i].assocuatedAnswerOptionID = answerId;
								showAnswer[i].answerText = option;
							//	break;
							  }
							  }
							  
							  for (i = 0; i<showAnswer.length;i++){
							  
							 if(showAnswer[i].associatedQuestionId != questionId){
							     array = {associatedQuestionId: questionId,
                                     assocuatedAnswerOptionID: answerId,
					                 answerText: option,
					                 attemptID: 2,
					                 associatedTestID: 6,
					                 timeInSecondsAfterTest: 30,
					                 jobID: 2
					                 };
						         showAnswer.unshift(array);
								 break;
								 }}
		
						   $('#showAnswer').show();
						$('#showAnswer').text("Your Selected Answer is: "+arr);
						var ansoid = showAnswer[i].assocuatedAnswerOptionID;
						var ansoid1 = ansoid - 1;
						 var property = $('.buttons');
                        for( i =0;i<4;i++){
	                    if(property[i].id == ansoid1){
		                property[i].style.backgroundColor = 'blue';}
						else{
						property[i].style.backgroundColor = 'red';
					//	document.getElementById(ansoid-1).style.color = "black";
						//document.getElementById(ansoid-1).style.background = "white";
						}
                       }

					}
});
}
$('#OptionList_act').hide();
$('#OptionList_answerOptionText').hide();
var op = o;
//for (var i = 0, iLen = op; i < iLen; ++i){
for (var i = 0;i < op; ++i){

//for(var c = o; c < op; c++)
 // {
  jQuery("#OptionList").jqGrid('addRowData',i,optionList[i]);
    
//}
}
//});
}

						
var questionList = null;
var o = 4;
var questionList = null;
function callOption() {
     var op = o+4;

	 $("#ShowOption").html('&nbsp&nbsp&nbsp&nbsp&nbsp <table border="1" align="centre" id="OptionList"></table>');
	 for(var a = 0; a < 2; a++){
        jQuery("#OptionList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#ShowTest").width(),
   	colNames:['Actions','Id','QuestionID','IsCorrectAnswer','AnswerOptionType','AnswerOptionMimeType','Options'],
   	colModel:[
	            {name:'act',index:'act', width:75,sortable:false},
			{name:'aoid', index:'aoid', classes:'aoid', hidden:true},
                     {name:'associatedQuestionID', index:'associatedQuestionID', hidden:true},
 	        		   {name:'isCorrectAnswer', index:'isCorrectAnswer', hidden:true},
 	        	       {name:'answerOptionType',index:'answerOptionType',hidden:true},
 	                   {name:'answerOptionMimeType',index:'answerOptionMimeType',hidden:true},
			 {name:'answerOptionText', index:'answerOptionText'},
 	                   
   	],
	gridComplete: function(){
		var ids = jQuery("#OptionList").jqGrid('getDataIDs');
		for(var i=o;i < op;i++){
			var cl = ids[i];
			be = "<button class='buttons' id='"+i+"' onclick='changeColor()'></button>";
		//	be = "<input style='height:22px;width:20px;' type='button' class='buttons' value='E' onclick=\"jQuery('#OptionList').editRow('"+cl+"');\"  />"; 
			//se = "<input style='height:22px;width:20px;' type='button' class='buttons' value='S' onclick=\"jQuery('#OptionList').saveRow('"+cl+"');\"  />"; 
			//ce = "<input style='height:22px;width:20px;' type='button' class='buttons' value='C' onclick=\"jQuery('#OptionList').restoreRow('"+cl+"');\" />"; 
			jQuery("#OptionList").jqGrid('setRowData',i,{act:be});
		}	
	},
	onSelectRow:function(){
	                       var sel = $(this).jqGrid('getGridParam','selrow');
	                       var option = $(this).jqGrid('getCell',sel,'answerOptionText');
 	        		        answerId = $(this).jqGrid('getCell',sel,'aoid');
						    questionId = $(this).jqGrid('getCell',sel,'associatedQuestionID');
						   console.log(questionId+answerId);
						// $('#h').css("background-color", "blue");
						   arr = jQuery.makeArray(option);
						   /*array = {questionid: questionId,
                                    answerid: answerId
									};
                           showAnswer.unshift(array);*/
						   for (i = 0; i<showAnswer.length;i++){
						      if(showAnswer[i].associatedQuestionId == questionId){
							    showAnswer[i].assocuatedAnswerOptionID = answerId;
								showAnswer[i].answerText = option;
							//	break;
							  }
							  }
							  
							  for (i = 0; i<showAnswer.length;i++){
							  
							 if(showAnswer[i].associatedQuestionId != questionId){
							     array = {associatedQuestionId: questionId,
                                     assocuatedAnswerOptionID: answerId,
					                 answerText: option,
					                 attemptID: 2,
					                 associatedTestID: 6,
					                 timeInSecondsAfterTest: 30,
					                 jobID: 2
					                 };
						         showAnswer.unshift(array);
								 break;
								 }}
							  
						   $('#showAnswer').show();
						$('#showAnswer').text("Your Selected Answer is: "+arr);
						var ansoid = showAnswer[i].assocuatedAnswerOptionID;
						var ansoid1 = ansoid-1;
						var property = $('.buttons');
                        for( i =0;i<4;i++){
	                    if(property[i].id == ansoid1){
		                property[i].style.backgroundColor = 'blue';}
						else{
						property[i].style.backgroundColor = 'red';
						}
                       }

						

					}
});
}
$('#OptionList_act').hide();
$('#OptionList_answerOptionText').hide();
//for (var i = 0, iLen = op; i < iLen; ++i){
for (var i = o;i < op; ++i){

//for(var c = o; c < op; c++)
 // {

    jQuery("#OptionList").jqGrid('addRowData',i,optionList[i]);
    
//}
}
}

function previousOption() {
     $("#ShowOption").html('&nbsp&nbsp&nbsp&nbsp&nbsp <table border="1" align="centre" id="OptionList"></table>');
	 var c = o-4;
	 c = c-4;
	 var op = c+4;
	 for(var a = 0; a < 2; a++){
	    jQuery("#OptionList").jqGrid({
	datatype: "local",
	height: 'auto',
	width: $("#ShowTest").width(),
   	colNames:['Actions','Id','QuestionID','IsCorrectAnswer','AnswerOptionType','AnswerOptionMimeType','Options'],
   	colModel:[
	            {name:'act',index:'act', width:75,sortable:false},
			{name:'aoid', index:'aoid', classes:'aoid', hidden:true},
                     {name:'associatedQuestionID', index:'associatedQuestionID', hidden:true},
 	        		   {name:'isCorrectAnswer', index:'isCorrectAnswer', hidden:true},
 	        	       {name:'answerOptionType',index:'answerOptionType',hidden:true},
 	                   {name:'answerOptionMimeType',index:'answerOptionMimeType',hidden:true},
			 {name:'answerOptionText', index:'answerOptionText'},
 	                   
   	],
	gridComplete: function(){
		var ids = jQuery("#OptionList").jqGrid('getDataIDs');
		for(var i=c;i < op;i++){
			var cl = ids[i];
			be = "<button class='buttons' id='"+i+"' onclick='changeColor()'></button>";
		//	be = "<input style='height:22px;width:20px;' type='button' class='buttons' value='E' onclick=\"jQuery('#OptionList').editRow('"+cl+"');\"  />"; 
			//se = "<input style='height:22px;width:20px;' type='button' class='buttons' value='S' onclick=\"jQuery('#OptionList').saveRow('"+cl+"');\"  />"; 
			//ce = "<input style='height:22px;width:20px;' type='button' class='buttons' value='C' onclick=\"jQuery('#OptionList').restoreRow('"+cl+"');\" />"; 
			jQuery("#OptionList").jqGrid('setRowData',i,{act:be});
		}	
	},
	onSelectRow:function(){
	                       var sel = $(this).jqGrid('getGridParam','selrow');
	                       var option = $(this).jqGrid('getCell',sel,'answerOptionText');
 	        		        answerId = $(this).jqGrid('getCell',sel,'aoid');
						    questionId = $(this).jqGrid('getCell',sel,'associatedQuestionID');
						   console.log(questionId+answerId);
						// $('#h').css("background-color", "blue");
						   arr = jQuery.makeArray(option);
						   /*array = {questionid: questionId,
                                    answerid: answerId
									};
                           showAnswer.unshift(array);*/
						   for (i = 0; i<showAnswer.length;i++){
						      if(showAnswer[i].associatedQuestionId == questionId){
							    showAnswer[i].assocuatedAnswerOptionID = answerId;
								showAnswer[i].answerText = option;
							//	break;
							  }
							  }
							  
							  for (i = 0; i<showAnswer.length;i++){
							  
							 if(showAnswer[i].associatedQuestionId != questionId){
							     array = {associatedQuestionId: questionId,
                                     assocuatedAnswerOptionID: answerId,
					                 answerText: option,
					                 attemptID: 2,
					                 associatedTestID: 6,
					                 timeInSecondsAfterTest: 30,
					                 jobID: 2
					                 };
						         showAnswer.unshift(array);
								 break;
								 }}
							  
						   $('#showAnswer').show();
						$('#showAnswer').text("Your Selected Answer is: "+arr);
						var ansoid = showAnswer[i].assocuatedAnswerOptionID;
						var ansoid1 = ansoid-1;
						var property = $('.buttons');
                        for( i =0;i<4;i++){
	                    if(property[i].id == ansoid1){
		                property[i].style.backgroundColor = 'blue';}
						else{
						property[i].style.backgroundColor = 'red';
						}
                       }}
});
}
$('#OptionList_act').hide();
$('#OptionList_answerOptionText').hide();
//for (var i = 0, iLen = op; i < iLen; ++i){
for (var i = c;i < op; ++i){

//for(var c = o; c < op; c++)
 // {

    jQuery("#OptionList").jqGrid('addRowData',i,optionList[i]);
    o = c+4;
//}
}
}

var answerId;
var arr;
var questionId;
//console.log(showAnswer);
//function showFlag() {
//$("#flag").show()


//}

var flagValue = 0;
function showFlag()
{
 
elem = document.getElementById('flag');
if (elem.style.display == 'block')
{
elem.style.display = 'none';
flagVlue = 0;
}
else
{
elem.style.display = 'block';
flagValue = 1;
}
for (i = 0; i<arFlag.length;i++){
						      if(arFlag[i].questionid == questionId){
							    arFlag[i].flagvalue = flagValue;					  
                                flagValue = 0;

								//break;
							  }
							  else 
							  {
							     arrayFlag = {questionid: questionList[incdecQues].qid,
                                              flagvalue: flagValue };
                                 arFlag.unshift(arrayFlag);
								 flagValue = 0;
								 break;
							  }
							  }

}

/*function showPreviousAnswer(){

for (var i = 0,iLen = showAnswer.length;i<iLen;i++){
var quesid = showAnswer[i].associatedQuestionId;
var ansoid = showAnswer[i].assocuatedAnswerOptionID;
if( quesid == incdecQues+1){
$('#showAnswer').show();
$('#showAnswer').text("Your Selected Answer is: "+arr);
//elem.style.display = 'block';
}
else
{
$('#showAnswer').hide();
elem = document.getElementById('flag');
elem.style.display = 'none';
flagValue = 0;
}
}
}*/
function showPreviousFlag(){

for (var i = 0,iLen = arFlag.length;i<iLen;i++){
//arrayFlag = {questionid: questionList[incdecQues].qid,
//            flagvalue: flagValue };
 //arFlag.unshift(arrayFlag);
elem = document.getElementById('flag');
var quesidforflag = arFlag[i].questionid;
var flagvalue1 = arFlag[i].flagvalue;

if( quesidforflag == incdecQues+1){
if(flagvalue1 == 1)
{
elem.style.display = 'block';
console.log(flagvalue1);
break;
}
else
{
elem.style.display = 'none'; 
flagValue = 0;
break;
}
}
}
}
var ansoid;
function showPreviousAnswer(){

for (var i = 0,iLen = showAnswer.length;i<iLen;i++){
var quesid = showAnswer[i].associatedQuestionId;
ansoid = showAnswer[i].assocuatedAnswerOptionID;
var ansoid1 = ansoid - 1;
//var quesidforflag = arFlag[i].questionid;
//var flagvalue1 = arFlag[i].flagvalue;
//if( quesid == incdecQues+1 || quesidforflag == incdecQues+1){

if( quesid == incdecQues+1){
$(incdecQues).css("background-color", "blue");
console.log(optionList[ansoid-1].answerOptionText);
var answer = optionList[ansoid-1].answerOptionText;
$('#showAnswer').show();
//document.getElementById(ansoid).css("background-color", "blue");
document.getElementById(ansoid-1).style.color = "white";
document.getElementById(ansoid-1).style.background = "#FFD700";
//elem.style.display = 'block';
$('#showAnswer').text("Your Selected Answer is: "+answer);
var property = $('.buttons');
       // property.style.background-color="#RRGGBB";
	   for( i =0;i<4;i++){
	   if(property[i].id == ansoid1)
		property[i].style.backgroundColor = 'blue';
       }
console.log(quesid);
console.log(ansoid);
console.log(flag);
break;
}
else
{
$('#showAnswer').hide();
//break;
}
}
}

//For Sending Answer
var inputJsonStringAnswer = null;
var finalDataInputAnswer = null;
var testSubject = null;
function sendAnswer() {
	
	var serviceObj = Search(SEND_ANSWER);
	var dataToBeModified = serviceObj[KEY_DATA];
	var uidd = uid;
	showAnswer = JSON.stringify(showAnswer);
	var r = new RegExp('"', 'g');
    showAnswer = showAnswer.replace(r, "'");
	console.log(showAnswer);
	//showAnswer = JSON.parse(showAnswer);
	uid = parseInt(uidd);
        inputJsonStringAnswer = JSON.stringify(dataToBeModified);
        finalDataInputAnswer = inputJsonStringAnswer.replace('%e',uid);
		finalDataInputAnswer = finalDataInputAnswer.replace('%s',showAnswer);
        console.log(finalDataInputAnswer);
	var getSkillData = serviceObj[KEY_DATA];

$.ajax({
    url: serviceObj[KEY_URL],
    contentType: 'application/json',
    type: 'POST',
    data: finalDataInputAnswer,
    success: function(result){
	console.log(result);

}

});
}



//for progressbar

var out = null;
var amount = 2701;
var finishvar = 1;
function GetCount(iid){

	amount = amount-1;
	var time = amount;   

	// if time is already past
	if(amount < 0){
		document.getElementById(iid).innerHTML="FINISH!";
		$(this).dialog("close");
	}
	// else date is still good
	else{
			if(finishvar == 1){
		days=0;hours=0;mins=0;secs=0;out="";

		mins=Math.floor(amount/60);//minutes
		amount=amount%60;

		secs=Math.floor(amount);//seconds
		out += mins +" "+((mins==1)?"min":"mins")+": ";
		out += secs +" "+((secs==1)?"sec":"secs")+", ";
		out = out.substr(0,out.length-2);
		document.getElementById(iid).innerHTML=out;
               amount = time;
		setTimeout(function(){GetCount(iid)}, 1000);
		}
		else{
		      
		}
	}
}


  $(function() {
    var progressbar = $( "#progressbar" ),
      progressLabel = $( ".progress-label" );
 
    progressbar.progressbar({
 //     value: false,
      complete: function() {
        progressLabel.text( "Finish" );
      }
    });

    function progress() {
      var val = progressbar.progressbar( "value" ) ;
      progressbar.progressbar( "value", val+1);
      
      if ( val > -1 && val < 99 ) {
        setTimeout( progress, 27000 );
      }
    }
 
    setTimeout( progress, 27000 );
  });



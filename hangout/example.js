var Answers = {
  CLICK: 'c'
};


var DEFAULT_STATUS = {};
DEFAULT_STATUS[Answers.CLICK] = 'Click';

var MAX_STATUS_LENGTH = 255;
var statusVisible_ = false;
var state_ = null;
var metadata_ = null;
var participants_ = null;
var statusForm_ = null;
var statusInput_ = null;
var container_ = null;

function defer(func) {
  window.setTimeout(func, 10);
}

function makeUserKey(id, key) {
  return id + ':' + key;
}

var saveValue = null;
var removeValue = null;
var submitDelta = null;

(function() {
  var prepareForSave = function(keyOrState, opt_value) {
    var state = null;
    if (typeof keyOrState === 'string') {
      state = {};
      state[keyOrState] = opt_value;
    } else if (typeof keyOrState === 'object' && null !== keyOrState) {
      state = {};
      for (var key in keyOrState) {
        if (keyOrState.hasOwnProperty(key)) {
          state[key] = keyOrState[key];
        }
      }
    } else {
      throw 'Unexpected argument.';
    }
    return state;
  };

  var prepareForRemove = function(keyOrListToRemove) {
    var delta = null;
    if (typeof keyOrListToRemove === 'string') {
      delta = [keyOrListToRemove];
    } else if (typeof keyOrListToRemove.length === 'number' &&
               keyOrListToRemove.propertyIsEnumerable('length')) {
      for (var i = 0, iLen = keyOrListToRemove.length; i < iLen; ++i) {
        if (typeof keyOrListToRemove[i] === 'string') {
          delta.push(keyOrListToRemove[i]);
        }
      }
    } else {
      throw 'Unexpected argument.';
    }
    return delta;
  };

  var submitDeltaInternal = function(addState, opt_removeState) {
    gapi.hangout.data.submitDelta(addState, opt_removeState);
  };

  saveValue = function(keyOrState, opt_value) {
    var delta = prepareForSave(keyOrState, opt_value);
    if (delta) {
      submitDeltaInternal(delta);
    }
  };

  removeValue = function(keyOrListToRemove) {
    var delta = prepareForRemove(keyOrListToRemove);
    if (delta) {
      submitDeltaInternal({}, delta);
    }
  };

  submitDelta = function(addState, opt_removeState) {
    if ((typeof addState !== 'object' && typeof addState !== 'undefined') ||
        (typeof opt_removeState !== 'object' &&
         typeof opt_removeState !== 'undefined')) {
      throw 'Unexpected value for submitDelta';
    }
    var toAdd = addState ? prepareForSave(addState) : {};
    var toRemove = opt_removeState ? prepareForRemove(opt_removeState) :
        undefined;
    submitDeltaInternal(toAdd, toRemove);
  };
})();

function onAnswer(newAnswer) {
  var myId = getUserHangoutId();
  var answerKey = makeUserKey(myId, 'answer');
  var current = getState(answerKey);

  if (current === newAnswer) {
    removeValue(answerKey);
  } else {
    saveValue(answerKey, newAnswer);
  }
}

function getStatusMessage(participantId) {
  return getState(makeUserKey(participantId, 'status'));
}

function getState(opt_stateKey) {
  return (typeof opt_stateKey === 'string') ? state_[opt_stateKey] : state_;
}

function getMetadata(opt_metadataKey) {
  return (typeof opt_metadataKey === 'string') ? metadata_[opt_metadataKey] :
      metadata_;
}

 function getUserHangoutId() {
  return gapi.hangout.getParticipantId();
}

function render() {
  if (!state_ || !metadata_ || !participants_ || !container_) {
    return;
  }

  if (statusVisible_) {
    return;
  }

  var data = {
    total: 0,
    responded: false
 };
  data[Answers.CLICK] = [];

  var myId = getUserHangoutId();
  for (var i = 0, iLen = participants_.length; i < iLen; ++i) {
    var p = participants_[i];
    var answerKey = makeUserKey(p.id, 'answer');
    var answer = getState(answerKey);
    var meta = getMetadata(answerKey);

    if (answer && data[answer]) {
      data[answer].push(p);
      if (p.id === myId) {
        data.responded = true;
      }
      ++data.total;

      var name = p.person.displayName;
      var parts = name.split('@');
      if (parts && parts.length > 1) {
        p.person.displayName = parts[0];
      }

      p.status = getStatusMessage(p.id) || '';
      p.sortOrder = meta.timestamp;
    }
  }

  var sortFunc = function(a, b) {
    return a.sortOrder - b.sortOrder;
  };
  for (var answer in data) {
    if (data.hasOwnProperty(answer) && data[answer].sort) {
      data[answer].sort(sortFunc);
   }
  }

  container_
      .empty()
      .append(createAnswersTable(data));
}

function updateLocalDataState(state, metadata) {
  state_ = state;
  metadata_ = metadata;
render();
}

function updateLocalParticipantsData(participants) {
  participants_ = participants;
render();
}

function prepareAppDOM() {
  statusInput_ = $('<input />')
      .attr({
        'id': 'status-input',
        'type': 'text',
        'maxlength': MAX_STATUS_LENGTH
      });
  statusForm_ = $('<form />')
      .attr({
        'action': '',
        'id': 'status-form'
      })
      .append(statusInput_);

  var statusDiv = $('<div />')
      .attr('id', 'status-box')
      .addClass('status-box')
      .append(statusForm_);

  statusForm_.submit(function() {
    return false;
  });

  statusInput_.keypress(function(e) {
    if (e.which === 13) {

    }
    e.stopPropagation();
  }).blur(function(e) {
    e.stopPropagation();
  }).mousedown(function(e) {
    e.stopPropagation();
  }).hide();

 container_ = $('<div id="container" />');
 var body = $('body');
  body.mousedown(function(e) {
    e.stopPropagation();
  }).append(container_, statusDiv);
}

function createAnswersTable(data) {
  var buttonRow = $('<tr />');
  var onButtonMouseDown = function() {
    $(this).addClass('selected');
  };
  var getButtonMouseUpHandler = function(ans) {
    return function() {
      $(this).removeClass('selected');
      onAnswer(ans);
    };
  };

  for (var key in Answers) {
    if (Answers.hasOwnProperty(key)) {
      var ans = Answers[key];
      var ansLink = $('<a style="float: center;" />')
          .attr('href', '#')
          .text("C")
          .click(function() {
            return false;
          });
      var ansBtn = $('<div style="float: center;" />')
          .addClass('button')
          .append(ansLink)
          .mousedown(onButtonMouseDown)
          .mouseup(getButtonMouseUpHandler(ans));
      var respondList = $('<ul />');
     for (var i = 0, iLen = data[ans].length; i < iLen; ++i) {
        respondList.append(createParticipantElement(data[ans][i], ans));
      }       

      var ansCell = $('<td />')
          .attr('id', key)
          .append(ansBtn, respondList);

      // Add list of participants below each button.
      buttonRow.append(ansCell);
    }
  }

  var table = $('<table />')
      .attr({
        'cellspacing': '2',
        'cellpadding': '0',
        'summary': '',
        'width': '100%'
      }).append(buttonRow);



  return table;
}

function createParticipantElement(participant,response) {
  
var avatar = $('<img />').attr({
    'width': '27',
    'alt': 'Avatar',
    'class': 'avatar',
    'src': participant.person.image && participant.person.image.url ?
        participant.person.image.url : DEFAULT_ICONS[response]
  });

  var name = $('<h2 />').text(participant.person.displayName);

  var cont = $('<object data= "https://www.youtube.com/embed?v=Al4SbeVyLm4" width="560" height="315" />');
  
  return $('<li />').append(cont);
}

(function() {
  if (gapi && gapi.hangout) {

    var initHangout = function(apiInitEvent) {
      if (apiInitEvent.isApiReady) {
        prepareAppDOM();

        gapi.hangout.data.onStateChanged.add(function(stateChangeEvent) {
          updateLocalDataState(stateChangeEvent.state,
                               stateChangeEvent.metadata);
        });
        gapi.hangout.onParticipantsChanged.add(function(partChangeEvent) {
          updateLocalParticipantsData(partChangeEvent.participants);
        });

        if (!state_) {
          var state = gapi.hangout.data.getState();
          var metadata = gapi.hangout.data.getStateMetadata();
          if (state && metadata) {
            updateLocalDataState(state, metadata);
          }
        }
        if (!participants_) {
          var initParticipants = gapi.hangout.getParticipants();
          if (initParticipants) {
            updateLocalParticipantsData(initParticipants);
          }
        }

        gapi.hangout.onApiReady.remove(initHangout);
      }
    };

    gapi.hangout.onApiReady.add(initHangout);
  }
})();
﻿$(document).ready(function () {
    var searchTable = $("#grdUserPaidSubscription").DataTable();
    searchTable.columns(0).visible(false);
    searchTable.columns(1).visible(false);
    
    SearchUserSubscription();

    var UserName = localStorage.getItem("LoginUserName");
    GetLoginUserDetails();
});

function GetLoginUserDetails() {

    $.ajax({
        url: '../Signin/Signin.aspx/getLoginUserId',
        contentType: " application/json; charset=utf-8",
        type: 'POST',
        async: false,
        success: function (data) {
            if (data.d.Status == 1) {
                var AddUserName = $('#AddUserName');
                AddUserName.empty();
                AddUserName.append('  <a class="nav-link js-scroll-trigger">' + data.d.Result.UserName + '</a>');

                var AddLogOutButton = $('#AddLogOutButton');
                AddLogOutButton.empty();
                AddLogOutButton.append('<a class="nav-link js-scroll-trigger" href="../LogOut/LogOut" >Log Out</a> ');
            }

        },
        error: function (request, error) {
            console.log(arguments);
            alert(" Can't do because: " + error);
        },
    });


}



function Reset()
{
    $('#fromDate').val("");
    $('#toDate').val("");
    $('#planName').val("");
    $('#productType').val("");
    $('#status').val(0);
    $('#userName').val("");
}


function SearchUserSubscription()
{
    var inputData = {};
    var fromDate= $('#fromDate').val();
    var toDate= $('#toDate').val();
    var planName= $('#planName').val();
    var productType= $('#productType').val();
    var status= $('#status').val();
    var userName = $('#userName').val();

    inputData.fromDate = fromDate;
    inputData.toDate = toDate;
    inputData.planName = planName;
    inputData.productType = productType;
    inputData.status = status;
    inputData.userName = userName;


    ajaxCaller("POST", "UserSubScriptionDetails.aspx/getALlPaidSubSriptionDetails", JSON.stringify(inputData), "application/json", "Please wait").done(function (data) {
        if (data.d.Status == 1) {
            showMessage("green", data.d.Description, "");
            var DataResult = data.d.Result;

            var searchTable = $("#grdUserPaidSubscription").DataTable();
            searchTable.clear().draw();
            var oResult = DataResult;
            if (oResult.length > searchCount) {
                showMessage("green", "There are more than " + searchCount + " results, Please refine your search.", "");
            }
            var searchDataSet = [];
            for (i = 0; i < oResult.length; i++) {
                var dataSet = [];
                var result = oResult[i];
                dataSet.push(result.PlanID);
                dataSet.push(result.UserSubScriptionID);
                dataSet.push(result.planName);
                dataSet.push(result.producttype);
                dataSet.push(result.FromDate);
                dataSet.push(result.ToDate);
                dataSet.push(result.UserName);
                dataSet.push(result.PlanType);

                searchDataSet[i] = dataSet;
            }
            searchTable.rows.add(searchDataSet).draw();
            searchTable.columns(0).visible(false);
            searchTable.columns(1).visible(false);
            searchTable.column(1).visible(false).order("desc");
            searchTable.columns.adjust().draw();

        } else if (data.d.Status == 2) {
            showMessage("red", data.d.Description, "");
        } else if (data.d.Status == 0) {
            showMessage("red", data.d.Description, "");
        }
    });



}

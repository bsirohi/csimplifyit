﻿var stripeKey = "";
var userEmailId;
var handler = "";
var currentPlanLevel;
//  showSubscriptionPlans("");
var currency = "USD";
var planAmount = 0;
var isPlanAutoRenewal = "1";
var paymentOrderNo = "";
var paymentInitKey = "";
var purchasedPlanKey = "";
var selectedPaymentType = "1";
var couponCode = "";
var SubScriptionDetails;
var getPlanDetails;

var base_url;
var getLogInUser;
$(document).ready(function () {

    base_url = window.location.origin;

    var host = window.location.host;

    var pathArray = window.location.pathname.split('/');

    GetLoginUserDetails();
    getLogInUser = localStorage.getItem("LoginUserDetails");



    function GetLoginUserDetails() {

        $.ajax({
            url: '../Signin/Signin.aspx/getLoginUserId',
            contentType: " application/json; charset=utf-8",
            type: 'POST',
            async: false,
            success: function (data) {
                
                if (data.d.Status == 1) {
                    userEmailId = data.d.Result.EmailId1;
                    var AddUserName = $('#AddUserName');
                    AddUserName.append('  <a class="nav-link js-scroll-trigger">' + data.d.Result.UserName + '</a>');

                    var AddLogOutButton = $('#AddLogOutButton');
                    AddLogOutButton.append('<a class="nav-link js-scroll-trigger" href="../LogOut/LogOut" >Log Out</a> ');
                }
            },
            error: function (request, error) {
                console.log(arguments);
                alert(" Can't do because: " + error);
            },
        });

      
    }


    


    if (getLogInUser === "1")
    {
        getAllSubscription();
        $('#USerDetailsSection').show();
        $('#SignInSection').hide();
    }
    else
    {
        getAllSubscription();
        $('#SignInSection').show();
        $('#USerDetailsSection').hide();
    }
    

    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
    hashes = hashes.replace('#', '');
    hashes = hashes.split('&');

    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
    }
    ;
    if (hash[1] == undefined) {
       // return;
    }
   
    UserType = hash[1];

  

    if (UserType === "1")
    {       
        $('#SimpleMassMailMearge').show();
        $('#SimpleRecruitmentManger').hide();
        $('#SimpleWorkflowManager').hide();
        $('#SimplePaymentManger').hide();
    } else if (UserType === "2")
    {
        $('#SimpleMassMailMearge').hide();
        $('#SimpleRecruitmentManger').show();
        $('#SimpleWorkflowManager').hide();
        $('#SimplePaymentManger').hide();
    }
    else if (UserType === "3") {
        $('#SimpleMassMailMearge').hide();
        $('#SimpleRecruitmentManger').hide();
        $('#SimpleWorkflowManager').show();
        $('#SimplePaymentManger').hide();
    }
    else if (UserType === "4") {
        $('#SimpleMassMailMearge').hide();
        $('#SimpleRecruitmentManger').hide();
        $('#SimpleWorkflowManager').hide();
        $('#SimplePaymentManger').show();
    }

    

    //** 
  
        // First, parse the query string
        var params = {}, queryString = location.hash.substring(1),
              regex = /([^&=]+)=([^&]*)/g, m;
        while (m = regex.exec(queryString)) {
            params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
        }
        var ss = queryString.split("&")
        // window.location = "Home.aspx?" + ss[1];
        if (ss != undefined) {
            var token = ss[0];
            if (token !== "")
            {
                OpenGoogleLoginWindow(token);
            }
            
        }
        
          


  

    //**

  
    //Select on change first Product

    $('#Starter_period').on('change', function () {
        var Id = $('#Starter_period').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var StarterPrice = $('#StarterPrice');
        StarterPrice.empty();
        StarterPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');

    });
    
    $('#Professionalperiod').on('change', function () {
        var Id = $('#Professionalperiod').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var ProfessionalPrice = $('#ProfessionalPrice');
        ProfessionalPrice.empty();
        ProfessionalPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');

    });

    $('#Premiumperiod').on('change', function () {
       
        var Id = $('#Premiumperiod').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var PremiumPrice = $('#PremiumPrice');
        PremiumPrice.empty();
        PremiumPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');


    });

    $('#Supportperiod').on('change', function () {
       
        var Id = $('#Supportperiod').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var SupportPrice = $('#SupportPrice');
        SupportPrice.empty();
        SupportPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span style="font-size: 40px; vertical-align: top; font-weight: bold; color: #333;">' + Amount[0].DiscountValue + '</span>');

    });



    //Second Product

    $('#Starter_periodSRM').on('change', function () {
        var Id = $('#Starter_periodSRM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var StarterPrice = $('#StarterPriceSRM');
        StarterPrice.empty();
        StarterPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');

    });

    $('#ProfessionalperiodSRM').on('change', function () {
        var Id = $('#ProfessionalperiodSRM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var ProfessionalPrice = $('#ProfessionalPriceSRM');
        ProfessionalPrice.empty();
        ProfessionalPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');

    });

    $('#PremiumperiodSRM').on('change', function () {

        var Id = $('#PremiumperiodSRM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var PremiumPrice = $('#PremiumPriceSRM');
        PremiumPrice.empty();
        PremiumPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');


    });

    $('#SupportperiodSRM').on('change', function () {

        var Id = $('#SupportperiodSRM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var SupportPrice = $('#SupportPriceSRM');
        SupportPrice.empty();
        SupportPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span style="font-size: 40px; vertical-align: top; font-weight: bold; color: #333;">' + Amount[0].DiscountValue + '</span>');

    });




    //Third Product

    $('#Starter_periodSWM').on('change', function () {
        var Id = $('#Starter_periodSWM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var StarterPrice = $('#StarterPriceSWM');
        StarterPrice.empty();
        StarterPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');

    });

    $('#ProfessionalperiodSWM').on('change', function () {
        var Id = $('#ProfessionalperiodSWM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var ProfessionalPrice = $('#ProfessionalPriceSWM');
        ProfessionalPrice.empty();
        ProfessionalPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');

    });

    $('#PremiumperiodSWM').on('change', function () {

        var Id = $('#PremiumperiodSWM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var PremiumPrice = $('#PremiumPriceSWM');
        PremiumPrice.empty();
        PremiumPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');


    });

    $('#SupportperiodSWM').on('change', function () {

        var Id = $('#SupportperiodSWM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var SupportPrice = $('#SupportPriceSWM');
        SupportPrice.empty();
        SupportPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span style="font-size: 40px; vertical-align: top; font-weight: bold; color: #333;">' + Amount[0].DiscountValue + '</span>');

    });




    //Fourth Product

    $('#Starter_periodSPM').on('change', function () {
        var Id = $('#Starter_periodSPM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var StarterPrice = $('#StarterPriceSPM');
        StarterPrice.empty();
        StarterPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');

    });

    $('#ProfessionalperiodSPM').on('change', function () {
        var Id = $('#ProfessionalperiodSPM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var ProfessionalPrice = $('#ProfessionalPriceSPM');
        ProfessionalPrice.empty();
        ProfessionalPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');

    });

    $('#PremiumperiodSPM').on('change', function () {

        var Id = $('#PremiumperiodSPM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var PremiumPrice = $('#PremiumPriceSPM');
        PremiumPrice.empty();
        PremiumPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span  class="discountedPrice">' + Amount[0].ActualValue + '</span><span> ' + Amount[0].DiscountValue + '</span>');


    });

    $('#SupportperiodSPM').on('change', function () {

        var Id = $('#SupportperiodSPM').val();  //or alert($(this).val());
        var Amount = SubScriptionDetails.filter(function (Data) {
            return Data.spid == Id && Data.planFeaturesCode == "1001";
        });
        var SupportPrice = $('#SupportPriceSPM');
        SupportPrice.empty();
        SupportPrice.append('<img src="../assets/img/dollar.png" style="width: 25px;"><span style="font-size: 40px; vertical-align: top; font-weight: bold; color: #333;">' + Amount[0].DiscountValue + '</span>');

    });




    
    //end here

    $.ajax({
        url: 'StripePayment.aspx/getstripeKey',
        contentType: " application/json; charset=utf-8",
        type: 'POST',
        async: false,
        // type: 'json',
        success: function (data) {
            var Data = data.d;
            if (Data.Status == 1) {
                stripeKey = Data.Result.Description;
            } else if (Data.Status == 2) {
               
            } else {
                
            }
        },
        error: function (request, error) {
            console.log(arguments);
            alert(" Can't do because: " + error);
        },
        //beforeSend: function () {
            
        //},
        //complete: function () {
          
        //}
    });

   // getallSubscription();

   


     handler = StripeCheckout.configure({
        key: stripeKey,
        locale: 'auto',
        email: userEmailId,
        allowRememberMe: false,
        currency: "USD",
        name: "Pay",
        zipCode: false,
        billingAddress: false,
        
        token: function (token) {
            saveStripePayment(token);
        }
    });

    setTimeout(function ()
    {
      //  openhandler();
    }, 100000);
  
   
});







function OpenGoogleLoginWindow(userToken)
{
    $.ajax({

        url: 'https://www.googleapis.com/oauth2/v1/userinfo?' + userToken,
        //  data: null,
        success: function (resp) {
            user = resp;
            if (user.verified_email == true)
            {
                var input = {};
                input.FirstName = user.name;
                input.LastName = user.family_name;
                input.Email = user.email;
                input.Password = '';
                input.usertype = localStorage.getItem("UserType");
                input.IsGoogleSignin = input.usertype;
                SignIn(input);
            }
            else
            {
                window.location.href = base_url+"/Sign-in/Signin.aspx";
            }
            
        },
        error: function (request, error) {
            console.log(arguments);
            alert(" Can't do because: " + error);
        },

    });
}



//Author by- Billu - 27-12-2019
/* Get Subscription Plan*/

var PlanID;
var PlanName;

function GetPlanAmount(PlanID,Code)
{
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == Code;
    });
}

// for first product
function bascicPlan()
{    
    PlanID = $('#basic_period').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    PlanName = "basic";
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}


function StarterPlan() {
    PlanName = "Starter";
    PlanID = $('#Starter_period').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}

function ProfessionalPlan() {
    PlanName = "Professional";
    PlanID = $('#Professionalperiod').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);

}

function PremiumPlan() {
    PlanName = "Premium";
    PlanID = $('#Premiumperiod').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);

}

function SupportPlan() {
    PlanName = "Support";
    PlanID = $('#Supportperiod').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}


// Second product plan


function bascicPlan1() {
    PlanID = $('#basic_periodSRM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    PlanName = "basic";
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}


function StarterPlan1() {
    PlanName = "Starter";
    PlanID = $('#Starter_periodSRM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}

function ProfessionalPlan1() {
    PlanName = "Professional";
    PlanID = $('#ProfessionalperiodSRM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);

}

function PremiumPlan1() {
    PlanName = "Premium";
    PlanID = $('#PremiumperiodSRM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);

}

function SupportPlan1() {
    PlanName = "Support";
    PlanID = $('#SupportperiodSRM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}

// Third Product Plan

function bascicPlan3() {
    PlanID = $('#basic_periodSWM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    PlanName = "basic";
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}


function StarterPlan3() {
    PlanName = "Starter";
    PlanID = $('#Starter_periodSWM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}

function ProfessionalPlan3() {
    PlanName = "Professional";
    PlanID = $('#ProfessionalperiodSWM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);

}

function PremiumPlan3() {
    PlanName = "Premium";
    PlanID = $('#PremiumperiodSWM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);

}

function SupportPlan3() {
    PlanName = "Support";
    PlanID = $('#SupportperiodSWM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}

// Fourth Product Plan

function bascicPlan4() {
    PlanID = $('#basic_periodSPM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    PlanName = "basic";
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}


function StarterPlan4() {
    PlanName = "Starter";
    PlanID = $('#Starter_periodSPM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}

function ProfessionalPlan4() {
    PlanName = "Professional";
    PlanID = $('#ProfessionalperiodSPM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);

}

function PremiumPlan4() {
    PlanName = "Premium";
    PlanID = $('#PremiumperiodSPM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);

}

function SupportPlan4() {
    PlanName = "Support";
    PlanID = $('#SupportperiodSPM').val();
    var Amount = SubScriptionDetails.filter(function (Data) {
        return Data.spid == PlanID && Data.planFeaturesCode == "1001";
    });
    planAmount = Amount[0].DiscountValue;
    canInitiateForPayment(PlanID, planAmount);
}





// End here

function validatePlanIsValid(inputData)
{
    ajaxCaller("POST", "StripePayment.aspx/canInitiateForPayment", JSON.stringify(inputData), "application/json", "Please wait").done(function (data) {
        if (data.d.Status == 1) {
           

        } else if (data.d.Status == 2) {
            // $('#myModal').modal('show');
        } else if (data.d.Status == 0) {
            showMessage("red", data.d.Description, "");

           
        }
    });

}




var SubScriptionPlanID;
function canInitiateForPayment(PlanId, planAmount) {
    var inputData = {};
    SubScriptionPlanID = PlanId;
    inputData.PlanId = PlanId;
    inputData.planAmount = planAmount;

    var Plan=  getPlanDetails.filter(function (Data) {
        return Data.spId == PlanId ;
    });

    if (getLogInUser === "1")
    {
        ajaxCaller("POST", "StripePayment.aspx/canInitiateForPayment", JSON.stringify(inputData), "application/json", "Please wait").done(function (data) {
            if (data.d.Status == 1) {
                //  showMessage("green", data.d.Description, "");        
                handler.open({
                    name: 'C Simplify IT',
                    description: 'Bringing Plan:' + Plan[0].planDescription,
                    currency: "USD",
                    amount: planAmount * 100
                });

            } else if (data.d.Status == 2) {
                // $('#myModal').modal('show');
            } else if (data.d.Status == 0) {
                
                showMessage("red", data.d.Description, "");

                // var popUpText = data.response.Result[0].paymentOrderDesc;
                // swal({
                // title: "Unable to initiate Payment!",
                // text: popUpText,
                // type: "warning",
                // showCancelButton: true,
                // cancelButtonClass: "btn btn-warning-swal",
                // closeOnCancel: true
                // });
            }
        });

    }
    else
    {
        showMessage("red", "Please LogIn", "");
        window.location.href = "../signin/signin.aspx?id=1";
    }

  
}



function saveStripePayment(token) {
    var SubScriptionId = PlanID;
    token.SubScriptionId = SubScriptionId;
    token.currency = "USD";
    token.amount = planAmount;
    token.isPlanAutoRenewal = isPlanAutoRenewal;
    token.couponCode = couponCode;
    token.paymentInitKey = paymentInitKey;
    token.paymentOrderNo = paymentOrderNo;
    token.purchasedPlanKey = purchasedPlanKey;
    token.paymentType = selectedPaymentType;
    var x = JSON.stringify(token);

    ajaxCaller("POST", "../Charge/Charge.aspx/saveStripPayments", JSON.stringify(token), "application/json", "Please wait").done(function (data) {
        if (data.d.Status == 1) {
            showMessage("green", data.d.Description, "");
            $('#SignInSection').hide();
            $('#USerDetailsSection').show();

            if (PlanName == "basic") {
                
            }
            else if (PlanName == "Starter") {

            }
            else if (PlanName == "Professional") {

            }
            else if (PlanName == "Premium") {

            }
            else if (PlanName == "Support") {

            }

        } else if (data.d.Status == 2) {
            // $('#myModal').modal('show');
        } else if (data.d.Status == 0) {
            showMessage("red", data.d.Description, "");
        }
    });





    //$.ajax({
    //    type: 'POST',
    //    url: '../Charge/Charge.aspx/saveStripPayments',
    //    contentType: "application/json; charset=utf-8",    
    //    async: false,
    //    dataType: 'json',
    //    data: "{'requestData':'" + x + "'}",
    //    success: function (data) {
    //        var Data = data.d;
    //        if (Data.Status == 1) {
    //            stripeKey = Data.Result.Description;
    //        } else if (Data.Status == 2) {

    //        } else {

    //        }
    //    },
    //    error: function (request, error) {
    //        console.log(arguments);
    //        alert(" Can't do because: " + error);
    //    },
    //    beforeSend: function () {

    //    },
    //    complete: function () {

    //    }
    //});


    //ajaxCaller("POST", contextPath + "/saveStripPayment.action", JSON.stringify({ token }), "application/json", "Please wait").done(function (data) {
    //    if (data.response.Status == 2) {
    //        $('#myModal').modal('show');
    //    } else if (data.response.Status == 0) {
    //        showMessage("red", data.response.Description, "");
    //    } else if (data.response.Status == 1) {
    //        $("#planStatus").hide();
    //        var result = data.response.Result;
    //        if (result.length == 1) {
    //            var message = result[0].message;
    //            swal({
    //                title: "Your request is successful.",
    //                text: message,
    //                type: "success",
    //                confirmButtonClass: "btn btn-success-swal",
    //                //confirmButtonText: "Proceed",
    //                showConfirmButton: true,
    //                closeOnConfirm: true
    //            },
    //            function () {
    //                window.location.href = contextPath + '/my-plan';
    //            });
    //        }
    //    }
    //});

}


/* End Subscription plan*/



/// Google Authantication 


var OAUTHURL = 'https://accounts.google.com/o/oauth2/auth?';
var VALIDURL = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=';
var SCOPE = 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email';
var CLIENTID = '509303823636-4nmni39qd4m0egmiv7cgkdqq57teuo5h.apps.googleusercontent.com';
var REDIRECT = 'http://localhost:2306/Charge/Charge.aspx'; // 
var LOGOUT = 'http://localhost:2306/Strip_Payment_way/StripePayment.aspx'; // http://localhost:2306/Strip_Payment_way/StripePayment.aspx
var TYPE = 'token';
var _url = OAUTHURL + 'scope=' + SCOPE + '&client_id=' + CLIENTID + '&redirect_uri=' + REDIRECT + '&response_type=' + TYPE;
var acToken;
var tokenType;
var expiresIn;
var user;
var loggedIn = false;





function login() {

    var win = window.open(_url, "windowname1", 'width=800, height=600');
    var pollTimer = window.setInterval(function () {
        try {
            console.log(win.document.URL);
            if (win.document.URL.indexOf(REDIRECT) != -1) {
                window.clearInterval(pollTimer);
                var url = win.document.URL;
                acToken = gup(url, 'access_token');
                tokenType = gup(url, 'token_type');
                expiresIn = gup(url, 'expires_in');

                win.close();
                debugger;
                validateToken(acToken);
            }
        }
        catch (e) {

        }
    }, 500);
}

function gup(url, name) {
    namename = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\#&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    if (results == null)
        return "";
    else
        return results[1];
}

function validateToken(token) {

    getUserInfo();
    $.ajax(

        {

            url: VALIDURL + token,
            data: null,
            success: function (responseText) {
                var x = responseText;

            },

        });

}

function getUserInfo() {


    $.ajax({

        url: 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' + acToken,
      //  data: null,
        success: function (resp) {
            user = resp;
            console.log(user);
            $('#uname').html('Welcome ' + user.name);
            $('#uemail').html('Email: ' + user.email)
            $('#imgHolder').attr('src', user.picture);


        },


    }),

    $.ajax({

        url: '../Charge/Charge.aspx',

        type: 'POST',
        //data: {
        //    email: user.email,
        //    name: user.name,
        //    gender: user.gender,
        //    lastname: user.lastname,
        //    location: user.location
        //},
        success: function () {
            window.location.href = base_url+"/Charge/Charge.aspx";
        },

        //dataType: "jsonp"

    });
}




function openhandler()
{
    handler.open({
        name: 'CSIT',
        description: 'bringing transparency to subscription plan',
        currency: 'USD',
        amount:1 * 100
    });
}

//function getallSubscription()
//{
//    $.ajax({
//        url: '/StripePayment.aspx/getSubSription',
//        data: "{ }",
//      //  processData: false,
//        contentType:" application/json; charset=utf-8",
//        type: 'POST',
//        type:'json',
//        success: function (data) {
//            if (data.Status == 1) {
              
//              //  showMessage("green", data.Description, "");
//                $('#textForincident' + countCklistItem).val("");
//                $('#AttchmentsLabel').html("");
//                $('#button_issue_creator_notif_' + countCklistItem).text(data.Result)
//                UploadedDocs = "";
//                $(".incidence").css('display', 'none');
//                alert(data.Description);

//            } else if (data.Status == 2) {
//               // showMessage("red", data.Description, "");
//            } else {
//               // showMessage("red", data.Description, "");
//            }
//        },
//        error: function (request, error) {
//            console.log(arguments);
//            alert(" Can't do because: " + error);
//        },
//        beforeSend: function () {
//            //showMessage("loading", "Please wait ! Reporting Incident");
//        },
//        complete: function () {
//           // $('.ajaxLoading').hide();
//           // $("#IncidentSection").css('display', 'none');
//          //  $("#textForincident").val("");
//        }
//    });
//}


$(document).on('click', '.doPayment', function (e) {
    selectedPlanBtnId = $(this).attr("data-selectedPlanBtnId");
    if (!isUserLoggedIn) {
        $("#loginTextIfExist").hide();
        $(".login-steps").show();
        $('#registerform').modal('show');
        return false;
    }

    var btn = $(this);
    if (!canInitiateForPayment(btn)) {
        return false;
    }
    e.preventDefault();
});



// Close Checkout on page navigation:
window.addEventListener('popstate', function () {
    handler.close();
});

function canInitiateForPayment1(btn) {
    var inputData = {};
    inputData.purchasedPlanKey = $(btn).attr("data-purchasedPlanKey");
    inputData.subscriptionPlan = $(btn).attr("data-subscriptionPlan");
    planAmount = $(btn).attr("data-amount");

    inputData.isPlanAutoRenewal = "1";

    if (btn.id = "btnBasicPlan" && $("#basicPlanAutoRenewed").prop("checked"))
        inputData.isPlanAutoRenewal = "1";

    if (btn.id = "btnPremiumPlan" && $("#premiumPlanAutoRenewed").prop("checked"))
        inputData.isPlanAutoRenewal = "1";

    if (btn.id = "btnProPlan" && $("#proPlanAutoRenewed").prop("checked"))
        inputData.isPlanAutoRenewal = "1";
    inputData.currency = currency;
    inputData.amount = planAmount;
    inputData.paymentChannel = "S";
    isPlanAutoRenewal = inputData.isPlanAutoRenewal;
    purchasedPlanKey = inputData.purchasedPlanKey;

    ajaxCaller("POST", contextPath + "/canInitiateForPayment_AR.action", JSON.stringify(inputData), "application/json", "Please wait").done(function (data) {
        if (data.response.Status == 1) {
            if (data.response.Result[0].canInitPayment == "1") {
                planAmount = planAmount;
                paymentInitKey = data.response.Result[0].paymentInitKey;
                paymentOrderNo = data.response.Result[0].paymentOrderNo;
                //subscriptionEnd= data.response.Result[0].subscriptionEnd;
                askUserForPaymentProceed = data.response.Result[0].askUserForPaymentProceed;
                if (askUserForPaymentProceed == "1") {
                    // Open Checkout with further options:
                    if (data.response.Result[0].isShowStripePopup == "1") {
                        handler.open({
                            name: 'BondEvalue',
                            description: 'bringing transparency to bonds',
                            currency: "USD",
                            amount: planAmount * 100
                        });
                    } else if (data.response.Result[0].isShowStripePopup == "0") {
                        var token = {};
                        saveStripePayment(token);
                    }
                } else if (askUserForPaymentProceed == "1") {

                }
            }
        } else if (data.response.Status == 2) {
            $('#myModal').modal('show');
        } else if (data.response.Status == 0) {
            showMessage("red", data.response.Description, "");

            // var popUpText = data.response.Result[0].paymentOrderDesc;
            // swal({
            // title: "Unable to initiate Payment!",
            // text: popUpText,
            // type: "warning",
            // showCancelButton: true,
            // cancelButtonClass: "btn btn-warning-swal",
            // closeOnCancel: true
            // });
        }
    });
    return false;
}


function testcall()
{
    $.ajax({

        type: "POST",
        url: "../Charge/Charge.aspx/test1",
        data: '{Name:"satish"}',
        contentType: "application/json; charset=utf-8",
        
        dataType: "json'",
        
        success: function (data) {
            var Data = data.d;
            if (Data.Status == 1) {
                stripeKey = Data.Result.Description;
            } else if (Data.Status == 2) {

            } else {

            }
        },
        error: function (request, error) {
            console.log(arguments);
            alert(" Can't do because: " + error);
        },
        beforeSend: function () {

        },
        complete: function () {

        }
    });
}

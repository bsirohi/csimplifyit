﻿
var base_url;
$(document).ready(function () {

    base_url = window.location.origin;

    var host = window.location.host;

    var pathArray = window.location.pathname.split('/');


    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
    hashes = hashes.replace('#', '');
    hashes = hashes.split('&');

    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
    }
    ;
    if (hash[1] == undefined) {
        // return;
    }
   
    var ustype = hash[1];
    var GetElement = $('#Sdetails');
    GetElement.append(' <a href="'+base_url+'/Login/LoginCSit.aspx?id=' + ustype + '" class="link link--blue" style="cursor: pointer;">Log In</a>');

    //Facebook Window
    window.fbAsyncInit = function () {
        FB.init({
            appId: '3740748119276391',
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse XFBML
            oauth: true,// enable OAuth 2.0
            version: 'v2.0'
        });
    };


    
});


function SaveRegistration()
{
    debugger;
    input = {};
    var FirstName = $("#Firstname").val();
    if (FirstName == null || FirstName == "" || FirstName == undefined) {
        showMessage("red", "Please Enter First Name");
        $("#Firstname").addClass("inputError");
        $("#Firstname").focus();
        return;
    } else {
        $("#Firstname").removeClass("inputError");
        input.FirstName = FirstName;
    }
    var LastName = $("#Lastname").val();
    if (LastName == null || LastName == "" || LastName == undefined) {
        showMessage("red", "Please Enter Last Name");
        $("#Lastname").addClass("inputError");
        $("#Lastname").focus();
        return;
    } else {
        $("#Lastname").removeClass("inputError");
        input.LastName = LastName;
    }
    var Email = $("#WorkEmail").val();
    if (Email == null || Email == "" || Email == undefined) {
        showMessage("red", "Please Enter Email like abc@gmail");
        $("#WorkEmail").addClass("inputError");
        $("#WorkEmail").focus();
        return;
    } else {
        //$("$WorkEmail").removeClass("inputError");
        input.Email = Email;
    }
    if ($("#PassWord").val().length < 8) {
        showMessage("red", 'Minimum length = 8');
        $("#PassWord").addClass("inputError");
        $("#PassWord").focus();
        return;
    } else
        if ($("#PassWord").val().length > 12) {
            showMessage("red", 'Maximum length = 12');
            $("#PassWord").addClass("inputError");
            $("#PassWord").focus();
               return;
    }

    var Password = $("#PassWord").val();
    if (Password == null || Password == "" || Password == undefined) {
        showMessage("red", "Please Enter Password");
        $("#PassWord").addClass("inputError");
        $("#PassWord").focus();
        return;
    } else {
        $("#PassWord").removeClass("inputError");
        input.Password = Password;
    }
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
    hashes = hashes.replace('#', '');
    hashes = hashes.split('&');
   
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
    }
    ;
    if (hash[1] == undefined) {
        return;
    }
    if (!hash[1].match(/^\d+$/)) {
        return;
    }
    var usertype = hash[1];

    input.usertype = usertype;
    input.IsGoogleSignin = 1;
    ajaxCaller("POST", "Signin.aspx/SaveReg", JSON.stringify(input), "application/json", "Please wait!").done(function (data) {
        if (data.d.Status == 1) {
            showMessage("green", data.d.Description, "");
            if (input.IsGoogleSignin == 1) {
                window.location.href = base_url + "/Login/LoginCSit.aspx?id=" + usertype;
            }
        }
        else {
            showMessage("red", data.d.Description, "");

        }

    });
}

function validateEmail(sEmail) {
    debugger;
    var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

    if (!sEmail.match(reEmail)) {
        showMessage("red", "Invalid email addres")
        $("#WorkEmail").addClass("inputError");
        $("#WorkEmail").focus();
        return;

        return;
    }

    return true;

}



function loginByFacebook() {
    FB.login(function (response) {
        if (response.authResponse) {
            FacebookLoggedIn(response);
        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    }, { scope: 'email' });
}

//function FacebookLoggedIn(response) {
//    var loc = '/Signin/Signin.aspx';
//    if (loc.indexOf('?') > -1)
//        window.location = loc + '&authprv=facebook&access_token=' + response.authResponse.accessToken;
//    else
//        window.location = loc + '?authprv=facebook&access_token=' + response.authResponse.accessToken;
//}



function FacebookLoggedIn(response) {
    debugger;
      $.ajax({
          url: '../Signin/Signin.aspx/GetFacebookUserJSON',
        data: "{'access_token':'" + response.authResponse.accessToken + "'}",
        contentType: " application/json; charset=utf-8",
        type: 'POST',
        success: function (resp) {
            user = resp;
            var jsondata = Object.keys(user)
            var objdata = user[jsondata[0]];
            var Email = jQuery.parseJSON(user[jsondata[0]])['email'];
            var Name = jQuery.parseJSON(user[jsondata[0]])['name'];
            var FirstName = jQuery.parseJSON(user[jsondata[0]])['first_name'];
            var LastName = jQuery.parseJSON(user[jsondata[0]])['last_name'];
            
            if (Email == null) {
                showMessage("red", "could not connect to your account Your email is required for us to connect with your Facebook account.");
                return;
            }
            else {
                var input = {};
                input.Email = Email;
                input.Name = Name;
                input.FirstName = FirstName;
                input.LastName = LastName;
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
                hashes = hashes.replace('#', '');
                hashes = hashes.split('&');

                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                }
                ;
                if (hash[1] == undefined) {
                    return;
                }
                if (!hash[1].match(/^\d+$/)) {
                    return;
                }
                var usertype = hash[1];
                input.usertype = usertype;
                input.IsGoogleSignin = input.usertype;
                SignIn(input);
               // window.location.href = "https://localhost:44319/Signin/Signin.aspx";
            }

        },
        error: function (request, error) {
            console.log(arguments);
            alert(" Can't do because: " + error);
        },

    });
}

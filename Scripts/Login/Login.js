﻿
var base_url;
$(document).ready(function () {

    base_url = window.location.origin;

    var host = window.location.host;

    var pathArray = window.location.pathname.split('/');



     var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
    hashes = hashes.replace('#', '');
    hashes = hashes.split('&');

    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
    };
    if (hash[1] == undefined) {
        // return;
    }
    var ustype = hash[1];
    var GetElement = $('#Ldetails');
    GetElement.append(' <a href="' + base_url + '/signin/signin.aspx?id=' + ustype + '" class="link link--blue" style="cursor: pointer;">Signin</a>');
});


function ScreenLogin() {
    debugger;
    var inputData = {};
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
    hashes = hashes.replace('#', '');
    hashes = hashes.split('&');

    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
    }
    ;
    if (hash[1] == undefined) {
        return;
    }
    if (!hash[1].match(/^\d+$/)) {
        return;
    }
    var usertype = hash[1];
    
    var Email = $("#WorkEmail").val();
    if (Email == null || Email == "" || Email == undefined) {
        showMessage("red", "Please Enter Email");
        $("#WorkEmail").addClass("inputError");
        $("#WorkEmail").focus();
        return;
    } else {
        $("#WorkEmail").removeClass("inputError");
        inputData.Email = Email;
    }
    var Password = $("#PassWord").val();
    if (Password == null || Password == "" || Password == undefined) {
        showMessage("red", "Please Enter Password");
        $("#PassWord").addClass("inputError");
        $("#PassWord").focus();
        return;
    } else {
        $("#PassWord").removeClass("inputError");
        inputData.Password = Password;
    }
   
    inputData.usertype = usertype;
    ajaxCaller("POST", "LoginCSit.aspx/LoginScreendata", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
        if (data.d.Status == 1) {
            showMessage("green", data.d.Description, "");
            window.location.href = base_url + "/strip_payment_Way/stripePayment.aspx";
            //if (inputData.usertype == 1) {
            //    window.location.href = base_url+"/strip_payment_Way/stripePayment.aspx";
            //}
            //else {
            //    window.location.href = base_url + "/SubScriptionDetails/UserSubScriptionDetails.aspx";
            //}
        }
        else {
            showMessage("red", data.d.Description, "");
        }
    });
}
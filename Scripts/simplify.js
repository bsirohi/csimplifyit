$(document).ready(function () {

    $('body').on('click', '.list-group .list-group-item', function () {
        $(this).toggleClass('active');
    });
    $('.list-arrows button').click(function () {
        var $button = $(this), actives = '';
        if ($button.hasClass('move-left')) {
            actives = $('.list-right ul li.active');
            actives.clone().appendTo('.list-left ul');
            actives.remove();
        } else if ($button.hasClass('move-right')) {
            actives = $('.list-left ul li.active');
            actives.clone().appendTo('.list-right ul');
            actives.remove();
        }
    });
    $('.dual-list .selector').click(function () {
        var $checkBox = $(this);
        if (!$checkBox.hasClass('selected')) {
            $checkBox.addClass('selected').closest('.well').find('ul li:not(.active)').addClass('active');
            $checkBox.children('i').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
        } else {
            $checkBox.removeClass('selected').closest('.well').find('ul li.active').removeClass('active');
            $checkBox.children('i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
        }
    });
    $('[name="SearchDualList"]').keyup(function (e) {
        var code = e.keyCode || e.which;
        if (code == '9') return;
        if (code == '27') $(this).val(null);
        var $rows = $(this).closest('.dual-list').find('.list-group li');
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return ! ~text.indexOf(val);
        }).hide();
    });

    function searchSkills() {
        //var data =JSON.stringify("{'meta': {'token': 'dXNlclBhc3N3b3JkODA0NzY0NDgwNDc2NDQ=','userID': '2', 'role':'admin'},'data':[{'skill':'java'}]}") ;
        $.ajax({
            type: "POST",
            url: "/takemyjob.svc/getSkillsForUser",
            data: "{\"inputData\": \"{'meta': {'token': 'dXNlclBhc3N3b3JkODA0NzY0NDgwNDc2NDQ=','userID': '2', 'role':'admin'},'data':[{'skill':'java'}]}\"}",
            contentType: "application/json",
            dataType: "json",
            crossDomain: true,

            success: function (msg) {

                if (msg.status == 1) {

                    var skillsArray = msg.results;

                    jQuery.each(skillsArray, function (key, value) {

                        $("#list1").append('     <li id="' + skillsArray[key]['skillID'] + '" class="list-group-item">' + skillsArray[key]['skillName'] + '</li>');
                        return

                    });

                }
            }
        });
    }
    function getSkillsFromList2() {
        return $('#list2 li').map(function (i, n) {
            return $(n).attr('id');
        }).get().join(',');
    }
    $("#btnSubmit").click(function () {
        if (vallidationOnSave() == 1) {
            uploadSkills();
        }
    });

    function vallidationOnSave() {
        selectedSkills = getSkillsFromList2();
        if (selectedSkills == '') {
            alert("Select Skill for Url");
            return 0;
        }
        if ($("#UplineNo").val()=='') {
            alert("Please Enter Url ");
            return 0;
        }
        else {
            return 1;
        }
    }

    function uploadSkills() {
        //var data =JSON.stringify("{'meta': {'token': 'dXNlclBhc3N3b3JkODA0NzY0NDgwNDc2NDQ=','userID': '2', 'role':'admin'},'data':[{'skill':'java'}]}") ;
        var selectedSkills = getSkillsFromList2();
        $.ajax({
            type: "POST",
            url: "/takemyjob.svc/updateSkillWithURL",
            data: "{\"inputData\": \"{'meta': {'token': 'dXNlclBhc3N3b3JkODA0NzY0NDgwNDc2NDQ=','userID': '2', 'role':'admin'},'data':[{'skillList':'" + selectedSkills + "','url':'" + $("#UplineNo").val() + "'}]}\"}",
            contentType: "application/json",
            dataType: "json",
            crossDomain: true,

            success: function (msg) {

                if (msg.status == 1) {

                    var skillsArray = msg.results;

                    jQuery.each(skillsArray, function (key, value) {

                        $("#list1").append('     <li id="' + skillsArray[key]['skillID'] + '" class="list-group-item">' + skillsArray[key]['skillName'] + '</li>');
                        return

                    });

                }
            }
        });
    }


    searchSkills();
});

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="spreadIn_market.aspx.cs" Inherits="csimplifyit.WebForm15" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->
 <br />
 <div class="clear"></div>
 <hr />
<h1 class="text_blue">SpreadIn Market</h1>
 <div class="clear"></div>

 <div class="three_fourth">
 
 
 <p>SpreadIn Market helps you spread your wings! based on your strategies and plans, using C Simplify IT Affiliates. It helps you increase your online presence and  tracks help provided by affiliates. Its analytics and auto adjustment features keep you in auto pilot mode, while you focus on your key services. It makes sure Google sees your sight, tracks online linking of your site, advertise your business, creates web footprints, put it as nearby on maps,  improves site visibility, add social features on website and send alerts if your site rating goes down.</p>
 </div>
     
<div class="one_fourth column-last">
<img class="radius_10" src="images/SpreadIN.png" />
</div>
     
  
 </div>
</asp:Content>

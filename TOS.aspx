﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TOS.aspx.cs" Inherits="csimplifyit.TOS" %>--%>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <meta charset="utf-8">  
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>C Simplify IT</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.jpg">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.jpg">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicons/favicon.ico">
    <link rel="manifest" href="assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="assets/img/favicons/mstile-150x150.jpg">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="assets/css/theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/TOS.css">
</head>
<body>
    
    <!--===============================================-->
    <!--    Fancynav-->
    <!--===============================================-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#landing-page">
          <img src="assets/img/logo.png" alt="C Simplify IT" height="45" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown1">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#landing-page" >Home</a>
            <%--</li>--%>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" id="sln"  href="Default.aspx#whatwedo">Solutions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#offerings">Offerings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#technology">Technologies</a>
            </li>

               <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbarDropdownMenuProduct" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product</a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuProduct">
                <a class="dropdown-item" href="SM3/SimpleMassMailMerge.aspx">Simple Mass Mail Merge</a>
                <a class="dropdown-item" href="SM3/Simple_Recruitment_Manager.aspx">Simple Recruitment Manager</a>
                 <a class="dropdown-item" href="SM3/Simple_Workflow_Manager.aspx">Simple Workflow Manager</a>
                  <a class="dropdown-item" href="SPOS/SimplePointofSale.aspx">Simple Point of Sale</a>
                   <a class="dropdown-item" href="STDGDoc/SimpleTabularDocumentGenerator.aspx">Simple Tabular Document Generator</a>
                  
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#whyus">Why Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#clients">Our Clients</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact Us</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!--===============================================-->
    <!--    End of Fancynav-->
    <!--===============================================-->



    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main minh-100vh" id="top">


      <!-- ============================================-->
      <!-- Preloader ==================================-->
      <div class="preloader" id="preloader">
        <div class="loader">
          <div class="line-scale-pulse-out-rapid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
      <!-- ============================================-->
      <!-- End of Preloader ===========================-->

	  
	  <!-- ============================================-->
      <!-- <section> begin ============================-->
      <div class="speciality">
        <a href="Our_Specialities.aspx">Our Specialities</a>
      </div>
      <!-- <section> close ============================-->
      <!-- ============================================-->

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="text-center" id="privacyPolicy">

        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-12" data-zanim-trigger="scroll" data-zanim-timeline="{}">
              <div class="col-12 mb-3 mb-md-5 text-center overflow-hidden">
                <h2 class="fs-3 fs-sm-4" data-zanim-xs='{"delay":0.1}'><span class="text-underline">privacy policy</span></h2>
              </div>
              <div class="overflow-hidden">
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>Cues Simplify IT ("We", "Us", "Our", "C Simplify IT"), takes the client's ("You", "Your" or "User") privacy very seriously. The use of information collected through our service shall be limited to the purpose of providing the service for which our Clients have engaged us.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>Google sign-in will authenticate your identity and provide you the option to share certain personal information with us such as your email address. The information you provide is held in strict confidence. We collect this information to generate your user license, send payment receipts and communicate with the user about product updates.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>We may also send you service-related email announcements on rare occasions when it is necessary to do so. For instance, we may send out notifications regarding service related issues such as maintenance periods, billing problems, and other items that may impact service.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>Our services use the Google OAuth 2.0 protocol for authentication without requiring the user to share their login credentials with us. OAuth also allows secure access to the user’s data, as required by the add-ons, and the data resides strictly inside your Google account. We do not store any of your account data on our servers.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>As is true of most websites, we gather certain non-personal statistical information automatically and store it in cookies and log files. This information includes internet protocol (IP) addresses, browser version, operating system, date/time stamp, and other interactions with the application. We use this information to analyze trends, to identify potential cases of abuse and to help diagnose technical problems.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>If your personal information changes, or if you no longer desire our service, you may delete or deactivate it by uninstalling our Google add-on or by contacting us. We will respond to your request within 2 business days.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>We reserve the right to disclose your personal information as required by law, such as to comply with a subpoena, bankruptcy proceedings or similar legal process, and when we believe that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud and/or to comply with a judicial proceeding, court order, or legal process served on our website.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>We use other third parties payment processor Stripe, to bill you for services. We neither store nor have access to your credit card details.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>When you send emails through our add-ons, you may optionally choose to track behavior such as who opened the emails and who clicked the links. To do this, we include single-pixel gifs, also called web beacons, in the outgoing emails. Web beacons allow us to collect information about who opened the email, their IP address, browser or email client type, and other similar details. This information is securely stored inside secure databases and can only be retrieved by the user who sent the email.</p>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="text-center bg-light" id="termsOfService">

        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-12" data-zanim-timeline="{}" data-zanim-trigger="scroll">
              <div class="col-12 mb-3 mb-md-5 text-center">
                <h2 class="fs-3 fs-sm-4" data-zanim-xs='{"delay":0.2}'><span class="text-underline">terms of service</span></h2>
              </div>
              <div class="overflow-hidden">
                <p class="lead text-sans-serif" data-zanim-xs='{"delay":0.3}'>We take all measures reasonably necessary to protect User Personal Information from unauthorized access, alteration, or destruction; maintain data accuracy, and help ensure the appropriate use of User Personal Information. We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it. No method of transmission, or method of electronic storage, is 100% secure. Therefore, we cannot guarantee its absolute security.</p>
                <p class="lead text-sans-serif" data-zanim-xs='{"delay":0.3}'>Cues Simplify IT does not target its offerings toward, and does not knowingly collect any personal information from, users under 18 years of age.</p>
                <p class="lead text-sans-serif" data-zanim-xs='{"delay":0.3}'>In the event we go through a business transition, such as a merger, acquisition by another company, or sale of all or a portion of its assets, your personal information will likely be among the assets transferred. You will be notified via email of any such change in ownership or control of your personal information.</p>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="text-center" id="warrantiesNDisclaimers">

        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-12" data-zanim-trigger="scroll" data-zanim-timeline="{}">
              <div class="col-12 mb-3 mb-md-5 text-center overflow-hidden">
                <h2 class="fs-3 fs-sm-4" data-zanim-xs='{"delay":0.3}'><span class="text-underline">our warranties and disclaimers</span></h2>
              </div>
              <div class="overflow-hidden">
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.4}'>We provide our Services using a commercially reasonable level of skill and care and we hope that you will enjoy using them. But there are certain things that we don’t promise about our Services. We provide the services on an "as is" basis.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.4}'>We make no warranties, expressed or implied, and hereby disclaim and negate all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. In no event shall C Simplify IT be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use our software and services.</p>
                <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.4}'>We encourage you to periodically review this page for the latest information on our privacy practices.</p>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->



      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="text-sans-serif" id="contact">

        <div class="container">
          <div class="row">
            <div class="col-lg-4 pr-lg-5">
              <h3 class="mb-3">C Simplify IT</h3>
              <p>We are a specialized Global IT services & solution organization established in 2011, with a team of 120+ consulting & engineering professionals spread across its delivery centers at India (Gurugram), USA (Fremont, CA) . We are a specialist IT Consulting firm, leveraging innovation & passion at its best, C Simplify IT is trusted by Fortune 500 companies and start-ups to help business to add value across our clients worldwide. Our clients are based in USA, Singapore, Malaysia, Switzerland, UK, Hong Kong, and India.</p>
            </div>
            <div class="col-md-6 col-lg-4 pr-lg-6 ml-lg-auto mt-6 mt-lg-0">             
              <h3 class="mb-3">Socials</h3>
              <p class="mb-0">Stay connected with C Simplify IT via our social media pages. If you haven't taken the opportunity to visit our social media pages, please do so. It's a great way to interact with us, get your questions answered and make suggestions.</p>
              <br/>
              <a class="btn btn-dark btn-sm mr-2" href="https://www.facebook.com/CSimplifyIT-210115279023481/" target="_blank"><span class="fab fa-facebook-f"></span></a>
              <a class="btn btn-dark btn-sm" href="https://in.linkedin.com/company/c-simplify-it-services-private-limited" target="_blank"><span class="fab fa-linkedin-in"></span></a>
            </div>
            <div class="col-md-6 col-lg-4 mt-6 mt-lg-0 office-address">
              <h3 class="mb-3">Get in touch</h3>
              <address><b><u>India Head Office:</u></b>
                <br/>Plot No. 52, II Floor,
                <br/> Sector 32, Gurgaon (Haryana) - 122003
                <br/><b>Call Us:</b> <a href="tel:+91 98999762274">+91 9899976227</a> (For Tech Queries),<br /><a href="tel:+91 8950230044">+91 8950230044</a> (For Business Queries)
                <br/><b>E-Mail:</b> <a href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
                <br/><br/><b><u>US Head Office:</u></b>
                <br/>46878 Fernald St, Fremont, CA 94539, USA
                <br/><b>Call Us:</b>  <a href="tel:+1 408 987 5597">+1 408 987 5597</a>
                <br/><b>E-Mail:</b> <a href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
              </address>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

 </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!--===============================================-->
    <!--    Footer-->
    <!--===============================================-->
    <footer class="footer bg-black text-600 py-4 text-sans-serif text-center overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-4 order-lg-2">
            <a class="indicator indicator-up js-scroll-trigger" href="#top"><span class="indicator-arrow indicator-arrow-one" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":0.9}'></span><span class="indicator-arrow indicator-arrow-two" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":1.05}'></span></a>
          </div>
          <div class="col-lg-4 text-lg-left mt-4 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              Copyright &copy; 2018 C Simplify IT</p>
          </div>
          <div class="col-lg-4 text-lg-right order-lg-2 mt-2 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              <a class="text-600" href="TOS.aspx">Terms of Service</a>
            </p>
          </div> 
        </div>
      </div>
    </footer>


    <!--===============================================-->
    <!--    Modal for language selection-->
    <!--===============================================-->
    <div class="remodal bg-black remodal-select-language" data-remodal-id="language">
      <div class="remodal-close" data-remodal-action="close"></div>
      <ul class="list-unstyled pl-0 my-0 py-4 text-sans-serif">
        <li>
          <a class="pt-1 d-block text-white font-weight-semi-bold" href="#">English</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="#">Français</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="page-rtl.html">عربى</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="#">Deutsche</a>
        </li>
      </ul>
    </div>


    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    
     <script src="assets/js/scrolling-nav.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/lib/loaders.css/loaders.css.js"></script>
    <script src="assets/js/stickyfill.min.js"></script>
    <script src="assets/lib/remodal/remodal.js"></script>
    <script src="assets/lib/jtap/jquery.tap.js"></script>
    <script src="assets/js/rellax.min.js"></script>
    <script src="assets/lib/owl.carousel/owl.carousel.js"></script>
    <script src="assets/lib/lightbox2/js/lightbox.js"></script>
    <script src="assets/lib/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/lib/isotope-packery/packery-mode.pkgd.min.js"></script>
    <script src="assets/js/theme.js"></script>   
</body>
</html>


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StartupItPractices : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
     
    }

    /// <summary>
    /// This method for  Send Mail
    /// </summary>
    public void sendMailForUser()
    {
        string getTexValue = txtEmailid.Value.ToString();
      


        string lsmessage = string.Empty;
        //if (getExcelData > 0)
        //{
        //    for (int i = 0; i < getExcelData; i++)
        //    {
        //        lsmessage = "<table border=1; cellspacing=0; style='width: 570px;'>";
        //        lsmessage = lsmessage + @"<tr><th width=""15%"">Custome Name</th> <th width=""15%"">Industry Segment</th> <th width=""15%"">Location</th><th width=""15%"">Status</th></tr>";
        //        lsmessage = lsmessage + " <tr>";
        //        lsmessage = lsmessage + " <td></td>";
        //        lsmessage = lsmessage + " </tr>";
        //        lsmessage = lsmessage + " </table>";
        //    }

        //}
        //else
        //{
        //    lsmessage = "<table border=1; cellspacing=0; style='width: 570px;'>";
        //    lsmessage = lsmessage + @"<tr><th width=""15%"">Custome Name</th> <th width=""15%"">Industry Segment</th> <th width=""15%"">Location</th><th width=""15%"">Status</th></tr>";
        //    lsmessage = lsmessage + " <tr>";
        //    lsmessage = lsmessage + " <td  colspan='4'><center>No any duplicate record found </center></td>";
        //    lsmessage = lsmessage + " </tr>";
        //    lsmessage = lsmessage + " </table>";
        //}
        string lsGetURl = string.Empty;
        string lsEmailAddress = string.Empty;
        lsEmailAddress = getTexValue;
        if (lsEmailAddress != null && lsEmailAddress != "" && lsEmailAddress != string.Empty)
        {
            string[] msg = new string[5];
            msg[0] = "Greeting from CSimplify IT.<br> Hope you are having a wonderful day. <br><br>Please find attachment the document for Startup Best IT Practices you requested.<br><br> Please contact Mr.Kshitij Dhamija for further queries.<br>Email ID: Kshitij@csimplifyit.com<br>Phone Number:+91 8950230044";
            msg[1] = lsGetURl;
            msg[2] = lsmessage;
            //Task Thetask = Task.Run(() =>
            sendMailWithTemplate("C Simplify IT Startup Best IT Practices CheckList", lsEmailAddress, msg, "templates/FileUplaodEmailTemplate.html");
        }
    }

    /// <summary>
    /// Template for send mail
    /// </summary>
    /// <param name="emailsubject"></param>
    /// <param name="address"></param>
    /// <param name="message"></param>
    /// <param name="path"></param>
    /// <returns></returns>
    public void sendMailWithTemplate(string emailsubject, string address, string[] message, string path)
    {
       
        string[] result = new string[2];
        var fromAddress = new MailAddress("satish@csimplifyit.com", "Satish");
        try
        {
            string str_uploadpath = Path.Combine(HttpRuntime.AppDomainAppPath, path);
            string body = System.IO.File.ReadAllText(str_uploadpath);
            var toAddress = new MailAddress(address);
            string subject = emailsubject;
            using (SmtpClient client = CreateMailClient(fromAddress))
            {
               
                using (MailMessage message1 = msg(fromAddress, toAddress, emailsubject, body))
                {
                    client.Send(message1);
                }
            }
        }
        catch (Exception ex)
        {
            lblShowErrormessage.Text = ex.ToString();
            
        }
    }
    /// <summary>
    /// Set email id in bcc and cc 
    /// </summary>
    /// <param name="fromAddress"></param>
    /// <param name="toAddress"></param>
    /// <param name="subject"></param>
    /// <param name="body"></param>
    /// <returns></returns>
    public MailMessage msg(MailAddress fromAddress, MailAddress toAddress, string subject, string body)
    {
        MailAddress addressBcc1 = new MailAddress("satish@csimplifyit.com");
        MailAddress Bcc1 = new MailAddress("harnoor@csimplifyit.com");
      
        Attachment loAttachment = new Attachment(Server.MapPath("~/Templates/Best IT Practices.xlsx"));
        var message = new MailMessage(fromAddress, toAddress)
        {
            Subject = subject,
            Body = body,
            IsBodyHtml = true,
        };
        message.Attachments.Add(loAttachment);
        message.CC.Add(addressBcc1);
        message.Bcc.Add(Bcc1);
        return message;
    }

    /// <summary>
    /// Create mail Client
    /// </summary>
    /// <param name="fromAddress"></param>
    /// <returns></returns>
    public SmtpClient CreateMailClient(MailAddress fromAddress)
    {
        const string fromPassword = "Sa420@_CsimplifyIt";
        var client = new SmtpClient
        {
            Host = "smtp.gmail.com",
            Port = 587,
            EnableSsl = false,
            DeliveryMethod = SmtpDeliveryMethod.Network,
            
            Credentials = new NetworkCredential(fromAddress.Address, fromPassword),

        };
        return client;
    }


    /// <summary>
    /// Create Attachment for file
    /// </summary>
    /// <param name="excelContent"></param>
    /// <returns></returns>
    Attachment WrapExcelBytesInAttachment(byte[] excelContent)
    {
        try
        {
            Stream stream = new MemoryStream(excelContent);
            Attachment attachment = new Attachment(stream, "fileName.xls");
            attachment.ContentType = new ContentType("application/vnd.ms-excel");
            return attachment;
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    /// <summary>
    /// Event for Button Click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        sendMailForUser();
    }
}
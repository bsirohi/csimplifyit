-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: csimplifydb
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `helperkeywords`
--

DROP TABLE IF EXISTS `helperkeywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helperkeywords` (
  `helperKeywordId` int(11) NOT NULL AUTO_INCREMENT,
  `keywords` varchar(2000) NOT NULL,
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`helperKeywordId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `helperkeywords`
--

LOCK TABLES `helperkeywords` WRITE;
/*!40000 ALTER TABLE `helperkeywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `helperkeywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `helperkeywordslink`
--

DROP TABLE IF EXISTS `helperkeywordslink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helperkeywordslink` (
  `helperKeywordLinkId` int(11) NOT NULL AUTO_INCREMENT,
  `helperKeywordId` int(11) NOT NULL COMMENT 'Foreign Key to "helperKeywordId" in table "helperkeywords"',
  `keywordId` int(11) NOT NULL COMMENT 'Foreign Key to "keywordId" in table "keywords"',
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`helperKeywordLinkId`),
  KEY `FK_helperkeywordslink_1` (`helperKeywordId`),
  KEY `FK_helperkeywordslink_2` (`keywordId`),
  CONSTRAINT `FK_helperkeywordslink_1` FOREIGN KEY (`helperKeywordId`) REFERENCES `helperkeywords` (`helperKeywordId`),
  CONSTRAINT `FK_helperkeywordslink_2` FOREIGN KEY (`keywordId`) REFERENCES `keywords` (`keywordId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `helperkeywordslink`
--

LOCK TABLES `helperkeywordslink` WRITE;
/*!40000 ALTER TABLE `helperkeywordslink` DISABLE KEYS */;
/*!40000 ALTER TABLE `helperkeywordslink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keywords`
--

DROP TABLE IF EXISTS `keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keywords` (
  `keywordId` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` text NOT NULL,
  `mandatory` int(1) NOT NULL DEFAULT '0' COMMENT '"0"-optional and "1"-mandatory"',
  `rank` int(2) DEFAULT NULL,
  `locationSubURLId` int(11) DEFAULT NULL,
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`keywordId`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keywords`
--

LOCK TABLES `keywords` WRITE;
/*!40000 ALTER TABLE `keywords` DISABLE KEYS */;
INSERT INTO `keywords` VALUES (8,'kindercare,kinder care,kindercare locations,kindercare gurugram,kindercare delhi,kindercare gurgoan,best daycare in gurgoan,best play school in india',1,NULL,20,NULL,NULL,NULL,'2018-08-28 08:46:13'),(9,'pumpkin house ,pumpkin play school,pumpkin house play school,pumpkin house play school gurugram haryana,best play school and daycare in gurgaon,play schools in dlf phase 1 gurgaon',1,NULL,21,NULL,NULL,NULL,'2018-08-28 08:42:03'),(10,'little unicorn,little unicorn daycareLittle Unicorn\'s International Garderie,Introducing a new brand for diaper bags, muslin blankets, luxury blankets, and car seat accessories,Gurgaon, India,',1,NULL,1,NULL,NULL,NULL,'2018-08-28 08:44:05'),(11,'mini marvels,best daycare,best dyacare in india,best daycare gurugram,best play school in gurgoan',1,NULL,7,NULL,NULL,NULL,'2018-08-28 08:47:33'),(12,'the sixth element day care gurgaon,maandpa ,beast daycare in gurugram,best daycare in ncr delhi',1,NULL,2,NULL,NULL,NULL,'2018-08-28 08:48:52'),(13,'kinder plume,kinder plume preschool & day care gurugram haryana,day care in sector 56 gurgaon,playschools in gurgaon sector 56',1,NULL,8,NULL,NULL,NULL,'2018-08-28 08:50:02'),(14,'About Us-Apprentice Work Portal,about us,about us templates,about us page,about us awp,awp About Us,Apprentice About Us,Apprentice portal about us',1,NULL,19,NULL,NULL,NULL,'2018-05-22 10:20:28'),(15,'Contact Us-Apprentice Work Portal,contact us page,contact us template,Contact us Apptrentice,Contact Us Apprentice portal,Contact us awp,awp contact us,work portal contact us,awp contact,Apprentice Contact,Apprentice portal contact',1,NULL,18,NULL,NULL,NULL,'2018-05-22 10:20:28'),(16,'Art Supplies,Pets Art,Electronic Art,Nail Art,Pixel Art,Clip Art,creative arts agency,wall art,wallpaper art,martial arts,nail art designs,arts and craft,paper art,nude art,how great thou art,art supplies online,art supplies list,art supplies for kids,art supplies for sketching.',1,NULL,16,NULL,NULL,NULL,'2018-05-22 10:24:04'),(17,'Placement Training,Best job Placement Training,placement cell,training methodology,placement support training,placement assured training,placement assistance training,placement training best sites,	placement training activities,	placement training cours',1,NULL,17,NULL,NULL,NULL,'2018-05-22 10:24:04'),(18,'portfolio definition, dictionary, english, british, american, business, british english, thesaurus, define portfolio, portfolio meaning, what is portfolio, spelling, conjugation, audio pronunciation, free, online, english,Best Portfolio Manager, mutual funds investing, mutual funds investment, stock mutual funds, money market mutual funds, top mutual funds, best mutual funds, best performing mutual funds',1,NULL,9,NULL,NULL,NULL,'2018-05-22 10:44:12'),(19,'Adelaide, University, Australia, global, study, overseas, international, opportunities, holidays, learning, pathway, programs, costs, departure, tour, world, over seas, graduate, destinations',1,NULL,10,NULL,NULL,NULL,'2018-05-22 10:48:56'),(20,'study abroad, study abroad guide, study abroad help, study abroad information,study abroad, student guide, why study abroad, how to study abroad',1,NULL,11,NULL,NULL,NULL,'2018-05-22 10:52:24'),(21,'trust iskra, iskra power division, efficient solution, digitalization, digitalization 3.0, digitalization 4.0, manufacture, manufacturing, production, digital, efficiency, autonomous production, autonomous ',1,NULL,12,NULL,NULL,NULL,'2018-05-22 11:00:05'),(22,'Student internship, summer internship, winter internship, summer internships in gurgaon, internships in gurgaon, interns, internship program, internship in india, global internship, global internship program, find internship in india, interns for company, students internship, company internship, interns, internship, internships, find internships,  internship india, engineering internships in india, interns for company, paid internship,it internship in Gurgaon, engineering internships in india, MBA Internships in Gurgaon, Marketing Internships in Gurgaon, Media Internships in Gurgaon, Internships in Gurgaon, Internship in Gurgaon, Engineering internships in Gurgaon, Finance internships in Gurgaon, online internship   in Gurgaon,summer internship  in Gurgaon ,Student Internships in Gurgaon,Virtual Internships in Gurgaon ',1,NULL,13,NULL,NULL,NULL,'2018-05-22 11:03:26'),(23,'E-Learning, Software Architecture, Software Product Line, Web Services,AUTOSAR, E-Learning, wiki, online learning, introduction, Automotive Open System Architecture, fundamentals,e-learning software, elearning software, e-learning software system, e-learning software systems, elearning software, elearning software system, web-based elearning software, elearning software solution, elearning software solutions, hosted e-learning management system',1,NULL,14,NULL,NULL,NULL,'2018-05-22 11:12:25'),(24,'kindercare,kinder care,kindercare locations,kindercare gurugram,kindercare delhi,kindercare gurgoan,best daycare in gurgoan,best play school in india',1,NULL,15,NULL,NULL,NULL,'2018-08-28 08:46:13'),(41,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model',1,NULL,29,NULL,NULL,NULL,'2018-09-26 11:15:01'),(42,'Our Retail practice offers a comprehensive suite of Front End/Back End IT Services spanning consulting, enterprise services, implementations, and functionality additions. Our robust retail process knowledge, specialized tools, and consulting expertise helps clients to enhance retail processes efficiency and fulfillment precision, reduce costs and decrease cycle times from order to fulfillment.',1,NULL,30,NULL,NULL,NULL,'2018-09-26 11:15:02'),(43,'YOCarry Transport Management System with illiterate design, A TMS system designed to be used by people who cannot read/write. Be 10X better.',1,NULL,31,NULL,NULL,NULL,'2018-09-26 11:15:02'),(44,'Process Re-engineering using BPM Publishing processes from production and editorial, to content management, rights management and ERP integration. Assets management for dynamic content and digital rights management. Digital Media content lifecycle and distribution Social CRM manage information with the ever-growing social media channel by implementing new approaches to data analytics and CRM Use Tools to manage Media and Publishing Marklogic, Adobe, Bonitasoft BPM, Push BIZ IN',1,NULL,32,NULL,NULL,NULL,'2018-09-26 11:16:23'),(45,'We focus on, Enabling new channels, implementing processes, integrating IT systems for synergies and ensuring compliance in Core Banking, Loans and Mortgages, Treasury & Cash, Trade Finance, Un-secure lending, multi channel and personal banking.',1,NULL,33,NULL,NULL,NULL,'2018-09-26 11:16:23'),(46,'C Simplify IT involving in the design, development, creation, use and maintenance of information systems for the healthcare industry. Automated and interoperable healthcare information systems are expected to lower costs, improve efficiency and reduce error.We are also providing better consumer care and services.',1,NULL,34,NULL,NULL,NULL,'2018-09-26 11:19:10'),(47,'Make your clients fall in love with your magical offering without sending emails/sms/physical mails.Just be visible to your clients when you wish to. Increase traffic to your website upto 10X.',1,NULL,35,NULL,NULL,NULL,'2018-09-26 11:19:10'),(48,'C Simplify IT is focussed on cutting the supports cost and has build SIMS to stabilise the web systems faster and reduce the to & fro exchange mails/phone calls on support incidences.',1,NULL,36,NULL,NULL,NULL,'2018-09-26 11:19:10'),(49,'Their energies, ways of thinking and passion to do something important is unique. Welcome, we will help you gain respect which you deserve.',1,NULL,37,NULL,NULL,NULL,'2018-09-26 11:20:59'),(50,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,38,NULL,NULL,NULL,'2018-09-26 05:24:02'),(51,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,39,NULL,NULL,NULL,'2018-09-26 05:24:02'),(52,'Capture payments using Multiple Payment Tenders - Cash, Credit Card, Gift Vouchers.',1,NULL,40,NULL,NULL,NULL,'2018-09-26 11:20:59'),(53,'In SYNCH \"Integrates\" Your Ecommerce Business, Suppliers, Vendors, and Customers in many ways.',1,NULL,41,NULL,NULL,NULL,'2018-09-26 11:22:23'),(54,'These days, in technology and digital marketing circles, \" bots \" are a frequent source of discussion. By definition, a bot (short for \"web robot\") is a software program that operates as an agent for an individual, group of individuals, organization or even legitimate businesses.',1,NULL,42,NULL,NULL,NULL,'2018-09-26 11:22:23'),(55,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,43,NULL,NULL,NULL,'2018-09-26 05:24:02'),(56,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,44,NULL,NULL,NULL,'2018-09-26 05:24:02'),(57,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,45,NULL,NULL,NULL,'2018-09-26 11:36:57'),(58,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,46,NULL,NULL,NULL,'2018-09-26 11:36:57'),(59,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,47,NULL,NULL,NULL,'2018-09-26 11:36:57'),(60,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,48,NULL,NULL,NULL,'2018-09-26 11:36:57'),(61,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,49,NULL,NULL,NULL,'2018-09-26 11:36:57'),(62,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,50,NULL,NULL,NULL,'2018-09-26 11:36:57'),(63,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',1,NULL,51,NULL,NULL,NULL,'2018-09-26 11:36:57'),(64,'simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us',0,NULL,52,NULL,NULL,NULL,'2018-09-27 05:31:53');
/*!40000 ALTER TABLE `keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locationsuburlkeywordlink`
--

DROP TABLE IF EXISTS `locationsuburlkeywordlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationsuburlkeywordlink` (
  `locationSubURLKeywordLinkId` int(11) NOT NULL AUTO_INCREMENT,
  `locationSubURLId` int(11) NOT NULL COMMENT 'Foreign Key to "locationSubURLId" in table "locationsuburls"',
  `keywordId` int(11) NOT NULL COMMENT 'Foreign Key to "keywordId" in table "keywords"',
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`locationSubURLKeywordLinkId`),
  KEY `FK_locationsuburlkeywordlink_1` (`locationSubURLId`),
  KEY `FK_locationsuburlkeywordlink_2` (`keywordId`),
  CONSTRAINT `FK_locationsuburlkeywordlink_1` FOREIGN KEY (`locationSubURLId`) REFERENCES `locationsuburls` (`locationSubURLId`),
  CONSTRAINT `FK_locationsuburlkeywordlink_2` FOREIGN KEY (`keywordId`) REFERENCES `keywords` (`keywordId`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locationsuburlkeywordlink`
--

LOCK TABLES `locationsuburlkeywordlink` WRITE;
/*!40000 ALTER TABLE `locationsuburlkeywordlink` DISABLE KEYS */;
INSERT INTO `locationsuburlkeywordlink` VALUES (7,13,8,NULL,NULL,NULL,'2018-08-28 08:28:31'),(8,24,9,NULL,NULL,NULL,'2018-08-28 08:17:26'),(9,25,10,NULL,NULL,NULL,'2018-08-28 08:17:26'),(10,26,11,NULL,NULL,NULL,'2018-08-28 08:17:26'),(11,27,12,NULL,NULL,NULL,'2018-08-28 08:17:26'),(12,28,13,NULL,NULL,NULL,'2018-08-28 08:17:26'),(13,1,14,NULL,NULL,NULL,'2018-08-28 08:17:26'),(14,18,15,NULL,NULL,NULL,'2018-05-22 10:21:08'),(15,16,16,NULL,NULL,NULL,'2018-05-22 10:25:10'),(16,17,17,NULL,NULL,NULL,'2018-05-22 10:25:10'),(17,9,18,NULL,NULL,NULL,'2018-05-22 10:44:34'),(18,10,19,NULL,NULL,NULL,'2018-05-22 10:49:35'),(19,11,20,NULL,NULL,NULL,'2018-05-22 11:00:38'),(20,12,21,NULL,NULL,NULL,'2018-05-22 11:00:39'),(21,13,22,NULL,NULL,NULL,'2018-05-22 11:03:50'),(22,14,23,NULL,NULL,NULL,'2018-05-22 11:12:40'),(23,23,24,NULL,NULL,NULL,'2018-08-28 08:27:34'),(24,29,41,NULL,NULL,NULL,'2018-09-25 13:26:59'),(25,30,42,NULL,NULL,NULL,'2018-09-25 13:45:17'),(26,31,43,NULL,NULL,NULL,'2018-09-25 13:45:17'),(27,32,44,NULL,NULL,NULL,'2018-09-25 16:41:21'),(28,33,45,NULL,NULL,NULL,'2018-09-25 16:41:21'),(29,34,46,NULL,NULL,NULL,'2018-09-25 16:41:21'),(30,35,47,NULL,NULL,NULL,'2018-09-25 16:41:21'),(31,36,48,NULL,NULL,NULL,'2018-09-25 16:41:21'),(32,37,49,NULL,NULL,NULL,'2018-09-25 16:41:21'),(33,38,50,NULL,NULL,NULL,'2018-09-25 16:41:21'),(34,39,51,NULL,NULL,NULL,'2018-09-25 16:41:21'),(35,40,52,NULL,NULL,NULL,'2018-09-25 16:41:21'),(36,41,53,NULL,NULL,NULL,'2018-09-26 11:38:38'),(37,42,54,NULL,NULL,NULL,'2018-09-26 11:38:38'),(38,43,55,NULL,NULL,NULL,'2018-09-26 11:38:38'),(39,44,56,NULL,NULL,NULL,'2018-09-26 11:38:38'),(40,45,57,NULL,NULL,NULL,'2018-09-26 11:38:38'),(41,46,58,NULL,NULL,NULL,'2018-09-26 11:38:38'),(42,47,59,NULL,NULL,NULL,'2018-09-26 11:38:39'),(43,48,60,NULL,NULL,NULL,'2018-09-26 11:38:39'),(44,49,61,NULL,NULL,NULL,'2018-09-26 11:38:39'),(45,50,62,NULL,NULL,NULL,'2018-09-26 11:38:39'),(46,51,63,NULL,NULL,NULL,'2018-09-26 11:38:39'),(47,52,55,NULL,NULL,NULL,'2018-09-27 05:33:48');
/*!40000 ALTER TABLE `locationsuburlkeywordlink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locationsuburls`
--

DROP TABLE IF EXISTS `locationsuburls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationsuburls` (
  `locationSubURLId` int(11) NOT NULL AUTO_INCREMENT,
  `subURL` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LocCode` int(11) NOT NULL COMMENT 'Foreign key to "LocCode" in table "location"',
  `tcid` int(11) DEFAULT NULL,
  `tsid` int(11) DEFAULT NULL,
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`locationSubURLId`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locationsuburls`
--

LOCK TABLES `locationsuburls` WRITE;
/*!40000 ALTER TABLE `locationsuburls` DISABLE KEYS */;
INSERT INTO `locationsuburls` VALUES (1,'/PeitzkersPrizeCaseStudies',3,NULL,1477,NULL,NULL,NULL,'2018-05-22 10:03:24'),(2,'/Workshops',3,NULL,911,NULL,NULL,NULL,'2018-05-22 10:03:24'),(7,'/ThesisSupport',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:03:50'),(8,'/TutorialsForArchitectureCredits',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:03:50'),(9,'/Portfoliodeck',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:04:47'),(10,'/StudyTours',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:04:47'),(11,'/OverseasStudyGuides',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:04:47'),(12,'/Eproduction',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:04:47'),(13,'/Internships',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:04:47'),(14,'/E-learningForSoftwareProgramsForArchitecture',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:06:54'),(15,'/ArchiPod',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:10:10'),(16,'/ArtSupplies',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:10:10'),(17,'/PlacementTraining',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:10:10'),(18,'/ContactUs',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:10:10'),(19,'/AboutUs',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:10:10'),(20,'/LanguageCourseOnline',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:10:10'),(21,'/SmartCitiesInfo',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:10:10'),(22,'/DoItYourselfSitesLinks',3,NULL,NULL,NULL,NULL,NULL,'2018-05-22 10:10:10'),(23,'/Daycare/Kinder-Care',27,NULL,NULL,1,'2018-08-27 23:38:07',NULL,'2018-08-27 18:08:06'),(24,'/Daycare/Pumpkin-House-Playschool',28,NULL,NULL,1,'2018-08-27 23:38:07',NULL,'2018-08-27 18:08:07'),(25,'/Daycare/Little-Unicorn',32,NULL,NULL,1,'2018-08-27 23:38:07',NULL,'2018-08-27 18:08:07'),(26,'/Daycare/Mini-Marvel',34,NULL,NULL,1,'2018-08-27 23:38:07',NULL,'2018-08-27 18:08:07'),(27,'/Daycare/The-Sixth-Element',35,NULL,NULL,1,'2018-08-27 23:38:07',NULL,'2018-08-27 18:08:07'),(28,'/Daycare/Kinder-Plume',40,NULL,NULL,1,'2018-08-27 23:38:07',NULL,'2018-08-27 18:08:07'),(29,'/technology.aspx',41,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(30,'/retail.aspx',42,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(31,'/transport-management.aspx',43,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(32,'/mediapublish.aspx',44,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(33,'/financial.aspx',43,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(34,'/healthcare.aspx',44,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(35,'/pushbiz.aspx',45,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(36,'/sims.aspx',46,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(37,'/freshervilla.aspx',47,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(38,'/transport-management-system.aspx',48,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(39,'/tallent-nest.aspx',49,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(40,'/smartpos.aspx',50,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(41,'/insynch.aspx',51,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(42,'/bots-machinelearning.aspx',52,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(43,'/bpm-services.aspx',53,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(44,'/e-commerce.aspx',54,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(45,'/geo-based-iot-internet-of-thing-services.aspx',55,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(46,'/artificial-intelligence-services.aspx',56,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(47,'/cloud_integrations.aspx',57,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(48,'/mobility_apps.aspx',58,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-24 05:40:33'),(49,'/testing.aspx',59,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-24 05:40:33'),(50,'/aboutus.aspx',60,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(51,'/contact.aspx',61,NULL,NULL,1,'2018-09-24 00:00:00',NULL,'2018-09-23 18:30:00'),(52,'/default.aspx',55,NULL,NULL,1,'2018-09-27 00:00:00',NULL,'2018-09-26 18:30:00');
/*!40000 ALTER TABLE `locationsuburls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locationsuburltaglink`
--

DROP TABLE IF EXISTS `locationsuburltaglink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locationsuburltaglink` (
  `locationSubURLTagLinkId` int(11) NOT NULL AUTO_INCREMENT,
  `locationSubURLId` int(11) NOT NULL COMMENT 'Foreign Key to "locationSubURLId" in table "locationsuburls"',
  `tagId` int(11) NOT NULL COMMENT 'Foreign key to "tagId" in table "tagmaster"',
  `tagValue` text,
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`locationSubURLTagLinkId`),
  KEY `FK_locationsuburltaglink_1` (`locationSubURLId`),
  KEY `FK_locationsuburltaglink_2` (`tagId`),
  CONSTRAINT `FK_locationsuburltaglink_1` FOREIGN KEY (`locationSubURLId`) REFERENCES `locationsuburls` (`locationSubURLId`),
  CONSTRAINT `FK_locationsuburltaglink_2` FOREIGN KEY (`tagId`) REFERENCES `tagmaster` (`tagId`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locationsuburltaglink`
--

LOCK TABLES `locationsuburltaglink` WRITE;
/*!40000 ALTER TABLE `locationsuburltaglink` DISABLE KEYS */;
INSERT INTO `locationsuburltaglink` VALUES (1,1,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"About Us-Apprentice Work Portal,about us,about us templates,about us page,about us awp,awp About Us,Apprentice About Us,Apprentice portal about us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"About Us-Apprentice Work Portal,about us,about us templates,about us page,about us awp,awp About Us,Apprentice About Us,Apprentice portal about ussimplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysisAbout Us-Apprentice Work Portal,about us,about us templates,about us page,about us awp,awp About Us,Apprentice About Us,Apprentice portal about us\">\n	 ',NULL,NULL,NULL,'2018-09-26 09:07:49'),(2,29,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 11:22:34'),(3,30,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" Our Retail practice offers a comprehensive suite of Front End/Back End IT Services spanning consulting, enterprise services, implementations, and functionality additions. Our robust retail process knowledge, specialized tools, and consulting expertise helps clients to enhance retail processes efficiency and fulfillment precision, reduce costs and decrease cycle times from order to fulfillment. C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"Our Retail practice offers a comprehensive suite of Front End/Back End IT Services spanning consulting, enterprise services, implementations, and functionality additions. Our robust retail process knowledge, specialized tools, and consulting expertise helps clients to enhance retail processes efficiency and fulfillment precision, reduce costs and decrease cycle times from order to fulfillment.,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-26 11:22:34'),(4,31,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"YOCarry Transport Management System with illiterate design, A TMS system designed to be used by people who cannot read/write. Be 10X better.,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"YOCarry Transport Management System with illiterate design, A TMS system designed to be used by people who cannot read/write. Be 10X better.simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysisYOCarry Transport Management System with illiterate design, A TMS system designed to be used by people who cannot read/write. Be 10X better.\">\n	 ',NULL,NULL,NULL,'2018-09-26 11:22:34'),(5,32,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., Process Re-engineering using BPM Publishing processes from production and editorial, to content management, rights management and ERP integration. Assets management for dynamic content and digital rights management. Digital Media content lifecycle and distribution Social CRM manage information with the ever-growing social media channel by implementing new approaches to data analytics and CRM Use Tools to manage Media and Publishing Marklogic, Adobe, Bonitasoft BPM, Push BIZ IN\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"Process Re-engineering using BPM Publishing processes from production and editorial, to content management, rights management and ERP integration. Assets management for dynamic content and digital rights management. Digital Media content lifecycle and distribution Social CRM manage information with the ever-growing social media channel by implementing new approaches to data analytics and CRM Use Tools to manage Media and Publishing Marklogic, Adobe, Bonitasoft BPM, Push BIZ IN,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 11:49:54'),(6,33,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" We focus on, Enabling new channels, implementing processes, integrating IT systems for synergies and ensuring compliance in Core Banking, Loans and Mortgages, Treasury & Cash, Trade Finance, Un-secure lending, multi channel and personal banking. C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"We focus on, Enabling new channels, implementing processes, integrating IT systems for synergies and ensuring compliance in Core Banking, Loans and Mortgages, Treasury & Cash, Trade Finance, Un-secure lending, multi channel and personal banking.,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-26 11:22:34'),(7,34,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"C Simplify IT involving in the design, development, creation, use and maintenance of information systems for the healthcare industry. Automated and interoperable healthcare information systems are expected to lower costs, improve efficiency and reduce error.We are also providing better consumer care and services.,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"C Simplify IT involving in the design, development, creation, use and maintenance of information systems for the healthcare industry. Automated and interoperable healthcare information systems are expected to lower costs, improve efficiency and reduce error.We are also providing better consumer care and services.simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysisC Simplify IT involving in the design, development, creation, use and maintenance of information systems for the healthcare industry. Automated and interoperable healthcare information systems are expected to lower costs, improve efficiency and reduce error.We are also providing better consumer care and services.\">\n	 ',NULL,NULL,NULL,'2018-09-26 11:22:35'),(8,35,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., Make your clients fall in love with your magical offering without sending emails/sms/physical mails.Just be visible to your clients when you wish to. Increase traffic to your website upto 10X.\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"Make your clients fall in love with your magical offering without sending emails/sms/physical mails.Just be visible to your clients when you wish to. Increase traffic to your website upto 10X.,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 11:22:35'),(9,36,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" C Simplify IT is focussed on cutting the supports cost and has build SIMS to stabilise the web systems faster and reduce the to & fro exchange mails/phone calls on support incidences. C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"C Simplify IT is focussed on cutting the supports cost and has build SIMS to stabilise the web systems faster and reduce the to & fro exchange mails/phone calls on support incidences.,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-26 11:22:35'),(10,37,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"Their energies, ways of thinking and passion to do something important is unique. Welcome, we will help you gain respect which you deserve.,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"Their energies, ways of thinking and passion to do something important is unique. Welcome, we will help you gain respect which you deserve.simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysisTheir energies, ways of thinking and passion to do something important is unique. Welcome, we will help you gain respect which you deserve.\">\n	 ',NULL,NULL,NULL,'2018-09-26 11:22:35'),(11,38,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 09:31:48'),(12,39,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-26 09:07:49'),(13,40,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"Capture payments using Multiple Payment Tenders - Cash, Credit Card, Gift Vouchers.,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"Capture payments using Multiple Payment Tenders - Cash, Credit Card, Gift Vouchers.simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysisCapture payments using Multiple Payment Tenders - Cash, Credit Card, Gift Vouchers.\">\n	 ',NULL,NULL,NULL,'2018-09-26 11:22:35'),(14,41,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., In SYNCH \"Integrates\" Your Ecommerce Business, Suppliers, Vendors, and Customers in many ways.\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"In SYNCH \"Integrates\" Your Ecommerce Business, Suppliers, Vendors, and Customers in many ways.,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 11:38:43'),(15,42,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" These days, in technology and digital marketing circles, \" bots \" are a frequent source of discussion. By definition, a bot (short for \"web robot\") is a software program that operates as an agent for an individual, group of individuals, organization or even legitimate businesses. C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"These days, in technology and digital marketing circles, \" bots \" are a frequent source of discussion. By definition, a bot (short for \"web robot\") is a software program that operates as an agent for an individual, group of individuals, organization or even legitimate businesses.,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-26 11:38:44'),(16,43,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact ussimplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysissimplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us\">\n	 ',NULL,NULL,NULL,'2018-09-26 11:49:54'),(17,44,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 11:38:44'),(18,45,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-27 05:45:22'),(19,46,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact ussimplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysissimplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us\">\n	 ',NULL,NULL,NULL,'2018-09-26 11:38:44'),(20,47,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 11:49:54'),(21,48,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-27 05:45:22'),(22,49,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact ussimplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysissimplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us\">\n	 ',NULL,NULL,NULL,'2018-09-26 11:38:44'),(23,50,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 11:38:44'),(24,51,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-27 05:45:22'),(25,13,1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"Student internship, summer internship, winter internship, summer internships in gurgaon, internships in gurgaon, interns, internship program, internship in india, global internship, global internship program, find internship in india, interns for company, students internship, company internship, interns, internship, internships, find internships,  internship india, engineering internships in india, interns for company, paid internship,it internship in Gurgaon, engineering internships in india, MBA Internships in Gurgaon, Marketing Internships in Gurgaon, Media Internships in Gurgaon, Internships in Gurgaon, Internship in Gurgaon, Engineering internships in Gurgaon, Finance internships in Gurgaon, online internship   in Gurgaon,summer internship  in Gurgaon ,Student Internships in Gurgaon,Virtual Internships in Gurgaon ,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"Student internship, summer internship, winter internship, summer internships in gurgaon, internships in gurgaon, interns, internship program, internship in india, global internship, global internship program, find internship in india, interns for company, students internship, company internship, interns, internship, internships, find internships,  internship india, engineering internships in india, interns for company, paid internship,it internship in Gurgaon, engineering internships in india, MBA Internships in Gurgaon, Marketing Internships in Gurgaon, Media Internships in Gurgaon, Internships in Gurgaon, Internship in Gurgaon, Engineering internships in Gurgaon, Finance internships in Gurgaon, online internship   in Gurgaon,summer internship  in Gurgaon ,Student Internships in Gurgaon,Virtual Internships in Gurgaon simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysisStudent internship, summer internship, winter internship, summer internships in gurgaon, internships in gurgaon, interns, internship program, internship in india, global internship, global internship program, find internship in india, interns for company, students internship, company internship, interns, internship, internships, find internships,  internship india, engineering internships in india, interns for company, paid internship,it internship in Gurgaon, engineering internships in india, MBA Internships in Gurgaon, Marketing Internships in Gurgaon, Media Internships in Gurgaon, Internships in Gurgaon, Internship in Gurgaon, Engineering internships in Gurgaon, Finance internships in Gurgaon, online internship   in Gurgaon,summer internship  in Gurgaon ,Student Internships in Gurgaon,Virtual Internships in Gurgaon \">\n	 ',NULL,NULL,NULL,'2018-09-27 05:45:22'),(26,13,2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., Student internship, summer internship, winter internship, summer internships in gurgaon, internships in gurgaon, interns, internship program, internship in india, global internship, global internship program, find internship in india, interns for company, students internship, company internship, interns, internship, internships, find internships,  internship india, engineering internships in india, interns for company, paid internship,it internship in Gurgaon, engineering internships in india, MBA Internships in Gurgaon, Marketing Internships in Gurgaon, Media Internships in Gurgaon, Internships in Gurgaon, Internship in Gurgaon, Engineering internships in Gurgaon, Finance internships in Gurgaon, online internship   in Gurgaon,summer internship  in Gurgaon ,Student Internships in Gurgaon,Virtual Internships in Gurgaon \" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"Student internship, summer internship, winter internship, summer internships in gurgaon, internships in gurgaon, interns, internship program, internship in india, global internship, global internship program, find internship in india, interns for company, students internship, company internship, interns, internship, internships, find internships,  internship india, engineering internships in india, interns for company, paid internship,it internship in Gurgaon, engineering internships in india, MBA Internships in Gurgaon, Marketing Internships in Gurgaon, Media Internships in Gurgaon, Internships in Gurgaon, Internship in Gurgaon, Engineering internships in Gurgaon, Finance internships in Gurgaon, online internship   in Gurgaon,summer internship  in Gurgaon ,Student Internships in Gurgaon,Virtual Internships in Gurgaon ,C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-27 05:34:06'),(27,13,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" kindercare,kinder care,kindercare locations,kindercare gurugram,kindercare delhi,kindercare gurgoan,best daycare in gurgoan,best play school in india C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"kindercare,kinder care,kindercare locations,kindercare gurugram,kindercare delhi,kindercare gurgoan,best daycare in gurgoan,best play school in india,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-27 05:45:22'),(28,52,3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis, predictive analysis, integration, legacy, migration, training, skill, survey, marketing, tool, product, framework, gurgaon, india, services, java, j2ee, .net, mobile, pos, point of sale, hotel, data model, contact, contact us,C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-27 05:45:22');
/*!40000 ALTER TABLE `locationsuburltaglink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tagmaster`
--

DROP TABLE IF EXISTS `tagmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tagmaster` (
  `tagId` int(11) NOT NULL AUTO_INCREMENT,
  `tagName` varchar(200) NOT NULL,
  `tagValue` varchar(2000) NOT NULL,
  `tagType` varchar(50) NOT NULL COMMENT '"meta"-meta seo tags for google,"og"-open graph tags for facebook, "twitter"-twitter tags for twitter card',
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tagId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tagmaster`
--

LOCK TABLES `tagmaster` WRITE;
/*!40000 ALTER TABLE `tagmaster` DISABLE KEYS */;
INSERT INTO `tagmaster` VALUES (1,'Meta Google','meta','meta',NULL,NULL,NULL,'2018-03-09 05:38:10'),(2,'Meta Facebook','og','og',NULL,NULL,NULL,'2018-03-09 05:38:12'),(3,'Meta Twitter','twitter','twitter',NULL,NULL,NULL,'2018-03-09 05:38:13');
/*!40000 ALTER TABLE `tagmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `templateId` int(11) NOT NULL AUTO_INCREMENT,
  `templateText` text NOT NULL,
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`templateId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template`
--

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
INSERT INTO `template` VALUES (1,'<meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <meta name=\"alexaVerifyID\" content=\"pznLu6td6nvef9iY4zVLkvff3cM\" />\n    <meta name=\"alexaVerifyID\" content=\"qva_Zm3erO7Yxor3F2-Z5rWnSxg\" />\n    <meta name=\"msvalidate.01\" content=\"1A69BCFCA120CF757D36E59EAA106653\" />\n    <meta name=\"robots\" content=\"index,follow\">\n    <meta name=\"distribution\" content=\"global\">\n    <meta name=\"rating\" content=\"general\" />\n    <meta name=\"google-site-verification\" content=\"mE7hrS6eDwwtuzVSgmcbIE1ulOTohBCYyatz2wkovAE\" />\n    <meta name=\"description\" content=\"{token},C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"keywords\" content=\"{token}simplify,financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource, development, maintenance, support, descriptive analysis{token}\">\n	 ',NULL,NULL,NULL,'2018-09-26 09:03:49'),(2,'<meta property=\"og:title\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care..., {token}\" />\n   <meta property=\"og:locale\" content=\"en_US\" />\n   <meta property=\"og:type\" content=\"website\" />\n   <meta property=\"og:description\" content=\"{token},C Simplify IT Do eCommerce!10x better\"/>\n   <meta property=\"og:site_name\"  content=\"financial services,machine learning, bots, logistics, mdm, master data management,data mining, data warehousing, inventory, transport management, tms , consulting, wealth management, statistical analysis, document management, content management, manufacturing, retail, ios, android, resource,\" />\n',NULL,NULL,NULL,'2018-09-26 09:11:59'),(3,'<meta charset=\"utf-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta name=\"twitter:card\" content=\"summary_large_image\">\n    <meta name=\"twitter:site\" content=\"@dmehta104\">\n    <meta name=\"twitter:creator\" content=\"@dmehta104\">\n    <meta name=\"twitter:domain\" content=\"http://csimplifyit.com/\"/>\n    <meta name=\"twitter:title\" content=\" {token} C Simplify IT Do eCommerce!10x better\" />\n    <meta name=\"twitter:description\" content=\"{token},C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"/>\n    <meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\"/>',NULL,NULL,NULL,'2018-09-26 08:46:13'),(4,'<meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><meta name=\"author\" content=\"gaurav developer\"><meta name=\"twitter:card\" content=\"summary_large_image\"><meta name=\"twitter:site\" content=\"@dmehta104\"><meta name=\"twitter:creator\" content=\"@dmehta104\"><meta name=\"twitter:title\" content=\"C Simplify IT Do eCommerce!10x better\"><meta name=\"twitter:description\" content=\"C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care...\"><meta name=\"twitter:image\" content=\"http://csimplifyit.com/assets/plugins/parallax-slider/img/1.png\">',NULL,NULL,NULL,'2018-09-26 06:12:05');
/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templatetaglink`
--

DROP TABLE IF EXISTS `templatetaglink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templatetaglink` (
  `templateTagLinkId` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL COMMENT 'Foreign Key to "templateId" in table "template"',
  `tagId` int(11) NOT NULL COMMENT 'Foreign key to "tagId" in table "tagmaster"',
  `CRTBy` int(11) DEFAULT NULL,
  `CRTDate` datetime DEFAULT NULL,
  `UPDBy` int(11) DEFAULT NULL,
  `UPDdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`templateTagLinkId`),
  KEY `FK_templatetaglink_1` (`templateId`),
  KEY `FK_templatetaglink_2_idx` (`tagId`),
  CONSTRAINT `FK_templatetaglink_1` FOREIGN KEY (`templateId`) REFERENCES `template` (`templateId`),
  CONSTRAINT `FK_templatetaglink_2` FOREIGN KEY (`tagId`) REFERENCES `tagmaster` (`tagId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templatetaglink`
--

LOCK TABLES `templatetaglink` WRITE;
/*!40000 ALTER TABLE `templatetaglink` DISABLE KEYS */;
INSERT INTO `templatetaglink` VALUES (1,1,1,NULL,NULL,NULL,'2018-09-26 09:07:44'),(2,2,2,NULL,NULL,NULL,'2018-09-26 09:07:44'),(3,3,3,NULL,NULL,NULL,'2018-09-26 09:07:44');
/*!40000 ALTER TABLE `templatetaglink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'csimplifydb'
--
/*!50003 DROP PROCEDURE IF EXISTS `CaseChecklistConfidence` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CaseChecklistConfidence`(IN loc int)
BEGIN 

select checklist.fdPkId ,checklist.fdChkListTitle,tc.fdPkId,tc.fdCaseTitle,
IFNULL(sum(issueAnsCount.incidentcount), 0) AS issuesCount ,
IFNULL(sum(ansDocCount.docCount), 0) AS docCount,
avg(answer.fdCreaterConfidence) as avgCreConfidence
from tbchecklistanswer answer

join tbchecklist checklist on checklist.fdPkId = answer.fdCheckListId
join tbcase tc on tc.fdPkId = answer.fdCaseId
join tblocationmaster locMaster on locMaster.fdLocationid = tc.fdLocCode 

join tbcaseconfiguration caseConfig on caseConfig.fdCaseId=answer.fdPkId and caseConfig.fdCheckListId=answer.fdCheckListId

LEFT JOIN (SELECT COUNT(fdIncidenceId) AS incidentcount, fdChecklistAnswerId FROM tbchecklistansincidencelink 
GROUP BY fdChecklistAnswerId) issueAnsCount ON issueAnsCount.fdChecklistAnswerId = answer.fdPkId

LEFT JOIN (SELECT COUNT(fdDocId) AS docCount, fdchklistAnswerId FROM tbchecklistanswerdoclink
GROUP BY fdchklistAnswerId) ansDocCount ON ansDocCount.fdchklistAnswerId = answer.fdPkId
where locMaster.fdLocationid = @locId 
group by answer.fdCaseId,answer.fdCheckListId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `CHANGESEOTAGCONTENT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CHANGESEOTAGCONTENT`(OUT `out_param` VARCHAR(10))
BEGIN
  DECLARE `done1` INT DEFAULT FALSE;
  DECLARE `cursor_locationSubURLId` INT;
  DECLARE `cursor_locationSubURLIds` CURSOR FOR (SELECT `locationSubURLId` FROM `locationsuburls`);
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    ROLLBACK;
    SET `out_param`='ERROR';
  END;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET `done1` = TRUE;
  IF `done1` = FALSE THEN
   OPEN `cursor_locationSubURLIds`;
   `loop1` : loop
      FETCH `cursor_locationSubURLIds` INTO `cursor_locationSubURLId`;
      IF `done1` THEN
        LEAVE `loop1`;
      END IF;
      BLOCK2 : BEGIN
        DECLARE `fetch_tagId` INT;
        DECLARE `fetch_keyword` VARCHAR(2000);
        DECLARE `fetch_tagValue` VARCHAR(5000);
        DECLARE `done2` INT DEFAULT FALSE;
        DECLARE `cursor_tagIds` CURSOR FOR (select `tagId` from `locationsuburltaglink` where `locationSubURLId`=`cursor_locationSubURLId`);
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET `done2` = TRUE;
        IF `done2` = FALSE THEN
          OPEN `cursor_tagIds`;
            `loop2` : loop
              FETCH `cursor_tagIds` INTO `fetch_tagId`;
              IF `done2` THEN
                LEAVE `loop2`;
              END IF;
              select `keyword` into `fetch_keyword` from (
                select `keyword` from `keywords` where `keywordId` in (select `keywordId` from `locationsuburlkeywordlink` where `locationSubURLId` = `cursor_locationSubURLId`)
                union all 
                select `keywords` from `helperkeywords` hk join `helperkeywordslink` hkl ON hk.helperKeywordId = hkl.helperKeywordId 
                where hkl.keywordId in (select `keywordId` from `locationsuburlkeywordlink` where `locationSubURLId` = `cursor_locationSubURLId`)
              ) as `temp` order by rand() limit 1;
              SELECT replace(t.templateText, '{token}', `fetch_keyword`) into `fetch_tagValue` FROM `templatetaglink` ttl 
              INNER JOIN `template` t ON ttl.templateId = t.templateId 
              where ttl.tagId = `fetch_tagId`;
              UPDATE `locationsuburltaglink` SET `tagValue` = `fetch_tagValue` 
              where `locationSubURLId` = `cursor_locationSubURLId` and `tagId` = `fetch_tagId`;
            END loop loop2;
          CLOSE `cursor_tagIds`;
        END IF;
      END BLOCK2;
   END loop loop1;
   CLOSE `cursor_locationSubURLIds`;
  END IF;
  SET `out_param`='SUCCESS';
  SELECT `out_param`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getBottomLocationConfidence` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getBottomLocationConfidence`(IN pCaseId int,IN pStepId int,in pLocationId int,
IN pChecklistId int,IN pFromDate date,IN pToDate date)
BEGIN

select lc.fdLocationid,lc.fdLocationName,TRUNCATE(avg(ifnull(ans.fdCreaterConfidence,0)),2) as fdCreaterConfidence from tblocationmaster lc 
join tbcase ca on lc.fdLocationid=ca.fdLocation and ca.fdStatus=1
join tbcaseconfiguration cg on cg.fdCaseId=ca.fdPkId
join tbchecklistanswer ans on ans.fdCaseId=ca.fdPkId and ans.fdCheckListId=cg.fdCheckListId 

group by lc.fdLocationid order by fdCreaterConfidence asc limit 10; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `GETSEOCONTENT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `GETSEOCONTENT`(IN `in_param` VARCHAR(1000), OUT `out_param` TEXT)
BEGIN
  DECLARE `done` INT DEFAULT FALSE;
  DECLARE `fetch_locationSubURLId` INT;
  DECLARE `cursor_locationSubURLId` CURSOR FOR (SELECT `locationSubURLId` FROM `locationsuburls` where `subURL` = `in_param`);
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    ROLLBACK;
    SET `out_param`='ERROR';
  END;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET `done` = TRUE;
  IF `done` = FALSE THEN
   OPEN `cursor_locationSubURLId`;
   `loop1` : loop
      FETCH `cursor_locationSubURLId` INTO `fetch_locationSubURLId`;
      IF `done` THEN
        LEAVE `loop1`;
      END IF;
      SET SESSION group_concat_max_len = 30000;
      select GROUP_CONCAT(`tagValue` separator '\n') into `out_param` from `locationsuburltaglink` where `locationSubURLId` = `fetch_locationSubURLId`;
   END loop loop1;
   CLOSE `cursor_locationSubURLId`;
  END IF;
  SELECT `out_param`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getTopLocationConfidence` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getTopLocationConfidence`(IN pCaseId int,IN pStepId int,in pLocationId int,
IN pChecklistId int,IN pFromDate date,IN pToDate date)
BEGIN

select lc.fdLocationid,lc.fdLocationName,TRUNCATE(avg(ifnull(ans.fdCreaterConfidence,0)),2) as fdCreaterConfidence from tblocationmaster lc 
join tbcase ca on lc.fdLocationid=ca.fdLocation and ca.fdStatus=1
join tbcaseconfiguration cg on cg.fdCaseId=ca.fdPkId
join tbchecklistanswer ans on ans.fdCaseId=ca.fdPkId and ans.fdCheckListId=cg.fdCheckListId 

group by lc.fdLocationid order by fdCreaterConfidence desc limit 10; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `HighestIncidenceLocation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `HighestIncidenceLocation`()
BEGIN
select lc.fdLocationid,lc.fdLocationName,count(distinct inc.fdIncId) as IncidenceCount,count( distinct ca.fdPkId) as CountCase
from tblocationmaster lc 
join tbcase ca on lc.fdLocationid=ca.fdLocation
join tbchecklistanswer ans on ans.fdCaseId=ca.fdPkId
join tbchecklistansincidencelink ansi on ansi.fdChecklistAnswerId=ans.fdPkId
join tbincidence inc on inc.fdIncId=ansi.fdIncidenceId and inc.fdStatus!=3
group by lc.fdLocationid order by IncidenceCount desc limit 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `InProgresAuditCh` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `InProgresAuditCh`()
BEGIN
select lc.fdLocationName,count(distinct csr.fdPkId) as ProgressAudit,
count(distinct csf.fdPkId) as ClosedAudit,(count(distinct csr.fdPkId)+count(distinct csr.fdPkId)) as TotalAudit 
from tblocationmaster lc 
left join tbcase csr on csr.fdLocation=lc.fdLocationid and csr.fdStatus!=6
left join tbcase csf on csf.fdLocation=lc.fdLocationid and csf.fdStatus=6
group by lc.fdLocationid HAVING TotalAudit != 0 order by TotalAudit desc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `LowestIncidenceLocation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `LowestIncidenceLocation`()
BEGIN
select lc.fdLocationid,lc.fdLocationName,count(distinct inc.fdIncId) as IncidenceCount,count(distinct ca.fdPkId) as CountCase
from tblocationmaster lc 
join tbcase ca on lc.fdLocationid=ca.fdLocation
join tbchecklistanswer ans on ans.fdCaseId=ca.fdPkId
join tbchecklistansincidencelink ansi on ansi.fdChecklistAnswerId=ans.fdPkId
join tbincidence inc on inc.fdIncId=ansi.fdIncidenceId and inc.fdStatus!=3
group by lc.fdLocationid order by IncidenceCount asc limit 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spFetchCase` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchCase`(IN liCaseId int,IN liStepId int,
IN liChecklistId int,IN ldFromDate date,IN ldToDate date)
BEGIN
select cs.fdCaseTitle as caseName,count(1) as caseStepsCount from tbcase cs 
left join tbcaseconfiguration cf on cs.fdPkId=cf.fdCaseId group by cf.fdCaseId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `TicketsClosedByLocCh` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `TicketsClosedByLocCh`()
BEGIN
select lc.fdLocationid,lc.fdLocationName,count(distinct inc.fdIncId) as ClosedIncidenceCount,count( distinct ca.fdPkId) as CountCase
from tblocationmaster lc 
join tbcase ca on lc.fdLocationid=ca.fdLocation
left join tbchecklistanswer ans on ans.fdCaseId=ca.fdPkId
left join tbchecklistansincidencelink ansi on ansi.fdChecklistAnswerId=ans.fdPkId
left join tbincidence inc on inc.fdIncId=ansi.fdIncidenceId and inc.fdStatus=3
group by lc.fdLocationid order by ClosedIncidenceCount desc limit 10;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UploadChecklist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `UploadChecklist`(In createdby int,In fileId int, Out Description varchar(550), 
Out Status int)
BEGIN

DECLARE Counter1 int DEFAULT 0; 
DECLARE Counter2 int DEFAULT 0; 
DECLARE msg TEXT;
DECLARE errono varchar(200);
DECLARE sqlStateNo TEXT;
DECLARE done INT DEFAULT 0;


Declare v_SequenceNo int default 1;
Declare v_ChkListTypId int;
Declare v_ChkListCatId int;
Declare v_MaxCatId int;
Declare v_ChkListItemId text;
Declare v_ChkLink int;

Declare v_ChkLstTyp text;
Declare v_ChkLstCat text;
Declare v_ChkListItem text;
Declare v_rowNo int;

DECLARE GetCheckListCur CURSOR FOR select temp.fdProcess as Typ,temp.fdSubProcess as Cat,
temp.fdQuestion as Item,temp.fdRowNum as RowNum from tempChecklist temp;

DECLARE CONTINUE HANDLER FOR SQLSTATE '02000'
		BEGIN
			SET done = TRUE;
		END;

DECLARE exit handler for sqlexception
BEGIN
	
GET DIAGNOSTICS CONDITION 1
        msg=MESSAGE_TEXT,errono=MYSQL_ERRNO,sqlStateNo=RETURNED_SQLSTATE;
        SET Description=concat('ERROR:',' ',errono,' ','SQL State:',sqlStateNo,' ', 'Description: ',msg);
		SET Status=0;
        rollback;        
update tbinboundoutboundfileprocess set fdStatus=3 where fdFuId=fileId;
SET SQL_SAFE_UPDATES = 1;
END;

SET SQL_SAFE_UPDATES = 0;

Set Description='';
Set Status=0;

DROP  temporary table if exists tempChecklist;
 CREATE TEMPORARY TABLE tempChecklist (
    fdIndex int  NULL ,
    fdProcess text,
	fdSubProcess text,
    fdQuestion text ,
    fdRowNum int
 );


insert into tempChecklist 
select fdIndex, fdProcess, fdSubProcess, fdQuestion, fdRowNum from excelchecklist where fdfileId=fileId
and fdProcess is not null and fdSubProcess is not null and fdQuestion is not null
and fdProcess!='' and fdSubProcess !='' and fdQuestion !='';


open GetCheckListCur;
	read_loop : LOOP
			FETCH GetCheckListCur INTO v_ChkLstTyp,v_ChkLstCat,v_ChkListItem,v_rowNo;
            
            IF done IS TRUE THEN
				LEAVE `read_loop`;
				END IF;
            
               Set v_ChkListTypId=(select fdPkId from tbchecklist where fdChkListTitle=v_ChkLstTyp);
               if(v_ChkListTypId is null) then
               Begin 
					INSERT INTO `tbchecklist`
					(`fdChkListTitle`,`fdCheckListInstruction`,`fdCheckListType`,`fdStatus`,`fdCreatedOn`,`fdCreatedBy`)
					VALUES
					(v_ChkLstTyp,v_ChkLstTyp,1,1,now(),createdby);
                    set v_ChkListTypId=(SELECT LAST_INSERT_ID());
               end;
               End if;

				Set v_ChkListCatId=(select fdKeyCode from tbcodelkup where fdLkUpCode='CHK_LIST_CAT_TYPE' and fdKey1=v_ChkLstCat);
                if(v_ChkListCatId is null) then
                Begin
					set v_MaxCatId=(select max(fdKeyCode) from tbcodelkup where fdLkUpCode='CHK_LIST_CAT_TYPE');
					INSERT INTO `tbcodelkup`(`fdLkUpCode`,`fdKeyCode`,`fdDescription`,`fdKey1`,`fdCreatedOn`,`fdCreatedBy`,`fdModifiedOn`,`fdModifiedBy`,
					`fdIsSystemDefined`)
					VALUES
					('CHK_LIST_CAT_TYPE',v_MaxCatId+1,v_ChkLstCat,v_ChkLstCat,now(),createdby,now(),
					createdby,1);
                    set v_ChkListCatId=(SELECT v_MaxCatId+1);
                end;
                End if;
				
                Set v_ChkListItemId=(select fdPkId from tbchecklistitems where fdDesc=v_ChkListItem);
				if(v_ChkListItemId is null) then
				Begin
					INSERT INTO `tbchecklistitems`
					(`fdTitle`,`fdDesc`,`fdStatus`,`fdCreatedOn`,`fdCreatedBy`)
					VALUES
					(SUBSTRING_INDEX(v_ChkListItem,' ',6),v_ChkListItem,1,now(),createdby);
                    set v_ChkListItemId=(SELECT LAST_INSERT_ID());
				end;
				End if;
					

				set v_ChkLink=(select fdchklistId from tbchecklistitemlink where fdchklistId=v_ChkListTypId and fdchkListItemId=v_ChkListItemId and fdCategoryOfCheckList=v_ChkListCatId);
				set v_SequenceNo=(select count(1)  from tbchecklistitemlink where fdchklistId=v_ChkListTypId);
                if(v_ChkLink is null) then
                Begin
					INSERT INTO `tbchecklistitemlink`
					(`fdchklistId`,`fdchkListItemId`,`fdSequenceNumber`,`fdCreatedOn`,`fdCreatedBy`,`fdCategoryOfCheckList`)
					VALUES
					(v_ChkListTypId,v_ChkListItemId,v_SequenceNo+1,now(),createdby,v_ChkListCatId);
                end;
                End if;
                
                
                
               
END LOOP;
	CLOSE GetCheckListCur;
		SET done=0;
        
INSERT INTO `tbinboundoutboundfileprocessaudit`
(fdAssInboundOutboundId,fdTypeOfAudit,fdRowNo,fdColNo,fdCreatedBy,fdCreatedOn,fdAuditDescription,fdColumnName,
fdColumnData)
select fdfileId,1,fdRowNum,2,createdby,now(),'Process cant be null','Process','' from excelchecklist where fdfileId=fileId
and (fdProcess is null or fdProcess='');

INSERT INTO `tbinboundoutboundfileprocessaudit`
(fdAssInboundOutboundId,fdTypeOfAudit,fdRowNo,fdColNo,fdCreatedBy,fdCreatedOn,fdAuditDescription,fdColumnName,
fdColumnData)
select fdfileId,1,fdRowNum,3,createdby,now(),'Sub Process cant be null','Sub Process','' from excelchecklist where fdfileId=fileId
and (fdSubProcess is null or fdSubProcess='');

INSERT INTO `tbinboundoutboundfileprocessaudit`
(fdAssInboundOutboundId,fdTypeOfAudit,fdRowNo,fdColNo,fdCreatedBy,fdCreatedOn,fdAuditDescription,fdColumnName,
fdColumnData)
select fdfileId,1,fdRowNum,4,createdby,now(),'CheckList Item cant be null','Question','' from excelchecklist where fdfileId=fileId
and (fdQuestion is null or fdQuestion='');


set Counter1=(select count(1) from excelchecklist where fdfileId=fileId
and fdProcess is not null and fdSubProcess is not null and fdQuestion is not null
and fdProcess!='' and fdSubProcess !='' and fdQuestion !='');
set Counter2=(select count(1) FROM excelchecklist E where E.fdfileId=fileId);

if(Counter2=Counter1)
then
BEGIN

update tbinboundoutboundfileprocess set fdStatus=1 where fdFuId=fileId;

SET Status=1; 
SET Description= concat("All ",Counter1," rows successfully uploaded!") ;
END;
elseif(Counter1=0)
then
BEGIN
update tbinboundoutboundfileprocess set fdStatus=3 where fdFuId=fileId;
SET Status=2; 
SET Description="There is No data found for upload on this file";
END;
else
BEGIN

update tbinboundoutboundfileprocess set fdStatus=3 where fdFuId=fileId;
SET Status=2; 
SET Description= concat(Counter1," rows successfully uploaded!") ;
END;
END IF;


SET SQL_SAFE_UPDATES = 1;

select Status,Description;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-27 12:27:54

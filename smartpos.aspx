<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>
<asp:Content ID="smartposHead" ContentPlaceHolderID="HeadContent" runat="server">
  <title>Smartpos | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="smartposMaincontent" ContentPlaceHolderID="MainContent" runat="server">

	<div class="wrapper">
		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Smart POS - Geo/IOT Support</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.aspx">Home</a></li>
					<li><a href="">Products</a></li>
                    <li><a href="">Smart POS - Geo/IOT Support</a></li>
					
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		
       
        <!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				<!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
						<div class="carousel-inner">
							<div class="item active">
								<img alt="" src="assets/img/main/pos.jpg">
								<div class="carousel-caption">
									<p>Smart POS (Point of Sale) is the time and place where a retail transaction is completed.  </p>
								</div>
							</div>
							<!--<div class="item">
								<img alt="" src="assets/img/main/img12.jpg">
								<div class="carousel-caption">
									<p>Cras justo odio, dapibus ac facilisis into egestas.</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/img13.jpg">
								<div class="carousel-caption">
									<p>Justo cras odio apibus ac afilisis lingestas de.</p>
								</div>
							</div>-->
						</div>

						<div class="carousel-arrow">
							<a data-slide="prev" href="#myCarousel" class="left carousel-control">
								<i class="fa fa-angle-left"></i>
							</a>
							<a data-slide="next" href="#myCarousel" class="right carousel-control">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
				<!-- End Carousel -->

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>Smart POS - Point of Sale </h2>
					<p>Smart POS (Point of Sale) is the time and place where a retail transaction is completed.  </p>
					 <p><i class="fa fa-arrow-circle-right color-green"></i> Quick Order Capture using time and motion design.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i> Order Capture using Web/Mobile Channels.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i> Run Promotions - Bill Buster, Quantity, Line.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i> Capture payments using Multiple Payment Tenders - Cash, Credit Card, Gift Vouchers.</p>
                    <p><i class="fa fa-arrow-circle-right color-green"></i> Auto allocation of batches based on expiry dates.</p>
					
					<a href="https://www.youtube.com/embed/n_5tQrszX8s" class="btn-u btn-u-large" data-toggle="modal" data-target="#myModal">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
			</div><!--/row-->

			<div class="tag-box tag-box-v2">
				<p>C Simplify IT is focussed on cutting the supports cost and has build SIMS to stabilise the web systems faster and reduce the to & fro exchange mails/phone calls on support incidences.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>
            
            
            <!------------------------------Modal--------------------------------------->
             <!-- Modal -->
              <div class="modal fade wow bounceInRight" id="myModal" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header" style="background-color:#EF6262;">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title text-center" style="color:#fff;">Smart POS - Geo/IOT Support</h4>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-4by3">
                          <iframe class="embed-responsive-item" width="420" height="315" src="https://www.youtube.com/embed/ESLM722Aoq0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>
             <!------------------------------ End Modal--------------------------------------->

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
       
       
       
 
</div><!--/wrapper-->
    </asp:Content>
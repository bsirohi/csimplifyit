<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  %>
<asp:Content ID="insynchHead" ContentPlaceHolderID="HeadContent" runat="server">
      <title>IN SYNCH  | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="insynchMaincontent" ContentPlaceHolderID="MainContent" runat="server">
    

	<div class="wrapper">
    	<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">IN SYNCH - Intelligent follow ups</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.aspx">Home</a></li>
					<li><a href="">Products</a></li>
                    <li><a href="">IN SYNCH - Intelligent follow ups</a></li>
					
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		
        <!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>IN SYNCH - Intelligent follow ups</h2>
					<p>In SYNCH "Integrates" Your Ecommerce Business, Suppliers, Vendors, and Customers in many ways.</p>
                    <p>Try "In SYNCH " For Follow UPS.</p>
                    <p><i class="fa fa-arrow-circle-right color-green"></i> It's Cool.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i> It's Good.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i>  It Saves Pain.</p>
                     <p><i class="fa fa-arrow-circle-right color-green"></i> It Integerates.</p>
					
					
					<a href="https://www.youtube.com/embed/f7CfNRBX-2k" class="btn-u btn-u-large" data-toggle="modal" data-target="#myModal">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
                <!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
						
							<div class="embed-responsive embed-responsive-4by3">
                              <iframe src="https://www.youtube.com/embed/f7CfNRBX-2k"  width="530" height="300" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                            </div>
                        

						
					</div>
				</div>
				<!-- End Carousel -->
			</div><!--/row-->

			<div class="tag-box tag-box-v2">
				<p>In SYNCH "Integerates" Your Ecommerce Business, Suppliers, Vendors, and Customers in many ways.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>
            <!------------------------------Modal--------------------------------------->
             <!-- Modal -->
              <div class="modal fade wow bounceInRight" id="myModal" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header" style="background-color:#EF6262;">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title text-center" style="color:#fff;">IN SYNCH - Intelligent follow ups</h4>
                    </div>
                    <div class="modal-body">
                      <div class="embed-responsive embed-responsive-4by3">
                          <iframe class="embed-responsive-item" width="530" height="300" src="https://www.youtube.com/embed/f7CfNRBX-2k" frameborder="0" allowfullscreen></iframe>

                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>
             <!------------------------------ End Modal--------------------------------------->

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
</div><!--/wrapper-->

</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="budget_it_project.aspx.cs" Inherits="csimplifyit.WebForm21" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <script>
	  $(function () {
		  $('#slides').slides({
			  preload: true,
			  preloadImage: 'img/loading.gif',
			  play: 5000,
			  pause: 2500,
			  hoverPause: true



		  });
	  });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->
   <hr />	
	
	<h2 class="text_blue">Budget IT Project App for Budgeting your IT needs - FREE App</h2>
	<p>Sales or Business Development teams in field and Senior Executives are posed with many project sizing related questions in IT, this app helps answer those in less than 30 secs! Many a times it becomes a nightmare to wait for weeks, for technical teams/pre-sales teams/supports team to respond and to come up with simple sizing related answers. These cause strains in very beginning of customer relationship cycles and teams with-in organization. Budget IT Project!  App indeed may help you increase your sales, improve your relationships and improve your chances of accelerating fast (on corporate ladder)! </p>
	<div class="two_third">
	<span class="bold text_red">Potential Consequences of Using Rules of Thumb</span>
	<p>Using rules of thumb for estimating projects is dangerous and can have significant consequences, including:</p>
	<ol>
	<li>
		Significant project overruns. If project budgets are incorrectly set based on rules of thumb, it
	will be impossible to attain budget expectations.
	</li>
	<li>
	
	Lack of financing to complete the project. If budgets are exceeded, there is no guarantee
	that more funding for project completion will be available.
	</li>
	<li>
	 Loss of confidence of senior management as a result of budget overruns. Influenced by
	financial issues, management may decide to go in a new direction, with new project leaders and
	team members.</li>
<li>
	Incorrect return on investment (ROI) or business case associated with the initiative.
	Because ROI is a function of cost deducted from benefits, if the cost side of the equation is
	wrong, the return will also be wrong. It may turn out that planned benefits were not attainable
	from project inception.</li>
	</ol>
	

	</div>
<div class="one_third column-last">
	   <a href="https://itunes.apple.com/in/app/budget-it-project/id460813102?mt=8"> <img src="images/free_app.png" style="width:90%; float:right;" /></a>
	</div>

	<div class="clear"></div>

	<div class="one_third">
		<img src="images/free_app_large.png" style="width:99%;" />
	</div>
	<div class="two_third column-last">
	<p><span class="text_blue">Use this App to know the effort and budget required to do an IT project,</span> based on world standard estimation guidelines. It also helps in finding the HDD and RAM requirements for your projects and beyond.</p>
	<p>Project estimations presented by App is based on world standard estimation techniques of function point analysis, and HDD/RAM estimations are based on major blue chip companies’ white papers and suggestions and our experience, all intelligent practices noted are captured in this Budget IT Project App. To simplify the estimations basic assumptions are captured with-in app e.g. that application will be developed using the cost effective model where onsite and offshore presence is involved, complexity of modules is also captured with 45% low complexity and 15% high complexity modules etc. Although no estimation models can take care of all parameters, which can affect the estimates, here it is an attempt to improve our guesstimates and apply some standard parameters across all our estimations. Balance is maintained in its design, to leave the complexity of parameter settings to us and capture basic inputs from you.

 

   <p> With few basic answers to project based questions like How many screens are needed, how many integrations will be part of project, how many reports are needed, how many query screens are needed and how many data structures or tables are involved, it is attempted to apply the estimation model and share the output in simple format. Also mentioned in output are some of the important assumption taken by app, to allow you change those parameters and provide better estimations to your Customers, and Bosses.
</p>
 
 <p>
	Feedback is a gift, do write back your experience and help us improve</p>
	</div>
	</div>
</asp:Content>

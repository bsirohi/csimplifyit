﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="templates.aspx.cs" Inherits="csimplifyit.WebForm24" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true



        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


   <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->
   <hr />	
	
	<h2 class="text_blue">Templates</h2>
    <p>Templates help in re-use and ensuring things are consistent. Consistency helps in less training needs and same experience to the end users. Just think, how well Microsoft performed for many years by reducing the needs to trainings to end staff with every new product. These templates will help you do much more in lesser time or you will be able to enjoy your time more! </p>

    <h2><a href="crisp.aspx">Crisp</a></h2>
   <h2> <a href="detailed.aspx">Detailed</a></h2>
</div>
</asp:Content>

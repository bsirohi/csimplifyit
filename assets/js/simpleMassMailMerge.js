$('document').ready(function () {
	$('.installationData').hide();
    $('.configurationData').hide();
    $('.executionData').hide();
    $('.additionalLinksData').hide();
})
function cross() {
    $('.installationData').hide();
    $('.configurationData').hide();
    $('.executionData').hide();
    $('.additionalLinksData').hide();
};
function installation() {
    $('.installationData').show();
    $('.configurationData').hide();
    $('.executionData').hide();
    $('.additionalLinksData').hide();
}
function configuration() {
    $('.installationData').hide();
    $('.configurationData').show();
    $('.executionData').hide();
    $('.additionalLinksData').hide();
}
function execution() {
    $('.installationData').hide();
    $('.configurationData').hide();
    $('.executionData').show();
    $('.additionalLinksData').hide();
}
function additionalLinks() {
	$('.installationData').hide();
    $('.configurationData').hide();
    $('.executionData').hide();
    $('.additionalLinksData').show();
}
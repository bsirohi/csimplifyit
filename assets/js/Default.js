﻿$(document).ready(function () {
    GetLoginUserDetails();
});



function GetLoginUserDetails() {

    $.ajax({
        url: '../Signin/Signin.aspx/getLoginUserId',
        contentType: " application/json; charset=utf-8",
        type: 'POST',
        async: false,
        success: function (data) {

            if (data.d.Status == 1) {
                var AddUserName = $('#AddUserName');
               
                AddUserName.append('  <a class="nav-link js-scroll-trigger">' + data.d.Result.UserName + '</a>');

                var AddLogOutButton = $('#AddLogOutButton');
                AddLogOutButton.append('<a class="nav-link js-scroll-trigger" href="../LogOut/LogOut" >Log Out</a> ');
            }
        },
        error: function (request, error) {
            console.log(arguments);
            alert(" Can't do because: " + error);
        },
    });


}


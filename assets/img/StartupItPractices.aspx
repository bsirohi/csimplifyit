﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StartupItPractices.aspx.cs" Inherits="StartupItPractices" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/emailer.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
	<title>Emailer</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#landing-page">
          <img src="images/logocs.png" alt="C Simplify IT" height="45"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown1">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#landing-page">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#whatwedo">Solutions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#offerings">Offerings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#technology">Technologies</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#whyus">Why Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#clients">Our Clients</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact Us</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="main-content">

        assets/img/slider5.jpg
    	<img src="assets/img/main-image.jpg" class="img-fluid"/>
    </div>
    <div class="emailForm">
    	<form class="formprop" runat="server">
          
          	<div class="row">
            	<div class="col-md-12">
             		<p><span>Startup</span></p>
                    <p><span>Best IT Practices</span></p>
            	</div>

            </div>
        	
            <div class="row">
            	<div class="col-md-12">
                	<small>Stay updated with best IT Practices, latest trends in the market and much more.</small>
                </div>
            </div>
            <br/>
          <div class="form-group">
              <div class="row">
                <div class="col-12">

                   
                <input type="text" class="form-control" id="txtEmailid" runat="server" placeholder="Enter your email"/>
                </div>
              </div>
          </div>
          
          <div class="row">
                <div class="col-12">
                    <asp:Button ID="btnSubmit" class="btn btn-primary btnprop" runat="server" Text="Get the Document" OnClick="btnSubmit_Click" />
               <%-- <button type="submit" class="btn btn-primary btnprop">Get the Document</button>--%>
                </div>
              </div>
          
        </form>
    </div>

    <!-- <section> begin ============================-->
      <section class="text-sans-serif" id="contact">

        <div class="container">
          <div class="row">
            <div class="col-lg-4 pr-lg-5">
              <h3 class="mb-3">C Simplify IT</h3>
              <p>C Simplify IT based in Gurgaon, India, established in 2011, with a team of 100+ consulting & engineering professionals. We are a specialist IT Consulting Firm, Focused on generating exceptional value for our clients. Our clients are in Singapore, Malaysia, Switzerland, UK, Hong Kong, and India.</p>
            </div>
            <div class="col-md-6 col-lg-4 pr-lg-6 ml-lg-auto mt-6 mt-lg-0">             
              <h4 class="mb-2">Socials</h4>
              <p class="mb-0">Stay connected with C Simplify IT via our social media pages. If you haven't taken the opportunity to visit our social media pages, please do so. It's a great way to interact with us, get your questions answered and make suggestions.</p>
              <br/>
              <a class="btn btn-dark btn-sm mr-2" href="https://www.facebook.com/CSimplifyIT-210115279023481/" target="_blank"><span class="fa fa-facebook"></span></a>
              <a class="btn btn-dark btn-sm" href="https://in.linkedin.com/company/c-simplify-it-services-private-limited" target="_blank"><span class="fa fa-linkedin"></span></a>
            </div>
            <div class="col-md-6 col-lg-4 mt-6 mt-lg-0">
              <h4 class="mb-3">Get in touch</h4>
              <!-- <p class="mb-0"><span class="text-black">Tel:</span>
                <a class="text-700" href="tel:+91 9899976227">+91 9899976227</a>
              </p> -->
              <p class="mb-0"><span class="text-black">Drop us an email at:</span>
                <a class="text-700 salesCS" href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
              </p>
              <h4 class="mb-3 mt-4">Find us</h4>
              <address>C Simplify IT
                <br/>Plot No. 52, II Floor, Sector 32,
                <br/>Gurgaon (Haryana) - 122003</address>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->

    <!--    Footer-->
    <!--===============================================-->
    <footer class="footer bg-black text-600 py-4 text-sans-serif text-center overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
      <div class="container">
        <div class="row align-items-center">
         <!--  <div class="col-lg-4 order-lg-2">
            <a class="indicator indicator-up" href="#top" data-fancyscroll="data-fancyscroll"><span class="indicator-arrow indicator-arrow-one" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":0.9}'></span><span class="indicator-arrow indicator-arrow-two" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":1.05}'></span></a>
          </div> -->
          <div class="col-lg-4 text-lg-left mt-4 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              Copyright &copy; 2018 C Simplify IT</p>
          </div>
        </div>
      </div>
    </footer>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
</body>

</html>

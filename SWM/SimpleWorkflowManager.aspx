﻿<!DOCTYPE html>

<html lang="en-us">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">

  <title>Getting Started - Simple Workflow Manager</title>
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/just-the-docs.css">
<script src="../Scripts/jquery-1.4.3.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" src="assets/js/vendor/lunr.min.js"></script>
  
  <script type="text/javascript" src="assets/js/just-the-docs.js"></script>
  <script type="text/javascript">

    function testcall() {
        $.ajax({

            type: "POST",
            url: "SimpleWorkflowManager.aspx/test",
           
            contentType: "application/json; charset=utf-8",

            dataType: "json'",

            success: function (data) {
                var Data = data.d;
                if (Data.Status == 1) {
                    stripeKey = Data.Result.Description;
                } else if (Data.Status == 2) {

                } else {

                }
            },
            error: function (request, error) {
                console.log(arguments);
                alert(" Can't do because: " + error);
            },
            beforeSend: function () {

            },
            complete: function () {

            }
        });
    }

	</script>
  <meta name="viewport" content="width=device-width, initial-scale=1">

     <style>
        .installBtn {
            margin-left: 493px;
            font-size: 13px;
            background-color: cornflowerblue;
            color: white;
            padding: 10px;
            border: red;
            border-radius: 3px;
            PADDING-TOP: 1PX;
            padding-bottom: 1px;
            height: 31PX;
        }
    </style>
</head>


  <div class="page-wrap">
    <div class="side-bar">
      <a href="" class="site-title fs-6 lh-tight">C Simplify IT</a>
      <span class="fs-3"><button class="js-main-nav-trigger navigation-list-toggle btn btn-outline" type="button" data-text-toggle="Hide">Menu</button></span>
      <div class="navigation main-nav js-main-nav">
        <nav>
  <ul class="navigation-list">

         <li class="navigation-list-item active">
            <a href="/SWM/SimpleWorkflowManager.aspx" class="navigation-list-link active">Getting Started</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/stepbystep/index.html" class="navigation-list-link">Step by Step Guide</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/features/index.html" class="navigation-list-link">Features</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/tipsntricks/index.html" class="navigation-list-link">Tips & Tricks</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/benefits/index.html" class="navigation-list-link">Benefits</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/privacypolicy/index.html" class="navigation-list-link">Privacy Policy</a>
          </li>

  </ul>
</nav>

      </div>
      <footer role="contentinfo" class="s/ite-footer">
        <p class="text-small text-grey-dk-000 mb-0">This site is created by <a href="http://csimplifyit.com/">C Simplify IT</a>, for Simple Workflow Manager.</p>
      </footer>
    </div>
    <div class="main-content-wrap">
      <div class="page-header">
        <div class="main-content">
          
          <div class="search js-search">
            <div class="search-input-wrap">
              <input type="text" class="js-search-input search-input" placeholder="Search Manual" aria-label="Search Manual" autocomplete="off">
              <svg width="14" height="14" viewBox="0 0 28 28" xmlns="http://www.w3.org/2000/svg" class="search-icon"><title>Search</title><g fill-rule="nonzero"><path d="M17.332 20.735c-5.537 0-10-4.6-10-10.247 0-5.646 4.463-10.247 10-10.247 5.536 0 10 4.601 10 10.247s-4.464 10.247-10 10.247zm0-4c3.3 0 6-2.783 6-6.247 0-3.463-2.7-6.247-6-6.247s-6 2.784-6 6.247c0 3.464 2.7 6.247 6 6.247z"/><path d="M11.672 13.791L.192 25.271 3.02 28.1 14.5 16.62z"/></g></svg>
            </div>
            <div class="js-search-results search-results-wrap"></div>
          </div>
          
          
        </div>
      </div>
      <div class="main-content">

        <div class="page-content">
          <h2 id="welcome-to-digiquote-pro"><strong>Simple Workflow Manager</strong> <button class="installBtn"  onclick="window.open('https://gsuite.google.com/marketplace/app/simple_workflow_manager/444419522558','_blank')" >Install Now</button></h2>

          <p>The simplest way to manage your customers using multiple calenders by creating google calender events for candidates with just one click.</p>

          <p><strong>Simple Workflow Manager helps in following key process:</strong></p>
          <img src="assets\images\gettingstarted.png" alt="Getting Started">
          <p><strong>Steps:</strong></p>
          <div class="steps">
            <div class="steps_process">
            
            <div class="row odd_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\coldcalling.gif" alt="cold calling" />
              </div>
              <div class="col-md-11">
                <h2>Step 1:</h2>
                <p>Go to the Google Spreadsheet, click the Add-ons menu and you’ll see a new menu called Simple Workflow Manager. Click on Configure Campaign. A popup will appear on the right side of the sheet. Prepare the source data, refer to “How to prepare data” mentioned below these steps.</p>
              </div>
            </div> 

            <div class="row even_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\research.gif" alt="background check" />
              </div>
              <div class="col-md-11">
                <h2>Step 2:</h2>
                <p>Go to your Goodle Docs and create an event description template with variables written in {{variable}} notation. These variables will be replaced by the data in your Job Sheet,then the replaced data will be the description of your event.</p>
              </div>
            </div> 

            <div class="row odd_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\arrangeMeeting.gif" alt="arrange a meeting" />
              </div>
              <div class="col-md-11">
                <h2>Step 3:</h2>
                <p>Add columns in a Google sheet for each of the variables used in "Event Description Template". The column names should not contain the curly brackets and is case sensitive.</p>
              </div>
            </div> 

            <div class="row even_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\customerInputs.gif" alt="get customer input" />
              </div>
              <div class="col-md-11">
                <h2>Step 4:</h2>
                <p>Add the source data in your Google Sheet and follow the wizard to configure SRM. Click on Run after checking the summary of events in the Add-on. One event would be generated for each row in the sheet.</p>
              </div>
            </div> 

            <div class="row odd_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\draftSolution.gif" alt="generate Draft Solution" />
              </div>
              <div class="col-md-11">
                <h2>Step 5:</h2>
                <p>Click on Run.</p>
              </div>
            </div> 

            </div>
          </div>
          
          
          <div class="features">
            <h2 style="text-align: center;padding-top: 12px;">Features:</h2>
            <div class="row">
              <div class="col-md-3" style="border-right: 1px solid;">
                <div class="features_li">
                  <i class="fa fa-users" aria-hidden="true" style="color: #0089ff;"></i>
                  <p>Create your own event description</p>
                </div>
              </div>
              <div class="col-md-3" style="border-right: 1px solid;">
                <div class="features_li">
                  <i class="fa fa-pencil-square" aria-hidden="true" style="color: green;"></i>
                  <p>Manage calenders for each customer separately</p>
                </div>
              </div>
            <!-- </div>
            <div class="row"> -->
              <div class="col-md-3" style="border-right: 1px solid;">
                <div class="features_li">
                  <i class="fa fa-file-text" aria-hidden="true" style="color: orange;"></i>
                  <p>Create multiple events with one click</p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="features_li">
                  <i class="fa fa-comments" aria-hidden="true" style="color: red;"></i>
                  <p>Track monthly/yearly progress</p>
                </div>
              </div>
            </div>
          </div>
          <div class="setup_section">
            <div class="row">
              <div class="col-md-7" style="border-right: 1px solid;">
                <h2>Setup Simple Recruitment Manager</h2>
                <div style="margin-top: 1rem;">
                  <h3>Install the Google Add-on</h3>
                  <p>1. Install Simple Recruitment Manager add-on from the Google store. The add-on is compatible with all browsers and only requires a Gmail account.</p>
                </div>
                <div style="margin-top: 1rem;">
                  <h3>Create Job Sheet</h3>
                  <p>1. Create Job sheet either manually or using "(Optional) Create Job sheet" link present in the right popup to create jobsheet manually.</p>
                  <p>2. Fill all the necessary columns.</p>
                  <p>To add links of resume in the "Resume" column of sheet - you have to click on "Add-ons" option present in the menu bar then "Simple Recruitment Manager" after that "Add Resume from Drive", popup appears. Select Resume from your drive and it will be automatically added in the first blank cell in Resume column.</p>
                </div>
                <div style="margin-top: 1rem;">
                  <h3>Run Simple Recruitment Manager</h3>
                  <p>1. Either use our default template for description or edit your own template using the Edit button in "Event Template Settings". Then, click on "Next" button.</p>
                  <p>2. Check your event status, then click on "Execute".</p>
                </div>         
              </div>

              <div class="col-md-5">
                <h2>Quick Navigation Guide</h2>
                 <div class="quick_nav">
                  <a href="docs/tipsntricks/#tipsNTricks1" style="white-space: unset;">Creating Gmail Template for Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="docs/tipsntricks/#tipsNTricks2" style="white-space: unset;">Creating Dynamic Google Documents  using Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="docs/tipsntricks/#tipsNTricks3" style="white-space: unset;">How to create Google Sheet for Gmail templates and Google Docs using Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="docs/tipsntricks/#tipsNTricks4" style="white-space: unset;">How to read Usage Report generated by Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="docs/tipsntricks/#tipsNTricks5" style="white-space: unset;">How to save dynamically generated documents using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="docs/tipsntricks/#tipsNTricks6" style="white-space: unset;">How to TEST a Configured Campaign  using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="docs/tipsntricks/#tipsNTricks7" style="white-space: unset;">How to use predefined templates (GMail and Google Docs) using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>  
          </div>  
          <!-- <div class="third_section">
            
          </div> -->
          
          
        </div>
      </div>
    </div>
  </div>
</html>

﻿
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>C Simplify IT</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.jpg">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.jpg">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicons/favicon.ico">
    <link rel="manifest" href="assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="assets/img/favicons/mstile-150x150.jpg">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="assets/css/theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/document_management.css">
</head>
<body>
    
    <!--===============================================-->
    <!--    Fancynav-->
    <!--===============================================-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#landing-page">
          <img src="assets/img/logo.png" alt="C Simplify IT" height="45" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown1">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#landing-page" >Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" id="sln"  href="Default.aspx#whatwedo">Solutions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#offerings">Offerings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#technology">Technologies</a>
            </li>


              
               <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbarDropdownMenuProduct" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product</a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuProduct">
                <a class="dropdown-item" href="SM3/SimpleMassMailMerge.aspx">Simple Mass Mail Merge</a>
                     <a class="dropdown-item" href="SM3/Simple_Recruitment_Manager.aspx">Simple Recruitment Manager</a>
                  <a class="dropdown-item" href="SM3/Simple_Workflow_Manager.aspx">Simple Workflow Manager</a>
                  
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#whyus">Why Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="Default.aspx#clients">Our Clients</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact Us</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!--===============================================-->
    <!--    End of Fancynav-->
    <!--===============================================-->



    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main minh-100vh" id="top">


      <!-- ============================================-->
      <!-- Preloader ==================================-->
      <div class="preloader" id="preloader">
        <div class="loader">
          <div class="line-scale-pulse-out-rapid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
      <!-- ============================================-->
      <!-- End of Preloader ===========================-->

	  
	  <!-- ============================================-->
      <!-- <section> begin ============================-->
      <div class="speciality">
        <a href="Our_Specialities.aspx">Our Specialities</a>
      </div>
      <!-- <section> close ============================-->
      <!-- ============================================-->
	  
      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0 overflow-hidden" id="digital-header">

        <div class="bg-holder overlay digital-header overlay-1" style="background-image:url(assets/img/workflow6.jpg)" data-zanim-xs='{"delay":0,"animation":"zoom-out"}' data-zanim-trigger="scroll">
        </div>
        <!--/.bg-holder-->

        <div class="container">
          <div class="row minh-100vh align-items-center py-8 justify-content-center text-center">
            <div class="col-lg-10 col-xl-10" data-zanim-timeline="{}" data-zanim-trigger="scroll">
              <h1 class="fs-3 fs-sm-4 fs-md-5 text-white parallax display-4" data-rellax-speed="4"><span class="overflow-hidden d-block"><span class="d-inline-block font-weight-black" data-zanim-xs='{"delay":0.1}'>we have built</span></span><span class="overflow-hidden d-block"><span class="d-inline-block text-uppercase d-block py-1" data-zanim-xs='{"delay":0.2}'>100+ workflows around content</span></span>
                <span
                  class="overflow-hidden d-block"><span class="d-inline-block font-weight-black text-underline mb-1" data-zanim-xs='{"delay":0.3}'>that get results</span></span>
              </h1>
              <div class="overflow-hidden parallax" data-rellax-speed="3">
                <h3 class="mt-3 mt-sm-4 mt-md-5 mb-4 text-300 font-weight-normal" data-zanim-xs='{"delay":0.4}'>analyse, track, manage and store unstructured content
                  <br class="d-none d-sm-block" />and reduce business turnaround time through straight through workflows</h3>
              </div>
              <div class="parallax" data-rellax-speed="2">
                <div data-zanim-xs='{"delay":0.5}'>
                  <a class="btn btn-info rounded-capsule" href="mailto:sales@csimplifyit.com">Contact Us</a>
                </div>
              </div>
            </div>
          </div>
          <a class="indicator indicator-down  js-scroll-trigger" data-zanim-timeline='{"delay":1}' data-zanim-trigger="scroll"  data-fancyscroll="data-fancyscroll" data-offset="60" href="#targetdown"><span class="indicator-arrow indicator-arrow-one" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":0.25}'></span><span class="indicator-arrow indicator-arrow-two" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":0.5}'></span></a>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

      <div id="targetdown"></div>

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="bg-light" id="expertise">

        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-md-12" data-zanim-timeline="{}" data-zanim-trigger="scroll">
              <div class="col-12 mb-3 mb-md-5 text-center">
                <h2 class="fs-3 fs-sm-4"><span class="text-underline">expertise in robotic process automation</span></h2>
              </div>
              <div class="overflow-hidden">
                <p class="text-sans-serif text-center" data-zanim-xs='{"delay":0.3}'>We have implemented over 100+ workflows enabled through document & content management technologies. Our lead engineers come from product engineering backgrounds. We have setup a team of 100+ engineers to automate your workflows through robotic process automation(RPA). Our technical team has expertise in integrating existing workflows to RPA for following platforms:</p>
                <div class="row justify-content-between mt-4">
                  <div class="col-lg-4 px-lg-4">
                    <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                      <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                      <div class="media-body">
                        <div class="overflow-hidden">
                          <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>FileNET BPM And Content Management Platform</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
                    <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                      <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                      <div class="media-body">
                        <div class="overflow-hidden">
                          <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Documentum Product Suite</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 px-lg-4">
                    <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                      <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                      <div class="media-body">
                        <div class="overflow-hidden">
                          <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Oracle BPM and Web Centre Content Platform</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row justify-content-between mt-4">
                  <div class="col-lg-4 px-lg-4">
                    <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                      <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                      <div class="media-body">
                        <div class="overflow-hidden">
                          <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>OpenText Implementations for Web & Document Management</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
                    <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                      <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                      <div class="media-body">
                        <div class="overflow-hidden">
                          <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Alfresco Open Source Content Management Solution</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 px-lg-4">
                    <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                      <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                      <div class="media-body">
                        <div class="overflow-hidden">
                          <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Custom Built Document Management solution using DigitalDocs Platform</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="overflow-hidden py-0" id="process-automation">
          <div class="bg-holder overlay overlay-0 parallax" style="background-image:url(assets/img/slider5.jpg);" data-rellax-percentage="0.5">
          </div>
          <div class="align-items-center justify-content-center text-center py-6">
            <div class="col-12 mb-3 mb-md-5 text-center">
              <h2 class="fs-3 fs-sm-4"><span class="text-underline">100+ workflows implemented by our process specialists</span></h2>
            </div>
            <div class="col-lg-12 px-lg-8 mt-6 mt-lg-0">
              <div class="row" data-zanim-xs='{"delay":0.3}'>
                <div class="col-md-3">
                  <h3 class="fs-3 fs-sm-4">Accounts Payable & Receivable</h3>
                </div>
                <div class="col-md-3">
                  <h3 class="fs-3 fs-sm-4">Order<br/> to Delivery</h3>
                </div>
                <div class="col-md-3">
                  <h3 class="fs-3 fs-sm-4">Delivery<br/> to Cash</h3>
                </div>
                <div class="col-md-3">
                  <h3 class="fs-3 fs-sm-4">Contract<br/> Management</h3>
                </div>
              </div>
              <div class="row" data-zanim-xs='{"delay":0.5}'>
                <div class="col-md-3">
                  <h3 class="fs-3 fs-sm-4">Policies & Compliance Management</h3>
                </div>
                <div class="col-md-3">
                  <h3 class="fs-3 fs-sm-4">RFP, RFQ & Bid Management</h3>
                </div>
                <div class="col-md-3">
                  <h3 class="fs-3 fs-sm-4">Employee Reimbursement Management</h3>
                </div>
                <div class="col-md-3">
                  <h3 class="fs-3 fs-sm-4">Hire<br/> to Retire</h3>
                </div>
              </div>
            </div>

          </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-5 py-lg-7 bg-light" id="enterprise-integration">

        <div class="container">
            <div class="col-12 mb-3 mb-md-5 text-center">
              <h2 class="fs-3 fs-sm-4"><span class="text-underline">enterprise integration</span></h2>
            </div>
          <div class="row justify-content-between">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/logo/mulesoft.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Enterprise Service Bus, and Messaging Infrastructure</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/logo/sap.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>SAP Integration using Content Adapters</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/logo/tci.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Captiva & Kofax Integration for Document Scanning</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/logo/oracle.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Oracle Financials & Fusion Components. IBM & Oracle SOA Frameworks</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/logo/ibm.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>IBM Watson AI Framework for Content Analytics; RPA Integration using UIPath</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/logo/microsoft.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Microsoft AX & Dynamics 365 Optical Character Recognition (OCR) & PDF Readers</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-5 py-lg-7" id="business-process">

        <div class="container">
            <div class="col-12 mb-3 mb-md-5 text-center">
              <h2 class="fs-3 fs-sm-4"><span class="text-underline">end-to-end services to manage your content & workflows</span></h2>
            </div>
          <div class="row justify-content-between">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>covering entire operations to automate</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Finance Workflows</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Purchasing & Contracting</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Real Estate Building Architecture Approvals</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Real Estate Project Change Management</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Engineering Safety</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Vendor Onboarding</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>fully automated content driven workflows</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>100+ Workflows</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>200+ Different Document & Content Elements</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Integration with SAP Enterprise Platform</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->


      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-5 py-lg-7 bg-light" id="crm">

        <div class="container">
            <div class="col-12 mb-3 mb-md-5 text-center">
              <h2 class="fs-3 fs-sm-4"><span class="text-underline">crm</span></h2>
            </div>
          <div class="row justify-content-between">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>center of excellence</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Proactive Expertise Building</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Develop Delivery Accelerators</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Knowledge Management</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Methodologies & Frameworks</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Alliance Partners & Competency Development</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Creating Capability to deliver value to Customer</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>offerings</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Consulting</b> <br/>(Roadmap / Blueprints, Remediation Services, Assessment & Implementation, GAP Analysis)</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Implementation & Support</b><br/> (Global Process Definition, Interface Development,  Data Migration, Global Template Definition, Hosting, Application Maintenance, Managed Services, SLA Based Support)</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Upgrades / Migration</b> <br/>(Version Upgrade, Upgrade Assessment, Package Migration, Re-Implementation)</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>purpose of offerings</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>The focus is on creating value for the customer and the company over the longer term.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Our CRM Solutions enables organizations to gain ‘competitive advantage’ over competitors.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Our CRM Solutions focus on strategically significant markets. Not all customers are equally important.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>benefits</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Reduced Costs, because right things will be done (effective and efficient operation).</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Increased Customer Satisfaction, because they will get exactly what they want (meeting & exceeding expectations).</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>When Customer value the customer service that they receive from suppliers, they are less likely to look for alternatives.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Ensuring that the focus of the organization is external.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Growth in number of Customers.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Maximization of Opportunities (Increased Services, Referrals).</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Increased access to a source of Market and Competitor Information.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Highlighting Poor Operational Processes.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Long Term Profitability and sustainability.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->


      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-5 py-lg-7" id="hrsolutions">

        <div class="container">
            <div class="col-12 mb-3 mb-md-5 text-center">
              <h2 class="fs-3 fs-sm-4"><span class="text-underline">hr solutions</span></h2>
            </div>
          <div class="row justify-content-between">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>business process expertise</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Compensation Management</b> (Managing performance appraisals, and compensation changes linked to performance management. Budgets, distribution etc).</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Recruitment</b> (Full cycle of recruitment from job postings to selection, and offer. Internal and External job Postings).</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Learning Management System</b> <br/>(Content management both internal & external, mandatory and discretionary learning processes).</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Onboarding</b> (New Employee Onboarding Processes, including managing variations for grades etc).</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Attendance</b> (Attendance and Integration with Biometrics).</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>EMGM</b> (Annual and Mid Year Performance Reviews. Goal Settings, self review, manager comments etc).</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Employee Benefits Management</b><br/> (Flexible benefits management, tax declarations, managing changes to the declarations etc).</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Employee Central</b> (Core employee information capture, managing, and employment history management).</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'><b>Off-Boarding/Exit Processes</b> <br/>(Managing resignations, separation, and revokes).</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>offerings</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Data Modeling</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Data Governance</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Business Analysis</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Target Process Development</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Platform Implementation & Maintenance</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Enterprise Integration</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>tools</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Success Factor</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Custom HR Development</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Oracle People Soft</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->


      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-5 py-lg-7 bg-light" id="testing">

        <div class="container">
            <div class="col-12 mb-3 mb-md-5 text-center">
              <h2 class="fs-3 fs-sm-4"><span class="text-underline">testing</span></h2>
            </div>
            <div class="row justify-content-between">
              <div class="col-12 mb-2 mb-md-3 text-center">
                <h3>testing life cycle</h3>
              </div>
              <img src="assets/img/testing.jpg" class="img-fluid" style="height:500px; margin: 0 auto;">
            </div>
          
          <div class="row justify-content-between mt-4">
            <div class="col-12 mb-2 mb-md-3 text-center">
              <h3>types of testing</h3>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Functional Testing</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Localization/Internationalization Testing</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Performance Testing</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Volume Testing</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Security Testing</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Compatibility Testing</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Compliance Testing</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4 py-4 py-lg-0">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Automation Testing</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>HA/DR Testing</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row justify-content-between mt-4">
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Usability Testing</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Stress Testing</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 px-lg-4">
              <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                <img class="mr-3" src="assets/img/icons/list.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                <div class="media-body">
                  <div class="overflow-hidden">
                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>API Testing</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->


      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="overflow-hidden py-0" id="case-study">
          <div class="position-absolute overflow-hidden a-0">
            <div class="bg-holder overlay overlay-0 rounded" style="background-image:url(assets/img/workflow5.jpg);" data-zanim-trigger="scroll" data-zanim-lg='{"animation":"zoom-out","delay":0}'>
            </div>
            <!--/.bg-holder-->

          </div>
          <div class="align-items-center justify-content-center text-center py-6">
            <div class="col-12 mb-3 mb-md-5 text-center">
              <h2 class="fs-3 fs-sm-4"><span class="text-underline">case study</span></h2>
            </div>
            <div class="col-lg-12 px-lg-8 mt-6 mt-lg-0">
              <div class="owl-carousel owl-theme owl-nav-outer" data-options='{"dots":false,"nav":true,"items":1,"autoplay":true,"loop":true,"autoplayHoverPause":true}'>
                <div class="item">
                  <h4 class="font-weight-bold text-white fs-1 fs-sm-2 mb-4">"India’s Largest Real Estate Developer"</h4>
                  <div class="row">
                    <div class="col-md-6">
                      <ul>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>A complete business process transformation that resulted in people to focus back on the core business</span></li>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Complete digital conversion of the architectural and compliance records for faster retrieval, and save on storage space</span></li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>An enhanced employee experience by automation of key workflows like travel, and other reimbursements</span></li>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Streamlined review and comments on the core engineering, and architecture artefacts</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <h4 class="font-weight-bold text-white fs-1 fs-sm-2 mb-4">"One of the largest Multi Level Marketing firms in India"</h4>
                  <div class="row">
                    <div class="col-md-6">
                      <ul>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>200% improvement in volume of dealer onboarding</span></li>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Distributed scalable solution handled 4 times sales transaction volumes facilitating revenue growth</span></li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>The new API solution helped to develop better marketing tools on the mobile platform</span></li>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Improved visibility of the network across more cities through a web presence</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <h4 class="font-weight-bold text-white fs-1 fs-sm-2 mb-4">"A Swiss Healthcare Device Manufacturer"</h4>
                  <div class="row">
                    <div class="col-md-6">
                      <ul>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Complete integrated platform to manage data, content, and device interactions</span></li>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Compliance and Validation management through records management</span></li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Enabling new product launches through an enterprise solution</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <h4 class="font-weight-bold text-white fs-1 fs-sm-2 mb-4">"Implementing Microsoft Dynamics CRM"</h4>
                  <div class="row">
                    <div class="col-md-6">
                      <ul>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Transparency in sales data, which helped in decision-making processes, and provided a 360-degree view of their customer</span></li>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Significant Increase in Revenue</span></li>
                      </ul>
                    </div>
                    <div class="col-md-6">
                      <ul>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>A complete Standardized Model for every step</span></li>
                        <li class="fs-0 mb-0 text-white font-weight-normal"><span>Transparency & Flexibility</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->


      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="text-sans-serif" id="contact">

        <div class="container">
          <div class="row">
            <div class="col-lg-4 pr-lg-5">
              <h3 class="mb-3">C Simplify IT</h3>
              <p>We are a specialized Global IT services & solution organization established in 2011, with a team of 120+ consulting & engineering professionals spread across its delivery centers at India (Gurugram), USA (Fremont, CA) . We are a specialist IT Consulting firm, leveraging innovation & passion at its best, C Simplify IT is trusted by Fortune 500 companies and start-ups to help business to add value across our clients worldwide. Our clients are based in USA, Singapore, Malaysia, Switzerland, UK, Hong Kong, and India.</p>
            </div>
            <div class="col-md-6 col-lg-4 pr-lg-6 ml-lg-auto mt-6 mt-lg-0">             
              <h3 class="mb-3">Socials</h3>
              <p class="mb-0">Stay connected with C Simplify IT via our social media pages. If you haven't taken the opportunity to visit our social media pages, please do so. It's a great way to interact with us, get your questions answered and make suggestions.</p>
              <br/>
              <a class="btn btn-dark btn-sm mr-2" href="https://www.facebook.com/CSimplifyIT-210115279023481/" target="_blank"><span class="fab fa-facebook-f"></span></a>
              <a class="btn btn-dark btn-sm" href="https://in.linkedin.com/company/c-simplify-it-services-private-limited" target="_blank"><span class="fab fa-linkedin-in"></span></a>
            </div>
            <div class="col-md-6 col-lg-4 mt-6 mt-lg-0 office-address">
              <h3 class="mb-3">Get in touch</h3>
              <address><b><u>India Head Office:</u></b>
                <br/>Plot No. 52, II Floor,
                <br/> Sector 32, Gurgaon (Haryana) - 122003
                <br/><b>Call Us:</b> <a href="tel:+91 98999762274">+91 9899976227</a> (For Tech Queries),<br /><a href="tel:+91 8950230044">+91 8950230044</a> (For Business Queries)
                <br/><b>E-Mail:</b> <a href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
                <br/><br/><b><u>US Head Office:</u></b>
                <br/>46878 Fernald St, Fremont, CA 94539, USA
                <br/><b>Call Us:</b>  <a href="tel:+1 408 987 5597">+1 408 987 5597</a>
                <br/><b>E-Mail:</b> <a href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
              </address>
            </div>
          </div>
        </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->

 </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!--===============================================-->
    <!--    Footer-->
    <!--===============================================-->
    <footer class="footer bg-black text-600 py-4 text-sans-serif text-center overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-4 order-lg-2">
            <a class="indicator indicator-up js-scroll-trigger" href="#top"><span class="indicator-arrow indicator-arrow-one" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":0.9}'></span><span class="indicator-arrow indicator-arrow-two" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":1.05}'></span></a>
          </div>
          <div class="col-lg-4 text-lg-left mt-4 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              Copyright &copy; 2018 C Simplify IT</p>
          </div>
          <div class="col-lg-4 text-lg-right order-lg-2 mt-2 mt-lg-0">
            <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              <a class="text-600" href="TOS.aspx">Terms of Service</a>
            </p>
          </div>
        </div>
      </div>
    </footer>


    <!--===============================================-->
    <!--    Modal for language selection-->
    <!--===============================================-->
    <div class="remodal bg-black remodal-select-language" data-remodal-id="language">
      <div class="remodal-close" data-remodal-action="close"></div>
      <ul class="list-unstyled pl-0 my-0 py-4 text-sans-serif">
        <li>
          <a class="pt-1 d-block text-white font-weight-semi-bold" href="#">English</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="#">Français</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="page-rtl.html">عربى</a>
        </li>
        <li>
          <a class="pt-1 d-block text-500" href="#">Deutsche</a>
        </li>
      </ul>
    </div>


    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    
     <script src="assets/js/scrolling-nav.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/lib/loaders.css/loaders.css.js"></script>
    <script src="assets/js/stickyfill.min.js"></script>
    <script src="assets/lib/remodal/remodal.js"></script>
    <script src="assets/lib/jtap/jquery.tap.js"></script>
    <script src="assets/js/rellax.min.js"></script>
    <script src="assets/lib/owl.carousel/owl.carousel.js"></script>
    <script src="assets/lib/lightbox2/js/lightbox.js"></script>
    <script src="assets/lib/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/lib/isotope-packery/packery-mode.pkgd.min.js"></script>
    <script src="assets/js/theme.js"></script>   
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="resetPassword.aspx.cs" Inherits="resetPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
       
        .TakeMyJobLoginButton
        {
            font-weight: 700;
            margin-left: 0px;
        }
       
        .style2
        {
            color: #993333;
        }
       
    </style>
</head>
<body>
   
 <form id="form1" runat="server">
    <div>
     <div  style="margin: 0 auto; border:1px solid #a1a1a1; width:750px;height:500px;margin-top:40px;">

<div style="float:left;width:278px;height:500px;background-color:#fafafa;border-right:1px solid #a1a1a1;">
<table width="278">
<tr><td style="padding:10px"><p><font face="verdana" color="black" size="4">Create password to manage your Take My Job account.</font></p></td></tr>
<tr><td style="padding:10px"><p class="style2"><font face="BOOK ANTIQUA" size="3">To view latest job updates, to post job, to inform friends about your resignation please,complete registration process. </font></p></td></tr>
</table>
</div>
<div style="float:left;width:450px;">
<table border='0' class='tab' align='center'>
<tr>
<td height="150" bgcolor="white" style="vertical-align:top;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Create your password</strong><br><br>
    <span class="style2">You can change or reset the password for your takemyjob account by providing some information.</span></td>
<td height="150" bgcolor="white" style="vertical-align:top;"><img src="images/csimplifyitlogo.png" height="120" width="80" align="right"></td>
</tr>
<tr><td height="60px" colspan="2"><hr><td>
    <br />
    </tr>
    <tr>
        <td align="center" colspan="2">
        <asp:Label ID="message" runat="server" Text="Label" Visible="False"></asp:Label></td></tr>
<tr><td height="60px" align="center" colspan="2">
    <asp:TextBox ID="txtpassword" runat="server" size='30px' CssClass="new" TextMode="Password" 
        placeholder="password" ></asp:TextBox>
    
    </td></tr>
<tr><td height="60px" align="center" colspan="2"><asp:TextBox ID="txtconfirmPassword" 
        runat="server" size='30px' CssClass="new" TextMode="Password" placeholder="Confirm Password" ></asp:TextBox>
    </td></tr>
<tr><td height="60px" align="center" colspan="2">
    <asp:Button ID="btnSubmit" 
        runat="server" Text="Submit" CssClass="TakeMyJobLoginButton" Height="23px" 
        Width="207px" onclick="btnSubmit_Click" /></td></tr>
        <tr><td height="60px" align="center" colspan="2">
            <asp:CompareValidator ID="CompareValidator1" runat="server" 
        ControlToCompare="txtpassword" ErrorMessage="These passwords don't match. Try again?" 
                ControlToValidate="txtconfirmPassword"></asp:CompareValidator></td></tr>
</table>
</div>
     
</div>
    </div>
    </form>

     
        
</body>
</html>

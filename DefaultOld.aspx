<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>

<asp:Content ID="DefaultHead" ContentPlaceHolderID="HeadContent" runat="server">
    <title>C Simplify IT - Do eCommerce!10x better.</title>
    
</asp:Content>
<asp:Content ID="DefaultMaincontent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="wrapper">
        <!--=== Slider ===-->
        <div id="layerslider" style="width: 100%; height: 500px;">

            <div class="ls-slide" style="slidedirection: right; transition2d: 92,93,105;">
                <img src="assets/img/sliders/layer/1.jpg" class="ls-bg" alt="Slide background" />

                <span class="ls-s-1" style="color: #fff; line-height: 45px; font-weight: 200; font-size: 35px; top: 150px; left: -40px; slidedirection: top; slideoutdirection: bottom; durationin: 1000; durationout: 1000;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">Use our expertise in  </i>
                    <br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">Bots and Machine Learning </i>
                    <br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">to reconfigure the man machine </i>
                    <br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">relationships in your enterprise</i>
                    <br>
                    <br>
                </span>


                <a class="btn-u btn-u-red ls-s-1" href="http://csimplifyit.com/bots-machinelearning.aspx" style="padding: 9px 20px; font-size: 25px; top: 350px; left: -40px; slidedirection: bottom; slideoutdirection: bottom; durationin: 2000; durationout: 2000;">Read More
                </a>

                <img src="assets/plugins/parallax-slider/img/slider-botsandMachine.jpg" alt="Slider Image" class="ls-s-1" style="top: 30px; left: 500px; slidedirection: right; slideoutdirection: bottom; durationin: 3000; durationout: 3000;">
            </div>

            <!-- First slide -->
            <div class="ls-slide" style="slidedirection: right; transition2d: 92,93,105;">
                <img src="assets/img/sliders/layer/1.jpg" class="ls-bg" alt="Slide background">

                <span class="ls-s-1" style="color: #fff; line-height: 45px; font-weight: 200; font-size: 35px; top: 200px; left: 10px; slidedirection: top; slideoutdirection: bottom; durationin: 1000; durationout: 1000;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">PushBiz</i><br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">Makes your Business &#39;Visible&#39;</i><br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">to your clients</i>
                </span>


                <a class="btn-u btn-u-red ls-s-1" href="https://www.pushbiz.in/" style="padding: 9px 20px; font-size: 25px; top: 350px; left: 10px; slidedirection: bottom; slideoutdirection: bottom; durationin: 2000; durationout: 2000;">Signup Now
                </a>

                <img src="assets/plugins/parallax-slider/img/pushbiz.png" alt="Slider Image" class="ls-s-1" style="top: 30px; left: 400px; slidedirection: right; slideoutdirection: bottom; durationin: 3000; durationout: 3000;">
            </div>
            <!-- End First slide -->

            <!-- Second Slide -->
            <div class="ls-slide" data-ls="slidedelay:4500; transition2d:25;">
                <img src="assets/img/sliders/layer/2.jpg" class="ls-bg" alt="Slide background" />

                <img class="ls-l" src="assets/plugins/parallax-slider/img/1.png" style="top: 55%; left: 30%;"
                    data-ls="offsetxin:left; durationin:1500; delayin:900; fadein:false; offsetxout:left; durationout:1000; fadeout:false;" />

                <span class="ls-s-1" style="line-height: 45px; font-size: 35px; color: #fff; top: 150px; left: 590px; slidedirection: top; slideoutdirection: bottom; durationin: 3500; durationout: 3500; delayin: 1000;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">Premier IT Company </i>
                    <br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">For eCommerce, BPM, </i>
                    <br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">Mobility and Analytics</i>
                </span>

                <a class="btn-u btn-u-orange ls-s-1" href="aboutus.aspx" style="padding: 9px 20px; font-size: 25px; top: 340px; left: 590px; slidedirection: bottom; slideoutdirection: top; durationin: 3500; durationout: 2500; delayin: 1000;">Know More
                </a>
            </div>
            <!-- End Second Slide -->
            <!-- Third slide -->
            <div class="ls-slide" style="slidedirection: right; transition2d: 105;">
                <img src="assets/img/sliders/layer/1.jpg" class="ls-bg" alt="Slide background" />

                <span class="ls-s-1" style="color: #fff; line-height: 45px; font-weight: 200; font-size: 35px; top: 200px; left: 10px; slidedirection: top; slideoutdirection: bottom; durationin: 1000; durationout: 1000;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">In SYNCH "Integrates"</i><br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">Your Ecommerce</i><br>
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 0px 20px;">Business, Suppliers..</i>
                </span>


                <a class="btn-u btn-u-red ls-s-1" href="insynch.aspx" style="padding: 9px 20px; font-size: 25px; top: 350px; left: 10px; slidedirection: bottom; slideoutdirection: bottom; durationin: 2000; durationout: 2000;">Visit Project
                </a>

                <img src="assets/img/sliders/insynch-video.png" alt="Slider Image" class="ls-s-1" style="top: 100px; left: 550px; slidedirection: right; slideoutdirection: bottom; durationin: 3000; durationout: 3000;" />
            </div>
            <!-- End Third slide -->

            <!-- Fourth Slide -->
            <div class="ls-slide" data-ls="transition2d:93;">
                <img src="assets/img/sliders/layer/3.jpg" class="ls-bg" alt="Slide background" />

                <i class="fa fa-chevron-circle-right ls-s-1" style="color: #fff; font-size: 24px; top: 70px; left: 40px; slidedirection: left; slideoutdirection: top; durationin: 1500; durationout: 500;"></i>

                <span class="ls-s-2" style="color: #fff; font-weight: 200; font-size: 22px; top: 70px; left: 70px; slidedirection: top; slideoutdirection: bottom; durationin: 1500; durationout: 500;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 5px 20px;">Business Intelligence</i>
                </span>

                <i class="fa fa-chevron-circle-right ls-s-1" style="color: #fff; font-size: 24px; top: 120px; left: 40px; slidedirection: left; slideoutdirection: top; durationin: 2500; durationout: 1500;"></i>

                <span class="ls-s-2" style="color: #fff; font-weight: 200; font-size: 22px; top: 120px; left: 70px; slidedirection: top; slideoutdirection: bottom; durationin: 2500; durationout: 1500;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 5px 20px;">Content Management</i>

                </span>

                <i class="fa fa-chevron-circle-right ls-s-1" style="color: #fff; font-size: 24px; top: 170px; left: 40px; slidedirection: left; slideoutdirection: top; durationin: 3500; durationout: 3500;"></i>

                <span class="ls-s-2" style="color: #fff; font-weight: 200; font-size: 22px; top: 170px; left: 70px; slidedirection: top; slideoutdirection: bottom; durationin: 3500; durationout: 2500;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 5px 20px;">Microsoft & Java technologies</i>

                </span>

                <i class="fa fa-chevron-circle-right ls-s-1" style="color: #fff; font-size: 24px; top: 220px; left: 40px; slidedirection: left; slideoutdirection: top; durationin: 4500; durationout: 3500;"></i>

                <span class="ls-s-2" style="color: #fff; font-weight: 200; font-size: 22px; top: 220px; left: 70px; slidedirection: top; slideoutdirection: bottom; durationin: 4500; durationout: 3500;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 5px 20px;">Application Development</i>

                </span>

                <i class="fa fa-chevron-circle-right ls-s-1" style="color: #fff; font-size: 24px; top: 270px; left: 40px; slidedirection: left; slideoutdirection: top; durationin: 5500; durationout: 4500;"></i>

                <span class="ls-s-2" style="color: #fff; font-weight: 200; font-size: 22px; top: 270px; left: 70px; slidedirection: top; slideoutdirection: bottom; durationin: 5500; durationout: 4500;">
                    <i style="background-color: #ef6262; opacity: 0.8; padding: 5px 20px;">Support and Testing</i>

                </span>

                <a class="btn-u btn-u-blue ls-s1" href="contact.aspx" style="padding: 9px 20px; font-size: 25px; top: 340px; left: 40px; slidedirection: bottom; slideoutdirection: bottom; durationin: 6500; durationout: 3500;">Ask for demo
                </a>

                <img src="assets/img/sliders/sam.png" alt="Slider Image" class="ls-s-1" style="top: 50px; left: 550px; slidedirection: right; slideoutdirection: bottom; durationin: 1500; durationout: 1500;" />
            </div>
            <!-- End  Fourth Slide -->
        </div>
        <!--/layer_slider-->
        <!-- What we do Starts here-->
		<section id="services" class="bg-grey">
			<div class="wow bounceInRight container content-md">
	        	<div class="title-v1">
					<h2>What WE DO</h2>
					<p style="text-align:center;">We are <strong>passionate</strong> about exploiting technology and deploying intelligent business practices<br> to meet the challenges faced by <strong>today&#39;s</strong> services industry.</p>
				</div>

				<div class="col-md-12">
					<div class="row">
						<div class="service-slider wow fadeIn animated ">
							<div class="card text-center">
				                <div class="service-block service-block-red service-or">
				                	<div class="service-bg"></div>
									<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-social-youtube"></i>
									<h2 class="heading-md">Bots and Machine learning</h2>
									<p class="justify">By definition, a bot (short for &#34; web robot &#34;) is a software program that operates as an agent for an individual, group of individuals, organization or even legitimate businesses.</p>
			                        <a href="botsandmachinelearning.aspx" class=" "><button type="button" class="btn-u btn-u-aqua ">Read More</button></a>
								</div>
				            </div>

				            <div class="card text-center">
				                <div class="service-block service-block-sea service-or">
				                	<div class="service-bg"></div>
									<i class="icon-custom icon-color-light rounded-x icon-line color-light icon-screen-smartphone"></i>
									<h2 class="heading-md">Mobility Apps</h2>
									<p class="justify">Apple&#39;s iPhone/iPad makes rich content, information and entertainment usable in locations both at home and on the road much more simply than before.</p>
			                        <a href="mobility.aspx" class=" "><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
								</div>
				            </div>

							<div class="card text-center">
				                <div class="service-block service-block-orange service-or">
									<div class="service-bg"></div>
									<i class="icon-custom icon-color-light icon-handbag rounded-x "></i>
									<h2 class="heading-md">E-Commerce Services</h2>
									<p class="justify">Our Magento practice offers a comprehensive suite of eCommerce related services spanning consulting, enterprise services and functionality additions.</p>
			                        <a href="retail.aspx" class=""><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
								</div>
				            </div>

							<div class="card text-center">
				                <div class="service-block service-block-grey service-or">
									<div class="service-bg"></div>
									<i class="icon-custom icon-color-light icon-directions icon-line "></i>
									<h2 class="heading-md">Geo Based/IOT Internet of things</h2>
									<p class="justify">Simplify IT context-enriched services use information about a person or object to proactively anticipate the user&#39;s need and serve up the content, product or service most appropriate to the user.</p>
			                        <a href="retail.aspx" class=""><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
								</div>
				            </div>
					
							<div class="card text-center">
				                <div class="service-block service-block-yellow service-or">
									<div class="service-bg"></div>
									<i class="icon-custom icon-color-light rounded-x icon-line icon-cloud-download"></i>
									<h2 class="heading-md">Cloud Integrations Management</h2>
									<p class="justify">At C SIMPLIFY IT, we have been helping enterprises drive business transformation by harnessing the power of technology.</p>
			                        <a href="cloud_integrations.aspx" class=""><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
								</div>
				            </div>

				            <div class="card text-center">
				                <div class="service-block service-block-brown service-or">
				                	<div class="service-bg"></div>
									<i class="icon-custom icon-color-light rounded-x icon-line color-light   icon-wrench "></i>
									<h2 class="heading-md">Testing</h2>
									<p class="justify">C Simplify IT provides Maintenance FREE Apps, to cut the pains of its customers and ensure that this channel should help customers improve.</p>
			                        <a href="testing-verticals.aspx" class=""><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
								</div>
				            </div>
		            	</div>
					</div>
				</div>
	        </div>
		</section>
		<!-- What we do Ends here-->
        <!-- Focused Verticals Starts here-->
<section id="verticals">
    <div class="container content-md">
        <div class="title-v1">
            <h2>Focused Verticals</h2>
            <p style="text-align:center;">We are <strong>passionate</strong> about exploiting technology and deploying intelligent business practices<br> to meet the challenges faced by <strong>today&#39;s</strong> services industry.</p>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="card-slider wow fadeIn animated">
                    <div class="card text-center">
                        <div class="service-block service-block-red service-or">
                            <div class="service-bg"></div>
                            <i class="icon-custom icon-color-light  icon-handbag  rounded-x "></i>
                            <h2 class="heading-md">Retail</h2>
                            <p class="justify">Our Retail practice offers a comprehen-sive suite of Front End/Back End IT Services spanning consulting,enterprise services , implementations, and functionality additions.</p><a href="retail.aspx"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>

                        </div>
                    </div>

                    <div class="card text-center">
                        <div class="service-block service-block-sea service-or">
                            <div class="service-bg"></div>
                            <i class="icon-custom icon-color-light  icon-plane  icon-line "></i>
                            <h2 class="heading-md">Transport Management</h2>
                            <p class="justify">TMS is a mobile enabled, web-based tool that helps customers gain real time visibility across their booking and delivery offices.</p><a href="transport-management.aspx"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                        </div>
                    </div>

                    <div class="card text-center">
                        <div class="service-block service-block-orange service-or">
                            <div class="service-bg"></div>
                            <i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-credit-card "></i>
                            <h2 class="heading-md">Financial</h2>
                            <p class="justify">C Simplify IT is leading the transformation of financial services. We have been helping clients meet growth objectives while coping with market realities.</p><a href="financial.aspx"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                        </div>
                    </div>

                    <div class="card text-center">
                        <div class="service-block service-block-grey service-or">
                            <div class="service-bg"></div>
                            <i class="icon-custom icon-color-light rounded-x icon-line color-light   icon-briefcase"></i>
                            <h2 class="heading-md">Health Care</h2>
                            <p class="justify">C Simplify IT involving in the design, development, creation, use and maintenance of information systems for the healthcare industry.</p><a href="healthcare.aspx"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                        </div>
                    </div>

                    <div class="card text-center">
                        <div class="service-block service-block-yellow service-or">
                            <div class="service-bg"></div>
                            <i class="icon-custom icon-color-light rounded-x icon-line color-light   icon-volume-1  "></i>
                            <h2 class="heading-md">Media and Publishing</h2>
                            <p class="justify">Process Re-engineering using BPM Publishing processes from production and editorial, to content management, rights management and ERP integration.</p><a href="mediapublish.aspx"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Focused Verticals Ends here-->
<!--Our Products Starts here-->
<section id="products" class="bg-grey">
    <div class="container content-md our-product">
        <div class="title-v1">
            <h2>Our Products</h2>
            <p>C Simplify IT products are excellent and the roadmap for future improvements is genuinely exciting.</p>
        </div>

        <div class="our-solution">
            <div class="row margin-bottom-40">
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-red service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light icon-layers rounded-x "></i>
                        <h2 class="heading-md">PushBiz - Be Visible</h2>
                        <p>Earn 10x More</p>
                        <a href="pushbiz.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-sea service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light icon-credit-card icon-line "></i>
                        <h2 class="heading-md">SIMS</h2>
                        <p>Cutting the supports cost </p>
                        <a href="sims.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-orange service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light rounded-x icon-line icon-bar-chart"></i>
                        <h2 class="heading-md">Fresher Villa</h2>
                        <p>Skill Builder</p>
                        <a href="freshervilla.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-grey service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light rounded-x icon-line color-light icon-diamond"></i>
                        <h2 class="heading-md">Talent Nest</h2>
                        <p>Knowledge Management</p>
                        <a href="tallent-nest.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-yellow service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-rocket"></i>
                        <h2 class="heading-md">Smart POS</h2>
                        <p>Geo/IOT Support</p>
                        <a href="smartpos.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-brown service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-handbag"></i>
                        <h2 class="heading-md">IN SYNCH</h2>
                        <p>Intelligent follow ups</p>
                        <a href="insynch.aspx" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Read More</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Our Products Ends here-->
<!-- Our Solutions Starts Here -->
<section id="solution" class="wow bounceInLeft">
    <div class="container content-md  no-gutter">
        <div class="title-v1">
            <h2>Our Solutions</h2>
            <p>C Simplify IT has executed numerous webbased and workflow onsite and offshore projects successfully and has created an extensive pool of resources that are trained to use the right processes, standards, and tools to deliver high levels of service.</p>
        </div>

        <div class="our-solution solutions">
            <div class="row margin-bottom-40">
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-red service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light icon-layers rounded-x "></i>
                        <h2 class="heading-md">SIMS</h2>
                        <p>Reports and Records incidence of failure on any web based system with single click</p>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-sea service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light icon-credit-card icon-line "></i>
                        <h2 class="heading-md">Payment Gateway Integration</h2>
                        <p>Improve your cash flow by enabling payments through Apple Pay, Google Playstore.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-orange service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light rounded-x icon-line icon-bar-chart"></i>
                        <h2 class="heading-md">Skill Builder</h2>
                        <p>Builds skills of your workforce to deliver quality. It trains, assess and compares their performance.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-grey service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light rounded-x icon-line color-light icon-diamond"></i>
                        <h2 class="heading-md">Constant Connect</h2>
                        <p>Communicate with your clients on missed calls and on marketing events, based on.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-yellow service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-rocket"></i>
                        <h2 class="heading-md">Smart Data Input Services</h2>
                        <p>Use OCR/ICR based scanning solution process your paper based orders on fly.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-lg-2">
                    <div class="service-block service-block-brown service-or">
                        <div class="service-bg"></div>
                        <i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-handbag"></i>
                        <h2 class="heading-md">Magento/ Shopify Integration</h2>
                        <p>Integrate orders on "internet of things" channel with your.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Colorful Service Blocks -->
<!-- Parallax Counter -->
<div class="parallax-counter-v4 parallaxBg1" id="facts">
    <div class="container content-sm">
        <div class="row">
            <div class="col-md-3 col-xs-6 md-margin-bottom-50">
                <i class="icon-cup"></i>
                <span class="counter">20</span>
                <h4>Coffee&#39;s Drunk</h4>
            </div>
            <div class="col-md-3 col-xs-6 md-margin-bottom-50">
                <i class="icon-clock"></i>
                <span class="counter">21</span>
                <h4>Projects</h4>
            </div>
            <div class="col-md-3 col-xs-6">
                <i class="icon-emoticon-smile"></i>
                <span class="counter">21</span>
                <h4>Happy Clients</h4>
            </div>
            <div class="col-md-3 col-xs-6">
                <i class=" icon-users"></i>
                <span class="counter">78</span>
                <h4>Team Members</h4>
            </div>
        </div>
    </div>
</div>
<!-- End Parallax Counter -->
<!-- About Section Starts here-->
<div class="about bg-grey">
    <div class="container content-sm">
        <div class="row margin-bottom-30">
            <div class="col-md-8 md-margin-bottom-40">
                <div class="headline"><h2>Welcome To C Simplify IT</h2></div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" width="420" height="315"
                                    src="https://www.youtube.com/embed/UTS8Y9VEpeY" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-sm-8 justify">
                        <p>C Simplify IT Services established in the year 2011 located in Gurgaon, India. We are handling projects in India, Singapore, Hong Kong, Thailand and USA. We are mainly focussed on Retail and Shipping & Logistics. Having team size of 70+ people. We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.We are mainly focussed in Retail, Transport Management, Financial Services and Health Care. We have developed six different products PushBiz - Be Visible, SIMS (Smart Incident management System), Freshers Villa - Skill Builder, Tallent Nest - Knowledge Management, Smart POS - Geo/IOT Support and IN SYNCH - Intelligent follow ups.Our in-house solution products are SIMS (Smart Incident management System), IN SYNC (Follow up framework).</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="headline"><h2>Our Clients</h2></div>
                <div id="myCarousel" class="wow bounceInRight carousel slide carousel-v1">
                    <div class="carousel-inner ">
                        <div class="item active block">
                            <a href="https://www.pushbiz.in/" target="_blank">
                                <img src="assets/img/main/img4.jpg" alt="">
                                <div class="carousel-caption block-caption text-center">
                                    <p class="justify text-center">C Simplify IT Product</p>
                                    <p class="justify text-center">Be - visible - Earn 10x More.</p><br>
                                </div>
                            </a>
                        </div>
                        <div class="item block">
                            <a href="https://myaccount.vinculumgroup.com/" target="_blank">
                                <img src="assets/img/main/express.png" alt="">
                                <div class="carousel-caption block-caption">
                                    <p class="justify text-center">Eretail Express</p>
                                    <p class="justify text-center">Sell on ecommerce marketplaces globally.</p><br>
                                </div>
                            </a>
                        </div>
                        <div class="item block">
                            <a href="#" target="_blank">
                                <img src="assets/img/main/tms1.png" alt="">
                                <div class="carousel-caption block-caption">
                                    <p class="justify text-center">Transport Management System</p>
                                    <p class="justify text-center">Transport Management System - Web based tool.</p><br>
                                </div>
                            </a>
                        </div>
                        <div class="item block">
                            <a href="http://www.vskills.in/practice/vSkillsHome" target="_blank">
                                <img src="assets/img/main/img2.jpg" alt="">
                                <div class="carousel-caption block-caption">
                                    <p class="justify text-center">Vskills</p>
                                    <p class="justify text-center">Thousnads of pratice tests.</p><br>
                                </div>
                            </a>
                        </div>
                        <div class="item block">
                            <a href="http://ec2-52-27-39-197.us-west-2.compute.amazonaws.com/" target="_blank">
                                <img src="assets/img/main/gyg.jpg" alt="">
                                <div class="carousel-caption block-caption">
                                    <p class="justify text-center">GYG</p>
                                    <p class="justify text-center">Responsive UI Point of sale</p>
                                    <p class="justify text-center">Singapore/Malaysia</p><br>
                                </div>
                            </a>
                        </div>
                        <div class="item block">
                            <img class="project-img" src="assets/img/main/bi.jpg" alt="">
                            <div class="carousel-caption block-caption">
                                <p class="justify">Priority Vendor</p>
                                <p class="justify">Analytics for Invoice Discounting</p>
                            </div>
                        </div>
                        <div class="item block">
                            <a href="smartpos.aspx">
                                <img class="project-img" src="assets/img/main/pos.jpg" alt="">
                                <div class="carousel-caption block-caption">
                                    <p class="justify">Vestige</p>
                                    <p class="justify">Point of Sale running at 3000+ locations</p>
                                </div>
                            </a>
                        </div>
                        <div class="item block">
                            <img class="project-img" src="assets/img/main/lanepro.png" alt="">
                            <div class="carousel-caption block-caption">
                                <p class="justify">Motherson Sumi</p>
                                <p class="justify">Mobility App For Servicing</p>
                            </div>
                        </div>
                        <div class="item block">
                            <img class="project-img" src="assets/img/main/picknpacks.png" alt="">
                            <div class="carousel-caption block-caption">
                                <p class="justify">Ur Doorstep</p>
                                <p class="justify">Mobility App For Shipping Orders</p>
                            </div>
                        </div>
                        <div class="item block">
                            <img class="project-img" src="assets/img/main/shipping.jpg" alt="">
                            <div class="carousel-caption block-caption">
                                <p class="justify">Ur Doorstep</p>
                                <p class="justify">Mobility App For Shipping Orders</p>
                            </div>
                        </div>
                        <div class="item block">
                            <img class="project-img" src="assets/img/main/eretail.png" alt="">
                            <div class="carousel-caption block-caption">
                                <p class="justify">eRetail</p>
                                <p class="justify">Mobility App For Inventory Management System</p>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-arrow">
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--About Section Ends here-->
<!--Our Partners Starts here-->
<div class="container content-sm" id="ourpartners">
    <div class="title-v1">
        <h2>Our Partners</h2>
    </div>
    <div class="row">
        <div class=" wow rotateInDownLeft col-md-6 md-margin-bottom-40">
            <div class="funny-boxes funny-boxes-colored funny-boxes-red">
                <div class="row">
                    <div class="col-md-4 funny-boxes-img">
                        <img alt="" src="assets/img/patterns/vheader.png" class="img-responsive partner-logo-img">
                        <ul class="list-unstyled">
                            <li><i class="fa-fw fa fa-briefcase"></i> Noida </li>
                            <li><i class="fa-fw fa fa-map-marker"></i> Uttar Pradesh, India</li>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <h2><a href="#">Vinculum</a></h2>
                        <ul class="list-unstyled funny-boxes-rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                        <p class="justify">Vinculum partnered with C Simplify IT for Implementation & Roll out of Retail Products, Standardization of Store and Merchandising Operations. C Simplify IT helped to implement and rollout the Retail products in record time.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="wow rotateInDownRight col-md-6">
            <div class="funny-boxes funny-boxes-colored funny-boxes-blue">
                <div class="row">
                    <div class="col-md-4 funny-boxes-img">
                        <img alt="" src="assets/img/patterns/webexpress.jpg" class="img-responsive partner-logo-img">
                        <ul class="list-unstyled">
                            <li><i class="fa-fw fa fa-briefcase"></i> Chamarajpet</li>
                            <li><i class="fa-fw fa fa-map-marker"></i> Bangalore, India </li>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <h2><a href="#">Webxpress</a></h2>
                        <ul class="list-unstyled funny-boxes-rating">
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                        <p class="justify">Webxpress partnered with C Simplify IT for mobile enabled and web-based tool that help the customers gain real time visibility across their booking and delivery offices through Transportation Products.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Our Partners Ends here-->
<!--Our Clients Starts here-->
 <div class="col-md-12 clients-section parallaxBg">
    <div class="container">
        <div class="title-v1">
            <h2>Our Clients</h2>
        </div>
        <ul class="owl-clients-v2">
            <li class="item"><a href="https://www.panasonic.com/in/" target="_blank"><img src="assets/img/clients/panasonic.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="http://www.krahejacorp.com/" target="_blank"><img src="assets/img/clients/kraheja.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="http://www.neubio.io/" target="_blank"><img src="assets/img/clients/neubio.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="http://www.myvestige.com/" target="_blank"><img src="assets/img/clients/vestige-logo.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="https://paytm.com/" target="_blank"><img src="assets/img/clients/paytm.png" alt=""></a></li>
            <li class="item"><a href="http://www.apllvascor.com/" target="_blank"><img src="assets/img/clients/apl-logo.png" alt=""></a></li>
            <li class="item"><a href="https://bondevalue.com/" target="_blank"><img src="assets/img/clients/bondevalue.png" alt=""></a></li>
            <li class="item"><a href="http://transorg.com/" target="_blank"><img src="assets/img/clients/transorg.png" alt=""></a></li>
            <li class="item"><a href="http://www.askmebazaar.com/"><img src="assets/img/clients/askmebazaar.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="https://www.vinculumgroup.com/"><img src="assets/img/clients/vinculum.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="http://www.jardines.com/" target="_blank"><img src="assets/img/clients/jardine.jpeg" alt=""></a></li>
            <li class="item"><a href="http://www.aswatson.com/" target="_blank"><img src="assets/img/clients/aswatson.png" alt=""></a></li>
            <li class="item"><a href="https://www.priorityvendor.com/priorityvendor/handleGeneral.do"><img src="assets/img/clients/priority-vendor.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="http://www.urdoorstep.com/" target="_blank"><img src="assets/img/clients/ur-doorstep.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="http://www.vskills.in/practice/vSkillsHome"><img src="assets/img/clients/vskills.jpg" alt="" target="_blank"></a></li>
            <li class="item"><a href="http://www.dhl.co.in/en.aspx" target="_blank"><img src="assets/img/clients/dhl.png" alt=""></a></li>
            <li class="item"><a href="http://coldex.in/" target="_blank"><img src="assets/img/clients/coldex.png" alt="" target="_blank"></a></li>
            <li class="item"><a href="http://www.motherson.com/motherson-sumi-systems-limited.aspx"><img src="assets/img/clients/motherson.jpg" alt="" target="_blank"></a></li>
        </ul>
    </div>
</div>
<!--Our Client Ends here-->
     </div>   
    <!--/wrapper-->
    
</asp:Content>

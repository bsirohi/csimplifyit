<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>Recruitment Services | C Simplify IT - Do eCommerce!10x better.</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
    
    <meta name="keywords" content="Magento, service,services, practice, comphrehensive, eCommerce, spanning, consulting, services & more, sevices and more, retail process, knowledge,tools, consulting, expertise, enhance, efficiency, precision, decrease cycle, fulfillment, ready to use, set of package, website creation, competitive prices, fully operating, short deadlines, all in one, actions, extending, appealing solutions, start-ps, lower costs">
	<meta name="author" content="gaurav developer">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@dmehta104">
    <meta name="twitter:creator" content="@dmehta104">
    <meta name="twitter:title" content="C Simplify IT - E-Commerce Services">
    <meta name="twitter:description" content="Our Magento practice offers a comprehensive suite of eCommerce related services spanning consulting, enterprise services and functionality additions. Our robust retail process knowledge, specialized tools, and consulting expertise helps us to enhance eCommerce efficiency and fulfillment precision, reduce costs and decrease cycle times from order to fulfillment...">
    <meta name="twitter:image" content="http://csimplifyit.com/assets/img/banners/ExtensiveMagentoDevelopmentServices.png">
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="assets/css/headers/header-default.css">
    <link rel="stylesheet" href="assets/css/headers/header-v6.css">
	<link rel="stylesheet" href="assets/css/footers/footer-v2.css">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="assets/plugins/animate.css">
	<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css">
	<link rel="stylesheet" href="assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css">

	<!-- CSS Page Style -->
	<link rel="stylesheet" href="assets/css/pages/page_search.css">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="assets/css/theme-colors/default.css" id="style_color">
	<link rel="stylesheet" href="assets/css/theme-skins/dark.css">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="assets/css/custom.css">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-63272985-1’, 'auto');
        ga('send', 'pageview');
    </script>
</head>


<body class="header-fixed header-fixed-space">

	<div class="wrapper">
    
		<!--=== Header ===-->
		<div class="header header-sticky">
			<div class="container">
				<!-- Logo -->
				<a class="logo" href="Default.aspx">
					<img src="assets/img/logo1-default.png" alt="Logo">
				</a>
				<!-- End Logo -->
                 <!-- Topbar -->
				<div class="topbar">
					<ul class="loginbar pull-right">
						<li class="hoverSelector">
							<i class="icon-custom  rounded-x   icon-call-in "></i>
							<a href="tel://+9198999 76227" style="font-size:14px;">+91 98999 76227</a>
					</ul>
				</div>
				<!-- End Topbar -->

				
				<!-- Toggle get grouped for better mobile display -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
                   
				</button>
				<!-- End Toggle -->
			</div><!--/end container-->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
				<div class="container">
					<ul class="nav navbar-nav">
                        <!-- Verticals -->
						<li><a href="technology.aspx">Technology</a></li><li class="dropdown "><a class="dropdown-toggle" data-toggle="dropdown" href="#">Verticals</a>
				            <ul class="dropdown-menu">
										<li><a href="retail.aspx">Retail</a></li>
										<li><a href="transport-management.aspx">Transport Management</a></li>
                                        <li><a href="mediapublish.aspx">Media and Publishing</a></li>
										<li><a href="financial.aspx">Financial Services</a></li>
                                        <li><a href="healthcare.aspx">Health Care</a></li>
                                        
				            </ul>
						</li>
						<!-- End Verticals -->
                        
                        <!---Products ------>
                        <li ></li>
                        <li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Products</a>
				            <ul class="dropdown-menu">
										<li><a href="pushbiz.aspx">PushBiz - Be Visible</a></li>
										<li><a href="sims.aspx">SIMS - Smart Incedent Management System</a></li>
										<li><a href="freshervilla.aspx">FresherVilla - Skill Builder</a></li><li><a  href="transport-management-system.aspx">TMS - Transport Management System</a></li></li>
										<li><a href="tallent-nest.aspx">Talent Nest - Knowledge Management</a></li>
                                        <li><a href="smartpos.aspx">Smart POS - Geo/IOT Support</a></li>
                                        <li><a href="insynch.aspx">IN SYNCH - Intelligent follow ups</a></li>
                                        
				            </ul>
						</li>
                        <!---End Products ----->

						<!-- Servces -->
						<li class="dropdown active">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Services</a>
				            <ul class="dropdown-menu">
										 <li><a href="bots-machinelearning.aspx">Bots and Machine Learning Services</a></li><li><a href="bpm-services.aspx">BPM Services</a></li> <li><a href="e-commerce.aspx">E-Commerce Services</a></li>
										<li><a href="geo-based-iot-internet-of-thing-services.aspx">Geo Based/IOT Internet Of Thing Services</a></li> <li><a href="artificial-intelligence-services.aspx">Artificial Intelligence Services</a></li>
										<li><a href="cloud_integrations.aspx">Cloud Integration Management Services</a></li>
                                        <li ><a href="mobility_apps.aspx">Mobility Apps Services</a></li>
									                                 
                                        <li><a href="testing.aspx">Testing Services</a></li>
				            </ul>
						</li>
						<!-- End Services -->
                        <!-- Pages -->
						  <li class=""><a href="aboutus.aspx" class="">About Us</a></li>
						<!-- End Pages -->
                        
                        <!----Contact Us ---->
                        <li class=""><a href="contact.aspx" class="">Contact Us</a></li>
						<!-- End Contact Us -->

					</ul>
				</div><!--/end container-->
			</div><!--/navbar-collapse-->
		</div>
		<!--=== End Header ===-->


		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Recruitment Services</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.aspx">Home</a></li>
					<li><a href="">Services</a></li>
					<li class="active">Recruitment Services</li>
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row">
				<div class="col-md-6">
					<h2 class="title-v2">Recruitment Services</h2>
					<p>Hire world class talent "just in time", using recruit circle, having 300+ recruiters across globe. Not only C Simplify IT will find you 10x better resources, but in 10x shorter timeframes.</p><br>
				
				<p><i class="fa fa-arrow-circle-right color-green"></i> Source 10x talent for your business, across Globe. 300+ recruit circle services for customer.</p>
				
				<p><i class="fa fa-arrow-circle-right color-green"></i> Best in class Quality and SLA for worldwide resourcing requirements. With-in 8 hrs*  </p>
				
				<p><i class="fa fa-arrow-circle-right color-green"></i> Technical Geeks and Domain specific Search Services. World class talent.</p>
				
				<p><i class="fa fa-arrow-circle-right color-green"></i> Contract to Hire (minimum 6 months), for immediate short term projects</p>
				
				<p><i class="fa fa-arrow-circle-right color-green"></i> Train your people/setup "In house Academy" for skill building  </p>
				
				</div>
				<div class="col-md-6 ">
					<img class=" wow tada img-responsive pull-right md-pt-40" style="margin-top:100px;height:140px;" src="http://tms.csimplifyit.com/images/RCircleLogo.png" alt="">
				</div>
			</div>
		</div><!--/container-->
		<!--=== End Content Part ===-->
        <!--=== Content Part ===-->
	

    <!--=== Footer v2 ===-->
		<div id="footer-v2" class="footer-v2">
			<div class="footer">
				<div class="container">
					<div class="row">
						<!-- About -->
						<div class="col-md-3 md-mt-40">
							<a href="index.aspx"><img id="logo-footer" class="footer-logo" src="assets/img/logo-original.png" alt=""></a>
							<p class="margin-bottom-20">C Simplify IT Services established in the year 2011 located in gurgaon, India. we are handling projects in India, Singapore, Hong Kong, Thailand and USA.We have expertise in Expertise in BI, Content Management, Microsoft & Java technologies, Application Development, Support and Testing.</p>

							</div>
						<!-- End About -->

						<!-- Link List -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="headline"><h2 class="heading-sm">Useful Links</h2></div>
							<ul class="list-unstyled link-list">
								<li><a href="aboutus.aspx">About us</a><i class="fa fa-angle-right"></i></li>
								<li><a href="e-commerce.aspx">Services</a><i class="fa fa-angle-right"></i></li>								
								<li><a href="retail.aspx">Verticals</a><i class="fa fa-angle-right"></i></li>
								<li><a href="contact.aspx">Contact us</a><i class="fa fa-angle-right"></i></li>
                                <li><a href="sitemap.xml">Sitemap</a><i class="fa fa-angle-right"></i></li>
							</ul>
						</div>
						<!-- End Link List -->

						<!-- Latest Tweets -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="latest-tweets">
								<div class="headline"><h2 class="heading-sm">Social Links</h2></div>
								<div class="latest-tweets-inner">
									<ul class="social-icons">
                                        <li>
                                            <a href="https://www.facebook.com/CSimplifyIT-210115279023481/"
                                               data-original-title="Facebook" class="rounded-x social_facebook" target="_blank"></a></li>
                                        <li><a href="https://twitter.com/dmehta104" data-original-title="Twitter" class="rounded-x social_twitter" target="_blank"></a></li>
                                        <li><a href="https://plus.google.com/u/0/+CSimplifyITServicesPrivateLimitedNewDelhi/about" data-original-title="Goole Plus" class="rounded-x social_googleplus" target="_blank"></a></li>
                                        <li><a href="https://www.linkedin.com/company/2702366" data-original-title="Linkedin" class="rounded-x social_linkedin" target="_blank"></a></li>
                                    </ul>
                                </div>
							</div>
						</div>
						<!-- End Latest Tweets -->

						<!-- Address -->
						<div class="col-md-3 md-margin-bottom-40 md-mt-40">
							<div class="headline"><h2 class="heading-sm">Contact Us</h2></div>
							<address class="md-margin-bottom-40">
								<i class="fa fa-home"></i>C Simplify IT Services Private Limited <br />&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;SCO-36, Huda Market, Sector 31,<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbspGurgaon, Haryana ( India ).<br />
								<i class="fa fa-phone"></i>Phone: +91 98999 76227 <br />
								<i class="fa fa-globe"></i>Website: <a href="#">www.csimplifyit.com</a> <br />
								<i class="fa fa-envelope"></i>Email: <a href="sales@csimplifyit.com" class="">sales@csimplifyit.com</a></a>
							</address>

							<!-- Social Links -->
							
							<!-- End Social Links -->
						</div>
						<!-- End Address -->
					</div>
				</div>
			</div><!--/footer-->

			<div class="copyright">
				<div class="container">
					<p class="text-center"><script>	document.write(new Date().getFullYear());</script> &copy; All Rights Reserved. by <a target="_blank" href="index.aspx">C Simplify IT</a></p>
<p class="text-center" style="font-size:12px;">Contact us @ Cues Simplify IT Services Private Limited (CIN U72900HR2011PTC043111)
Regd. Office: H No 314/21, Street No 5, Ward No 21, Madanpuri, Gurgaon, Haryana - 122001, INDIA<br/> Phone: +91 98 999 76227 Email ID: ez@CSimplifyIT.com</p>
				</div>
			</div><!--/copyright-->
		</div>
		<!--=== End Footer v2 ===-->
</div><!--/wrapper-->



<!-- JS Global Compulsory -->
<script type="text/javascript" src="assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="assets/plugins/jquery.parallax.js"></script>
<script type="text/javascript" src="assets/plugins/counter/waypoints.min.js"></script>
<script type="text/javascript" src="assets/plugins/counter/jquery.counterup.min.js"></script>
<script type="text/javascript" src="assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="assets/js/app.js"></script>
<script type="text/javascript" src="assets/js/plugins/style-switcher.js"></script>
<script type="text/javascript" src="assets/js/plugins/cube-portfolio/cube-portfolio-lightbox.js"></script>
<script type="text/javascript" src="assets/plugins/wow-animations/js/wow.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        App.initCounter();
        App.initParallaxBg();
        StyleSwitcher.initStyleSwitcher();
        new WOW().init();
    });
</script>
<!--[if lt IE 9]>
	<script src="assets/plugins/respond.js"></script>
	<script src="assets/plugins/html5shiv.js"></script>
	<script src="assets/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->

</body>
</html>

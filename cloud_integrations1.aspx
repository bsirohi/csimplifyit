﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cloud_integrations.aspx.cs" Inherits="csimplifyit.WebForm6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->
 <br />
 <div class="clear"></div>
 <hr />
<h1 class="text_blue">Cloud Integrations</h1>
 <div class="clear"></div>
 <div class="three_fourth">
 <p>
 At C SIMPLIFY IT, we have been helping enterprises drive business transformation by harnessing the power of technology. Leveraging technology 
expertise & decades of experience in managing multiple customer IT environments,C SIMPLIFY IT team has put together the right people, tools and processes to deliver 
end-to-end cloud IT services.</p>

<p><span class="text_red">Cross-functional or enterprisewide initiatives, such as business process integration or multichannel sales, require integration of applications and services belonging 
to different SOA domains.</span> As organizations move to the more advanced stages of SOA adoption, they will increasingly look at a federated SOA approach to: 
 </p>
 <ul>
 <li>
    Integrate, coordinate and harmonize different "internal" SOA domains</li>
    <li>Deal with "external" domains, such as partners, clients, suppliers, outsourcers, contractors and service providers </li>
 
 </ul>
    
 </div>

 <div class="one_fourth column-last">
     <img class="radius_10" src="images/Clouds.png" />
 </div>
 <div class="clear"></div>
 <h3 class="text_red">C SIMPLIFY IT Cloud Offerring includes</h3>

 <ul>
	<li>An unbiased assessment of the application portfolio to obtain readiness to be moved into a given cloud environment </li>
	<li>Helps choose the right cloud environment, by providing an objective comparison amongst various cloud models - ROI Assessments</li>
	<li>Helps in laying out roadmap for cloud adoption by identifying high potential candidates that can quickly take advantage of cloud benefits </li>
	<li>Allows to decide the correct threshold by rating the threshold parameter values rather than choosing an arbitrary point </li>
	<li>Narrows down the scope of analysis for cost-benefit, Risk Mitigation, migration approach, target platform etc. </li>
	<li>Cloud migration planning</li>
	<li>General cloud education and best practices </li>
</ul>
<p>Why customers need to evaluate the cloud offerings now !:</p>

<p class="text_red "><strong>Ride the wave</strong></p>

<ul class="dashed_list">
<li>The Cloud is an emerging phenomenon,but will be mainstream in 3-5 years </li>

</ul>


<p class="text_red "><strong>You and your subsidiaries need to begin now</strong></p>

<ul class="dashed_list">
    <li> We are at the outer bands of an approaching hurricane</li>
    <li>Business models need to be adapted</li>
    <li>Investments and partnerships need to be evaluated</li>
	<li>This takes time</li>
	

</ul>


<p class="text_red "><strong>Position yourself for success</strong></p>

<ul class="dashed_list">
<li>Determine what your role(s) is going to be </li>
<li>Develop your competitive differentiation</li>
	
</ul>

</div>
</asp:Content>

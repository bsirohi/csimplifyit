﻿using csimplifyit.DBContext;
using csimplifyit.Model;
using csimplifyit.MsgFormatter;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace csimplifyit.SubScriptionDetails
{
    public partial class UserSubScriptionDetails : System.Web.UI.Page
    {
        private static csitEntities1 db = new csitEntities1();
        private static Dictionary<string, object> moResponse;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static object getALlPaidSubSriptionDetails(string requestData)
        {
            try
            {

                Login.Login log = new Login.Login();
                long UserID = log.getUserId();

                if (UserID == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, "Session Time Out. Plaese Login!", null, "saveStripPayments");
                    return moResponse;
                }

                var getUserType = db.usermasters.Where(s => s.userid == UserID).FirstOrDefault();

                JObject jobt = JObject.Parse(requestData);
                                  
                var getallData = "select sp.planDescription as planName,sp.planAutoRenewalDescription as PlanType,(um.FirstName) as UserName, "
                               +" us.associatedPlanId as PlanID, us.uspId as UserSubScriptionID, date_format(us.subscriptionOn, '%d-%b-%Y') as FromDate,  "
                               +" date_format(us.subscriptionEnd, '%d-%b-%Y') as ToDate, clproductype.Key1 as producttype   from user_subscriptions us "
                               +" join subscription_plan sp on us.associatedPlanId = sp.spId "
                               +" join usermaster um on um.userid = us.userId "
                               +" join codelkup clproductype on clproductype.KeyCode = sp.subscriptionProduct and clproductype.LkUpCode = 'PRODUCT_NAME' "
                               +" where us.status not in(0,-100)  ";


                int PlanID = 0;
                bool lbSuccess = false;
                DateTime fromDate = new DateTime();
                DateTime toDate = new DateTime();

                MySqlParameter ldfromDatepmtr = new MySqlParameter();
                MySqlParameter ldToDatepmtr = new MySqlParameter();
                MySqlParameter lsplanNamepmtr = new MySqlParameter();
                MySqlParameter lsproductTypepmtr = new MySqlParameter();
                MySqlParameter lsuserNamepmtr = new MySqlParameter();
                MySqlParameter lsuseridpmtr = new MySqlParameter();

                if(getUserType!=null)
                {
                    if (getUserType.userType != 0)
                    {
                        getallData += " and (us.userId)=@userId";
                        lsuseridpmtr = new MySqlParameter("@userId", UserID);
                    }
                }
                


                if (jobt["fromDate"] != null && jobt["fromDate"].ToString() != "" && jobt["fromDate"].ToString() != string.Empty)
                {
                    lbSuccess = DateTime.TryParse(jobt["fromDate"].ToString(), out fromDate);
                    if (lbSuccess == false)
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "Can not parse from Date", null, "getALlPaidSubSriptionDetails");
                        return moResponse;
                    }
                    else
                    {
                        ldfromDatepmtr = new MySqlParameter("@fromDate", fromDate);
                        getallData += " and date(us.subscriptionOn)>=date(@fromDate)";                      
                        lbSuccess = true;
                    }
                }

                if (jobt["toDate"] != null && jobt["toDate"].ToString() != "" && jobt["toDate"].ToString() != string.Empty)
                {
                    lbSuccess = DateTime.TryParse(jobt["toDate"].ToString(), out toDate);
                    if (lbSuccess == false)
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "Can not parse To Date", null, "getALlPaidSubSriptionDetails");
                        return moResponse;
                    }
                    else
                    {
                        ldToDatepmtr = new MySqlParameter("@toDate", toDate);
                        getallData += " and date(us.subscriptionOn)<=date(@toDate)";
                      
                        lbSuccess = true;
                    }
                }


                if (jobt["planName"] != null && jobt["planName"].ToString() != "" && jobt["planName"].ToString() != string.Empty)
                {                   
                    getallData += " and sp.planDescription like @planName";
                    lsplanNamepmtr = new MySqlParameter("@planName", "%"+ jobt["planName"].ToString()+"%");                     
                }

                if (jobt["productType"] != null && jobt["productType"].ToString() != "" && jobt["productType"].ToString() != string.Empty)
                {
                    getallData += " and clproductype.Key1 like @productType";
                    lsproductTypepmtr = new MySqlParameter("@productType", "%" + jobt["productType"].ToString() + "%");
                }

                if (jobt["userName"] != null && jobt["userName"].ToString() != "" && jobt["userName"].ToString() != string.Empty)
                {
                    getallData += " and um.FirstName like @userName";
                    lsuserNamepmtr = new MySqlParameter("@userName", "%" + jobt["userName"].ToString() + "%");
                }

                object[] parameters = new object[] { ldfromDatepmtr , ldToDatepmtr , lsplanNamepmtr, lsproductTypepmtr, lsuserNamepmtr, lsuseridpmtr };

                var Result = db.Database.SqlQuery<getPaidUserSubscription>(getallData + " order by us.uspId desc  limit 100 ", parameters).ToList();              
                moResponse = RespMsgFormatter.formatResp(1, "Successfully Execute", Result, "getALlPaidSubSriptionDetails");
                return moResponse;
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:" + ex.InnerException + "Message:" + ex.Message + "Stack Trace:" + ex.StackTrace, null, "getALlPaidSubSriptionDetails");
                return moResponse;
            }
        }




    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserSubScriptionDetails.aspx.cs" Inherits="csimplifyit.SubScriptionDetails.UserSubScriptionDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        
    <link href="../assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="../assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="../assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="../assets/css/theme.css" rel="stylesheet">


     <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.jpg">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.jpg">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicons/favicon.ico">

    <link rel="manifest" href="../assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="../assets/img/favicons/mstile-150x150.jpg">
    <meta name="theme-color" content="#ffffff">

    <meta name="google-sigin-client_id" content="509303823636-4nmni39qd4m0egmiv7cgkdqq57teuo5h.apps.googleusercontent.com" />
        <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->

    <link href="../assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="../assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="../assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="../assets/lib/semantic-ui-accordion/accordion.min.css" rel="stylesheet">
    <link rel="../stylesheet" href="assets/css/font-awesome.css">
    <link href="../assets/css/theme.css" rel="stylesheet">
    <link rel="../stylesheet" type="text/css" href="assets/css/document_management.css">
     <%--<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet" />--%>
    <link href="../contents/UtilityCss/common.css" rel="stylesheet" />

    <link href="../contents/DataTableCss/jquery.dataTables.min.css" rel="stylesheet" />

   

    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
   
   
    <script src="../assets/js/plugins.js"></script>
    <script src="../assets/lib/loaders.css/loaders.css.js"></script>
    <script src="../assets/js/stickyfill.min.js"></script>
    <script src="../assets/lib/remodal/remodal.js"></script>
    <script src="../assets/lib/jtap/jquery.tap.js"></script>
    <script src="../assets/js/rellax.min.js"></script>
    <script src="../assets/lib/owl.carousel/owl.carousel.js"></script>
    <script src="../assets/lib/lightbox2/js/lightbox.js"></script>
    <script src="../assets/lib/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="../assets/lib/isotope-packery/packery-mode.pkgd.min.js"></script>
    <script src="../assets/js/Default.js"></script>
    <%--<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
     <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>--%>
    <script src="../assets/js/theme.js"></script>
    
    <script src="../Scripts/Utility/common.js"></script>

  <%-- <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>--%>
    <script src="../Scripts/DataTableJs/dataTables.fixedHeader.js"></script>
    <script src="../Scripts/DataTableJs/jquery.dataTables.min.js"></script>

    <script src="../Scripts/UserSubsription/UserSubscription.js"></script>
   
    


    <script type="text/javascript">

       
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
              m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
          ga('create', 'UA-63272985-1', 'auto');
          ga('send', 'pageview');

      </script>



    
        <style type="text/css">
     .plans:hover {
        box-shadow: 1px 5px 8px #3333337d;
        background: white !important;
      }
      #pricing_details li {
          list-style: none;
      }
      .yes_feature:before {
          content: '\2713';
          color: green;
          font-weight: bold;
          padding-right: 5px;
      }
      .not_feature:before {
          content: '\2715';
          color: red;
          font-weight: bold;
          padding-right: 5px;
      }
      .promo-code { 
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
      }
      .promo-checkbox { margin: 0.5rem 0; }
      .upgradePlans {
        color: blue;
        font-weight: bold;
        font-size: 20px;
        display: block;
      }
      #studio-contact {
          border-bottom: 2px solid #929292;
      }
      .amount span{
        font-size: 40px; 
        vertical-align: top; 
        font-weight: bold; 
        color: #333;
      }
      .discountedPrice {
        font-size: 24px !important; 
        text-decoration: line-through;
      }
      .needs-validation label {
    font-size: 1rem;
    color: #000;
}
    </style>
</head>
<body>
    
    <div>
    

    <!--===============================================-->
    <!--    Fancynav-->
    <!--===============================================-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#landing-page">
          <img src="../assets/img/logo.png" alt="C Simplify IT" height="45" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown1">
          <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#landing-page">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#whatwedo">Solutions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#offerings">Offerings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#technology">Technologies</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbarDropdownMenuProduct" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product</a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuProduct">
                <a class="dropdown-item" href="SM3/SimpleMassMailMerge.aspx">Simple Mass Mail Merge</a>
                     <a class="dropdown-item" href="SRM/SimpleRecruitmentManager.aspx">Simple Recruitment Manager</a>
                  <a class="dropdown-item" href="googleworkflow/SimpleWorkflowManager.aspx">Simple Workflow Manager</a>
                  
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Strip_Payment_way/StripePayment.aspx">Pricing</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#whyus">Why Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#clients">Our Clients</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#contact">Contact Us</a>
            </li>
                  <li class="nav-item" id="AddUserName">                                                      
            </li>
               <li class="nav-item" id="AddLogOutButton">                                                         
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!--===============================================-->
    <!--    End of Fancynav-->
    <!--===============================================-->



    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main minh-100vh" id="top">


      <!-- ============================================-->
      <!-- Preloader ==================================-->
      <div class="preloader" id="preloader">
        <div class="loader">
          <div class="line-scale-pulse-out-rapid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
      <!-- ============================================-->
      <!-- End of Preloader ===========================-->

	  
	  <!-- ============================================-->
      <!-- <section> begin ============================-->
      <div class="speciality">
        <a href="../Our_Specialities.aspx">Our Specialities</a>
      </div>
      <!-- <section> close ============================-->
      <!-- ============================================-->
	  
      <!-- <User Details> Open ============================-->
      <!-- ============================================-->

          <!-- <section> begin ============================-->	
      <section class="py-0" id="planForm">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 py-8">
                <div class="col-12 mb-3 mb-md-5 text-center">
                    <h2 class="fs-3 fs-sm-4"><span class="text-underline">Plan</span></h2>
                  </div>
                <div class="formSection mb-3 mb-md-5">  
                  <div class="needs-validation mt-4" novalidate>
                  <div class="form-row">
                    <div class="col-md-4 mb-3">
                      <label for="fromDate">From Date</label>
                      <input class="form-control form-control-sm" id="fromDate" type="date" placeholder="From Date" required="">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="toDate">To Date</label>
                      <input class="form-control form-control-sm" id="toDate" type="date" placeholder="To Date" required="">
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="planName">Plan Name</label>
                      <input class="form-control form-control-sm" id="planName" type="text" placeholder="Plan Name" required="">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-6 mb-3">
                      <label for="productType">Product Type</label>
                      <input class="form-control form-control-sm" id="productType" type="text" placeholder="Product Type" required="">
                    </div>
                   <%-- <div class="col-md-3 mb-3">
                      <label for="status">Status</label>
                      <select class="form-control form-control-sm" id="status">
                        <option>Done</option>
                        <option>Pending</option>
                      </select>
                    </div>--%>
                    <div class="col-md-6 mb-3">
                      <label for="userName">User Name</label>
                      <input class="form-control form-control-sm" id="userName" type="text" placeholder="User Name" required="">
                    </div>
                  </div>
                       <div class="form-row">
                    <div class="col-md-6 mb-3">
                     
                    </div>
                    <div class="col-md-3 mb-3">
                     
                    </div>
                    <div class="col-md-3 mb-3" style="padding-left: 96px;">
                        <button class="btn btn-secondary btn-sm" onclick="Reset()" type="submit">Reset</button>
                     <button class="btn btn-primary btn-sm" onclick="SearchUserSubscription()" type="submit">Submit</button>
                    </div>
                  </div>
                  
                </div>
              </div>

              <div class="table-responsive mt-3">
                <table class="table table-striped table-bordered" id="grdUserPaidSubscription" style="width:100%"">
                  <thead>
                    <tr>
                      <th hidden="hidden">PlanID</th>
                      <th hidden="hidden">SubscriptionID</th>

                      <th>Plan Name</th>
                      <th>Product Type</th>
                      <th>From Date</th>
                      <th>To Date</th>
                      <th>User Name</th>
                      <th>Plan Type</th>
                    </tr>
                  </thead>              
                </table>
              </div>
              </div>
            </div>
          </div>
        <!-- end of .container-->

      </section>
      <!-- <section> close ============================-->
      <


      <!-- <section> close ============================-->
      <!-- ============================================-->

    </div>
  
</body>
</html>

<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  %>
<asp:Content ID="AboutHead" ContentPlaceHolderID="HeadContent" runat="server">
    <title>About Us | C Simplify IT - Do eCommerce!10x better.</title>
    
    
</asp:Content>
<asp:Content ID="AboutMaincontent" ContentPlaceHolderID="MainContent" runat="server">


	<div class="wrapper">
    
		<!--=== Breadcrumbs v3 ===-->
		<div class="breadcrumbs-v3 img-v3 text-center">
			<div class="container">
				<h1>ABOUT C SIMPLIFY IT</h1>
				<p> </p>
			</div><!--/end container-->
		</div>
		<!--=== End Breadcrumbs v3 ===-->

		<!--=== Title v1 ===-->
		<div class="container content-sm">
			<div class="title-v1 no-margin-bottom">
                <h2 class="no-margin-bottom">Premier IT Company for eCommerce, BPM and Mobility</h2>
					<p>We make things <strong>10X better!</strong> by simplifying and engaging customers in new ways.</p>
				</div>
			</div>
			<!--=== End Title v1 ===-->

			<!--=== Service Block v4 ===-->
			<div class="service-block-v4">
				<div class="container content-sm">
					<div class="row">
						<div class="col-md-4 service-desc md-margin-bottom-50">
							<i class="icon-diamond"></i>
							<h3>Design</h3>
							<p class="no-margin-bottom">We have created a company focused around a growing number of highly skilled individuals who are expert in delivering well designed solutions.</p>
						</div>
						<div class="col-md-4 service-desc md-margin-bottom-50">
							<i class="icon-rocket"></i>
							<h3>Development</h3>
							<p class="no-margin-bottom">We have expertise in BI, Content Management, Microsoft & Java technologies, Application Development.</p>
						</div>
						<div class="col-md-4 service-desc">
							<i class="icon-support"></i>
							<h3>Support</h3>
							<p class="no-margin-bottom">we are able to deliver short term results that support long term development goals.</p>
						</div>
					</div><!--/end row-->
				</div><!--/end container-->
			</div>
			<!--=== End Service Block v4 ===-->

			<!--=== Our Skills ===-->
			<div class="container content-sm">
				<div class="headline-center margin-bottom-60">
					<h2>WHAT WE ARE GOOD</h2>
					<p>C Simplify IT Services helps its Customers increase their customer engagement, brand recognition, leads and sales. </p>
					</div>

					<div class="row">
						<div class="col-sm-6 md-margin-bottom-50">
							<p>We offer you a low-risk, fixed price contract, based on fully scoped deliverables. We will definitely make things simple and build a strong case for long term associations. In "C SImpify IT" we have created a company focused around a growing number of highly skilled individuals who are expert in delivering well designed solutions. Using excellent 'intelligent' business practices and best of breed technologies we are able to deliver short term results that support long term development goals.</p>
							<p>Making a difference by simple but intelligent changes' is important to us. We are highly motivated and enthusiastic, and we feel passinate and proud about what we do! Working in partnership with our customers we help them extend their reach into an ever changing competitive marketplace. We continually seek to add value through transformation initiatives and high growth models.</p><br>
							
							
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-xs-3 text-center">
									<div class="progress progress-u vertical bottom" style="opacity: 1; left: 0px;">
										<div class="progress-bar progress-bar-u" role="progressbar" data-height="94" ></div>
									</div>
									<span>94%</span>
									<h4>BI Technologies</h4>
								</div>
								<div class="col-xs-3 text-center">
									<div class="progress progress-u vertical bottom" style="opacity: 1; left: 0px;">
										<div class="progress-bar progress-bar-light-green" role="progressbar" data-height="92" ></div>
									</div>
									<span>92%</span>
									<h4>Mobility/IOT Technologies</h4>
								</div>
								<div class="col-xs-3 text-center">
									<div class="progress progress-u vertical bottom" style="opacity: 1; left: 0px;">
										<div class="progress-bar progress-bar-aqua" role="progressbar" data-height="95" ></div>
									</div>
									<span>95%</span>
									<h4>Enterprises Integration</h4>
								</div>
								<div class="col-xs-3 text-center">
									<div class="progress progress-u vertical bottom" style="opacity: 1; left: 0px;">
										<div class="progress-bar progress-bar-dark-blue" role="progressbar" data-height="96" ></div>
									</div>
									<span>96%</span>
									<h4>Ecommerce Implementation</h4>
								</div>
							</div>
						</div>
					</div><!--/end row-->
				</div>
				<!--=== End Our Skills ===-->
             <!-- Parallax Counter -->
		<div class="parallax-counter-v4 parallaxBg1" id="facts">
			<div class="container content-sm">
				<div class="row">
					<div class="col-md-3 col-xs-6 md-margin-bottom-50">
						<i class="icon-cup"></i>
						<span class="counter">20</span>
						<h4>Coffee's Drunk</h4>
					</div>
					<div class="col-md-3 col-xs-6 md-margin-bottom-50">
						<i class="icon-clock"></i>
						<span class="counter">24</span>
						<h4>Projects</h4>
					</div>
					<div class="col-md-3 col-xs-6">
						<i class="icon-emoticon-smile"></i>
						<span class="counter">21</span>
						<h4>Happy Clients</h4>
					</div>
					<div class="col-md-3 col-xs-6">
						<i class=" icon-users"></i>
						<span class="counter">78</span>
						<h4>Team Members</h4>
					</div>
				</div><!--/end row-->
			</div><!--/end container-->
		</div>
		<!-- End Parallax Counter -->
				

				<!-- Team Block -->
		<div class="team-v1 content-lg">
			<div class="container">
				<div class="title-v1">
					<h2>Meet Our Team</h2>
					<p>We <strong>meet</strong> and get to know you. You tell us and we listen. <br>
					We build your application to realise your vision and we <strong>deliver</strong> the ready product.</p>
				</div>

				<div class="row team-v6">
                    <div class="col-md-3"></div>
					<div class="col-md-3 col-sm-6 md-margin-bottom-50">
						<img alt="" src="assets/img/team/deepak-mehta.jpg" class="img-responsive">
						<span>Deepak Mehta</span>
						<small>CEO & Founder</small>
						<ul class="list-inline social-icons-v1">
							<li><a href="https://twitter.com/dmehta104" target="_blank"><i class="rounded-x fa fa-twitter"></i></a></li>
							<li><a href="https://www.linkedin.com/in/dmehta104" target="_blank"><i class="rounded-x fa fa-linkedin"></i></a></li>
							<li><a href="https://plus.google.com/103953010660948378209/posts" target="_blank"><i class="rounded-x fa fa-google-plus"></i></a></li>
						</ul>
					</div>
                    <div class="col-md-3 col-sm-6 md-margin-bottom-50">
						<img alt="" src="assets/img/team/subroto-roy.jpg" class="img-responsive">
						<span>Subrata Roy</span>
						<small>Vice President </small>
						<ul class="list-inline social-icons-v1">
							<li><a href="#"><i class="rounded-x fa fa-twitter"></i></a></li>
							<li><a href="https://www.linkedin.com/in/subrata-roy-b6447810" target="_blank"><i class="rounded-x fa fa-linkedin"></i></a></li>
							<li><a href="https://plus.google.com/112865577273186107143/posts" target="_blank"><i class="rounded-x fa fa-google-plus"></i></a></li>
						</ul>
					</div>	
                    <div class="col-md-1 col-sm-6"></div>
				</div>
			</div>
		</div>
		<!-- End Team Block -->
       

					<!-- Testimonials Carousel -->
			<div class="testimonials-bs bg-image-v2 parallaxBg1">
				<div class="container">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<div class="item active">
								<div class="headline-center-v2 headline-center-v2-dark">
									<h2>What Our Clients Say About Us</h2>
									<span class="bordered-icon"><i class="fa fa-th-large"></i></span>
									<p>Today is memorable day for me where my dream comes true.Finally, C Simplify IT team made "Mission Impossible" as possible, thanks for time and listening my words and made my dreams true. I realized the team has proven, " Smart Work Fails Some Times But Hard Work Never Fails " Thanks from my bottom of heart.</p>
									<span class="author">Franklin Rambo, <a href="#">CEO</a></span>
								</div>
							</div>
							<div class="item">
								<div class="headline-center-v2 headline-center-v2-dark">
									<h2>What Our Clients Say About Us</h2>
									<span class="bordered-icon"><i class="fa fa-th-large"></i></span>
									<p>C Simplify IT have done a great job for us. They have good resource pool available of qualified IT staff so I know when I am in need of a reliable and efficient helping hand I can quickly get it. I am also very impressed with their Quality Control system. Because of their fantastic work, I have extended my contract with them, I am looking forward to work with C Simplify IT.</p>
									<span class="author">Arvind Khurana, <a href="#">CEO</a></span>
								</div>
							</div>
							<div class="item">
								<div class="headline-center-v2 headline-center-v2-dark">
									<h2>What Our Clients Say About Us</h2>
									<span class="bordered-icon"><i class="fa fa-th-large"></i></span>
									<p>The products are excellent and the roadmap for future improvements is genuinely exciting.</p>
									<span class="author">Ashtyn Frauk, <a href="#">Director</a></span>
								</div>
							</div>
                            <div class="item">
								<div class="headline-center-v2 headline-center-v2-dark">
									<h2>What Our Clients Say About Us</h2>
									<span class="bordered-icon"><i class="fa fa-th-large"></i></span>
									<p>Interactive data visualization software may be simple, but the technology is far from it.</p>
									<span class="author">Martine Naroe, <a href="#">Founder</a></span>
								</div>
							</div>
						</div>

						<div class="carousel-arrow">
							<a class="right carousel-control-v2" href="#carousel-example-generic" data-slide="next">
								<i class="rounded-x fa fa-angle-left"></i>
							</a>
							<a class="left carousel-control-v2" href="#carousel-example-generic" data-slide="prev">
								<i class="rounded-x fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
			<!-- End Testimonials Carousel -->
<!-- End Services Section -->
			<!-- Owl Clients v1 -->
            
                <div class="col-md-12 clients-section parallaxBg">
                    <div class="container">
                        <div class="title-v1">
                            <h2>Our Clients</h2>
                        </div>
                       <ul class="owl-clients-v2">
                            <li class="item"><a href="http://www.myvestige.com/" target="_blank"><img src="assets/img/clients/vestige-logo.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="https://paytm.com/" target="_blank"><img src="assets/img/clients/paytm.png" alt=""/></a></li>
                            <li class="item"><a href="http://www.apllvascor.com/" target="_blank"><img src="assets/img/clients/apl-logo.png" alt=""/></a></li>
                             <li class="item"><a href="https://bondevalue.com/" target="_blank"><img src="assets/img/clients/bondevalue.png" alt=""/></a></li>
							<li class="item"><a href="http://transorg.com/" target="_blank"><img src="assets/img/clients/transorg.png" alt=""/></a></li>
                            <li class="item"><a href="http://www.askmebazaar.com/"><img src="assets/img/clients/askmebazaar.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="https://www.vinculumgroup.com/"><img src="assets/img/clients/vinculum.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="http://www.jardines.com/" target="_blank"><img src="assets/img/clients/jardine.jpeg" alt=""/></a></li>
                            <li class="item"><a href="http://www.aswatson.com/" target="_blank"><img src="assets/img/clients/aswatson.png" alt=""/></a></li>
                             <li class="item"><a href="https://www.priorityvendor.com/priorityvendor/handleGeneral.do"><img src="assets/img/clients/priority-vendor.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="http://www.urdoorstep.com/" target="_blank"><img src="assets/img/clients/ur-doorstep.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="http://www.vskills.in/practice/vSkillsHome"><img src="assets/img/clients/vskills.jpg" alt="" target="_blank"/></a></li>                            
                             <li class="item"><a href="http://www.dhl.co.in/en.aspx" target="_blank"><img src="assets/img/clients/dhl.png" alt=""/></a></li>
                            <li class="item"><a href="http://coldex.in/" target="_blank"><img src="assets/img/clients/coldex.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="http://www.motherson.com/motherson-sumi-systems-limited.aspx"><img src="assets/img/clients/motherson.jpg" alt="" target="_blank"/></a></li>
                            
                        </ul>
                    </div>
                  </div>
            

			<!-- End Owl Clients v1 -->
            

					</div><!--/wrapper-->
    </asp:Content>
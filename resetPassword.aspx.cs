﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class resetPassword : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
           
            string token = Request.QueryString["token"].ToString();          
            Session["token"] = Request.QueryString["token"].ToString();         
        }
        catch (Exception exc) { Console.WriteLine(exc.Message); }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            DataModel model = new DataModel();
            string token = null;
            token = Session["token"].ToString();
            dt = model.selectIntoTable("select deliverTo,qid from Queue where ackknowledgementToken='" + token + "'");
            if (dt.Rows.Count != 0)
            {
                model.insertIntoTable("update Users set userPassword='" + EncryptBase64.EncodeTo64(txtpassword.Text) + "' where uid=" + dt.Rows[0][0]);
                model.deleteFromTable("delete from Queue where qid="+dt.Rows[0][1].ToString());
                HttpContext.Current.Response.Redirect("http://csimplyfyit.appspot.com");
            }
            else {
               
                message.Text = "Invallid attempt";
                message.Visible = true;
            }
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
            message.Text = exc.Message;
            message.Visible = true;
        }

    }
}
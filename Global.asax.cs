﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Configuration;
using Stripe;
using System.Web.Routing;

namespace csimplifyit.Application_Start
{
    public class Global : System.Web.HttpApplication
    {
        public string stripePublishableKey = WebConfigurationManager.AppSettings["StripePublishableKey"];
        protected void Application_Start(object sender, EventArgs e)
        {
            var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
            StripeConfiguration.ApiKey = secretKey;

            RouteTable.Routes.Add(
    "chargesRoute",
    new Route("test1/{test1}",
    new PageRouteHandler("~/Charge/Charge.aspx")));

            RouteTable.Routes.Add(
   "UserLogOut",
   new Route("LogOut/{LogOut}",
   new PageRouteHandler("~/Signin/Signin.aspx")));


            // RegisterRoutes(RouteTable.Routes);

        }


        static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("test1", "Charges", "~/Charge/Charge.aspx");

            routes.MapPageRoute("LogOut", "LogOut", "~/Signin/Signin.aspx");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
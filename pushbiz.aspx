<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"%>
<asp:Content ID="pushBizHead" ContentPlaceHolderID="HeadContent" runat="server">
     <title>Pushbiz | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="pushBizMaincontent" ContentPlaceHolderID="MainContent" runat="server">
   

	<div class="wrapper">
		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">PushBiz - Be Visible</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.aspx">Home</a></li>
                    <li><a href="index.aspx">Products</a></li>
					<li><a href="#">PushBiz - Be Visible</a></li>
					
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				<!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
						<div class="carousel-inner">
							<div class="item active">
								<img alt="" src="assets/img/main/img4.jpg">
								<div class="carousel-caption">
									<p>PushBiz - Make your Business 'Visible' to your clients</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/pushbiz-2.png">
								<div class="carousel-caption">
									<p>PushBIZ helps you engage your customers in real time and with cutting edge technology</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/pushbiz-3.png">
								<div class="carousel-caption">
									<p>In 5 minutes, you can begin pushing your business messages to your clients. It is magical!</p>
								</div>
							</div>
                           
						</div>

						<div class="carousel-arrow">
							<a data-slide="prev" href="#myCarousel" class="left carousel-control">
								<i class="fa fa-angle-left"></i>
							</a>
							<a data-slide="next" href="#myCarousel" class="right carousel-control">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
				<!-- End Carousel -->

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>PushBiz - Be visible</h2>
					<p style="font-size:13px;"><strong>Earn more - "Be Visible" - </strong> Make your clients fall in love with your magical offering without sending emails/sms/physical mails.Just be visible to your clients when you wish to. Increase traffic to your website upto 10X.</p>
                    <p style="font-size:13px;"><strong>Share Goodies - </strong> Share your special promotions, breaking news, product updates with your customers in real time.Excite them on special business occasions to visit you. Share enjoyable and actionable stories.</p>
					<p style="font-size:13px;"><strong>Be Fast and Furious</strong> - Watch the traffic roll in - fast and furious. It creates "loyal" customers in real time! and will help you beat your competitors hands down.</p>
                    
					
					<a href="https://www.pushbiz.in/" class="btn-u btn-u-large">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
			</div><!--/row-->
            <!-- Icon Boxes 41 -->
		<div class="container  our-solution">
			<div class="row margin-bottom-40">
                <div class="title-v1">
                    <h2>PushBiz Feature</h2>
                    
                </div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-sea service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light   icon-magic-wand  rounded-x "></i>
						
						<p style="font-size:13px;">In 5 minutes, you can begin pushing your business messages to your clients. It is magical!</p>
                        <a href="http://www.pushbiz.in/" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Try It</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-red service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light icon-credit-card icon-line "></i>
						
						<p style="font-size:13px;"> PushBIZ.IN helps you engage your customers in real time and with cutting edge technology.</p>
                        <a href="http://www.pushbiz.in/" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Try It</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-blue service-or">
						<div class="service-bg"></div>
						<i class="icon-custom icon-color-light rounded-x icon-line  icon-link "></i>
						
						<p style="font-size:13px;">Integration of your website and pushBIZ takes 10 minutes. For Wordpress Sites, PushBIZ.IN plugin is also available.</p>
                        <a href="http://www.pushbiz.in/" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Try It</button></a>
					</div>
				</div>
			</div>

			<div class="row ">
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-grey">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-envelope-letter "></i>
						
						<p style="font-size:13px;">PushBIZ.IN provides out of the box integration with 500+ business channels using zapier platform.</p>
                        <a href="http://www.pushbiz.in/" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Try It</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="service-block service-block-yellow">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-bar-chart  "></i>
						
						<p style="font-size:13px;">Google Analytics or pushBIZ.IN Analytics can show you impact of using pushBIZ.IN in real time.</p>
                        <a href="http://www.pushbiz.in/" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Try It</button></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-12">
					<div class="service-block service-block-dark-blue">
						<i class="icon-custom icon-color-light rounded-x icon-line color-light  icon-rocket"></i>
						
						<p style="font-size:13px;">PushBIZ.IN provides your IT team with API. Using API your own team can push the messages to clients on certain events. </p>
                        <a href="http://www.pushbiz.in/" class=" md-pt-20"><button type="button" class="btn-u btn-u-aqua">Try It</button></a>
					</div>
				</div>
			</div>

			
		</div>
		<!-- End Icon Boxes 41 -->

			<div class="tag-box tag-box-v2">
				<p style="font-size:13px;">PushBIZ helps you engage your customers in real time and with cutting edge technology. All actions taken by customer are based on messages targeted by you to your customers. All clicks point to your own website. You can expect 10X increase in traffic and increased customer loyalty.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
    </div><!--/wrapper-->

</asp:Content>





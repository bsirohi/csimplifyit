<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>

<asp:Content ID="MediaHeader" ContentPlaceHolderID="HeadContent" runat="server">
   <title>Media Publish | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="MediaContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wrapper">

        <!--=== Header ===-->

        <!--=== End Header ===-->


        <!--=== Breadcrumbs ===-->
        <div class="breadcrumbs">
            <div class="container">
                <h1 class="pull-left">Media and Publishing</h1>
                <ul class="pull-right breadcrumb">
                    <li><a href="index.aspx">Home</a></li>
                    <li><a href="">Verticals</a></li>
                    <li class="active">Media and Publishing</li>
                </ul>
            </div>
        </div>
        <!--/breadcrumbs-->
        <!--=== End Breadcrumbs ===-->

        <!--=== Content Part ===-->
        <div class="container content">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="title-v2">Media and Publishing</h2>
                    <p>
                        Process Re-engineering using BPM
                        
                       Publishing processes from production and editorial, to content management, rights management and ERP integration.

                     Assets management for dynamic content and digital rights management. 

Digital Media content lifecycle and distribution

Social CRM manage information with the ever-growing social media channel by implementing new approaches to data analytics and CRM

Use Tools to manage Media and Publishing Marklogic, Adobe, Bonitasoft BPM, Push BIZ IN
                    </p>
                    <br>
                </div>
                <div class="col-md-6 ">
                    <img class="wow fadeInLeft img-responsive pull-right md-pt-40" src="assets/img/banners/DedidatedTeam.png" alt="">
                </div>
            </div>
        </div>
        <!--/container-->
        <!--=== End Content Part ===-->


        <!--=== Footer v2 ===-->


    </div>
    <!--/wrapper-->

</asp:Content>

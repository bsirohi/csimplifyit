﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for Queue
/// </summary>

public class Queue
{
    DataModel model = new DataModel();
    EmailSend sendEmail = new EmailSend();
    DataTable dt = new DataTable();
    public int queueType { get; set; }
    public int deliverTo { get; set; }
    public int deliveryRequestedBy { get; set; }
    public int deliverUsingTemplate { get; set; }
    public string dataToMerge { get; set; }
    public int deliveryStatus { get; set; }
    public int deliveryPriority { get; set; }
    public string queueComments
    { get; set; }
    public Queue()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int queueJobNotification(int jobID)
    {
        int postedBy = 0;
       dt= model.selectIntoTable("select postedBy from job where jid=" + jobID);
        if (dt.Rows.Count != 0) {
            postedBy =Convert.ToInt32( dt.Rows[0][0]);
         dt=   model.selectIntoTable("SELECT uid " +
                                    "FROM Users A, RefereredUsers B WHERE A.uid = B.refUserId AND B.regUserId ="+postedBy +"AND userType=1");
         if (dt.Rows.Count != 0) {
             for (int columnindex = 0; columnindex < dt.Columns.Count; columnindex++)
             {
                 int deliverTo=Convert.ToInt32(dt.Rows[0][columnindex]);
                dt= model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,deliveryRequestedBy," +
                             "deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn," +
                               "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                              ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                              "(2," + deliverTo + ","+postedBy+",1,null,0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')");
                if (dt.Rows.Count != 0) { 
                processTo(Convert.ToInt32( dt.Rows[0][0]));
                }
             }
         }
        }
        return 0;

    }
    private int queueFriendInvitation(int registerUid) {
        return 0;
    }
    private void queueJobExpirationNotifications() { }
    private void processTo(int refID)
    {
        dt = model.selectIntoTable("select queueType,deliverTo,deliveryRequestedBy," +
                                  "deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn," +
                                    "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                                   ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression from Queue where qid=" + refID);
        if (dt.Rows.Count != 0) {
            intializePropertiesFromTable(dt);
        }
        if (queueType == 2) {
            if (deliveryRequestedBy == null)
            {
                dt = model.selectIntoTable("select userName from Users where uid =" + deliverTo);
                sendEmail.sendMail(dt.Rows[0][0].ToString(), "Your Link For Reset Password is :http:/developer.csimplifyit.com/Default.aspx?linkshow$" + dataToMerge);

            }
            else { 
            
            }
        
        }

    }
    public void insertImp()
    {
        try
        {
            model.insert("insert into Queue (queueType,deliverTo,deliveryRequestedBy," +
                          "deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn," +
                            "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                           ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                           "(2,1,2,1,'hello',0,now(),now(),1,'1', DATE_ADD(now(),INTERVAL 30 DAY),DATE_ADD(now()," +
                           "INTERVAL 30 DAY),'null','null')");
        }
        catch (Exception exc) { }
    }
    public void queueRegistrationMail(int userID,string encodedToken) {
                dt = model.insertIntoTableWithLastId("insert into Queue (queueType,deliverTo,deliveryRequestedBy," +
                             "deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn," +
                               "requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter" +
                              ",deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values" +
                              "(2," + userID + ","+null+",1,'" + encodedToken + "',0,now(),now(),0,'1', DATE_ADD(now(),INTERVAL 5 DAY),now(),'null','null')");
                if (dt.Rows.Count != 0)
                {
                    processTo(Convert.ToInt32(dt.Rows[0][0]));
                }
    }
    private void intializePropertiesFromTable(DataTable dt)
    {
        try{
            for (int columnindex = 0; columnindex < dt.Columns.Count; columnindex++)
            {
                if (dt.Columns[columnindex].Caption.Equals("queueType"))
                {
                    queueType = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliverTo"))
                {
                    deliverTo = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliveryRequestedBy"))
                {
                    deliveryRequestedBy = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("delivedeliverUsingTemplaterTo"))
                {
                    deliverUsingTemplate = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliverTo"))
                {
                    deliverTo = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("dataToMerge"))
                {
                    dataToMerge = (dt.Rows[0][columnindex]).ToString();
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliveryStatus"))
                {
                    deliveryStatus = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("deliveryPriority"))
                {
                    deliveryPriority = Convert.ToInt32(dt.Rows[0][columnindex]);
                }
                else if (dt.Columns[columnindex].Caption.Equals("queueComments"))
                {
                    queueComments = dt.Rows[0][columnindex].ToString();

                }
            }
           
        }
        catch (Exception exc)
        {
            throw exc;
        }
            
    }
        
    
}

//insert into Queue (queueType,deliverTo,deliveryRequestedBy,
//deliverUsingTemplate,dataToMerge,deliveryStatus,deliveryStatusUpdatedOn,
//requestQueuedOn,deliveryPriority,removeFromQueueAfterProcessing,expireMessageAfter
//,deliverOnlyAfterDateTime,queueComments,queueFilterExpression)values
//(2,1,2,1,'hello',0,now(),now(),1,'1', DATE_ADD(now(),INTERVAL 30 DAY),DATE_ADD(now(),
//INTERVAL 30 DAY),'null','null')

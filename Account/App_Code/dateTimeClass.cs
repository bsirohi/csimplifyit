﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

/// <summary>
/// Summary description for dateTimeClass
/// </summary>
public class dateTimeClass
{
	public dateTimeClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static string datetime() {
        var isoDateTimeFormat = CultureInfo.InvariantCulture.DateTimeFormat;
        string datetime = DateTime.Now.ToString(isoDateTimeFormat.SortableDateTimePattern);
        return datetime;
    }
}
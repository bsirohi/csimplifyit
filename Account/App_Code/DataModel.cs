﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for DataModel
/// </summary>
public class DataModel
{
    //CSimplifyitException exception;
    string connectionstring = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
    public string insertIntoTable(string query) {
         MySqlConnection con = new MySqlConnection(connectionstring);
         try
         {
             con.Open();
             MySqlCommand command = new MySqlCommand(query, con);
             command.ExecuteNonQuery();
             con.Close();
             return "true";
         }
              
         catch(Exception exc) {
             return exc.Message;
         }
         finally { con.Close(); }
    }
    public Boolean updateIntoTable(string query) {
        MySqlConnection con = new MySqlConnection(connectionstring);
        try
        {
          
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            int noOfRecords= command.ExecuteNonQuery();
           
            return true;

        }
        catch (Exception exc)
        {

        }
        finally { con.Close(); }
        return false;
    
    }
    public DataTable selectIntoTable(string query) {
        DataTable dt = new DataTable();
        MySqlConnection con = new MySqlConnection(connectionstring);
        MySqlDataReader reader;
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            reader = command.ExecuteReader();
         
            dt.Load(reader);
            
        }
        catch (Exception exc) {
           throw exc;
        }
        finally { con.Close(); }
        return dt ;
    }
    public int updateIntoTablewithRowCount(string query)
    {
        int noOfRecords = 0;
        MySqlConnection con = new MySqlConnection(connectionstring);
        try
        {
           
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            noOfRecords = command.ExecuteNonQuery();

            return noOfRecords;

        }
             
        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }

    }
    public string insert(string query)
    {
        MySqlConnection con = new MySqlConnection(connectionstring);

        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            con.Close();
            return "true";
        }

        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
    }
    public int insertwithRowCount(string query) {
        MySqlConnection con = new MySqlConnection(connectionstring);
        int rowCount = 0;
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
           rowCount= command.ExecuteNonQuery();
            con.Close();
            return rowCount;
        }

        catch (Exception exc)
        {
            throw exc;
        }
        finally { con.Close(); }
    }
    public DataSet selectQueryInDataSet(string query) {
        DataSet ds = new DataSet();
        
        MySqlDataAdapter adp = new MySqlDataAdapter(query, connectionstring);
        try
        {
            adp.Fill(ds); 
        }
        catch (Exception exc)
        {
            throw exc;
        }
        return ds;
    }
    public DataTable insertIntoTableWithLastId(string query) {
        DataTable dt=new DataTable();
        MySqlDataReader reader;
        MySqlConnection con = new MySqlConnection(connectionstring);
        try
        {
            con.Open();
            MySqlCommand command = new MySqlCommand(query, con);
            command.ExecuteNonQuery();
            command = new MySqlCommand("SELECT LAST_INSERT_ID()", con);
            reader = command.ExecuteReader();
            dt.Load(reader);
            return dt;
        }

        catch (Exception exc)
        {
            throw exc; 
        }
        finally { con.Close(); }
    
    }
}
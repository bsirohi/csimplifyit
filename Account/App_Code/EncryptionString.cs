﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for EncryptionString
/// </summary>
public  static class EncryptBase64
{
    public static string EncodeTo64(string toEncode)
    {

        byte[] toEncodeAsBytes

             = System.Text.Encoding.Unicode.GetBytes(toEncode);

        string returnValue
              = System.Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;

    }
    public static string DecodeFrom64(string encodedData)
    {

        byte[] encodedDataAsBytes

            = System.Convert.FromBase64String(encodedData);

        string returnValue =

           System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);

        return returnValue;

    }
}
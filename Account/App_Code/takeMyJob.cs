﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using System.Web.Script.Serialization;
using System.ServiceModel.Web;
using System.Globalization;
using System.Web;
using System.Net;
using System.Runtime.Serialization.Json;
// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "takeMyJob" in code, svc and config file together.
public class takeMyJob : ItakeMyJob
{
    DataModel model = new DataModel();
    ResponseData response;
    Rewards rewards = new Rewards();
    DataTable table = new DataTable();
    oAuthLinkedIn linkedin = new oAuthLinkedIn();
    public Stream getToken(string inputData)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {

                }
                if (objelement.Key.Equals("data"))
                {

                    list = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }

            string username = null;
            string password = null;
            string authSite = null;
            foreach (var keyvalue in list)
            {
                if (keyvalue.Key.Equals("userName"))
                {
                    username = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("userPassword"))
                {
                    password = keyvalue.Value;
                }
                if (keyvalue.Key.Equals("authSite"))
                {
                    authSite = keyvalue.Value;
                }
            }
            string result = getPassword(username, password, authSite);
            string[] splitValuesFromGetgetPass = result.Split('&');
            if (splitValuesFromGetgetPass.Length == 1)
            {
                model.insertIntoTable("insert into Users ( userName,displayName,userType,userStatus,userPassword,modifiedBy,"
                 + "currentCompanyName,currentJobTitle,modifiedDateTime ) values" +
                 " ('" + username + "','" + null + "','" + "0'," + "'0','"
                 + dateTimeClass.datetime() + "','" + null + "','"
                 + null + "','" + null + "', now() )");
                dt = model.selectIntoTable("select uid userPassword from Users where userName='" + username + "'");
                if (dt.Rows.Count != 0)
                {
                    int uid = Convert.ToInt32(dt.Rows[0][0]);
                    byte[] array = Encoding.ASCII.GetBytes(uid.ToString() + dateTimeClass.datetime());
                    string encoded = System.Convert.ToBase64String(array);
                    model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,authSite) value (" + uid + ",'" + encoded + "','system')");
                    EmailSend sendEmail = new EmailSend();
                    sendEmail.sendMail(username, " :http:/developer.csimplifyit.com/Default.aspx?linkshow$" + encoded);
                }
                response = new ResponseData(1, Constant.notvallid, "[{\"token\":\"\" ,\"uid\":\"\" }]");
            }
            else if (splitValuesFromGetgetPass[0].Equals("wrongpwd"))
            {
                response = new ResponseData(1, Constant.notvallid, "[{\"token\": \"\",\"uid\":\"" + splitValuesFromGetgetPass[1] + "\" }]");
            }
            else
            {
                response = new ResponseData(1, Constant.vallid, "[{\"token\":\"" + splitValuesFromGetgetPass[1] + "\",\"uid\":\"" + splitValuesFromGetgetPass[2] + "\" }]");
            }
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            returnString = response.returnStatment();
            return returnStmnt(returnString);
        }
    }
    private string calculateMD5Hash(string input)
    {
        // step 1, calculate MD5 hash from input
        MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
        byte[] hash = md5.ComputeHash(inputBytes);

        // step 2, convert byte array to hex string
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }

        return sb.ToString();
    }
    public Stream getRefrenceFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        int regid = 0;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);

                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("registeruser"))
                        {
                            regid = Convert.ToInt32(list.Value);
                            dt = model.selectIntoTable("SELECT distinct(uid),count(toID) as pendingRequests,displayName,userName,userContactNo," +
                                "refUserPrivacyConcern as isBlocked ,userType,userStatus,currentCompanyName,currentJobTitle,currentIndustry FROM " +
                              " Users A,RefereredUsers B  Left JOIN   FriendRequests As F ON F.RequestStatus=0" +
                                " and F.introducedBy=B.refUserId and F.toID=" + regid + " WHERE A.uid = B.refUserId AND B.regUserId =" + regid + "  group by uid order by displayName");
                            string result = convertDataTableToJson(dt);
                            if (result != null)
                            {
                                response = new ResponseData(1, Constant.foundData, result);
                                returnString = response.returnStatment();
                                return returnStmnt(returnString);
                            }
                            else
                            {
                                response = new ResponseData(1, Constant.notFound, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }

            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream blockUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        // string returnString = null;
        string requestingUserid = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string blockUserId = null;
                    string blockRequestType = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("requestingUserid"))
                        {
                            requestingUserid = list.Value;
                        }
                        if (list.Key.Equals("blockUserId"))
                        {
                            blockUserId = list.Value;
                        }
                        if (list.Key.Equals("blockRequestType"))
                        {
                            blockRequestType = list.Value;
                        }
                    }
                    if (Convert.ToInt32(blockRequestType) <= 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " regUserPrivacyConcern='" + blockRequestType + "' , regUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                        " refUserPrivacyConcern='" + blockRequestType + "' , refUserPrivacyConcernDateTime = now() where refUserId ='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            //dt=model.selectIntoTable("select "
                            response = new ResponseData(1, Constant.blockUser, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else if (Convert.ToInt32(blockRequestType) > 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " refUserPrivacyConcern='" + blockRequestType + "', refUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                       " regUserPrivacyConcern='" + blockRequestType + "', regUserPrivacyConcernDateTime = now() where refUserId='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            //  auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "specify user is blocked", null, null);
                            response = new ResponseData(1, Constant.blockUser, "[{\"blockUserId\":\"" + blockUserId + "\"}]");
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        //  auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "user data columns is not correct", null, null);
                        response = new ResponseData(0, Constant.checkDataColumns, null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            // auditInfo("takemyjob", "blockuser", "want to block friends",Convert.ToInt32( requestingUserid), "user not exists", null, null);

            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream unBlockUser(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string requestingUserid = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string blockUserId = null;
                    string blockRequestType = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("requestingUserid"))
                        {
                            requestingUserid = list.Value;
                        }
                        if (list.Key.Equals("blockUserId"))
                        {
                            blockUserId = list.Value;
                        }
                        if (list.Key.Equals("blockRequestType"))
                        {
                            blockRequestType = list.Value;
                        }
                    }
                    if (Convert.ToInt32(blockRequestType) <= 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                           " regUserPrivacyConcern='" + 0 + "' , regUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                          " refUserPrivacyConcern='" + 0 + "' , refUserPrivacyConcernDateTime = now() where refUserId ='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            //  auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "specify user is blocked", null, null);
                            response = new ResponseData(1, Constant.unblockUser, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else if (Convert.ToInt32(blockRequestType) > 100 && requestingUserid != null && blockUserId != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update RefereredUsers set" +
                         " refUserPrivacyConcern='" + 0 + "', refUserPrivacyConcernDateTime = now() where regUserId='" + requestingUserid + "'and refUserId='" + blockUserId + "'");
                        model.updateIntoTablewithRowCount("update RefereredUsers set" +
                       " regUserPrivacyConcern='" + 0 + "', regUserPrivacyConcernDateTime = now() where refUserId='" + requestingUserid + "'and regUserId='" + blockUserId + "'");
                        if (noOfRowAffecting >= 1)
                        {
                            // auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "specify user is blocked", null, null);
                            response = new ResponseData(1, Constant.unblockUser, "[{\"unblockUserId\":\"" + blockUserId + "\"}]");
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        //  auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "user data columns is not correct", null, null);
                        response = new ResponseData(0, Constant.checkDataColumns, null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            // auditInfo("takemyjob", "unblockuser", "want to un block friends", Convert.ToInt32(requestingUserid), "user not exists", null, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            //   auditInfo("takemyjob", "unblockuser", "want to unblock friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private List<KeyValuePair<string, string>> jsonToKeyValuePair(string json)
    {
        var list = new List<KeyValuePair<string, string>>();
        // KeyValuePair<string, string> jsonkeyvalue=new KeyValuePair<string,string>(null,null);
        try
        {
            JObject root = JObject.Parse(json);
            JArray items = (JArray)root["data"];
            foreach (var data in items)
            {
                JObject localdata = JObject.Parse(data.ToString());
                foreach (var jsonobject in localdata)
                {
                    {
                        list.Add(new KeyValuePair<string, string>(jsonobject.Key.ToString(), jsonobject.Value.ToString()));
                    }
                }

            }
            return list;
        }
        catch (Exception exc)
        {
            throw exc;
        }
        throw new NotImplementedException();
    }
    public Stream postNewJob(string inputData)
    {
        string returnString = "";
        DataTable dt = new DataTable();
        try
        {
            string jobIDInJson = null;

            Boolean flag = false;
            JObject jsonrow = JObject.Parse(inputData);
            List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
            foreach (var rowobj in jsonrow)
            {

                if (rowobj.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(rowobj.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(returnString);
                            }
                        }
                    }
                }
                if (rowobj.Key.Equals("data") && flag == true)
                {
                    jobIDInJson = insertFromKeyValuePostJob("Job", "{\"data\":" + rowobj.Value.ToString() + "}");
                    //  string response = model.insertIntoTable(query);

                    if (jobIDInJson != null)
                    {
                        response = new ResponseData(1, "Successfully posted", jobIDInJson);
                        return returnStmnt(response.returnStatment());
                    }
                }

            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            response = new ResponseData(0, exc.Message, null);
            //returnString = "{\"status\":0,\"Description\":\""+exc.Message+"\",\"results\":[]}";
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream applyForJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        string appliedByUser = null;
        string returnString = "";
        try
        {
            DataTable dt = new DataTable();
            Boolean flag = false;
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data"))
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            if (flag == true)
            {
                string jobRefid = null;
                foreach (var list in jsonkeyvalue)
                {
                    if (list.Key.Equals("appliedByUser"))
                    {
                        appliedByUser = list.Value.ToString();
                    }
                    if (list.Key.Equals("jobRefid"))
                    {
                        jobRefid = list.Value.ToString();
                    }
                }
                if (appliedByUser != null && jobRefid != null)
                {
                    dt = model.selectIntoTable("SELECT Jaid FROM JobApplications where jobRefid=" + jobRefid + " and appliedByUser=" + appliedByUser);
                    if (dt.Rows.Count == 0)
                    {
                        foreach (var objelement in jsonrow)
                        {
                            if (objelement.Key.Equals("data"))
                            {
                                dt = model.insertIntoTableWithLastId("insert into JobApplications (" +
                                    "appliedOnDateTime,applicationStatus,jobRefid,appliedByUser) values (now(),1," + jobRefid + "," + appliedByUser + ")");
                                if (dt.Rows.Count != 0)
                                {
                                    response = new ResponseData(1, Constant.applicationPostSuccessfully, "[{\"jobID\":\"" + jobRefid + "\"}]");
                                    return returnStmnt(response.returnStatment());
                                }
                            }
                        }
                    }
                    else
                    {
                        response = new ResponseData(1, Constant.alreadyapplyjob, "[{\"jobID\":\"" + jobRefid + "\"}]");
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            response = new ResponseData(1, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            returnString = "{\"status\":0,\"Description\":\"" + exc.Message + "\" ,\"results\":[null]}";
            return returnStmnt(returnString);
        }

    }
    public string convertDataTableToJson(object value)
    {
        Type type = value.GetType();

        Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

        json.NullValueHandling = NullValueHandling.Ignore;

        json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
        json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
        json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        if (type == typeof(DataTable))
            json.Converters.Add(new DataTableConverter());
        else if (type == typeof(DataSet))
            json.Converters.Add(new DataSetConverter());

        StringWriter sw = new StringWriter();
        Newtonsoft.Json.JsonTextWriter writer = new JsonTextWriter(sw);
        writer.Formatting = Formatting.None;
        writer.QuoteChar = '"';
        json.Serialize(writer, value);
        string output = sw.ToString();
        writer.Close();
        sw.Close();

        return output;
    }
    //private string convertDataTableToJson(DataTable dt)
    //{
    //    try
    //    {
    //        StringBuilder Sb = new StringBuilder();

    //        string[] StrDc = new string[dt.Columns.Count];
    //        string HeadStr = string.Empty;
    //        for (int i = 0; i < dt.Columns.Count; i++)
    //        {
    //            StrDc[i] = dt.Columns[i].Caption;
    //            HeadStr += "\"" + StrDc[i] + "\" : \"" + StrDc[i] + i.ToString() + "¾" + "\",";
    //        }
    //        HeadStr = HeadStr.Substring(0, HeadStr.Length - 1);
    //        Sb.Append("[");
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            string TempStr = HeadStr;
    //            Sb.Append("{");
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {
    //             string data = (dt.Rows[i][j].ToString().Replace("\n", " ")).Replace('"',' ');
    //             if (vallidateJson(data).Equals(true))
    //             {
    //                 TempStr = TempStr.Replace(dt.Columns[j] + j.ToString() + "¾", data);
    //             }
    //             else {
    //                 TempStr = TempStr.Replace(dt.Columns[j] + j.ToString() + "¾", "");
    //             }
    //            }
    //            Sb.Append(TempStr + "},");
    //        }
    //        Sb = new StringBuilder(Sb.ToString().Substring(0, Sb.ToString().Length - 1));
    //        if (dt.Rows[0][0] != null)
    //            Sb.Append("]");

    //        return Sb.ToString();
    //    }
    //    catch (Exception exc)
    //    {
    //        throw exc;
    //    }
    //}
    //private string convertDataTableToJson(DataSet ds) { 

    //}
    private string getPassword(string username, string password, string authSite)
    {
        string uid = null;
        try
        {
            DataTable dt;
            dt = model.selectIntoTable("select userPassword,uid from Users where userName='" + username + "' and userType=1");
            if (dt.Rows.Count > 0)
            {
                uid = dt.Rows[0][1].ToString();
                if (dt.Rows[0][0].ToString().Equals(EncryptBase64.EncodeTo64(password)))
                {
                    string result = dt.Columns[0].ToString() + GenerateNonce();
                    byte[] array = Encoding.ASCII.GetBytes(result + GenerateNonce());
                    string encoded = System.Convert.ToBase64String(array);
                    dt = model.selectIntoTable("select * from AuthenticationTokens where regUserid=" + uid + " and authSite='" + authSite + "'");
                    if (dt.Rows.Count != 0)
                    {
                        model.updateIntoTable("update AuthenticationTokens set token='" + encoded + "' where regUserid=" + uid + " and authSite='" + authSite + "'");
                        return "vallid&" + encoded + "&" + uid;
                    }
                    else
                    {
                        model.insertIntoTable("insert into AuthenticationTokens (token,regUserid,authSite) values ('" + encoded + "'," + uid + ",'" + authSite + "')");
                        return "vallid&" + encoded + "&" + uid;
                    }
                }
                return "wrongpwd&" + uid;
            }
            else
            {
                dt = model.selectIntoTable("select userPassword,uid from Users where userName='" + username + "'");
                if (dt.Rows.Count == 0)
                {
                    return "saveuser";
                }
                else
                {
                    return "wrongpwd&" + uid;
                }
            }
        }
        catch (Exception exc)
        {

            throw exc;
        }
    }
    private string insertFromKeyValuePostJob(string tableName, string jsonarr)
    {
        DataTable dt = new DataTable();
        StringBuilder insertStmt = new StringBuilder();
        string jobTitle = null;
        string postedBy = null;
        insertStmt.Append("insert into " + tableName + "(jobStatus,jobType,postedOnDateTime,jobExpiresOn,joblastModifiedOn,");
        StringBuilder values = new StringBuilder();
        values.Append("values (1,2,now(),DATE_ADD(now(),INTERVAL 30 DAY),now(),");
        try
        {
            JObject root = JObject.Parse(jsonarr);
            JArray items = (JArray)root["data"];
            int count = 0;
            foreach (var data in items)
            {
                JObject localdata = JObject.Parse(data.ToString());
                foreach (var jsonobject in localdata)
                {
                    if (jsonobject.Key.Equals("jobTitle"))
                    {
                        jobTitle = jsonobject.Value.ToString();
                    }
                    if (jsonobject.Key.Equals("postedBy"))
                    {
                        postedBy = jsonobject.Value.ToString();
                    }
                    if (localdata.Count != count + 1)
                    {
                        count++;
                        insertStmt.Append(jsonobject.Key + ",");
                        values.Append("'" + jsonobject.Value.ToString() + "',");
                    }
                    else
                    {
                        insertStmt.Append(jsonobject.Key + ")");
                        values.Append("'" + jsonobject.Value.ToString() + "')");
                    }
                }
            }
            dt = model.insertIntoTableWithLastId(insertStmt.Append(values.ToString()).ToString());
            if (dt.Rows.Count != 0)
            {
                return convertDataTableToJson(dt);
            }

            return null;
        }
        catch (Exception exc)
        {
            throw exc;
        }
    }
    public virtual string GenerateNonce()
    {
        Random random = new Random();
        // Just a simple implementation of a random number between 123400 and 9999999
        return random.Next(123400, 9999999).ToString();
    }
    public Stream getLov(string inputData)
    {
        /*string jsonWhereClause = null;*/
        string returnString = "";
        try
        {
            DataTable dt = new DataTable();

            List<KeyValuePair<string, string>> jsonkeyvalue;
            jsonkeyvalue = jsonToKeyValuePair(inputData);
            if (jsonkeyvalue[0].Value.Equals(""))
            {
                if (jsonkeyvalue[1].Key.Equals("jsonwhereclause"))
                {
                    dt = model.selectIntoTable("SELECT * FROM  ListofValues " + jsonkeyvalue[1].Value.ToString());
                }
                else
                {
                    returnString = "{\"status\":0,\"Description\":\"input string is not in correct format\",\"results\":[null]}";
                }
                string result = convertDataTableToJson(dt);

                returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[" + result + "]}";
            }
            else
            {
                returnString = "{\"status\":0,\"Description\":\"not a vallid token\",\"results\":[null]}";
            }
        }
        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":\"null\"}";
        }
        return new MemoryStream(Encoding.UTF8.GetBytes(returnString));
    }/* no result found is on description if no data found and status success */
    public Stream getJobs(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int userID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    string result = null;
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("userID"))
                    {
                        userID = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select jid,postedBy, DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle,jobReferenceLink,joblatitude,jobLongitude,jobLocationName,jobType,jobComments,jobSkills," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn , DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,"
                            + "jobVertical,jobRole from Job As J, RefereredUsers As R where  J.postedBy = R.regUserID AND  R.refUserID = '" + userID +
                            "' and jobStatus = 1 and jobExpiresOn > now() and regUserPrivacyConcern = 0 order by postedOnDateTime desc ");
                        if (dt.Rows.Count != 0)
                        {
                            result = convertDataTableToJson(dt);
                        }
                        if (result != null)
                        {
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean vallidateToken(string token)
    {
        table = model.selectIntoTable("select * from AuthenticationTokens where token='" + token + "'");
        if (table.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private List<KeyValuePair<string, string>> metaToKeyValuePair(string meta)
    {
        var list = new List<KeyValuePair<string, string>>();
        // KeyValuePair<string, string> jsonkeyvalue=new KeyValuePair<string,string>(null,null);
        try
        {
            JObject jobj = JObject.Parse(meta);

            foreach (var data in jobj)
            {
                list.Add(new KeyValuePair<string, string>(data.Key.ToString(), data.Value.ToString()));

            }
            return list;
        }
        catch (Exception exc)
        {
            throw exc;
        }


    }
    public Stream knowMyFriends(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        //  string returnString = null;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                            }
                        }
                        if (keypair.Key.Equals("userID"))
                        {

                            userID = keypair.Value.ToString();
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != null)
                {
                    Boolean resposnevalue = insertJsonintoUsers("Users", "{\"data\":" + objelement.Value.ToString() + "}", Convert.ToInt32(userID));
                    if (resposnevalue == true)
                    {
                        response = new ResponseData(1, "Contact has been saved", null);
                        return returnStmnt(response.returnStatment());
                    }
                }

            }
            response = new ResponseData(0, Constant.invallidtoken, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception exc)
        {
            //  returnString = "{\"status\":0,\"Description\":\"" + exc.Message + "\",\"results\":[null]}";
            response = new ResponseData(0, exc.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertJsonintoUsers(string tableName, string jsonarr, int uid)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = model.selectIntoTable("select displayName from Users where uid=" + uid.ToString());
            if (dt.Rows.Count != 0)
            {
                string modifiedBy = dt.Rows[0][0].ToString();
                string authsite = null;
                string userContactNo = null;
                string userName = null;
                string source = null;
                StringBuilder insertStmt = new StringBuilder();
                StringBuilder values = new StringBuilder();

                JObject root = JObject.Parse(jsonarr);
                JArray items = (JArray)root["data"];
                foreach (var data in items)
                {
                    insertStmt.Append("insert into " + tableName + "( userType,userStatus,userPassword,modifiedBy,modifiedDateTime,createdBy,createdDatetime,");
                    values.Append("values ('0','0','" + dateTimeClass.datetime() + "','" + modifiedBy + "',now(),'" + modifiedBy + "',now(),");
                    JObject localdata = JObject.Parse(data.ToString());
                    foreach (var jsonobject in localdata)
                    {
                        if (jsonobject.Key.Equals("displayName"))
                        {
                            insertStmt.Append(jsonobject.Key + ",");
                            values.Append("'" + jsonobject.Value.ToString() + "',");
                        }
                        if (jsonobject.Key.Equals("source"))
                        {
                            authsite = jsonobject.Value.ToString();
                            continue;
                        }
                        if (jsonobject.Key.Equals("userContactNo"))
                        {
                            userContactNo = jsonobject.Value.ToString();
                            insertStmt.Append(jsonobject.Key + ",");
                            values.Append("'" + jsonobject.Value.ToString() + "',"); ;
                        }
                        if (jsonobject.Key.Equals("userName"))
                        {
                            userName = jsonobject.Value.ToString();
                            insertStmt.Append(jsonobject.Key + ")");
                            values.Append("'" + jsonobject.Value.ToString() + "')"); ;
                        }

                    }
                    string query = insertStmt.Append(values.ToString()).ToString();
                    insertStmt.Clear();
                    values.Clear();
                    try
                    {
                        dt = model.selectIntoTable("select distinct(uid) from Users where userContactNo='" + userContactNo + "' OR userName='" + userName + "'");
                        if (dt.Rows.Count == 0)
                        {
                            dt = model.insertIntoTableWithLastId(query);

                            if (dt.Rows.Count != 0)
                            {
                                string refuid = dt.Rows[0][0].ToString();
                                model.insertIntoTable("insert into RefereredUsers (regUserId,refUserId,createdDatetime) values ('" + uid + "','" + refuid + "',now())");
                                model.insertIntoTable("insert into AuthenticationTokens (regUserId,lastModifiedDate,createdDateTime,authSite)values('" + refuid + "',now(),now(),'" + source + "')");
                            }
                        }
                    }

                    catch (Exception exc) { Console.WriteLine(exc.Message); }
                }

            }
            else { return false; }
            return true;
        }
        catch (Exception exc)
        {
            throw exc;
        }

    }
    public string testfunc()
    {

        //  callTwilioFormessage("hello");
        return "data";
    }
    private Stream returnStmnt(string returnstmnt)
    {
        return new MemoryStream(Encoding.UTF8.GetBytes(returnstmnt));
    }
    private string convertTableIntoJob(DataTable dt, int registerUid)
    {
        string jobRole = null;
        string jobTitle = null;
        StringBuilder sb = new StringBuilder();
        StringBuilder sbforkey = new StringBuilder();
        try
        {
            int count = dt.Rows.Count;
            sb.Append("insert into Job (postedBy,postedOnDateTime,jobReferenceLink,jobLatitude,jobLongitude" +
                ",jobLocationName,jobType,jobComments,jobExpiresOn," +
                "joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,jobVertical,"
                + "JobTitle,jobRole,jobSkills ) values ('" + registerUid.ToString() +
                "',now(),'null','null','null','null',1,'null',DATE_ADD(now(),INTERVAL 30 DAY),now(),'1','1','2','1','");
            if (count != 0)
            {
                jobRole = dt.Rows[0][0].ToString();
                jobTitle = dt.Rows[0][1].ToString();
                for (int i = 0; i < count; i++)
                {
                    sbforkey.Append(dt.Rows[i][3].ToString() + ", ");
                }
            }
            sb.Append(jobTitle + "','" + jobRole + "','" + sbforkey.ToString() + "')");
            dt = model.selectIntoTable("select jid from Job where postedBy='" + registerUid.ToString()
                + "' and jobType=1 and now() < jobExpiresOn;");
            if (dt.Rows.Count == 0)
            {
                dt = model.insertIntoTableWithLastId(sb.ToString());
                if (dt.Rows.Count != 0)
                {
                    //  dt = model.selectIntoTable("select "
                    return "1&" + dt.Rows[0][0].ToString();
                }
            }
            else
            {
                //  dt = model.selectIntoTable("select jid, max(jobExpiresOn) from Job where postedBy='" + registerUid.ToString()
                //+ "' and jobType=1 ");
                return "-1&" + dt.Rows[0][0].ToString();

            }

        }

        catch (Exception exc)
        {
            throw exc;
        }

        return sb.ToString();
    }
    public Stream takemyJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {
                        if (list.Key.Equals("registerUid"))
                        {
                            dt = model.selectIntoTable("Select  u.currentJobTitle,u.currentIndustry,u.currentCompanyName,luvValueDisplayName " +
                             "from  mehtadb1.Users u, mehtadb1.UserSkills s, mehtadb1.ListofValues  l " +
                               "where u.uid = " + list.Value.ToString() +
                                " and u.uid = s.uid" +
                                " and s.skillRef = l.luvid" +
                                 " Order by l.luvValueDisplayName;");
                            if (dt.Rows.Count == 0)
                            {
                                response = new ResponseData(2, "please login with linkedin to generate your skills", null);
                                return (returnStmnt(response.returnStatment()));
                            }
                            string[] resultstring = convertTableIntoJob(dt, Convert.ToInt32(list.Value)).Split('&');
                            if (resultstring[0].Equals("1"))
                            {
                                response = new ResponseData(1, "Job successfully Posted", "[{\"jobID\" :\"" + resultstring[1] + "\"}]");
                                return returnStmnt(response.returnStatment());
                            }
                            else
                            {
                                response = new ResponseData(1, Constant.jobalreadyposted, "[{\"jobID\" :\"" + resultstring[1] + "\"}]");
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }
    }
    public Stream optOut(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string requestingUserid = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("requestingUserid"))
                        {
                            requestingUserid = list.Value;
                        }

                    }
                    if (requestingUserid != null)
                    {
                        int noOfRowAffecting = model.updateIntoTablewithRowCount("update Users set" +
                          " userType='2' , modifiedDateTime = now() where uid='" + requestingUserid + "'");
                        int rowaffectingjobtable = model.updateIntoTablewithRowCount("update Job set" +
                          " jobStatus='3' where postedBy='" + requestingUserid + "'");
                        if (noOfRowAffecting >= 1 && rowaffectingjobtable >= 0)
                        {
                            //  returnString = "{\"status\":0,\"Description\":\"user  blocked\",\"results\":[null]}";
                            response = new ResponseData(0, "User Blocked", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                    else
                    {
                        //returnString = "{\"status\":0,\"Description\":\"Check Column names of DataField\",\"results\":[null]}";
                        response = new ResponseData(0, "Check columns of inputData", null);
                        return returnStmnt(response.returnStatment());
                    }
                }
            }
            // returnString = "{\"status\":0,\"Description\":\"this blockuser is not exist  \",\"results\":[null]}";
            response = new ResponseData(0, "this blockuser is not exist", null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream payments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string paymentSource = null;
                    int paymentFromUserId = 0;
                    int paymentStatus = 0;
                    string paymentRecieptID = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("paymentSource"))
                        {
                            paymentSource = list.Value;
                        }
                        if (list.Key.Equals("paymentFromUserId"))
                        {
                            paymentFromUserId = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentStatus"))
                        {
                            paymentStatus = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentRecieptID"))
                        {
                            paymentRecieptID = list.Value;
                        }
                    }
                    if (Convert.ToInt32(paymentFromUserId) != 0 && paymentSource != null && paymentRecieptID != null && paymentStatus != 0)
                    {

                    }


                }
            }
            returnString = "{\"status\":0,\"Description\":\"this blockuser is not exist  \",\"results\":[null]}";
            return returnStmnt(returnString);
        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[null]}";
            return returnStmnt(returnString);
        }
    }
    public Stream getcurrentRewards(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    string paymentSource = null;
                    int paymentFromUserId = 0;
                    int paymentStatus = 0;
                    string paymentRecieptID = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("paymentSource"))
                        {
                            paymentSource = list.Value;
                        }
                        if (list.Key.Equals("paymentFromUserId"))
                        {
                            paymentFromUserId = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentStatus"))
                        {
                            paymentStatus = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("paymentRecieptID"))
                        {
                            paymentRecieptID = list.Value;
                        }
                    }
                    if (Convert.ToInt32(paymentFromUserId) != 0 && paymentSource != null && paymentRecieptID != null && paymentStatus != 0)
                    {

                    }


                }
            }
            returnString = "{\"status\":0,\"Description\":\"this blockuser is not exist  \",\"results\":[null]}";
            return returnStmnt(returnString);
        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[null]}";
            return returnStmnt(returnString);
        }
    }
    private void auditInfo(string inputData, int auditEvent, string eventDoneByUser, int associatedRefId)
    {
        try
        {
            DataTable dt = new DataTable();
            // dt = model.selectIntoTable("select luvid from ListofValues where luvValueDisplayName='" + luvValueDisplayName + "' and luvCategory='" + luvCategory + "'");
            if (inputData.Length > 500)
            {
                string[] arrOfString = SplitByLength(inputData, 499).ToArray();
                if (arrOfString.Length == 2)
                    model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
                        "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
                        + eventDoneByUser + "','" + arrOfString[0] + "','" + arrOfString[1] + "','null'," + associatedRefId + ",now()");
                else
                {
                    model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
         "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
         + eventDoneByUser + "','" + arrOfString[0] + "','" + arrOfString[1] + "','" + arrOfString[2] + "'," + associatedRefId + ",now()");
                }
            }
            else
            {
                model.insertIntoTable("insert into AuditTrail (auditEvent,eventDoneByUser," +
        "userActionChanges1,userActionChanges2,userActionChanges3,associatedRefId,eventDateTime )values (" + auditEvent + ",'"
        + eventDoneByUser + "','" + inputData + "','" + null + "','null'," + associatedRefId + ",now()");
            }
        }
        catch (Exception exc) { Console.WriteLine(exc.Message); }
    }
    public string[] SplitByLength(string s, int length)
    {
        string[] arr = new string[3];
        //for (int i = 0; i < s.Length; i += length)
        //{
        //    if (i + length <= s.Length)
        //    {
        //        yield return s.Substring(i, length);
        //    }
        //    else
        //    {
        //        yield return s.Substring(i);
        //    }
        //}
        return arr;
    }
    public Stream FileUpload(Stream file)
    {
        Boolean flag = true;


        //  flag = vallidateToken(file.token);
        if (flag == true)
        {
            try
            {
                //string APP_PATH = System.Web.HttpContext.Current.Request.ApplicationPath.ToLower();
                //string it = System.Web.HttpContext.Current.Server.MapPath(APP_PATH);
                // string path = HttpContext.Current.Server.MapPath
                FileStream fileToupload = new FileStream("abc.txt", FileMode.Create);
                Console.WriteLine(fileToupload.Name.ToString());
                byte[] bytearray = new byte[10000];
                int bytesRead, totalBytesRead = 0;
                do
                {
                    bytesRead = file.Read(bytearray, 0, bytearray.Length);
                    totalBytesRead += bytesRead;
                } while (bytesRead > 0);
                fileToupload.Write(bytearray, 0, bytearray.Length);


                fileToupload.Close();
                fileToupload.Dispose();

                //  flagafterUpload = true;
                response = new ResponseData(0, "saved", null);
                return returnStmnt(response.returnStatment());
                //model.insertIntoTable("insert into Resume (refDocumentPath,uploadDateTime,refUserID,resumeTitle,resumeMime) values (' content\\"+file.fileName+
                //    "', now(),'"+file.associatedId+"','"+file.
            }
            catch (Exception exc)
            {
                response = new ResponseData(0, exc.Message, null);
                return returnStmnt(response.returnStatment());
            }

        }
        response = new ResponseData(0, "saved", null);
        return returnStmnt(response.returnStatment());

    }
    private byte[] GetBytes(Stream filecontents)
    {
        int result = 0;
        int bytesRead;
        byte[] buffer = new byte[100000];
        do
        {
            bytesRead = filecontents.Read(buffer, 0, buffer.Length);
            result += bytesRead;
        } while (bytesRead > 0);
        return buffer.Take(result).ToArray();
    }
    public Stream getCurrentRewards(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    int associatedRegId = 0;
                    int sumOfRewardes = 0;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("associatedRegId"))
                        {
                            associatedRegId = Convert.ToInt32(list.Value);
                        }
                    }
                    if (associatedRegId != 0)
                    {
                        sumOfRewardes = rewards.sumOfRewardes(associatedRegId);
                        returnString = "{\"status\":0,\"Description\":\"Your Rewards is calculted  \",\"results\":[{\"Rewards\":" + sumOfRewardes + "}]}";
                    }
                }
            }
            returnString = "{\"status\":0,\"Description\":\"  \",\"results\":[null]}";
            return returnStmnt(returnString);
        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[null]}";
            return returnStmnt(returnString);
        }
    }
    public Stream getCDNUrl(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;

        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                //data have 4 parameters=application name,location,lattitude,longitude
                {
                    response = new ResponseData(1, Constant.foundData, "[{\"CDNUrl\":\"http://developer.csimplifyit.com/takemyjob.svc/\"}]");
                    return returnStmnt(response.returnStatment());
                }

            }
            // auditInfo("takemyjob", "getfriends", "want to get refrence friends",regid,Constant.jsonNotCorrect , null, null);
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getInterestedFriendsInmyjobs(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        int postedBy = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("postedBy"))
                    {
                        postedBy = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select jid , count(appliedbyUser) as jobApplications,(jobExpiresOn>now()) as jobExpireFlag,  DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle,jobReferenceLink,joblatitude,jobLongitude,jobLocationName,jobType,jobComments," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn , DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,"
                            + "jobVertical,jobRole from Job As J LEFT JOIN   JobApplications As A ON J.jid = A.jobRefId Where J.postedBy = '" + postedBy + "' group by jid order by postedOnDateTime desc");
                        string result = convertDataTableToJson(dt);
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream postResumeToHelpingFriends(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            string jobID = null;
            string resumeRequireForIDs = null;
            //jobid,resumeRequireForIds
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("resumeRequireForIDs"))
                        {
                            resumeRequireForIDs = list.Value;
                        }
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }
                    if (resumeRequireForIDs != null && jobID != null)
                    {
                        dt = model.selectIntoTable("select rid, refDocumentPath, resumeTitle, jid , appliedByUser,  DATE_FORMAT(appliedOnDateTime,'%d-%b-%Y') as appliedOnDateTime," +
                            "jobTitle," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn "
                            + "from Job As J LEFT JOIN   JobApplications As A ON J.jid = A.jobRefId LEFT JOIN Resume R ON  R.refUserId = A.appliedByUser where J.jid = " + jobID + "");
                        string result = convertDataTableToJson(dt);
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream getMessagesFromFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        // string returnString = null; 
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    // recievedBy,recievedFrom,recievedSince,Message,messageType
                    string recievedBy = null;
                    string recievedFrom = null;
                    string recievedSince = null;
                    string dynamicWhereClause = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("myID"))
                        {
                            recievedBy = list.Value;
                        }
                        if (list.Key.Equals("friendID"))
                        {
                            recievedFrom = list.Value;
                        }
                        if (list.Key.Equals("recievedSince"))
                        {
                            recievedSince = list.Value;
                        }
                    }
                    dynamicWhereClause = recievedSince == "" ? "" : "and messageDateTime > '" + recievedSince + "'";
                    if (Convert.ToInt32(recievedFrom) != 0)
                    {
                        dt = model.selectIntoTable("select messageMime, messageDateTime, messageStatus, messageRefUrl, msgType,forID,byID, message " +
                             "from FriendlyMessages where forID = " + recievedBy + " and byID = " + recievedFrom + dynamicWhereClause + " order by messageDateTime desc");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Messages", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream sendMessagetoFriend(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string sendBy = null;
                    string message = null;
                    string messageType = null;
                    string sendTo = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("sendBy"))
                        {
                            sendBy = list.Value;
                        }
                        if (list.Key.Equals("message"))
                        {
                            message = list.Value;
                        }
                        if (list.Key.Equals("messageType"))
                        {
                            messageType = list.Value;
                        }
                        if (list.Key.Equals("sendTo"))
                        {
                            sendTo = list.Value;
                        }
                    }
                    if (Convert.ToInt32(sendBy) != 0)
                    {
                        string responseOfInsert = model.insert("insert into FriendlyMessages(messageMime, messageDateTime, messageStatus, messageRefUrl, msgType,forID,byID, message) " +
                             "values('application/text', now(), 1, ''," + messageType + ", " + sendBy + ", " + sendTo + ", '" + message + "' )");
                        if (responseOfInsert.Equals("true"))
                        {
                            response = new ResponseData(0, Constant.messageSaved, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }

                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream queueSingleNotification(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Queue queue = new Queue();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    int queueType = 0;
                    int queueRegarding = 0;
                    int queueRegardingRefId = -1;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("queueType"))
                        {
                            queueType = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("queueRegarding"))
                        {
                            queueRegarding = Convert.ToInt32(list.Value);
                        }
                        if (list.Key.Equals("queueRegardingRefId"))
                        {
                            queueRegardingRefId = Convert.ToInt32(list.Value);
                        }

                    }
                    if (queueRegarding != 0 && queueRegardingRefId != -1)
                    {
                        if (queueRegarding == 1)
                        {
                            queue.queueJobNotification(queueRegardingRefId);
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.userNotExist, null);
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            // auditInfo("takemyjob", "blockuser", "want to block friends", Convert.ToInt32(requestingUserid), "Exception Occur", null, null);
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream postOnYourSocialWall(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        // string returnString = null;
        string requestingUserid = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                // returnString = "{\"status\":0,\"Description\":\"invallid token\",\"results\":[null]}";
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string message = null;
                    foreach (var list in jsonkeyvalue)
                    {                            // requestingUserid = regUserId, blockUserId = refUserId, blockRequestType
                        if (list.Key.Equals("requestingUserid"))
                        {
                            requestingUserid = list.Value;
                        }
                        if (list.Key.Equals("message"))
                        {
                            message = list.Value;
                        }
                    }
                    string responselinkedin = linkedin.updateStatusOnOauthServer(message, requestingUserid);
                    if (responselinkedin.Equals("true"))
                    {
                        response = new ResponseData(1, "Successfully posted", null);
                    }
                    else if (responselinkedin.Equals("false"))
                    {
                        response = new ResponseData(1, "Please register with your linkedin account with us.", null);
                    }
                }
                // response = new ResponseData(0, "Not a Vallid User", null);

            }
            return returnStmnt(response.returnStatment());
        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private string callTwilioFormessage(string message)
    {
        try
        {
            string param = "Body={'inputData':{'meta': {'token': '098084038403240434324324kjhkhfjhdskf','action':'actionName','validation': 'runthisvalidationonserver', 'previousaction': 'actionName' },'data':[{'contactNo':'8950706505','message':'hi parveen'},{'contactNo':'965834474871','message':'thanks'}]}}";
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://use-this.appspot.com/");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] buffer = System.Text.Encoding.Unicode.GetBytes(param);
            Stream POSTstream = request.GetRequestStream();
            POSTstream.Write(buffer, 0, buffer.Length);
            HttpWebResponse POSTResponse = (HttpWebResponse)request.GetResponse();
            Stream responseStream = POSTResponse.GetResponseStream();
            byte[] responsebytes = GetBytes(responseStream);
            string response = System.Text.Encoding.UTF8.GetString(responsebytes);
        }
        catch (Exception ecx)
        {
            Console.WriteLine(ecx.Message);
        }
        return "";
    }
    private Boolean vallidateJson(string message)
    {
        try
        {
            JObject vallidateobject = JObject.Parse("{\"data\":\"" + message + "\"}");
            return true;
        }
        catch (Exception exc)
        {
            Console.WriteLine(exc.Message);
        }
        return false;
    }
    public Stream likeCount(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;
                    string jobComments = null;
                    string expiryDays = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }
                        if (list.Key.Equals("jobComments"))
                        {
                            jobComments = list.Value;
                        }
                        if (list.Key.Equals("expiryDays"))
                        {
                            expiryDays = list.Value;
                        }
                    }
                    if (userID != null && jobID != null)
                    {
                        int rows = model.updateIntoTablewithRowCount("update Job set jobComments='" + jobComments + "',jobExpiresOn="
                            + "DATE_ADD(now(),INTERVAL " + Convert.ToInt32(expiryDays) + " DAY) where jid=" + jobID + " and postedBy=" + userID);
                        if (rows != 0)
                        {
                            response = new ResponseData(1, "Your comments for job has been updated", null);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "You have no job for comments", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }

    }
    public Stream forgetPassword(string inputData)
    {
        List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {

                }
                if (objelement.Key.Equals("data"))
                {

                    list = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                }
            }
            string username = null;
            foreach (var keyvalue in list)
            {
                if (keyvalue.Key.Equals("userName"))
                {
                    username = keyvalue.Value;
                }
            }
            if (username != null)
            {
                dt = model.selectIntoTable("select uid userPassword from Users where userName='" + username + "'");
                if (dt.Rows.Count != 0)
                {
                    int uid = Convert.ToInt32(dt.Rows[0][0]);
                    byte[] array = Encoding.ASCII.GetBytes(uid.ToString() + dateTimeClass.datetime());
                    string encoded = System.Convert.ToBase64String(array);
                    dt = model.selectIntoTable("select * from AuthenticationTokens where regUserId=" + uid + " and authSite='system'");
                    if (dt.Rows.Count == 0)
                    {
                        model.insertIntoTable("insert into AuthenticationTokens (regUserId,token,authSite) value (" + uid + ",'" + encoded + "','system')");

                    }
                    else
                    {
                        model.updateIntoTable("update AuthenticationTokens set token='" + encoded + "' where regUserId=" + uid + " and authSite='system'");
                    }
                    EmailSend sendEmail = new EmailSend();
                    sendEmail.sendMail(username, "You recently asked to reset your Facebook password. Your link for reset password :http:/developer.csimplifyit.com/Default.aspx?linkshow$" + encoded);
                    response = new ResponseData(0, "Mail send ", null);
                    return returnStmnt(response.returnStatment());

                }
                else
                {
                    response = new ResponseData(0, "invallid userName", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input data is not correct", null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            returnString = response.returnStatment();
            return returnStmnt(returnString);
        }
    }
    public Stream updateCommentOnJob(string inputData)
    {

        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;
                    string jobComments = null;
                    string expiryDays = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }
                        if (list.Key.Equals("jobComments"))
                        {
                            jobComments = list.Value;
                        }
                        if (list.Key.Equals("expiryDays"))
                        {
                            expiryDays = list.Value;
                        }
                    }
                    if (userID != null && jobID != null)
                    {
                        int rows = model.updateIntoTablewithRowCount("update Job set jobComments='" + jobComments + "',jobExpiresOn="
                            + "DATE_ADD(now(),INTERVAL " + Convert.ToInt32(expiryDays) + " DAY) where jid=" + jobID + " and postedBy=" + userID);
                        if (rows != 0)
                        {
                            dt = model.selectIntoTable("select jid from Job where jid= " + jobID + " and postedBy = " + userID);
                            if (dt.Rows.Count != 0)
                            {
                                response = new ResponseData(1, "Your comments for job has been updated", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        else
                        {
                            response = new ResponseData(1, "You have no job for comments", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }

    }
    public Stream getJobApplications(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        DataSet ds = new DataSet();
        int jobID = 0;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            Boolean flag = false;
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");
                    if (jsonkeyvalue[0].Key.Equals("jobID"))
                    {
                        jobID = Convert.ToInt32(jsonkeyvalue[0].Value);
                        dt = model.selectIntoTable("select distinct(jid) , appliedByUser,userName, userContactNo, displayName, DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle, DATE_FORMAT(appliedOnDateTime,'%d-%b-%Y') as appliedOnDateTime," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn, "
                            + "jobVertical,jobRole from Users U,  JobApplications As A LEFT JOIN Job As J  ON J.jid = A.jobRefId Where A.appliedByUser = U.uid and A.jobRefId = " + jobID + " order by appliedOnDateTime desc");
                        string result = convertDataTableToJson(dt
                            );
                        if (result != null)
                        {

                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":" + result + "}";
                            response = new ResponseData(1, Constant.foundData, result);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            // returnString = "{\"status\":1,\"Description\":\"found data\",\"results\":[null]}";
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, Constant.jsonNotCorrect, null);
            return returnStmnt(response.returnStatment());
        }
        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    public Stream revokeJob(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }

                    }
                    if (userID != null && jobID != null)
                    {
                        int countJobRowUpdate = model.updateIntoTablewithRowCount("update Job set jobStatus=4 where jid=" + jobID + " and postedBy=" + userID);
                        if (countJobRowUpdate > 0)
                        {
                            dt = model.selectIntoTable("select jid from Job where jid=" + jobID + " and postedBy=" + userID);
                            response = new ResponseData(1, "Job Revoked", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Could not revoke job", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }

                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream revokeJobApplication(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }

                    }
                    if (userID != null && jobID != null)
                    {
                        int countJobRowUpdate = model.updateIntoTablewithRowCount("update JobApplications set applicationStatus=4 where jobRefid=" + jobID + " and appliedByUser=" + userID);
                        if (countJobRowUpdate > 0)
                        {
                            dt = model.selectIntoTable("select jaid from JobApplications where jobRefid=" + jobID + " and appliedByUser=" + userID);
                            response = new ResponseData(1, "Job application Revoked", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Could not revoke job application", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream registerServerNotification(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string serverNotificationToken = null;
                    string userID = null;
                    string deviceID = null;

                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("serverNotificationToken"))
                        {
                            serverNotificationToken = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }
                        if (list.Key.Equals("deviceID"))
                        {
                            deviceID = list.Value;
                        }

                    }
                    if (userID != null && deviceID != null && serverNotificationToken != null)
                    {
                        dt = model.selectIntoTable("select regUserId from AuthenticationTokens where socialID ='" + deviceID + "',token='" + serverNotificationToken + "'");
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Server notification token already Exist", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            dt = model.insertIntoTableWithLastId("insert into AuthenticationTokens(regUserId,token,lastModifiedDate,createdDateTime) values"
                                + userID + ",'" + serverNotificationToken + "',now(),now()");
                            if (dt.Rows.Count != 0)
                            {
                                response = new ResponseData(1, "Server notification token successfully generated", convertDataTableToJson(dt));
                                return returnStmnt(response.returnStatment());
                            }
                        }
                    }

                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream introduceFriends(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());
                            }
                        }
                        if (keypair.Key.Equals("userID"))
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != null)
                {
                    insertedIntoFriendRequests("{\"data\":" + objelement.Value.ToString() + "}", userID);
                    response = new ResponseData(1, "success", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertedIntoFriendRequests(string jsonarr, string uid)
    {
        DataTable dt = new DataTable();
        string requestType = null;
        string forFriendID = null;
        string toID = null;
        StringBuilder insertStmt = new StringBuilder();
        JObject root = JObject.Parse(jsonarr);
        JArray items = (JArray)root["data"];
        foreach (var data in items)
        {
            JObject localdata = JObject.Parse(data.ToString());
            foreach (var keyvalues in localdata)
            {
                if (keyvalues.Key.Equals("requestType"))
                {
                    requestType = keyvalues.Value.ToString();
                } if (keyvalues.Key.Equals("forFriendID"))
                {
                    forFriendID = keyvalues.Value.ToString();
                } if (keyvalues.Key.Equals("toID"))
                {
                    toID = keyvalues.Value.ToString();
                }
            }
            table = model.selectIntoTable("select * from RefereredUsers where regUserId=" + toID + " and refUserId=" + forFriendID);
            if (table.Rows.Count == 0)
            {
                table = model.selectIntoTable("select * from FriendRequests where forFriendID=" + forFriendID + " and toID =" + toID);
                if (table.Rows.Count == 0)
                {
                    insertStmt.Append("insert into FriendRequests (requestStatus,introducedOn,introducedBy,requestType,forFriendID,toID) values (0,now(),"
                        + uid + "," + requestType + ","
                        + forFriendID + "," + toID + ")");
                    model.insertIntoTable(insertStmt.ToString());

                }
                insertStmt.Clear();
            }
        }
        return true;
    }
    public Stream getPendingRequests(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string toID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("toID"))
                        {
                            toID = list.Value;
                        }
                    }
                    if (toID != null)
                    {
                        dt = model.selectIntoTable("select distinct(F.frid),U.uid,U.displayName,U.currentJobTitle," +
                            " I.displayName as introduceBy from Users as U, Users as I ,FriendRequests as F where U.uid=F.forFriendID and " +
                       "RequestStatus=0 and  F.introducedBy=I.uid and toID=" + toID);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream introductionUpdate(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string userID = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                        if (keypair.Key.Equals("userID"))
                        {
                            userID = keypair.Value;
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true && userID != null)
                {
                    insertedIntroductionUpdate("{\"data\":" + objelement.Value.ToString() + "}", userID);
                    response = new ResponseData(1, "success", null);
                    return returnStmnt(response.returnStatment());
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }
    }
    private Boolean insertedIntroductionUpdate(string jsonarr, string toID)
    {
        DataTable dt = new DataTable();
        string forFriendID = null;
        string updateStatus = null;
        StringBuilder insertStmt = new StringBuilder();
        JObject root = JObject.Parse(jsonarr);
        JArray items = (JArray)root["data"];
        foreach (var jsonkeyvalue in items)
        {
            JObject localdata = JObject.Parse(jsonkeyvalue.ToString());
            foreach (var list in localdata)
            {                          // sendBy,sendTo,Message,messageType
                if (list.Key.Equals("forFriendID"))
                {
                    forFriendID = list.Value.ToString();
                }
                if (list.Key.Equals("updateStatus"))
                {
                    updateStatus = list.Value.ToString();
                }
                if (toID != null && forFriendID != null && updateStatus != null)
                {
                    int status = Convert.ToInt32(updateStatus);
                    dt = model.selectIntoTable("select * from RefereredUsers where regUserId=" + toID + " and refUserId=" + forFriendID);
                    if (dt.Rows.Count == 0)
                    {
                        if (status == 1)
                        {
                            dt = model.insertIntoTableWithLastId("insert into RefereredUsers (regUserId,refUserId,createdDateTime) values(" + toID + "," + forFriendID + ",now())");
                            if (dt.Rows.Count != 0)
                            {
                                model.updateIntoTable("update FriendRequests set requestStatus=1 where forFriendID=" + forFriendID + " and toID=" + toID);
                            }
                        }
                        else if (status == -1)
                        {
                            model.updateIntoTable("update FriendRequests set requestStatus=-1 where forFriendID=" + forFriendID + " and toID=" + toID);
                        }
                    }
                }
            }


        }
        return true;
    }
    public string keyValuePairToJson(List<KeyValuePair<string, string>> list)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var KeyValuePair in list)
        {
            //sb.Append("[{"+list.+"}");
        }
        return sb.ToString();
    }
    public Stream updateJobApplicationComments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;
                    string applicationNotes = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }
                        if (list.Key.Equals("applicationNotes"))
                        {
                            applicationNotes = list.Value;
                        }
                    }
                    if (userID != null && jobID != null)
                    {
                        int rowUpdates = model.updateIntoTablewithRowCount("update JobApplications set applicationNotes='" + applicationNotes + "' where jobRefid=" + jobID
                            + " and appliedByUser=" + userID);
                        if (rowUpdates != 0)
                        {
                            response = new ResponseData(1, "Application notes successfully updated", null);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Application notes already posted", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }
    }
    public Stream getJobApplicationComments(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        string returnString = null;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    string userID = null;
                    string applicationNotes = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                        if (list.Key.Equals("userID"))
                        {
                            userID = list.Value;
                        }
                        if (list.Key.Equals("applicationNotes"))
                        {
                            applicationNotes = list.Value;
                        }
                    }
                    if (userID != null && jobID != null)
                    {
                        int rowUpdates = model.updateIntoTablewithRowCount("update JobApplications set applicationNotes='" + applicationNotes + "' where jobRefid=" + jobID
                            + " and appliedByUser=" + userID);
                        if (rowUpdates != 0)
                        {
                            response = new ResponseData(1, "Application notes successfully updated", null);
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, "Application notes already posted", null);
                            return returnStmnt(response.returnStatment());
                        }
                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            returnString = "{\"status\":0,\"Description\":\"" + ex.Message + "\",\"results\":[]}";
            return returnStmnt(returnString);
        }
    }
    public Stream getJobDetail(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string jobID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("jobID"))
                        {
                            jobID = list.Value;
                        }
                    }
                    if (jobID != null)
                    {
                        dt = model.selectIntoTable("select jid,postedBy, DATE_FORMAT(postedOnDateTime,'%d-%b-%Y') as postedOnDateTime," +
                            "jobTitle,jobReferenceLink,joblatitude,jobLongitude,jobLocationName,jobType,jobComments,jobSkills," +
                            " DATE_FORMAT(jobExpiresOn,'%d-%b-%Y') as jobExpiresOn , DATE_FORMAT(joblastModifiedOn,'%d-%b-%Y') as joblastModifiedOn,jobStatus,jobExperienceMinYears,jobExperienceMaxYears,"
                            + "jobVertical,jobRole from Job where jid=" + jobID);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }

    }
    public Stream getFriendDetail(string inputData)
    {
        List<KeyValuePair<string, string>> jsonkeyvalue = new List<KeyValuePair<string, string>>();
        Boolean flag = false;
        try
        {
            DataTable dt = new DataTable();
            JObject jsonrow = JObject.Parse(inputData);
            foreach (var objelement in jsonrow)
            {
                if (objelement.Key.Equals("meta"))
                {
                    jsonkeyvalue = metaToKeyValuePair(objelement.Value.ToString());
                    foreach (var keypair in jsonkeyvalue)
                    {
                        if (keypair.Key == "token")
                        {
                            flag = vallidateToken(keypair.Value);
                            if (flag != true)
                            {
                                response = new ResponseData(0, Constant.invallidtoken, null);
                                return returnStmnt(response.returnStatment());

                            }
                        }
                    }
                }
                if (objelement.Key.Equals("data") && flag == true)
                {
                    jsonkeyvalue = jsonToKeyValuePair("{\"data\":" + objelement.Value.ToString() + "}");

                    string friendID = null;
                    foreach (var list in jsonkeyvalue)
                    {                          // sendBy,sendTo,Message,messageType
                        if (list.Key.Equals("friendID"))
                        {
                            friendID = list.Value;
                        }
                    }
                    if (friendID != null)
                    {
                        dt = model.selectIntoTable("SELECT distinct(uid),displayName,userName,userContactNo," +
                                "userType,userStatus,currentCompanyName,currentJobTitle,currentIndustry fron Users where uid=" + friendID);
                        if (dt.Rows.Count != 0)
                        {
                            response = new ResponseData(1, "Suggesting friends", convertDataTableToJson(dt));
                            return returnStmnt(response.returnStatment());
                        }
                        else
                        {
                            response = new ResponseData(1, Constant.notFound, null);
                            return returnStmnt(response.returnStatment());
                        }

                    }
                }
            }
            response = new ResponseData(0, "Input parameter are not vallid", null);
            return returnStmnt(response.returnStatment());

        }

        catch (Exception ex)
        {
            response = new ResponseData(0, ex.Message, null);
            return returnStmnt(response.returnStatment());
        }


    }
}

// metaToKeyValuePair(" {\"token\": 1,\"action\": \"actionName\", \"validation\": \"runthisvalidationonserver\", \"previousaction\": \"actionName\" }");
//   insertJsonintoUsers("Users", "{ \"data\": [   {   \"name\": \"kuldeep\",   \"userContactNo\": \"89769954\",   \"userName\": \"abc@gmail.com;\",\"source\": \"iOS/Android/LinkedIn/FaceBook etc\" },   {\"name\":\"mohit\",\"userContactNo\": \"8901232002\",\"emailid\": \"cvf@gmail.com\",\"source\":\"iOS Or Android/LinkedIn/FaceBook etc\",  }]}", 1);
// .........require a candidate for the job having an experience of .........

// getCsimplifyitCDNBasedServerUrl(string inputData,application name,lattitude ,longitude,divicetype,){
//return "baseurl"
//}
//userid 
//sendMessageRoFriend
// sendBy,sendTo,Message,messageType

//getMessagesFromFriend
// recievedBy,recievedFrom,recievedSince,Message,messageType
//  updateComments and Expiry(userid,no of days)jid commnet expirydays

//userID,jobID
//knowmyfriends add uid in parameter.
//chack userSkill table,
// post job api for jobexpire
//space between skill, and 

//getJobDetail(jobid)
//getFriend (friend)
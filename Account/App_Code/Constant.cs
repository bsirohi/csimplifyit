﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Constant
/// </summary>
public static class Constant
{
    public const string vallid = "Vallid user";
    public const string notvallid = "Not a vallid user";
    public const string invallidtoken = "invallid token";
    public const string foundData = "Found Data";
    public const string notFound = " Data not found";
    public const string jsonNotCorrect = "Your json string is not in correct format";
    public const string blockUser = " User is blocked";
    public const string unblockUser = " User is Unblocked";
    public const string checkDataColumns = "Check Column names of datafields";
    public const string userNotExist = "Blocked user does not exist";
    public const string applicationPostSuccessfully = "application for job successfully posted";
    public const string alreadyapplyjob = "job is already applied";
    public const string messageforunregister = "Mail has been send for registration";
    public const string CSimplifyitBaseUrl = "http://developer.csimplifyit.com/takemyjob.svc/";
    public const string jobalreadyposted = "Job already posted";
    public const string messageSaved = "Message has been Saved";
    public const string givenUserNotExist = "User does not Exist";
   
}
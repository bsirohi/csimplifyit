﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ItakeMyJob" in both code and config file together.
[ServiceContract]
public interface ItakeMyJob
{
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getToken/", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json),]
    Stream getToken(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getRefrenceFriends/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getRefrenceFriend(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/postJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream postNewJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/applyJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream applyForJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getLov/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getLov(string inputData);
    /* no result found is on description if no data found and status success */
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getJobs/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getJobs(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/knowMyFriends/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream knowMyFriends(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/blockUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream blockUser(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/unblockUser/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream unBlockUser(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/takeMyJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream takemyJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/optOut/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream optOut(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/payments/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream payments(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/rewards/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getcurrentRewards(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/FileUpload/")]
    Stream FileUpload(Stream file);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getCDNUrl/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getCDNUrl(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getInterestedFriendsInmyjobs/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getInterestedFriendsInmyjobs(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/postResumeToHelpingFriends/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream postResumeToHelpingFriends(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getMessagesFromFriend/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getMessagesFromFriend(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/sendMessagetoFriend/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream sendMessagetoFriend(string inputData);
    [OperationContract]
    [WebInvoke(Method = "GET", UriTemplate = "/test/", ResponseFormat = WebMessageFormat.Json)]
    string testfunc();
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/postOnYourSocialWall/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream postOnYourSocialWall(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/updateCommentOnJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream updateCommentOnJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/forgetPassword/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream forgetPassword(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getJobApplications/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getJobApplications(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/revokeJob/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream revokeJob(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/revokeJobApplication/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream revokeJobApplication(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/introduceFriends/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream introduceFriends(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/getPendingRequests/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream getPendingRequests(string inputData);
    [OperationContract]
    [WebInvoke(Method = "POST", UriTemplate = "/introductionUpdate/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
    Stream introductionUpdate(string inputData);
}

/*blockUser(user who intiate blocking, user to be block) in user table set the values  */
/*private function */;

//postOnYourSocialWall

//getApi() Meta part.....data:appName,currentLatitude,currentLognitude
//postResumeToHelpingFriends
//write auditTable api

// initiate payments,confirm paymens,bad payment,sentpayment invoice,

//getJobApplication
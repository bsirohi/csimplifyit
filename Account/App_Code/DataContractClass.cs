﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;

/// <summary>
/// Summary description for DataContractClass
/// </summary>
  [DataContract]
public class FileContentSerialize
{
   [DataMember]
    public Stream fileContents { get; set; }
    [DataMember]
    public string token { get; set; }
    [DataMember]
    public string fileType {get;set;}
    [DataMember]
    public string associatedId { get; set; }
    [DataMember]
    public string fileName { get; set; }
}
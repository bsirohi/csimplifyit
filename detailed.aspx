﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="detailed.aspx.cs" Inherits="csimplifyit.WebForm23" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true



        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


   <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->
   <hr />	
	
	<h2 class="text_blue">Detailed</h2>
    <p>Detailed templates are useful to ensure all stakeholders are on same page, when engagement start or engagement is big enough which requires multiple teams to work in tandem and high communication needs. Elaborative steps with clear entry and exit criterion. </p>
</div>
</asp:Content>

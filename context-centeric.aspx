﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="context_centeric.aspx.cs" Inherits="csimplifyit.WebForm5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
	    $('#slides').slides({
		    preload: true,
		    preloadImage: 'img/loading.gif',
		    play: 5000,
		    pause: 2500,
		    hoverPause: true,
		    start: 3   //no of slide to be displayed 

	    });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<div id="body-wrapper" class="clearfix"><!--start body-wrapper-->

 <div class="clear"></div>
 <hr />
<h1 class="text_blue">Context Centric Services</h1>
 <div class="clear"></div>
 <div class="three_fourth">
 <p>
<span class="text_red"> Simplify IT context-enriched services use information about a person or object to proactively anticipate the user's need and serve up the content, 
product or service most appropriate to the user.</span> The IT industry is beginning to recognize a pattern where augmented reality offerings, mobile location-aware ads, 
mobile search and social mobile sites fall under the umbrella term. Context-aware computing is about improving the user experience for customers, business partners 
and employees by using the information about a person or object's environment, activities, connections and preferences to anticipate the user's needs and proactively 
serve up the most appropriate content, product or service. Enterprises can leverage context-aware computing to better target prospects, increase customer intimacy, 
and enhance associate productivity and collaboration.

 </p>

 </div>

 <div class="one_fourth column-last">
 <div class="fancy"><a id="fancybox" href="images/Context-Aware.jpg"> <img class="radius_10" src="images/Context-Aware.jpg" /></a></div>
	
 </div>
 <div class="clear"></div>
<p> CSimplify IT Context-enriched services are provided by context brokers, which are designed to retrieve, process and stage this information so that subscribing 
functions can use relevant context in addition to processing incoming data. When an application uses context-enriched services, it is a context-aware application.
 As a best practice, context-enriched software services have the modularity and accessibility of SOA and use SOA-related standards. </p>
 <div class="clear"></div>
 <div class="three_fourth">
	
	  <div class="fancy"><a id="fancybox" href="images/Context%20Aware.PNG">  <img class="radius_20" src="images/Context%20Aware.PNG" /></a></div>
 
 </div>
 <div class="one_third last">
 <p>Usually current context-aware solutions are fragmented — they are individually designed, custom-developed and deployed, and, because of their competitive importance,
  are often not widely distributed or advertised.</p>
 <br />
<p>C Simpify IT context-aware computing solutions, are clearly using context information to improve the user experience. These include Apple's recent developer guidance 
regarding location-aware advertising, the augmented reality systems that give you information on an object shown in the camera lens of your phone, and the ability of 
Google Android-based phones to augment services based on the user's contacts, behavior and other components of context information. </p>
 </div>
 </div>
</asp:Content>

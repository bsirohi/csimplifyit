﻿using System;
using System.Collections.Generic;
using csimplifyit.DBContext;
using csimplifyit.MsgFormatter;
using System.Web.Services;
using csimplifyit.Model;
using Newtonsoft.Json.Linq;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Text;
using csimplifyit.Sign_in;
using System.Linq;
using System.IO;

namespace csimplifyit.Login
{
    public partial class Login : System.Web.UI.Page
    {
        private static csitEntities1 db = new csitEntities1();
        private static Dictionary<string, object> moResponse;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static object UserLogin()
        {
            try
            {


                return moResponse;
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:" + ex.InnerException + "Message:" + ex.Message + "Stack Trace:" + ex.StackTrace, null, "SaveDocumentClass");
                return moResponse;
            }
        }


        public long getUserId()
        {

            if (Session == null)
            {
                return 0;
            }
            else
            {
                UserInfo userInfo = (UserInfo)Session["userInfo"];
                if (userInfo != null)
                {
                    return userInfo.userId;
                }
                else
                {
                    return 0;
                }
            }
        }
        
        protected string getUserName()
        {
            UserInfo userInfo = (UserInfo)Session["userInfo"];
            if (userInfo != null)
            {
                return userInfo.userName;
            }
            else
            {
                return string.Empty;
            }

        }

        protected string getEmailaddress()
        {
            UserInfo userInfo = (UserInfo)Session["userInfo"];
            if (userInfo != null)
            {
                return userInfo.email;
            }
            else
            {
                return null;
            }

        }


        public void setLoggedInUserDetails(UserInfo userInfo)
        {
            if (Session == null)
            {
                var sSession = System.Web.HttpContext.Current.Session;
                sSession["userInfo"] = userInfo;
            }
            else if (Session["userInfo"] == null)
            {
                Session["userInfo"] = userInfo;

            }

        }
       [WebMethod]
        public static object LoginScreendata(string requestData)
        {
            try
            {
                usermaster Regdetails = new usermaster();
                JObject jsonobj = JObject.Parse(requestData);

                string Email = string.Empty;
                if (jsonobj["Email"] != null && jsonobj["Email"].ToString() != "")
                {
                    Email = jsonobj["Email"].ToString();
                }


                string password = string.Empty;
                if (jsonobj["Password"] != null && jsonobj["Password"].ToString() != "")
                {
                    password = jsonobj["Password"].ToString();

                    var fetchpswd = db.usermasters.Where(s => s.EmailId1 == Email).FirstOrDefault();
                    if (fetchpswd != null)
                    {
                        var decyber = Decrypt(fetchpswd.Password);
                        if (password == decyber)
                        {
                            long UserId = fetchpswd.userid;
                            csimplifyit.Sign_in.Signin.GoogleLogIN(UserId);
                            moResponse = RespMsgFormatter.formatResp(1, "Login Sucessfully ", null, "LoginScreendata");
                            return moResponse;

                        }
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "User Does not Exist", null, "LoginScreendata");
                        return moResponse;
                    }


                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "Enter Valid Password", null, "LoginScreendata");
                    return moResponse;

                }



                return moResponse;
            }

            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:" + ex.InnerException + "Message:" + ex.Message + "Stack Trace:" + ex.StackTrace, null, "SaveDocumentClass");
                return moResponse;
            }

        }

        public static string Encrypt(string text)
        {
            encr lokey = new encr();

            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(lokey.key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }

        public static string Decrypt(string cipher)
        {
            encr lokey = new encr();
            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(lokey.key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateDecryptor())
                    {
                        byte[] cipherBytes = Convert.FromBase64String(cipher);
                        byte[] bytes = transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);
                        return UTF8Encoding.UTF8.GetString(bytes);
                    }
                }
            }
        }


    }


}
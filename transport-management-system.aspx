<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>
<asp:Content ID="TransportHead" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Transport-Management | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="TransportMaincontent" ContentPlaceHolderID="MainContent" runat="server">       
	<div class="wrapper">
    
		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Transport Management System</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.aspx">Home</a></li>
					<li><a href="">Products</a></li>
					<li><a  href="transport-management-system.aspx">Transport Management System</a></li></li>					
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		
        <!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				<!-- Content Info -->
				<div class="col-md-5">
					<h2>YOCarry Transport Management</h2>
					<p style="text-align:justify;">YOCarry Transport Management System with illiterate design, A TMS system designed to be used by people who cannot read/write. Be 10X better. </p>
                    <p>C Simplify IT transportation management system (TMS) helps companies move freight from origin to destination efficiently, reliably, and cost effectively. TMS can reduce freight spend. Several methods can be used to reduce freight spend, with the different "savings buckets" having different levels of savings. Using the TMS system Customer can choose Carrier based on time of transit, cost of shipping, and insurance limits. Key features of C Simplify IT - Transport Management System (TMS).</p>
					<p>1. Contract Management - Create Contracts for fixed rate, monthly rates and based on unit of measurement. Auto calculate estimates and generate Invoices using contract information.</p>
					<p>2. Work Order- Track movement of vehicles for monthly and adhoc movements. Ability to capture empty movements and non conformance days.</p>
					<p>3. Upload Vehicle Movement - Upload the vehicle movement using excel sheets.</p>
					<p>4. Generate Estimates and Invoices - Generate estimates and invoices using vehicle movement data. Workflow for approvals and rejection on each lane level movement. </p>
					<p>5. TMS Analytics - TMS gives you confidence that your customers are receiving their shipments on time, using our TMS analytics which scores carriers on various parameters. </p>
					
					<a href="#" class="btn-u btn-u-large">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
                <!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
						<div class="carousel-inner" id="tms-carousel">
                            <div class="item active">
								<img alt="" src="assets/img/main/YOCarryTMS .png">
								<div class="carousel-caption">
									<p>YOCarry Transport Management System with illiterate design </p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/tms1.png">
								<div class="carousel-caption">
									<p>Transport Management System - Dashboard . </p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/tms2.png">
								<div class="carousel-caption">
									<p>Transport Management System - Contract Master.</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/tms3.png">
								<div class="carousel-caption">
									<p>Transport Management System - Work Order.</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/estimate_report.png">
								<div class="carousel-caption">
									<p>Transport Management System - Estimate Report.</p>
								</div>
							</div>
						</div>

						<div class="carousel-arrow">
							<a data-slide="prev" href="#myCarousel" class="left carousel-control">
								<i class="fa fa-angle-left"></i>
							</a>
							<a data-slide="next" href="#myCarousel" class="right carousel-control">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
				<!-- End Carousel -->
			</div><!--/row-->
			

			<div class="tag-box tag-box-v2">
				<p>C Simplify IT transportation management system (TMS) helps companies move freight from origin to destination efficiently, reliably, and cost effectively.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
      
     
 
</div><!--/wrapper-->
    </asp:Content>
<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"  %>
<asp:Content ID="ContactHead" ContentPlaceHolderID="HeadContent" runat="server">
    <title>Contact Us | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="ContactMaincontent" ContentPlaceHolderID="MainContent" runat="server">
	<div class="wrapper">
		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left"> Contacts</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.aspx">Home</a></li>
					<li class="active">Contacts</li>
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		<!-- Google Map -->
		<div id="map" class="map">
		</div><!---/map-->
		<!-- End Google Map -->

		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row margin-bottom-30">
				<div class="col-md-9 mb-margin-bottom-30">
					<div class="headline"><h2>Contact Form</h2></div>
					<p class="justify">We Solve Customers Issues in minimum time & to make you keep information handy. Use Web, Calls and WhatsApp ! to communicate and Send Important Informations. </p><br />

					<form action="assets/php/sky-forms-pro/demo-contacts-process.php" method="post" id="sky-form3" class="sky-form contact-style">
						<fieldset class="no-padding">
							<label>Name <span class="color-red">*</span></label>
							<div class="row sky-space-20">
								<div class="col-md-7 col-md-offset-0">
									<div>
										<input type="text" name="name" id="name" class="form-control">
									</div>
								</div>
							</div>

							<label>Email <span class="color-red">*</span></label>
							<div class="row sky-space-20">
								<div class="col-md-7 col-md-offset-0">
									<div>
										<input type="text" name="email" id="email" class="form-control">
									</div>
								</div>
							</div>

							<label>Message <span class="color-red">*</span></label>
							<div class="row sky-space-20">
								<div class="col-md-11 col-md-offset-0">
									<div>
										<textarea rows="8" name="message" id="message" class="form-control"></textarea>
									</div>
								</div>
							</div>

							<p><button type="submit" class="btn-u">Send Message</button></p>
						</fieldset>

						<div class="message">
							<i class="rounded-x fa fa-check"></i>
							<p>Your message was successfully sent!</p>
						</div>
					</form>
				</div><!--/col-md-9-->

				<div class="col-md-3">
					<!-- Contacts -->
					<div class="headline"><h2>Contacts</h2></div>
					<ul class="list-unstyled who margin-bottom-30">
						<li><a href="#"><i class="fa fa-home"></i>C Simplify IT Services Private Limited <br />&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  Plot No. 52, II Floor, Sector 32,<br/>&nbsp; &nbsp;&nbsp;&nbsp;&nbspGurgaon, Haryana ( India ).</a></li>
						<li><a href="#"><i class="fa fa-envelope"></i>sales@csimplifyit.com</a></li>
						<li><a href="#"><i class="fa fa-phone"></i>+91 98999 76227</a></li>
						<li><a href="#"><i class="fa fa-globe"></i>http://www.csimplifyit.com</a></li>
					</ul>

					<!-- Business Hours -->
					<div class="headline"><h2>Office Hours</h2></div>
					<ul class="list-unstyled margin-bottom-30">
						<li><strong>Monday-Saturday:</strong> 10am to 8pm</li>
						<li><strong>Sunday:</strong> Closed</li>
					</ul>

					
				</div><!--/col-md-3-->
			</div><!--/row-->

			
		</div><!--/container-->
        <!-- End Services Section -->
			<!-- Owl Clients v1 -->
            
                <div class="col-md-12 clients-section parallaxBg">
                    <div class="container">
                        <div class="title-v1">
                            <h2>Our Clients</h2>
                        </div>
                       <ul class="owl-clients-v2">
                            <li class="item"><a href="http://www.myvestige.com/" target="_blank"><img src="assets/img/clients/vestige-logo.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="https://paytm.com/" target="_blank"><img src="assets/img/clients/paytm.png" alt=""/></a></li>
                            <li class="item"><a href="http://www.apllvascor.com/" target="_blank"><img src="assets/img/clients/apl-logo.png" alt=""/></a></li>
                             <li class="item"><a href="https://bondevalue.com/" target="_blank"><img src="assets/img/clients/bondevalue.png" alt=""/></a></li>
							<li class="item"><a href="http://transorg.com/" target="_blank"><img src="assets/img/clients/transorg.png" alt=""/></a></li>
                            <li class="item"><a href="http://www.askmebazaar.com/"><img src="assets/img/clients/askmebazaar.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="https://www.vinculumgroup.com/"><img src="assets/img/clients/vinculum.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="http://www.jardines.com/" target="_blank"><img src="assets/img/clients/jardine.jpeg" alt=""/></a></li>
                            <li class="item"><a href="http://www.aswatson.com/" target="_blank"><img src="assets/img/clients/aswatson.png" alt=""/></a></li>
                             <li class="item"><a href="https://www.priorityvendor.com/priorityvendor/handleGeneral.do"><img src="assets/img/clients/priority-vendor.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="http://www.urdoorstep.com/" target="_blank"><img src="assets/img/clients/ur-doorstep.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="http://www.vskills.in/practice/vSkillsHome"><img src="assets/img/clients/vskills.jpg" alt="" target="_blank"/></a></li>                            
                             <li class="item"><a href="http://www.dhl.co.in/en.aspx" target="_blank"><img src="assets/img/clients/dhl.png" alt=""/></a></li>
                            <li class="item"><a href="http://coldex.in/" target="_blank"><img src="assets/img/clients/coldex.png" alt="" target="_blank"/></a></li>
                            <li class="item"><a href="http://www.motherson.com/motherson-sumi-systems-limited.aspx"><img src="assets/img/clients/motherson.jpg" alt="" target="_blank"/></a></li>
                            
                        </ul>
                    </div>
                  </div>
            

			<!-- End Owl Clients v1 -->
            
        
		<!-- End Content Part -->
		<!--=== End Content Part ===-->

		

	</div><!--/wrapepr-->
    <script type="text/javascript" src="assets/js/forms/contact.js"></script>
	<script type="text/javascript" src="assets/js/pages/page_contacts.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            //App.initCounter();
            //App.initParallaxBg();
            //LayerSlider.initLayerSlider();
            ContactPage.initMap();
            ContactForm.initContactForm();
            OwlCarousel.initOwlCarousel();
            StyleSwitcher.initStyleSwitcher();
            //ParallaxSlider.initParallaxSlider();
            //ProgressBar.initProgressBarVertical();
            //new WOW().init();
        });

       

    </script>
   </asp:Content>
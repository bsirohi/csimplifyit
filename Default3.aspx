<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
	CodeBehind="Default.aspx.cs" Inherits="csimplifyit._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
   <script>
     $(function () {
         $('#slides').slides({
             preload: true,
             preloadImage: 'img/loading.gif',
             play: 5000,
             pause: 2500,
             hoverPause: true



         });
     });
	</script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">


   <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->
   <hr />	
   	
    <h2 class="text_blue">Simplify your Apps with our "Context Aware and Maintenance FREE! solutions" for red hot startups, ever greens and blue chips - C SimplifyIT (India's IT Company for Mobile and Workflow Apps)</h2>
		<div class="one_half"><!--start top half content-->
		<h3>"C" represents hotness, speed of light, and part of DNA.</h3>
		<p> C Simplify IT Services helps its Customers increase their customer engagement, brand recognition, leads and sales.</p>

		<p>If you are in a business which is constantly changing and  looking to advertise and connect with audiences on world class mobile solutions and drive consumer 
		impact and engagement through context aware apps, then C Simplify IT is place to start.</p> 
        
		<p>Right first time and cycle time, are two simple things, which drives all businesses and improves business results. At CSimplifyIT we believe this is  'what should be focus' to improve your web/mobile presence. The competitive environment with increasing customer demands for innovation and higher value, is transforming the way businesses operate across industries. Organizations are looking for transformation to improve intimacy with customers, productivity, efficiency, market share, and profitability .When we together control ‘Right first time’ and 'cycle time', we can create magic for our Customers!</p>
		
	</div>
	<div class="one_half column-last">
		<div class="fancy"><a id="fancybox" href="images/Our_Focus.png"><img alt="fancybox" style="height:370px; width:100%;" src="images/Our_Focus.png" /></a></div>
	</div>
	<!--end top half content-->

	<div class="clearfix"></div>

	<div>
		<p>We have learned how to interpret our customers’ needs and wants. We are passionate about exploiting technology and deploying intelligent business practices to meet the challenges faced by today’s services industry. We build IT systems which are Simple, Intelligent and which gel seamlessly with existing applications and IT landscape.</p>

		<p>At "C Simplify IT" we have created a company focused around a growing number of highly skilled individuals who are expert in delivering well designed Publisher and Advertiser solutions. Using excellent 'intelligent' business practices and best of breed technologies we are able to deliver short term results that support long term goals. </p>
	</div>
	<div class="one_fourth"><strong><span class="bigtext_25">We will partner with you to:</span></strong></div>
	  <div class="three_fourth column-last">
		<ul>
		<li><span>reach a highly engaged audience, in contextually relevant environments with best-in-class "context aware" solutions</span>
		</li><li><span>offer you a combination of great mobile and websites, superior targeting</span>
		</li><li><span>optimize the placements of your ads where they will be performing best and driving results</span>
		</li><li><span>identify the optimal mix of channels to attract the relevant target audience for online campaigns</span>
		</li><li><span>Improve 'intelligence' in your business practices</span>
		</li><li><span>Extend the capability of yourOperations,  Marketing and IT departments</span>
		</li><li><span>Implement solutions that help underpin your business</span> </li></ul></div>
        <div class="clear"></div>
		<p>
		<strong>We offer you a low-risk, fixed price contract, based on fully scoped deliverables. We will definitely make things simple and build a strong case for long 
		term associations. </strong>In "C SImpify IT" we have created a company focused around a growing number of highly skilled individuals who are expert in delivering
		 well designed solutions. Using excellent 'intelligent' business practices and best of breed technologies we are able to deliver short term results that support long 
		 term development goals.</p>
		<div class="one_half">
		
		<strong>We will partner with you to:</strong>

<ul>
	<li><strong>Suggest Mobile/Web solutions to exploit your existing systems</strong></li>
	<li><strong>Improve 'intelligence' in your business practices</strong></li>
	<li><strong>Extend the capability of your IT department</strong></li>
	<li><strong>Implement solutions that help underpin your business</strong></li>
	<li><strong>Bring down operations costs through maintenance FREE Apps</strong></li>
</ul> 




<p class="text_blue"><strong>We offer you a low-risk, fixed price contract, based on fully scoped deliverables. We will definitely make things simple and build a strong case for 
long term associations.</strong></p>
  <p>
	 Making a difference by simple but intelligent changes' is important to us. We are highly motivated and enthusiastic, and we feel passinate and proud about what we do!
 Working in partnership with our customers we help them extend their reach into an ever changing competitive marketplace. We continually seek to add value through
  transformation initiatives and high growth models.<br />Our customers entrust us to due to our simple business models and deliveries on time and within budget.

		
		</p>
		  

</div>


		<div class="one_half last">
		<div class="fancy"><a id="fancybox" href="images/Services-Snapshot.PNG"><img style="height:400px; width:100%;" alt="fancybox"src="images/Services-Snapshot.PNG" /></a></div>
	   
	</div>
	</div>
  
	  
</asp:Content>
	
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="fly_high.aspx.cs" Inherits="csimplifyit.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script>
    $(function () {
        $('#slides').slides({
            preload: true,
            preloadImage: 'img/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true,
            start: 3   //no of slide to be displayed 

        });
    });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div id="body-wrapper" class="clearfix"><!--start body-wrapper-->

 <div class="clear"></div>
 <hr />
<h1 class="text_blue">Fly high! - We Simplify</h1>
 <div class="clear"></div>
    <div class="one_half"><img class="radius_10" src="images/engageus.png" /></div> 
        <div class="one_half column-last">
        <p class="bigtext_25">You get <span class="text_blue">12 Months Maintenance Free Services</span> on all Apps developed by us. This policy helps cut costs and have peace of mind, for our Customers. We have listened to them and they said, gain our trust by showing your expertise! if you can.</p>
        <p>Email us @ <a class="text_blue" href="mailto:Honey.Sachdeva@CSimplifyIT.com">Honey.Sachdeva@CSimplifyIT.com</a>
or Skype @<span class="text_blue"> CSimplifyIT</span></p>
<h2 class="text_blue">Here is what we do</h2>
        <ol>
	        <li>Adaptive designs e.g. early feedback is important but how many times?</li>
	        <li>Learning by nature e.g. snakes live in sea, sand, trees, under ground but they do not have legs, then how they became that versatile?</li>
	        <li>Resilience e.g. persistence ensures learnings survive and evolve, matches Darwin's  "survival of fittest"</li>
	        <li>Experience e.g. Skillful  speedy sensible simple designs </li>
	        <li>Clear Crisp Communication e.g. between humans and systems, increases performance and cut costs</li>
	        <li>Risk mitigation through expert intervention e.g. we spend more on R&D when risks appear, similar to producing drugs for new diseases</li>
        </ol>
        </div> 
</div>
</asp:Content>

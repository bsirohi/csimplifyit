﻿<!DOCTYPE html>

<html lang="en-us">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <title>Getting Started - Simple Mass Mail Merger</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/just-the-docs.css">

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="assets/js/vendor/lunr.min.js"></script>


    <script type="text/javascript" src="assets/js/just-the-docs.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .installBtn {
            margin-left: 493px;
            font-size: 15px;
            background-color: cornflowerblue;
            color: white;
            padding: 10px;
            border: red;
            border-radius: 3px;
            PADDING-TOP: 1PX;
            padding-bottom: 1px;
            height: 31PX;
        }
    </style>
</head>


<div class="page-wrap">
    <div class="side-bar">
        <a href="https://csimplifyit.com/" class="site-title fs-6 lh-tight">C Simplify IT</a>
        <span class="fs-3">
            <button class="js-main-nav-trigger navigation-list-toggle btn btn-outline" type="button" data-text-toggle="Hide">Menu</button></span>
        <div class="navigation main-nav js-main-nav">
            <nav>
                <ul class="navigation-list">

                    <li class="navigation-list-item active">
                        <a href="SimpleMassMailMerge.aspx" class="navigation-list-link active">Getting Started</a>
                    </li>

                    <li class="navigation-list-item">
                        <a href="docs/features/index.html" class="navigation-list-link">Features</a>
                    </li>

                    <li class="navigation-list-item">
                        <a href="docs/tipsntricks/index.html" class="navigation-list-link">Tips & Tricks</a>
                    </li>

                    <li class="navigation-list-item">
                        <a href="docs/benefits/index.html" class="navigation-list-link">Benefits</a>
                    </li>
                     <li class="navigation-list-item">
                   <a href="../Strip_Payment_way/StripePayment.aspx?id=1" class="navigation-list-link">Price</a>
                 </li>

                    <li class="navigation-list-item">
                        <a href="docs/privacypolicy/index.html" class="navigation-list-link">Privacy Policy</a>
                    </li>

                </ul>
            </nav>

        </div>
        <footer role="contentinfo" class="site-footer">
            <p class="text-small text-grey-dk-000 mb-0">This site is created by <a href="https://csimplifyit.com/">C Simplify IT</a>, for Simple Mass Mail Merge.</p>
        </footer>
    </div>
    <div class="main-content-wrap">
        <div class="page-header">
            <div class="main-content">

                <div class="search js-search">
                    <div class="search-input-wrap">
                        <input type="text" class="js-search-input search-input" placeholder="Search Manual" aria-label="Search Manual" autocomplete="off">
                        <svg width="14" height="14" viewBox="0 0 28 28" xmlns="http://www.w3.org/2000/svg" class="search-icon">
                            <title>Search</title>
                            <g fill-rule="nonzero">
                                <path d="M17.332 20.735c-5.537 0-10-4.6-10-10.247 0-5.646 4.463-10.247 10-10.247 5.536 0 10 4.601 10 10.247s-4.464 10.247-10 10.247zm0-4c3.3 0 6-2.783 6-6.247 0-3.463-2.7-6.247-6-6.247s-6 2.784-6 6.247c0 3.464 2.7 6.247 6 6.247z" />
                                <path d="M11.672 13.791L.192 25.271 3.02 28.1 14.5 16.62z" />
                            </g></svg>
                    </div>
                    <div class="js-search-results search-results-wrap"></div>
                </div>


            </div>
        </div>
        <div class="main-content">

            <div class="page-content">
                <h2 id="welcome-to-digiquote-pro"><strong>Simple Mass Mail Merge</strong>
                    <button class="installBtn"  onclick="window.open('https://gsuite.google.com/marketplace/app/simple_mass_mail_merge/1087023983878','_blank')" >Install Now</button>
                </h2>


                <p>The simplest way to send your own configured, personalised email campaigns hassle-free from your gmail account.</p>

                <p><strong>Simple Mass Mail Merge helps in following key process:</strong></p>
                <img src="assets\images\gettingstarted.png" alt="Getting Started">


                <div class="features">
                    <h2 style="text-align: center; padding-top: 12px;">Features:</h2>
                    <div class="row">
                        <div class="col-md-3" style="border-right: 1px solid;">
                            <div class="features_li">
                                <i class="fa fa-users" aria-hidden="true" style="color: #0089ff;"></i>
                                <p>Using pre-defined template</p>
                            </div>
                        </div>
                        <div class="col-md-3" style="border-right: 1px solid;">
                            <div class="features_li">
                                <i class="fa fa-pencil-square" aria-hidden="true" style="color: green;"></i>
                                <p>Testing a configured campaign</p>
                            </div>
                        </div>
                        <!-- </div>
            <div class="row"> -->
                        <div class="col-md-3" style="border-right: 1px solid;">
                            <div class="features_li">
                                <i class="fa fa-file-text" aria-hidden="true" style="color: orange;"></i>
                                <p>Creating data-sheet automatically</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="features_li">
                                <i class="fa fa-comments" aria-hidden="true" style="color: red;"></i>
                                <p>Track your campaign</p>
                            </div>
                        </div>
                    </div>
                </div>

                <p><strong>Steps:</strong></p>
                <div class="steps">
                    <div class="steps_process">

                        <div class="row odd_steps">
                            <div class="col-md-1 steps_gif">
                                <img src="assets\images\coldcalling.gif" alt="cold calling" />
                            </div>
                            <div class="col-md-11">
                                <h2>Step 1:</h2>
                                <p>Go to the Google Spreadsheet, click the Add-ons menu and you’ll see a new menu called SM3. Click on Configure Campaign. A popup will appear on the right side of the sheet. Prepare the source data, refer to “How to prepare data” mentioned below these steps.</p>
                            </div>
                        </div>

                        <div class="row even_steps">
                            <div class="col-md-1 steps_gif">
                                <img src="assets\images\research.gif" alt="background check" />
                            </div>
                            <div class="col-md-11">
                                <h2>Step 2:</h2>
                                <p>On the first page you’ll get an option to choose the email template (The drafts in your Gmail) and attachment template (Only one Doc from google drive). There is also an option to create a draft from our add-on or you can click on the search icon on the top to choose from our free templates.</p>
                            </div>
                        </div>

                        <div class="row odd_steps">
                            <div class="col-md-1 steps_gif">
                                <img src="assets\images\arrangeMeeting.gif" alt="arrange a meeting" />
                            </div>
                            <div class="col-md-11">
                                <h2>Step 3:</h2>
                                <p>After selecting the templates click on “Next” to move to the next screen to choose the source data sheet. Select the lead worksheet and then choose the datasheet (by default sheet1). Also you have to provide the email column present in the datasheet which consist of email addresses of all the recipients.</p>
                            </div>
                        </div>

                        <div class="row even_steps">
                            <div class="col-md-1 steps_gif">
                                <img src="assets\images\customerInputs.gif" alt="get customer input" />
                            </div>
                            <div class="col-md-11">
                                <h2>Step 4:</h2>
                                <p>When you run SM3, it will send all the mails immediately. However, you also have an option to schedule emails and the program will automatically send them later. To schedule an email, go to the Scheduled Date column and insert the date and time when you wish to schedule that particular email. Use the dd/mm/yyyy hh:mm format.</p>
                            </div>
                        </div>

                        <div class="row odd_steps">
                            <div class="col-md-1 steps_gif">
                                <img src="assets\images\draftSolution.gif" alt="generate Draft Solution" />
                            </div>
                            <div class="col-md-11">
                                <h2>Step 5:</h2>
                                <p>Click on Send.</p>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="setup_section">
                    <div class="row">
                        <div class="col-md-7" style="border-right: 1px solid;">
                            <h2>Setup Simple Mass Mail Merge</h2>
                            <div style="margin-top: 1rem;">
                                <h3>Install the Google Add-on</h3>
                                <p>1. Install SM3 add-on from the Google store. The add-on is compatible with all browsers and only requires a Gmail account.</p>
                            </div>
                            <div style="margin-top: 1rem;">
                                <h3>Create Gmail Draft as the merge template</h3>
                                <p>1. Go to your Gmail or Google Inbox account and create a new draft message. You can include one or more variable fields in the email message using the {{FIELDNAME}} notation and these will be replaced with the actual values from the sheet when the emails are sent.</p>
                                <p>2. Say you want to send an email to a group where the content of the message body is mostly similar except a few fields like salutation, first name and city that will be unique for each message. What you need to do is add columns in the sheet (Created in Google Sheets) for each of these variable fields. And in your Gmail Draft, you can refer to these variable fields as {{First Name}}, {{Company Name}} and so on.</p>
                            </div>
                            <div style="margin-top: 1rem;">
                                <h3>Run SM3</h3>
                                <p>1. Add the source data in a Google Sheet and follow the wizard to run SM3. One email would be sent for each row in the sheet.</p>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <h2>Quick Navigation Guide</h2>
                            <div class="quick_nav">
                                <a href="docs/tipsntricks/#tipsNTricks1" style="white-space: unset;">Creating Gmail Template for Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <br />
                                <br />
                                <a href="docs/tipsntricks/#tipsNTricks2" style="white-space: unset;">Creating Dynamic Google Documents  using Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <br />
                                <br />
                                <a href="docs/tipsntricks/#tipsNTricks3" style="white-space: unset;">How to create Google Sheet for Gmail templates and Google Docs using Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <br />
                                <br />
                                <a href="docs/tipsntricks/#tipsNTricks4" style="white-space: unset;">How to read Usage Report generated by Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <br />
                                <br />
                                <a href="docs/tipsntricks/#tipsNTricks5" style="white-space: unset;">How to save dynamically generated documents using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <br />
                                <br />
                                <a href="docs/tipsntricks/#tipsNTricks6" style="white-space: unset;">How to TEST a Configured Campaign  using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <br />
                                <br />
                                <a href="docs/tipsntricks/#tipsNTricks7" style="white-space: unset;">How to use predefined templates (GMail and Google Docs) using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="third_section">
            
          </div> -->


            </div>
        </div>
    </div>
</div>
</html>


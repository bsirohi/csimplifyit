﻿<!DOCTYPE html>

<html lang="en-us">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">

  <title>Getting Started - Simple Workflow Manager</title>
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/googleworkflow/assets/css/just-the-docs.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" src="/assets/js/vendor/lunr.min.js"></script>
  
  <script type="text/javascript" src="/googleworkflow/assets/js/just-the-docs.js"></script>
     <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "HowTo",
            "name": "How to Workflow Automation Google Suite",
            "step":[
                {
                    "@type": "HowToStep",
                    "name": "Install Simple Workflow Manager for GSuite",
                    "text": "Navigate to GSuite Marketplace and search for Simple Workflow Manager. Click on Install Now button.",
                    "url": "https://gsuite.google.com/marketplace/app/simple_workflow_manager/444419522558"
                },
          {
                    "@type": "HowToStep",
                    "name": "Identify Workflow Trigger",
                    "text": "You can select from 4 different types of workflow trigger, 1. On Google form submit, 2. On Gmail arrival, 3. At specific time, 4. Using google sheet.",
                    "url": "https://csimplifyit.com/googleworkflow/SimpleWorkflowManager.aspx"
                },
                {
                    "@type": "HowToStep",
                    "name": "Define and save workflow steps",
                    "text": "You can define at each workflow step, the participant, document to be generated and email to be sent. Dynamic replacement of {{Tags}} is supported in Email and Document generation. You can also define escalation path when participant fails to take action in a defined time frame.",
                    "url": "https://csimplifyit.com/googleworkflow/SimpleWorkflowManager.aspx"
                },
                {
                    "@type": "HowToStep",
                    "name": "Run your workflow",
                    "text": "Click Run button and sentbox in Gmail. Click on link provided to complete the task.",
                    "url": "https://csimplifyit.com/googleworkflow/SimpleWorkflowManager.aspx"
                },
                {
                    "@type": "HowToStep",
                    "name": "Review running workflows using inbox",
                    "text": "You can track history of your workitems and terminate any workitem at any time.",
                    "url": "https://csimplifyit.com/googleworkflow/SimpleWorkflowManager.aspx"
                },
                {
                    "@type": "HowToStep",
                    "name": "(Optional) Define routiong condition between steps",
                    "text": "For complex workflows you may define routing conditions to skip steps and/or change participants dynamically.",
                    "url": "https://csimplifyit.com/googleworkflow/SimpleWorkflowManager.aspx"
                }
            ]
        }
    </script>
  <meta name="viewport" content="width=device-width, initial-scale=1">


    <style>
     .installBtn {
            float: right;
            font-size: 13px;
            background-color: cornflowerblue;
            color: white;
            padding: 10px;
            border: red;
            border-radius: 3px;
            PADDING-TOP: 1PX;
            padding-bottom: 1px;
            height: 31PX;
        }

    </style>
  <!-- <style type="text/css">

nav ul li a:not(:only-child):after, nav ul li a:visited:not(:only-child):after {
  padding-left: 4px;
  content: ' ▾';
}
.nav-dropdown {
  position: absolute;
  display: none;
  z-index: 1;
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.15);
  /* Mobile navigation */
}
.nav-mobile {
  display: none;
  position: absolute;
  top: 0;
  right: 0;
  background: #262626;
  height: 70px;
  width: 70px;
}
a:not([class]) {
    text-decoration: none;
    background-image: unset !important;
    background-repeat: repeat-x;
    background-position: 0 100%;
    background-size: 1px 1px;
}
.nav-dropdown {
    background: #cecece;
    padding-left: 0;
    padding: 15px;
}
.nav-dropdown li {
    list-style: none;
}
.nav-dropdown li a:hover {
    color: #0056b3;
    text-decoration: underline;
}
@media only screen and (max-width: 798px) {
  
  .nav-dropdown {
    position: static;
  }
}


  </style>
  <script type="text/javascript">(function($) { // Begin jQuery
  $(function() { // DOM ready
    // If a link has a dropdown, add sub menu toggle.
    $('nav ul li a:not(:only-child)').click(function(e) {
      $(this).siblings('.nav-dropdown').toggle();
      // Close one dropdown when selecting another
      $('.nav-dropdown').not($(this).siblings()).hide();
      e.stopPropagation();
    });
    // Clicking away from dropdown will remove the dropdown class
    $('html').click(function() {
      $('.nav-dropdown').hide();
    });
    // Toggle open and close nav styles on click
    $('#nav-toggle').click(function() {
      $('nav ul').slideToggle();
    });
    // Hamburger to X toggle
    $('#nav-toggle').on('click', function() {
      this.classList.toggle('active');
    });
  }); // end DOM ready
})(jQuery); // end jQuery</script> -->
</head>


  <div class="page-wrap">
    <div class="side-bar">
      <a href="" class="site-title fs-6 lh-tight">C Simplify IT</a>
      <span class="fs-3"><button class="js-main-nav-trigger navigation-list-toggle btn btn-outline" type="button" data-text-toggle="Hide">Menu</button></span>
      <div class="navigation main-nav js-main-nav">
        <nav>
          <ul class="navigation-list">

          <li class="navigation-list-item active">
            <a href="SimpleWorkflowManager.aspx" class="navigation-list-link active">Getting Started</a>
          </li>
         <!--  <li>
            <a href="#!">Services</a>
            <ul class="nav-dropdown">
              <li>
                <a href="#!">Web Design</a>
              </li>
              <li>
                <a href="#!">Web Development</a>
              </li>
              <li>
                <a href="#!">Graphic Design</a>
              </li>
            </ul>
          </li> -->
          <!-- <li class="navigation-list-item">
            <a href="/docs/stepbystep/" class="navigation-list-link">Step by Step Guide</a>
          </li> -->
          <li class="navigation-list-item">
            <h5>Step by Step Guide</h5>
            <ul>
              <li>
                <a href="docs/install/" class="navigation-list-link">Install</a>
              </li>
              <li>
                <a href="docs/createyourfirstworkflow/" class="navigation-list-link">Create your First Workflow</a>
              </li>
              <li>
                <a href="docs/trackingworkitems/" class="navigation-list-link">Tracking Work Items</a>
              </li>
              <li>
                <a href="docs/understandingmyaccount/" class="navigation-list-link">Understanding my Account</a>
              </li>
            </ul>
          </li>

          <li class="navigation-list-item">
            <h5>Trigger</h5>
            <ul>
              <li>
                <a href="docs/initiateonformsubmission/" class="navigation-list-link">Initiate on Form Submission</a>
              </li>
              <li>
                <a href="docs/initiateusinggooglesheet/" class="navigation-list-link">Initiate Using this Google Sheet</a>
              </li>
              <li>
                <a href="docs/initiatefromemail/" class="navigation-list-link">Initiate from Email</a>
              </li>
              <li>
                <a href="docs/scheduletimebasedinitiation/" class="navigation-list-link">Schedule Time Based Initiation</a>
              </li>
            </ul>
          </li>

          <li class="navigation-list-item">
            <a href="docs/features/" class="navigation-list-link">Features</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/tipsntricks/" class="navigation-list-link">Tips & Tricks</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/benefits/" class="navigation-list-link">Benefits</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/contactingsupport/" class="navigation-list-link">Contacting Support</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/upgrademyplan/" class="navigation-list-link">Upgrade my Plan</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/faqs/" class="navigation-list-link">FAQs</a>
          </li>

           <li class="navigation-list-item">
            <a href="../Strip_Payment_way/StripePayment.aspx?id=3" class="navigation-list-link">Price</a>
          </li>

          <li class="navigation-list-item">
            <a href="docs/privacypolicy/" class="navigation-list-link">Privacy Policy</a>
          </li>

  </ul>
</nav>

      </div>
      <footer role="contentinfo" class="site-footer">
        <p class="text-small text-grey-dk-000 mb-0">This site is created by <a href="http://csimplifyit.com/">C Simplify IT</a>, for Simple Workflow Manager.</p>
      </footer>
    </div>
    <div class="main-content-wrap">
      <div class="page-header">
        <div class="main-content">
          
          <div class="search js-search">
            <div class="search-input-wrap">
              <input type="text" class="js-search-input search-input" placeholder="Search Manual" aria-label="Search Manual" autocomplete="off">
              <svg width="14" height="14" viewBox="0 0 28 28" xmlns="http://www.w3.org/2000/svg" class="search-icon"><title>Search</title><g fill-rule="nonzero"><path d="M17.332 20.735c-5.537 0-10-4.6-10-10.247 0-5.646 4.463-10.247 10-10.247 5.536 0 10 4.601 10 10.247s-4.464 10.247-10 10.247zm0-4c3.3 0 6-2.783 6-6.247 0-3.463-2.7-6.247-6-6.247s-6 2.784-6 6.247c0 3.464 2.7 6.247 6 6.247z"/><path d="M11.672 13.791L.192 25.271 3.02 28.1 14.5 16.62z"/></g></svg>
            </div>
            <div class="js-search-results search-results-wrap"></div>
          </div>
          
          
        </div>
      </div>
      <div class="main-content">

        <div class="page-content">
          <h2 id="welcome-to-digiquote-pro"><strong>Simple Workflow Manager</strong><button class="installBtn" onclick="window.open('https://gsuite.google.com/marketplace/app/simple_workflow_manager/444419522558', '_blank')">Install Now</button></h2>

          <p>Simple Workflow Manager is a simple, free and powerful Google Sheet Add-On to manage your Google form responses or Google sheet data into automated workflows which make approvals and dynamic document generation in workflows super simple.</p>
          <p>Simple Workflow Manager helps you manage any workflow with-in Google products using following features:-</p>
          <ul>
            <li>Quickly create and assign workflows to your team using Google Sheet or using individual emails.</li>
            <li>Generate and get approvals on dynamic documents based on user inputs in workflows.</li>
            <li>Use Google forms to collect structured data for your teams in required flow.</li>
            <li>Track activity from the dashboard and get notified when tasks are done.</li>
          </ul>

          <p><strong>Simple Workflow Manager is as simple as:</strong></p>
          <img src="assets\images\swm_fd.png" alt="Getting Started">
          <p><strong>Steps:</strong></p>
          <div class="steps">
            <div class="steps_process">
            
            <div class="row odd_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\first.gif" alt="cold calling" />
              </div>
              <div class="col-md-11">
                <h2>Step 1:</h2>
                <p>Navigate to GSuite Marketplace and search for Simple Workflow Manager. Click on Install Now button.</p>
              </div>
            </div> 

            <div class="row even_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\second.gif" alt="background check" />
              </div>
              <div class="col-md-11">
                <h2>Step 2:</h2>
                <p>You can select from 4 different types of workflow trigger, 1. On Google form submit, 2. On Gmail arrival, 3. At specific time, 4. Using google sheet.</p>
              </div>
            </div> 

            <div class="row odd_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\third.gif" alt="arrange a meeting" />
              </div>
              <div class="col-md-11">
                <h2>Step 3:</h2>
                <p>You can define at each workflow step, the participant, document to be generated and email to be sent. Dynamic replacement of {{Tags}} is supported in Email and Document generation. You can also define escalation path when participant fails to take action in a defined time frame.</p>
              </div>
            </div> 

            <div class="row even_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\fourth.gif" alt="get customer input" />
              </div>
              <div class="col-md-11">
                <h2>Step 4:</h2>
                <p>Click Run button and sentbox in Gmail. Click on link provided to complete the task.</p>
              </div>
            </div> 

            <div class="row odd_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\fifth.gif" alt="generate Draft Solution" />
              </div>
              <div class="col-md-11">
                <h2>Step 5:</h2>
                <p>You can track history of your workitems and terminate any workitem at any time.</p>
              </div>
            </div> 

            <div class="row even_steps">
              <div class="col-md-1 steps_gif">
                <img src="assets\images\fourth.gif" alt="get customer input" />
              </div>
              <div class="col-md-11">
                <h2>Step 6:</h2>
                <p>For complex workflows you may define routing conditions to skip steps and/or change participants dynamically.</p>
              </div>
            </div>

            </div>
          </div>
          
          
          <div class="features">
            <h2 style="text-align: center;padding-top: 12px;">Features:</h2>
            <div class="row">
              <div class="col-md-3" style="border-right: 1px solid;">
                <div class="features_li">
                  <i class="fa fa-flag-checkered" aria-hidden="true" style="color: #0089ff;"></i>
                  <p>Understanding workflow triggers</p>
                </div>
              </div>
              <div class="col-md-3" style="border-right: 1px solid;">
                <div class="features_li">
                  <i class="fa fa-file-photo-o" aria-hidden="true" style="color: green;"></i>
                  <p>Designing workflow</p>
                </div>
              </div>
            <!-- </div>
            <div class="row"> -->
              <div class="col-md-3" style="border-right: 1px solid;">
                <div class="features_li">
                  <i class="fa fa-file-text" aria-hidden="true" style="color: orange;"></i>
                  <p>Creating draft and doc templates</p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="features_li">
                  <i class="fa fa-map-marker" aria-hidden="true" style="color: red;"></i>
                  <p>Track your workflow</p>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="setup_section">
            <div class="row">
              <div class="col-md-7" style="border-right: 1px solid;">
                <h2>Setup Simple Workflow Manager</h2>
                <div style="margin-top: 1rem;">
                  <h3>Install the Google Add-on</h3>
                  <p>1. Install Simple Workflow Manager add-on from the Google store. The add-on is compatible with all browsers and only requires a Gmail account.</p>
                </div>
                <div style="margin-top: 1rem;">
                  <h3>Create Job Sheet</h3>
                  <p>1. Create Job sheet either manually or using "(Optional) Create Job sheet" link present in the right popup to create jobsheet manually.</p>
                  <p>2. Fill all the necessary columns.</p>
                  <p>To add links of resume in the "Resume" column of sheet - you have to click on "Add-ons" option present in the menu bar then "Simple Workflow Manager" after that "Add Resume from Drive", popup appears. Select Resume from your drive and it will be automatically added in the first blank cell in Resume column.</p>
                </div>
                <div style="margin-top: 1rem;">
                  <h3>Run Simple Workflow Manager</h3>
                  <p>1. Either use our default template for description or edit your own template using the Edit button in "Event Template Settings". Then, click on "Next" button.</p>
                  <p>2. Check your event status, then click on "Execute".</p>
                </div>         
              </div>

              <div class="col-md-5">
                <h2>Quick Navigation Guide</h2>
                <div class="quick_nav">
                  <a href="/docs/tipsntricks/#tipsNTricks1" style="white-space: unset;">Creating Gmail Template for Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="/docs/tipsntricks/#tipsNTricks2" style="white-space: unset;">Creating Dynamic Google Documents  using Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="/docs/tipsntricks/#tipsNTricks3" style="white-space: unset;">How to create Google Sheet for Gmail templates and Google Docs using Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="/docs/tipsntricks/#tipsNTricks4" style="white-space: unset;">How to read Usage Report generated by Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="/docs/tipsntricks/#tipsNTricks5" style="white-space: unset;">How to save dynamically generated documents using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="/docs/tipsntricks/#tipsNTricks6" style="white-space: unset;">How to TEST a Configured Campaign  using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a><br/><br/>
                  <a href="/docs/tipsntricks/#tipsNTricks7" style="white-space: unset;">How to use predefined templates (GMail and Google Docs) using  Simple Mass Mail Merge <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>  
          </div>  --> 
          <!-- <div class="third_section">
            
          </div> -->
          
          
        </div>
      </div>
    </div>
  </div>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace csimplifyit.Model
{
    public class Stripe
    {
    }

    public class getSubscription
    {
        public string planName { get; set; }
        public string planDescription { get; set; }
        public string planFeaturesCode { get; set; }
        public string DiscountValue { get; set; }
        public string ActualValue { get; set; }      
        public string spid { get; set; }
        public string spsId { get; set; }
        public int Producttype { get; set; }
    }

   


    public class getPaidUserSubscription
    {
        public string planName { get; set; }
        public string PlanType { get; set; }
        public string UserName { get; set; }
        public int PlanID { get; set; }
        public int UserSubScriptionID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string producttype { get; set; }
    }



}
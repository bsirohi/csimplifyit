﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace csimplifyit.Model
{
    public class Registration
    {
    }

    public class encr
    {
        public string key { get; set; } = "A!9HHhi%XjjYY4YP2@Nob009X";

    }

    public class UserInfo
    {
        public long userId { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public DateTime? lastLoggedInDate { get; set; }
        public int? userType { get; set; }

    }

    public class FacebookUser
    {
        public long id { get; set; }
        public string email { get; set; }

        public string name
        { get; set; }

        public string first_name
        { get; set; }

        public string last_name
        { get; set; }

        public string gender
        { get; set; }

        public string link
        { get; set; }

        public DateTime updated_time
        { get; set; }

        public DateTime birthday
        { get; set; }

        public string locale
        { get; set; }

        public string picture
        { get; set; }

        public FacebookLocation location
        { get; set; }
    }

    public class FacebookLocation
    {
        public string id
        { get; set; }

        public string name
        { get; set; }
    }


}
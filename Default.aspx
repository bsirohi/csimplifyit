<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>C Simplify IT</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.jpg">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.jpg">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicons/favicon.ico">
    <link rel="manifest" href="assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="assets/img/favicons/mstile-150x150.jpg">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="assets/css/theme.css" rel="stylesheet">
    <script type="text/javascript">

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-63272985-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>


    <!--===============================================-->
    <!--    Fancynav-->
    <!--===============================================-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#landing-page">
                <img src="assets/img/logo.png" alt="C Simplify IT" height="45" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown1">
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#landing-page">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#whatwedo">Solutions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#offerings">Offerings</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#technology">Technologies</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuProduct" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product</a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuProduct">
                            <a class="dropdown-item" href="SM3/SimpleMassMailMerge.aspx">Simple Mass Mail Merge</a>
                            <a class="dropdown-item" href="SRM/SimpleRecruitmentManager.aspx">Simple Recruitment Manager</a>
                            <a class="dropdown-item" href="googleworkflow/SimpleWorkflowManager.aspx">Simple Workflow Manager</a>
                            <a class="dropdown-item" href="SPOS/SimplePointofSale.aspx">Simple Point of Sale</a>
                            <a class="dropdown-item" href="STDGDoc/SimpleTabularDocumentGenerator.aspx">Simple Tabular Document Generator</a>

                        </div>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="Strip_Payment_way/StripePayment.aspx">Pricing</a>
                        </li>
                    <%--</li>--%>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#whyus">Why Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#clients">Our Clients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#contact">Contact Us</a>
                    </li>
                     <li class="nav-item" id="AddUserName">                                                      
            </li>
               <li class="nav-item" id="AddLogOutButton">                                                         
            </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--===============================================-->
    <!--    End of Fancynav-->
    <!--===============================================-->



    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main minh-100vh" id="top">


        <!-- ============================================-->
        <!-- Preloader ==================================-->
        <div class="preloader" id="preloader">
            <div class="loader">
                <div class="line-scale-pulse-out-rapid">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
        <!-- ============================================-->
        <!-- End of Preloader ===========================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <div class="speciality">
            <a href="Our_Specialities.aspx">Our Specialities</a>
        </div>
        <!-- <section> close ============================-->
        <!-- ============================================-->


        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="landing-page py-0 overflow-hidden bg-white" data-zanim-timeline='{"delay":0.4}' data-zanim-trigger="scroll" id="landing-page">

            <div class="container-fluid">
                <div class="devices-wrapper">
                    <div class="macbook-1" data-zanim-xs='{"delay":0.5,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="3" src="assets/img/showcase/compete.jpg" alt="">
                    </div>
                    <div class="ipad--l-1" data-zanim-xs='{"delay":0.5,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="3.8" src="assets/img/showcase/teamwork.jpg" alt="">
                    </div>
                    <div class="iphone-1" data-zanim-xs='{"delay":0.4,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="4.8" src="assets/img/showcase/teamwork1.jpg" alt="">
                    </div>
                    <div class="ipad--l-2" data-zanim-xs='{"delay":0.3,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="3.4" src="assets/img/showcase/aspire.jpg" alt="">
                    </div>
                    <div class="iphone-2" data-zanim-xs='{"delay":0.2,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="4" src="assets/img/showcase/oar.jpg" alt="">
                    </div>
                    <div class="macbook-2" data-zanim-xs='{"delay":0.4,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="5" src="assets/img/showcase/hurdles.jpg" alt="">
                    </div>
                    <div class="ipad--l-3" data-zanim-xs='{"delay":0.5,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="3.7" src="assets/img/showcase/dream.jpg" alt="">
                    </div>
                    <div class="ipad--p-1" data-zanim-xs='{"delay":0.5,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="4.8" src="assets/img/showcase/business.jpg" alt="">
                    </div>
                    <div class="iphone-3" data-zanim-xs='{"delay":0.3,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="3" src="assets/img/showcase/oar.jpg" alt="">
                    </div>
                    <div class="iphone-4" data-zanim-xs='{"delay":0.2,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="3" src="assets/img/showcase/partner.jpg" alt="">
                    </div>
                    <div class="iphone--l-1" data-zanim-xs='{"delay":0.5,"animation":"slide-left","duration":1.5}'>
                        <img class="device parallax" data-rellax-speed="2" src="assets/img/showcase/data.jpg" alt="">
                    </div>
                </div>
                <div class="row align-items-center col-lg-8 px-lg-8 py-8 minh-100vh">
                    <div class="owl-carousel owl-theme owl-nav-outer" data-options='{"dots":false,"nav":true,"items":1,"autoplay":true,"loop":true,"autoplayHoverPause":true}'>
                        <div class="item">
                            <h1 class="display-4 fs-2 fs-sm-4 fs-md-5">challenge</h1>
                            <p class="mt-2 mt-sm-3 text-sans-serif lead">Empowering businesses embrace business challenges through simplified IT Solutions</p>
                        </div>
                        <div class="item">
                            <h1 class="display-4 fs-2 fs-sm-4 fs-md-5">aspire</h1>
                            <p class="mt-2 mt-sm-3 text-sans-serif lead">Making your business aspire for the top with technology that works for you</p>
                        </div>
                        <div class="item">
                            <h1 class="display-4 fs-2 fs-sm-4 fs-md-5">compete</h1>
                            <p class="mt-2 mt-sm-3 text-sans-serif lead">Leave your competition behind with valuable technology innovations</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="text-center bg-light" id="whoweare">

            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-md-12" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                        <div class="col-12 mb-3 mb-md-5 text-center">
                            <h2 class="fs-3 fs-sm-4"><span class="text-underline">who we are?</span></h2>
                        </div>
                        <div class="overflow-hidden">
                            <p class="lead text-sans-serif" data-zanim-xs='{"delay":0.3}'>C Simplify IT based in Gurgaon, India, established in 2011, with a team of 100+ consulting & engineering professionals. We are a specialist IT Consulting Firm, Focused on generating exceptional value for our clients. Our clients are in Singapore, Malaysia, Switzerland, UK, Hong Kong, and India.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section id="betterus">

            <div class="container">
                <div class="col-12 mb-3 mb-md-5 text-center">
                    <h2 class="fs-3 fs-sm-4"><span class="text-underline">what makes us 10x better?</span></h2>
                </div>
                <div class="row justify-content-between">
                    <div class="col-lg-4 px-lg-4">
                        <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.1}' data-zanim-trigger="scroll">
                            <img class="mr-3" src="assets/img/icons/implementation3.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>faster implementation</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>One of our customer, the largest test platforms in India, went from having 0 users to 3000 users/day in just 6 months, after connecting with us.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 px-lg-4 pt-4 pt-lg-0">
                        <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.2}' data-zanim-trigger="scroll">
                            <img class="mr-3" src="assets/img/icons/technology2.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>depth in technology</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>We helped our client, a startup, to develop and launch a complete tech platform which runs entirely on Blockchain & AI.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 px-lg-4">
                        <div class="media text-sans-serif overflow-hidden" data-zanim-timeline='{"delay":0.3}' data-zanim-trigger="scroll">
                            <img class="mr-3" src="assets/img/icons/business2.png" alt="icon" width="50" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>deliver business value</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="mb-0" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>We helped our client's business to go from 15 crores per month to 200 crores per month.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="py-5 py-lg-8 text-center overflow-hidden" id="clientsuccess">

            <div class="bg-holder overlay overlay-0 parallax" style="background-image: url(assets/img/slider3.jpg);" data-rellax-percentage="0.5">
            </div>
            <!--/.bg-holder-->

            <div class="container">
                <div class="col-12 mb-2 mb-md-4 text-center">
                    <h1 class="fs-3 fs-sm-4"><span class="text-underline">client success</span></h1>
                </div>
                <div class="row">
                    <div class="col overflow-hidden">
                        <h2 class="fs-4 fs-md-5 text-white display-4" data-zanim-xs='{"delay":0.1}' data-zanim-trigger="scroll">Integrating mobility solutions for image capture, with enterprise transaction system to bring 10X efficiency gains for a logistics leader.</h2>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="text-center" id="whatwedo">

            <div class="container">
                <div class="col-12 mb-3 mb-md-5 text-center">
                    <h2 class="fs-3 fs-sm-4"><span class="text-underline">delivering solutions which are:</span></h2>
                </div>
                <div class="row justify-content-between">
                    <div class="col-lg-4 px-lg-4" data-zanim-timeline='{"delay":0.1,"animation":"slide-right"}' data-zanim-trigger="scroll">
                        <img class="rounded img-fluid" src="assets/img/processautomation.jpg" alt="" data-zanim-xs='{"delay":0.1,"animation":"zoom-in"}' />
                        <div class="overflow-hidden">
                            <h4 class="mt-4" data-zanim-xs='{"delay":0.2}'>intelligent</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}'>Solutions that improve intelligence through insights into data and content.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 px-lg-4 pt-4 pt-lg-0" data-zanim-timeline='{"delay":0.1,"animation":"slide-right"}' data-zanim-trigger="scroll">
                        <img class="rounded img-fluid" src="assets/img/automation.jpg" alt="" data-zanim-xs='{"delay":0.1,"animation":"zoom-in"}' />
                        <div class="overflow-hidden">
                            <h4 class="mt-4" data-zanim-xs='{"delay":0.2}'>automated</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}'>Process automation that brings consistency and efficiency in business.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 px-lg-4" data-zanim-timeline='{"delay":0.1,"animation":"slide-right"}' data-zanim-trigger="scroll">
                        <img class="rounded img-fluid" src="assets/img/blockchain.jpg" alt="" data-zanim-xs='{"delay":0.1,"animation":"zoom-in"}' />
                        <div class="overflow-hidden">
                            <h4 class="mt-4" data-zanim-xs='{"delay":0.2}'>trusted</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}'>Establishing higher trust in your data & content through blockchain, and security frameworks.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="py-5 py-lg-8 text-center overflow-hidden" id="clientsuccess1">

            <div class="bg-holder overlay overlay-0 parallax" style="background-image: url(assets/img/slider4.jpg);" data-rellax-percentage="0.5">
            </div>
            <!--/.bg-holder-->

            <div class="container">
                <div class="col-12 mb-2 mb-md-4 text-center">
                    <h1 class="fs-3 fs-sm-4"><span class="text-underline">client success</span></h1>
                </div>
                <div class="row">
                    <div class="col overflow-hidden">
                        <h2 class="fs-4 fs-md-5 text-white display-4" data-zanim-xs='{"delay":0.1}' data-zanim-trigger="scroll">Modernising legacy application to a robust web architecture with single central database connecting enterprise channels across 6000 locations, to provide real time order management & confirmation, helping business scale up for the growth.</h2>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section id="offerings">

            <div class="container">
                <div class="col-12 mb-3 mb-md-5 text-center">
                    <h2 class="fs-3 fs-sm-4"><span class="text-underline">our offerings</span></h2>
                </div>
                <div class="row">
                    <div class="col-lg-6 pr-lg-4 mb-4">
                        <div class="media" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="oveflow-hidden">
                                <img class="media-img mr-3 mr-sm-4" src="assets/img/icons/businessanalyst.png" alt="" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            </div>
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 class="mb-2" data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>business analysis & consulting</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Process Design, Business Documentation, Business Acceptance Testing Support, & Technology Architecture.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pr-lg-4 mb-4">
                        <div class="media" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="oveflow-hidden">
                                <img class="media-img mr-3 mr-sm-4" src="assets/img/icons/applicationsupport2.png" alt="" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            </div>
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 class="mb-2" data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>startup incubation & product development</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Rapid Technology Delivery, Concept to Solution Cycle of 3 Months, Fully Supported Operations & Technology Team to run your startup business model.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pr-lg-4 mb-4">
                        <div class="media" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="oveflow-hidden">
                                <img class="media-img mr-3 mr-sm-4" src="assets/img/icons/designdevelopment1.png" alt="" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            </div>
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 class="mb-2" data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>technology application development</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Custom Application Development, Application Support & Maintenance.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pr-lg-4 mb-4">
                        <div class="media" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="oveflow-hidden">
                                <img class="media-img mr-3 mr-sm-4" src="assets/img/icons/cloudsetup3.png" alt="" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            </div>
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 class="mb-2" data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>cloud migration</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Process Design, Business Documentation, Business Acceptance Testing Support, & Technology Architecture.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pr-lg-4 mb-4 mb-lg-0">
                        <div class="media" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="oveflow-hidden">
                                <img class="media-img mr-3 mr-sm-4" src="assets/img/icons/dataextraction1.png" alt="" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            </div>
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 class="mb-2" data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>technology & devOps outsourcing</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>Fully integrated application support & Devops centre dedicated for you.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pr-lg-4">
                        <div class="media" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                            <div class="oveflow-hidden">
                                <img class="media-img mr-3 mr-sm-4" src="assets/img/icons/seo1.png" alt="" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}' />
                            </div>
                            <div class="media-body">
                                <div class="overflow-hidden">
                                    <h4 class="mb-2" data-zanim-xs='{"delay":0.1,"animation":"slide-right"}'>search engine optimization services</h4>
                                </div>
                                <div class="overflow-hidden">
                                    <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2,"animation":"slide-right"}'>If you're struggling we can help you with our SEO service to get more visitors to your site.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->




        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="border-top border-300" id="ourprocess">

            <div class="container">
                <div class="row justify-content-center text-center mb-4">
                    <div class="col-lg-6">
                        <h2 class="fs-3 fs-sm-4 text-underline mb-3">precision crafted solutions</h2>
                        <p class="text-sans-serif lead text-500 font-italic">Our decades of software engineering expertise is reflected through design of our solutions that conform to the highest standards of architectural quality, and a deep passion with which we nurture our engineering capability at C Simplify IT.</p>
                        <hr class="hr-short mt-3 border-300" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pl-lg-7">
                        <div class="row align-items-end">
                            <div class="col-lg-6 order-lg-2 text-center">
                                <img class="img-fluid" src="assets/img/softwareengineer.png" alt="" width="400" />
                            </div>
                            <div class="col-lg-6 border-lg-left border-lg-bottom border-300 pb-lg-8 mt-4 mt-lg-0 mb-4 mb-lg-0">
                                <div class="process-item ml-6 ml-sm-8 ml-lg-6">
                                    <span class="process-item-number">01</span>
                                    <h3>software engineering</h3>
                                    <p class="text-sans-serif lead">Precision in software engineering achieved through our innovative design principles. Our core values of engineering excellence in software development.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 border-lg-right border-lg-bottom border-300 text-center py-lg-4">
                                <img class="img-fluid" src="assets/img/agilediscipline.png" alt="" width="400" />
                            </div>
                            <div class="col-lg-6 mt-4 mt-lg-0 mb-4 mb-lg-0 my-lg-8">
                                <div class="process-item ml-6 ml-sm-8 ml-lg-6">
                                    <span class="process-item-number">02</span>
                                    <h3>driving agile disciplines</h3>
                                    <p class="text-sans-serif lead">Driving agile disciplines for crafting outcomes that bring customer delight. Collaborative solutions implementation upholding agile practices with tangible benefits to clients.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 order-lg-2 text-center align-self-center">
                                <img class="img-fluid" src="assets/img/businessknowledge.png" alt="" width="400" />
                            </div>
                            <div class="col-lg-6 border-lg-left border-300 pt-lg-8 mb-0 mb-lg-0 mt-4 mt-lg-0">
                                <div class="process-item ml-6 ml-sm-8 ml-lg-6">
                                    <span class="process-item-number">03</span>
                                    <h3>business knowledge</h3>
                                    <p class="text-sans-serif lead">Business knowledge that drives real value from Technology Solutions. Consulting model of linking enterprise solutions across sales, operations, finance, and human resources that brings 10X returns on your IT investments.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section id="technology">

            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-12 mb-3 mb-lg-5 text-center">
                        <h2 class="fs-3 fs-sm-4"><span class="text-underline">our technology expertise</span></h2>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center">
                    <div class="col-sm-8 col-lg-4 text-lg-right mt-5 mt-lg-0 pr-lg-3 pr-xl-4">
                        <div class="overflow-hidden">
                            <h4 class="mb-3" data-zanim-xs='{"delay":0.2}' data-zanim-trigger="scroll">server side</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}' data-zanim-trigger="scroll">.NET, php, struts, hibernate, cakephp, spring, Java, JSP for server side technology.</p>
                        </div>
                        <div class="overflow-hidden">
                            <h4 class="mb-3" data-zanim-xs='{"delay":0.2}' data-zanim-trigger="scroll">analytics & bpm technology</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}' data-zanim-trigger="scroll">IBM Watson, filenet, bonitasoff for analytics & bpm technology.</p>
                        </div>
                        <div class="overflow-hidden">
                            <h4 class="mb-3" data-zanim-xs='{"delay":0.2}' data-zanim-trigger="scroll">business intelligence</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}' data-zanim-trigger="scroll">qlikview, pentaho, spagobl, pega for business intelligence technology.</p>
                        </div>
                        <div class="overflow-hidden">
                            <h4 class="mb-3" data-zanim-xs='{"delay":0.2}' data-zanim-trigger="scroll">erp</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}' data-zanim-trigger="scroll">salesforce, oracle financials, microsoft dynamics.</p>
                        </div>
                    </div>
                    <div class="col-sm-8 col-lg-4 mt-4 mt-lg-0 px-lg-4">
                        <img class="w-100 rounded" src="assets/img/technology1.png" alt="" data-zanim-xs='{"animation":"zoom-out","delay":0.1}' data-zanim-trigger="scroll" />
                    </div>
                    <div class="col-sm-8 col-lg-4 mt-5 mt-lg-0 pl-lg-3 pl-xl-4">
                        <div class="overflow-hidden">
                            <h4 class="mb-3" data-zanim-xs='{"delay":0.2}' data-zanim-trigger="scroll">bots & machine learning technology</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}' data-zanim-trigger="scroll">wit.ai, IBM Watson, sirikit, blueprism, UI path, cloud machine learning for bots & machine learnig technology.</p>
                        </div>
                        <div class="overflow-hidden">
                            <h4 class="mb-3" data-zanim-xs='{"delay":0.2}' data-zanim-trigger="scroll">mobility, omni channel technology</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}' data-zanim-trigger="scroll">phonegap, windows, android, ios for mobility, omni channel technology.</p>
                        </div>
                        <div class="overflow-hidden">
                            <h4 class="mb-3" data-zanim-xs='{"delay":0.2}' data-zanim-trigger="scroll">ui/ux technology</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}' data-zanim-trigger="scroll">bootstrap, HTML5, CSS3, JSON, AngularJS for UI/UX technology.</p>
                        </div>
                        <div class="overflow-hidden">
                            <h4 class="mb-3" data-zanim-xs='{"delay":0.2}' data-zanim-trigger="scroll">testing</h4>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}' data-zanim-trigger="scroll">selenium, loadrunner, appium.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section id="whyus">

            <div class="container">
                <div class="col-12 mb-3 mb-md-5 text-center">
                    <h2 class="fs-3 fs-sm-4"><span class="text-underline">why us?</span></h2>
                </div>
                <div class="row m-0">
                    <div class="col-sm-6 col-lg-3">
                        <img class="w-100 rounded" src="assets/img/experienced2.jpg" alt="" data-zanim-xs='{"delay":0,"animation":"zoom-in"}' data-zanim-trigger="scroll" />
                        <h4 class="mt-4">experience & courage</h4>
                        <p class="text-sans-serif lead">Absolutely right blend of technical & functional expertise.</p>
                    </div>
                    <div class="col-sm-6 col-lg-3 mt-4 mt-sm-0">
                        <img class="w-100 rounded" src="assets/img/focused1.jpg" alt="" data-zanim-xs='{"delay":0.1,"animation":"zoom-in"}' data-zanim-trigger="scroll" />
                        <h4 class="mt-4">precisely focused</h4>
                        <p class="text-sans-serif lead">Uniquely groomed engineering talent having experience over 6 years.</p>
                    </div>
                    <div class="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                        <img class="w-100 rounded" src="assets/img/services1.jpg" alt="" data-zanim-xs='{"delay":0.2,"animation":"zoom-in"}' data-zanim-trigger="scroll" />
                        <h4 class="mt-4">not just services</h4>
                        <p class="text-sans-serif lead">Our reusable patterns and components.</p>
                    </div>
                    <div class="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                        <img class="w-100 rounded" src="assets/img/basics.jpg" alt="" data-zanim-xs='{"delay":0.3,"animation":"zoom-in"}' data-zanim-trigger="scroll" />
                        <h4 class="mt-4">to the basics</h4>
                        <p class="text-sans-serif lead">Simplified Solution Design; Agility and speed of Delivery.</p>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section id="ourteam">

            <div class="container">
                <div class="col-12 mb-3 mb-md-5 text-center">
                    <h2 class="fs-3 fs-sm-4"><span class="text-underline">meet our team</span></h2>
                </div>
                <div class="sortable minh-100vh" data-options='{"layoutMode":"packery"}'>
                    <div class="row no-gutters sortable-container sortable-container-gutter-fix">
                        <a class="col-6 col-md-3 sortable-item p-2 photography" href="assets/img/gallery/team3-f.jpg" data-lightbox="image">
                            <img class="w-100" src="assets/img/gallery/team3.jpg" alt="" data-zanim-xs='{"animation":"zoom-in"}' data-zanim-trigger="scroll" />
                        </a>
                        <a class="col-6 col-md-3 sortable-item p-2 photography" href="assets/img/gallery/team7-f.jpg" data-lightbox="image">
                            <img class="rounded w-100 fit-cover" src="assets/img/gallery/team7.jpg" alt="" data-zanim-xs='{"animation":"zoom-in","delay":0.1}' data-zanim-trigger="scroll" />
                        </a>
                        <a class="col-6 col-md-3 sortable-item p-2 photography" href="assets/img/gallery/team8-f.jpg" data-lightbox="image">
                            <img class="rounded w-100 fit-cover" src="assets/img/gallery/team8.jpg" alt="" data-zanim-xs='{"animation":"zoom-in","delay":0.7}' data-zanim-trigger="scroll" />
                        </a>
                        <a class="col-6 col-md-3 sortable-item p-2 studio" href="assets/img/gallery/team1-f.jpg" data-lightbox="image">
                            <img class="rounded w-100 fit-cover" src="assets/img/gallery/team1.jpg" alt="" data-zanim-xs='{"animation":"zoom-in","delay":0.4}' data-zanim-trigger="scroll" />
                        </a>
                        <a class="col-6 col-md-6 sortable-item p-2 studio illustration" href="assets/img/gallery/team-f.jpg" data-lightbox="image">
                            <img class="w-100 fit-cover" src="assets/img/gallery/team.jpg" alt="" data-zanim-xs='{"animation":"zoom-in","delay":0.2}' data-zanim-trigger="scroll" />
                        </a>
                        <a class="col-6 col-md-3 sortable-item p-2 photography" href="assets/img/gallery/team6-f.jpg" data-lightbox="image">
                            <img class="rounded w-100 fit-cover" src="assets/img/gallery/team6.jpg" alt="" data-zanim-xs='{"animation":"zoom-in","delay":0.5}' data-zanim-trigger="scroll" />
                        </a>
                        <a class="col-6 col-md-3 sortable-item p-2 photography" href="assets/img/gallery/team4-f.jpg" data-lightbox="image">
                            <img class="rounded w-100 fit-cover" src="assets/img/gallery/team4.jpg" alt="" data-zanim-xs='{"animation":"zoom-in","delay":0.6}' data-zanim-trigger="scroll" />
                        </a>
                        <div class="col-12 col-sm-6 sortable-item p-2 illustration">
                            <div class="embed-responsive embed-responsive-4by3">
                                <div class="owl-carousel owl-theme owl-dots-inner owl-theme-white embed-responsive-item" data-options='{"items":1,"autoplay":true,"loop":true,"animateOut":"fadeOut","nav":true}' data-zanim-xs='{"animation":"zoom-in","delay":0.8}' data-zanim-trigger="scroll">
                                    <img class="w-100 fit-cover rounded" src="assets/img/gallery/team11.jpg" alt="" />
                                    <img class="w-100 fit-cover rounded" src="assets/img/gallery/team13.jpg" alt="" />
                                </div>
                            </div>
                        </div>
                        <a class="col-6 col-md-3 sortable-item p-2 interior" href="assets/img/gallery/team2-f.jpg" data-lightbox="image">
                            <img class="rounded w-100 fit-cover" src="assets/img/gallery/team2.jpg" alt="" data-zanim-xs='{"animation":"zoom-in","delay":0.3}' data-zanim-trigger="scroll" />
                        </a>
                        <a class="col-6 col-md-3 sortable-item p-2 photography" href="assets/img/gallery/team5-f.jpg" data-lightbox="image">
                            <img class="rounded w-100 fit-cover" src="assets/img/gallery/team5.jpg" alt="" data-zanim-xs='{"animation":"zoom-in","delay":0.4}' data-zanim-trigger="scroll" />
                        </a>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->


        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="overflow-hidden py-0" id="clients">
            <div class="position-absolute overflow-hidden a-0">
                <div class="bg-holder overlay overlay-0 rounded" style="background-image: url(assets/img/slider5.jpg);" data-zanim-trigger="scroll" data-zanim-lg='{"animation":"zoom-out","delay":0}'>
                </div>
                <!--/.bg-holder-->

            </div>
            <div class="align-items-center justify-content-center text-center py-6">
                <div class="col-12 mb-3 mb-md-5 text-center">
                    <h2 class="fs-3 fs-sm-4"><span class="text-underline">trusted since 2011</span></h2>
                </div>
                <div class="col-lg-12 px-lg-8 mt-6 mt-lg-0">
                    <div class="owl-carousel owl-theme owl-nav-outer" data-options='{"dots":false,"nav":true,"items":1,"autoplay":true,"loop":true,"autoplayHoverPause":true}'>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="font-weight-bold text-white fs-1 fs-sm-2 mb-4">"Retail & Supply Chain"</h4>
                                    <h6 class="fs-0 mb-0 text-white font-weight-normal">Our robust retail process knowledge, specialized tools, and consulting expertise helps clients to enhance retail processes efficiency and fulfillment precision, reduce costs and decrease cycle times from order to fulfillment.</h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client9.png" alt="" />
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client4.png" alt="" />
                                        </div>
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client6.png" alt="" />
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client5.png" alt="" />
                                        </div>
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client8.png" alt="" />
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client11.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="font-weight-bold text-white fs-1 fs-sm-2 mb-4">"Financial Services"</h4>
                                    <h6 class="fs-0 mb-0 text-white font-weight-normal">Our deep expertise in front to back understanding of the financial services functions with excellent experience on taking projects from concept to implementation makes us the partner of choice for Financial Services Business.</h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client3.png" alt="" />
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client10.png" alt="" />

                                        </div>
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client16.png" alt="" />
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client17.png" alt="" />
                                        </div>
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client15.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="font-weight-bold text-white fs-1 fs-sm-2 mb-4">"Logistics"</h4>
                                    <h6 class="fs-0 mb-0 text-white font-weight-normal">We have in-depth understanding of the shipping and logistics industry through our strong project experiences, and bringing simplified solutions to scale up the growth and meet the upcoming challenges of customer acquisitions to fulfilment.</h6>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client.png" alt="" />
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client1.png" alt="" />
                                        </div>
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client12.png" alt="" />
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client13.png" alt="" />
                                        </div>
                                        <div class="col-4">
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client2.png" alt="" />
                                            <img class="logo-grid img-fluid" src="assets/img/logo/client14.png" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="py-5 py-lg-7" id="testimonials">

            <div class="container">
                <div class="col-12 mb-3 mb-md-5 text-center">
                    <h2 class="fs-3 fs-sm-4"><span class="text-underline">our principals</span></h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row align-items-center mb-7">
                            <div class="col-sm-auto">
                                <div class="mx-auto ml-sm-0 mb-4 mb-sm-0">
                                    <img class="avatar avatar-egg" src="assets/img/deepaksir.jpg" alt="" />
                                </div>
                            </div>
                            <div class="col pl-5">
                                <blockquote class="blockquote" data-zanim-xs='{"delay":0.1}' data-zanim-trigger="scroll">
                                    <p class="text-sans-serif lead blockquote-content">It has been a fantastic journey to grow this team from start to a hundred engineers. C Simplify IT gets its strength through the conviction that talent must be nurtured and grown to make a truly productive professional. We are committed to make each and every one of our customer succeed with the productive talent we have built over years.</p>
                                    <div class="h6 fs-0 font-weight-black">Deepak Mehta</div>
                                    <div class="h6 text-500">Founder</div>
                                </blockquote>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-sm-auto order-lg-2">
                                <div class="mx-auto ml-sm-0 mb-4 mb-sm-0">
                                    <img class="avatar avatar-egg" src="assets/img/anilsir.jpg" alt="" />
                                </div>
                            </div>
                            <div class="col pl-5">
                                <blockquote class="blockquote" data-zanim-xs='{"delay":0.1}' data-zanim-trigger="scroll">
                                    <p class="text-sans-serif lead blockquote-content">We are the techies with bundles of energy, passion, and a drive to make it a win for our customers. C Simplify IT stands on the fundamentals of delivering true success in our engagements through quality, precision and excellence.</p>
                                    <div class="h6 fs-0 font-weight-black">Anil Chaudhry</div>
                                    <div class="h6 text-500">Managing Partner</div>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->

        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="text-center" id="employment">

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8" data-zanim-trigger="scroll" data-zanim-timeline="{}">
                        <div class="col-12 mb-3 mb-md-5 text-center overflow-hidden">
                            <h2 class="fs-3 fs-sm-4" data-zanim-xs='{"delay":0.1}'><span class="text-underline">start your career with us</span></h2>
                        </div>
                        <div class="overflow-hidden">
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.2}'>We don't know what type of career you have in mind. But we do know that you can find the right opportunity with us. C Simplify IT is more diverse than almost any other company. Show us what you can do and where you want to go. We'll provide you with the support to develop your talent and personal strengths.</p>
                            <p class="text-sans-serif lead" data-zanim-xs='{"delay":0.3}'>Drop us an email at: <a class="job-email" href="mailto:jobs@csimplifyit.com">jobs@csimplifyit.com</a></p>
                        </div>
                        <div data-zanim-xs='{"delay":0.3}'>
                            <a class="btn btn-dark rounded-capsule mt-4" href="mailto:jobs@csimplifyit.com">Share Your CV</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->


        <!-- ============================================-->
        <!-- <section> begin ============================-->
        <section class="text-sans-serif" id="contact">

            <div class="container">
                <div class="row">
                    <div class="col-lg-4 pr-lg-5">
                        <h3 class="mb-3">C Simplify IT</h3>
                        <p>We are a specialized Global IT services & solution organization established in 2011, with a team of 120+ consulting & engineering professionals spread across its delivery centers at India (Gurugram), USA (Fremont, CA) . We are a specialist IT Consulting firm, leveraging innovation & passion at its best, CSimplify IT is trusted by Fortune 500 companies and start-ups to help business to add value across our clients worldwide. Our clients are based in USA, Singapore, Malaysia, Switzerland, UK, Hong Kong, and India.</p>
                    </div>
                    <div class="col-md-6 col-lg-4 pr-lg-6 ml-lg-auto mt-6 mt-lg-0">
                        <h3 class="mb-3">Socials</h3>
                        <p class="mb-0">Stay connected with C Simplify IT via our social media pages. If you haven't taken the opportunity to visit our social media pages, please do so. It's a great way to interact with us, get your questions answered and make suggestions.</p>
                        <br />
                        <a class="btn btn-dark btn-sm mr-2" href="https://www.facebook.com/CSimplifyIT-210115279023481/" target="_blank"><span class="fab fa-facebook-f"></span></a>
                        <a class="btn btn-dark btn-sm" href="https://in.linkedin.com/company/c-simplify-it-services-private-limited" target="_blank"><span class="fab fa-linkedin-in"></span></a>
                    </div>
                    <div class="col-md-6 col-lg-4 mt-6 mt-lg-0 office-address">
                        <h3 class="mb-3">Get in touch</h3>
                        <address>
                            <b><u>India Head Office:</u></b>
                            <br />
                            Plot No. 52, II Floor,
                <br />
                            Sector 32, Gurgaon (Haryana) - 122003
                <br />
                            <b>Call Us:</b> <a href="tel:+91 98999762274">+91 9899976227</a> (For Tech Queries),<br />
                            <b>E-Mail:</b> <a href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
                            <br />
                            <br />
                            <b><u>US Head Office:</u></b>
                            <br />
                            46878 Fernald St, Fremont, CA 94539, USA
                <br />
                            <b>Call Us:</b>  <a href="tel:+1 510 402 1718">+1 510 402 1718</a>
                            <br />
                            <b>E-Mail:</b> <a href="mailto:sales@csimplifyit.com">sales@csimplifyit.com</a>
                        </address>
                    </div>
                </div>
            </div>
            <!-- end of .container-->

        </section>
        <!-- <section> close ============================-->
        <!-- ============================================-->



    </main>
    <!-- ===============================================-->
    <!--    End of Main Content-->
    <!-- ===============================================-->




    <!--===============================================-->
    <!--    Footer-->
    <!--===============================================-->
    <footer class="footer bg-black text-600 py-4 text-sans-serif text-center overflow-hidden" data-zanim-timeline="{}" data-zanim-trigger="scroll">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 order-lg-2">
                    <a class="indicator indicator-up js-scroll-trigger" href="#top"><span class="indicator-arrow indicator-arrow-one" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":0.9}'></span><span class="indicator-arrow indicator-arrow-two" data-zanim-xs='{"from":{"opacity":0,"y":15},"to":{"opacity":1,"y":-5,"scale":1},"ease":"Back.easeOut","duration":0.4,"delay":1.05}'></span></a>
                </div>
                <div class="col-lg-4 text-lg-left mt-4 mt-lg-0">
                    <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
                        Copyright &copy; 2018 C Simplify IT
                    </p>
                </div>
                <div class="col-lg-4 text-lg-right order-lg-2 mt-2 mt-lg-0">
                    <%-- <p class="fs--1 text-uppercase ls font-weight-bold mb-0">
              <a class="text-600" href="TOS.aspx">Terms of Service</a>
            </p>--%>
                </div>
            </div>
        </div>
    </footer>


    <!--===============================================-->
    <!--    Modal for language selection-->
    <!--===============================================-->
    <div class="remodal bg-black remodal-select-language" data-remodal-id="language">
        <div class="remodal-close" data-remodal-action="close"></div>
        <ul class="list-unstyled pl-0 my-0 py-4 text-sans-serif">
            <li>
                <a class="pt-1 d-block text-white font-weight-semi-bold" href="#">English</a>
            </li>
            <li>
                <a class="pt-1 d-block text-500" href="#">Français</a>
            </li>
            <li>
                <a class="pt-1 d-block text-500" href="page-rtl.html">عربى</a>
            </li>
            <li>
                <a class="pt-1 d-block text-500" href="#">Deutsche</a>
            </li>
        </ul>
    </div>


    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="assets/js/scrolling-nav.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/lib/loaders.css/loaders.css.js"></script>
    <script src="assets/js/stickyfill.min.js"></script>
    <script src="assets/lib/remodal/remodal.js"></script>
    <script src="assets/lib/jtap/jquery.tap.js"></script>
    <script src="assets/js/rellax.min.js"></script>
    <script src="assets/lib/owl.carousel/owl.carousel.js"></script>
    <script src="assets/lib/lightbox2/js/lightbox.js"></script>
    <script src="assets/lib/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/lib/isotope-packery/packery-mode.pkgd.min.js"></script>
    <script src="assets/js/Default.js"></script>
    <script src="assets/js/theme.js"></script>

</body>

</html>

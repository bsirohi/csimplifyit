﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class remoteLogin : System.Web.UI.Page
{
    oAuthLinkedIn linkedin = new oAuthLinkedIn();
    protected void Page_Load(object sender, EventArgs e)
    {

            linkedinConnect();
       
    }
    private void linkedinConnect()
    {
       string link = null;
        Session["oauthclient"] = "linkedin";
        link = linkedin.AuthorizationLinkGet();
        Session["tokensecret"] = linkedin.TokenSecret;
        linkedin.connectToOauthServer(link);
    }
}
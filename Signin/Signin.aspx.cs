﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using csimplifyit.DBContext;
//using System.Web.Script.Services;
using csimplifyit.Model;
using csimplifyit.MsgFormatter;
//using Newtonsoft.JsonResult;
using Newtonsoft.Json;
using System.Web.Services;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Text;
using csimplifyit.Login;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Web.Security;

namespace csimplifyit.Sign_in
{
    public partial class Signin : System.Web.UI.Page
    {
        //TODO: paste your facebook-app key here!!
        public const string FaceBookAppKey = "0b1b0ce703aff23fcd688267c10c2b37";

        private static csitEntities1 db = new csitEntities1();
        private static Dictionary<string, object> moResponse;
        // public const string FaceBookAppKey = "XXXXXXXXXXXXX";
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

                string product = RouteData.Values["LogOut"] as string;
                if (product != null)
                {
                    LogOut();
                }


                //UserInfo unfo = new UserInfo();
                //if (unfo.email != null)
                //{
                //    Response.Cookies.Clear();
                //    FormsAuthentication.SignOut();
                //    HttpCookie c = new HttpCookie("login");
                //    c.Expires = DateTime.Now.AddYears(-2);
                //    Response.Cookies.Add(c);

                //    Session.Clear();
                //    Response.Redirect(@"../Strip_Payment_way/StripePayment.aspx");
                //}
               
            }
            
            
        }
        //****Registration Details Saved
        [WebMethod]
        public static object SaveReg(string requestData)
        {
            try
            {
                usermaster Regdetails = new usermaster();
                JObject jsonobj = JObject.Parse(requestData);

                string FirstName = string.Empty;
                if (jsonobj["FirstName"] != null && jsonobj["FirstName"].ToString() != "")
                    FirstName = jsonobj["FirstName"].ToString();

                else
                {
                    moResponse = RespMsgFormatter.formatResp(1, "FirstName is Required", null, "SaveReg");
                    return moResponse;
                }

                string LastName = string.Empty;
                if (jsonobj["LastName"] != null && jsonobj["LastName"].ToString() != "")
                    LastName = jsonobj["LastName"].ToString();

                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "LastName is Required", null, "SaveReg");
                    return moResponse;
                }
                string Email = string.Empty;
                if (jsonobj["Email"] != null && jsonobj["Email"].ToString() != "")
                {
                    Email = jsonobj["Email"].ToString();
                    var ValidateEmail = db.usermasters.Where(s => s.EmailId1.ToUpper() == Email.ToUpper() && s.Status == 1).FirstOrDefault();
                    if (ValidateEmail != null)
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "Email-Id is already used", "", "SaveReg");
                        return moResponse;
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "Email is Required", null, "SaveReg");
                    return moResponse;
                }
                string Password = string.Empty;
                if (jsonobj["Password"] != null && jsonobj["Password"].ToString() != "")
                    Password = jsonobj["Password"].ToString();
                else
                {

                    moResponse = RespMsgFormatter.formatResp(0, "Password is required ", null, "SaveReg");
                    return moResponse;
                }
                int usertype = 0;
                if (jsonobj["usertype"] != null && jsonobj["usertype"].ToString() != "")
                    usertype = int.Parse(jsonobj["usertype"].ToString());
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "", null, "SaveReg");
                    return moResponse;
                }
                //Change Password in Encrypted mode 
                var cybertext = Encrypt(Password);
                //Save Details in database with encrypted password
                Regdetails.FirstName = FirstName;
                Regdetails.Title = 1;
                Regdetails.LastName = LastName;
                Regdetails.EmailId1 = Email;
                Regdetails.Status = 1;
                Regdetails.Address1 = "";
                Regdetails.Phone1 = "";
                Regdetails.UserName = FirstName;
                Regdetails.City = 1;
                Regdetails.Country = 95;
                Regdetails.userType = usertype;
                Regdetails.ownerLocCode = 2;
                Regdetails.State = 1;
                Regdetails.Password = cybertext;
                Regdetails.subscriptionPlanId = 1;
                Regdetails.CRTBy = 1;
                Regdetails.CRTDate = DateTime.Now;
                Regdetails.UPDBy = 1;
                Regdetails.UPDdate = DateTime.Now;
                Regdetails.limitMulFactor = 1;
                db.usermasters.Add(Regdetails);
                db.SaveChanges();
                moResponse = RespMsgFormatter.formatResp(1, "Registration Sucessfully ", Regdetails, "SaveReg");
                return moResponse;

            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:" + ex.InnerException + "Message:" + ex.Message + "Stack Trace:" + ex.StackTrace, null, "getALlPaidSubSriptionDetails");
                return moResponse;
            }
        }







        [WebMethod]
        public static object RegestierSignIN(string requestData)
        {
            try
            {
                usermaster Regdetails = new usermaster();
                JObject jsonobj = JObject.Parse(requestData);

                string FirstName = string.Empty;
                if (jsonobj["FirstName"] != null && jsonobj["FirstName"].ToString() != "")
                    FirstName = jsonobj["FirstName"].ToString();



                string LastName = string.Empty;
                if (jsonobj["LastName"] != null && jsonobj["LastName"].ToString() != "")
                    LastName = jsonobj["LastName"].ToString();


                string Email = string.Empty;
                if (jsonobj["Email"] != null && jsonobj["Email"].ToString() != "")
                {
                    Email = jsonobj["Email"].ToString();
                    var ValidateEmail = db.usermasters.Where(s => s.EmailId1.ToUpper() == Email.ToUpper() && s.Status == 1).FirstOrDefault();
                    if (ValidateEmail != null)
                    {
                        long UserId = ValidateEmail.userid;
                        GoogleLogIN(UserId);
                        moResponse = RespMsgFormatter.formatResp(1, "Login Sucessfully ", null, "RegestierSignIN");
                        return moResponse;
                    }
                    else
                    {
                        int usertype = 0;
                        if (jsonobj["usertype"] != null && jsonobj["usertype"].ToString() != "")
                            usertype = int.Parse(jsonobj["usertype"].ToString());

                        //Change Password in Encrypted mode 
                        var cybertext = Encrypt("Password");
                        //Save Details in database with encrypted password
                        Regdetails.FirstName = FirstName;
                        Regdetails.Title = 1;
                        Regdetails.LastName = LastName;
                        Regdetails.EmailId1 = Email;
                        Regdetails.Status = 1;
                        Regdetails.Address1 = "";
                        Regdetails.Phone1 = "";
                        Regdetails.UserName = FirstName;
                        Regdetails.City = 1;
                        Regdetails.Country = 95;
                        Regdetails.userType = usertype;
                        var logdata = db.codelkups.Where(s => s.KeyCode == usertype && s.LkUpCode== "USER_TYP").FirstOrDefault();
                        if (logdata != null)
                        {
                            Regdetails.ownerLocCode = 2;
                            Regdetails.State = 1;
                            Regdetails.Password = cybertext;
                            Regdetails.CRTBy = 1;
                            Regdetails.CRTDate = DateTime.Now;
                            Regdetails.UPDBy = 1;
                            Regdetails.UPDdate = DateTime.Now;
                            Regdetails.subscriptionPlanId = 1;
                            Regdetails.limitMulFactor = 1;
                            db.usermasters.Add(Regdetails);
                            db.SaveChanges();
                            long SaveUserID = Regdetails.userid;

                            moResponse = RespMsgFormatter.formatResp(1, "Registration Sucessfully ", Regdetails, "RegestierSignIN");
                            GoogleLogIN(SaveUserID);
                        }
                        else
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Not a valid User ", Regdetails, "RegestierSignIN");
                        }                        

                    }
                }



                return moResponse;

            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:" + ex.InnerException + "Message:" + ex.Message + "Stack Trace:" + ex.StackTrace, null, "getALlPaidSubSriptionDetails");
                return moResponse;
            }
        }

        [WebMethod]
        public static object GoogleLogIN(long UserID)
        {
            try
            {

                UserInfo unfo = new UserInfo();
                var Result = db.usermasters.Where(s => s.userid == UserID && s.Status == 1).FirstOrDefault();
                if (Result != null)
                {
                    unfo.email = Result.EmailId1;
                    unfo.userId = Result.userid;
                    unfo.userName = Result.UserName;
                    unfo.userType = Result.userType;
                    Login.Login log = new Login.Login();
                    log.setLoggedInUserDetails(unfo);
                    if (unfo.userType == 0)

                    {
                        return moResponse;
                    }
                }
                else {

                    moResponse = RespMsgFormatter.formatResp(1, "User Is not valid ", null, "GoogleLogIN");
                    return moResponse;
                }

                return moResponse;
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:" + ex.InnerException + "Message:" + ex.Message + "Stack Trace:" + ex.StackTrace, null, "SaveDocumentClass");
                return moResponse;
            }
        }

        public static string Encrypt(string text)
        {
            encr lokey = new encr();

            using (var md5 = new MD5CryptoServiceProvider())
            {
                using (var tdes = new TripleDESCryptoServiceProvider())
                {
                    tdes.Key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(lokey.key));
                    tdes.Mode = CipherMode.ECB;
                    tdes.Padding = PaddingMode.PKCS7;

                    using (var transform = tdes.CreateEncryptor())
                    {
                        byte[] textBytes = UTF8Encoding.UTF8.GetBytes(text);
                        byte[] bytes = transform.TransformFinalBlock(textBytes, 0, textBytes.Length);
                        return Convert.ToBase64String(bytes, 0, bytes.Length);
                    }
                }
            }
        }

        [WebMethod]
        public static object getLoginUserId()
        {
            try
            {
                Login.Login log = new Login.Login();
                long UserID = log.getUserId();
                if (UserID != 0)
                {
                    var getUserDetails = db.usermasters.Where(s => s.userid == UserID).Select(s => new { s.userid, s.userType,s.UserName,s.EmailId1 }).FirstOrDefault();
                    moResponse = RespMsgFormatter.formatResp(1, "Get User ID", getUserDetails, "getLoginUserId");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(-1, "User Not Log In ", null, "getLoginUserId");
                }
                return moResponse;
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Inner Exception:" + ex.InnerException + "Message:" + ex.Message + "Stack Trace:" + ex.StackTrace, null, "getLoginUserId");
                return moResponse;
            }



        }

        [WebMethod]
         public static string GetFacebookUserJSON(string access_token)
        {
            string url = string.Format("https://graph.facebook.com/me?access_token={0}&fields=email,name,first_name,last_name,link", access_token);

            WebClient wc = new WebClient();
            Stream data = wc.OpenRead(url);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            data.Close();
            reader.Close();

            return s;
        }


        //[WebMethod]
        //public static string LogOutUser(string access_token)
        //{

        //  Signin.LogOut();
        //    return "ok";
        //}




        [WebMethod]
        public void LogOut()
        {
            
            Response.Cookies.Clear();
            FormsAuthentication.SignOut();
            HttpCookie c = new HttpCookie("login");
            c.Expires = DateTime.Now.AddYears(-2);
            Response.Cookies.Add(c);

            Session.Clear();
            Response.Redirect(@"../Strip_Payment_way/StripePayment.aspx");
            
        }


    }
}
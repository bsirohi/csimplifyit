﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Signin.aspx.cs" Inherits="csimplifyit.Sign_in.Signin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        
    <link href="../assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="../assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="../assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="../assets/css/theme.css" rel="stylesheet">
    <link href="../contents/Admin/signin.css" rel="stylesheet" />
    

     <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicons/apple-touch-icon.jpg">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicons/favicon-32x32.jpg">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicons/favicon-16x16.jpg">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicons/favicon.ico">

    <link rel="manifest" href="../assets/img/favicons/manifest.json">
    <meta name="msapplication-TileImage" content="../assets/img/favicons/mstile-150x150.jpg">
    <meta name="theme-color" content="#ffffff">

    <meta name="google-sigin-client_id" content="509303823636-4nmni39qd4m0egmiv7cgkdqq57teuo5h.apps.googleusercontent.com" />
        <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="../assets/lib/loaders.css/loaders.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Mono%7cPT+Serif:400,400i%7cLato:100,300,400,700,800,900" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal.css" rel="stylesheet">
    <link href="../assets/lib/remodal/remodal-default-theme.css" rel="stylesheet">
    <link href="../assets/lib/owl.carousel/owl.carousel.css" rel="stylesheet">
    <link href="../assets/lib/lightbox2/css/lightbox.css" rel="stylesheet">
    <link href="../assets/lib/semantic-ui-accordion/accordion.min.css" rel="stylesheet">
    <link rel="../stylesheet" href="assets/css/font-awesome.css">
    <link href="../assets/css/theme.css" rel="stylesheet">
    <link rel="../stylesheet" type="text/css" href="assets/css/document_management.css">
    <link href="../contents/UtilityCss/common.css" rel="stylesheet" />



   
     <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="../assets/js/scrolling-nav.js"></script>
    <script src="../assets/js/plugins.js"></script>
    <script src="../assets/lib/loaders.css/loaders.css.js"></script>
    <script src="../assets/js/stickyfill.min.js"></script>
    <script src="../assets/lib/remodal/remodal.js"></script>
    <script src="../assets/lib/jtap/jquery.tap.js"></script>
    <script src="../assets/js/rellax.min.js"></script>
    <script src="../assets/lib/owl.carousel/owl.carousel.js"></script>
    <script src="../assets/lib/lightbox2/js/lightbox.js"></script>
    <script src="../assets/lib/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="../assets/lib/isotope-packery/packery-mode.pkgd.min.js"></script>
    <%--<script src="../assets/js/Default.js"></script>--%>
    <script src="../assets/js/theme.js"></script>
    <script src="../Scripts/Utility/common.js"></script>
    <script src="../Scripts/Facebook/facebook.js"></script>
    <script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
    <%-- <script src="../Scripts/Stripe/Stripe.js"></script>--%>
  <%-- <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>--%>

    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <%--<script src="../Scripts\Stripe\Stripe.js"></script>--%>

    <script src="../Scripts/signin/signin.js"></script>
    <script type="text/javascript">

       
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
              m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
          ga('create', 'UA-63272985-1', 'auto');
          ga('send', 'pageview');

      </script>



    
        <style type="text/css">
     .plans:hover {
        box-shadow: 1px 5px 8px #3333337d;
        background: white !important;
      }
      #pricing_details li {
          list-style: none;
      }
      .yes_feature:before {
          content: '\2713';
          color: green;
          font-weight: bold;
          padding-right: 5px;
      }
      .not_feature:before {
          content: '\2715';
          color: red;
          font-weight: bold;
          padding-right: 5px;
      }
      .promo-code { 
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
      }
      .promo-checkbox { margin: 0.5rem 0; }
      .upgradePlans {
        color: blue;
        font-weight: bold;
        font-size: 20px;
        display: block;
      }
      #studio-contact {
          border-bottom: 2px solid #929292;
      }
      .amount span{
        font-size: 40px; 
        vertical-align: top; 
        font-weight: bold; 
        color: #333;
      }
      .discountedPrice {
        font-size: 24px !important; 
        text-decoration: line-through;
      }
    </style>
</head>


<body>


    <!--===============================================-->
    <!--    Fancynav-->
    <!--===============================================-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#landing-page">
          <img src="../assets/img/logo.png" alt="C Simplify IT" height="45" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown1">
          <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#landing-page">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#whatwedo">Solutions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#offerings">Offerings</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#technology">Technologies</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbarDropdownMenuProduct" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Product</a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuProduct">
                <a class="dropdown-item" href="../SM3/SimpleMassMailMerge.aspx">Simple Mass Mail Merge</a>
                  <a class="dropdown-item" href="../SRM/SimpleRecruitmentManager.aspx">Simple Recruitment Manager</a>
                  <a class="dropdown-item" href="../googleworkflow/SimpleWorkflowManager.aspx">Simple Workflow Manager</a>
                  <a class="dropdown-item" href="SPOS/SimplePointofSale.aspx">Simple Point of Sale</a>
                  <a class="dropdown-item" href="STDGDoc/SimpleTabularDocumentGenerator.aspx">Simple Tabular Document Generator</a>
                  
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Strip_Payment_way/StripePayment.aspx">Pricing</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#whyus">Why Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#clients">Our Clients</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="../Default.aspx#contact">Contact Us</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!--===============================================-->
    <!--    End of Fancynav-->
    <!--===============================================-->



    <!-- ===============================================-->
    <!--    Main Content-->
    <!-- ===============================================-->
    <main class="main minh-100vh" id="top">


      <!-- ============================================-->
      <!-- Preloader ==================================-->
      <div class="preloader" id="preloader">
        <div class="loader">
          <div class="line-scale-pulse-out-rapid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </div>
      <!-- ============================================-->
      <!-- End of Preloader ===========================-->

	  
	  <!-- ============================================-->
      <!-- <section> begin ============================-->
      <%--<div class="speciality">
        <a href="#">Our Specialities</a>
      </div>--%>
      <!-- <section> close ============================-->
      <!-- ============================================-->
	  
      <!-- ============================================-->
      <!-- <section> begin ============================-->
      <section class="py-0" id="plansPricing">

        <div class="container-fluid">
            <div  class="row" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                 <div class="col-lg-6"> </div>
                      <div class="col-lg-6 px-0" id="login">
              <div class="h-100 py-9" data-zanim-lg='{"animation":"slide-right","from":{"opacity":1},"delay":0.4}' data-zanim-trigger="scroll">
                <div class="bg-holder" style="background-image:url(../assets/img/pricing.jpg);" data-zanim-trigger="scroll" data-zanim-lg='{"animation":"slide-right","delay":0}'>
                </div>
                <!--/.bg-holder-->
              </div>
              </div>
            </div>
             <div class="container" id="backgr">
          <div class="row" id="csit" >
           
           
               
              
    
            <div class="col-lg-6">
                 <div class="row align-items-center justify-content-center py-6 py-md-8">
                <div class="col-11 col-sm-10">
                  <div class="overflow-hidden">
                    <h2 class="font-weight-light text-uppercase fs-3 fs-lg-4" data-zanim-lg='{"delay":0.4}'><span class="font-weight-black"> Plans &<br/> Pricing</span></h2>
                  </div>
                  <div class="overflow-hidden">
                    <p id="contentdata" class="lead text-sans-serif font-weight-normal pr-xl-9 mt-4 text-500" data-zanim-lg='{"delay":0.5}'>Whether your time-saving<br/> automation needs are large or small,<br/> we’re here to help you scale.</p>
                  </div>
                 <%-- <div class="overflow-hidden">
                    <div class="mt-3 mt-lg-5" data-zanim-xs='{"delay":0.6}'>
                      <a class="btn btn-danger" href="#">create account</a>
                      <h6 class="font-weight-normal mt-2 text-500">Have an account?
                        <a class="text-900 font-weight-bold ml-1" href="#">Sign in<span class="d-inline-block ml-1">&xrarr;</span></a>
                      </h6>
                    </div>
                  </div>--%>
                </div>
              </div>
            </div>  
            <div class="col-lg-6" id="loginbox" style="margin-top:3px;" class="mainbox">                
            <div class="panel panel-info" >
                   <%-- <div class="panel-heading">
                        <div class="panel-title">Sign In</div>
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
                    </div>  --%>   

                    <div style="padding-top:24px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <div id="loginform" class="form-horizontal" role="form">
                            
                            <div class="row">
                                <div class="col-md-6">
                                     <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="Firstname" type="text" class="form-control" name="First Name" placeholder="First Name" />
                                      </div>
                                     </div>
                                    <div class="col-md-6">
                                         <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="Lastname" type="text" class="form-control" name="Last Name" placeholder="Last Name" />
                                      </div>
                                    </div>
                               </div>
                             <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="WorkEmail" type="text" class="form-control" name="Work Email" placeholder="Work Email" onblur="validateEmail(this.value)"/>
                                      </div>
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input id="PassWord" type="password" class="form-control" name="Password" placeholder="Password" />
                                      </div>
                             <div class="separator separator--light"><span class="separator__content">OR</span></div>
                            <div class="row">
                                <div class="col-md-12" id="btnlink">
                                <button class="loginBtn loginBtn--facebook" onclick="loginByFacebook()" >
                                  Login with Facebook
                                 </button>      
                           
                                    <button class="loginBtn loginBtn--google" onclick="OpenGoogleLoginPopup()">
                                    Login with Google
                                        </button>
                                </div>
                               
                            </div>
                              
                                    
                         

                                
                            <%--<div class="input-group">
                                      <div class="checkbox">
                                        <label>
                                          <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                        </label>
                                      </div>
                                    </div>--%>
                               

                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-12 controls">
                                      <a id="btn-login" href="#" class="btn btn-success" onclick="SaveRegistration()">Sign Up</a>
                                      <%--<a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a>--%>

                                    </div>
                                </div>


                                 <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                           <p id="Sdetails">Already have an account? 
                                      
                                           </p>
                                       <%-- <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                            Sign Up Here
                                        </a>--%>
                                        </div>
                                    </div>
                                </div>    
                            </div>     


                        </div>
                        </div>                     
                    </div>  
      
        <div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title">Sign Up</div>
                            <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
                        </div>  
                        <div class="panel-body" >
                            <div id="signupform" class="form-horizontal" role="form">
                                
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>
                                    
                                
                                  
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">First Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="firstname" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">Last Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="passwd" placeholder="Password">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="icode" class="col-md-3 control-label">Invitation Code</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="icode" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="button" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                                        <span style="margin-left:8px;">or</span>  
                                    </div>
                                </div>
                                
                                <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">
                                    
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-fbsignup" type="button" class="btn btn-primary"><i class="icon-facebook"></i>   Sign Up with Facebook</button>
                                    </div>                                           
                                        
                                </div>
                                
                                
                                
                            </div>
                         </div>
                    </div>

               
               
                
         </div> 
   
    
            
       </div>
            
          </div>
            
        </div>
       
      

      </section>
      <!-- <section> close ============================-->
      <!-- ============================================-->
         <!-- ============================================-->
      <!-- <section> begin ============================-->


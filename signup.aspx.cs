﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using csimplifyit;
using OAuth4Client.OAuth;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Net.Mail;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;

public partial class signup : System.Web.UI.Page
{
    oAuthLinkedIn linkedin = new oAuthLinkedIn();
    oAuthYahoo yahoo = new oAuthYahoo();
    OauthTwitter twitter = new OauthTwitter();
    OAuthFacebook facebook = new OAuthFacebook();
    OAuthContext OContext = new OAuthContext("");
    JsonParserSaveData parser = new JsonParserSaveData();
    DataModel model = new DataModel();
    DataTable dt = new DataTable();
    string[] displaydata = new string[2];
    protected void Page_Load(object sender, EventArgs e)
    {
        processAfterOauthRequest();


    }
    private void savepassword(string uid, string password)
    {

        dt = model.selectIntoTable("select regUserId from AuthenticationTokens where token='" + uid + "'");
        if (dt.Rows.Count != 0)
        {
            model.insertIntoTable("update Users set userPassword='" + EncryptBase64.EncodeTo64(password) + "' where uid=" + dt.Rows[0][0]);
        }
    }

    private void processAfterOauthRequest()
    {
        var request = HttpContext.Current.Request;
        List<string> myControlList;
        myControlList = new List<string>();
        if (!string.IsNullOrEmpty(request.QueryString["code"]))
            myControlList.Add(request.QueryString["code"]);
        try
        {
            if (Session["oauthclient"].ToString() == "linkedin")
            {
                getLinkedinValues(request);
                HttpContext.Current.Response.Redirect("https://csimplifyit.com/Default.aspx?success");
            }
        }
        catch (Exception exc) { Console.WriteLine(exc.Message); }
        //try
        //{
        //    if (Session["oauthclient"].ToString() == "yahoo")
        //    {
        //        getYahooValues(request);
        //        HttpContext.Current.Response.Redirect("https://csimplifyit.com/Default.aspx?success");

        //    }
        //}
        //catch (Exception exc) { }
        //try
        //{
        //    if (Session["oauthclient"].ToString' ASHJ,./?.() == "twitter")
        //    {
        //        getTwitterValues(request);
        //        HttpContext.Current.Response.Redirect("https://csimplifyit.com/Default.aspx?success");

        //    }
        //}
        //catch (Exception exc) { }
        try
        {
            if (Session["oauthclient"].ToString() == "facebook")
            {
                getFacebookValues(request);
                //    HttpContext.Current.Response.Redirect("http://developer.csimplifyit.com/Default.aspx?success");
                HttpContext.Current.Response.Redirect("https://csimplifyit.com/Default.aspx?success");
            }
        }
        catch (Exception exc) { Console.WriteLine(exc.Message); }
    }
    private void getFacebookValues(HttpRequest request)
    {
        //if (!string.IsNullOrEmpty(request.QueryString["code"]))
        //{
        //    OContext.Code = (request.QueryString["code"]);
        //}
        //if (!string.IsNullOrEmpty(request.QueryString["oauth_token"]))
        //    OContext.AccessToken = (request.QueryString["oauth_token"]);
        //OContext.GetAccessToken();
        //displaydata = parser.displayReceiveData(OContext.GetProfileResponse("https://graph.facebook.com/me"), "facebook");
        //parser.emailaddress = displaydata[2];
        //Session["emailaddress"] = displaydata[2]; 
        //string message = saveRegistrationForm(displaydata[2]);
        //    updateusercredential(OContext.AccessToken, OContext.Code, null, displaydata[2], "facebook");

        //    if (message != "availuser" && message != "tryagain")
        //    {
        //        sendMail(displaydata[2], "your link is :http:/developer.csimplifyit.com/Default.aspx?linkshow$" + message);
        //    }
        //parser.saveConnectionData(OContext.GetProfileResponse("https://graph.facebook.com/me/friends"), "facebook");
    }
    private void getTwitterValues(HttpRequest request)
    {
        // if (!string.IsNullOrEmpty(request.QueryString["oauth_verifier"]))
        //     twitter.Verifier = (request.QueryString["oauth_verifier"].ToString());
        // if (!string.IsNullOrEmpty(request.QueryString["oauth_token"]))
        //     twitter.Token = (request.QueryString["oauth_token"]);
        // twitter.TokenSecret = Session["tokensecret"].ToString();
        // twitter.AccessTokenGet();
        // parser.saveConnectionData(twitter.getAllOauthconnection(), "twitter");
        //displaydata= parser.displayReceiveData(twitter.getProfile(), "twitter");

    }
    private void getYahooValues(HttpRequest request)
    {
        if (!string.IsNullOrEmpty(request.QueryString["oauth_verifier"]))
            yahoo.Verifier = (request.QueryString["oauth_verifier"].ToString());
        if (!string.IsNullOrEmpty(request.QueryString["oauth_token"]))
            yahoo.Token = (request.QueryString["oauth_token"]);
        yahoo.TokenSecret = Session["tokensecret"].ToString();
        yahoo.AccessTokenGet();
        string data = yahoo.getAllOauthconnection("http://social.yahooapis.com/v1/user/{0}/contacts?format=JSON");
        //     lblmessage.Text = "Thanks for join us";
        //savedata.saveConnectionData(yahoo.getAllOauthconnection("http://social.yahooapis.com/v1/user/"), "yahoo");
        //  saveAllData("yahoo",yahoo.getAllOauthconnection());
        // string data2 = yahoo.getAllOauthconnection("http://social.yahooapis.com/v1/user/{0}/profile/");
    }
    private void getLinkedinValues(HttpRequest request)
    {
        string[] credential = new string[3];
        if (!string.IsNullOrEmpty(request.QueryString["oauth_verifier"]))
            linkedin.Verifier = (request.QueryString["oauth_verifier"].ToString());
        if (!string.IsNullOrEmpty(request.QueryString["oauth_token"]))
            linkedin.Token = (request.QueryString["oauth_token"]);
        linkedin.TokenSecret = Session["tokensecret"].ToString();
        linkedin.AccessTokenGet();
        credential[0] = linkedin.accesstoken;
        credential[1] = linkedin.accesssecrettoken;
        credential[2] = linkedin.Verifier;
        InsertFromOauth insertLinkedProfile = new InsertFromOauth();
        displaydata = insertLinkedProfile.insertLinkeinProfile(linkedin.getProfile(), credential);
        parser.saveConnectionData(linkedin.getAllOauthconnection(), "linkedin", displaydata);
    }
}


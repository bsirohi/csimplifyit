<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" %>
<asp:Content ID="FreshervillaHead" ContentPlaceHolderID="HeadContent" runat="server">
     <title>Freshervilla | C Simplify IT - Do eCommerce!10x better.</title>
</asp:Content>
<asp:Content ID="FreshervillaMaincontent" ContentPlaceHolderID="MainContent" runat="server">
    <%-- Body or main content of content page --%>    

	<div class="wrapper">
    
		<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs">
			<div class="container">
				<h1 class="pull-left">Fresher Villa</h1>
				<ul class="pull-right breadcrumb">
					<li><a href="index.aspx">Home</a></li>
					<li><a href="#">Products</a></li>
					
				</ul>
			</div>
		</div><!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->

		
        <!--=== Content Part ===-->
		<div class="container content">
			<div class="row portfolio-item margin-bottom-50">
				

				<!-- Content Info -->
				<div class="col-md-5">
					<h2>Freshers Villa - Skill Builder</h2>
					<p>Freshers bring new energies and hopes to any Organization. Their energies, ways of thinking and passion to do something important is unique. Welcome, we will help you gain respect which you deserve.</p>
					<p></p>
					
					<a href="http://freshervilla.appspot.com/" class="btn-u btn-u-large">VISIT THE PROJECT</a>
				</div>
				<!-- End Content Info -->
                <!-- Carousel -->
				<div class="col-md-7">
					<div class="carousel slide carousel-v1" id="myCarousel">
						<div class="carousel-inner">
							<div class="item active">
								<img alt="" src="assets/img/main/freshervilla.png">
								<div class="carousel-caption">
									<p>Freshers bring new energies and hopes to any Organization. </p>
								</div>
							</div>
							<!--<div class="item">
								<img alt="" src="assets/img/main/img12.jpg">
								<div class="carousel-caption">
									<p>Cras justo odio, dapibus ac facilisis into egestas.</p>
								</div>
							</div>
							<div class="item">
								<img alt="" src="assets/img/main/img13.jpg">
								<div class="carousel-caption">
									<p>Justo cras odio apibus ac afilisis lingestas de.</p>
								</div>
							</div>-->
						</div>

						<div class="carousel-arrow">
							<a data-slide="prev" href="#myCarousel" class="left carousel-control">
								<i class="fa fa-angle-left"></i>
							</a>
							<a data-slide="next" href="#myCarousel" class="right carousel-control">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
				</div>
				<!-- End Carousel -->
			</div><!--/row-->

			<div class="tag-box tag-box-v2">
				<p>Freshers bring new energies and hopes to any Organization. Their energies, ways of thinking and passion to do something important is unique. Welcome, we will help you gain respect which you deserve.</p>
			</div>

			<div class="margin-bottom-20 clearfix"></div>

			
		</div><!--/container-->
		<!--=== End Content Part ===-->
 
</div><!--/wrapper-->

    </asp:Content>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MagentoServices.aspx.cs" Inherits="MagentoServices" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>C Simplify IT</title>
	<script src="Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
	<link href="slide/css/global.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
	<script src="slide/js/slides.min.jquery.js" type="text/javascript"></script>
  
	<!-- Menu-->
	<link href="Styles/menu/menu.css" rel="stylesheet" type="text/css" />
	<!-- Menu End-->	
	<!--FANCY BOX-->
	
	<link href="Styles/style.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
	    $(document).ready(function () {
	        jQuery(".pull_feedback").toggle(function () {
	            jQuery("#feedback").animate({ left: "0px" });
	            return false;
	        },
			function () {
			    jQuery("#feedback").animate({ left: "-312px" });
			    return false;
			}
		); //toggle
	    }); //document.ready

	</script>
	
</head>
<body>
	<form id="Form1" runat="server">
   <div class="header">
	<div id="logo">
		<a href="#">
		<img alt="logo" height="56" src="images/C Simplify IT Logo.png"/><img alt="logo" height="40" width="100" src="images/logo.png">
		</a>
		<!--<span>Free HTML5 Template</span>-->
	</div>
	<ul class="menu">
    <li><a>Call Us : +91 98999 76227</li>
	<li><a href="Default.aspx">Home</a></li>
	
	<li><a href="services.aspx">Services</a>

		<ul>
            <li><a href="magentoservices.aspx">E-Commerce Services</a></li>
			<li><a href="context_centeric.aspx">Context Centric Services</a></li>
			<li><a href="cloud_integrations.aspx" >Cloud Integrations</a></li>
			<li><a href="mobility_apps.aspx" >Mobility Apps</a>
			 <ul>
			<li><a href="budget_it_project.aspx">Budget IT Project</a></li>
			
			
			</ul>
			
			
			</li>
			<li><a href="botsandmachinelearning.aspx" >Bots and Machine Learning</a></li>
			<li><a href="do_it_yourself.aspx">Do It Yourself</a>
			<ul>
			<li><a href="fly_high.aspx">Fly High</a></li>
			 </ul>
			</li>
			<li><a href="testing.aspx" >Testing</a></li>

		</ul>

	</li>
	
	<li><a href="solutions.aspx">Solutions</a>
	<ul>
            <li><li><a href="TakeMyJob.aspx">Take My Job</a></li>
			<li><a href="aim_searcher.aspx">Aim Searcher</a></li>
			<li><a href="brand_thermometer.aspx" >Brand Thermometer</a></li>
			<li><a href="brand_xtender.aspx" >Brand Xtender</a></li>
			<li><a href="constant_connect.aspx" >Constant Connect</a></li>
			<li><a href="spreadIn_market.aspx" >SpreadIn Market</a></li>
		<li><a href="stepIn_fast.aspx" >StepIn Fast</a></li>

		</ul></li>


		<li><a href="verticals.aspx">Verticals</a>

		<ul>
			<li><a href="financial_services.aspx">Financial  Services</a></li>
			<li><a href="healthcare_services.aspx" >Healthcare Services</a></li>
		</ul> 

	</li>
	<li class="contact_us_menu"><a  href="#">Contact Us</a>
		<ul class="last-childs">
				<li><a href="About.aspx">About us</a></li>
				<li><a href="p_promoters.aspx">Product Promoters</a></li>
				<li><a href="p_sellers.aspx" >Product Sellers</a></li>
		</ul>
	
	</li>

</ul> <!-- end .menu -->
   

</div>
				   
		<div class="main">
		<div id="container">
        <div class="clear"></div>
         <div class="header"></div>

             <div class="header" style="height:120px"; >
                 <div style="height:40px;margin-left:30%"> <p style="text-align:center;font-weight:bold;font-size:26px;float:left;margin-top: 10px;">Magento Services </p><img style="float:left" alt="logo" height="45" src="images/Magento-Logo.jpg"/></div>
                
                 <p></p>
                 <div style="border-top:1px solid;border-right:1px solid;border-left:1px solid;width:960px;margin-left:2px;">
                    <p style="text-align:center;font-size:15px; margin:2px 0px 0px 5px;">Our Magento practice offers a comprehensive suite of eCommerce related services spanning consulting, enterprise services and functionality additions. Our robust retail process knowledge, specialized tools, and consulting expertise helps us to enhance eCommerce efficiency and fulfillment precision, reduce costs and decrease cycle times from order to fulfillment.</p>
                 </div>
                 
             </div>
         <table >
           <tr><td style="border:1px solid #a1a1a1";>
         <div class="clear"></div>
       
 <div class="three_fourth">
  <p style="text-align:center;font-weight:bold; margin-top:15px">Services For You To Stay Connected</p>
 <p></p>
 <ul>
    <li>Magento Front-End Integration Framework helps reduce effort by 60-70%, useful for integration with workflow based applications and systems</li>
    <li>IPRs to generate test data in master data structures, encrypt/change prod data, and dynamic UI generation for masters.</li>
    <li>Business Process Services - Knowledge of more than 500+ processes/workflows.</li>
    <li>Maintenance FREE IT*</li>
 </ul>
</div>
<div class="one_fourth column-last">
<%--<a  href="https://itunes.apple.com/us/app/take-my-job/id647966003?mt=8&uo=4" target="itunes_store"style="display:inline-block;overflow:hidden;background:url(http://linkmaker.itunes.apple.com/htmlResources/assets/images/web/linkmaker/badge_appstore-lrg.png) no-repeat;width:135px;height:40px;margin:30% 30% 30% 30%;@media only screen{background-image:url(http://linkmaker.itunes.apple.com/htmlResources/assets/images/web/linkmaker/badge_appstore-lrg.svg);}"></a>--%>
<br />
<br />
<img src="images/ServicesForYouToStayConnected .png" alt="ServicesForYouToStayConnected">
</div>
</td></tr>

<div class="clear"></div>
<tr><td style="border:1px solid #a1a1a1; background-color:White">
<div class="header">  </div>
         <div class="one_fourth" >
<img src="images/ExtensiveMagentoDevelopmentServices.png" alt="ExtensiveMagentoDevelopmentServices">
<br />
</div>
 <div class="three_fourth column-last" >
  <p style="text-align:center;font-weight:bold; margin-top:5px">Extensive Magento Development Services from C Simplify IT</p>
 <p style="text-align:center;font-size:15px; margin:2px 0px 0px 5px;">C Simplify IT is well known provider of professional Magento development services. Our company has a wide range of ready-to-use Magento products, a set of package services for Magento website creation, as well as custom development and integration services at competitive prices.</p>
</div>
</td></tr>
   <tr><td style="border:1px solid #a1a1a1";>
         <div class="clear"></div>
       
 <div class="three_fourth">
  <p style="text-align:center;font-weight:bold; margin-top:15px">C Simplify IT Magento Development Packages</p>
 <p></p>
 <p style="text-align:center;font-size:15px; margin:2px 0px 0px 5px;">If you want to get a fully-operating Magento website within short deadlines and limited budget, ordering All-in-One Magento development package would be the best choice! Once you opt for it, we take up all the worries and perform a set of actions to set up a website from scratch.</p>
</div>
<div class="one_fourth column-last">
<img src="images/scanourservice.png" alt="scanourservice">

</div>
</td></tr>
<div class="clear"></div>
<tr><td style="border:1px solid #a1a1a1; background-color:White">
<div class="header"> </div>
         <div class="one_fourth" >
<img src="images/WideRangeOfServices.png" alt="WideRangeOfServices">
</div>
 <div class="three_fourth column-last" >
  <p style="text-align:center;font-weight:bold; margin-top:5px">C Simplify IT Magento Service has a Wide Range of Modules on Offer</p>
 <p style="text-align:center;font-size:15px; margin:2px 0px 0px 5px;">C Simplify IT is happy to introduce a great number of Magento modules and extensions that are bound to increase business success of your online store. Magento modules on offer comply with international Magento standards and thus are compatible with all Magento versions. Our team can well assure you that we have just enough experience to provide professional Magento web development of any complexity level.</p>
</div>
	   	   

</td></tr>

 <tr><td style="border:1px solid #a1a1a1";>
         <div class="clear"></div>
       
 <div class="three_fourth">
  <p style="text-align:center;font-weight:bold; margin-top:15px">C Simplify IT Offshore dedicated teams</p>
 <p></p>
 <p style="text-align:center;font-size:15px; margin:2px 0px 0px 5px;">Our offshore dedicated team is the most appealing solution for companies in need of extending their in-house development capacity without the overheads of additional direct manpower and infrastructure resources. It is also a good solution for start-ups looking for lower costs and high quality solutions provided by real Magento experts.</p>
</div>
<div class="one_fourth column-last">
<img src="images/DedidatedTeam.png" alt="DedidatedTeam">
</div>
</td></tr>
<div class="clear"></div>
<tr><td style="border:1px solid #a1a1a1; background-color:White">
<div class="header"></div>
         <div class="one_fourth" >
<img src="images/CustomMagentoDevelopment.png" alt="CustomMagentoDevelopment">
</div>
 <div class="three_fourth column-last" >
  <p style="text-align:center;font-weight:bold; margin-top:5px">Custom Magento Development Services is What We Do Best</p>
 <p style="text-align:center;font-size:15px; margin:2px 0px 0px 5px;">Our team of developers comprises of specialists with extensive experience in Magento technologies. We have been developing Magento based websites of various complexity over the past 2 years. That included development from scratch and upgrade of existing Magento modules and extensions.</p>
</div>
	   	   

</td></tr>
<div class="clear"></div>
<tr><td style="border:1px solid #a1a1a1; background-color:White">
<div class="three_fourth">
  <p style="text-align:center;font-weight:bold; margin-top:15px">Magento Has it All for Your Online Business</p>
 <p></p>
 <p style="text-align:center;font-size:15px; margin:2px 0px 0px 5px;">So, don’t hesitate to contact us or Get a Quote for your specific details. We will readily study your requirements and offer a working solution within the set budget and deadline. We arelooking forward to interesting and productive cooperation.</p>
</div>
<div class="one_fourth column-last">
<img src="images/wideRangeOfModule.png" alt="wideRangeOfModule">
</div>
</td></tr>
 <tr><td> 
<div class="clear"></div>
<div class="header">
         </div>
         </td></tr>

</table>
		</div>
	  <div id="feedback">

		
			<h2>We Love to hear from you</h2>
			
		<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
		<asp:UpdatePanel ID="UpdatePanel1" runat="server"  UpdateMode="Conditional" >
		<ContentTemplate>
	   
		<fieldset id="inputs" class="errormessage">
			<asp:Label ID="Label1" runat="server" Visible="false" Text="Label" required></asp:Label><asp:RequiredFieldValidator
				ID="RequiredFieldValidator1" ControlToValidate="name" runat="server" ErrorMessage="Name Required" CssClass="errormessage"></asp:RequiredFieldValidator>
			<asp:TextBox ID="name" runat="server" placeholder="Name"></asp:TextBox>
			<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
				ControlToValidate="email" ErrorMessage="Email Required" CssClass="errormessage"></asp:RequiredFieldValidator>
			<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
				ControlToValidate="email"  ErrorMessage="Invalid Email" 
				ValidationExpression="\w+([-+.]\w+)*[-+]?@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
				CssClass="errormessage"></asp:RegularExpressionValidator>
			   <asp:TextBox ID="email" runat="server" placeholder="Email"></asp:TextBox>
		 <asp:TextBox ID="query" TextMode="multiline" runat="server" style="resize: none;" placeholder="Your Feed back"></asp:TextBox>
			
		 </fieldset>
		
		 <fieldset id="actions">
   
		<%--<asp:Button ID="submit" runat="server"  Text="Submit" onclick="submit_Click" />--%>
	   
		 </fieldset>
		  </ContentTemplate>
		 </asp:UpdatePanel>
		
		<a href="#" class="pull_feedback" id="invoke_button" title="Click to leave feedback">Feedback</a>
	</div>


				
		<div class="container footer_wrapper" id="body-wrapper" style="margin-bottom:10px;">
			<asp:SiteMapPath ID="SiteMapPath1" SkipLinkText="Skip Breadcrumb" runat="server">
			</asp:SiteMapPath>
            
		<div class="">
		<footer>
		<div class="one_fifth">
		<a href="Default.aspx">Home</a>
		<ul>
		
		</ul> 
		</div>
		 <div class="one_fifth">
		<a href="services.aspx">Services</a>
		<ul>
        <li><a href="magentoservices.aspx">E-Commerce Services</a></li>
		<li><a href="context_centeric.aspx">Context Centric Services</a></li>
			<li><a href="cloud_integrations.aspx" >Cloud Integrations</a></li>
			<li><a href="mobility_apps.aspx" >Mobility Apps</a></li>
			<li><a href="botsandmachinelearning.aspx" >Bots and Machine Learning</a></li>
			<li><a href="do_it_yourself.aspx">Do It Yourself</a></li>
          	<li><a href="testing.aspx" >Testing</a></li>

		</ul> 
		</div> 
		
		<div class="one_fifth">
		<a href="solutions.aspx">Solutions</a>
		<ul>
        <li><li><a href="TakeMyJob.aspx">Take My Job</a></li>
		<li><a href="aim_searcher.aspx">Aim Searcher</a></li>
			<li><a href="brand_thermometer.aspx" >Brand Thermometer</a></li>
			<li><a href="brand_xtender.aspx" >Brand Xtender</a></li>
			<li><a href="constant_connect.aspx" >Constant Connect</a></li>
			<li><a href="spreadIn_market.aspx" >SpreadIn Market</a></li>
		<li><a href="stepIn_fast.aspx" >StepIn Fast</a></li>


		</ul> 
		</div> 
		 <div class="one_fifth">
		<a href="services.aspx">Verticals</a>
		<ul>
		<li><a href="financial_services.aspx">Financial  Services</a></li>
			<li><a href="healthcare_services.aspx" >Healthcare Services</a></li>
			

		</ul> 
		</div> 
		
		<div class="one_fifth column-last">
		<a  href="#">Contact Us</a>
		<ul>
		<li><a href="About.aspx">About us</a></li>
				<li><a href="p_promoters.aspx">Product Promoters</a></li>
				<li><a href="p_sellers.aspx" >Product Sellers</a></li>
		</ul> 
		</div>
		
		</footer>
		</div>
			<%--<ul id="footer-ul">
				<li><a href="Default.aspx">Home</a></li>
				<li><a href="About.aspx">About us</a></li>
				<li><a href="do_it_yourself.aspx">Do it yourself</a></li>
				<li></li>
				<li><a href="stepIn_fast.aspx">Plans Detail</a></li>
			</ul>--%>
		
			
		
			
		</div><!--container-->
	
	</div><!--footer-links-->
	<div class="clear"></div>

		<p class="allrights" > Copyright © 2013-14 CSimplifyIT. All rights reserved.</p>


</form>
	<!--Nivo Slider-->
   <script src="Scripts/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
	<link href="Scripts/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
	    $(document).ready(function () {
	        $("a#fancybox").fancybox();
	    });
	</script>
	<style>
	.fancy a img {
	border: 1px solid #BBB;
	padding: 2px;
	margin: 10px 20px 10px 0;
	vertical-align: top;
	}
	.fancy a img.last {
	margin-right: 0;	
	}
	</style>
<!--FANCY BOX-->
	
	
</body>

<!--
use this instead of current script to play slider if want caption & uncommnet the caption styling code in global.css too 

<script>
		$(function(){
			$('#slides').slides({
				preload: true,
				preloadImage: 'img/loading.gif',
				play: 5000,
				pause: 2500,
				hoverPause: true,
				animationStart: function(current){
					$('.caption').animate({
						bottom:-35
					},100);
					if (window.console && console.log) {
						// example return of current slide number
						console.log('animationStart on slide: ', current);
					};
				},
				animationComplete: function(current){
					$('.caption').animate({
						bottom:0
					},200);
					if (window.console && console.log) {
						// example return of current slide number
						console.log('animationComplete on slide: ', current);
					};
				},
				slidesLoaded: function() {
					$('.caption').animate({
						bottom:0
					},200);
				}
			});
		});
	</script>-->
 <!-- Created by Sahil Popli Dated-24-09-2012 -->
 
</html>

